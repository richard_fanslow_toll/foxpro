*!* MARC JACOBS WHLS MBOL 945 (Whse. Shipping Advice) Creation Program
*!* Creation Date: 01.18.2012 by Joe

PARAMETERS cBOL,nAcctNum,cOffice

PUBLIC ARRAY thisarray(1)
PUBLIC c_CntrlNum,c_GrpCntrlNum,cReturned,cWO_Num,cWO_NumStr,nWO_Num1,lTesting,lUpdated,nSubCnt,cMailName,lOverflow
PUBLIC lEmail,lDoMJFilesOut,dDateTimeCal,nFilenum,tsendto,tcc,tsendtoerr,tccerr,lPSU,cProgname,NormalExit,cErrMsg
PUBLIC nPTCount,nTotSubs,cPTCount,cPTString,tsendtotest,tcctest,tccwhse,lTrackFlag,lcHoldPath,lcArchivePath,lcHoldPath
PUBLIC cMod,cMBOL,cFilenameShort,cFilenameOut,cFilenameArch,lParcelType,cWO_NumList,lcPath,cEDIType,cISA_Num,cSubBOLDetail
cProcName = "marcjacobsw945_create_mbol"

CLEAR

TRY

	lTesting   = .f.  && Set to .t. for testing
	lTestinput = .F.  &&  Set to .t. for test input files only!
	lDoCompare = .F.

	DO m:\dev\prg\_setvars WITH lTesting
	ON ESCAPE CANCEL
	SET REPROCESS TO 500
	ON ERROR RETRY

	IF lTesting && AND lTestinput
*!* TEST DATA AREA
		CLOSE DATABASES ALL
		IF !USED('edi_trigger')
			cEDIFolder = "F:\3PL\DATA\"
			USE (cEDIFolder+"edi_trigger") IN 0 ALIAS edi_trigger
		ENDIF
		cBOL = "04070086303324101"
		cTime = DATETIME()
		cOffice = "N"
		cMod  = "J"
	ENDIF

	IF !TYPE("cOffice")="C"
		IF !INLIST(cOffice,"J","L","N")
			WAIT WINDOW "Must pass an office or a correct office............."
			RETURN
		ENDIF
	ENDIF
	cOffice = IIF(cOffice = "J","N",cOffice)
	cMod = IIF(cOffice = "N","J",cOffice)
	goffice = cMod
	gMasterOffice = cOffice

	useca("ackdata","wh")

	cMBOL = ""
	lParcelType = .F.
	cEDIType = "945"
	cSubBOLDetail =  ""
	lOverflow = .F.

	cProgname = "marcjacobsw945_create"
	STORE "" TO tattach,cShip_ref,ccustname,cWO_Num,cWO_NumStr,cWO_NumList,tcc,tsendto,tsendtoerr,tccerr,tsendtotest,tcctest
	lIsError = .F.
	lCloseOutput = .T.
	lPick = .T.
	lPrepack = !lPick
	NormalExit = .F.
	lUpdated = .F.
	lTrackFlag = .F.
	nOrigSeq = 0
	STORE 0 TO nFilenum,nSubCnt,nPTCount

	IF lTrackFlag
		THROW
	ENDIF

	WAIT WINDOW "At the start of MARC JACOBS WHOLESALE 945 "+IIF(lTesting,"TEST "," ")+"MBOL process..." TIMEOUT 2

	lTrackFlag = .T.  && Using this to shut down procesa in case server 945 process loops

	lFederated = .F.
	IF VARTYPE(nAcctNum) = "L"
		nAcctNum = 6303
	ENDIF
*	cOffice = "J"  no longer hard coded, comes in as a parameter, added in ML, PG 02/05/2015
	cPPName = "MarcJacobsW"
	tfrom = "Toll EDI Operations <toll-edi-ops@tollgroup.com>"
	tmessage=""
	STORE .F. TO lUPSType,lPSU,lSQLMail
	lISAFlag = .T.
	lSTFlag = .T.
	nLoadid = 1

*	CREATE CURSOR temp945amj (accountid i,isa_num c(10),st_num c(10),wo_num i,bol_no c(20),ship_ref c(20),filename c(50),filedate T)
	cBOL=TRIM(cBOL)
	cMBOL=TRIM(cBOL)

	IF lTestinput
		cUseFolder = "F:\WHP\WHDATA\"
	ELSE
		xReturn = "XXX"
		DO m:\dev\prg\wf_alt WITH cMod,nAcctNum
		cUseFolder = UPPER(xReturn)
	ENDIF

	WAIT WINDOW "Folder Used: "+cUseFolder TIMEOUT 2

	lEmail = IIF(cBOL = '99999',.F.,.T.)
	lDoMJFilesOut = IIF(cBOL = '99999',.F.,.T.) && If true, copies output to FTP or testout folders (default = .t.)
	lTestMail = lTesting && Sends mail to Joe only
	lStandalone = lTesting
	STORE "LB" TO cWeightUnit

*!*	lTestMail = .t.
*!*	lDoMJFilesOut = .f.

	closedata()

	WAIT WINDOW "Now preparing tables and variables" NOWAIT NOCLEAR
	STORE "" TO lcPath,lcArchivePath,lcHoldPath
*!* SET CUSTOMER CONSTANTS
	ccustname = "MARCJACOBSW"  && Customer Identifier
	cX12 = "004010"  && X12 Standards Set used
	cMailName = "Marc Jacobs (W)"
	SELECT 0
	USE F:\3pl\DATA\mailmaster ALIAS mm
	LOCATE FOR mm.edi_type = "945" AND mm.office=cOffice AND mm.accountid = 6303
	IF !FOUND()
		WAIT WINDOW "Acct Params not found in Mailmaster" TIMEOUT 2
		DO ediupdate WITH "Mailmaster Error",.T.
		THROW
	ENDIF
	lcPath = ALLTRIM(mm.basepath)
	lcArchivePath = ALLTRIM(mm.archpath)
	lcHoldPath = ALLTRIM(mm.HoldPath)
	tsendto = IIF(mm.use_alt,sendtoalt,sendto)
	tcc = IIF(mm.use_alt,ccalt,cc)
	LOCATE
	LOCATE FOR mm.edi_type = 'MISC' AND mm.taskname = "MJADDWHSE"
	tccwhse = IIF(mm.use_alt,ccalt,cc)
	LOCATE
	LOCATE FOR mm.edi_type = 'MISC' AND mm.taskname = "MJTEST"
	tsendtotest = IIF(mm.use_alt,sendtoalt,sendto)
	tcctest = IIF(mm.use_alt,ccalt,cc)
	tsendtoerr = IIF(mm.use_alt,sendtoalt,sendto)
	tccerr = IIF(mm.use_alt,ccalt,cc)
	USE IN mm

	lcPath = IIF(lTesting,"F:\FTPUSERS\MarcJacobs\945-Staging\testout\",lcPath)

	lcHoldPath = lcHoldPath+"mbolhold\"
	CD &lcHoldPath
	DELETE FILE(lcHoldPath+"*.*")

*!* SET OTHER CONSTANTS
	DO CASE
		CASE INLIST(cOffice,"N", "J")
			cCustLoc =  "NJ"
			cFMIWarehouse = ""
			cCustPrefix = "945"+"j"
			cDivision = "New Jersey"
			cFolder = "WHN"
			cSF_Addr1  = "800 FEDERAL BLVD"
			cSF_CSZ    =  "CARTERET*NJ*07008"
			csendid  = 'USNJ1DC'
			Crcvid    = '2129070080'
			cfilepath = 'F:\FTPUSERS\MJ_Wholesale\WMS_Outbound\945Out\'

		CASE cOffice = "L"
			cCustLoc =  "CA"
			cFMIWarehouse = ""
			cCustPrefix = "945"+"c"
			cDivision = "California"
			cFolder = "WHC"
			cSF_Addr1  = "3355 DULLES DRIVE"
			cSF_CSZ    =  "MIRA LOMA*CA*91752"
			csendid  = 'USCA1DC'
			Crcvid    = '2129070080'
			cfilepath = 'F:\FTPUSERS\MJ_Wholesale_CA\WMS_Out\945Out\'

		CASE cOffice = "M"
			cCustLoc =  "FL"
			cFMIWarehouse = ""
			cCustPrefix = "945"+"f"
			cDivision = "Florida"
			cFolder = "WHM"
			cSF_Addr1  = "11400 NW 32ND AVE"
			cSF_CSZ    = "MIAMI*FL*33167"

		OTHERWISE
			cCustLoc =  "CA"
			cFMIWarehouse = ""
			cCustPrefix = "945"+"c"
			cDivision = "California"
			cFolder = "WHC"
			cSF_Addr1  = "450 WESTMONT DRIVE"
			cSF_CSZ    = "SAN PEDRO*CA*90731"
			cfilepath = 'F:\FTPUSERS\MJ_Wholesale_CA\WMS_Outbound\945Out\'
	ENDCASE

	lNonEDI = .F.  && Assumes not a Non-EDI 945
	STORE "" TO lcKey
	STORE 0 TO alength,nLength
	cTime = DATETIME()
	cDelTime = SUBSTR(TTOC(cTime,1),9,4)
	cCarrierType = "M" && Default as "Motor Freight"
	dapptnum = ""

*!* SET OTHER CONSTANTS

	cfd = "*"
	csegd = "~"
	nSegCtr = 0
	cterminator = ">" && Used at end of ISA segment
	cCustFolder = "MarcJacobs"

	IF USED('OUTSHIP')
		USE IN outship
	ENDIF
	IF USED('outdet')
		USE IN outdet
	ENDIF

	xsqlexec("select * from outship where accountid = "+TRANSFORM(nAcctNum)+" and bol_no = '"+cBOL+"'",,,"wh")
	INDEX ON bol_no TAG bol_no
	INDEX ON ship_ref TAG ship_ref
	INDEX ON wo_num TAG wo_num

	selectoutdet()
	SELECT outdet
	INDEX ON outdetid TAG outdetid

	IF !USED("scacs")
		USE ("f:\3pl\data\parcel_carriers") IN 0 ALIAS scacs ORDER TAG scac
	ENDIF

	IF USED('bl')
		USE IN bl
	ENDIF

	csq1 = [select * from bl where mblnum =']+cMBOL+[']
	xsqlexec(csq1,"bl",,"wh")

	SELECT 0
	SELECT bol_no FROM bl WHERE bl.mblnum = cMBOL INTO CURSOR tempbl1 GROUP BY 1
	IF lTesting OR lTestinput
*			BROWSE
	ENDIF
	STORE RECCOUNT() TO nSubBillCnt
	USE IN tempbl1
	SELECT bl
	LOCATE

	SELECT 0
	CREATE CURSOR edipop (filename c(80),ship_ref c(20),bol c(20))  && Temp table for sub-BOL file info

	SELECT 0
	CREATE CURSOR tempinfo (subbol c(20),ptcount c(10),ptstring m)

*!* Mail sending for all MBOL 945s
*	ASSERT .F. MESSAGE "At creation of MBOL notice...>>DEBUG<<"
	IF !lTesting
		tsubject2 = "NOTICE: MARC JACOBS (W) 945 MBOL Process"
		SELECT 0
		USE F:\3pl\DATA\mailmaster ALIAS mm SHARED
		LOCATE FOR mm.edi_type = "MISC" AND mm.taskname = "GENERAL"
		tsendto2 = IIF(mm.use_alt,mm.sendtoalt,mm.sendto)
		tcc2 = IIF(mm.use_alt,mm.ccalt,mm.cc)
		USE IN mm

		tattach2 = " "
		tmessage2 = "Potential files for MARC JACOBS (W) 945 MBOL# "+cMBOL+"...monitor output for correct number of files."
		tmessage2 = tmessage2 + CHR(13) + "Expected number of 945 files: "+ALLTRIM(STR(nSubBillCnt))
		tmessage2 = tmessage2 + CHR(13) + "Processed at "+TTOC(DATETIME())
		DO FORM m:\dev\frm\dartmail2 WITH tsendto2,tfrom,tsubject2,tcc2,tattach2,tmessage2,"A"
	ENDIF
	WAIT WINDOW "" TIMEOUT 1
*!* End Mailing section

	IF !SEEK(cBOL,'outship','bol_no')
		WAIT WINDOW "BOL not found in OUTSHIP" TIMEOUT 2
		DO ediupdate WITH "BOL NOT FOUND",.T.
		THROW
	ELSE
		nWO_Num = outship.wo_num
		cSCAC = ALLTRIM(outship.scac)
		IF SEEK(cSCAC,'scacs','scac')
			lUPSType = .T.
			cCarrierType = "U"
			lFedex = scacs.fedex
		ENDIF
	ENDIF

	SELECT DISTINCT dcnum FROM outship WHERE outship.bol_no = cBOL GROUP BY 1 INTO CURSOR tempx
	LOCATE
	IF EMPTY(ALLTRIM(tempx.dcnum))
		SET STEP ON
		SELECT outship
		REPLACE dcnum WITH '9999',storenum WITH 9999 FOR outship.bol_no = cBOL AND EMPTY(outship.dcnum) IN outship
		SELECT dcnum FROM outship WHERE outship.bol_no = cBOL GROUP BY 1 INTO CURSOR tempx
*		xsqlexec("update outship set dcnum = '9999',storenum = 9999 where bol_no = '"+cMBOL+"' and dcnum = ' '",,,"wh")
*		xsqlexec("select dcnum from outship where bol_no = '"+cMBOL+"'","tempx",,"wh")
	ENDIF
	IF EMPTY(tempx.dcnum)
		USE IN tempx
		SELECT storenum FROM outship WHERE outship.bol_no = cBOL GROUP BY 1 INTO CURSOR tempx
		IF EOF()
			DO ediupdate WITH "EMPTY DC/STORE#",.T.
			THROW
		ENDIF

		IF EMPTY(tempx.storenum)
			DO ediupdate WITH "EMPTY DC/STORE#",.T.
			THROW
		ELSE
			IF RECCOUNT('tempx')  > 1
				DO ediupdate WITH "MULTI DC NUMS",.T.
				THROW
			ENDIF
		ENDIF
	ENDIF
	IF USED('tempx')
		USE IN tempx
	ENDIF

	lDoCompare = .F. && these are all scanpack
	IF lDoCompare
		cRetMsg = "X"
		DO m:\dev\prg\sqldata-comparemj WITH cUseFolder,cBOL,nWO_Num,cOffice,nAcctNum,cPPName
		IF cRetMsg<>"OK"
			lCloseOutput = .T.
			cWO_Num = ALLTRIM(STR(nWO_Num1))
			DO ediupdate WITH "BAD SQL COMPARE",.T.
			THROW
		ENDIF
	ENDIF

	IF !USED('OUTDET')
		USE (cUseFolder+"OUTDET") IN 0 NOUPDATE ALIAS outdet
		SELECT outdet
		LOCATE
	ENDIF

&& Added code blocks to the following to overcome BOL vs. WO selection issues, 12.23.2005
	SELECT outship
	LOCATE
	IF USED("sqlwomj")
		USE IN sqlwomj
	ENDIF
	DELETE FILE "F:\3pl\DATA\sqlwomj.dbf"
	SELECT bol_no,wo_num ;
		FROM outship WHERE bol_no = cBOL ;
		GROUP BY 1,2 ;
		ORDER BY 1,2 ;
		INTO DBF F:\3pl\DATA\sqlwomj
	USE IN sqlwomj
	USE F:\3pl\DATA\sqlwomj IN 0 ALIAS sqlwomj

	cRetMsg = ""

	DO m:\dev\prg\sqlconnectmj_bol  WITH nAcctNum,ccustname,cPPName,.T.,cOffice

	IF cRetMsg<>"OK"
		DO ediupdate WITH cRetMsg,.T.
		THROW
	ENDIF

	IF lTesting
		SELECT vmarcjacobswpp
*		BROWSE
	ENDIF

	SELECT outship
	LOCATE
	WAIT CLEAR
	WAIT WINDOW "Now creating Header information" NOWAIT NOCLEAR

	SELECT outship
	SET ORDER TO TAG wo_num
	LOCATE

	IF !SEEK(nWO_Num)
		WAIT CLEAR
		WAIT WINDOW "Invalid Work Order Number - Not Found in OUTSHIP!" TIMEOUT 2
		DO ediupdate WITH "WO NOT FOUND",.T.
		THROW
	ENDIF

	IF !lTesting
		IF EMPTY(cBOL)  && May be empty if triggered from PT screen
			WAIT CLEAR
			WAIT WINDOW "MISSING Bill of Lading Number!" TIMEOUT 2
			DO ediupdate WITH "BOL# EMPTY",.T.
			THROW
		ELSE
			IF !SEEK(PADR(TRIM(cBOL),20),"outship","bol_no")
				WAIT CLEAR
				WAIT WINDOW "Invalid Bill of Lading Number - Not Found!" TIMEOUT 2
				DO ediupdate WITH "BOL# NOT FOUND",.T.
				THROW
			ENDIF
		ENDIF
	ENDIF

*************************************************************************
*1	PTIDSOURCE/OUTSHIP RECORD MAIN LOOP
*	Loops through individual OUTSHIP lines for BOL#
*************************************************************************
	WAIT CLEAR
	WAIT WINDOW "Now creating BOL#-based information..." NOWAIT NOCLEAR

	SELECT outship
	SET ORDER TO
	IF lTesting
		COUNT TO N FOR outship.bol_no = cBOL
	ELSE
		COUNT TO N FOR outship.bol_no = cBOL AND !Emptynul(del_date)
	ENDIF
	IF N=0
		DO ediupdate WITH "INCOMP BOL, Del Date",.T.
		THROW
	ENDIF
	cMissDel = ""

	WAIT CLEAR
	WAIT WINDOW "Now creating Master BOL#-based information..." TIMEOUT 1
	cMBOL = ALLTRIM(cBOL)
	SELECT bl
	bolscanstr = "bl.mblnum = cMBOL"
	COUNT TO nTotSubs FOR &bolscanstr

	WAIT WINDOW "Will scan through "+TRANSFORM(nTotSubs)+" sub-BOLs..." TIMEOUT 1

	LOCATE
	LOCATE FOR &bolscanstr
	SCAN FOR &bolscanstr
		nBLRec = RECNO()
		STORE TRIM(bl.bol_no) TO cBOL
*?cBOL
		DO m:\dev\prg\swc_cutctns_gen WITH cBOL

		IF cMBOL = '04070086303751167' AND cBOL # '04070086303748396'
			LOOP
		ENDIF

		dDateTimeCal = DATETIME()
		dt1 = TTOC(dDateTimeCal,1)
		dtmail = TTOC(dDateTimeCal)
		dt2 = dDateTimeCal
		cString = ""
		IF INLIST(cOffice,"N","J")
			csendqual = "ZZ"
			csendid = "USNJ1DC"
			crecqual = "12"
			crecid = "2129070080"
		ENDIF

		IF cOffice='L'
			csendqual = "ZZ"
			csendid = "USCA1DC"
			crecqual = "12"
			crecid = "2129070080"
		ENDIF

		cdate = DTOS(DATE())
		cTruncDate = RIGHT(cdate,6)
		cTruncTime = SUBSTR(TTOC(dDateTimeCal,1),9,4)
		cfiledate = cdate+cTruncTime
		IF cOffice='J'
			cOrig = "J"
		ENDIF

		IF cOffice='L'
			cOrig = "L"
		ENDIF

		nSTCount = 0
		cStyle = ""
		cPTString = ""
		nTotCtnWt = 0
		nTotCtnCount = 0
		cTargetStyle = ""

** PAD ID Codes
		csendidlong = PADR(csendid,15," ")
		crecidlong = PADR(crecid,15," ")

*		ASSERT .F. MESSAGE "At hold path"
		cFilenameHold = (lcHoldPath+cCustPrefix+dt1+".txt")
		cFilenameShort = JUSTFNAME(cFilenameHold)
		cFilenameOut = (lcPath+cFilenameShort)
		cFilenameArch = (lcArchivePath+cFilenameShort)
		nFilenum = FCREATE(cFilenameHold)

		DO num_incr_isa
		headerfill()

		nAcctNum = bl.accountid

		nBLID = bl.blid
		nSubCnt = nSubCnt + 1

		xsqlexec("select * from bldet where mod='"+goffice+"' and blid="+TRANSFORM(nBLID),,,"wh")

		SELECT bldet
		SCAN FOR bldet.blid = nBLID AND bldet.accountid = nAcctNum
			CREATE CURSOR temp945amj (accountid i,isa_num c(10),st_num c(10),wo_num i,bol_no c(20),ship_ref c(20),filename c(50),filedate T)
			cBLShip_ref = ALLTRIM(bldet.ship_ref)

			IF "OV"$cBLShip_ref
				LOOP
			ENDIF

			INSERT INTO edipop (filename,ship_ref,bol) VALUES (cFilenameArch,cBLShip_ref,cBOL)

			lCombPT = IIF(bldet.combinedpt,.T.,.F.)
			IF lCombPT
				bldetscanstr = "outship.combpt = PADR(cBLShip_ref,20) AND outship.accountid = nAcctNum"
			ELSE
				bldetscanstr = "outship.ship_ref = PADR(cBLShip_ref,20) AND outship.accountid =nAcctNum"
			ENDIF

			SELECT outship  && shift to OUTSHIP processing

			LOCATE FOR accountid = nAcctNum AND &bldetscanstr
			IF !FOUND()
				ASSERT .F. MESSAGE "At no-find of Outbound PT#"
				cErrMsg = "MISS OBD PT"
				DO ediupdate WITH cErrMsg,.T.
				THROW
			ENDIF
			LOCATE

			COUNT TO N FOR (!Emptynul(del_date) AND accountid = nAcctNum AND &bldetscanstr)
			IF N=0
				cErrMsg = "INCOMP SUB-BOL: "+cBOL
				ASSERT .F. MESSAGE cErrMsg+" >>DEBUG<<"
				SET STEP ON
				DO ediupdate WITH cErrMsg,.T.
				THROW
			ELSE
				IF lCombPT
					WAIT WINDOW "Number of Combined PTs: "+ALLTRIM(STR(N)) TIMEOUT 2
				ENDIF
			ENDIF

			LOCATE  && still in outship
			SCAN FOR accountid = nAcctNum AND &bldetscanstr
				SCATTER MEMVAR MEMO
				IF outship.qty = 0 OR (outship.ctnqty = 0 AND !outship.masterpack)
					LOOP
				ENDIF
				alength = ALINES(apt,outship.shipins,.T.,CHR(13))

				lMacy = IIF("MACY"$m.consignee,.T.,.F.)
				lJCP = IIF("PENNEY"$m.consignee,.T.,.F.)
				nWO_Num = outship.wo_num
				cWO_Num = ALLTRIM(STR(nWO_Num))
				IF !(cWO_Num$cWO_NumStr)
					cWO_NumStr = IIF(EMPTY(cWO_NumStr),cWO_Num,cWO_NumStr+","+cWO_Num)
				ENDIF
				IF !(cWO_Num$cWO_NumList)
					cWO_NumList = IIF(EMPTY(cWO_NumList),cWO_Num,cWO_NumList+CHR(13)+cWO_Num)
				ENDIF
				IF !lTestinput
					xsqlexec("select * from outwolog where accountid = "+TRANSFORM(nAcctNum)+" and wo_num = "+TRANSFORM(nWO_Num),,,"wh")
					LOCATE
					lPick = outwolog.picknpack
					lPrepack = !lPick
				ENDIF
				nTotCtnCount = m.qty
				cPO_Num = ALLTRIM(m.cnee_ref)
				cShip_ref = ALLTRIM(m.ship_ref)
				nPTCount = nPTCount+1

				IF LEFT(cBOL,3)="PSU" AND "MACY"$outship.consignee
					lPSU = .T.
				ENDIF

*!* Added this code to trap miscounts in OUTDET Units
				IF lPrepack
					SELECT outdet
					SET ORDER TO
					SUM totqty TO nUnitTot1 FOR units AND outdet.outshipid = outship.outshipid
					SUM (VAL(PACK)*totqty) TO nUnitTot2 FOR !units AND outdet.outshipid = outship.outshipid
					IF nUnitTot1<>nUnitTot2
						SET STEP ON 
						WAIT WINDOW "UNITS TOTQTY ERROR AT PT "+cShip_ref TIMEOUT 5
						DO ediupdate WITH "OUTDET UNITS TOTQTY ERR",.T.
						THROW
					ENDIF

					SELECT vmarcjacobswpp
					SUM totqty TO nUnitTot2 FOR vmarcjacobswpp.outshipid = outship.outshipid
					IF nUnitTot1<>nUnitTot2
						DO ediupdate WITH "OUTDET/SQL UNITS TOTQTY ERR",.T.
						THROW
					ENDIF
					
					SUM totqty TO nCtnTot1 FOR  !units AND outdet.outshipid = outship.outshipid
					SELECT vmarcjacobswpp
					COUNT TO nCtnTot2 FOR vmarcjacobswpp.outshipid = outship.outshipid
					IF nCtnTot1<>nCtnTot2
						SET STEP ON 
						DO ediupdate WITH "OUTDET/SQL CTNS TOTQTY ERR",.T.
						THROW
					ENDIF
				ELSE  && Added to trap mismatches in PnP data, 01.08.2018, Joe
					SELECT outdet
					SET ORDER TO
					SUM totqty TO nUnitTot1 FOR units AND outdet.outshipid = outship.outshipid

					SELECT vmarcjacobswpp
					SUM totqty TO nUnitTot2 FOR vmarcjacobswpp.outshipid = outship.outshipid
					IF nUnitTot1<>nUnitTot2
						SET STEP ON 
						DO ediupdate WITH "OUTDET/SQL UNITS TOTQTY ERR",.T.
						THROW
					ENDIF
					
					*!* Check carton totals by outshipid
				ENDIF
				SELECT outdet
				SET ORDER TO outdetid
				LOCATE

*!* End code addition

				IF (("PENNEY"$UPPER(outship.consignee)) OR ("KMART"$UPPER(outship.consignee)) OR ("K-MART"$UPPER(outship.consignee)))
					lApptFlag = .T.
				ELSE
					lApptFlag = .F.
				ENDIF

				IF lTestinput OR lTesting
					ddel_date = DATE()
				ELSE
					ddel_date = outship.del_date
				ENDIF

				IF lTestinput
					dapptnum = "99999"
					dapptdate = DATE()
				ELSE
					dapptnum = ALLTRIM(outship.appt_num)

					IF EMPTY(dapptnum) && Penney/KMart Appt Number check
						IF (!(lApptFlag) OR (DATE()={^2007-08-20} AND cBOL = "04907314677036340"))
							dapptnum = ""
						ELSE
							DO ediupdate WITH "EMPTY APPT #",.T.
							THROW
						ENDIF
					ENDIF
					dapptdate = outship.appt

					IF EMPTY(dapptdate)
						dapptdate = IIF(lTesting,DATE(),outship.del_date)
					ENDIF
				ENDIF

				IF ALLTRIM(outship.SForCSZ) = ","
					BLANK FIELDS outship.SForCSZ NEXT 1 IN outship
				ENDIF

				cTRNum = ""
				cPRONum = ""

				IF (("WALMART"$outship.consignee) OR ("WAL-MART"$outship.consignee) OR ("WAL MART"$outship.consignee))
					IF LEFT(outship.keyrec,2) = "PR"
						STORE TRIM(keyrec) TO cPRONum
					ENDIF
					IF LEFT(outship.keyrec,2) = "TR"
						STORE TRIM(keyrec) TO cTRNum
					ENDIF
				ENDIF

				IF cBOL = "99911111111111111"
					cShip_ref = ALLTRIM(STRTRAN(cShip_ref,"OVER",""))
				ENDIF

				nOutshipid = m.outshipid
				cPTString = IIF(EMPTY(cPTString),m.consignee+" "+cShip_ref,cPTString+CHR(13)+m.consignee+" "+cShip_ref)

				cCountry = ""
				IF "CITY"$shipins AND "STATE"$shipins AND "ZIPCODE"$shipins AND "COUNTRY"$shipins
					cCity = segmentget(@apt,"CITY",alength)
					cState =segmentget(@apt,"STATE",alength)
					cZIP =segmentget(@apt,"ZIPCODE",alength)
					cCountry =segmentget(@apt,"COUNTRY",alength)
					cCountry = IIF(EMPTY(cCountry),"US",cCountry) && default value
				ELSE
					cCountry = IIF(EMPTY(cCountry),"US",cCountry) && default value
					m.CSZ = TRIM(m.CSZ)

					IF EMPTY(M.CSZ)
						WAIT CLEAR
						WAIT WINDOW "No SHIP-TO City/State/ZIP info...exiting" TIMEOUT 2
						DO ediupdate WITH "BAD ADDRESS INFO",.T.
						THROW
					ENDIF
					m.CSZ = ALLTRIM(STRTRAN(m.CSZ,"  "," "))
					IF !","$m.CSZ
						DO ediupdate WITH "NO COMMA CSZ-"+cShip_ref,.T.
						THROW
					ENDIF
					nSpaces = OCCURS(" ",ALLTRIM(m.CSZ))
					IF nSpaces = 0
						WAIT WINDOW "Ship-to CSZ Segment Error" TIMEOUT 3
						DO ediupdate WITH "Ship-to CSZ: "+cShip_ref,.T.
						THROW
					ELSE
						nCommaPos = AT(",",m.CSZ)
						nLastSpace = AT(" ",m.CSZ,nSpaces)
						IF ISALPHA(SUBSTR(TRIM(m.CSZ),AT(" ",m.CSZ,nSpaces-1)+1,2))
							cCity = LEFT(TRIM(m.CSZ),AT(",",m.CSZ)-1)
							cState = SUBSTR(m.CSZ,nCommaPos+2,2)
							cZIP = TRIM(SUBSTR(m.CSZ,nLastSpace+1))
						ELSE
							WAIT CLEAR
*						WAIT WINDOW "NOT ALPHA: "+SUBSTR(TRIM(m.CSZ),AT(" ",m.CSZ,nSpaces-1)+1,2) TIMEOUT 3
							cCity = LEFT(TRIM(m.CSZ),AT(",",m.CSZ)-1)
							cState = SUBSTR(TRIM(m.CSZ),AT(" ",m.CSZ,nSpaces-2)+1,2)
							cZIP = TRIM(SUBSTR(m.CSZ,nLastSpace+1))
						ENDIF
					ENDIF
				ENDIF

				STORE "" TO cSForCity,cSForState,cSForZip,cSForCountry
				IF !lFederated
					IF "SFORCITY"$shipins AND "SFORSTATE"$shipins AND "SFORZIPCODE"$shipins AND "SFORCOUNTRY"$shipins
						cSForCity = segmentget(@apt,"SFORCITY",alength)
						cSForState =segmentget(@apt,"SFORSTATE",alength)
						cSForZipCode =segmentget(@apt,"SFORZIPCODE",alength)
						cSForCountry =segmentget(@apt,"SFORCOUNTRY",alength)
						cSForCountry = IIF(EMPTY(cSForCountry),"US",cSForCountry) && default value
					ELSE
						cSForCountry = IIF(EMPTY(cSForCountry),"US",cSForCountry) && default value
						IF !EMPTY(M.SForCSZ)
							m.SForCSZ = ALLTRIM(STRTRAN(m.SForCSZ,"  "," "))
							nSpaces = OCCURS(" ",ALLTRIM(m.SForCSZ))
							IF nSpaces = 0
								m.SForCSZ = STRTRAN(m.SForCSZ,",","")
								cSForCity = ALLTRIM(m.SForCSZ)
								cSForState = ""
								cSForZip = ""
							ELSE
								nCommaPos = AT(",",m.SForCSZ)
								nLastSpace = AT(" ",m.SForCSZ,nSpaces)
								nMinusSpaces = IIF(nSpaces=1,0,1)
								IF ISALPHA(SUBSTR(TRIM(m.SForCSZ),AT(" ",TRIM(m.SForCSZ),nSpaces-nMinusSpaces)+1,2))
									cSForCity = LEFT(TRIM(m.SForCSZ),AT(",",m.SForCSZ)-1)
									cSForState = SUBSTR(m.SForCSZ,nCommaPos+2,2)
									cSForZip = ALLTRIM(SUBSTR(m.SForCSZ,nCommaPos+4))
								ELSE
									WAIT CLEAR
*									WAIT WINDOW "NOT ALPHA: "+SUBSTR(TRIM(m.SForCSZ),AT(" ",m.SForCSZ,nSpaces-1)+1,2) TIMEOUT 3
									cSForCity = LEFT(TRIM(m.SForCSZ),AT(",",m.SForCSZ)-1)
									cSForState = SUBSTR(TRIM(m.SForCSZ),AT(" ",m.SForCSZ,nSpaces-2)+1,2)
									cSForZip = ALLTRIM(SUBSTR(m.SForCSZ,nSpaces-1))
								ENDIF
							ENDIF
						ELSE
							cStoreName = segmentget(@apt,"STORENAME",alength)
						ENDIF
					ENDIF
				ENDIF

				DO num_incr_st
				WAIT CLEAR
				WAIT WINDOW "Now creating Line Item information" NOWAIT NOCLEAR

				IF DATETIME()>DATETIME(2016,11,29,20,00,00)  && This insert keeps
					INSERT INTO temp945amj (accountid,isa_num,st_num,wo_num,bol_no,ship_ref,filename,filedate) ;
						VALUES (m.accountid,c_CntrlNum,c_GrpCntrlNum,m.wo_num,m.bol_no,m.ship_ref,cFilenameShort,dt2)

					m.groupnum=c_CntrlNum
					m.isanum=PADL(c_CntrlNum,9,"0")
					m.transnum=PADL(c_GrpCntrlNum,9,"0")
					m.edicode="SW"
					m.accountid=m.accountid
					m.loaddt=DATE()
					m.loadtime=DATETIME()
					m.filename=cfilepath+JUSTFNAME(cFilenameShort)
					m.ship_ref=m.ship_ref
					m.senderid=csendid
					m.rcvid=Crcvid
					insertinto("ackdata","wh",.T.)
					tu("ackdata")
				ENDIF

				STORE "ST"+cfd+"945"+cfd+PADL(c_GrpCntrlNum,9,"0")+csegd TO cString
				DO cstringbreak
				nSTCount = nSTCount + 1
				nSegCtr = 1


				IF lJCP AND lUPSType
					STORE "W06"+cfd+"N"+cfd+cShip_ref+cfd+cdate+cfd+"0"+cfd+TRIM(cWO_Num)+cfd+cPO_Num+cfd+cShip_ref+csegd TO cString

					IF ALLTRIM(m.consignee)='AMAZON'  &&&& added 8/13/15  TM to send PRO num
						STORE "W06"+cfd+"N"+cfd+cShip_ref+cfd+cdate+cfd+"0"+cfd+TRIM(outship.keyrec)+cfd+cPO_Num+cfd+cShip_ref+csegd TO cString
					ELSE
					ENDIF


				ELSE
					STORE "W06"+cfd+"N"+cfd+cShip_ref+cfd+cdate+cfd+TRIM(cBOL)+cfd+TRIM(cWO_Num)+cfd+cPO_Num+cfd+cShip_ref+csegd TO cString

					IF ALLTRIM(m.consignee)='AMAZON'  &&&& added 8/13/15  TM to send PRO num
						STORE "W06"+cfd+"N"+cfd+cShip_ref+cfd+cdate+cfd+"0"+cfd+TRIM(outship.keyrec)+cfd+cPO_Num+cfd+cShip_ref+csegd TO cString
					ELSE
					ENDIF
				ENDIF
				DO cstringbreak
				nSegCtr = nSegCtr + 1
				nCtnNumber = 1  && Seed carton sequence count

				cStoreNum = ALLTRIM(m.dcnum)
				IF EMPTY(TRIM(cStoreNum))
					IF (!EMPTY(m.storenum) AND m.storenum<>0)
						IF LEN(ALLTRIM(STR(m.storenum))) < 5
							cStoreNum = PADL(ALLTRIM(STR(m.storenum)),4,"0")
						ELSE
							cStoreNum = ALLTRIM(STR(m.storenum))
						ENDIF
					ENDIF
					IF EMPTY(cStoreNum)
						cStoreNum = segmentget(@apt,"STORENUM",alength)
						IF EMPTY(cStoreNum)
							DO ediupdate WITH "MISS STORENUM",.T.
							THROW
						ENDIF
					ENDIF
				ENDIF

				STORE "N1"+cfd+"SF"+cfd+"FMI INC."+cfd+"91"+cfd+"01"+csegd TO cString
				DO cstringbreak
				nSegCtr = nSegCtr + 1

				IF !lFederated
					STORE "N3"+cfd+TRIM(cSF_Addr1)+csegd TO cString
					DO cstringbreak
					nSegCtr = nSegCtr + 1

					STORE "N4"+cfd+cSF_CSZ+cfd+"US"+csegd TO cString
					DO cstringbreak
					nSegCtr = nSegCtr + 1
				ENDIF

				STORE "N1"+cfd+"ST"+cfd+ALLTRIM(m.consignee)+cfd+"92"+cfd+cStoreNum+csegd TO cString
				DO cstringbreak
				nSegCtr = nSegCtr + 1

				STORE "N3"+cfd+TRIM(outship.address)+csegd TO cString
				DO cstringbreak
				nSegCtr = nSegCtr + 1

				STORE "N4"+cfd+cCity+cfd+cState+cfd+cZIP+cfd+cCountry+csegd TO cString
				DO cstringbreak
				nSegCtr = nSegCtr + 1

				IF !EMPTY(m.sforstore)
					STORE "N1"+cfd+"Z7"+cfd+ALLTRIM(m.shipfor)+cfd+"91"+cfd+TRIM(m.sforstore)+csegd TO cString
					DO cstringbreak
					nSegCtr = nSegCtr + 1

					STORE "N3"+cfd+TRIM(m.sforaddr1)+csegd TO cString
					DO cstringbreak
					nSegCtr = nSegCtr + 1

					IF !EMPTY(cSForState)
						STORE "N4"+cfd+cSForCity+cfd+cSForState+cfd+cSForZip+cfd+cSForCountry+csegd TO cString
					ELSE
						STORE "N4"+cfd+cSForCity+csegd TO cString
					ENDIF
					DO cstringbreak
					nSegCtr = nSegCtr + 1
				ENDIF

				STORE "N9"+cfd+"PGC"+cfd+"CTN25"+csegd TO cString
				DO cstringbreak
				nSegCtr = nSegCtr + 1

				STORE "N9"+cfd+"MB"+cfd+cMBOL+csegd TO cString
				DO cstringbreak
				nSegCtr = nSegCtr + 1

				IF lJCP
					STORE "N9"+cfd+"PUA"+cfd+dapptnum+csegd TO cString
					DO cstringbreak
					nSegCtr = nSegCtr + 1
				ENDIF

				IF !EMPTY(cPRONum)
					STORE "N9"+cfd+"RE"+cfd+cPRONum+csegd TO cString
					DO cstringbreak
					nSegCtr = nSegCtr + 1
				ENDIF

*!* For JCPenney orders only
				IF cacctnum = "JCP" OR ("PENNEY"$UPPER(m.consignee))
					IF lTesting AND EMPTY(m.batch_num)
						cLoadID = PADR(ALLTRIM(STR(nLoadid)),6,"0")
						nLoadid = nLoadid + 1
					ELSE
						cLoadID = ALLTRIM(m.batch_num)
						IF EMPTY(cLoadID)
							cLoadID = ALLTRIM(m.appt_num)
							IF EMPTY(cLoadID)
								DO ediupdate WITH "MISS LOADID",.T.
								THROW
							ENDIF
						ENDIF
					ENDIF
					STORE "N9"+cfd+"P8"+cfd+TRIM(cLoadID)+csegd TO cString
					DO cstringbreak
					nSegCtr = nSegCtr + 1
				ENDIF

				IF "AMAZON"$outship.consignee
					STORE "N9"+cfd+"AO"+cfd+dapptnum+csegd TO cString
					DO cstringbreak
					nSegCtr = nSegCtr + 1
				ENDIF

				STORE "G62"+cfd+"10"+cfd+TRIM(DTOS(ddel_date))+cfd+"A"+cfd+cDelTime+csegd TO cString  && Ship date/time
				DO cstringbreak
				nSegCtr = nSegCtr + 1

				SELECT scacs
				IF lTesting AND (EMPTY(m.scac) OR EMPTY(m.ship_via))
					STORE "TEST" TO cSCAC
					STORE "TEST SHIPPER" TO m.ship_via
				ENDIF

				IF !EMPTY(cTRNum)
					STORE "W27"+cfd+cCarrierType+cfd+TRIM(cSCAC)+cfd+TRIM(m.ship_via)+REPLICATE(cfd,2)+"TL"+;
						REPLICATE(cfd,2)+cTRNum+csegd TO cString
				ELSE
					STORE "W27"+cfd+cCarrierType+cfd+TRIM(cSCAC)+cfd+TRIM(m.ship_via)+csegd TO cString
				ENDIF
				DO cstringbreak
				nSegCtr = nSegCtr + 1

*************************************************************************
*2	DETAIL LOOP - MATCHING SHIP_REF FIELD (FROM OUTSHIP)
*************************************************************************
				WAIT CLEAR
				WAIT WINDOW "Now creating Detail information "+cShip_ref NOWAIT NOCLEAR
				SELECT vmarcjacobswpp
				SELECT outdet
				SET ORDER TO TAG outdetid
				SELECT vmarcjacobswpp
				SET RELATION TO outdetid INTO outdet
				STORE 0 TO nShipCtnTotQty,nShipDetTotQty

				LOCATE
				LOCATE FOR ship_ref = TRIM(cShip_ref) AND outshipid = nOutshipid
				IF !FOUND()
					IF DATETIME()<DATETIME(2016,10,14,19,15,00)
						LOOP
					ENDIF
					IF !lTesting
						WAIT WINDOW "PT "+cShip_ref+" NOT FOUND in vmarcjacobswpp...ABORTING" TIMEOUT 2
						IF !lTesting
							lSQLMail = .T.
						ENDIF
						SET STEP ON
						DO ediupdate WITH "MISS PT-SQL: "+cShip_ref,.T.
						THROW
					ELSE
						LOOP
					ENDIF
				ENDIF

*ASSERT .F. MESSAGE "At outdet line scan"
				scanstr = "vmarcjacobswpp.ship_ref = cShip_ref and vmarcjacobswpp.outshipid = nOutshipid"

				SELECT vmarcjacobswpp
				LOCATE
				IF lTesting
*					BROWSE
				ENDIF
				LOCATE FOR &scanstr
				cCartonNum= "XXX"

				lDoManSegment = .T.
				lDoPALSegment = .T.
				DO WHILE &scanstr

					alength = ALINES(apt,outdet.printstuff,.T.,CHR(13))
					lSkipBack = .T.

					IF TRIM(vmarcjacobswpp.ucc) <> cCartonNum
						STORE TRIM(vmarcjacobswpp.ucc) TO cCartonNum
					ENDIF

					DO WHILE vmarcjacobswpp.ucc = cCartonNum
						IF vmarcjacobswpp.totqty = 0 OR vmarcjacobswpp.ucc = "CUTS"
							IF !EOF()
								SKIP 1 IN vmarcjacobswpp
								LOOP
							ELSE
								EXIT
							ENDIF
						ENDIF

						IF lDoManSegment
							STORE "LX"+cfd+ALLTRIM(STR(nCtnNumber))+csegd TO cString   && Carton Seq. # (up to Carton total)
*!*								lDoManSegment = .T.
*!*								lDoPALSegment = .T.
							DO cstringbreak
							nSegCtr = nSegCtr + 1
						ENDIF

						cUCCNumber = vmarcjacobswpp.ucc
						IF EMPTY(cUCCNumber) OR ISNULL(cUCCNumber)
							WAIT CLEAR
							WAIT WINDOW "Empty UCC Number in vmarcjacobswpp "+cShip_ref TIMEOUT 2
							lSQLMail = .T.
							DO ediupdate WITH "EMPTY UCC# in "+cShip_ref,.T.
							THROW
						ENDIF
						cUCCNumber = TRIM(STRTRAN(TRIM(cUCCNumber)," ",""))

						IF lDoManSegment
							lDoManSegment = .F.
							STORE "MAN"+cfd+"GM"+cfd+TRIM(cUCCNumber)+csegd TO cString  && SCC-14 Number (Wal-mart spec)
							DO cstringbreak
							nSegCtr = nSegCtr + 1
							nShipCtnQty = INT(VAL(outdet.PACK))
							nShipCtnTotQty = nShipCtnTotQty+1
							cCtnWt= "0.00" && init the weight value
							IF (EMPTY(outdet.ctnwt) OR outdet.ctnwt = 0) AND outdet.totqty > 0
								cCtnWt = ALLTRIM(STR(INT(CEILING(outship.weight)/outship.ctnqty)))
								IF EMPTY(cCtnWt) OR INT(VAL(cCtnWt)) < 1
									cCtnWt = '1'
								ENDIF
								nTotCtnWt = nTotCtnWt + INT(VAL(cCtnWt))
							ELSE
								STORE ALLTRIM(STR(outdet.ctnwt)) TO cCtnWt
								nTotCtnWt = nTotCtnWt + outdet.ctnwt
							ENDIF

						ENDIF

						cColor = TRIM(outdet.COLOR)
						cSize = TRIM(outdet.ID)
						cUPC = TRIM(outdet.upc)

						IF (ISNULL(cUPC) OR EMPTY(cUPC))
							cUPC = TRIM(vmarcjacobswpp.upc)
						ENDIF

						cStyle = TRIM(outdet.STYLE)
						cItemNum = TRIM(outdet.custsku)

						nShipDetQty = vmarcjacobswpp.totqty
						nShipDetTotQty = nShipDetTotQty+nShipDetQty
						IF ISNULL(nShipDetQty)
							nShipDetQty = outdet.totqty
							nShipDetTotQty = nShipDetTotQty+nShipDetQty
						ENDIF

						nOrigDetQty = vmarcjacobswpp.qty
						IF ISNULL(nOrigDetQty)
							nOrigDetQty = nShipDetQty
						ENDIF

						IF lDoPALSegment
							STORE "PAL"+REPLICATE(cfd,11)+cCtnWt+cfd+cWeightUnit+csegd TO cString
							DO cstringbreak
							nSegCtr = nSegCtr + 1
							lDoPALSegment = .F.
						ENDIF

						IF lUPSType AND lMacy
							SELECT tracknum
							cTrkNumber = ALLT(tracknum.trknumber)
							SKIP 1 IN tracknum
							STORE "N9"+cfd+"BM"+cfd+cTrkNumber+csegd TO cString
							DO cstringbreak
							nSegCtr = nSegCtr + 1
						ENDIF

*!* Changed the following to utilize original 940 unit codes from Printstuff field
						cUnitCode = TRIM(segmentget(@apt,"UNITCODE",alength))
						IF EMPTY(cUnitCode)
							cUnitCode = "EA"
						ENDIF

						cShipStat = "SH"
						STORE "W12"+cfd+cShipStat+cfd+ALLTRIM(STR(nOrigDetQty))+cfd+ALLTRIM(STR(nShipDetQty))+cfd+;
							cfd+cUnitCode+cfd+cfd+"UP"+cfd+cUPC+REPLICATE(cfd,13)+"VN"+cfd+cStyle+csegd TO cString
						DO cstringbreak
						nSegCtr = nSegCtr + 1

						IF !EMPTY(cColor)
							STORE "N9"+cfd+"VC"+cfd+cColor+csegd TO cString
							DO cstringbreak
							nSegCtr = nSegCtr + 1
						ENDIF

						IF !EMPTY(cSize)
							STORE "N9"+cfd+"SZ"+cfd+cSize+csegd TO cString
							DO cstringbreak
							nSegCtr = nSegCtr + 1
						ENDIF

						SKIP 1 IN vmarcjacobswpp
					ENDDO
					lDoManSegment = .T.
					lDoPALSegment = .T.
					nCtnNumber = nCtnNumber + 1
					lSkipBack = .T.
				ENDDO

*************************************************************************
*2^	END DETAIL MATCHING SHIP_REF LOOP
*************************************************************************
				WAIT CLEAR
				WAIT WINDOW "Now creating Section Closure information" NOWAIT NOCLEAR
				STORE "W03"+cfd+ALLTRIM(STR(nShipDetTotQty))+cfd+ALLTRIM(STR(nTotCtnWt))+cfd+;
					cWeightUnit+cfd+cfd+cfd+ALLTRIM(STR(nShipCtnTotQty))+cfd+"CT"+csegd TO cString   && Units sum, Weight sum, carton count

				nTotCtnWt = 0
				nTotCtnCount = 0
				FWRITE(nFilenum,cString)

				STORE  "SE"+cfd+ALLTRIM(STR(nSegCtr+2))+cfd+PADL(c_GrpCntrlNum,9,"0")+csegd TO cString
				FWRITE(nFilenum,cString)

				SELECT outship
				WAIT CLEAR
			ENDSCAN
			SELECT bldet
		ENDSCAN

		WAIT WINDOW "" TIMEOUT 1
		ASSERT .F. MESSAGE "At end of loop: Sub-BOL "+cBOL


*************************************************************************
*1^	END OUTSHIP MAIN LOOP
*************************************************************************
		close945()
		=FCLOSE(nFilenum)

		IF !lTesting
			INSERT INTO F:\WH\MJFILES (TYPE,voucher,pickticket,bol,filename,WHEN) VALUES ("MBOL-945","WHOLESALE","",cBOL,JUSTFNAME(cFilenameHold),DATETIME())
			IF !USED("edi_trigger")
				USE F:\3pl\DATA\edi_trigger SHARED IN 0
			ENDIF
			SELECT edi_trigger
			DO ediupdate WITH "945 CREATED",.F.
			SELECT edi_trigger
			LOCATE

			IF !USED("ftpedilog")
				SELECT 0
				USE F:\edirouting\ftpedilog ALIAS ftpedilog
				INSERT INTO ftpedilog (TRANSFER,ftpdate,filename,acct_name,TYPE) VALUES ("945-"+ccustname+"-"+cCustLoc,dt2,JUSTFNAME(cFilenameHold),UPPER(ccustname),"945")
				USE IN ftpedilog
			ENDIF
		ENDIF

		WAIT CLEAR

*!* Transfers files to correct output folders
		ASSERT .F. MESSAGE "At FilesOut Statement...debug"
		COPY FILE [&cFilenameHold] TO [&cFilenameArch]
		SET STEP ON 
		IF !lTesting
			WAIT WINDOW "" TIMEOUT 1
			SELECT temp945amj
			COPY TO "f:\3pl\data\temp945mj.dbf"
			USE IN temp945amj
			SELECT 0
			USE "f:\3pl\data\temp945mj.dbf" ALIAS temp945mj
			SELECT 0
			USE "f:\3pl\data\pts_sent945.dbf" ALIAS pts_sent945
			APPEND FROM "f:\3pl\data\temp945mj.dbf"
			USE IN pts_sent945
			USE IN temp945mj
			DELETE FILE "f:\3pl\data\temp945mj.dbf"
		ENDIF

		cPTCount = ALLTRIM(STR(nPTCount))
		cTotSubs = ALLTRIM(STR(nTotSubs))
		cSubCnt = ALLTRIM(STR(nSubCnt))

		INSERT INTO tempinfo (subbol,ptcount,ptstring) VALUES (cBOL,cPTCount,cPTString)

		WAIT CLEAR
		WAIT WINDOW cMailName+" 945 EDI File output complete for"+CHR(13)+"Sub-BOL: "+cBOL NOWAIT
		cPTString = ""
		nPTCount = 0
		ASSERT .F. MESSAGE "At BL file in end of scan loop"
		SELECT bl
		IF !lTesting
*		asn_out_data()
		ENDIF
		cWO_NumList = ""

	ENDSCAN

*!* Create eMail confirmation message
	IF lTestMail
		tsendto = tsendtotest
		tcc = tcctest
	ENDIF
	tsubject = cMailName+" 945 EDI FILE (MBOL) from TGF as of "+dtmail+" ("+cCustLoc+")"
	tattach = " "
	tmessage = "945 EDI Info from TGF, file "+cFilenameShort+CHR(13)
	tmessage = tmessage + "Division: "+cDivision+", Master BOL# "+TRIM(cMBOL)+CHR(13)
	tmessage = tmessage + "For Work Orders: "+cWO_NumStr+","+CHR(13)
	tmessage = tmessage +"has been created and will be transmitted ASAP."+CHR(13)
	tmessage = tmessage + "If you have any questions, please eMail or call 732-750-9000 x217."+CHR(13)+CHR(13)
	tmessage = tmessage + "Below is a list of "+cSubCnt+" sub-BOLs and counts, with orders contained."+CHR(13)
	SELECT tempinfo
	IF lTesting
		COPY TO c:\tempfox\tempinfo
	ENDIF
	SCAN
		tmessage = tmessage+CHR(13)+PADR(ALLTRIM(tempinfo.subbol),22)+tempinfo.ptcount+CHR(13)
		tmessage = tmessage+ALLTRIM(tempinfo.ptstring)+CHR(13)
	ENDSCAN

	IF !EMPTY(cMissDel)
		tmessage = tmessage+CHR(13)+CHR(13)+cMissDel+CHR(13)+CHR(13)
	ENDIF
	IF lTesting OR lTestinput
		tmessage = tmessage+CHR(13)+ "This is a TEST MBOL 945"
	ENDIF

	IF lEmail
		DO FORM m:\dev\frm\dartmail2 WITH tsendto,tfrom,tsubject,tcc,tattach,tmessage,"A"
	ENDIF

	SET STEP ON
	IF lDoMJFilesOut
		CD &lcHoldPath
		COPY FILE (lcHoldPath+"*.*") TO (lcPath+"*.*")
		DELETE FILE(lcHoldPath+"*.*")
	ENDIF

	IF !lTesting AND !lTestinput
		SELECT edi_trigger
		DO ediupdate WITH "945 CREATED",.F.
		SELECT edi_trigger
		LOCATE
	ENDIF
	RELEASE ALL LIKE c_CntrlNum,c_GrpCntrlNum
	WAIT CLEAR
	WAIT WINDOW cMailName+" 945 EDI File output complete" TIMEOUT 1
	NormalExit = .T.


CATCH TO oErr
	IF !NormalExit
		ASSERT .F. MESSAGE "In Error CATCH"
		SET STEP ON
		IF lTesting
			tsendto  = tsendtotest
			tcc = tcctest
		ELSE
			tsendto  = tsendtoerr
			tcc = tccerr
		ENDIF

		tmessage = ccustname+" Error processing "+CHR(13)

		IF !lUpdated
			SET STEP ON
			DO ediupdate WITH "ERRHAND ERROR",.T.
			tsubject = ccustname+" Error ("+TRANSFORM(oErr.ERRORNO)+") at "+TTOC(DATETIME())
			tattach  = ""


			tmessage =tmessage+ "Try/CATCH Exeception Message:"+CHR(13)+;
				[  Error: ] + STR(oErr.ERRORNO) +CHR(13)+;
				[  LineNo: ] + STR(oErr.LINENO) +CHR(13)+;
				[  Message: ] + oErr.MESSAGE +CHR(13)+;
				[  Procedure: ] + oErr.PROCEDURE +CHR(13)+;
				[  Details: ] + oErr.DETAILS +CHR(13)+;
				[  StackLevel: ] + STR(oErr.STACKLEVEL) +CHR(13)+;
				[  LineContents: ] + oErr.LINECONTENTS+CHR(13)+;
				[  UserValue: ] + oErr.USERVALUE
		ENDIF
		tmessage =tmessage+CHR(13)+cProgname
		tmessage =tmessage+CHR(13)+"BOL "+cBOL
		tmessage =tmessage+CHR(13)+CHR(13)+cErrMsg

		tsubject = cMailName+" 945 EDI Error at "+TTOC(DATETIME())
		tattach  = ""
		tfrom    ="TGF EDI Operations <fmi-edi-ops@fmiint.com>"
		DO FORM m:\dev\frm\dartmail2 WITH tsendto,tfrom,tsubject,tcc,tattach,tmessage,"A"
	ENDIF
FINALLY
	ON ERROR
	closedata()
ENDTRY

*** END OF CODE BODY

****************************
PROCEDURE close945
****************************
** Footer Creation

	STORE  "GE"+cfd+ALLTRIM(STR(nSTCount))+cfd+c_CntrlNum+csegd TO cString
	FWRITE(nFilenum,cString)

	STORE  "IEA"+cfd+"1"+cfd+PADL(c_CntrlNum,9,"0")+csegd TO cString
	FWRITE(nFilenum,cString)

ENDPROC

****************************
PROCEDURE num_incr_isa
****************************

	IF !USED("serfile")
		USE ("f:\3pl\data\serial\"+ccustname+"945_serial") IN 0 ALIAS serfile
	ENDIF
	SELECT serfile
	nISA_Num = serfile.seqnum
	c_CntrlNum = ALLTRIM(STR(serfile.seqnum))
	REPLACE serfile.seqnum WITH serfile.seqnum + 1 IN serfile
	SELECT outship
ENDPROC

****************************
PROCEDURE num_incr_st
****************************

	IF !USED("serfile")
		USE ("f:\3pl\data\serial\"+ccustname+""945_serial") IN 0 ALIAS serfile
	ENDIF
	SELECT serfile
	IF lTesting AND lSTFlag
		nOrigGrpSeq = serfile.grpseqnum
		lSTFlag = .F.
	ENDIF
	c_GrpCntrlNum = ALLTRIM(STR(serfile.grpseqnum))
	REPLACE serfile.grpseqnum WITH serfile.grpseqnum + 1 IN serfile
	SELECT outship
ENDPROC

****************************
PROCEDURE segmentget
****************************

	PARAMETER thisarray,lcKey,nLength

	FOR i = 1 TO nLength
		IF i > nLength
			EXIT
		ENDIF
		lnEnd= AT("*",thisarray[i])
		IF lnEnd > 0
			lcThisKey =TRIM(SUBSTR(thisarray[i],1,lnEnd-1))
			IF OCCURS(lcKey,lcThisKey)>0
				RETURN SUBSTR(thisarray[i],lnEnd+1)
				i = 1
			ENDIF
		ENDIF
	ENDFOR

	RETURN ""

****************************
PROCEDURE ediupdate
****************************
	PARAMETER cStatus,lIsError
	lUpdated = .T.
	cErrMsg = cStatus

	IF !lTesting
		SELECT edi_trigger
		nRec = RECNO()
		IF !lIsError  && Changed EDI_TRIGGER file945 population routine to insert sub-BOL files, 05.03.2012
			NormalExit = .T.
			SELECT edipop
			SCAN
				SELECT edi_trigger
				LOCATE FOR edi_trigger.accountid = nAcctNum ;
					AND edi_trigger.bol = cMBOL ;
					AND edi_trigger.ship_ref = edipop.ship_ref
				IF !FOUND()
					IF DATETIME()<DATETIME(2016,10,14,19,15,00)
						LOOP
					ENDIF
					SET STEP ON
					cErrMsg = "Problem with EDI Population at PT# "+TRIM(edipop.ship_ref)
					REPLACE edi_trigger.processed WITH .T.,edi_trigger.proc945 WITH .F.,edi_trigger.file945 WITH "",;
						edi_trigger.fin_status WITH cErrMsg,edi_trigger.errorflag WITH .T. ;
						FOR edi_trigger.bol = cMBOL AND edi_trigger.accountid = nAcctNum AND edi_type = "945 ";
						IN edi_trigger
					IF lCloseOutput
						=FCLOSE(nFilenum)
						ERASE &cFilenameHold
					ENDIF
					NormalExit = .F.
					THROW
				ELSE
					bolst = "SUBBOL*"+edipop.bol
					REPLACE edi_trigger.processed WITH .T.,edi_trigger.proc945 WITH .T.,edi_trigger.file945 WITH ALLTRIM(edipop.filename),;
						edi_trigger.isa_num WITH cISA_Num,edi_trigger.comments WITH IIF(EMPTY(edi_trigger.comments),bolst,edi_trigger.comments+CHR(13)+bolst),;
						edi_trigger.fin_status WITH "945 CREATED",edi_trigger.errorflag WITH .F.,edi_trigger.when_proc WITH DATETIME() ;
						NEXT 1 IN edi_trigger
					RELEASE bolst
				ENDIF
			ENDSCAN
		ELSE
			REPLACE edi_trigger.processed WITH .T.,edi_trigger.proc945 WITH .F.,edi_trigger.file945 WITH "",;
				edi_trigger.fin_status WITH cStatus,edi_trigger.errorflag WITH .T. ;
				FOR edi_trigger.bol = cMBOL AND edi_trigger.accountid = nAcctNum AND edi_type = "945 "
			IF lCloseOutput
				=FCLOSE(nFilenum)
				ERASE &cFilenameHold
			ENDIF
		ENDIF
	ENDIF

	IF lIsError AND lEmail AND cStatus<>"SQL ERROR"
		SET STEP ON
		tsubject = "945 Error in "+cMailName+" BOL "+TRIM(cBOL)+"(At PT "+cShip_ref+")"
		tattach = " "
		tsendto = tsendtoerr
		tcc = IIF(cStatus = "EMPTY DC/STORE#",tccwhse,tccerr)
		tmessage = "945 Processing for MBOL# "+cMBOL+CHR(13)+"SUBBOL# "+cBOL+", WO# "+cWO_Num+" produced this error: (Office: "+cOffice+"): "+cStatus+CHR(13)+"Check EDI_TRIGGER and re-run"
		IF "TOTQTY ERR"$cStatus
			tmessage = tmessage + CHR(13) + "At OUTSHIPID: "+ALLTRIM(STR(m.outshipid))
		ENDIF
		DO FORM m:\dev\frm\dartmail2 WITH tsendto,tfrom,tsubject,tcc,tattach,tmessage,"A"
		NormalExit = .T.
	ENDIF

*	CLOSEDATA()

	IF !lTesting
		SELECT edi_trigger
		LOCATE
	ENDIF
ENDPROC

****************************
PROCEDURE cstringbreak
****************************
	cLen = LEN(ALLTRIM(cString))
	FWRITE(nFilenum,cString)
ENDPROC

*********************
PROCEDURE headerfill
*********************
*!* HEADER LEVEL EDI DATA

	cISA_Num = PADL(c_CntrlNum,9,"0")
	nISA_Num = INT(VAL(cISA_Num))

*!*		IF lTesting
*!*			cISACode = "T"
*!*		ELSE
	cISACode = "P"
*!*		ENDIF

	STORE "ISA"+cfd+"00"+cfd+"          "+cfd+"00"+cfd+"          "+cfd+csendqual+cfd+csendidlong+cfd+;
		crecqual+cfd+crecidlong+cfd+cTruncDate+cfd+cTruncTime+cfd+"U"+cfd+"00401"+cfd+;
		cISA_Num+cfd+"0"+cfd+cISACode+cfd+cterminator+csegd TO cString
	DO cstringbreak

	STORE "GS"+cfd+"SW"+cfd+csendid+cfd+crecid+cfd+cdate+cfd+cTruncTime+cfd+c_CntrlNum+;
		cfd+"X"+cfd+cX12+csegd TO cString
	DO cstringbreak

ENDPROC

*********************
PROCEDURE closedata
*********************
	IF USED('outship')
		USE IN outship
	ENDIF
	IF USED('outdet')
		USE IN outdet
	ENDIF
	IF USED('outwolog')
		USE IN outwolog
	ENDIF
	IF USED('bl')
		USE IN bl
	ENDIF
	IF USED('bldet')
		USE IN bldet
	ENDIF
	IF USED('serfile')
		USE IN serfile
	ENDIF
	IF USED('scacs')
		USE IN scacs
	ENDIF
	IF USED('edipop')
		USE IN edipop
	ENDIF
	IF USED('tempinfo')
		USE IN tempinfo
	ENDIF
	IF USED('mm')
		USE IN mm
	ENDIF
	IF USED('tempx')
		USE IN tempx
	ENDIF

ENDPROC