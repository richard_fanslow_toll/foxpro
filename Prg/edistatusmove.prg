PARAMETERS cBOLIn

*!* An auxiliary program to move certain current data into the edi_trigger.comments field prior to programatically closing a 945 in edi_trigger.

lTesting = .f.

IF lTesting
	CLOSE DATA ALL
	cBOL = '386761470008886'
	SELECT 0
	USE c:\tempfox\temp\edi_trigger
*	BLANK FIELDS comments FOR bol=cBOL
ENDIF

SELECT edi_trigger
SCAN FOR edi_trigger.bol = cBOL
	STORE "" TO cComments,cCommentsHdr,cTrigInfo,cProcInfo,cPrevStatus
	cCommentsHdr = IIF(EMPTY(ALLT(edi_trigger.comments)),"",edi_trigger.comments+CHR(13))
	cComments = cCommentsHdr
	cTrigInfo = "PREV. TRIG: "+TTOC(edi_trigger.trig_time)
	IF !cTrigInfo$edi_trigger.comments
		cComments = cComments+cTrigInfo+CHR(13)
	ENDIF
	cProcInfo = "PREV. PROC: "+TTOC(edi_trigger.when_proc)
	IF !	cProcInfo$edi_trigger.comments
		cComments = cComments+"PREV. PROC: "+IIF(EMPTY(edi_trigger.when_proc),"UNPROCESSED",TTOC(when_proc))+CHR(13)
	ENDIF
	cPrevStatus = "PREV. STATUS: "+ALLT(edi_trigger.fin_status)
	cComments = cComments+cPrevStatus+CHR(13)
	REPLACE edi_trigger.comments WITH cComments IN edi_trigger
	RELEASE cComments,cCommentsHdr,cTrigInfo,cProcInfo,cPrevStatus
ENDSCAN

IF lTesting
	BROWSE FIELDS bol,ship_ref,consignee,comments FOR edi_trigger.bol = cBOL
ENDIF
