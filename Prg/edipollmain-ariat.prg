SET STEP on
runack("EDIPOLLER_ARIAT")

CLOSE DATABASES ALL
CLEAR ALL
RELEASE ALL
cPath = ["M:\DEV\PRG; M:\DEV\PROJ"]
SET PATH TO &cPath ADDITIVE

WITH _SCREEN
	.AUTOCENTER = .T.
	.WINDOWSTATE = 0
	.BORDERSTYLE = 1
	.WIDTH = 260
	.HEIGHT = 210
	.TOP = 290
	.LEFT = 380
	.CLOSABLE = .F.
	.MAXBUTTON = .F.
	.MINBUTTON = .F.
	.SCROLLBARS = 0
	.CAPTION = "ARIAT POLLER - 945 ONLY"
ENDWITH
TRY
	DO m:\dev\prg\_setvars
	PUBLIC lSkipError
	lSkipError =  .F.
	SET STATUS BAR OFF
	SET SYSMENU OFF
	ON ESCAPE CANCEL
	ON ERROR debug
	SELECT 0
	USE F:\3pl\DATA\edipollertrip SHARED ALIAS ear1
	REPLACE ear1.ariat WITH .F. IN ear1
	USE IN ear1

	SELECT 0
	USE F:\3pl\DATA\last945time SHARED ALIAS ariatlast
	REPLACE checkflag WITH .T. FOR poller = "ARIAT" IN ariatlast
	REPLACE tries WITH 0 FOR poller = "ARIAT" IN ariatlast
	USE IN ariatlast
	ASSERT .F. MESSAGE "Going into poller run"
	DO FORM m:\dev\frm\edi_jobhandler_ariat

CATCH TO oErr
	ASSERT .F. MESSAGE "AT CATCH SECTION"
	IF !lSkipError
		tfrom    ="TGF EDI Processing Center <tgf-edi-ops@fmiint.com>"
		tsubject = "Ariat EDI Poller Error ("+TRANSFORM(oErr.ERRORNO)+") at "+TTOC(DATETIME())
		tattach  = ""
		SELECT 0
		USE F:\3pl\DATA\mailmaster ALIAS mm
		LOCATE
		LOCATE FOR mm.edi_type = 'MISC' AND mm.taskname = "GENERAL"
		tsendto = IIF(mm.use_alt,sendtoalt,sendto)
		tcc = IIF(mm.use_alt,ccalt,cc)
		USE IN mm

		tmessage = "Ariat Outbound 945 Poller Major Error..... Please fix me........!"
		lcSourceMachine = SYS(0)

		tmessage =tmessage+ "Try/CATCH Exeception Message:"+CHR(13)+;
			[  Error: ] + STR(oErr.ERRORNO) +CHR(13)+;
			[  LineNo: ] + STR(oErr.LINENO) +CHR(13)+;
			[  Message: ] + oErr.MESSAGE +CHR(13)+;
			[  Procedure: ] + oErr.PROCEDURE +CHR(13)+;
			[  Details: ] + oErr.DETAILS +CHR(13)+;
			[  StackLevel: ] + STR(oErr.STACKLEVEL) +CHR(13)+;
			[  LineContents: ] + oErr.LINECONTENTS+CHR(13)+;
			[  UserValue: ] + oErr.USERVALUE+CHR(13)+;
			[  Computer:  ] +lcSourceMachine+CHR(13)
		DO FORM m:\dev\FRM\dartmail2 WITH tsendto,tfrom,tsubject,tcc,tattach,tmessage,"A"
	ENDIF
	SELECT 0
	USE F:\3pl\DATA\edipollertrip SHARED ALIAS ear1
	REPLACE ear1.ariat WITH .T.
	CLOSE DATABASES ALL
FINALLY
	SET STATUS BAR ON
	ON ERROR
	SET LIBRARY TO 
ENDTRY
