* Populate the Insperity punch IMPORT table with punches for Drivers - for Darren & Mike DiV.

* EXEs = F:\UTIL\QUALCOMM\QCPUNCH_IMPORT.EXE

*** note: TIMESTAR_FTP_PUNCHES.PRG takes the punches this program retrieves and FTPs them to Insperity.

** modified to support SQL FX POS table 11/09/2015 MB
*
* 4/25/2018 MB: changed fmiint.com email to my Toll email.
*

runack("QCPUNCH_IMPORT")

LOCAL lnError, lcProcessName, loQCPUNCH_IMPORT

DECLARE INTEGER CreateMutex IN Win32API ;
	STRING @lpMutexAttributes, ;
	STRING bInitialOwner, ;
	STRING @lpName

DECLARE INTEGER GetLastError IN Win32API

* if running in EXE check to see if already running
IF _VFP.STARTMODE > 0 THEN
	lcProcessName = "QCPUNCH_IMPORT"
	CreateMutex(0,CHR(1),@lcProcessName)
	lnError = GetLastError()
	IF lnError = 183
		WAIT WINDOW TIMEOUT 10 "Process is already running..."
		RETURN .F.
	ENDIF
ENDIF

utilsetup("QCPUNCH_IMPORT")

loQCPUNCH_IMPORT = CREATEOBJECT('QCPUNCH_IMPORT')
loQCPUNCH_IMPORT.MAIN()

schedupdate()

CLOSE DATABASES ALL
RETURN

#DEFINE LOGIT 1
#DEFINE WAITIT 2
#DEFINE NOWAITIT 4
#DEFINE SENDIT 8
#DEFINE CRLF CHR(13) + CHR(10)
#DEFINE LF CHR(10)
#DEFINE RETURN_DATA_MANDATORY .F.
#DEFINE RETURN_DATA_NOT_MANDATORY .T.

DEFINE CLASS QCPUNCH_IMPORT AS CUSTOM

	cProcessName = 'QCPUNCH_IMPORT'

	lTestMode = .F.  && .F. FOR NORMAL OPERATION, .T. TO SUPPRESS ALL BUT DEVELOPER EMAILS AND CHANGE INPUT TABLE, ETC.

	lAutoYield = .T.
	cStartTime = TTOC(DATETIME())

	* connection properties
	nSQLHandle = 0

	* file properties
	nFileHandle = 0

	cInputTable = 'F:\EXPRESS\FXDATA\POS'
	cDriverPunchTable = 'F:\UTIL\QUALCOMM\DATA\DRPUNCHES'

	* date properties
	dtNow = DATETIME()

	dToday = DATE()

	* procesing props
	lUseSQLtables = .T.

	* wait window properties
	nWaitWindowTimeout = 2
	lWaitWindowIsOn = .T.

	* logfile properties
	lLoggingIsOn = .T.
	cLogFile = 'F:\UTIL\QUALCOMM\Logfiles\QCPUNCH_IMPORT_log.txt'

	* INTERNAL email properties
	lSendInternalEmailIsOn = .T.
	cFrom = 'mark.bennett@tollgroup.com'
	cSendTo = 'mark.bennett@tollgroup.com'
	cCC = ''
	cSubject = 'Qualcomm Driver Punch Import for: ' + DTOC(DATE())
	cAttach = ''
	cBodyText = ''

	cCOMPUTERNAME = ''
	cUSERNAME = ''

	FUNCTION INIT
		IF NOT DODEFAULT()
			RETURN .F.
		ENDIF
		WITH THIS
			CLEAR
			*SET RESOURCE OFF
			CLOSE DATA
			SET CENTURY ON
			SET DATE AMERICAN
			SET HOURS TO 24
			SET ANSI ON
			SET TALK OFF
			SET DELETED ON
			SET CONSOLE OFF
			SET EXCLUSIVE OFF
			SET SAFETY OFF
			SET EXACT OFF
			SET STATUS BAR ON
			SET SYSMENU OFF
			SET ENGINEBEHAVIOR 70
			_VFP.AUTOYIELD = .lAutoYield
			.cCOMPUTERNAME = GETENV("COMPUTERNAME")
			.cUSERNAME = GETENV("USERNAME")
			IF .lTestMode THEN
				.cSendTo = 'mark.bennett@tollgroup.com'
				.cLogFile = 'F:\UTIL\QUALCOMM\Logfiles\QCPUNCH_IMPORT_log_TESTMODE.txt'
				.cDriverPunchTable = 'F:\UTIL\QUALCOMM\TESTDATA\TDRPUNCHES'
			ENDIF
			.lLoggingIsOn = .lLoggingIsOn AND NOT EMPTY(.cLogFile)
			IF .lLoggingIsOn THEN
				SET ALTERNATE TO (.cLogFile) ADDITIVE
				SET ALTERNATE ON
			ENDIF
		ENDWITH
	ENDFUNC


	FUNCTION DESTROY
		WITH THIS
			IF .lLoggingIsOn  THEN
				SET ALTERNATE OFF
				SET ALTERNATE TO
			ENDIF
		ENDWITH
		DODEFAULT()
	ENDFUNC


	FUNCTION MAIN
		WITH THIS
			LOCAL lnNumberOfErrors, lcSQL, lcSQL2, lcSQL3, lcSQL4, loError, llValid, ldToday, ldStartDate, ldPunchDate, ldPunchDateTime
			LOCAL lcXLFileName, lcFileDate, lnRetCode, lcEditDate, lcPunchDate, lcPunchTime, lcEmp_Num, lcPunchtype
			LOCAL lcSendTo, lcCC, lcBodyText, ltPunchDateTime, ldProcDate, lcProcDate, lnDuplicates, lnInserts
			LOCAL lcPunchDateTime, lnSpacePos, lcSearch, llStandardTime, llEastern, llValidEmp_Num, lcSQLStartDate

			TRY

				*** Methodology: scan through POS table for not empty(KPEMP) AND (NOT KPSENT)
				*** AND KPDATETIME > 4 DAYS AGO
				*** those recs should be new punches from Qualcomm that need to go to Insperity.
				***
				*** But we will store the punches in a punch history table when we send them to Insperity; we'll check that table
				*** to prevent duplicate punches from going into Insperity.

				lnNumberOfErrors = 0

				.TrackProgress('Qualcomm Driver Punch Import process started....', LOGIT+SENDIT+NOWAITIT)
				.TrackProgress('COMPUTERNAME = ' + .cCOMPUTERNAME, LOGIT+SENDIT)
				.TrackProgress('USERNAME = ' + .cUSERNAME, LOGIT+SENDIT)
				.TrackProgress('USERNAME = ' + .cUSERNAME, LOGIT+SENDIT)
				.TrackProgress('PROJECT = QCPUNCH_IMPORT', LOGIT+SENDIT)
				IF .lTestMode THEN
					.TrackProgress('                        TEST MODE                        ', LOGIT+SENDIT)
				ENDIF
				IF .lUseSQLTables THEN
					.TrackProgress('====> Using SQL table fx.dbo.pos', LOGIT+SENDIT)
				ELSE
					.TrackProgress('.cInputTable = ' + .cInputTable, LOGIT+SENDIT)
				ENDIF  &&  .lUseSQLTables
				.TrackProgress('.cDriverPunchTable = ' + .cDriverPunchTable, LOGIT+SENDIT)

				ldToday = .dToday
				***********************
				* activate to force another date
				*ldToday = {^2014-12-17}
				***********************

				ldStartDate = ldToday - 4  && starts 4 days ago, just in case program didn't run over the weekend or some other problem.

				.cSubject = 'Qualcomm Driver Punch Import for: ' + DTOC(.dToday)

				lcStartDate = STRTRAN(DTOC(ldStartDate),"/","-")

				*	 create "YYYYMMDD" safe date format for use in SQL query
				SET DATE YMD
				lcSQLStartDate = STRTRAN(DTOC(ldStartDate),"/","")



				USE F:\HR\HRDATA\EMPLOYEE IN 0 SHARED ALIAS EMPLOYEE

				USE (.cDriverPunchTable) IN 0 SHARED ALIAS PUNCH_TABLE

				IF .lUseSQLTables THEN
					* Darren says the query must be in lower case and in FoxPro, not SQL format.
					*lcSQL = "SELECT * FROM FX.DBO.POS WHERE (ZDATE > '"  + lcSQLStartDate + "') AND (KPEMP > 0) AND (KPSENT = 0) "
					*lcSQL = "select * from fx.dbo.pos where zdate > {"  + DTOC(ldStartDate) + "} and kpemp > 0 and kpsent = 0 "
					lcSQL = "select * from fx.dbo.pos where zdate > {"  + DTOC(ldStartDate) + "} and kpemp > '0000' and kpsent = 0 "
					.TrackProgress('lcSQL = ' + lcSQL, LOGIT+SENDIT)

					XSQLEXEC(lcSQL,"POS_TABLE",,"FX")
					
				ELSE
					* use VFP TABLE
					USE (.cInputTable) AGAIN IN 0 SHARED ALIAS POS_TABLE
				ENDIF  &&  .lUseSQLTables
				
				lnDuplicates = 0
				lnInserts = 0


				*07/26/2013 10:34:23 AM

				.TrackProgress('Scanning POS_TABLE....', LOGIT+SENDIT)
				
*!*					SET STEP ON 

				SELECT POS_TABLE				

				SCAN FOR (ZDATE > ldStartDate) AND (NOT EMPTYNUL(KPDATETIME)) AND (NOT KPSENT) AND (NOT EMPTY(KPEMP))

					lcEmp_Num = ALLTRIM(POS_TABLE.KPEMP)	
					
					
					* validate the emp_num, because we are seeing bad data in that field
					llValidEmp_Num = .T.
					
					IF NOT (VAL(lcEmp_Num) > 0) THEN
						llValidEmp_Num = .F.
						.TrackProgress('ERROR: invalid emp_num: ' + lcEmp_Num, LOGIT+SENDIT)
					ENDIF
					
					IF llValidEmp_Num THEN
						SELECT EMPLOYEE
						LOCATE FOR emp_num = VAL(lcEmp_Num)
						IF FOUND() THEN
							.TrackProgress('Emp_num [' + lcEmp_Num + '] is for ' + EMPLOYEE.employee, LOGIT+SENDIT)
						ELSE
							llValidEmp_Num = .F.
							.TrackProgress('ERROR: could not find emp_num in employee table: ' + lcEmp_Num, LOGIT+SENDIT)
						ENDIF
					ENDIF
									
					IF llValidEmp_Num THEN
					
						ldPunchDateTime = POS_TABLE.KPDATETIME					
															
						ldPunchDate = TTOD(ldPunchDateTime)
						
						
						lcPunchDate = ALLTRIM(TRANSFORM(ldPunchDate))
						lcPunchtype = POS_TABLE.KPTYPE
						
						* lnDSTAdjustmentSecs
						
						lcPunchDateTime = ALLTRIM(TRANSFORM(ldPunchDateTime))
						lnSpacePos = AT(' ',lcPunchDateTime)
						lcPunchTime = SUBSTR(lcPunchDateTime,lnSpacePos + 1,5)

						lcSearch = "Emp # = " + lcEmp_Num + "   " + lcPunchDate + "   " + lcPunchTime + "   " + lcPunchtype
						.TrackProgress("Processing:  " + lcSearch, LOGIT+SENDIT)

						* CHECK PUNCH HISTORY TABLE FOR THIS EMPLOYEE; IF NOT FOUND, INSERT IT 
						SELECT PUNCH_TABLE
						LOCATE FOR (EMP_NUM = lcEmp_Num) AND (PUNCHDATE = ldPunchDate) AND (PUNCHTIME = lcPunchTime) AND (PUNCHTYPE = lcPunchtype)
						IF FOUND() THEN

							*lcDupe = "Dupe Punch, not loaded:  Emp # = " + lcEmp_Num + "   " + lcPunchDate + "   " + lcPunchTime + "   " + lcPunchtype
							.TrackProgress("Dupe Punch, not loaded:  " + lcSearch, LOGIT+SENDIT)						
							lnDuplicates = lnDuplicates + 1

						ELSE

							INSERT INTO PUNCH_TABLE ;
								(EMP_NUM, PUNCHDATE, PUNCHTIME, PUNCHTYPE, LOADEDDT) ;
								VALUES ;
								(lcEmp_Num, ldPunchDate, lcPunchTime, lcPunchtype, DATETIME())
								
							lnInserts = lnInserts + 1

						ENDIF && FOUND()

						* set Sent flag to True in POS table
						IF .lUseSQLTables THEN
							lcSQL = "UPDATE FX.DBO.POS SET KPSENT = 1 WHERE (POSID = " + TRANSFORM(POS_TABLE.POSID) + ")"
							.TrackProgress('lcSQL = ' + lcSQL, LOGIT+SENDIT)
							*XSQLEXEC("UPDATE FX.DBO.POS SET KPSENT = 1 WHERE (POSID = " + TRANSFORM(POS_TABLE.POSID) + ")",,,"FX")
							XSQLEXEC(lcSQL,,,"FX")
						ELSE
							* use VFP TABLE
							REPLACE POS_TABLE.KPSENT WITH .T. IN POS_TABLE
						ENDIF  &&  .lUseSQLTables
					
					ENDIF && llValidEmp_Num

				ENDSCAN  && POS_TABLE
				
				
				
				** do a scan of punch table to add driver names for punches just loaded
				USE F:\UTIL\TIMESTAR\DATA\EEINFO.DBF IN 0 ALIAS EEINFO
				
				SELECT PUNCH_TABLE
				SCAN FOR EMPTY(NAME)
					SELECT EEINFO
					LOCATE FOR BADGENUM = VAL(PUNCH_TABLE.EMP_NUM)
					IF FOUND() THEN
						REPLACE PUNCH_TABLE.NAME WITH EEINFO.NAME IN PUNCH_TABLE
						.TrackProgress("Processed punch for :  " + ALLTRIM(EEINFO.NAME), LOGIT+SENDIT)
					ENDIF				
				ENDSCAN
				
				

				.TrackProgress('# Inserts = ' + TRANSFORM(lnInserts), LOGIT+SENDIT)
				.TrackProgress('# Duplicates = ' + TRANSFORM(lnDuplicates), LOGIT+SENDIT)

				.TrackProgress('The process ended normally.', LOGIT+SENDIT)

			CATCH TO loError

				.TrackProgress('There was an error.',LOGIT+SENDIT)
				.TrackProgress('The Error # is: ' + TRANSFORM(loError.ERRORNO), LOGIT+SENDIT)
				.TrackProgress('The Error Message is: ' + TRANSFORM(loError.MESSAGE), LOGIT+SENDIT)
				.TrackProgress('The Error occurred in Program: ' + TRANSFORM(loError.PROCEDURE), LOGIT+SENDIT)
				.TrackProgress('The Error occurred at line #: ' + TRANSFORM(loError.LINENO), LOGIT+SENDIT)
				lnNumberOfErrors = lnNumberOfErrors + 1

			ENDTRY

			CLOSE DATA
			WAIT CLEAR
			***************** INTERNAL email results ******************************
			.TrackProgress('About to send status email.',LOGIT)
			.TrackProgress('==================================================================================================================', SENDIT)
			.TrackProgress('The logfile is: ' + .cLogFile, SENDIT)
			.TrackProgress('Qualcomm Driver Punch Import process started: ' + .cStartTime, LOGIT+SENDIT)
			.TrackProgress('Qualcomm Driver Punch Import process finished: ' + TTOC(DATETIME()), LOGIT+SENDIT)

			IF .lSendInternalEmailIsOn THEN
				* try to trap error from not having dartmail dll's registered on user's pc...
				TRY
					DO FORM dartmail2 WITH .cSendTo,.cFrom,.cSubject,.cCC,.cAttach,.cBodyText,"A"
					.TrackProgress('Sent status email.',LOGIT)
				CATCH TO loError
					.TrackProgress('There was an error sending the status email.',LOGIT)
					.TrackProgress('The Error # is: ' + TRANSFORM(loError.ERRORNO), LOGIT)
					.TrackProgress('The Error Message is: ' + TRANSFORM(loError.MESSAGE), LOGIT)
					.TrackProgress('The Error occurred in Program: ' + TRANSFORM(loError.PROCEDURE), LOGIT)
					.TrackProgress('The Error occurred at line #: ' + TRANSFORM(loError.LINENO), LOGIT)
					lnNumberOfErrors = lnNumberOfErrors + 1
				ENDTRY

			ELSE
				.TrackProgress('Did not send status email: THIS.lSendInternalEmailIsOn = FALSE.',LOGIT)
			ENDIF

		ENDWITH
		RETURN
	ENDFUNC && main


	*!*		FUNCTION ExecSQL
	*!*			LPARAMETERS tcSQL, tcCursorName, tlNoDataReturnedIsOkay
	*!*			LOCAL llRetval, lnResult
	*!*			WITH THIS
	*!*				* close target cursor if it's open
	*!*				IF USED(tcCursorName)
	*!*					USE IN (tcCursorName)
	*!*				ENDIF
	*!*				WAIT WINDOW "Fetching remote data; please wait..." NOWAIT
	*!*				lnResult = SQLEXEC(.nSQLHandle, tcSQL, tcCursorName)
	*!*				llRetval = ( lnResult > 0 )
	*!*				IF llRetval THEN
	*!*					* see if any data came back
	*!*					IF NOT tlNoDataReturnedIsOkay THEN
	*!*						IF (NOT USED(tcCursorName)) OR EOF(tcCursorName) THEN
	*!*							llRetval = .F.
	*!*							.TrackProgress('No data was returned by this query: ' +CRLF + tcSQL, LOGIT+SENDIT)
	*!*						ENDIF
	*!*					ENDIF
	*!*				ELSE
	*!*					.TrackProgress("Result Code [" + TRANSFORM(lnResult) + "] returned by this query: " + tcSQL, LOGIT+SENDIT)
	*!*				ENDIF
	*!*				WAIT CLEAR
	*!*				RETURN llRetval
	*!*			ENDWITH
	*!*		ENDFUNC


	PROCEDURE TrackProgress
		* do any combination of Wait Window, writing to logfile, and adding to body of email,
		* based on nFlags parameter.
		LPARAMETERS tcExpression, tnFlags
		WITH THIS
			IF BITAND(tnFlags,LOGIT) = LOGIT THEN
				IF .lLoggingIsOn THEN
					?
					? .cProcessName + "  " + TTOC(DATETIME()) + ": " + tcExpression
				ENDIF
			ENDIF
			IF .lWaitWindowIsOn THEN
				IF BITAND(tnFlags,NOWAITIT) = NOWAITIT THEN
					WAIT WINDOW tcExpression NOWAIT
				ENDIF
				IF BITAND(tnFlags,WAITIT) = WAITIT THEN
					WAIT WINDOW tcExpression TIMEOUT .nWaitWindowTimeout
				ENDIF
			ENDIF
			IF BITAND(tnFlags,SENDIT) = SENDIT THEN
				IF .lSendInternalEmailIsOn THEN
					.cBodyText = .cBodyText + tcExpression + CRLF + CRLF
				ENDIF
			ENDIF
		ENDWITH
	ENDPROC  &&  TrackProgress


ENDDEFINE
