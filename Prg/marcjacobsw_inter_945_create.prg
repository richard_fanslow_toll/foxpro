*!* MARC JACOBS WHLS 945, used to reconcile an accout to accout Xfer
*!* Creation Date: 3.06.2014 by PG

PARAMETERS cXferNum

PUBLIC ARRAY thisarray(1)
PUBLIC c_CntrlNum,c_GrpCntrlNum,cReturned,cWO_Num,cWO_NumStr,nWO_Num1,lTesting,cISA_Num,cOffice,lUpdated,lSingleBOL
PUBLIC lEmail,lDoMJFilesOut,dDateTimeCal,nFilenum,tsendto,tcc,tsendtoerr,tccerr,lPSU,cProgname,NormalExit,cErrMsg
PUBLIC lUPSType,lUPS,lFedex,tccwhse
cProcName = "marcjacobsw945_inter_945_create"

lTesting   = .F.  && Set to .t. for testing

CLOSE DATA ALL

DO m:\dev\prg\_setvars WITH lTesting
ON ESCAPE CANCEL
*On Error Throw

cProgname = "marcjacobsw_inter_945_create"
STORE "" TO tattach,cShip_ref,ccustname,cWO_NumStr,tcc
lIsError = .F.
lCloseOutput = .T.
nFilenum = 0
lPick = .T.
lPrepack = !lPick
NormalExit = .F.
lUpdated = .F.
cTrknumber = ""

WAIT WINDOW "At the start of MARC JACOBS WHLS 945 STD. PICKPACK process..." NOWAIT

TRY
	IF VARTYPE(nAcctNum) = "L"
		nAcctNum = 6303
	ENDIF
	cOffice = IIF(cOffice = "J","N",cOffice)
	cMod = IIF(cOffice = "N","J",cOffice)
	cPPName = "MarcJacobsW"
	tfrom = "Toll EDI Operations <toll-edi-ops@tollgroup.com>"
	tmessage=""

	lISAFlag = .T.
	lSTFlag = .T.

	IF lTesting
		CLOSE DATABASES ALL
		IF !USED('edi_trigger')
			cEDIFolder = "F:\3PL\DATA\"
			USE (cEDIFolder+"edi_trigger") IN 0 ALIAS edi_trigger
		ENDIF
		cTime = DATETIME()
	ENDIF

	IF lTesting
		cUseFolder = "F:\WHP\WHDATA\"
	ELSE
		xReturn = "XXX"
		DO m:\dev\prg\wf_alt WITH "J",6303
		cUseFolder = UPPER(xReturn)
	ENDIF

	IF lTesting
		WAIT WINDOW "Folder Used: "+cUseFolder TIMEOUT 1
	ENDIF

	lEmail = .T.
	lTestMail = lTesting && Sends mail to Joe only
	lDoMJFilesOut = !lTesting && If true, copies output to FTP folder (default = .t.)
	lStandalone = lTesting

	WAIT WINDOW "Now preparing tables and variables" NOWAIT NOCLEAR
	STORE "" TO lcPath,lcArchivePath,lcHoldPath

*!* SET CUSTOMER CONSTANTS
	ccustname = "MARCJACOBSW"  && Customer Identifier
	cX12 = "004010"  && X12 Standards Set used
	cMailName = "Marc Jacobs (W)"
	SELECT 0
	USE F:\3pl\DATA\mailmaster ALIAS mm
SET STEP ON 
	LOCATE FOR mm.edi_type = "945" AND mm.office=cOffice AND mm.accountid = 6303

	lcPath = ALLTRIM(mm.basepath)
	lcArchivePath = ALLTRIM(mm.archpath)
	lcHoldPath    = ALLTRIM(mm.HoldPath)
	tsendto       = ALLTRIM(IIF(mm.use_alt,sendtoalt,sendto))
	tcc           = ALLTRIM(IIF(mm.use_alt,ccalt,cc))

*  WAIT WINDOW "Mailing to: "+tsendto+CHR(13)+tcc TIMEOUT 2
	LOCATE
	LOCATE FOR mm.edi_type = 'MISC' AND mm.taskname = "MJADDWHSE"
	tccwhse = IIF(mm.use_alt,ccalt,cc)
	LOCATE
	LOCATE FOR mm.edi_type = 'MISC' AND mm.taskname = "MJTEST"
	tsendtotest = IIF(mm.use_alt,sendtoalt,sendto)
	tcctest = IIF(mm.use_alt,ccalt,cc)
	tsendtoerr = IIF(mm.use_alt,sendtoalt,sendto)
	tccerr = IIF(mm.use_alt,ccalt,cc)
	USE IN mm

	IF lTesting
		tsendto ="pgaidis@fmiint.com,tmarg@fmiint.com"
		tcc=""

		tsendtotest = "pgaidis@fmiint.com"
		tcctest     = ""

		tsendtoerr = "pgaidis@fmiint.com,tmarg@fmiint.com"
		tccerr     = ""
	ENDIF

*!* SET OTHER CONSTANTS

	cCustLoc =  "NJ"
	cFMIWarehouse = ""
	cCustPrefix = "945"+"j"
	cDivision = "New Jersey"
	cFolder = "WHN"
	cSF_Addr1  = "800 FEDERAL BLVD"
	cSF_CSZ    =  "CARTERET*NJ*07008"

	lNonEDI = .F.  && Assumes not a Non-EDI 945
	STORE "" TO lcKey
	STORE 0 TO alength,nLength
	cTime = DATETIME()
	cDelTime = SUBSTR(TTOC(cTime,1),9,4)
	cCarrierType = "M" && Default as "Motor Freight"
	dapptnum = ""

*!* SET OTHER CONSTANTS
	IF INLIST(cOffice,'C','1','2','5','6','7')
		dDateTimeCal = (DATETIME()-(3*3600))
	ELSE
		dDateTimeCal = DATETIME()
	ENDIF
	dt1 = TTOC(dDateTimeCal,1)
	dtmail = TTOC(dDateTimeCal)
	dt2 = dDateTimeCal
	cString = ""

	csendqual = "ZZ"
	csendid = "USNJ1DC"
	crecqual = "12"
	crecid = "2129070080"

	cfd = "*"
	csegd = "~"
	nSegCtr = 0
	cterminator = ">" && Used at end of ISA segment
	cdate = DTOS(DATE())
	cTruncDate = RIGHT(cdate,6)
	cTruncTime = SUBSTR(TTOC(dDateTimeCal,1),9,4)
	cfiledate = cdate+cTruncTime
	cOrig = "J"
	nSTCount = 0
	cStyle = ""
	cPTString = ""
	nTotCtnWt = 0
	nTotCtnCount = 0
	cTargetStyle = ""

** PAD ID Codes
	csendidlong = PADR(csendid,15," ")
	crecidlong = PADR(crecid,15," ")

	cCustFolder = "MarcJacobs"
	cFilename = (lcHoldPath+cCustPrefix+dt1+".txt")
	cFilenameShort = JUSTFNAME(cFilename)
	cFilenameOut = (lcPath+cFilenameShort)
	cFilenameArch = (lcArchivePath+cFilenameShort)
	nFilenum = FCREATE(cFilename)

	IF lTesting
		USE m:\dev\whdata\xfer  IN 0 ALIAS xfer
		USE m:\dev\whdata\xferdet IN 0 ALIAS xferdet
	ELSE
		USE (cUseFolder+"xfer")  IN  0 ALIAS xfer
		USE (cUseFolder+"xferdet") IN 0 ALIAS xferdet
	ENDIF

	IF !SEEK(VAL(ALLTRIM(TRANSFORM(cXferNum))),'xfer','xfernum')
		SET STEP ON
		WAIT WINDOW "Transfer not found in XFER\XFERDET" TIMEOUT 2
		DO ediupdate WITH "Xfer NOT FOUND",.T.
		THROW
		SET STEP ON
	ENDIF

	cRetMsg = ""

*!* HEADER LEVEL EDI DATA
	DO num_incr_isa

	cISA_Num = PADL(c_CntrlNum,9,"0")
	nISA_Num = INT(VAL(cISA_Num))

	IF lTesting
		cISACode = "T"
	ELSE
		cISACode = "P"
	ENDIF

	STORE "ISA"+cfd+"00"+cfd+"          "+cfd+"00"+cfd+"          "+cfd+csendqual+cfd+csendidlong+cfd+;
		crecqual+cfd+crecidlong+cfd+cTruncDate+cfd+cTruncTime+cfd+"U"+cfd+"00401"+cfd+;
		cISA_Num+cfd+"0"+cfd+cISACode+cfd+cterminator+csegd TO cString
	DO cstringbreak

	STORE "GS"+cfd+"SW"+cfd+csendid+cfd+crecid+cfd+cdate+cfd+cTruncTime+cfd+c_CntrlNum+;
		cfd+"X"+cfd+cX12+csegd TO cString
	DO cstringbreak

	STORE "ST"+cfd+"945"+cfd+PADL(c_GrpCntrlNum,9,"0")+csegd TO cString
	DO cstringbreak
	nSTCount = nSTCount + 1
	nSegCtr = 1


*************************************************************************
* Loops through xferdet for the xfer
*************************************************************************

	SELECT xferdet
	SCAN FOR xferid= xfer.xferid
		m.consignee  = xfer.fromacct
		ddel_date    = xfer.xferdt

*    Do num_incr_st
		WAIT CLEAR

		WAIT WINDOW "Now creating Line Item information" NOWAIT NOCLEAR

		STORE "W06"+cfd+"N"+cfd+"XFER_PT#"+cfd+cdate+cfd+""+cfd+TRIM(TRANSFORM(cXferNum))+cfd+TRIM(TRANSFORM(cXferNum))+csegd TO cString
		DO cstringbreak
		nSegCtr = nSegCtr + 1
		nCtnNumber = 1  && Seed carton sequence count

		STORE "N1"+cfd+"SF"+cfd+ALLTRIM(xfer.fromacct)+cfd+"91"+cfd+ALLTRIM(TRANSFORM(xfer.fromacctid))+csegd TO cString
		DO cstringbreak
		nSegCtr = nSegCtr + 1


		STORE "N1"+cfd+"ST"+cfd+ALLTRIM(xfer.toacct)+cfd+"92"+cfd+ALLTRIM(TRANSFORM(xfer.toacctid))+csegd TO cString
		DO cstringbreak
		nSegCtr = nSegCtr + 1

		STORE "G62"+cfd+"10"+cfd+TRIM(DTOS(ddel_date))+cfd+"A"+cfd+cDelTime+csegd TO cString  && Ship date/time
		DO cstringbreak
		nSegCtr = nSegCtr + 1

		STORE "W27"+cfd+"T"+cfd+"TOLL"+cfd+"TOLL"+csegd TO cString
		DO cstringbreak
		nSegCtr = nSegCtr + 1

*     Store "G72"+cfd+"504"+cfd+"06"+Replicate(cfd,6)+Allt(cFreight)+csegd To cString
*     Do cstringbreak
*     nSegCtr = nSegCtr + 1

*************************************************************************
*2  DETAIL LOOP - MATCHING SHIP_REF FIELD (FROM OUTSHIP)
*************************************************************************
		WAIT CLEAR
		WAIT WINDOW "Now creating Detail information "+TRANSFORM(cXferNum) NOWAIT NOCLEAR


		STORE 0 TO nShipCtnTotQty,nShipDetTotQty

		linenumber = 1
		SELECT xferdet
		SCAN FOR xferdet.xferid = xfer.xferid

			STORE "LX"+cfd+ALLTRIM(STR(linenumber))+csegd TO cString
			DO cstringbreak
			nSegCtr = nSegCtr + 1

			STORE "MAN"+cfd+"GM"+cfd+TRIM("INVENTORY MOVE")+csegd TO cString
			DO cstringbreak
			nSegCtr = nSegCtr + 1

			nShipDetQty = xferdet.qty
			nShipDetTotQty = nShipDetTotQty+nShipDetQty

			cStyle   = TRIM(xferdet.STYLE)
			cColor   = TRIM(xferdet.COLOR)
			cSize    = TRIM(xferdet.ID)
			cUPC     = TRIM(xferdet.upc)

			STORE "W12"+cfd+"SH"+cfd+ALLTRIM(STR(nShipDetQty))+cfd+ALLTRIM(STR(nShipDetQty))+cfd+cfd+"EA"+cfd+cfd+"UP"+cfd+cUPC+REPLICATE(cfd,13)+"VN"+cfd+cStyle+csegd TO cString
			DO cstringbreak
			nSegCtr = nSegCtr + 1

			STORE "N9"+cfd+"ST"+cfd+TRIM(cStyle)+csegd TO cString
			DO cstringbreak
			nSegCtr = nSegCtr + 1

			STORE "N9"+cfd+"CL"+cfd+TRIM(cColor)+csegd TO cString
			DO cstringbreak
			nSegCtr = nSegCtr + 1

			STORE "N9"+cfd+"SZ"+cfd+TRIM(cSize)+csegd TO cString
			DO cstringbreak
			nSegCtr = nSegCtr + 1

			STORE "N9"+cfd+"UP"+cfd+TRIM(cUPC)+csegd TO cString
			DO cstringbreak
			nSegCtr = nSegCtr + 1

		ENDSCAN


*************************************************************************
*2^  END DETAIL MATCHING SHIP_REF LOOP
*************************************************************************
		WAIT CLEAR
		WAIT WINDOW "Now creating Section Closure information" NOWAIT NOCLEAR
		STORE "W03"+cfd+ALLTRIM(STR(nShipDetTotQty))+csegd TO cString

		FWRITE(nFilenum,cString)
		STORE  "SE"+cfd+ALLTRIM(STR(nSegCtr+2))+cfd+PADL(c_GrpCntrlNum,9,"0")+csegd TO cString
		FWRITE(nFilenum,cString)
		WAIT CLEAR
	ENDSCAN

&&  Insert Into F:\WH\MJFILES (Type,voucher,pickticket,bol,filename,When) Values ("945","WHOLESALE","","xfer",cFilename,Datetime())


*************************************************************************
*1^  END OUTSHIP MAIN LOOP
*************************************************************************

	STORE  "GE"+cfd+ALLTRIM(STR(nSTCount))+cfd+c_CntrlNum+csegd TO cString
	FWRITE(nFilenum,cString)

	STORE  "IEA"+cfd+"1"+cfd+PADL(c_CntrlNum,9,"0")+csegd TO cString
	FWRITE(nFilenum,cString)

	=FCLOSE(nFilenum)

	SET STEP ON

	IF lTesting
		CLOSE DATA ALL
		THROW
	ENDIF




*!* Transfers files to correct output folders
	ASSERT .F. MESSAGE "At FilesOut Statement...debug"
	COPY FILE [&cFilename] TO [&cFilenameArch]
	IF !FILE(cFilenameArch)
		DO ediupdate WITH "NO ARCHIVE CREATED",.T.
	ENDIF
	IF lDoMJFilesOut AND !lTesting
		COPY FILE [&cFilename] TO [&cFilenameOut]
		DELETE FILE [&cFilename]
	ENDIF

	SELECT temp945mj
	COPY TO "f:\3pl\data\temp945omj.dbf"
	USE IN temp945mj
	SELECT 0
	USE "f:\3pl\data\temp945omj.dbf" ALIAS temp945omj
	SELECT 0
	USE "f:\3pl\data\pts_sent945.dbf" ALIAS pts_sent945
	APPEND FROM "f:\3pl\data\temp945omj.dbf"
	USE IN pts_sent945
	USE IN temp945omj
	DELETE FILE "f:\3pl\data\temp945omj.dbf"

	IF !lTesting
		SELECT edi_trigger
		DO ediupdate WITH "945 CREATED",.F.
		SELECT edi_trigger
		LOCATE

		IF !USED("ftpedilog")
			SELECT 0
			USE F:\edirouting\ftpedilog ALIAS ftpedilog
			INSERT INTO ftpedilog (TRANSFER,ftpdate,filename,acct_name,TYPE) VALUES ("945-"+ccustname+"-"+cCustLoc,dt2,cFilename,UPPER(ccustname),"945")
			USE IN ftpedilog
		ENDIF
	ENDIF

	WAIT CLEAR

*!* Create eMail confirmation message
	IF lTestMail
		tsendto = tsendtotest
		tcc = tcctest
	ENDIF

	cOutFolder = "FMI"+cCustLoc

	tsubject = cMailName+" 945 EDI File from TGF as of "+dtmail+" ("+cCustLoc+")"
	tattach = " "
	tmessage = "945 EDI Info from TGF, file "+cFilenameShort+CHR(13)
	tmessage = tmessage + "Division: "+cDivision+", BOL# "+TRIM("44")+CHR(13)
	tmessage = tmessage + "For Work Orders: "+cWO_NumStr+","+CHR(13)
	tmessage = tmessage + "containing these picktickets:"+CHR(13)
	tmessage = tmessage + cPTString + CHR(13)
	tmessage = tmessage +"has been created and will be transmitted ASAP."+CHR(13)+CHR(13)
	tmessage = tmessage + "If you have any questions, please eMail or call 732-750-9000 x217."
	IF lEmail
		DO FORM m:\dev\frm\dartmail2 WITH tsendto,tfrom,tsubject,tcc,tattach,tmessage,"A"
	ENDIF

	RELEASE ALL LIKE c_CntrlNum,c_GrpCntrlNum
	WAIT CLEAR
	WAIT WINDOW cMailName+" 945 EDI File output complete" AT 20,60 TIMEOUT 1

	NormalExit = .T.

CATCH TO oErr
	IF !NormalExit
*SET STEP ON
		IF lTesting
*			return
			tsendto  = tsendtotest
			tcc = tcctest
			tsendto  = "pgaidis@fmiint.com,tmarg@fmiint.com"
			tcc = ""
		ELSE
			tsendto  = tsendtoerr
			tcc = tccerr
		ENDIF

		tmessage = ccustname+" ERROR processing "+CHR(13)

*   Do ediupdate With Upper(Alltrim(oErr.Message)),.T.
		tsubject = "ERROR " +ccustname+" ERROR ("+TRANSFORM(oErr.ERRORNO)+") at "+TTOC(DATETIME())
		tattach  = ""

		tmessage =tmessage+ "Try/CATCH Exeception Message:"+CHR(13)+;
			[  Error: ] + STR(oErr.ERRORNO) +CHR(13)+;
			[  LineNo: ] + STR(oErr.LINENO) +CHR(13)+;
			[  Message: ] + oErr.MESSAGE +CHR(13)+;
			[  Procedure: ] + oErr.PROCEDURE +CHR(13)+;
			[  Details: ] + oErr.DETAILS +CHR(13)+;
			[  StackLevel: ] + STR(oErr.STACKLEVEL) +CHR(13)+;
			[  LineContents: ] + oErr.LINECONTENTS+CHR(13)+;
			[  UserValue: ] + oErr.USERVALUE
		tmessage =tmessage+CHR(13)+cProgname
		tmessage =tmessage+CHR(13)+"BOL "&&+cBOL
		tmessage =tmessage+CHR(13)+CHR(13)+cErrMsg

		tsubject = "ERROR " +cMailName+" 945 EDI Error at "+TTOC(DATETIME())
		tattach  = ""
		tfrom    ="TGF EDI Operations <toll-edi-ops@tollgroup.com>"
		DO FORM m:\dev\frm\dartmail2 WITH tsendto,tfrom,tsubject,tcc,tattach,tmessage,"A"
	ENDIF
FINALLY
	ON ERROR

ENDTRY

*** END OF CODE BODY

****************************
PROCEDURE close945
****************************
** Footer Creation

	STORE  "GE"+cfd+ALLTRIM(STR(nSTCount))+cfd+c_CntrlNum+csegd TO cString
	FWRITE(nFilenum,cString)

	STORE  "IEA"+cfd+"1"+cfd+PADL(c_CntrlNum,9,"0")+csegd TO cString
	FWRITE(nFilenum,cString)

ENDPROC

****************************
PROCEDURE num_incr_isa
****************************

	IF !USED("serfile")
		USE ("f:\3pl\data\serial\"+ccustname+"945_serial") IN 0 ALIAS serfile
	ENDIF
	SELECT serfile
	IF lTesting AND lISAFlag
		nOrigSeq = serfile.seqnum
		lISAFlag = .F.
	ENDIF
	nISA_Num = serfile.seqnum
	c_CntrlNum = ALLTRIM(STR(serfile.seqnum))
	REPLACE serfile.seqnum WITH serfile.seqnum + 1 IN serfile
ENDPROC

****************************
PROCEDURE num_incr_st
****************************

	IF !USED("serfile")
		USE ("f:\3pl\data\serial\"+ccustname+""945_serial") IN 0 ALIAS serfile
	ENDIF
	SELECT serfile
	IF lTesting AND lSTFlag
		nOrigGrpSeq = serfile.grpseqnum
		lSTFlag = .F.
	ENDIF
	c_GrpCntrlNum = ALLTRIM(STR(serfile.grpseqnum))
	REPLACE serfile.grpseqnum WITH serfile.grpseqnum + 1 IN serfile
ENDPROC

****************************
PROCEDURE segmentget
****************************

	PARAMETER thisarray,lcKey,nLength

	FOR i = 1 TO nLength
		IF i > nLength
			EXIT
		ENDIF
		lnEnd= AT("*",thisarray[i])
		IF lnEnd > 0
			lcThisKey =TRIM(SUBSTR(thisarray[i],1,lnEnd-1))
			IF OCCURS(lcKey,lcThisKey)>0
				RETURN SUBSTR(thisarray[i],lnEnd+1)
				i = 1
			ENDIF
		ENDIF
	ENDFOR

	RETURN ""

****************************
PROCEDURE ediupdate
****************************
	PARAMETER cStatus,lIsError
	lUpdated = .T.
	cErrMsg = cStatus

	IF !lTesting
		SELECT edi_trigger
		nRec = RECNO()
		IF !lIsError
			NormalExit = .T.
			REPLACE edi_trigger.processed WITH .T.,edi_trigger.proc945 WITH .T.,edi_trigger.file945 WITH cFilenameArch,;
				edi_trigger.isa_num WITH cISA_Num,;
				edi_trigger.fin_status WITH "945 CREATED",edi_trigger.errorflag WITH .F.,edi_trigger.when_proc WITH DATETIME() ;
				FOR edi_trigger.bol = cBOL AND edi_trigger.accountid = nAcctNum AND edi_type = "945 "
		ELSE
			REPLACE edi_trigger.processed WITH .T.,edi_trigger.proc945 WITH .F.,edi_trigger.file945 WITH "",;
				edi_trigger.fin_status WITH cStatus,edi_trigger.errorflag WITH .T. ;
				FOR edi_trigger.bol = cBOL AND edi_trigger.accountid = nAcctNum AND edi_type = "945 "
			IF lCloseOutput
				=FCLOSE(nFilenum)
				ERASE &cFilename
			ENDIF
		ENDIF
	ENDIF

	IF lIsError AND lEmail AND cStatus<>"SQL ERROR"
		tsubject = "ERROR - 945 ERROR in "+cMailName+" BOL "+TRIM(cBOL)+"(At PT "+cShip_ref+")"
		tattach = " "
		tsendto = tsendtoerr
		tcc = IIF(cErrMsg = "EMPTY DC/STORE#",tccwhse,tccerr)
		tmessage = "945 Processing for BOL# "+cBOL+", WO# "+cWO_Num+" produced this error: (Office/Mod: "+cOffice+"/"+cMod+"): "+cStatus+CHR(13)+"Check EDI_TRIGGER and re-run"
		IF "TOTQTY ERR"$cStatus
			tmessage = tmessage + CHR(13) + "At OUTSHIPID: "+ALLTRIM(STR(m.outshipid))
		ENDIF
		DO FORM m:\dev\frm\dartmail2 WITH tsendto,tfrom,tsubject,tcc,tattach,tmessage,"A"
		NormalExit = .T.
	ENDIF

	closedata()

	IF !lTesting
		SELECT edi_trigger
		LOCATE
	ENDIF
	IF lIsError
	ENDIF
ENDPROC

****************************
PROCEDURE cstringbreak
****************************
	cLen = LEN(ALLTRIM(cString))
	FWRITE(nFilenum,cString)
ENDPROC

****************************
PROCEDURE closedata
****************************
	IF USED('outship')
		USE IN outship
	ENDIF
	IF USED('outdet')
		USE IN outdet
	ENDIF

	IF USED('serfile')
		SELECT serfile
		IF lTesting
			REPLACE serfile.seqnum WITH nOrigSeq
			REPLACE serfile.grpseqnum WITH nOrigGrpSeq
		ENDIF
		USE IN serfile
	ENDIF
	IF USED('scacs')
		USE IN scacs
	ENDIF
	IF USED('mm')
		USE IN mm
	ENDIF
	IF USED('tempx')
		USE IN tempx
	ENDIF
ENDPROC
