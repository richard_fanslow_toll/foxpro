*
* Take the data from the 856 just uploaded and create an Inbound Work Order
*
*
PARAMETERS whichcontainer,whichaccount,whichshipid

WAIT WINDOW "Now in Nautica WO Creation module..." NOWAIT

naccountid = whichaccount  &&6356
m.accountid = whichaccount

*Select * From inwolog Where .F. Into Cursor xinwolog Readwrite

SELECT * FROM xdetail INTO CURSOR temp WHERE shipid = whichshipid AND accountid = whichaccount

SELECT xinwolog
SCATTER MEMVAR BLANK FIELDS EXCEPT CONTAINER,REFERENCE,inwologid
SELECT xpl
SCATTER MEMVAR BLANK

SELECT temp
LOCATE
*brow
SCATTER MEMVAR

IF m.accountid = 6356
	m.acctname ="KIPLING"
	tacct= 6356
ENDIF
IF m.accountid = 687
	m.acctname ="NAUTICA"
	tacct = 687
ENDIF

m.comments = "ASN UPLOAD"+CHR(13)+"CTR "+temp.CONTAINER
ninwologid = ninwologid + 1
m.inwologid = ninwologid
INSERT INTO xinwolog FROM MEMVAR

REPLACE xinwolog.acct_ref   WITH m.bol IN xinwolog
REPLACE xinwolog.brokerref  WITH temp.shipid IN xinwolog
REPLACE xinwolog.CONTAINER  WITH whichcontainer IN xinwolog

SELECT temp
GOTO TOP
firstloop = .T.
SELECT STYLE,COLOR,ID,PACK,upc,REFERENCE,ponum,COUNT(1) AS ctnqty FROM temp INTO CURSOR dtemp GROUP BY STYLE,COLOR,ID,PACK,upc
SELECT dtemp
*BROWSE TIMEOUT 10
ctnsum =0
m.plid = m.plid+1
SCAN
	SELECT xpl
	SCATTER MEMVAR BLANK MEMO
	m.inwologid = xinwolog.inwologid
*	m.plid   = plidvalue
	m.units  = .F.
	m.totqty = dtemp.ctnqty
	ctnsum   = ctnsum + m.totqty
	m.style  = dtemp.STYLE
	m.color  = dtemp.COLOR
	m.id     = dtemp.ID
	m.pack   = dtemp.PACK
	m.echo   = "UPC*"+ALLTRIM(dtemp.upc)+CHR(13)
	m.echo   = m.echo+"REFERENCE*"+dtemp.ponum
	m.accountid = tacct
	INSERT INTO xpl FROM MEMVAR
	REPLACE xinwolog.plunitsinqty WITH xinwolog.plunitsinqty+(dtemp.ctnqty*VAL(TRANSFORM(PACK))) IN xinwolog
	REPLACE xinwolog.plinqty WITH ctnsum IN xinwolog
	REPLACE xinwolog.plopenqty WITH ctnsum IN xinwolog
*	plidvalue = plidvalue +1
ENDSCAN

**************  OK now the Import  **************************888

SELECT xinwolog
LOCATE
IF lTesting
	BROWSE
ENDIF

SCAN  FOR !DELETED()  && Currently in XINWOLOG table
	cContainer = ALLT(xinwolog.CONTAINER)
	cCtnQty = ALLT(STR(xinwolog.plinqty))
	cUnitQty = ALLT(STR(xinwolog.plunitsinqty))

*Select inwolog
	cOffice = "M"

	SELECT xinwolog
	SCATTER MEMVAR MEMO
	SELECT inwolog

	m.inwologid =dygenpk("INWOLOG",IIF(lTesting,"whp","whm"))
	m.wo_num    =dygenpk("WONUM","whm")
	cWO_Num = ALLT(STR(m.wo_num))
	m.wo_date = DATE()
	m.addby = "TGF-PROC"
	m.adddt = DATETIME()
	m.addproc = "NAUT856"

	IF !lTesting
		insertinto("inwolog","wh",.T.)
		tu("inwolog")
	ENDIF

	SELECT xpl
	SCAN FOR xpl.inwologid = xinwolog.inwologid
		SCATTER MEMVAR MEMO
		m.addby = "TGF-PROC"
		m.adddt = DATETIME()
		m.addproc = "NAUT856"
		m.inwologid = inwolog.inwologid
		m.wo_num = inwolog.wo_num
		m.date_rcvd = DATE()
		m.office = cOffice
		m.mod = cOffice
		IF lTesting
			m.plid = dygenpk("PL","whp")
			INSERT INTO pl FROM MEMVAR
*			SET STEP ON
		ELSE
			insertinto("pl","wh",.T.)
		ENDIF
	ENDSCAN
	IF !lTesting
		tu("pl")
	ENDIF

*  Select detail  && make sure we update the ctnucc tabel with the inwonum per shipid........pg 04/15/2013
*  replace detail.inwonum with inwolog.wo_num  for container= whichcontainer and accountid = whichaccount and shipid = xinwolog.brokerref

	DELETE NEXT 1 IN xinwolog  && Added to eliminate duplicated header records due to no pointer movement in XINWOLOG, JOEB, 12.29.2016
ENDSCAN

*!*  Select detail
*!*  replace detail.inwonum with inwolog.wo_num  for container= whichcontainer and accountid = whichaccount



