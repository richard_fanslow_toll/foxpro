*!* tjmaxx861_create.prg (new EDI document), for TJX Corp.
*!* Created 06.15.2015, Joe from tjmaxx212outbound_create.prg

parameters nwo_num,ctype

set asserts off

assert .f. message "At start of TJMAXX 861 process"

public lc861type

lc861type = ctype

try

	public c_cntrlnum,c_grpcntrlnum,cwo_num,cgroupname,ccustname,cfilename,cprgname
	public tsendto,tcc,tsendtoerr,tccerr,cdelname,cmacysnet,ctripid,cmailname,ltestfile

	cprgname = "TJX 861"

	ltesting = .f.
*  lTestfile = .F. && Remove this 'lTestfile' variable when testing of the edi_trigger records is complete
	do m:\dev\prg\_setvars with ltesting
	on escape cancel

	closefiles()
	store "" to cfilename,cship_ref,cleg,cfileinfo

	liserror = .f.
	ldocatch = .t.
	lemail = !ltesting
	lemail = .t.
	ldoerror = .f.
	lfilesout = !ltesting

	nacctnum = 6565
	coffice = "I"

	if vartype(nwo_num)= "L"
		if ltesting
			close databases all
			lfilesout = .f.
			nwo_num = 1665472
			ctype = "A"
			dusedate = date()
		else
			cmsg = "No params specified"
			wait window cmsg timeout 2
			do ediupdate with cmsg,.t.
		endif
	endif
	cwo_num = alltrim(str(nwo_num))
	wait window "Now processing WO# "+alltrim(str(nwo_num))+", Type "+ctype nowait && Timeout 2

	xreturn = "XXX"
	do m:\dev\prg\wf_alt with coffice,nacctnum
	if empty(alltrim(xreturn))
		do ediupdate with "NO WHSE",.t.
		throw
	endif
	cusefolder = upper(xreturn)

	dimension thisarray(1)
	tfrom = "TGF EDI Operations <toll-edi-ops@tollgroup.com>"

	select 0
	if used('mm')
		use in mm
	endif
	if used('mailmaster')
		use in mailmaster
	endif
	use f:\3pl\data\mailmaster alias mm
	locate for mm.edi_type = "MISC" and mm.taskname = "JOETEST"
	tsendto = iif(mm.use_alt,mm.sendtoalt,mm.sendto)
	tcc = iif(mm.use_alt,mm.ccalt,mm.cc)
	locate
	locate for mm.edi_type = "MISC" and mm.taskname = "GENERAL"
	tsendtoerr = iif(mm.use_alt,mm.sendtoalt,mm.sendto)
	tccerr = iif(mm.use_alt,mm.ccalt,mm.cc)
	use in mm
	tattach = ""

	ccustname = "TJX"
	ccustfolder = "TJMAX"
	ccustprefix = "TJX861"+lower(coffice)+"_"+ctype
	dtmail = ttoc(datetime())
	dt2 = datetime()
	cmailname = proper(ccustname)

	lcpath = "f:\ftpusers\"+ccustfolder+"\temp\"
	cd &lcpath

*!* Open Tables
	if used('wolog')
		use in wolog
	endif
	use f:\wo\wodata\wolog in 0 alias wolog shared

	xsqlexec("select * from dellocs",,,"wo")
	index on str(accountid,4)+location tag acct_loc
	set order to

	if used('availlog')
		use in availlog
	endif

	if used('detail')
		use in detail
	endif

	use f:\wo\wodata\detail in 0

	if used('tjxdetail')
		use in tjxdetail
	endif
	use f:\tjmax\data\tjxdetail in 0

	cst_name = "TGF INC."
	do case
	case coffice = "C"
		cfmicity = "SAN PEDRO"
		cfmistate = "CA"
	case coffice = "M"
		cfmicity = "MIAMI"
		cfmistate = "FL"
	otherwise
		cfmicity = "CARTERET"
		cfmistate = "NJ"
	endcase

	cterminator = ">"  && Used at end of ISA segment
	cfd = "*"  && Field delimiter
	csegd = "~" && Segment delimiter

	csendqual = "ZZ"  && Sender qualifier
	csendid = "TGFUSA"  && Sender ID code
	crecqual = "12"  && Recip qualifier
	crecid = "5084758255"   && Recip ID Code
*crecid = Iif(lTesting Or lTestfile,crecid+"T",crecid)

	cdate = dtos(date())
	ctruncdate = right(cdate,6)
	ctrunctime = substr(ttoc(datetime(),1),9,4)
	cfiledate = cdate+ctrunctime
	csendidlong = padr(csendid,15," ")
	crecidlong = padr(crecid,15," ")
	cstring = ""

	nstcount = 0
	nsegctr = 0
	nlxnum = 1
*  cISACode = Iif(lTesting Or lTestfile,"T","P")
	cisacode = "P"

	wait clear
	wait window "Now creating WOLOG WO#-based 861 information..." nowait
	select wolog
	set order to tag wo_num
	locate
	locate for wolog.wo_num = nwo_num
	if !found()
		throw
	endif

*!*    IF !SEEK(nWO_Num,"wolog","wo_num")
*!*      THROW
*!*    ENDIF

	select dellocs
	locate for accountid = 6565 and location=wolog.delloc

	lclocid = dellocs.label
	whichshipto = dellocs.acct_name


*!* More Variables

	dt1 = ttoc(datetime(),1)
	cfilename = (ccustprefix+dt1+".edi")

	cfilenameshort = justfname(cfilename)
	cfilenamearchive = ("f:\ftpusers\TJMAX\861-staging\"+cfilenameshort)  && added this 9/28/2015, AS2 makes copies in the OUT archive
	cfilenameout = ("f:\ftpusers\TJMAX\OUT\"+cfilenameshort)

	nhandle = fcreate(cfilename)

	do num_incr_isa
	cisa_num = padl(c_cntrlnum,9,"0")

	store "ISA"+cfd+"00"+cfd+"          "+cfd+"00"+cfd+"          "+cfd+csendqual+cfd+csendidlong+cfd+;
	crecqual+cfd+crecidlong+cfd+ctruncdate+cfd+ctrunctime+cfd+"U"+cfd+"00401"+cfd+;
	cisa_num+cfd+"0"+cfd+cisacode+cfd+cterminator+csegd to cstring
	do cstringbreak

	store "GS"+cfd+"RC"+cfd+csendid+cfd+crecid+cfd+cdate+cfd+ctrunctime+cfd+c_cntrlnum+;
	cfd+"X"+cfd+"004010"+csegd to cstring
	do cstringbreak

	do num_incr_st
	store "ST"+cfd+"861"+cfd+padl(c_grpcntrlnum,9,"0")+csegd to cstring
	do cstringbreak
	nstcount = nstcount + 1
	nsegctr = nsegctr + 1

	nlxnum = 1
	clxnum = alltrim(str(nlxnum))
*cLXNum = PADR(ALLTRIM(STR(nLXNum)),4,"0")

*!*    Detail Information Loop

	if ltesting
*  SET STEP ON
	endif

	select wolog
	scan for wolog.wo_num = nwo_num
*  cPONum =  STRTRAN(ALLTRIM(wolog.po_num),".","")
		ccontainer = alltrim(wolog.container)
		cdeconcode = "996"  && Must determine where this code will be extracted from in production

		store "BRA"+cfd+alltrim(wolog.container)+cfd+dtoc(date(),1)+cfd+"00"+cfd+"1"+csegd to cstring
		do cstringbreak
		nsegctr = nsegctr + 1


** added this saildate code 10/30/2015, just like in the outbound 856,  PG

		select detail
		locate for wo_num = wolog.wo_num
		if found()
			lcsaildate=getmemodata("detail.suppdata","SAILDATE")
		else
			lcsaildate ="NA"
		endif

		if lcsaildate = "NA"
			select tjxdetail
			locate for container = wolog.container and tjxdetail.wo_date >= date()-90
			if found()
				lcsaildate=getmemodata("tjxdetail.suppdata","SAILDATE")  && look in the holding bin for the sail date if not uploaed into Detail
				if lcsaildate = "NA"
					tsubject = "TJX 861 Creation Error at "+ttoc(datetime())+"  No SailDate"
					xerrormsg=tsubject
					tmessage = xerrormsg
					do form dartmail2 with tsendto,tfrom,tsubject,tcc,tattach,tmessage,"A"
					ediupdate(xerrormsg,.t.)
					throw
				endif
			else

			endif
		endif
		store "DTM*370*"+alltrim(lcsaildate)+csegd to cstring
		do cstringbreak
		nsegctr = nsegctr + 1

		store "DTM"+cfd+"057"+cfd+dtoc(wolog.wo_date,1)+csegd to cstring  && WO Date, but should be the Saildate from the 856 data
		do cstringbreak
		nsegctr = nsegctr + 1


		if ctype = "861A"
			xsqlexec("select * from availlog where wo_num="+transform(nwo_num)+" and remarks='AVAILABLE'",,,"wo")
			if found()
				lcdate = nul2empty(avail)
			else
				lcdate=date()
			endif

			store "DTM"+cfd+"372"+cfd+dtoc(lcdate,1)+csegd to cstring  && Container available
			do cstringbreak
			nsegctr = nsegctr + 1
			select wolog
		endif

		if ctype = "861P"
			lcdate = wolog.pickedup
			store "DTM"+cfd+"140"+cfd+dtoc(lcdate,1)+csegd to cstring  && Pickup date
			do cstringbreak
			nsegctr = nsegctr + 1
			select wolog
		endif

		store "TD3"+cfd+cfd+left(alltrim(wolog.container),4)+cfd+substr(alltrim(wolog.container),5)+csegd to cstring
		do cstringbreak
		nsegctr = nsegctr + 1

		store "N1"+cfd+"DS"+cfd+cfd+"92"+cfd+cdeconcode+csegd to cstring
		do cstringbreak
		nsegctr = nsegctr + 1

		store "CTT"+cfd+"1"+csegd to cstring
		do cstringbreak
		nsegctr = nsegctr + 1

	endscan

	close861()
	lclosedfile=fclose(nhandle)

	if lclosedfile and ltesting
		wait window "TJM 861 File closed successfully" timeout 0.5
*    MODIFY FILE &cFilename
		release nhandle
	endif

	do ediupdate with "TJM 861 CREATED",.f.

	if lfilesout
		copy file &cfilename to &cfilenamearchive && leave the file in the staging folder ffor the archive
		copy file &cfilename to &cfilenameout
		delete file &cfilename
	endif

	if !ltesting
		if !used("ftpedilog")
			select 0
			use f:\edirouting\ftpedilog alias ftpedilog
			insert into ftpedilog (transfer,ftpdate,filename,acct_name,type) ;
			values ("861-"+ccustname,dt2,cfilename,upper(ccustname),"861")
			use in ftpedilog
		endif
	endif

*!* Finish processing
	if ltesting or ltestfile
		tsubject = "TJMAX 861 *TEST* EDI from TGF as of "+dtmail
	else
		tsubject = "TJMAX 861 EDI from TGF as of "+dtmail
	endif

	tmessage = cmailname+" 861 EDI FILE (TGF WOLOG WO# "+cwo_num+")"
	tmessage = tmessage+chr(13)+"has been created."
	if ltesting or ltestfile
		tmessage = tmessage + chr(13)+chr(13)+"This is a TEST RUN..."
	endif

	if lemail
*   Set Step On
		do form dartmail2 with tsendto,tfrom,tsubject,tcc,tattach,tmessage,"A"
	endif
	release all like c_cntrlnum,c_grpcntrlnum
	release all like norigseq,noriggrpseq
	wait clear
	wait window cmailname+" 861 EDI File output complete" timeout 1
	closefiles()

catch to oerr
	if ldocatch
		assert .f. message "In CATCH...debug"
		lemail = .t.
		do ediupdate with "TRY/CATCH ERROR",.t.
		tsubject = cmailname+" 861 Error ("+transform(oerr.errorno)+") at "+ttoc(datetime())
		tattach  = ""
		tmessage = ccustname+" Error processing "+chr(13)
		tmessage = tmessage+trim(program())+chr(13)

		tmessage =tmessage+ "Try/CATCH Exeception Message:"+chr(13)+;
		[  Error: ] + str(oerr.errorno) +chr(13)+;
		[  LineNo: ] + str(oerr.lineno) +chr(13)+;
		[  Message: ] + oerr.message +chr(13)+;
		[  Procedure: ] + oerr.procedure +chr(13)+;
		[  Details: ] + oerr.details +chr(13)+;
		[  StackLevel: ] + str(oerr.stacklevel) +chr(13)+;
		[  LineContents: ] + oerr.linecontents+chr(13)+;
		[  UserValue: ] + oerr.uservalue

		tsubject = "861 Process Error at "+ttoc(datetime())
		tattach  = ""
		tcc=""
		tfrom    ="TGF EDI 861 Process Ops <fmi-warehouse-ops@fmiint.com>"
		do form dartmail2 with tsendtoerr,tfrom,tsubject,tccerr,tattach,tmessage,"A"
		lemail = .f.
	endif
finally
	closefiles()
endtry

&& END OF MAIN CODE SECTION

**************************
procedure ediupdate
**************************
parameters cfin_status,liserror
if !ltesting
*  Assert .F. Message "In EDIUPDATE"
	ldocatch = .f.
	select edi_trigger
	locate
	if liserror
		replace edi_trigger.processed with .t.,edi_trigger.created with .f.,;
		edi_trigger.fin_status with cfin_status,edi_trigger.errorflag with .t.;
		edi_trigger.when_proc with datetime() ;
		for edi_trigger.wo_num = nwo_num and edi_trigger.edi_type = lc861type
		if file(cfilename)
			delete file &cfilename
		endif
		closefiles()
		errormail(cfin_status)
		throw
	else
		replace edi_trigger.processed with .t.,edi_trigger.created with .t.,;
		edi_trigger.when_proc with datetime(),;
		edi_trigger.fin_status with "861 CREATED";
		edi_trigger.errorflag with .f.,file214 with cfilename ;
		for edi_trigger.wo_num = nwo_num and edi_trigger.edi_type =lc861type
	endif
endif
endproc

****************************
procedure errormail
****************************
parameters cfin_status
ldocatch = .f.

assert .f. message "At Errormail...Debug"
tsubject = ccustname+" 861 EDI File Error"
tmessage = "861 EDI File Error, WO# "+cwo_num+chr(13)
tmessage = tmessage+chr(13)+chr(13)+"Error: "+cfin_status

if ltesting or ltestfile
	tmessage = tmessage + chr(10)+chr(10)+"This is TEST run..."
endif
if lemail
	do form dartmail2 with tsendto,tfrom,tsubject,tcc,tattach,tmessage,"A"
endif
endproc

*********************
procedure closefiles
*********************
if used('wolog')
	use in wolog
endif
if used('ftpedilog')
	use in ftpedilog
endif
if used('serfile')
	use in serfile
endif
endproc

****************************
procedure num_incr_isa
****************************

if !used("serfile")
	use ("F:\3pl\data\serial\tjx_serial") in 0 alias serfile
endif
select serfile
if ltesting
	norigseq = serfile.seqnum
	noriggrpseq = serfile.grpseqnum
	lisaflag = .f.
endif
nisa_num = serfile.seqnum
c_cntrlnum = alltrim(str(serfile.seqnum))
replace serfile.seqnum with serfile.seqnum + 1 in serfile
endproc

****************************
procedure num_incr_st
****************************

if !used("serfile")
	use ("F:\3pl\data\serial\tjx_serial") in 0 alias serfile
endif
select serfile
if ltesting
	norigseq = serfile.seqnum
	noriggrpseq = serfile.grpseqnum
	lstflag = .f.
endif
c_grpcntrlnum = alltrim(str(serfile.grpseqnum))
replace serfile.grpseqnum with serfile.grpseqnum + 1 in serfile
endproc

****************************
procedure segmentget
****************************

parameter thisarray,lckey,nlength

for i = 1 to nlength
	if i > nlength
		exit
	endif
	lnend= at("*",thisarray[i])
	if lnend > 0
		lcthiskey =trim(substr(thisarray[i],1,lnend-1))
		if occurs(lckey,lcthiskey)>0
			return substr(thisarray[i],lnend+1)
			i = 1
		endif
	endif
endfor

return ""

****************************
procedure close861
****************************
store  "SE"+cfd+alltrim(str(nsegctr+1))+cfd+padl(c_grpcntrlnum,9,"0")+csegd to cstring
do cstringbreak

store  "GE"+cfd+alltrim(str(nstcount))+cfd+c_cntrlnum+csegd to cstring
do cstringbreak

store  "IEA"+cfd+"1"+cfd+padl(c_cntrlnum,9,"0")+csegd to cstring
do cstringbreak

endproc


****************************
procedure cstringbreak
****************************
clen = len(alltrim(cstring))
=fputs(nhandle,cstring)
endproc
