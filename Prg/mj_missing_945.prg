**This script identifies when a 945 has not been created for a PT.  Therefore MJ has not invoiced that PT

utilsetup("MJ_MISSING_945")
Set Exclusive Off
Set Deleted On
Set Talk Off
Set Safety Off 

WITH _SCREEN	
	.AUTOCENTER = .T.
	.WINDOWSTATE = 0
	.BORDERSTYLE = 1
	.WIDTH = 320
	.HEIGHT = 210
	.TOP = 200
	.LEFT = 100
	.CLOSABLE = .T.
	.MAXBUTTON = .T.
	.MINBUTTON = .T.
	.SCROLLBARS = 0
	.CAPTION = "MJ_MISSING_945"
ENDWITH	

goffice="J"

If !Used("edi_trigger")
  use f:\3pl\data\edi_trigger shared In 0
ENDIF

xsqlexec("select accountid, wo_num, ship_ref from outship where mod='"+goffice+"' " + ;
	"and inlist(accountid,6303,6304,6305,6320,6321,6322,6323,6325,6543) " + ;
	"and delenterdt>{"+dtoc(date()-20)+"} and delenterdt<{"+ttoc(datetime()-1290)+"}","temp945",,"wh")

SELECT DISTINCT ACCOUNTID,WO_NUM,SHIP_REF,PROCESSED,ERRORFLAG,FIN_STATUS,BOL FROM EDI_TRIGGER WHERE EDI_TYPE='945 ' AND  ;
 INLIST(accountid,6303, 6304, 6305,6320, 6321, 6322,6323,6325,6543);
 INTO CURSOR TEMP21
SELECT * FROM temp945 t LEFT JOIN TEMP21 e ON t.accountid=e.accountid AND t.wo_num=e.wo_num AND t.ship_ref=e.ship_ref  INTO CURSOR temp22
SELECT * FROM TEMP22 WHERE ISNULL(ACCOUNTID_B) INTO CURSOR TEMP23
SELECT  TEMP23
If Reccount() > 0
  Export To "S:\MarcJacobsData\945_missing\missing_945_trigger"  Type Xls
  tsendto = "todd.margolin@tollgroup.com"
  tattach = "S:\MarcJacobsData\945_missing\missing_945_trigger.xls"
  tcc =""
  tFrom ="Toll/FMI EDI System Operations <fmi-transload-ops@fmiint.com>"
  tmessage = "MISSING_945_trigger "+Ttoc(Datetime())
  tSubject = "MISSING_945 In Trigger table, CHECK POLLER"
  Do Form m:\dev\frm\dartmail2 With tsendto,tFrom,tSubject, tcc,tattach,tmessage  ,"A"
ELSE 
  tsendto = "tmarg@fmiint.com"
  tattach = ""
  tcc =""
  tFrom ="Toll/FMI EDI System Operations <fmi-transload-ops@fmiint.com>"
  tmessage = "NO_MISSING_945_IN TRIGGER TABLE"+Ttoc(Datetime())
  tSubject = "NO_MISSING_945_IN TRIGGER TABLE exist"
  Do Form m:\dev\frm\dartmail2 With tsendto,tFrom,tSubject, tcc,tattach,tmessage  ,"A"
ENDIF

SET STEP ON 
** IDENTIFIES UNPROCESSED  945 RECORDS IN EDI TRIGGER
SELECT * FROM temp22 WHERE EMPTY(fin_status)INTO CURSOR temp24 READWRITE
SELECT temp24
If Reccount() > 0
  Export To "S:\MarcJacobsData\945_missing\945_not_processed_in_edi_trigger"  Type Xls

  tsendto = "todd.margolin@tollgroup.com"
  tattach = "S:\MarcJacobsData\945_missing\945_not_processed_in_edi_trigger.xls"
  tcc =""
  tFrom ="Toll/FMI EDI System Operations <fmi-transload-ops@fmiint.com>"
  tmessage = "945 Not processed In Trigger table_"+Ttoc(Datetime())
  tSubject = "945 Not processed In Trigger table, CHECK POLLER"
  Do Form m:\dev\frm\dartmail2 With tsendto,tFrom,tSubject, tcc,tattach,tmessage  ,"A"
ELSE 
  tsendto = "tmarg@fmiint.com"
  tattach = ""
  tcc =""
  tFrom ="Toll/FMI EDI System Operations <fmi-transload-ops@fmiint.com>"
  tmessage = "NO_945 unprocessed In Trigger table_"+Ttoc(Datetime())
  tSubject = "NO_945 unprocessed In Trigger table_exist"
  Do Form m:\dev\frm\dartmail2 With tsendto,tFrom,tSubject, tcc,tattach,tmessage  ,"A"
ENDIF

SET STEP ON 
SELECT * FROM temp22 WHERE ERRORFLAG INTO CURSOR temp25 READWRITE
SELECT temp25
If Reccount() > 0
  Export To "S:\MarcJacobsData\945_missing\945_with_errors"  Type Xls

  tsendto = "todd.margolin@tollgroup.com"
  tattach = "S:\MarcJacobsData\945_missing\945_with_errors.xls"
  tcc =""
  tFrom ="Toll/FMI EDI System Operations <fmi-transload-ops@fmiint.com>"
  tmessage = "945 WITH ERRORS_"+Ttoc(Datetime())
  tSubject = "945 WITH ERRORS"
  Do Form m:\dev\frm\dartmail2 With tsendto,tFrom,tSubject, tcc,tattach,tmessage  ,"A"
ELSE 
  tsendto = "tmarg@fmiint.com"
  tattach = ""
  tcc =""
  tFrom ="Toll/FMI EDI System Operations <fmi-transload-ops@fmiint.com>"
  tmessage = "NO_945's with errors exist_"+Ttoc(Datetime())
  tSubject = "NO_945's with errors exist"
  Do Form m:\dev\frm\dartmail2 With tsendto,tFrom,tSubject, tcc,tattach,tmessage  ,"A"
ENDIF


*******************************************MIRA LOMA
Close Data All

goffice="L"

If !Used("edi_trigger")
  use f:\3pl\data\edi_trigger shared In 0
ENDIF

xsqlexec("select accountid, wo_num, ship_ref, bol_no from outship where mod='"+goffice+"' " + ;
	"and inlist(accountid,6303,6304,6305,6320,6321,6322,6323,6325,6543) " + ;
	"and delenterdt>{"+dtoc(date()-20)+"} and delenterdt<{"+ttoc(datetime()-1290)+"}","temp945",,"wh")

delete for INLIST(ship_ref,'130045812','130045813','130045814') 
delete for INLIST(bol_no,'04070086303073122')
	 
SELECT DISTINCT ACCOUNTID,WO_NUM,SHIP_REF,PROCESSED,ERRORFLAG,FIN_STATUS,BOL FROM EDI_TRIGGER WHERE EDI_TYPE='945 ' AND TRIG_TIME>=DATE()-60 AND ;
 INLIST(accountid,6303, 6304, 6305,6320, 6321, 6322,6323,6325,6543);
 AND !INLIST(ship_ref,'130045812','130045813','130045814') AND !INLIST(bol,'04070086303073122') INTO CURSOR TEMP21
SELECT * FROM temp945 t LEFT JOIN TEMP21 e ON t.accountid=e.accountid AND t.wo_num=e.wo_num AND t.ship_ref=e.ship_ref  INTO CURSOR temp22
SELECT * FROM TEMP22 WHERE ISNULL(ACCOUNTID_B) INTO CURSOR TEMP23
SELECT  TEMP23

set step on

If Reccount() > 0
  Export To "S:\MarcJacobsData\945_missing\missing_945_trigger"  Type Xls

  tsendto = "tmarg@fmiint.com,todd.margolin@tollgroup.com"
  tattach = "S:\MarcJacobsData\945_missing\missing_945_trigger.xls"
  tcc =""
  tFrom ="Toll/FMI EDI System Operations <fmi-transload-ops@fmiint.com>"
  tmessage = "MISSING_945_trigger "+Ttoc(Datetime())
  tSubject = "MISSING_945 In Trigger table, CHECK POLLER"
  Do Form m:\dev\frm\dartmail2 With tsendto,tFrom,tSubject, tcc,tattach,tmessage  ,"A"
ELSE 
  tsendto = "tmarg@fmiint.com"
  tattach = ""
  tcc =""
  tFrom ="Toll/FMI EDI System Operations <fmi-transload-ops@fmiint.com>"
  tmessage = "NO_MISSING_945_IN TRIGGER TABLE"+Ttoc(Datetime())
  tSubject = "NO_MISSING_945_IN TRIGGER TABLE exist"
  Do Form m:\dev\frm\dartmail2 With tsendto,tFrom,tSubject, tcc,tattach,tmessage  ,"A"
ENDIF

SET STEP ON 
** IDENTIFIES UNPROCESSED  945 RECORDS IN EDI TRIGGER
SELECT * FROM temp22 WHERE EMPTY(fin_status)INTO CURSOR temp24 READWRITE
SELECT temp24
If Reccount() > 0
  Export To "S:\MarcJacobsData\945_missing\945_not_processed_in_edi_trigger"  Type Xls

  tsendto = "todd.margolin@tollgroup.com"
  tattach = "S:\MarcJacobsData\945_missing\945_not_processed_in_edi_trigger.xls"
  tcc =""
  tFrom ="Toll/FMI EDI System Operations <fmi-transload-ops@fmiint.com>"
  tmessage = "945 Not processed In Trigger table_"+Ttoc(Datetime())
  tSubject = "945 Not processed In Trigger table, CHECK POLLER"
  Do Form m:\dev\frm\dartmail2 With tsendto,tFrom,tSubject, tcc,tattach,tmessage  ,"A"
ELSE 
  tsendto = "tmarg@fmiint.com"
  tattach = ""
  tcc =""
  tFrom ="Toll/FMI EDI System Operations <fmi-transload-ops@fmiint.com>"
  tmessage = "NO_945 unprocessed In Trigger table_"+Ttoc(Datetime())
  tSubject = "NO_945 unprocessed In Trigger table_exist"
  Do Form m:\dev\frm\dartmail2 With tsendto,tFrom,tSubject, tcc,tattach,tmessage  ,"A"
ENDIF

SET STEP ON 
SELECT * FROM temp22 WHERE ERRORFLAG INTO CURSOR temp25 READWRITE
SELECT temp25
If Reccount() > 0
  Export To "S:\MarcJacobsData\945_missing\945_with_errors"  Type Xls

  tsendto = "todd.margolin@tollgroup.com"
  tattach = "S:\MarcJacobsData\945_missing\945_with_errors.xls"
  tcc =""
  tFrom ="Toll/FMI EDI System Operations <fmi-transload-ops@fmiint.com>"
  tmessage = "945 WITH ERRORS_"+Ttoc(Datetime())
  tSubject = "945 WITH ERRORS"
  Do Form m:\dev\frm\dartmail2 With tsendto,tFrom,tSubject, tcc,tattach,tmessage  ,"A"
ELSE 
  tsendto = "tmarg@fmiint.com"
  tattach = ""
  tcc =""
  tFrom ="Toll/FMI EDI System Operations <fmi-transload-ops@fmiint.com>"
  tmessage = "NO_945's with errors exist_"+Ttoc(Datetime())
  tSubject = "NO_945's with errors exist"
  Do Form m:\dev\frm\dartmail2 With tsendto,tFrom,tSubject, tcc,tattach,tmessage  ,"A"
ENDIF

Close Data All
schedupdate()
_Screen.Caption=gscreencaption