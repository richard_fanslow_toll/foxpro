lparameters xaccountid, xstyle, xcolor, xid, xpack, xunitqty, xfromloc, xtoloc, xctnpack, xctnqty, xpalletid, xnoinven

m.accountid=xaccountid
if !empty(xpalletid)
	m.palletid=xpalletid
endif

xwonumadd=0
xidadd=0
xpoadd=""
m.wo_num=0
m.inwologid=0

ppadjinwo(xaccountid, xunitqty, xctnqty)

m.accountid=xaccountid

* adjust out units

m.adjdt=qdate()
m.comment="P&P UNITS CONVERTED TO CARTONS"
gnocalc=.t.
m.units=.t.
m.style=xstyle
m.color=xcolor
m.id=xid
m.pack=xpack
m.totqty=-xunitqty
m.removeqty=0
m.remainqty=m.totqty
m.inadj=(m.totqty>0)
m.whseloc=xfromloc
m.addby=''
m.adddt={}

insertinto("adj","wh",.t.)

gnocalc=.f.

if inlist(m.whseloc,"CYCLE ","CYCLEU")
	email("Dyoung@fmiint.com","INFO: CYCLE location added to ADJ in unit2ctn",,,,,.t.,,,,,.t.,,.t.)
endif

invenadj(xnoinven)

* add cartons to inbound

m.po=xpoadd
m.date_rcvd=qdate()
m.pack=padr(transform(xctnpack),10)
m.units=.f.
m.totqty=xctnqty
m.removeqty=0
m.remainqty=m.totqty
store 9 to m.ctnwt, m.length, m.width, m.depth
m.ctncube=.42
m.totwt=m.totqty*m.ctnwt
m.totcube=m.totqty*m.ctncube

insertinto("indet","wh",.t.)
m.indetid=indet.indetid

if !seek(str(m.accountid,4)+trans(m.units)+m.style+m.color+m.id+m.pack,"inven","match")
	m.availqty=m.totqty
	insertinto("inven","wh",.t.)
	m.invenid=inven.invenid
else
	m.invenid=inven.invenid
	replace totqty with inven.totqty+m.totqty, availqty with inven.totqty-inven.allocqty-inven.holdqty in inven
endif

m.whseloc=xtoloc
m.locqty=m.totqty

if !indet.units and seek(str(accountid,4)+".F."+indet.style+indet.color+indet.id+indet.pack+m.whseloc,"invenloc","matchwh")	
	if !empty(invenloc.palletid)
		m.palletid=invenloc.palletid
	else
		if type("m.palletid")="C" and !empty(m.palletid)
			replace palletid with m.palletid in invenloc
		endif
	endif
endif

insertinto("inloc","wh",.t.)

m.hold=.f.

if !seek(str(m.accountid,4)+trans(m.units)+m.style+m.color+m.id+m.pack+m.whseloc,"invenloc","matchwh")
	if m.invenid=0
		email("Dyoung@fmiint.com","INFO: invenid=0 when inserting invenloc B",,,,,.t.,,,,,.t.,,.t.)
	endif
	insertinto("invenloc","wh",.t.)
else
	replace locqty with invenloc.locqty+m.locqty in invenloc
endif	

if holdloc(m.whseloc)
	replace hold with .t., holdtype with gholdtype in invenloc
	replace holdqty with inven.holdqty+m.locqty, availqty with inven.totqty-inven.allocqty-inven.holdqty in inven
	pnphold(m.locqty)
endif

* add units to inbound

m.totqty=xunitqty
m.pack="1         "
m.units=.t.
m.removeqty=0
m.remainqty=m.totqty
store 0 to m.ctnwt, m.length, m.width, m.depth, m.ctncube, m.totcube, m.totwt
insertinto("indet","wh",.t.)
m.indetid=indet.indetid

if !seek(str(m.accountid,4)+trans(m.units)+m.style+m.color+m.id+m.pack,"inven","match")
	m.availqty=m.totqty
	insertinto("inven","wh",.t.)
	m.invenid=inven.invenid
else
	m.invenid=inven.invenid
	replace totqty with inven.totqty+m.totqty, availqty with inven.totqty-inven.allocqty-inven.holdqty in inven
endif

m.whseloc="RACK"
m.locqty=m.totqty
insertinto("inloc","wh",.t.)

if !seek(str(m.accountid,4)+trans(m.units)+m.style+m.color+m.id+m.pack+m.whseloc,"invenloc","matchwh")
	if m.invenid=0
		email("Dyoung@fmiint.com","INFO: invenid=0 when inserting invenloc B",,,,,.t.,,,,,.t.,,.t.)
	endif
	insertinto("invenloc","wh",.t.)
else
	replace locqty with invenloc.locqty+m.locqty in invenloc
endif	
