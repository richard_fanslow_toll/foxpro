* FTE Info about Quality NJ temps for Mariel Calello.
* EXEs go in F:\UTIL\KRONOS\

utilsetup("KRONOSTEMPINFORPT")

LOCAL loKRONOSTEMPINFORPT

SET PROCEDURE TO M:\DEV\PRG\KRONOSCLASSLIBRARY ADDITIVE

loKRONOSTEMPINFORPT = CREATEOBJECT('KRONOSTEMPINFORPT')
loKRONOSTEMPINFORPT.MAIN()

schedupdate()

CLOSE DATABASES ALL
RETURN

#DEFINE LOGIT 1
#DEFINE WAITIT 2
#DEFINE NOWAITIT 4
#DEFINE SENDIT 8
#DEFINE CRLF CHR(13) + CHR(10)
#DEFINE LF CHR(10)
#DEFINE RETURN_DATA_MANDATORY .F.
#DEFINE RETURN_DATA_NOT_MANDATORY .T.
#DEFINE DAILY_OT_MULTIPLIER 1.5
#DEFINE DAILY_OT20_MULTIPLIER 2.0

DEFINE CLASS KRONOSTEMPINFORPT AS CUSTOM

	cProcessName = 'KRONOSTEMPINFORPT'

	lTestMode = .T.  && .F. FOR NORMAL OPERATION, .T. TO SUPPRESS ALL BUT DEVELOPER EMAILS AND CHANGE INPUT TABLE, ETC.

	lAutoYield = .T.

	cStartTime = TTOC(DATETIME())
	
	* connection properties
	nSQLHandle = 0

	* file properties
	nFileHandle = 0

	* date properties
	dtNow = DATETIME()

	dToday = DATE()
	* to force different pay weeks
	*dToday = {^2009-05-26}

	cToday = DTOC(DATE())
	
	* wait window properties
	nWaitWindowTimeout = 2
	lWaitWindowIsOn = .T.
	
	* obj properties
	oTempAgency = NULL

	* logfile properties
	lLoggingIsOn = .T.
	cLogFile = 'F:\UTIL\KRONOS\Logfiles\KRONOSTEMPINFORPT_log.txt' 

	* INTERNAL email properties
	lSendInternalEmailIsOn = .T.
	cFrom = 'FMI Corporate <fmicorporate@fmiint.com>'
	cSendTo = 'mbennett@fmiint.com'
	cCC = ''
	cSubject = ''
	cAttach = ''
	cBodyText = ''

	cCOMPUTERNAME = ''
	cUSERNAME = ''

	FUNCTION INIT
		IF NOT DODEFAULT()
			RETURN .F.
		ENDIF
		WITH THIS
			CLEAR
			*SET RESOURCE OFF
			CLOSE DATA
			SET CENTURY ON
			SET DATE YMD
			SET HOURS TO 24
			SET ANSI ON
			SET TALK OFF
			SET DELETED ON
			SET CONSOLE OFF
			SET EXCLUSIVE OFF
			SET SAFETY OFF
			SET EXACT OFF
			SET STATUS BAR ON
			SET SYSMENU OFF
			SET ENGINEBEHAVIOR 70
			_VFP.AUTOYIELD = .lAutoYield
			.cCOMPUTERNAME = GETENV("COMPUTERNAME")
			.cUSERNAME = GETENV("USERNAME")
			IF .lTestMode THEN
				.cSendTo = 'Mark Bennett <mbennett@fmiint.com>'
				.cLogFile = 'F:\UTIL\KRONOS\Logfiles\KRONOSTEMPINFORPT_log_TESTMODE.txt'
			ENDIF
			.lLoggingIsOn = .lLoggingIsOn AND NOT EMPTY(.cLogFile)
			IF .lLoggingIsOn THEN
				SET ALTERNATE TO (.cLogFile) ADDITIVE
				SET ALTERNATE ON
			ENDIF
		ENDWITH
	ENDFUNC


	FUNCTION DESTROY
		WITH THIS
			IF .lLoggingIsOn  THEN
				SET ALTERNATE OFF
				SET ALTERNATE TO
			ENDIF
		ENDWITH
		DODEFAULT()
	ENDFUNC


	FUNCTION MAIN
		WITH THIS
			LOCAL lnNumberOfErrors, lcSQL, ldToday, lcSQLStartDateTimeString, lcSQLEndDateTimeString, lcTempAgencycode
			LOCAL llRetval1, ldStartDate, ldEndDate, lcAPPLYDTM, lcType, lnTempMarkupREG, lnTempMarkupOT, lnWage, lnWageAmt
			LOCAL lcStartDate, lcEndDate, i, lcHoursField, lcWagesField, lcTempCoName

			TRY
				lnNumberOfErrors = 0
				ldToday = .dToday

				*.TrackProgress('==================================================================================================================', LOGIT+SENDIT)
				.TrackProgress('Kronos Temp Info process started....', LOGIT+SENDIT)
				.TrackProgress('COMPUTERNAME = ' + .cCOMPUTERNAME, LOGIT+SENDIT)
				.TrackProgress('USERNAME = ' + .cUSERNAME, LOGIT+SENDIT)
				.TrackProgress('PROJECT = KRONOSTEMPINFORPT', LOGIT+SENDIT)
				IF .lTestMode THEN
					.TrackProgress('====>TEST MODE', LOGIT+SENDIT)
				ENDIF

				
				.cSubject = 'Kronos Temp Info Report for: ' + TRANSFORM(ldToday)
				
				lcTempAgencycode = 'D' && Quality NJ temps

				.oTempAgency = CREATEOBJECT('TempAgency',lcTempAgencycode)				
				lcType = TYPE('.oTempAgency')
				
				IF NOT (lcType == 'O') THEN
					.TrackProgress('====> ERROR instantiating Temp Agency class!', LOGIT+SENDIT)
					THROW
				ELSE
					lcTempCoName = .oTempAgency.GetTempCoName()
				ENDIF
				
				ldStartDate = {^2017-01-01}
				ldEndDate = {^2017-10-31}
				
				lcStartDate = DTOC(ldStartDate)
				lcEndDate = DTOC(ldEndDate)

				lcSQLStartDateTimeString = .GetSQLStartDateTimeString( ldStartDate )
				lcSQLEndDateTimeString = .GetSQLEndDateTimeString( ldEndDate )
				
				
				* get time data for date range - with reg/ot/dt hours broken out
				* add a Month field and populate it in the time data
				* get the list of temp associates who worked in the date range
				* ADD FIELDS for 12 MONTHS, $, Dept, last punch date
				* for each associate get
				*		hire date
				*		total hours worked per month
				*		total $ per month
				*		last dept worked in
				*		date of last punch
				*		last hourly wage


					
				SET TEXTMERGE ON
				TEXT TO lcSQL
SELECT
D.LABORLEV5DSC AS AGENCY,
C.FULLNM AS NAME,
D.LABORLEV3DSC AS DEPT,
C.PERSONNUM,
C.PERSONID,
C.SHORTNM,
A.EMPLOYEEID,
A.APPLYDTM,
(CASE E.NAME WHEN 'Regular Hours' THEN A.DURATIONSECSQTY ELSE 0.000 END) / 3600 AS REGHOURS,
(CASE E.NAME WHEN 'Overtime Hours' THEN A.DURATIONSECSQTY ELSE 0.000 END) / 3600 AS OTHOURS,
(CASE E.NAME WHEN 'Doubltime Hours' THEN A.DURATIONSECSQTY ELSE 0.000 END) / 3600 AS DBLHOURS, 
(A.DURATIONSECSQTY / 3600.00) AS TOTHOURS
FROM WFCTOTAL A
JOIN WTKEMPLOYEE B
ON B.EMPLOYEEID = A.EMPLOYEEID
JOIN PERSON C
ON C.PERSONID = B.PERSONID
JOIN LABORACCT D
ON D.LABORACCTID = A.LABORACCTID
JOIN PAYCODE E
ON E.PAYCODEID = A.PAYCODEID
WHERE (A.APPLYDTM >= <<lcSQLStartDateTimeString>>)
AND (A.APPLYDTM <= <<lcSQLEndDateTimeString>>)
AND (D.LABORLEV1NM = 'TMP')
AND (D.LABORLEV5NM = '<<lcTempAgencycode>>')
AND (A.DURATIONSECSQTY > 0.00)
AND E.NAME IN ('Regular Hours','Overtime Hours','Doubltime Hours')
ORDER BY C.FULLNM, C.PERSONNUM
				ENDTEXT
				SET TEXTMERGE OFF

					

*!*						" AND EFFECTIVEDTM <= " + lcSQLEndDateTimeString + ; 
*!*						" AND EXPIRATIONDTM >= " + lcSQLStartDateTimeString + ;
					
				IF .lTestMode THEN
					.TrackProgress('', LOGIT+SENDIT)
					.TrackProgress('lcSQL =' + lcSQL, LOGIT+SENDIT)
				ENDIF

				.nSQLHandle = SQLCONNECT("WTK2","sa","kr0n1tes")

				IF .nSQLHandle > 0 THEN
						
						llRetval1 = .ExecSQL(lcSQL, 'CURINFO', RETURN_DATA_MANDATORY) 
						
						IF llRetval1 THEN
						
							*SELECT CURINFO
							*BROWSE
							
							SELECT A.*, ;
								SPACE(10) AS MONTH, ;
								00 AS NMONTH, ;
								000.00 AS HOURLYWAGE, ;
								0000.00 AS WAGEAMTE, ;
								0000.00 AS WAGEAMTA ;
								FROM CURINFO A ;
								INTO CURSOR CURINFO2 ;
								ORDER BY NAME, APPLYDTM ;
								READWRITE
							
							SELECT CURINFO2
							SCAN
								REPLACE CURINFO2.MONTH WITH CMONTH(CURINFO2.APPLYDTM), CURINFO2.NMONTH WITH MONTH(CURINFO2.APPLYDTM) IN CURINFO2
							ENDSCAN
							
							* NOW WE NEED TO POPULATE THE WAGES....
							SELECT CURINFO2
							SCAN
								lcAPPLYDTM = .GetSQLStartDateTimeStringForWages( CURINFO2.APPLYDTM )
								*lcAPPLYDTM = STRTRAN(DTOC(TTOD(CURINFO2.APPLYDTM)),"/","")
								
								WAIT WINDOW NOWAIT 'Getting Wage for ' + CURINFO2.NAME


				SET TEXTMERGE ON
				TEXT TO lcSQLWAGE
SELECT 
{fn ifnull(BASEWAGEHOURLYAMT,0.00)} AS WAGE
FROM BASEWAGERTHIST 
WHERE EMPLOYEEID = <<CURINFO2.EMPLOYEEID>> 
AND <<lcAPPLYDTM>> BETWEEN EFFECTIVEDTM AND EXPIRATIONDTM 
				ENDTEXT
				SET TEXTMERGE OFF
				
				*SET STEP ON 
				
								IF .ExecSQL(lcSQLWAGE, 'CURWAGES', RETURN_DATA_MANDATORY) THEN
									REPLACE CURINFO2.HOURLYWAGE WITH CURWAGES.WAGE IN CURINFO2
								ENDIF

							ENDSCAN
							
							WAIT CLEAR
							
							* NOW CALCULATE THE WAGEAMT$ TAKING DATE-SENSITIVE TEMP AGENCY MARKUP %S INTO ACCOUNT - THIS IS WHAT THE AGENCY WILL CHARGE US FOR EACH ASSOCIATE
							SELECT CURINFO2
							SCAN
								
								lnWage = (CURINFO2.HOURLYWAGE)
							
								*************************************
								
								lnTempMarkupREG = .oTempAgency.getTempAgencyMarkupREG(CURINFO2.SHORTNM, CURINFO2.APPLYDTM, CURINFO2.PERSONNUM)

								lnTempMarkupOT = .oTempAgency.getTempAgencyMarkupOT(CURINFO2.SHORTNM, CURINFO2.APPLYDTM, CURINFO2.PERSONNUM)

								lnWageAmt = (CURINFO2.REGHOURS * ROUND(lnWage * lnTempMarkupREG,2)) + ;
											(CURINFO2.OTHOURS * ROUND(lnWage * DAILY_OT_MULTIPLIER * lnTempMarkupOT,2)) + ;
											(CURINFO2.DBLHOURS * ROUND(lnWage * DAILY_OT20_MULTIPLIER * lnTempMarkupOT,2))
								
								REPLACE CURINFO2.WAGEAMTA WITH lnWageAmt IN CURINFO2  && THIS IS WHAT THE AGENCY WILL CHARGE US FOR EACH ASSOCIATE -INCLUDES THEIR MARKUP
								
								****************************************
								
								lnTempMarkupREG = 1.0

								lnTempMarkupOT = 1.0

								lnWageAmt = (CURINFO2.REGHOURS * ROUND(lnWage * lnTempMarkupREG,2)) + ;
											(CURINFO2.OTHOURS * ROUND(lnWage * DAILY_OT_MULTIPLIER * lnTempMarkupOT,2)) + ;
											(CURINFO2.DBLHOURS * ROUND(lnWage * DAILY_OT20_MULTIPLIER * lnTempMarkupOT,2))
								
								REPLACE CURINFO2.WAGEAMTE WITH lnWageAmt IN CURINFO2  && THIS IS WHAT EACH ASSOCIATE WILL GET PAID - DOES NOT INCLUDE THE AGENCY MARKUP
							
							ENDSCAN
							
							
							*SELECT CURINFO2
							*BROWSE
							
							* NOW GET DISTINCT LIST OF EMPLOYEES PLUS OTHER FIELDS, FROM THE TIME DATA CURSOR
							SELECT DISTINCT ;
							AGENCY, ;
							NAME, ;
							PERSONNUM ;
							FROM CURINFO2 ;
							INTO CURSOR CURTEMPSPRE

							SELECT ;
							A.*, ;
							0000 AS HOURS1, ;
							00000.00 AS WAGES1, ;
							0000 AS HOURS2, ;
							00000.00 AS WAGES2, ;
							0000 AS HOURS3, ;
							00000.00 AS WAGES3, ;
							0000 AS HOURS4, ;
							00000.00 AS WAGES4, ;
							0000 AS HOURS5, ;
							00000.00 AS WAGES5, ;
							0000 AS HOURS6, ;
							00000.00 AS WAGES6, ;
							0000 AS HOURS7, ;
							00000.00 AS WAGES7, ;
							0000 AS HOURS8, ;
							00000.00 AS WAGES8, ;
							0000 AS HOURS9, ;
							00000.00 AS WAGES9, ;
							0000 AS HOURS10, ;
							00000.00 AS WAGES10, ;
							0000 AS HOURS11, ;
							00000.00 AS WAGES11, ;
							0000 AS HOURS12, ;
							00000.00 AS WAGES12, ;
							SPACE(30) AS DEPT, ;
							CTOD('') AS LASTWKDATE, ;
							000.00 AS LASTWAGE ;
							FROM CURTEMPSPRE A ;
							INTO CURSOR CURTEMPS ;
							ORDER BY NAME, PERSONNUM ;
							READWRITE
							
							* SUM THE HOURS AND WAGES BY MONTH FOR EACH ASSOCIATE
							SELECT CURTEMPS
							SCAN
								FOR i = 1 TO 12  && FOR EACH MONTH
								
									SELECT SUM(TOTHOURS) AS TOTHOURS, ;
										SUM(WAGEAMTE) AS WAGEAMTE ;
										FROM CURINFO2 ;
										INTO CURSOR CURHW ;
										WHERE (NMONTH = i) ;
										AND (PERSONNUM = CURTEMPS.PERSONNUM)
										
									IF USED("CURHW") AND (NOT EOF('CURHW')) THEN
										lcHoursField = 'HOURS' + TRANSFORM(i)									
										lcWagesField = 'WAGES' + TRANSFORM(i)									
										REPLACE &lcHoursField WITH CURHW.TOTHOURS, &lcWagesField WITH CURHW.WAGEAMTE IN CURTEMPS									
										USE IN CURHW
									ENDIF
								
								ENDFOR
							ENDSCAN
							
							* NOW GET THE LAST DEPT, DATE WORKED, AND HOURLYWAGE, CHRONOLOGICALLY, FOR EACH ASSOCIATE
							SELECT CURINFO2
							INDEX ON APPLYDTM TAG APPLYDTM DESCENDING
							SET ORDER TO APPLYDTM
							GOTO TOP
							
							SELECT CURTEMPS
							SCAN
								SELECT CURINFO2
								LOCATE FOR PERSONNUM = CURTEMPS.PERSONNUM
								IF FOUND() THEN
									REPLACE CURTEMPS.LASTWKDATE WITH CURINFO2.APPLYDTM, CURTEMPS.DEPT WITH CURINFO2.DEPT, CURTEMPS.LASTWAGE WITH CURINFO2.HOURLYWAGE IN CURTEMPS
								ENDIF
							ENDSCAN
							
							*SELECT CURTEMPS		
							*BROWSE					
							
							SELECT CURTEMPS	
							COPY TO ("C:\A\INFO " + lcTempCoName + ".XLS") XL5	
							
							SELECT CURINFO2
							COPY TO ("C:\A\CURINFO2 " + lcTempCoName + ".XLS") XL5	
							
						ENDIF
						

					*CLOSE DATABASES ALL

				ELSE
					* connection error
					.TrackProgress('Unable to connect to WTK System.', LOGIT+SENDIT)
				ENDIF   &&  .nSQLHandle > 0
				
				.TrackProgress('The process ended normally .', LOGIT+SENDIT)
					
			CATCH TO loError

				.TrackProgress('There was an error.',LOGIT+SENDIT)
				.TrackProgress('The Error # is: ' + TRANSFORM(loError.ERRORNO), LOGIT+SENDIT)
				.TrackProgress('The Error Message is: ' + TRANSFORM(loError.MESSAGE), LOGIT+SENDIT)
				.TrackProgress('The Error occurred in Program: ' + TRANSFORM(loError.PROCEDURE), LOGIT+SENDIT)
				.TrackProgress('The Error occurred at line #: ' + TRANSFORM(loError.LINENO), LOGIT+SENDIT)
				lnNumberOfErrors = lnNumberOfErrors + 1
				IF TYPE('oExcel') = 'O' AND NOT ISNULL(oExcel) THEN
					oExcel.QUIT()
				ENDIF
				*CLOSE DATA
				*CLOSE ALL

			ENDTRY

			*CLOSE DATA
			WAIT CLEAR
			***************** INTERNAL email results ******************************
			.TrackProgress('About to send status email.',LOGIT)
			.TrackProgress('==================================================================================================================', SENDIT)
			.TrackProgress('The logfile is: ' + .cLogFile, SENDIT)
			.TrackProgress('KRONOS Temp Badge process started: ' + .cStartTime, LOGIT+SENDIT)
			.TrackProgress('KRONOS Temp Badge process finished: ' + TTOC(DATETIME()), LOGIT+SENDIT)

			IF .lSendInternalEmailIsOn THEN
				* try to trap error from not having dartmail dll's registered on user's pc...
				TRY
					DO FORM dartmail2 WITH .cSendTo,.cFrom,.cSubject,.cCC,.cAttach,.cBodyText,"A"
					.TrackProgress('Sent status email.',LOGIT)
				CATCH TO loError
					*=MESSAGEBOX('There was an error sending the status email. Please contact Mark Bennett.',0+64,"KRONOS Temp Time")
					.TrackProgress('There was an error sending the status email.',LOGIT)
					.TrackProgress('The Error # is: ' + TRANSFORM(loError.ERRORNO), LOGIT)
					.TrackProgress('The Error Message is: ' + TRANSFORM(loError.MESSAGE), LOGIT)
					.TrackProgress('The Error occurred in Program: ' + TRANSFORM(loError.PROCEDURE), LOGIT)
					.TrackProgress('The Error occurred at line #: ' + TRANSFORM(loError.LINENO), LOGIT)
					lnNumberOfErrors = lnNumberOfErrors + 1
					*CLOSE DATA
				ENDTRY

			ELSE
				.TrackProgress('Did not send status email: THIS.lSendInternalEmailIsOn = FALSE.',LOGIT)
			ENDIF

		ENDWITH
		RETURN
	ENDFUNC && main


*!*		FUNCTION MAIN
*!*			WITH THIS
*!*				LOCAL lnNumberOfErrors, lcSQL, ldToday, lcSQLStartDateTimeString, lcSQLEndDateTimeString, lcTempAgencycode
*!*				LOCAL llRetval1, ldStartDate, ldEndDate, lnFTEHoursPerDay, lnDaysInDateRange, lnFTEHoursInDateRange
*!*				LOCAL lcStartDate, lcEndDate

*!*				TRY
*!*					lnNumberOfErrors = 0
*!*					ldToday = .dToday

*!*					*.TrackProgress('==================================================================================================================', LOGIT+SENDIT)
*!*					.TrackProgress('Kronos Temp Info process started....', LOGIT+SENDIT)
*!*					.TrackProgress('COMPUTERNAME = ' + .cCOMPUTERNAME, LOGIT+SENDIT)
*!*					.TrackProgress('USERNAME = ' + .cUSERNAME, LOGIT+SENDIT)
*!*					.TrackProgress('PROJECT = KRONOSTEMPINFORPT', LOGIT+SENDIT)
*!*					IF .lTestMode THEN
*!*						.TrackProgress('====>TEST MODE', LOGIT+SENDIT)
*!*					ENDIF

*!*					
*!*					.cSubject = 'Kronos Temp Info Report for: ' + TRANSFORM(ldToday)
*!*					
*!*					lcTempAgencycode = 'D' && Quality NJ temps
*!*					
*!*					ldStartDate = {^2017-05-01}
*!*					ldEndDate = {^2017-10-31}
*!*					
*!*					lcStartDate = DTOC(ldStartDate)
*!*					lcEndDate = DTOC(ldEndDate)

*!*					lcSQLStartDateTimeString = .GetSQLStartDateTimeString( ldStartDate )
*!*					lcSQLEndDateTimeString = .GetSQLEndDateTimeString( ldEndDate )
*!*					
*!*					lnDaysInDateRange = ldEndDate - ldStartDate
*!*					.TrackProgress('lnDaysInDateRange = ' + TRANSFORM(lnDaysInDateRange), LOGIT+SENDIT)
*!*					
*!*					lnFTEHoursPerDay = 2080.00 / 365.00
*!*					.TrackProgress('lnFTEHoursPerDay = ' + TRANSFORM(lnFTEHoursPerDay), LOGIT+SENDIT)
*!*					
*!*					lnFTEHoursInDateRange = lnDaysInDateRange * lnFTEHoursPerDay
*!*					.TrackProgress('lnFTEHoursInDateRange = ' + TRANSFORM(lnFTEHoursInDateRange), LOGIT+SENDIT)
*!*					
*!*					* 1 - get time data for date range
*!*					* 2 - get the list of temp associates who worked in the desired date range
*!*					* 3 - for each associate get
*!*					*		hire date
*!*					*		total hours worked in date range
*!*					*		# of days in date range
*!*					*		# of days worked in date range
*!*					*		FTE for the date range
*!*					*		last hourly wage


*!*						
*!*					SET TEXTMERGE ON
*!*					TEXT TO lcSQL
*!*	SELECT
*!*	D.LABORLEV5DSC AS AGENCY,
*!*	C.FULLNM AS NAME,
*!*	C.PERSONNUM,
*!*	C.PERSONID,
*!*	A.EMPLOYEEID,
*!*	SUM((A.DURATIONSECSQTY / 3600.00)) AS TOTHOURS,
*!*	COUNT(DISTINCT A.APPLYDTM) AS DAYSWORKED
*!*	FROM WFCTOTAL A
*!*	JOIN WTKEMPLOYEE B
*!*	ON B.EMPLOYEEID = A.EMPLOYEEID
*!*	JOIN PERSON C
*!*	ON C.PERSONID = B.PERSONID
*!*	JOIN LABORACCT D
*!*	ON D.LABORACCTID = A.LABORACCTID
*!*	JOIN PAYCODE E
*!*	ON E.PAYCODEID = A.PAYCODEID
*!*	WHERE (A.APPLYDTM >= <<lcSQLStartDateTimeString>>)
*!*	AND (A.APPLYDTM <= <<lcSQLEndDateTimeString>>)
*!*	AND (D.LABORLEV1NM = 'TMP')
*!*	AND (D.LABORLEV5NM = '<<lcTempAgencycode>>')
*!*	AND A.DURATIONSECSQTY > 0.00
*!*	GROUP BY D.LABORLEV5DSC, C.FULLNM, C.PERSONNUM, C.PERSONID, A.EMPLOYEEID
*!*	ORDER BY C.FULLNM, C.PERSONNUM
*!*					ENDTEXT
*!*					SET TEXTMERGE OFF

*!*					SET TEXTMERGE ON
*!*					TEXT TO lcSQL2
*!*	SELECT
*!*	PERSONFULLNAME AS NAME,						
*!*	PERSONNUM,
*!*	COMPANYHIREDTM AS HIREDATE
*!*	FROM VP_EMPLOYEEV42
*!*	WHERE HOMELABORLEVELNM1 = 'TMP'
*!*	AND (HOMELABORLEVELNM5 = '<<lcTempAgencycode>>')
*!*					ENDTEXT
*!*					SET TEXTMERGE OFF

*!*						

*!*	*!*						" AND EFFECTIVEDTM <= " + lcSQLEndDateTimeString + ; 
*!*	*!*						" AND EXPIRATIONDTM >= " + lcSQLStartDateTimeString + ;
*!*						
*!*					IF .lTestMode THEN
*!*						.TrackProgress('', LOGIT+SENDIT)
*!*						.TrackProgress('lcSQL =' + lcSQL, LOGIT+SENDIT)
*!*					ENDIF

*!*					.nSQLHandle = SQLCONNECT("WTK2","sa","kr0n1tes")

*!*					IF .nSQLHandle > 0 THEN
*!*							
*!*							llRetval1 = .ExecSQL(lcSQL, 'CURINFO', RETURN_DATA_MANDATORY) ;
*!*								AND .ExecSQL(lcSQL2, 'CURV42', RETURN_DATA_MANDATORY)
*!*							
*!*							IF llRetval1 THEN
*!*							
*!*								*SELECT CURV42
*!*								*BROWSE
*!*								
*!*								*SELECT CURINFO
*!*								*BROWSE
*!*								
*!*								SELECT ;
*!*								CTOD('') AS HIREDATE, ;
*!*								(lcStartDate + "-" + lcEndDate) AS DATERANGE, ;
*!*								lnDaysInDateRange AS DAYS, ;
*!*								00.0000 AS FTE, ;
*!*								A.* ;
*!*								FROM CURINFO A ;
*!*								INTO CURSOR CURINFO2 ;
*!*								READWRITE				
*!*								
*!*								
*!*								* POPULATE HIREDATES
*!*								SELECT CURINFO2
*!*								SCAN
*!*									SELECT CURV42
*!*									LOCATE FOR PERSONNUM = CURINFO2.PERSONNUM
*!*									IF FOUND() THEN
*!*										REPLACE CURINFO2.HIREDATE WITH CURV42.HIREDATE IN CURINFO2
*!*									ENDIF
*!*								ENDSCAN
*!*								
*!*								* populate FTE
*!*								SELECT CURINFO2
*!*								SCAN
*!*									REPLACE CURINFO2.FTE WITH (TOTHOURS / lnFTEHoursInDateRange) IN CURINFO2
*!*								ENDSCAN

*!*								SELECT CURINFO2
*!*								BROWSE
*!*								
*!*								SELECT ;
*!*									AGENCY, ;
*!*									DATERANGE, ;
*!*									DAYS, ;
*!*									NAME, ;
*!*									HIREDATE, ;
*!*									TOTHOURS, ;
*!*									DAYSWORKED, ;
*!*									FTE ;
*!*								FROM CURINFO2 ;
*!*								INTO CURSOR CURINFO3 ;
*!*								ORDER BY FTE DESCENDING
*!*								
*!*								SELECT CURINFO3
*!*								BROWSE
*!*								
*!*								
*!*							ENDIF
*!*							

*!*						*CLOSE DATABASES ALL

*!*					ELSE
*!*						* connection error
*!*						.TrackProgress('Unable to connect to WTK System.', LOGIT+SENDIT)
*!*					ENDIF   &&  .nSQLHandle > 0
*!*					
*!*					.TrackProgress('The process ended normally .', LOGIT+SENDIT)
*!*						
*!*				CATCH TO loError

*!*					.TrackProgress('There was an error.',LOGIT+SENDIT)
*!*					.TrackProgress('The Error # is: ' + TRANSFORM(loError.ERRORNO), LOGIT+SENDIT)
*!*					.TrackProgress('The Error Message is: ' + TRANSFORM(loError.MESSAGE), LOGIT+SENDIT)
*!*					.TrackProgress('The Error occurred in Program: ' + TRANSFORM(loError.PROCEDURE), LOGIT+SENDIT)
*!*					.TrackProgress('The Error occurred at line #: ' + TRANSFORM(loError.LINENO), LOGIT+SENDIT)
*!*					lnNumberOfErrors = lnNumberOfErrors + 1
*!*					IF TYPE('oExcel') = 'O' AND NOT ISNULL(oExcel) THEN
*!*						oExcel.QUIT()
*!*					ENDIF
*!*					*CLOSE DATA
*!*					*CLOSE ALL

*!*				ENDTRY

*!*				*CLOSE DATA
*!*				WAIT CLEAR
*!*				***************** INTERNAL email results ******************************
*!*				.TrackProgress('About to send status email.',LOGIT)
*!*				.TrackProgress('==================================================================================================================', SENDIT)
*!*				.TrackProgress('The logfile is: ' + .cLogFile, SENDIT)
*!*				.TrackProgress('KRONOS Temp Badge process started: ' + .cStartTime, LOGIT+SENDIT)
*!*				.TrackProgress('KRONOS Temp Badge process finished: ' + TTOC(DATETIME()), LOGIT+SENDIT)

*!*				IF .lSendInternalEmailIsOn THEN
*!*					* try to trap error from not having dartmail dll's registered on user's pc...
*!*					TRY
*!*						DO FORM dartmail2 WITH .cSendTo,.cFrom,.cSubject,.cCC,.cAttach,.cBodyText,"A"
*!*						.TrackProgress('Sent status email.',LOGIT)
*!*					CATCH TO loError
*!*						*=MESSAGEBOX('There was an error sending the status email. Please contact Mark Bennett.',0+64,"KRONOS Temp Time")
*!*						.TrackProgress('There was an error sending the status email.',LOGIT)
*!*						.TrackProgress('The Error # is: ' + TRANSFORM(loError.ERRORNO), LOGIT)
*!*						.TrackProgress('The Error Message is: ' + TRANSFORM(loError.MESSAGE), LOGIT)
*!*						.TrackProgress('The Error occurred in Program: ' + TRANSFORM(loError.PROCEDURE), LOGIT)
*!*						.TrackProgress('The Error occurred at line #: ' + TRANSFORM(loError.LINENO), LOGIT)
*!*						lnNumberOfErrors = lnNumberOfErrors + 1
*!*						*CLOSE DATA
*!*					ENDTRY

*!*				ELSE
*!*					.TrackProgress('Did not send status email: THIS.lSendInternalEmailIsOn = FALSE.',LOGIT)
*!*				ENDIF

*!*			ENDWITH
*!*			RETURN
*!*		ENDFUNC && main
	

	FUNCTION GetSQLStartDateTimeString
		* returns a timestamp string for use in SQL queries
		LPARAMETERS tdDate
		LOCAL lcSetDate, lcSQLDateTimeString
		lcSetDate = SET('DATE')
		SET DATE YMD
		lcSQLDateTimeString   = "{ts '" + TRANSFORM(YEAR(tdDate)) + "-" + PADL(MONTH(tdDate),2,"0") + "-" + PADL(DAY(tdDate),2,"0") + " 00:00:00.0'}"
		SET DATE &lcSetDate.
		RETURN lcSQLDateTimeString
	ENDFUNC
	

	FUNCTION GetSQLStartDateTimeStringForWages
		* returns a timestamp string for use in SQL queries
		LPARAMETERS tdDate
		LOCAL lcSetDate, lcSQLDateTimeString
		lcSetDate = SET('DATE')
		SET DATE YMD
		lcSQLDateTimeString   = "{ts '" + TRANSFORM(YEAR(tdDate)) + "-" + PADL(MONTH(tdDate),2,"0") + "-" + PADL(DAY(tdDate),2,"0") + " 12:00:00'}"
		SET DATE &lcSetDate.
		RETURN lcSQLDateTimeString
	ENDFUNC
	

	FUNCTION GetSQLEndDateTimeString
		* returns a timestamp string for use in SQL queries
		LPARAMETERS tdDate
		LOCAL lcSetDate, lcSQLDateTimeString
		lcSetDate = SET('DATE')
		SET DATE YMD
		lcSQLDateTimeString = "{ts '" + TRANSFORM(YEAR(tdDate)) + "-" + PADL(MONTH(tdDate),2,"0") + "-" + PADL(DAY(tdDate),2,"0") + " 23:59:59.9'}"
		SET DATE &lcSetDate.
		RETURN lcSQLDateTimeString
	ENDFUNC
	

	FUNCTION ExecSQL
		LPARAMETERS tcSQL, tcCursorName, tlNoDataReturnedIsOkay
		LOCAL llRetval, lnResult
		WITH THIS
			* close target cursor if it's open
			IF USED(tcCursorName)
				USE IN (tcCursorName)
			ENDIF
			WAIT WINDOW "Fetching remote data; please wait..." NOWAIT
			lnResult = SQLEXEC(.nSQLHandle, tcSQL, tcCursorName)
			llRetval = ( lnResult > 0 )
			IF llRetval THEN
				* see if any data came back
				IF NOT tlNoDataReturnedIsOkay THEN
					IF (NOT USED(tcCursorName)) OR EOF(tcCursorName) THEN
						llRetval = .F.
						.TrackProgress('No data was returned by this query: ' +CRLF + tcSQL, LOGIT+SENDIT)
					ENDIF
				ENDIF
			ELSE
				.TrackProgress("Result Code [" + TRANSFORM(lnResult) + "] returned by this query: " + tcSQL, LOGIT+SENDIT)
			ENDIF
			WAIT CLEAR
			RETURN llRetval
		ENDWITH
	ENDFUNC


	PROCEDURE TrackProgress
		* do any combination of Wait Window, writing to logfile, and adding to body of email,
		* based on nFlags parameter.
		LPARAMETERS tcExpression, tnFlags
		WITH THIS
			IF BITAND(tnFlags,LOGIT) = LOGIT THEN
				IF .lLoggingIsOn THEN
					?
					? .cProcessName + "  " + TTOC(DATETIME()) + ": " + tcExpression
				ENDIF
			ENDIF
			IF .lWaitWindowIsOn THEN
				IF BITAND(tnFlags,NOWAITIT) = NOWAITIT THEN
					WAIT WINDOW tcExpression NOWAIT
				ENDIF
				IF BITAND(tnFlags,WAITIT) = WAITIT THEN
					WAIT WINDOW tcExpression TIMEOUT .nWaitWindowTimeout
				ENDIF
			ENDIF
			IF BITAND(tnFlags,SENDIT) = SENDIT THEN
				IF .lSendInternalEmailIsOn THEN
					.cBodyText = .cBodyText + tcExpression + CRLF + CRLF
				ENDIF
			ENDIF
		ENDWITH
	ENDPROC  &&  TrackProgress
	

ENDDEFINE
