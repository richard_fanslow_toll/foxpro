* Create list of employees/temps who should be in the clocks but don't have active badge #s
*
* Added checks for various other discrepancies 1/14/2010 MB.
*
* Build EXE as F:\UTIL\KRONOS\KRONOSTEMPBADGEREPORT.EXE
*
* converted to use FORM SendMail_au 12/13/2017 MB
* 
* put back to dartmail 1/29/2018 - because using form SendMail_au no longer sent me the emails.
*
* added agency G - Chartwell NJ and Agency F - Chartwell KY 2/1/2018 MB
*
* 4/11/2018 MB - deactivated flagging of Div 50 for Mira Loma as errors.
*
* 4/24/2018 MB: changed fmiint.com emails to Toll emails.

utilsetup("KRONOSTEMPBADGEREPORT")

LOCAL loKronosDataCheckReport

loKronosDataCheckReport = CREATEOBJECT('KronosDataCheckReport')
loKronosDataCheckReport.MAIN()

schedupdate()

CLOSE DATABASES ALL
RETURN

#DEFINE LOGIT 1
#DEFINE WAITIT 2
#DEFINE NOWAITIT 4
#DEFINE SENDIT 8
#DEFINE CRLF CHR(13) + CHR(10)
#DEFINE LF CHR(10)
#DEFINE RETURN_DATA_MANDATORY .F.
#DEFINE RETURN_DATA_NOT_MANDATORY .T.
#DEFINE DAILY_OT_MULTIPLIER 1.5

DEFINE CLASS KronosDataCheckReport AS CUSTOM

	cProcessName = 'KronosDataCheckReport'

	lTestMode = .T.  && .F. FOR NORMAL OPERATION, .T. TO SUPPRESS ALL BUT DEVELOPER EMAILS AND CHANGE INPUT TABLE, ETC.

	lAutoYield = .T.

	cStartTime = TTOC(DATETIME())
	
	* connection properties
	nSQLHandle = 0

	* file properties
	nFileHandle = 0

	* date properties
	dtNow = DATETIME()

	dToday = DATE()
	* to force different pay weeks
	*dToday = {^2009-05-26}

	cToday = DTOC(DATE())
	
	* wait window properties
	nWaitWindowTimeout = 2
	lWaitWindowIsOn = .T.

	* logfile properties
	lLoggingIsOn = .T.
	cLogFile = 'F:\UTIL\KRONOS\Logfiles\KronosDataCheckReport_log.txt' 

	* INTERNAL email properties
	lSendInternalEmailIsOn = .T.
	cFrom = 'No Reply <noreply@tollgroup.com>'
	cSendTo = 'mark.bennett@tollgroup.com'
	cCC = ''
	cSubject = ''
	cAttach = ''
	cBodyText = ''
	cEmailMode = "A"  && autosend in sendmail_au form
	cBCC = ''
	lDoNotSend = .F.

	cCOMPUTERNAME = ''
	cUSERNAME = ''

	FUNCTION INIT
		IF NOT DODEFAULT()
			RETURN .F.
		ENDIF
		WITH THIS
			CLEAR
			*SET RESOURCE OFF
			CLOSE DATA
			SET CENTURY ON
			SET DATE AMERICAN
			SET HOURS TO 24
			SET ANSI ON
			SET TALK OFF
			SET DELETED ON
			SET CONSOLE OFF
			SET EXCLUSIVE OFF
			SET SAFETY OFF
			SET EXACT OFF
			SET STATUS BAR ON
			SET SYSMENU OFF
			SET ENGINEBEHAVIOR 70
			_VFP.AUTOYIELD = .lAutoYield
			.cCOMPUTERNAME = GETENV("COMPUTERNAME")
			.cUSERNAME = GETENV("USERNAME")
			IF .lTestMode THEN
				.cSendTo = 'Mark Bennett <mark.bennett@tollgroup.com>'
				.cLogFile = 'F:\UTIL\KRONOS\Logfiles\KronosDataCheckReport_log_TESTMODE.txt'
			ENDIF
			.lLoggingIsOn = .lLoggingIsOn AND NOT EMPTY(.cLogFile)
			IF .lLoggingIsOn THEN
				SET ALTERNATE TO (.cLogFile) ADDITIVE
				SET ALTERNATE ON
			ENDIF
		ENDWITH
	ENDFUNC


	FUNCTION DESTROY
		WITH THIS
			IF .lLoggingIsOn  THEN
				SET ALTERNATE OFF
				SET ALTERNATE TO
			ENDIF
		ENDWITH
		DODEFAULT()
	ENDFUNC


	FUNCTION MAIN
		WITH THIS
			LOCAL lnNumberOfErrors, lcSQL, lcSQL2, lcSQL3, lcSQL4, lcSQL5, ldToday, lcSQLStartDateTimeString, lcSQLEndDateTimeString
			LOCAL llRetval1, llRetval2, llRetval3, llRetval4, llRetval5

			TRY
				lnNumberOfErrors = 0
				ldToday = .dToday

				*.TrackProgress('==================================================================================================================', LOGIT+SENDIT)
				.TrackProgress('KRONOS Data Check process started....', LOGIT+SENDIT)
				.TrackProgress('COMPUTERNAME = ' + .cCOMPUTERNAME, LOGIT+SENDIT)
				.TrackProgress('USERNAME = ' + .cUSERNAME, LOGIT+SENDIT)
				.TrackProgress('PROJECT = KRONOSTEMPBADGEREPORT', LOGIT+SENDIT)
				IF .lTestMode THEN
					.TrackProgress('                        TEST MODE                        ', LOGIT+SENDIT)
				ENDIF

				lcSQLStartDateTimeString = .GetSQLStartDateTimeString( ldToday )
				lcSQLEndDateTimeString = .GetSQLEndDateTimeString( ldToday )
				
				.cSubject = 'Kronos Data Check Report for: ' + TRANSFORM(ldToday)


				IF USED('CURBADGES') THEN
					USE IN CURBADGES
				ENDIF
				IF USED('CUROTHER') THEN
					USE IN CUROTHER
				ENDIF
				IF USED('CURFILENUMBADGE') THEN
					USE IN CURFILENUMBADGE
				ENDIF
				IF USED('CURFILENUMBADGE2') THEN
					USE IN CURFILENUMBADGE2
				ENDIF
				IF USED('CURMISC') THEN
					USE IN CURMISC
				ENDIF
				IF USED('CURMISC2') THEN
					USE IN CURMISC2
				ENDIF
				IF USED('CURMISC3') THEN
					USE IN CURMISC3
				ENDIF
				IF USED('CURMISC4') THEN
					USE IN CURMISC4
				ENDIF

				* main SQL
				lcSQL = ;
					" SELECT " + ;
					" HOMELABORLEVELNM1 AS ADP_COMP, " + ;
					" HOMELABORLEVELNM2 AS DIVISION, " + ;
					" HOMELABORLEVELNM3 AS DEPT, " + ;
					" PERSONNUM AS ID, " + ;
					" PERSONFULLNAME AS NAME, " + ;
					" EMPLOYMENTSTATUS AS STATUS " + ;						
					" FROM VP_EMPLOYEEV42 " + ;
					" WHERE EMPLOYMENTSTATUS = 'Active' " + ;
					" AND HOMELABORLEVELNM1 IN ('SXI','TMP') " + ;
					" AND NOT (  HOMELABORLEVELNM2 = '02' AND HOMELABORLEVELNM3 = '0655' ) " + ;
					" AND NOT PERSONNUM IN ('999','500000','220000','7770','7771','7777','7778','7779','7876','10000','20000','7780','7781','7790','7791','7795','7796','7760','7761') " + ;
					" AND NOT PERSONNUM IN ('7762','7763','7774','7775','7766','7767','7768','7769','7773','7725','7726','7730','7731','000000000') " + ;
					" AND NOT EXISTS " + ;
					" ( " + ; 
					" SELECT * FROM BADGEASSIGN " + ; 
					" WHERE  EFFECTIVEDTM <= " + lcSQLEndDateTimeString + ;
					" AND EXPIRATIONDTM >= " + lcSQLStartDateTimeString + ;
					" AND BADGEASSIGN.PERSONID = VP_EMPLOYEEV42.PERSONID " + ;
					") "

*!*					lcSQL2 = ;
*!*						" SELECT " + ;
*!*						" HOMELABORLEVELDSC5 AS COMPANY, " + ;
*!*						" PERSONNUM AS ID, " + ; 
*!*						" PERSONFULLNAME AS EMPLOYEE, " + ;
*!*						" PAYRULENAME AS PAYRULE, " + ;
*!*						" TIMEZONENAME AS TIMEZONE, " + ;
*!*						" DCMDEVGRPNAME AS CLOCKGRP " + ;
*!*						" FROM VP_EMPLOYEEV42 " + ;
*!*						" WHERE (HOMELABORLEVELNM1 = 'TMP') AND " + ; 
*!*						" (( HOMELABORLEVELNM5 = 'A') AND (HOMELABORLEVELNM2 = '59') AND " + ; 
*!*						" (({FN IFNULL(PAYRULENAME,'')} <> 'SP TEMPS') OR " + ;
*!*						" ({FN IFNULL(TIMEZONENAME,'')} <> 'Pacific') OR " + ;
*!*						" ({FN IFNULL(DCMDEVGRPNAME,'')} <> 'RIALTO'))) OR " + ;
*!*						" (( HOMELABORLEVELNM5 = 'A') AND (HOMELABORLEVELNM2 <> '59') AND " + ; 
*!*						" (({FN IFNULL(PAYRULENAME,'')} <> 'SP TEMPS') OR " + ;
*!*						" ({FN IFNULL(TIMEZONENAME,'')} <> 'Pacific') OR " + ;
*!*						" ({FN IFNULL(DCMDEVGRPNAME,'')} <> 'CA XPUNCH'))) OR " + ;
*!*						" (( HOMELABORLEVELNM5 IN ('B','K')) AND " + ; 
*!*						" (({FN IFNULL(PAYRULENAME,'')} <> 'ML TEMPS') OR " + ;
*!*						" ({FN IFNULL(TIMEZONENAME,'')} <> 'Pacific') OR " + ;
*!*						" ({FN IFNULL(DCMDEVGRPNAME,'')} <> 'ML XPUNCH'))) "

				lcSQLSP2 = ;
					" SELECT EMPLOYMENTSTATUS AS STATUS, " + ;
					" HOMELABORLEVELDSC5 AS COMPANY, " + ;
					" PERSONNUM AS ID, " + ; 
					" PERSONFULLNAME AS EMPLOYEE, " + ;
					" PAYRULENAME AS PAYRULE, " + ;
					" TIMEZONENAME AS TIMEZONE, " + ;
					" DCMDEVGRPNAME AS CLOCKGRP " + ;
					" FROM VP_EMPLOYEEV42 " + ;
					" WHERE (( HOMELABORLEVELNM5 = 'X') AND ( ({FN IFNULL(PAYRULENAME,'')} NOT IN ('SP TEMPS','SP TEMPS DIV 05')) OR ({FN IFNULL(TIMEZONENAME,'')} <> 'Pacific') OR ({FN IFNULL(DCMDEVGRPNAME,'')} <> 'CA XPUNCH')))" 

				lcSQLCA1 = ;
					" SELECT EMPLOYMENTSTATUS AS STATUS, " + ;
					" HOMELABORLEVELDSC5 AS COMPANY, " + ;
					" PERSONNUM AS ID, " + ; 
					" PERSONFULLNAME AS EMPLOYEE, " + ;
					" PAYRULENAME AS PAYRULE, " + ;
					" TIMEZONENAME AS TIMEZONE, " + ;
					" DCMDEVGRPNAME AS CLOCKGRP " + ;
					" FROM VP_EMPLOYEEV42 " + ;
					" WHERE (( HOMELABORLEVELNM5 = 'U') AND ( ({FN IFNULL(PAYRULENAME,'')} <> 'CARSON TEMPS') OR ({FN IFNULL(TIMEZONENAME,'')} <> 'Pacific') OR ({FN IFNULL(DCMDEVGRPNAME,'')} <> 'CARSON 1 AND 2')))" 

				lcSQLCA2 = ;
					" SELECT EMPLOYMENTSTATUS AS STATUS, " + ;
					" HOMELABORLEVELDSC5 AS COMPANY, " + ;
					" PERSONNUM AS ID, " + ; 
					" PERSONFULLNAME AS EMPLOYEE, " + ;
					" PAYRULENAME AS PAYRULE, " + ;
					" TIMEZONENAME AS TIMEZONE, " + ;
					" DCMDEVGRPNAME AS CLOCKGRP " + ;
					" FROM VP_EMPLOYEEV42 " + ;
					" WHERE (( HOMELABORLEVELNM5 = 'V') AND ( ({FN IFNULL(PAYRULENAME,'')} <> 'CARSON TEMPS') OR ({FN IFNULL(TIMEZONENAME,'')} <> 'Pacific') OR ({FN IFNULL(DCMDEVGRPNAME,'')} <> 'CARSON 1 AND 2')))" 

				lcSQLCA4 = ;
					" SELECT EMPLOYMENTSTATUS AS STATUS, " + ;
					" HOMELABORLEVELDSC5 AS COMPANY, " + ;
					" PERSONNUM AS ID, " + ; 
					" PERSONFULLNAME AS EMPLOYEE, " + ;
					" PAYRULENAME AS PAYRULE, " + ;
					" TIMEZONENAME AS TIMEZONE, " + ;
					" DCMDEVGRPNAME AS CLOCKGRP " + ;
					" FROM VP_EMPLOYEEV42 " + ;
					" WHERE (( HOMELABORLEVELNM5 = 'Y') AND ( ({FN IFNULL(PAYRULENAME,'')} <> 'ML TEMPS') OR ({FN IFNULL(TIMEZONENAME,'')} <> 'Pacific') OR ({FN IFNULL(DCMDEVGRPNAME,'')} <> 'ML XPUNCH')))" 

				lcSQLNJ1 = ;
					" SELECT EMPLOYMENTSTATUS AS STATUS, " + ;
					" HOMELABORLEVELDSC5 AS COMPANY, " + ;
					" PERSONNUM AS ID, " + ; 
					" PERSONFULLNAME AS EMPLOYEE, " + ;
					" PAYRULENAME AS PAYRULE, " + ;
					" TIMEZONENAME AS TIMEZONE, " + ;
					" DCMDEVGRPNAME AS CLOCKGRP " + ;
					" FROM VP_EMPLOYEEV42 " + ;
					" WHERE (( HOMELABORLEVELNM5 IN ('D','G') AND ( ({FN IFNULL(PAYRULENAME,'')} <> 'NJ1 TEMPS') OR ({FN IFNULL(TIMEZONENAME,'')} <> 'Eastern') OR ({FN IFNULL(DCMDEVGRPNAME,'')} <> 'NJ XPUNCH')))" 

				lcSQLKY1 = ;
					" SELECT EMPLOYMENTSTATUS AS STATUS, HOMELABORLEVELNM5 AS AGENCYCODE, " + ;
					" HOMELABORLEVELDSC5 AS COMPANY, " + ;
					" PERSONNUM AS ID, " + ; 
					" PERSONFULLNAME AS EMPLOYEE, " + ;
					" PAYRULENAME AS PAYRULE, " + ;
					" TIMEZONENAME AS TIMEZONE, " + ;
					" DCMDEVGRPNAME AS CLOCKGRP " + ;
					" FROM VP_EMPLOYEEV42 " + ;
					" WHERE (( HOMELABORLEVELNM5 IN ('F','Q','W','Z')) AND ( ({FN IFNULL(PAYRULENAME,'')} <> 'KY1 TEMPS') OR ({FN IFNULL(TIMEZONENAME,'')} <> 'Eastern') OR ({FN IFNULL(DCMDEVGRPNAME,'')} <> 'KY XPUNCH')))" 
					
				lcSQL3 = ;
					" SELECT " + ;
					" V.PERSONFULLNAME AS NAME, " + ;
					" V.EMPLOYMENTSTATUS AS STATUS, " + ;
					" V.HOMELABORLEVELNM2 AS DIVISION, " + ;
					" V.HOMELABORLEVELNM3 AS DEPT, " + ;
					" V.PERSONNUM AS ID, " + ;
					" (SELECT {FN IFNULL(MAX(BADGENUM),'NULL')} FROM BADGEASSIGN WHERE PERSONID = V.PERSONID " + ;
					" AND EXPIRATIONDTM = (SELECT MAX(EXPIRATIONDTM) FROM BADGEASSIGN WHERE PERSONID = V.PERSONID) " + ;
					" ) AS BADGENUM " + ;						
					" FROM VP_EMPLOYEEV42 V " + ;
					" WHERE V.EMPLOYMENTSTATUS = 'Active' " + ;
					" AND V.HOMELABORLEVELNM1 IN ('SXI','TMP') " + ;
					" AND NOT ( V.HOMELABORLEVELNM2 = '02' AND V.HOMELABORLEVELNM3 = '0655' ) " + ;
					" AND NOT V.PERSONNUM IN ('999','500000','220000','7770','7771','7777','7778','7779','7876','10000','20000','7780','7781','7790','7791','7795','7796','7760','7761') " + ;
					" AND NOT V.PERSONNUM IN ('7762','7763','7774','7775','7766','7767','7768','7769','7773','7725','7726','7730','7731','000000000') " + ;
					" AND NOT {FN SUBSTRING(V.PERSONFULLNAME,1,12)} = 'TIMEREVIEWER' " + ;
					" ORDER BY V.PERSONNUM "
					
					TEXT TO lcSQL4
SELECT
PERSONNUM AS ID,
PERSONFULLNAME AS NAME,						
EMPLOYMENTSTATUS AS STATUS,
basewagehourlyamt as WAGE,
HOMELABORLEVELNM1 AS ADP_COMP,
HOMELABORLEVELNM2 AS DIVISION,
HOMELABORLEVELNM3 AS DEPT,
HOMELABORLEVELNM4 AS WORKSITE,
HOMELABORLEVELNM7 AS TIMERVWR,
PAYRULENAME AS PAYRULE,
ACCRUALPRFLNAME AS ACPROFILE,
{FN IFNULL(DCMDEVGRPNAME,'?')} AS DEVGROUP,
TIMEZONENAME AS TIMEZONE,
SENIORITYRANKDATE AS SENIORDATE,
COMPANYHIREDTM AS HIREDATE
FROM VP_EMPLOYEEV42
WHERE EMPLOYMENTSTATUS = 'Active'
AND NOT PERSONNUM IN ('1804','1736','999','500000','220000','7770','7771','7777','7778','7779','7876','10000','20000','7780','7781','7790','7791','7795','7796','7760','7761') 
AND NOT PERSONNUM IN ('7762','7763','7774','7775','7766','7767','7768','7769','7773','7725','7726','7730','7731','000000000') 
ORDER BY PERSONNUM
					ENDTEXT

	* 				
					TEXT TO lcSQL5
SELECT
PERSONNUM AS ID,
PERSONFULLNAME AS NAME,						
EMPLOYMENTSTATUS AS STATUS,
basewagehourlyamt as WAGE,
HOMELABORLEVELNM1 AS ADP_COMP,
HOMELABORLEVELNM2 AS DIVISION,
HOMELABORLEVELNM3 AS DEPT,
HOMELABORLEVELNM4 AS WORKSITE
FROM VP_EMPLOYEEV42
WHERE EMPLOYMENTSTATUS = 'Active'
AND HOMELABORLEVELNM2 = '50'
ORDER BY PERSONNUM
					ENDTEXT

*!*						" AND EFFECTIVEDTM <= " + lcSQLEndDateTimeString + ; 
*!*						" AND EXPIRATIONDTM >= " + lcSQLStartDateTimeString + ;
					
				IF .lTestMode THEN
					.TrackProgress('', LOGIT)
					.TrackProgress('lcSQL =' + lcSQL, LOGIT)
					.TrackProgress('lcSQLSP2 =' + lcSQLSP2, LOGIT)
					.TrackProgress('lcSQLCA1 =' + lcSQLCA1, LOGIT)
					.TrackProgress('lcSQLCA2 =' + lcSQLCA2, LOGIT)
					.TrackProgress('lcSQLCA4 =' + lcSQLCA4, LOGIT)
					.TrackProgress('lcSQLNJ1 =' + lcSQLNJ1, LOGIT)
					.TrackProgress('lcSQLKY1 =' + lcSQLKY1, LOGIT)
					.TrackProgress('lcSQL3 =' + lcSQL3, LOGIT)
					.TrackProgress('lcSQL4 =' + lcSQL4, LOGIT)
					.TrackProgress('lcSQL5 =' + lcSQL5, LOGIT)
				ENDIF

				.nSQLHandle = SQLCONNECT("WTK2","sa","kr0n1tes")

				IF .nSQLHandle > 0 THEN

						llRetval1 = .ExecSQL(lcSQL, 'CURBADGES', RETURN_DATA_MANDATORY) 
						
						llRetval2 =	.ExecSQL(lcSQLSP2, 'CURBADSP2', RETURN_DATA_MANDATORY) 
						
						llRetval2 =	.ExecSQL(lcSQLCA1, 'CURBADCA1', RETURN_DATA_MANDATORY) 
						
						llRetval2 =	.ExecSQL(lcSQLCA2, 'CURBADCA2', RETURN_DATA_MANDATORY) 
						
						llRetval2 =	.ExecSQL(lcSQLCA4, 'CURBADCA4', RETURN_DATA_MANDATORY) 
						
						llRetval2 =	.ExecSQL(lcSQLNJ1, 'CURBADNJ1', RETURN_DATA_MANDATORY) 
						
						llRetval2 =	.ExecSQL(lcSQLKY1, 'CURBADKY1', RETURN_DATA_MANDATORY) 
							
						llRetval3 =	.ExecSQL(lcSQL3, 'CURFILENUMBADGE', RETURN_DATA_MANDATORY) 	
							
						llRetval4 =	.ExecSQL(lcSQL4, 'CURMISC', RETURN_DATA_MANDATORY) 	
							
*!*							llRetval5 =	.ExecSQL(lcSQL5, 'CURBADMIRALOMA', RETURN_DATA_NOT_MANDATORY) 	
						
						
						* \\APP1\FMISYS\UTIL\KRONOS\ATTACHMENTS
						
*!*							IF USED('CURBADMIRALOMA') AND NOT EOF('CURBADMIRALOMA') THEN
*!*								SELECT CURBADMIRALOMA
*!*								COPY TO \\APP1\FMISYS\UTIL\KRONOS\ATTACHMENTS\CURBADMIRALOMA.XLS XL5
*!*								.cAttach = "\\APP1\FMISYS\UTIL\KRONOS\ATTACHMENTS\CURBADMIRALOMA.XLS"
*!*							ENDIF						
						
						IF USED('CURBADGES') AND NOT EOF('CURBADGES') THEN
							SELECT CURBADGES
							COPY TO \\APP1\FMISYS\UTIL\KRONOS\ATTACHMENTS\NOBADGE.XLS XL5
							.cAttach = "\\APP1\FMISYS\UTIL\KRONOS\ATTACHMENTS\NOBADGE.XLS"
						ENDIF						

						IF USED('CURBADSP2') AND NOT EOF('CURBADSP2') THEN
							SELECT CURBADSP2
							COPY TO \\APP1\FMISYS\UTIL\KRONOS\ATTACHMENTS\CURBADSP2.XLS XL5
							.cAttach = .cAttach + IIF(EMPTY(.cAttach),"",",") + "\\APP1\FMISYS\UTIL\KRONOS\ATTACHMENTS\CURBADSP2.XLS"
						ENDIF

						IF USED('CURBADCA1') AND NOT EOF('CURBADCA1') THEN
							SELECT CURBADCA1
							COPY TO \\APP1\FMISYS\UTIL\KRONOS\ATTACHMENTS\CURBADCA1.XLS XL5
							.cAttach = .cAttach + IIF(EMPTY(.cAttach),"",",") + "\\APP1\FMISYS\UTIL\KRONOS\ATTACHMENTS\CURBADCA1.XLS"
						ENDIF

						IF USED('CURBADCA2') AND NOT EOF('CURBADCA2') THEN
							SELECT CURBADCA2
							COPY TO \\APP1\FMISYS\UTIL\KRONOS\ATTACHMENTS\CURBADCA2.XLS XL5
							.cAttach = .cAttach + IIF(EMPTY(.cAttach),"",",") + "\\APP1\FMISYS\UTIL\KRONOS\ATTACHMENTS\CURBADCA2.XLS"
						ENDIF

						IF USED('CURBADCA4') AND NOT EOF('CURBADCA4') THEN
							SELECT CURBADCA4
							COPY TO \\APP1\FMISYS\UTIL\KRONOS\ATTACHMENTS\CURBADCA4.XLS XL5
							.cAttach = .cAttach + IIF(EMPTY(.cAttach),"",",") + "\\APP1\FMISYS\UTIL\KRONOS\ATTACHMENTS\CURBADCA4.XLS"
						ENDIF

						IF USED('CURBADNJ1') AND NOT EOF('CURBADNJ1') THEN
							SELECT CURBADNJ1
							COPY TO \\APP1\FMISYS\UTIL\KRONOS\ATTACHMENTS\CURBADNJ1.XLS XL5
							.cAttach = .cAttach + IIF(EMPTY(.cAttach),"",",") + "\\APP1\FMISYS\UTIL\KRONOS\ATTACHMENTS\CURBADNJ1.XLS"
						ENDIF

						IF USED('CURBADKY1') AND NOT EOF('CURBADKY1') THEN
							SELECT CURBADKY1
							COPY TO \\APP1\FMISYS\UTIL\KRONOS\ATTACHMENTS\CURBADKY1.XLS XL5
							.cAttach = .cAttach + IIF(EMPTY(.cAttach),"",",") + "\\APP1\FMISYS\UTIL\KRONOS\ATTACHMENTS\CURBADKY1.XLS"
						ENDIF
												
						IF USED('CURFILENUMBADGE') AND NOT EOF('CURFILENUMBADGE') THEN
						
*!*								SELECT CURFILENUMBADGE
*!*								BROW
							
							SELECT * ;
								FROM CURFILENUMBADGE ;
								INTO CURSOR CURFILENUMBADGE2 ;
								WHERE ID <> BADGENUM ;
								ORDER BY ID

							IF USED('CURFILENUMBADGE2') AND NOT EOF('CURFILENUMBADGE2') THEN
								SELECT CURFILENUMBADGE2
								COPY TO \\APP1\FMISYS\UTIL\KRONOS\ATTACHMENTS\FILENUM_BADGE_MISMATCH.XLS XL5
								.cAttach = .cAttach + IIF(EMPTY(.cAttach),"",",") + "\\APP1\FMISYS\UTIL\KRONOS\ATTACHMENTS\FILENUM_BADGE_MISMATCH.XLS"
							ENDIF
													
						ENDIF	
						
						IF USED('CURMISC') AND NOT EOF('CURMISC') THEN

							SELECT ;
								UPPER(ALLTRIM(ID)) AS ID, ;
								UPPER(ALLTRIM(NAME)) AS NAME, ;						
								UPPER(ALLTRIM(STATUS)) AS STATUS, ;
								WAGE, ;
								UPPER(ALLTRIM(ADP_COMP)) AS ADP_COMP, ;
								UPPER(ALLTRIM(DIVISION)) AS DIVISION, ;
								UPPER(ALLTRIM(DEPT)) AS DEPT, ;
								UPPER(ALLTRIM(WORKSITE)) AS WORKSITE, ;
								UPPER(ALLTRIM(TIMERVWR)) AS TIMERVWR, ;
								UPPER(ALLTRIM(PAYRULE)) AS PAYRULE, ;
								UPPER(ALLTRIM(ACPROFILE)) AS ACPROFILE, ;
								UPPER(ALLTRIM(DEVGROUP)) AS DEVGROUP, ;
								TIMEZONE, ;
								SENIORDATE, ;
								HIREDATE ;
								FROM CURMISC ;
								INTO CURSOR CURMISC2
								
								
*!*	SELECT CURMISC2
*!*	BROWSE
*!*	throw
						
							SELECT NAME, ADP_COMP, ID, WORKSITE, DEVGROUP ;
							FROM CURMISC2 ;
							INTO CURSOR CURMISC3 ;
							WHERE ;
							( DEVGROUP == "ALL DEVICES" ) ;
							OR ;
							( (INLIST(ADP_COMP,'TMP')) AND (WORKSITE == "CA1")  AND (NOT DEVGROUP == "CARSON 1 AND 2") ) ;
							OR ;
							( (INLIST(ADP_COMP,'TMP')) AND (WORKSITE == "CA2")  AND (NOT DEVGROUP == "CARSON 1 AND 2") ) ;
							OR ;
							( (INLIST(ADP_COMP,'TMP')) AND (WORKSITE == "CA4")  AND (NOT DEVGROUP == "ML XPUNCH") ) ;
							OR ;
							( (INLIST(ADP_COMP,'TMP')) AND (WORKSITE == "SP2")  AND (NOT DEVGROUP == "CA XPUNCH") ) ;
							OR ;
							( (INLIST(ADP_COMP,'TMP')) AND (WORKSITE == "KY1") AND (NOT DEVGROUP == "KY XPUNCH") ) ;
							OR ;
							( (INLIST(ADP_COMP,'TMP')) AND (WORKSITE == "NJ1") AND (NOT DEVGROUP == "NJ XPUNCH") ) ;
							OR ;
							( (NOT INLIST(ADP_COMP,'SXI','TMP')) AND (NOT INLIST(DEVGROUP,"?","NO DEVICES")) ) ;
							ORDER BY WORKSITE, NAME

*!*								OR ;
*!*								( (ADP_COMP = 'SXI') AND (DEPT = "0650") AND (WORKSITE == "SP2") AND (NOT DEVGROUP == "CA DRIVERS") ) ;
							
							
							IF USED('CURMISC3') AND NOT EOF('CURMISC3') THEN
								SELECT CURMISC3
								COPY TO \\APP1\FMISYS\UTIL\KRONOS\ATTACHMENTS\DEVGROUP_MISMATCH.XLS XL5
								.cAttach = .cAttach + IIF(EMPTY(.cAttach),"",",") + "\\APP1\FMISYS\UTIL\KRONOS\ATTACHMENTS\DEVGROUP_MISMATCH.XLS"
							ENDIF
							
					
						
							SELECT NAME, ADP_COMP, ID, WORKSITE, PAYRULE ;
							FROM CURMISC2 ;
							INTO CURSOR CURMISC4 ;
							WHERE ;
							(INLIST(ADP_COMP,'TMP') AND (ALLTRIM(WORKSITE) == "KY1") AND (NOT ALLTRIM(PAYRULE) == "KY1 TEMPS")) ;
							OR ;
							(INLIST(ADP_COMP,'TMP') AND (ALLTRIM(WORKSITE) == "CA1") AND (NOT ALLTRIM(PAYRULE) == "CARSON TEMPS")) ;
							OR ;
							(INLIST(ADP_COMP,'TMP') AND (ALLTRIM(WORKSITE) == "CA2") AND (NOT ALLTRIM(PAYRULE) == "CARSON TEMPS")) ;
							OR ;
							(INLIST(ADP_COMP,'TMP') AND (ALLTRIM(WORKSITE) == "CA4") AND (NOT ALLTRIM(PAYRULE) == "ML TEMPS")) ;
							OR ;
							(INLIST(ADP_COMP,'TMP') AND (ALLTRIM(WORKSITE) == "SP2") AND (NOT INLIST(ALLTRIM(PAYRULE),"SP TEMPS","SP TEMPS DIV 05"))) ;
							OR ;
							(INLIST(ADP_COMP,'TMP') AND (ALLTRIM(WORKSITE) == "NJ1") AND (NOT ALLTRIM(PAYRULE) == "NJ1 TEMPS")) ;
							ORDER BY WORKSITE, NAME
							
							
							IF USED('CURMISC4') AND NOT EOF('CURMISC4') THEN
								SELECT CURMISC4
								COPY TO \\APP1\FMISYS\UTIL\KRONOS\ATTACHMENTS\PAYRULE_MISMATCH.XLS XL5
								.cAttach = .cAttach + IIF(EMPTY(.cAttach),"",",") + "\\APP1\FMISYS\UTIL\KRONOS\ATTACHMENTS\PAYRULE_MISMATCH.XLS"
							ENDIF
					
						ENDIF

						.TrackProgress('The process ended normally.', LOGIT+SENDIT)
						
						IF NOT EMPTY(.cAttach) THEN
							.cBodyText = "=====> See errors in attached spreadsheets!" + CRLF + CRLF + .cBodyText
						ENDIF

					*CLOSE DATABASES ALL

				ELSE
					* connection error
					.TrackProgress('Unable to connect to WTK System.', LOGIT+SENDIT)
				ENDIF   &&  .nSQLHandle > 0
			CATCH TO loError

				.TrackProgress('There was an error.',LOGIT+SENDIT)
				.TrackProgress('The Error # is: ' + TRANSFORM(loError.ERRORNO), LOGIT+SENDIT)
				.TrackProgress('The Error Message is: ' + TRANSFORM(loError.MESSAGE), LOGIT+SENDIT)
				.TrackProgress('The Error occurred in Program: ' + TRANSFORM(loError.PROCEDURE), LOGIT+SENDIT)
				.TrackProgress('The Error occurred at line #: ' + TRANSFORM(loError.LINENO), LOGIT+SENDIT)
				lnNumberOfErrors = lnNumberOfErrors + 1
				IF TYPE('oExcel') = 'O' AND NOT ISNULL(oExcel) THEN
					oExcel.QUIT()
				ENDIF
				*CLOSE DATA
				*CLOSE ALL

			ENDTRY

			*CLOSE DATA
			WAIT CLEAR
			***************** INTERNAL email results ******************************
			.TrackProgress('About to send status email.',LOGIT)
			.TrackProgress('==================================================================================================================', SENDIT)
			.TrackProgress('The logfile is: ' + .cLogFile, SENDIT)
			.TrackProgress('KRONOS Temp Badge process started: ' + .cStartTime, LOGIT+SENDIT)
			.TrackProgress('KRONOS Temp Badge process finished: ' + TTOC(DATETIME()), LOGIT+SENDIT)

			IF .lSendInternalEmailIsOn THEN
			
				* try to trap error from not having dartmail dll's registered on user's pc...
				TRY
					DO FORM dartmail2 WITH .cSendTo,.cFrom,.cSubject,.cCC,.cAttach,.cBodyText,"A"
					.TrackProgress('Sent status email.',LOGIT)
				CATCH TO loError
					*=MESSAGEBOX('There was an error sending the status email. Please contact Mark Bennett.',0+64,"KRONOS Temp Time")
					.TrackProgress('There was an error sending the status email.',LOGIT)
					.TrackProgress('The Error # is: ' + TRANSFORM(loError.ERRORNO), LOGIT)
					.TrackProgress('The Error Message is: ' + TRANSFORM(loError.MESSAGE), LOGIT)
					.TrackProgress('The Error occurred in Program: ' + TRANSFORM(loError.PROCEDURE), LOGIT)
					.TrackProgress('The Error occurred at line #: ' + TRANSFORM(loError.LINENO), LOGIT)
					lnNumberOfErrors = lnNumberOfErrors + 1
					*CLOSE DATA
				ENDTRY

*!*					m:\dev\frm\SendMail_au
*!*					PARAMETER 
*!*					lcTO        "someone@somewhere.com",                 && email address listing separated by  semi-colons
*!*					lcFrom      "NoReply <noreply@tollgroup.com>",        && from should always be a noreply@tollgroup.com
*!*					lcSubject   
*!*					lcCC        
*!*					lcAttach
*!*					lcBody
*!*					lcMode 
*!*					lcBCC
*!*					lcDoNotSend                                           && .T. will not send email, but record the data 

*SET STEP ON 

*!*				*lcAttach = .cAttach
*!*				
*!*					* body text contains sql select statements, so wrap with the SQL block comment characters 12/11/2017 MB
*!*					*lcBodyText = "/*" + CRLF + .cBodyText + CRLF + "*/"
*!*					
*!*					*lcBodyText = 'This is a test'
*!*	*!*					
*!*					DO FORM SendMail_au WITH .cSendTo, .cFrom, .cSubject, .cCC, .cAttach, .cBodyText, .cEmailMode, .cBCC, .lDoNotSend

			ELSE
				.TrackProgress('Did not send status email: THIS.lSendInternalEmailIsOn = FALSE.',LOGIT)
			ENDIF

		ENDWITH
		RETURN
	ENDFUNC && main
	
	
	FUNCTION BlockSQLComment
		LPARAMETERS tcSQL
		LOCAL lcSQL
		lcSQL = "/*" + CRLF + tcSQL + CRLF + "*/"
		RETURN lcSQL	
	ENDFUNC
	

	FUNCTION GetSQLStartDateTimeString
		* returns a timestamp string for use in SQL queries
		LPARAMETERS tdDate
		LOCAL lcSetDate, lcSQLDateTimeString
		lcSetDate = SET('DATE')
		SET DATE YMD
		lcSQLDateTimeString   = "{ts '" + TRANSFORM(YEAR(tdDate)) + "-" + PADL(MONTH(tdDate),2,"0") + "-" + PADL(DAY(tdDate),2,"0") + " 00:00:00.0'}"
		SET DATE &lcSetDate.
		RETURN lcSQLDateTimeString
	ENDFUNC
	

	FUNCTION GetSQLEndDateTimeString
		* returns a timestamp string for use in SQL queries
		LPARAMETERS tdDate
		LOCAL lcSetDate, lcSQLDateTimeString
		lcSetDate = SET('DATE')
		SET DATE YMD
		lcSQLDateTimeString = "{ts '" + TRANSFORM(YEAR(tdDate)) + "-" + PADL(MONTH(tdDate),2,"0") + "-" + PADL(DAY(tdDate),2,"0") + " 23:59:59.9'}"
		SET DATE &lcSetDate.
		RETURN lcSQLDateTimeString
	ENDFUNC
	

	FUNCTION ExecSQL
		LPARAMETERS tcSQL, tcCursorName, tlNoDataReturnedIsOkay
		LOCAL llRetval, lnResult
		WITH THIS
			* close target cursor if it's open
			IF USED(tcCursorName)
				USE IN (tcCursorName)
			ENDIF
			WAIT WINDOW "Fetching remote data; please wait..." NOWAIT
			lnResult = SQLEXEC(.nSQLHandle, tcSQL, tcCursorName)
			llRetval = ( lnResult > 0 )
			IF llRetval THEN
				* see if any data came back
				IF NOT tlNoDataReturnedIsOkay THEN
					IF (NOT USED(tcCursorName)) OR EOF(tcCursorName) THEN
						llRetval = .F.
						.TrackProgress('No data was returned by this query: ' +CRLF + tcSQL, LOGIT)
					ENDIF
				ENDIF
			ELSE
				.TrackProgress("Result Code [" + TRANSFORM(lnResult) + "] returned by this query: " + tcSQL, LOGIT)
			ENDIF
			WAIT CLEAR
			RETURN llRetval
		ENDWITH
	ENDFUNC


	PROCEDURE TrackProgress
		* do any combination of Wait Window, writing to logfile, and adding to body of email,
		* based on nFlags parameter.
		LPARAMETERS tcExpression, tnFlags
		WITH THIS
			IF BITAND(tnFlags,LOGIT) = LOGIT THEN
				IF .lLoggingIsOn THEN
					?
					? .cProcessName + "  " + TTOC(DATETIME()) + ": " + tcExpression
				ENDIF
			ENDIF
			IF .lWaitWindowIsOn THEN
				IF BITAND(tnFlags,NOWAITIT) = NOWAITIT THEN
					WAIT WINDOW tcExpression NOWAIT
				ENDIF
				IF BITAND(tnFlags,WAITIT) = WAITIT THEN
					WAIT WINDOW tcExpression TIMEOUT .nWaitWindowTimeout
				ENDIF
			ENDIF
			IF BITAND(tnFlags,SENDIT) = SENDIT THEN
				IF .lSendInternalEmailIsOn THEN
					.cBodyText = .cBodyText + tcExpression + CRLF + CRLF
				ENDIF
			ENDIF
		ENDWITH
	ENDPROC  &&  TrackProgress
	

ENDDEFINE
