* Checks for errors in the warehousing data

lparameter xoffice, xauto, xaccountid, xhistory, xarchive, xsendemail, xdevdata

if xhistory
	garchive=.t.
endif

if !empty(xaccountid)
	close databases all 
	set deleted on 
endif

set status bar off

do case
case date()={6/22/11}
	xdir="m:\bak\whbak\whdata-7\"
case xdevdata
	gldor=.f.
	? "test data"
	xoffice=upper(xoffice)
	xdir="m:\dev\whdata\"
case type("xaccountid")="N" and xaccountid=6532
	xdir="f:\whk\whdata\"
otherwise
	xdir=wf(xoffice)
endcase

if !empty(xaccountid)
	xfilter="accountid="+transform(xaccountid)
	xfilter2="outdet.accountid="+transform(xaccountid)
else
	xfilter=".t."
	xfilter2=".t."
endif

xsqlexec("select * from whoffice","whoffice2",,"wh")
index on office tag office
set order to

=seek(upper(xoffice),"whoffice2","office")
xmasteroffice=whoffice2.rateoffice
use in whoffice2

goffice=upper(xoffice)

if xarchive or xauto or !used("inwolog")
	close databases all
	set deleted on
	set talk off
	set exclusive off
	xsqlexec("select * from indet where mod='"+xoffice+"' and "+xfilter,,,"wh")
	xsqlexec("select * from inloc where mod='"+xoffice+"' and "+xfilter,,,"wh")
	index on indetid tag indetid
	set order to
	
	xsqlexec("select * from outdet where mod='"+xoffice+"' and "+xfilter,,,"wh")
	xsqlexec("select * from outloc where mod='"+xoffice+"' and "+xfilter,,,"wh")
	index on outdetid tag outdetid
	set order to
	
	xsqlexec("select * from inven where mod='"+goffice+"' and "+xfilter,,,"wh")
	xsqlexec("select * from invenloc where mod='"+goffice+"' and "+xfilter,,,"wh")
	index on invenid tag invenid
	set order to
	
	xsqlexec("select * from adj where mod='"+xoffice+"' and totqty#0",,,"wh")

	xsqlexec("select * from whseloc where office='"+xmasteroffice+"'","whseloc",,"wh")
	index on whseloc tag whseloc
	set order to
endif

create cursor xmatch (accountid i, units l, style c(20), color c(10), id c(10), pack c(10), whseloc c(7), ;
	qtyin n(8), qtyout n(8), qtyadjin n(8), qtyadjout n(8), qtyremove n(8), invenrem n(8), invensb n(8), ;
	invenis n(8), invendiff n(8), invendiff2 n(8), fixed l, notfound l)
	
index on str(accountid,4)+trans(units)+style+color+id+pack+whseloc tag whmatch

scatter memvar blank

* inbound

i=0
select indet
scan for &xfilter
	i=i+1
	if int(i/1000)=i/1000
		wait wind "indet - "+trans(i) nowait
	endif

	scatter memvar fields accountid, style, color, id, pack, units

	select inloc
	scan for indetid=indet.indetid
		if locqty>=0
			if !seek(str(indet.accountid,4)+trans(indet.units)+indet.style+indet.color+indet.id+indet.pack+inloc.whseloc,"xmatch","whmatch")
				m.whseloc=inloc.whseloc
				m.qtyin=inloc.locqty
				m.qtyremove=inloc.removeqty
				m.qtyadjout=0
				insert into xmatch from memvar
			else
				replace qtyin with xmatch.qtyin+inloc.locqty, ;
						qtyremove with xmatch.qtyremove+inloc.removeqty in xmatch
			endif
		else
			if !seek(str(indet.accountid,4)+trans(indet.units)+indet.style+indet.color+indet.id+indet.pack+inloc.whseloc,"xmatch","whmatch")
				m.whseloc=inloc.whseloc
				m.qtyin=0
				m.qtyremove=0
				m.qtyout=0
				m.qtyadjout=inloc.locqty
				insert into xmatch from memvar
			else
				replace qtyadjout with xmatch.qtyadjout+inloc.locqty in xmatch
			endif
		endif
	endscan
endscan

m.qtyin=0
m.qtyadjout=0

* outbound

select outdet.accountid, style, color, id, pack, units, whseloc, sum(locqty) as locqty ;
	from outdet, outloc ;
	where outdet.outdetid=outloc.outdetid ;
	and &xfilter2 ;
	group by 1,2,3,4,5,6,7 ;
	into cursor xoutdet

i=0
select xoutdet
scan
	i=i+1
	if int(i/1000)=i/1000
		wait wind "outdet - "+trans(i) nowait
	endif
    
	scatter memvar fields accountid, style, color, id, pack, units, whseloc, locqty
	if !seek(str(m.accountid,4)+trans(m.units)+m.style+m.color+m.id+m.pack+m.whseloc,"xmatch","whmatch")
		m.qtyremove=0
		m.qtyout=m.locqty
		insert into xmatch from memvar
	else
		replace qtyout with xmatch.qtyout+m.locqty in xmatch
	endif
endscan

m.qtyout=0

* adjustments

i=0
select adj
scan for &xfilter
	i=i+1
	if int(i/1000)=i/1000
		wait wind "adj - "+trans(i) nowait
	endif

	if totqty>0
		m.qtyadjin=totqty
		m.qtyadjout=0
	else
		m.qtyadjin=0
		m.qtyadjout=totqty
	endif
	
	if !seek(str(accountid,4)+trans(units)+style+color+id+pack+whseloc,"xmatch","whmatch")
		scatter memvar fields accountid, style, color, id, pack, units, whseloc
		m.qtyremove=removeqty
		insert into xmatch from memvar
	else
		replace qtyadjin with xmatch.qtyadjin+m.qtyadjin, ;
				qtyadjout with xmatch.qtyadjout+m.qtyadjout, ;
				qtyremove with xmatch.qtyremove+adj.removeqty in xmatch
	endif

*	select xmatch
*	assert !(style="H7FW0040  " and color="483" and id="XL")
endscan

m.qtyadjin=0
m.qtyadjout=0
m.qtyremove=0

* inventory

i=0
select inven
scan for &xfilter
	i=i+1
	if int(i/100)=i/100
		wait wind "inven - "+trans(i) nowait
	endif

	scatter memvar fields accountid, style, color, id, pack, units
	select invenloc
	scan for invenid=inven.invenid
		if !seek(str(inven.accountid,4)+trans(inven.units)+inven.style+inven.color+inven.id+inven.pack+invenloc.whseloc,"xmatch","whmatch")
			m.whseloc=invenloc.whseloc
			m.invenis=locqty
			insert into xmatch from memvar
		else
			replace invenis with xmatch.invenis+invenloc.locqty in xmatch
		endif
	endscan
endscan

replace all invensb with qtyin-qtyout+qtyadjin+qtyadjout, ;
			invendiff with invenis-invensb, ;
			invenrem with qtyin+qtyadjin-qtyremove, ;
			invendiff2 with invenis-invenrem in xmatch

select xmatch
delete for accountid=6145
copy to ("f:\auto\xmatchfull"+xoffice)
copy to ("f:\auto\xmatch"+xoffice) for invendiff#0 or (invendiff2#0 and !(inlist(whseloc,"CYCLE","XRACK") and invendiff2<0))

if !xauto and !xarchive
	brow for invendiff#0 or invendiff2#0
endif

if xsendemail
	email("Dyoung@fmiint.com","WHMATCH done",,,,,.t.,,,,,.t.)
endif