PARAMETERS nAcctNum,lcBol
IF VARTYPE(cOffice) # "C"
	cOffice = "C"
ENDIF

*xgetsql

csql = "tgfnjsql01"
cSQLPass = ""
*IF lTesting
*SET STEP ON
*ENDIF
SELECT 0
USE F:\wh\sqlpassword
cSQLPass = ALLTRIM(sqlpassword.PASSWORD)
USE IN sqlpassword

SET ESCAPE ON
ON ESCAPE CANCEL

IF lTestinput
	lcDSNLess="driver=SQL Server;server=tgfnjsql01;uid=SA;pwd=&cSQLPass;DATABASE=PICKPACK"
ELSE && AND nAcctNum # 6059
	lcDSNLess="driver=SQL Server;server=tgfnjsql01;uid=SA;pwd=&cSQLPass;DATABASE=PICKPACK"
ENDIF

nHandle=SQLSTRINGCONNECT(m.lcDSNLess,.T.)
SQLSETPROP(nHandle,"DispLogin",3)
WAIT WINDOW "Now processing "+cCustName+" SQL view...please wait" NOWAIT
IF nHandle<1 && bailout
	SET STEP ON
	WAIT WINDOW "Could not make SQL connection" TIMEOUT 3
	cRetMsg = "NO SQL CONNECT"
	RETURN cRetMsg
	THROW
ENDIF

if usesql()
	xsqlexec("select ship_ref, style, color, id, upc, outshipid, outdetid, totqty as oqty " + ;
		"from cartons where ship_ref='9999999'","results",,"pickpack")
else
	llSuccess=SQLEXEC(nHandle,"select ship_ref,style,color,id,upc,outshipid,outdetid,totqty as oqty from cartons where ship_ref= '9999999'","results")
	IF llSuccess<1  && no records -1 indicates no records and 1 indicates records found
		cRetMsg = "NO SQL CONNECT"
		RETURN cRetMsg
	ENDIF
endif

SELECT ship_ref,shipins FROM outship WHERE bol_no = lcBol INTO CURSOR ptdata

SELECT ptdata

*!* Scans through all PTs within the OUTSHIP BOL#
SCAN FOR "PROCESSMODE*PICKPACK"$ptdata.shipins
	if usesql()
		xsqlexec("select ship_ref, style, color, id, upc, outshipid, outdetid, sum(totqty) as oqty " + ;
			"from cartons where ship_ref='"+lcPt+"' group by ship_ref, style, color, id, upc, outshipid, outdetid","rslt1",,"pickpack")
	else
		lcPt = ptdata.ship_ref
		lcQ1 = [SELECT ship_ref,style,color,id,upc,outshipid,outdetid, Sum(totqty) as oqty FROM cartons ];
			+ [WHERE ship_ref = ']+lcPt+[' group by ship_ref,style,color,id,upc,outshipid,outdetid ]

		lcsql = lcQ1
		llSuccess=SQLEXEC(nHandle,lcsql,"rslt1")

		IF llSuccess<1  && no records -1 indicates no records and 1 indicates records found
			cRetMsg = "NO SQL CONNECT"
			RETURN cRetMsg
		ENDIF
	endif
	
	SELECT rslt1
	SCAN
		SCATTER MEMVAR
		SELECT results
		APPEND BLANK
		GATHER MEMVAR
	ENDSCAN
ENDSCAN

SCAN FOR "PROCESSMODE*PREPACK"$ptdata.shipins
	if usesql()
		xsqlexec("select ship_ref, style, color, id, upc, outshipid, outdetid, sum(totqty) as oqty " + ;
			"from cartons where ship_ref='"+lcpt+"' group by ship_ref, style, color, id, upc, outshipid, outdetid","rslt1",,"pickpack")
	else
		lcPt = ptdata.ship_ref
		lcQ1 = [SELECT ship_ref,style,color,id,upc,outshipid,outdetid, Sum(totqty) as oqty FROM cartons ];
			+ [WHERE ship_ref = ']+lcPt+[' group by ship_ref,style,color,id,upc,outshipid,outdetid ]

		lcsql = lcQ1
		llSuccess=SQLEXEC(nHandle,lcsql,"rslt1")

		IF llSuccess<1  && no records -1 indicates no records and 1 indicates records found
			cRetMsg = "NO SQL CONNECT"
			RETURN cRetMsg
		ENDIF
	endif
	
	SELECT rslt1
	SCAN
		SCATTER MEMVAR
		SELECT results
		APPEND BLANK
		GATHER MEMVAR
	ENDSCAN
ENDSCAN

SQLDISCONNECT(nHandle)

cRetMsg = "OK"
RETURN cRetMsg
