**packdaily.prg
_screen.Caption = "Pack Daily Routine - Packs Daily.dbf and modifies table structures as needed"

try
	use f:\wo\wodata\daily exclusive
	pack
	use in daily
catch
	lerror=.t.
endtry


**process table structure changes specified in tablechanges.dbf - mvw 12/2/08
use f:\kpi\kpidata\tablechange

scan for !processed
	try
		replace attemptdt with datetime() in tablechange

		xtable=alltrim(tablechange.table)
		xindex=alltrim(lower(tablechange.index))
		xindexname=alltrim(lower(tablechange.indexname))

		select 0
		use &xtable alias xtable exclusive
		ataginfo(atags)
		xexists=.f.
		for i = 1 to tagcount()
			if lower(atags(i,1))=xindexname
				xexists=.t.
				exit
			endif
		endfor

		if !xexists
			index on &xindex tag &xindexname
			replace processed with .t.,;
					processdt with datetime() in tablechange
			cTo = "mwinter@fmiint.com"
			cFrom ="TGF WMS Operations <fmicorporate@fmiint.com>"
			cSubject = xindexname+" index created in "+xtable+" successfully!"
			cMessage = xindexname+" index created in manifest successfully!"+alltrim(tablechange.emailMsg)
			DO FORM dartmail2 WITH cTo,cFrom,cSubject,"","",cMessage,"A"
		endif

		use in xtable
	catch
		lerror=.t.
	endtry
endscan
use in tablechange


close databases all
