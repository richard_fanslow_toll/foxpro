* Print the sheets explaining 2007 vacation changes, based on the various spreadsheets.

SET SAFETY OFF

LOCAL oExcel, oWorkbook, oWorksheet
LOCAL lnRow, lcRow, lcXLFile, lcPrintMark

*lcXLFile = GETFILE()
lcXLFile = 'C:\ACCRUALS_2007_FILES\axa_zxu_vacation_2007.xls'

oExcel = CREATEOBJECT("excel.application")
oExcel.VISIBLE = .T.

oWorkbook = oExcel.workbooks.OPEN(lcXLFile)

*oWorkbook.SAVEAS(lcFiletoSaveAs)

oWorksheet = oWorkbook.Worksheets[1]

FOR lnRow = 2 TO 149
	lcRow = ALLTRIM(STR(lnRow))

	* SHOULD WE PRINT THIS ROW?
	lcPrintMark = "N"
	
	IF NOT ISNULL( oWorksheet.RANGE("Z" + lcRow).VALUE ) THEN
		lcPrintMark = UPPER(ALLTRIM(oWorksheet.RANGE("Z" + lcRow).VALUE))
	ENDIF
	
	IF lcPrintMark = "Y" THEN
	
		* PRINT VAC EXPLANATION SHEET
		
		* first populate the print area of the spreadsheet
		
		* Employee Seniority Date
		oWorksheet.RANGE("AH7").VALUE = "Your effective Hire Date was: " + TRANSFORM(TTOD(oWorksheet.RANGE("E" + lcRow).VALUE))
		
		* Employee name
		oWorksheet.RANGE("AH3").VALUE = "For: " + ALLTRIM(oWorksheet.RANGE("C" + lcRow).VALUE)
		
		* 2006 vac Balance
		oWorksheet.RANGE("AI9").VALUE = oWorksheet.RANGE("H" + lcRow).VALUE
		
		* Vac accrued since last anniv
		oWorksheet.RANGE("AI10").VALUE = oWorksheet.RANGE("I" + lcRow).VALUE
		
		* 1/1/2007 vac Balance
		oWorksheet.RANGE("AI11").VALUE = oWorksheet.RANGE("K" + lcRow).VALUE
		
		* division
		oWorksheet.RANGE("AH25").VALUE = "Div: " + PADL(INT(oWorksheet.RANGE("A" + lcRow).VALUE),2,"0")
		
		* YAY PRINT!!!
		oWorksheet.PageSetup.PrintArea = "$AH$2:$AJ$28"
		oWorksheet.PrintOut()
		
		* MARK AS PRINTED
		oWorksheet.RANGE("AB" + lcRow).VALUE = "Y"
	ENDIF
	
	
*!*		IF ISNULL( oWorksheet.RANGE("Z" + lcRow).VALUE ) THEN
*!*			EXIT for
*!*		ENDIF

ENDFOR


* SAVE CHANGES

oWorkbook.Save()

oWorkbook.Close()

oExcel.Quit()