PARAMETERS cRunType,nWO_Num,cOffice,nAcctNum,nTripId
PUBLIC c_CntrlNum,c_GrpCntrlNum,cWO_Num,cGroupName,cDivision,cCustName,cFilename,div214,cST_CSZ
PUBLIC tsendto,tcc,tsendtoerr,tccerr,tFrom,cPrgName

cFilename = ""
cPrgName =  "VF 214 OUTBOUND"

lTesting = .F.
IF !USED('account')
	xsqlexec("select * from account where inactive=0","account",,"qq")
	index on accountid tag accountid
	set order to
ENDIF

ON ESCAPE CANCEL
lFClose = .T.
lIsError = .F.
lDoCatch = .T.
cShip_ref = ""
cST_CSZ = ""
nFilenum = 0
nAcctNum = 687
*ASSERT .f.
TRY
	cSystemName = "UNASSIGNED"
	cWO_Num = ""
	lEmail = .F.
	lDoError = .F.
	lFilesOut = .T.
	IF VARTYPE(nWO_Num)= "L"
		IF lTesting
			CLOSE DATABASES ALL
			lFilesOut = .F.
			nWO_Num = 711024
			cOffice = "N"
			cRunType = "E"
			nTripId =  947942
		ELSE
			WAIT WINDOW "No params specified" TIMEOUT 2
			lDoCatch = .F.
			THROW
		ENDIF
	ENDIF

	cDivision = IIF(cOffice="C","California",IIF(cOffice="M","Miami","New Jersey"))
	cOffCity = IIF(cOffice="C","SAN PEDRO",IIF(cOffice="M","MIAMI","CARTERET"))
	cOffState = IIF(cOffice="C","CA",IIF(cOffice="M","FL","NJ"))

	xReturn = "XXX"
	DO m:\dev\prg\wf_alt WITH cOffice,nAcctNum
	IF EMPTY(ALLTRIM(xReturn))
		DO errormail WITH "NO WHSE"
		THROW
	ENDIF
	cUseFolder = UPPER(xReturn)

	cWO_Num = ALLTRIM(STR(nWO_Num))
	cProcType = IIF(cRunType = "E","ENROUTE","DELIVERED")

	DIMENSION thisarray(1)
	tFrom = "TGF Warehouse Operations <warehouse-ops@fmiint.com>"
	tattach = ""

	SELECT 0
	USE F:\3pl\DATA\mailmaster ALIAS mm
	LOCATE FOR mm.edi_type = 'MISC' AND mm.taskname = 'GENERAL'
	tsendto = IIF(mm.use_alt,mm.sendtoalt,mm.sendto)
	tcc = IIF(mm.use_alt,mm.ccalt,mm.cc)
	tsendtoerr = IIF(mm.use_alt,mm.sendtoalt,mm.sendto)
	tccerr = IIF(mm.use_alt,mm.ccalt,mm.cc)
	USE IN mm

	cCustName = "VF"
	cCustFolder = "VF"
	cCustPrefix = cCustName+"214"+LOWER(cRunType)+"_"+LOWER(cOffice)
	dt1 = TTOC(DATETIME(),1)
	dtmail = TTOC(DATETIME())
	dt2 = DATETIME()

	cFilename = ("f:\ftpusers\"+cCustFolder+"\214-Staging\"+cCustPrefix+dt1+".txt")
	cFilenameShort = JUSTFNAME(cFilename)
	cFilename2 = ("f:\ftpusers\"+cCustFolder+"\214OUT\archive\"+cFilenameShort)
	cFilename3 = ("f:\ftpusers\"+cCustFolder+"\214OUT\"+cFilenameShort)
	nFilenum = FCREATE(cFilename)

	IF !USED('xref')
		SELECT 0
		USE "f:\vf\data\vf_xref" ALIAS xref ORDER accountid NOUPDATE
	ENDIF
	ASSERT .F. MESSAGE "At table checks"
	SELECT xref
	IF SEEK(nAcctNum,"xref","accountid")
		cGroupName = ALLTRIM(xref.div214)
		cSystemName = ALLTRIM(xref.acct_name)
	ELSE
		DO errormail WITH "MISSACCT"
		THROW
	ENDIF
	USE IN xref
	IF EMPTY(cGroupName)
		DO errormail WITH "MISSGROUP"
		THROW
	ENDIF

	cMailName = PROPER(STRTRAN(cGroupName,"VF","VANITY FAIR"))
	cMailName = IIF(EMPTY(cMailName),"VF",cMailName)

*!* Open Tables

	xsqlexec("select * from fxtrips where fxtripsid="+transform(ntripid),"fxtrips",,"fx",,,,.t.)
	
	IF reccount("fxtrips")=0
		DO errormail WITH "BAD TRIPID"
		THROW
	ELSE
		IF EMPTY(fxtrips.pickedup)
			DO errormail WITH "MISSPICKUP"
			THROW
		ENDIF
	ENDIF

*	cGroupName = ""

	xsqlexec("select * from dellocs",,,"wo")
	index on str(accountid,4)+location tag acct_loc
	set order to

	IF !USED('unloccodes')
		USE F:\3pl\DATA\vf_unloccodes IN 0 ALIAS unloccodes
	ENDIF

	SELECT 0
	IF !USED('manifest')
		USE F:\wo\wodata\manifest ALIAS manifest ORDER TAG wo_num SHARED NOUPDATE
	ENDIF
	IF !SEEK(nWO_Num)
		DO errormail WITH "MISSWO"
		THROW
	ELSE
		SELECT dellocs
		LOCATE
		LOCATE FOR dellocs.accountid = manifest.accountid AND dellocs.div = manifest.div
		IF FOUND()
			cDellocCity = UPPER(ALLTRIM(dellocs.city))
			cDellocState = UPPER(ALLTRIM(dellocs.state))
			cDellocZip = ALLTRIM(dellocs.zip)
			IF cDellocCity = "JFK"
				cDelCS = "JFK"
			ELSE
				cDelCS = cDellocCity+", "+cDellocState
			ENDIF
			cDellocUNCode = "US*UN*XYZ"
			IF !SEEK(cDelCS,'unloccodes','city')
				DO errormail WITH "MISSLOC"
				closefiles()
				RETURN
			ELSE
				cDellocUNCode = ALLTRIM(unloccodes.CODE)
			ENDIF
			cST_CSZ = cDellocCity+"*"+cDellocState+"*"+cDellocZip+"*"+cDellocUNCode
		ENDIF
	ENDIF

	cST_Name = "TGF INC."
	DO CASE
		CASE cOffice = "C"
*!*				cSF_Addr =  ("C/O "+cGroupName+"*450 WESTMONT DRIVE")
			cFMICity = "SAN PEDRO"
			cFMIState = "CA"
*!*				cFMIZip = "90731"
*!*				cSF_CSZ    = "SAN PEDRO*CA*90731*US*UN*SPQ"
		CASE cOffice = "N"
*!*				cSF_Addr =  ("C/O "+cGroupName+"*800 FEDERAL BLVD")
			cFMICity = "CARTERET"
			cFMIState = "NJ"
*!*				cFMIZip = "07008"
*!*				cSF_CSZ    = "CARTERET*NJ*07095*US*UN*CSF"
		CASE cOffice = "M"
*!*				cSF_Addr =  ("C/O "+cGroupName+"*9901 N.W. 106TH STREET")
			cFMICity = "MIAMI"
			cFMIState = "FL"
*!*				cFMIZip = "33178"
*!*				cSF_CSZ    = "MEDLEY*FL*33178*US*UN*MDY"
	ENDCASE

	cfd = "*"  && Field delimiter
	csegd = "~"  && Segment delimiter
	cterminator = ">"  && Used at end of ISA segment

	csendqual = "ZZ"  && Sender qualifier
	csendid = "FMIE"  && Sender ID code
	crecqual = "ZZ"  && Recip qualifier
	crecid = "APLUNET"   && Recip ID Code
	cDate = DTOS(DATE())
	cTruncDate = RIGHT(cDate,6)
	cTruncTime = SUBSTR(TTOC(DATETIME(),1),9,4)
	cfiledate = cDate+cTruncTime
	csendidlong = PADR(csendid,15," ")
	crecidlong = PADR(crecid,15," ")
	cString = ""

	nSTCount = 0
	nSegCtr = 0
	nLXNum = 1
	STORE "" TO cEquipNum,cHAWB

	IF lTesting
		cISACode = "T"
	ELSE
		cISACode = "P"
	ENDIF

	DO num_incr_isa
	cISA_Num = PADL(c_CntrlNum,9,"0")
	STORE "ISA"+cfd+"00"+cfd+"          "+cfd+"00"+cfd+"          "+cfd+csendqual+cfd+csendidlong+cfd+;
		crecqual+cfd+crecidlong+cfd+cTruncDate+cfd+cTruncTime+cfd+"^"+cfd+"00501"+cfd+;
		cISA_Num+cfd+"0"+cfd+cISACode+cfd+cterminator+csegd TO cString
	DO cstringbreak

	STORE "GS"+cfd+"QM"+cfd+csendid+cfd+crecid+cfd+cDate+cfd+cTruncTime+cfd+c_CntrlNum+;
		cfd+"X"+cfd+"005010"+csegd TO cString
	DO cstringbreak

	DO num_incr_st
	STORE "ST"+cfd+"214"+cfd+PADL(c_GrpCntrlNum,9,"0")+csegd TO cString
	DO cstringbreak
	nSTCount = nSTCount + 1
	nSegCtr = nSegCtr + 1

	SELECT manifest
	SET ORDER TO

	cTrailer = ALLT(manifest.trailer)
	cBL = cWO_Num
	STORE "B10"+cfd+cTrailer+cfd+cBL+cfd+csendid+csegd TO cString
	DO cstringbreak
	nSegCtr = nSegCtr + 1

	STORE "MS3"+cfd+csendid+cfd+"1"+cfd+cFMICity+cfd+"J"+cfd+cFMIState+csegd TO cString
	DO cstringbreak
	nSegCtr = nSegCtr + 1


	WAIT CLEAR
	WAIT WINDOW "Now creating Trucking WO#-based 214 information..." NOWAIT

*!* Now in line loop
	SCAN FOR manifest.wo_num = nWO_Num
		nRuns = IIF(cRunType = "E",2,1)
		FOR doloop = 1 TO nRuns
			STORE "LX"+cfd+ALLTRIM(STR(nLXNum))+csegd TO cString  && Size
			DO cstringbreak
			nSegCtr = nSegCtr + 1
			nLXNum = nLXNum + 1

			STORE "L11"+cfd+"003"+cfd+"8X"+csegd TO cString
			DO cstringbreak
			nSegCtr = nSegCtr + 1

			STORE "L11"+cfd+"VF CORP"+cfd+"IC"+csegd TO cString  && added 9/27/06
			DO cstringbreak
			nSegCtr = nSegCtr + 1

			cEquipNum = ALLTRIM(manifest.CONTAINER)
			cEquipQual = "EQ"
			cEquipDesc = "EQUIPMENT NUMBER"
			IF EMPTY(cEquipNum)
				cEquipNum = ALLTRIM(manifest.hawb)
				cEquipQual = "AW"
				cEquipDesc = "AIRBILL NUMBER"
				IF EMPTY(cEquipNum)
					DO errormail WITH "MISSEQUIP"
					THROW
				ENDIF
			ENDIF

			STORE "L11"+cfd+cEquipNum+cfd+cEquipQual+cfd+cEquipDesc+csegd TO cString
			DO cstringbreak
			nSegCtr = nSegCtr + 1

*!* FXTRIPS Data Acquisition
			IF cRunType = "E"
				cShipStatus = IIF(doloop = 1,"AF","B6")
				dStatusDate = IIF(doloop = 1,fxtrips.pickedup,fxtrips.delappt)
				cStatusTime = IIF(doloop = 1,fxtrips.picktime,fxtrips.delatime)
			ELSE
				cShipStatus = "X1"
				dStatusDate = fxtrips.delivered
				cStatusTime = fxtrips.delvtime
			ENDIF
			cStatusDate = DTOC(dStatusDate,1)
			IF EMPTY(cStatusDate)
				cStatusDate = DTOC(DATE(),1)
			ENDIF
			cStatusTime = "1530"

			STORE "AT7"+cfd+cShipStatus+REPLICATE(cfd,4)+cStatusDate+cfd+cStatusTime+cfd+"LT"+csegd TO cString
			DO cstringbreak
			nSegCtr = nSegCtr + 1

			cSeal = ALLTRIM(manifest.seal)
			IF EMPTY(cSeal)
				cSeal = "XXX"
			ENDIF
			STORE "M7"+cfd+cSeal+REPLICATE(cfd,4)+"SH"+csegd TO cString
			DO cstringbreak
			nSegCtr = nSegCtr + 1

			N101 = "XG"
			cEventLoc = IIF(N101="XG","EVENT LOCATION",cSFName)
			STORE "N1"+cfd+N101+cfd+cEventLoc+csegd TO cString
			DO cstringbreak
			nSegCtr = nSegCtr + 1

			STORE "N4"+cfd+cST_CSZ+csegd TO cString
			DO cstringbreak
			nSegCtr = nSegCtr + 1

* added arrival# here 5/17 in OID11
			cPO_Num = ALLTRIM(manifest.po_num)
			cDesc = ALLTRIM(manifest.STYLE)
			cQty = ALLTRIM(STR(manifest.quantity))
			cWtUnit = "L"
			cWeight = ALLTRIM(STR(manifest.weight))
			cVolume = ALLTRIM(STR(manifest.cbm,6,2))
			cArrival = ALLTRIM(manifest.arrival)
			STORE "OID"+cfd+cfd+cPO_Num+cfd+cDesc+cfd+"CTN"+cfd+cQty+cfd+;
				cWtUnit+cfd+cWeight+cfd+"X"+cfd+cVolume+cfd+cfd+cArrival+csegd TO cString
			DO cstringbreak
			nSegCtr = nSegCtr + 1

			STORE "SDQ"+cfd+"SX"+cfd+"56"+cfd+cGroupName+cfd+"1"+csegd TO cString
			DO cstringbreak
			nSegCtr = nSegCtr + 1
		ENDFOR
	ENDSCAN

*!* Finish processing
	DO close214
	IF nFilenum>0
		=FCLOSE(nFilenum)
	ENDIF
	DO ediupdate WITH "214 CREATED",.F.,.F.

	IF lFilesOut
		COPY FILE &cFilename TO &cFilename2
		COPY FILE &cFilename TO &cFilename3
		DELETE FILE &cFilename
	ENDIF

	IF !lTesting
		IF !USED("ftpedilog")
			SELECT 0
			USE F:\edirouting\ftpedilog ALIAS ftpedilog
			INSERT INTO ftpedilog (TRANSFER,ftpdate,filename,acct_name,TYPE) VALUES ("214-"+cCustName,dt2,cFilename,UPPER(cCustName),"214")
			USE IN ftpedilog
		ENDIF
	ENDIF

	IF lTesting
		tsubject = "VF 214 *TEST* EDI from TGF as of "+dtmail
	ELSE
		tsubject = "VF 214 EDI from TGF as of "+dtmail
	ENDIF

	tmessage = "214 ("+cProcType+") EDI Info from TGF-CA, Truck WO# "+cWO_Num+CHR(10)
	tmessage = tmessage + "for coalition "+cMailName
	tmessage = tmessage +" has been created."+CHR(10)+"(Filename: "+cFilenameShort+")"
*!*	+CHR(10)+CHR(10)
*!*	tmessage = tmessage + "If you have any questions, please eMail or call 732-750-9000 x217."
	IF lTesting
		tmessage = tmessage + CHR(10)+CHR(10)+"This is a TEST RUN..."
	ENDIF

	IF lEmail
		DO FORM dartmail2 WITH tsendto,tFrom,tsubject,tcc,tattach,tmessage,"A"
	ENDIF

	RELEASE ALL LIKE c_CntrlNum,c_GrpCntrlNum
	RELEASE ALL LIKE nOrigSeq,nOrigGrpSeq
	WAIT CLEAR
	WAIT WINDOW cMailName+" 214 "+cProcType+" EDI File output complete" AT 20,60 TIMEOUT 2

	closefiles()

CATCH TO oErr
	IF lDoCatch
		lEmail = .T.
		DO ediupdate WITH "ERRHAND ERROR",.T.
		tsubject = cCustName+" Error ("+TRANSFORM(oErr.ERRORNO)+") at "+TTOC(DATETIME())
		tattach  = ""
		tsendto  = tsendtoerr
		tcc = tccerr
		tmessage = cCustName+" Error processing "+CHR(13)

		tmessage =tmessage+ "Try/CATCH Exeception Message:"+CHR(13)+;
			[  Error: ] + STR(oErr.ERRORNO) +CHR(13)+;
			[  LineNo: ] + STR(oErr.LINENO) +CHR(13)+;
			[  Message: ] + oErr.MESSAGE +CHR(13)+;
			[  Procedure: ] + oErr.PROCEDURE +CHR(13)+;
			[  Details: ] + oErr.DETAILS +CHR(13)+;
			[  StackLevel: ] + STR(oErr.STACKLEVEL) +CHR(13)+;
			[  LineContents: ] + oErr.LINECONTENTS+CHR(13)+;
			[  UserValue: ] + oErr.USERVALUE

		tsubject = "945 EDI Poller Error at "+TTOC(DATETIME())
		tattach  = ""
		DO FORM dartmail2 WITH tsendto,tFrom,tsubject,tcc,tattach,tmessage,"A"
		lEmail = .F.
	ENDIF
FINALLY
	FCLOSE(nFilenum)
	closefiles()
ENDTRY

&& END OF MAIN CODE SECTION



****************************
PROCEDURE num_incr_isa
****************************

	IF !USED("serfile")
		USE ("F:\3pl\data\serial\"+cCustName+"214_serial") IN 0 ALIAS serfile
	ENDIF
	SELECT serfile
	IF lTesting
		nOrigSeq = serfile.seqnum
		nOrigGrpSeq = serfile.grpseqnum
		lISAFlag = .F.
	ENDIF
	nISA_Num = serfile.seqnum
	c_CntrlNum = ALLTRIM(STR(serfile.seqnum))
	REPLACE serfile.seqnum WITH serfile.seqnum + 1 IN serfile
	SELECT manifest
ENDPROC

****************************
PROCEDURE num_incr_st
****************************

	IF !USED("serfile")
		USE ("F:\3pl\data\serial\"+cCustName+"214_serial") IN 0 ALIAS serfile
	ENDIF
	SELECT serfile
	IF lTesting
		nOrigSeq = serfile.seqnum
		nOrigGrpSeq = serfile.grpseqnum
		lSTFlag = .F.
	ENDIF
	c_GrpCntrlNum = ALLTRIM(STR(serfile.grpseqnum))
	REPLACE serfile.grpseqnum WITH serfile.grpseqnum + 1 IN serfile
	SELECT manifest
ENDPROC

****************************
PROCEDURE segmentget
****************************

	PARAMETER thisarray,lcKey,nLength

	FOR i = 1 TO nLength
		IF i > nLength
			EXIT
		ENDIF
		lnEnd= AT("*",thisarray[i])
		IF lnEnd > 0
			lcThisKey =TRIM(SUBSTR(thisarray[i],1,lnEnd-1))
			IF OCCURS(lcKey,lcThisKey)>0
				RETURN SUBSTR(thisarray[i],lnEnd+1)
				i = 1
			ENDIF
		ENDIF
	ENDFOR

	RETURN ""

****************************
PROCEDURE close214
****************************
	STORE  "SE"+cfd+ALLTRIM(STR(nSegCtr+1))+cfd+PADL(c_GrpCntrlNum,9,"0")+csegd TO cString
	DO cstringbreak

	STORE  "GE"+cfd+ALLTRIM(STR(nSTCount))+cfd+c_CntrlNum+csegd TO cString
	DO cstringbreak

	STORE  "IEA"+cfd+"1"+cfd+PADL(c_CntrlNum,9,"0")+csegd TO cString
	DO cstringbreak

	RETURN

****************************
PROCEDURE cstringbreak
****************************
	cLen = LEN(ALLTRIM(cString))

	FWRITE(nFilenum,cString)
	RETURN


****************************
PROCEDURE errormail
****************************
	PARAMETERS msgtype
*ASSERT .f.
	lDoCatch = .F.

*ASSERT .F. MESSAGE "At Errormail...Debug"
	tsubject = cSystemName+" 214 EDI File Error"
	tmessage = cDivision+" 214 EDI Info, WO# "+cWO_Num+CHR(10)
	tsendto = tsendtoerr
	tcc = tccerr
	DO CASE
		CASE msgtype = "MISSGROUP"
			tmessage = tmessage + "There was no Group Name in WO# "+cWO_Num
			DO ediupdate WITH msgtype,.T.,.F.
		CASE msgtype = "MISSACCT"
			tmessage = tmessage + "Account ID "+ALLTRIM(STR(nAcctNum))+" does not exist in ACCOUNT."+CHR(10)
			tmessage = tmessage + "Check account information submitted from PICKUP trigger."
			DO ediupdate WITH msgtype,.T.,.F.
		CASE msgtype = "MISSWO"
			tmessage = tmessage + "WO# "+cWO_Num+" does not exist in F:\WO\WODATA\MANIFEST."
			DO ediupdate WITH msgtype,.T.,.F.
		CASE msgtype = "MISSEQUIP"
			tmessage = tmessage + "Equipment not specified for WO# "+cWO_Num+CHR(10)
			tmessage = tmessage + "Data in MANIFEST must be corrected."
			DO ediupdate WITH msgtype,.T.,.F.
		CASE msgtype = "BAD TRIPID"
			tmessage = tmessage + "Trip ID does not exist in F:\WO\WODATA\MANIFEST."
			DO ediupdate WITH msgtype,.T.,.F.
		CASE msgtype = "MISSPICKUP"
			tmessage = tmessage + "WO# "+cWO_Num+" contains no pickup date."+CHR(10)
			tmessage = tmessage + "Data in MANIFEST must be corrected."
			DO ediupdate WITH msgtype,.T.,.F.
			IF lTesting
				lEmail = .F.
			ELSE
				tsendto = tsendtoerr
				tcc = tccerr
			ENDIF
	ENDCASE

	IF lTesting
		tmessage = tmessage + CHR(10)+CHR(10)+"This is TEST DATA only..."
	ENDIF
	IF lEmail
		DO FORM dartmail2 WITH tsendto,tFrom,tsubject,tcc,tattach,tmessage,"A"
	ENDIF
ENDPROC

**************************
PROCEDURE ediupdate
**************************
	PARAMETERS cFin_status, lIsError,lFClose
	IF !lTesting
		SELECT edi_trigger
		LOCATE
		IF lIsError
			REPLACE edi_trigger.processed WITH .T.,edi_trigger.created WITH .F.,;
				edi_trigger.fin_status WITH cFin_status,edi_trigger.errorflag WITH .F.;
				edi_trigger.when_proc WITH DATETIME() ;
				FOR edi_trigger.wo_num = nWO_Num AND INLIST(edi_type,cRunType+"214","214"+cRunType)
			IF nFilenum>0
				=FCLOSE(nFilenum)
			ENDIF
			IF FILE(cFilename)
				DELETE FILE &cFilename
			ENDIF
			closefiles()
		ELSE
			REPLACE edi_trigger.processed WITH .T.,edi_trigger.created WITH .T.,proc214 WITH .T.;
				edi_trigger.when_proc WITH DATETIME(),;
				edi_trigger.fin_status WITH cRunType+"214 CREATED";
				edi_trigger.errorflag WITH .F.,file214 WITH cFilename ;
				FOR edi_trigger.wo_num = nWO_Num AND INLIST(edi_type,cRunType+"214","214"+cRunType)
		ENDIF
	ENDIF
ENDPROC

*********************
PROCEDURE closefiles
*********************
	IF USED('ftpedilog')
		USE IN ftpedilog
	ENDIF
	IF USED('manifest')
		USE IN manifest
	ENDIF
	IF USED('serfile')
		USE IN serfile
	ENDIF
	IF USED('account')
		USE IN account
	ENDIF
	IF USED('xref')
		USE IN xref
	ENDIF
	IF USED('dellocs')
		USE IN dellocs
	ENDIF
	IF USED('unloccodes')
		USE IN unloccodes
	ENDIF
ENDPROC
