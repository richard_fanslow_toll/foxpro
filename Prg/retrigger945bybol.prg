Parameters lcBOL

*!*  If cBOL = .f.


*!*  ************************************************************************************
*!*   lcbol= getwonumber()
*!*    IF Empty(lcbol)  && returns 0 means cancel was pressed so get out of prg
*!*     return
*!*    endif
*!*  ***********************
*!*  EndIf 

If !used("edi_trigger")
  Use f:\3pl\data\edi_trigger.dbf in 0 ALIAS Edi_trigger
EndIf   
Select edi_trigger
Set Order to bol
Seek lcbol
If !Found()
  close data all 
  Wait window at 10,10 "BOL not found:--> "+lcbol
  return
EndIf

Wait window at 10,10 "Retriggering WO#: "+Alltrim(Transform(lcBOL)) nowait
If Found()
  replace comments with comments +Chr(13)+;
  "------------------------------"+Chr(13)+;
  "Last trigger: "+Ttoc(trig_time)+Chr(13)+;
  "Last trig by: "+Alltrim(trig_by)+Chr(13)+;
  "Last trigfrom: "+Alltrim(trig_from)+Chr(13)+;
  "Reason for retrigger: 945 counts off"

  replace fin_status with ""  for bol = lcbol
  replace errorflag  with .f. for bol = lcbol
  replace trig_from  with "MANUAL" for bol = lcbol
  replace trig_time  with Datetime() for bol = lcbol
  replace trig_by    with "PGAIDIS" for bol = lcbol
  replace processed  with .F. for bol = lcbol
Else
  Wait window at 10,10 "BOL not found"
EndIf 
Use in edi_trigger

************************************************************
*  FUNCTION getwonumber()
************************************************************
FUNCTION getwonumber()
LOCAL lnWo,lnRetVal
lcRetval=""
 DO WHILE .T.
   lcBOL=INPUTBOX('BOL #',"Enter a BOL to Retrigger the 945 with.........")   && WO num has to be an integer
   lcRetVal=lcBOL
    IF Empty(lcBOL)  && either nothing was put into the box or cancel was pressed
      lnAnswer=MESSAGEBOX("There is no BOL entered or Cancel was pressed"+ CHR(13)+;
                 "   Do you want to exit program ?",36)
      IF lnAnswer=6  && yes exit
       RETURN lcRetval  && return 0
      ELSE  && try again
       LOOP
      Endif
    ENDIF
    RETURN lcRetVal
 ENDDO
EndFunc
************************************************************
