lparameter lcoffice, xaccountid, xtest

*utilsetup("SENDINVENTORY")
xscreencaption=_screen.Caption
*_screen.caption="SENDINVENTORY for Account: "+xaccountid+" and Office: "+lcOffice

guserid="SENDINVENTORY"
gprocess=guserid
gdevelopment=.f.

*xaccountid = val(xaccountid)
lcwhpath = wf(lcoffice,xaccountid)
xmod = wf(lcoffice,xaccountid,,,,,,.t.)
goffice=xmod

if !xtest
	xsqlexec("select * from inven where mod='"+goffice+"'",,,"wh")
else
	use f:\tmptmp\paulg\inven in 0
	use f:\tmptmp\paulg\outship in 0
	use f:\tmptmp\paulg\outdet in 0
	use f:\tmptmp\paulg\project in 0
endif

if xaccountid = 4610
	acctlist = "4610,4694"
else
	acctlist = transform(xaccountid)
endif

* get inventory

select units, style, color, id, pack, availqty, allocqty, holdqty, 0000000 as pickedqty, 0000000 as spqty ;
	from inven where inlist(accountid,&acctlist) into cursor inventory readwrite

index on transform(units)+style+color+id+pack tag match

* inprocess is a cursor of all outship records where they are not yet delivered and also excluding "notonwip"

xsqlexec("select * from outship where mod='"+goffice+"' and inlist(accountid,"+acctlist+") " + ;
	"and del_date={} and notonwip=0 and qty#0 order by wo_num","inprocess",,"wh")

xsqlexec("select units, style, color, id, pack, totqty from outship, outdet " + ;
	"where outship.outshipid=outdet.outshipid " + ;
	"and outship.mod='"+goffice+"' and inlist(outship.accountid,"+acctlist+") " + ;
	"and del_date={} and notonwip=0 and qty#0","xdy",,"wh") 
	
select units, style, color, id, pack, sum(totqty) as pickedqty ;
	from xdy group by units, style, color, id, pack into cursor xunshipped

select xunshipped
scan
	=seek(transform(units)+style+color+id+pack,"inventory","match")
	replace pickedqty with xunshipped.pickedqty in inventory
endscan

* uncompleted special projects

xsqlexec("select outdet.wo_num, units, style, color, id, pack, totqty " + ;
	"from outdet, project where outdet.wo_num=project.wo_num and outdet.mod='"+goffice+"' " + ;
	"and completeddt={} and inlist(project.accountid,"+acctlist+")","xdy",,"wh")

select wo_num, units, style, color, id, pack, sum(totqty) as spqty ;
	from xdy group by 1,2,3,4,5,6 into cursor xsp

select xsp
scan
	=seek(transform(units)+style+color+id+pack,"inventory","match")
	replace spqty with xsp.spqty in inventory
endscan

set step on 

* copy and email inventory file

select inventory
delete for availqty=0 and pickedqty=0 and holdqty=0 and spqty=0

do case
case xaccountid = 4610
	lcfilename = "NANJING_"+lcoffice+"_"+ttoc(datetime(),1)+".846"
	lcfilename2 = "NANJING_"+lcoffice+"_inproc_"+ttoc(datetime(),1)+".846"
	tsendto = 'pgaidis@fmiint.com'

	xlsfilename = justfname(lcfilename)
	xlsfilename= "f:\ftpusers\NANJING\846out\"+xlsfilename+".xls"
	copy to "f:\ftpusers\NANJING\846out\NANJING.xls" fields style,color,id,pack,availqty,pickedqty,holdqty,spqty xls

	tmessage = "Updated inventory file in the Nanjing FTP Site.............."+chr(13)+chr(13)+;
	"This email is generated via an automated process"+chr(13)+"For changes in content or distribution please contact: Paul Gaidis @ 732-750-9000x143 "
	tsubject= "Nanjing LA Inventory Sent: " +ttoc(datetime())

case Inlist(xaccountid,6416,6417,6418)
  Do case
    Case xaccountid = 6416
     lcfilename = "CR8REC_6416_"+lcoffice+"_"+ttoc(datetime(),1)
     lcfilename2 = "CR8REC_6416_"+lcoffice+"_inproc_"+ttoc(datetime(),1)
    Case xaccountid = 6417
     lcfilename = "CR8REC_6417_"+lcoffice+"_"+ttoc(datetime(),1)
     lcfilename2 = "CR8REC_6417_"+lcoffice+"_inproc_"+ttoc(datetime(),1)
    Case xaccountid = 6418
     lcfilename = "CR8REC_6418_"+lcoffice+"_"+ttoc(datetime(),1)
     lcfilename2 = "CR8REC_6418_"+lcoffice+"_inproc_"+ttoc(datetime(),1)
 Endcase    

*  lcfilename = "CR8REC_"+lcoffice+"_"+ttoc(datetime(),1)
*  lcfilename2 = "CR8REC_"+lcoffice+"_inproc_"+ttoc(datetime(),1)

  select 0
  use f:\3pl\data\mailmaster
  locate for accountid=6416 and taskname="SENDINVENTORY"
  tsendto=alltrim(sendto)+iif(empty(sendtoalt),"",", "+alltrim(sendtoalt))
  tcc=alltrim(cc)+iif(empty(ccalt),"",", "+alltrim(ccalt))
  use in mailmaster

  select inventory

  xlsfilename = justfname(lcfilename)
  xlsfilename= "f:\ftpusers\CR8REC\INVENTORYFILES\"+xlsfilename+".xls"
  copy to &xlsfilename fields style,color,id,pack,availqty,pickedqty,holdqty,spqty xls

  tmessage = "Updated inventory file in the CR8REC FTP Site.............."+chr(13)+chr(13)+;
  "This email is generated via an automated process"+chr(13)+"For changes in content or distribution please contact: Paul Gaidis @ 732-750-9000x143 "
  tsubject= "CR8REC Inventory Sent: " +ttoc(datetime())

case xaccountid = 6665
	Set Step On 
  lcfilename = "2XU_"+lcoffice+"_"+ttoc(datetime(),1)
	lcfilename2 = "2XU_"+lcoffice+"_inproc_"+ttoc(datetime(),1)
	tsendto = 'pgaidis@fmiint.com'
*	tsendto = 'pgaidis@fmiint.com'

	xlsfilename = justfname(lcfilename)
	xlsfilename= "f:\ftpusers\2XU\846out\"+xlsfilename+".xls"
	copy to &xlsfilename fields style,color,id,pack,availqty,pickedqty,holdqty,spqty xls

	tmessage = "Updated inventory file in the 2XU FTP Site.............."+chr(13)+chr(13)+;
	"This email is generated via an automated process"+chr(13)+"For changes in content or distribution please contact: Paul Gaidis @ 732-750-9000x143 "
	tsubject= "2XU Inventory Sent: " +ttoc(datetime())

case xaccountid = 4677
	lcfilename =  "COURTAULDS_"+lcoffice+"_"+ttoc(datetime(),1)+".846"
	lcfilename2 = "COURTAULDS_"+lcoffice+"_inproc_"+ttoc(datetime(),1)+".846"
	tsendto = 'pgaidis@fmiint.com,jackie.barrett@courtaulds.com'

	xlsbasefilename = justfname(lcfilename)
	xlsfilename= "f:\ftpusers\Courtaulds\846out\"+xlsbasefilename+".xls"
	xlsfilenamearchive= "f:\ftpusers\Courtaulds\846out\archive\"+xlsbasefilename+".xls"

	csvfilename = justfname(lcfilename)
	delimitedfilename= justfname(lcfilename)
	csvfilename= "f:\ftpusers\Courtaulds\846out\"+csvfilename+".csv"
	delimitedfilename= "f:\ftpusers\Courtaulds\846out\"+delimitedfilename+".csv"

	export to &xlsfilename fields style,color,id,pack,availqty,pickedqty,holdqty,spqty for units = .t. type xls
	export to &xlsfilenamearchive fields style,color,id,pack,availqty,pickedqty,holdqty,spqty for units = .t. type xls
	copy to &delimitedfilename fields style,color,id,pack,availqty,pickedqty,holdqty,spqty for units = .t. delimited

	tsubject= "Courtaulds Inventory Sent: " +ttoc(datetime())
	tmessage = "Updated inventory file in the Courtaulds FTP Site.............."+chr(13)+chr(13)+;
	"This email is generated via an automated process"+chr(13)+"For changes in content or distribution please contact: Paul Gaidis @ 732-750-9000x143 "

case xaccountid = 6137
	tsendto = 'pgaidis@fmiint.com'
    lcfilename =  "LP_INVENTORY_"+ttoc(datetime(),1)+".XLS"
    xlsfilename= "f:\ftpusers\LINDSAYPHILLIPS\INVENTORY\"+lcfilename

    Select *,Space(12) as upc From inventory Into cursor inventory2 readwrite
    
    Select inventory2
    Goto top
    Scan
      if upcmastsql(6137,Padr(Alltrim(inventory2.style),20),Padr(Alltrim(inventory2.color),10),Padr(Alltrim(inventory2.id),10))
        replace inventory2.upc With upcmast.upc In inventory2
      endif
    Endscan
    
    Select inventory2
    Goto top
	export to &xlsfilename fields style,color,id,upc,pack,availqty,pickedqty,holdqty,spqty for units = .t. type xls

	tsubject= "Lindsay Phillips Inventory Sent: " +ttoc(datetime())
	tmessage = "Updated inventory file in the Lindsay Phillips FTP Site.............."+chr(13)+chr(13)+;
	"This email is generated via an automated process"+chr(13)+"For changes in content or distribution please contact: Paul Gaidis @ 732-750-9000x143 "

case xaccountid = 6135
	tsendto = 'pgaidis@fmiint.com'
    lcfilename =  "JIMMYCHOO_"+ttoc(datetime(),1)+".XLS"
    xlsfilename= "f:\ftpusers\JIMMYCHOO\INVENTORY\"+lcfilename

    Select *,Space(12) as upc From inventory Into cursor inventory2 readwrite
    
    Use f:\3pl\data\jchoostyles In 0
    
    Select inventory2
    Goto top
    Scan
      Select jchoostyles
      Locate for jchoostyles.style =inventory2.style And  jchoostyles.color = inventory2.color And jchoostyles.size = inventory2.id
      If Found()
        replace inventory2.upc With jchoostyles.upc In inventory2
      endif
    Endscan
    
    Select inventory2
    Goto top
	export to &xlsfilename fields style,color,id,upc,pack,availqty,pickedqty,holdqty,spqty for units = .t. type xls

	tsubject= "Jimmy Choo Inventory Sent: " +ttoc(datetime())
	tmessage = "Updated inventory file in the Jimmy Choo FTP Site.............."+chr(13)+chr(13)+;
	"This email is generated via an automated process"+chr(13)+"For changes in content or distribution please contact: Paul Gaidis @ 732-750-9000x143 "
otherwise
	export to ("f:\inven"+transform(xaccountid)) fields style,color,id,pack,availqty,pickedqty,holdqty,spqty type xls
	email("Dyoung@fmiint.com","INFO: sendinventory is not set up for accountid "+transform(xaccountid),,,,,.t.,,,,,.t.,,.t.)
endcase

if inlist(xaccountid,4677,5383,5746,6665,6416,6417,6418,6137)
	tattach = xlsfilename
else
	tattach = ""
endif
set step on 
tfrom ="TGF Corporate <fmicorporate@fmiint.com>"

if !empty(tattach)
	do form dartmail2 with tsendto,tfrom,tsubject,"",tattach,tmessage,"A"
endif

schedupdate()

on error
_screen.Caption=xscreencaption