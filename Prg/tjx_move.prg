DO M:\DEV\PRG\_SETVARS WITH .T.
PUBLIC cfilename,cfileoutname,NormalExit
SET STEP ON 
TRY
	lTesting = .F.
	lOverrideBusy = .F.
	NormalExit = .F.

	IF !lTesting
		IF USED('ftpsetup')
		USE IN ftpsetup
		endif
		SELECT 0
		USE F:\edirouting\ftpsetup SHARED
		cTransfer = "TJX-MOVE"
		LOCATE FOR ftpsetup.transfer = cTransfer
		IF ftpsetup.chkbusy = .T. AND !lOverrideBusy
			WAIT WINDOW "File is processing...exiting" TIMEOUT 2
			NormalExit = .T.
			THROW
		ENDIF
		REPLACE chkbusy WITH .T. FOR ftpsetup.transfer = cTransfer
		USE IN ftpsetup
	ENDIF

	cDirIn = "F:\FTPUSERS\TJMAX\IN\"
	CD &cDirIn
	cCustname = "TJMAX"

	_SCREEN.WINDOWSTATE=1
	_SCREEN.CAPTION="TJX File Move Process"

	nFound = ADIR(ary1,'*.*')
	IF nFound = 0
		CLOSE DATABASES ALL
		WAIT WINDOW "No files found to process...exiting" TIMEOUT 2
		NormalExit = .t.
		throw
	ENDIF

	len1 = ALEN(ary1,1)

	SET STEP ON 
	FOR i = 1 TO len1
		ASSERT .F. MESSAGE "In move loop"
		cfilename = ALLT(ary1[i,1])
		WAIT WINDOW "Processing "+cfilename TIMEOUT 1

		xfile = (cDirIn+cfilename)
		cString = FILETOSTR(xfile)

** needed to add the If/endif here as we were receiving multiple docs(ISAs) different GS groups in a single file 

    If "GS*SH"$cString  && 856 files
      cDirOut = ("F:\FTPUSERS\TJMAX\856in\")
      cfileoutname = (cDirOut+cfilename)
      COPY FILE [&xfile] TO [&cfileoutname]
    Endif 

    If "GS*PV"$cString  && 250 files
      cDirOut = ("F:\FTPUSERS\TJMAX\250in\")
      cfileoutname = (cDirOut+cfilename)
      COPY FILE [&xfile] TO [&cfileoutname]
    Endif 

    If "GS*FA"$cString  && 997 files
      cDirOut = ("F:\FTPUSERS\TJMAX\997IN\")
      cfileoutname = (cDirOut+cfilename)
      COPY FILE [&xfile] TO [&cfileoutname]
    Endif 

    If "GS*PO"$cString  && 850 files
      cDirOut = ("F:\FTPUSERS\TJMAX\943in\")
      cfileoutname = (cDirOut+cfilename)
      COPY FILE [&xfile] TO [&cfileoutname]
    Endif 

    If "GS*PO"$cString  && 850 files
      cDirOut = ("F:\FTPUSERS\TJMAX\943in\")
      cfileoutname = (cDirOut+cfilename)
      COPY FILE [&xfile] TO [&cfileoutname]
    Endif 

    If "GS*AR"$cString  && 250 files
      cDirOut = ("F:\FTPUSERS\TJMAX\250in\")
      cfileoutname = (cDirOut+cfilename)
      COPY FILE [&xfile] TO [&cfileoutname]
    Endif 

    If "GS*SO"$cString  && 317 files
      cDirOut = ("F:\FTPUSERS\TJMAX\317in\")
      cfileoutname = (cDirOut+cfilename)
      COPY FILE [&xfile] TO [&cfileoutname]
    Endif 

    If "GS*SC"$cString  && 832 files
      cDirOut = ("F:\FTPUSERS\TJMAX\stylemaster\")
      cfileoutname = (cDirOut+cfilename)
      COPY FILE [&xfile] TO [&cfileoutname]
    Endif 
		
		WAIT WINDOW "File will be moved to folder: "+cDirOut NOWAIT

		IF FILE(cfileoutname)
			IF FILE(xfile)
				DELETE FILE [&xfile]
			ENDIF
		ENDIF
	ENDFOR

	WAIT WINDOW "TJX file move process complete" TIMEOUT 1
	SELECT 0
	USE F:\edirouting\ftpsetup SHARED
	If !Used("ftpsetup")
      USE F:\edirouting\ftpsetup SHARED In 0 
    ENDIF
	cTransfer = "TJX-MOVE"
	REPLACE chkbusy WITH .F. FOR ftpsetup.transfer = cTransfer
	CLOSE DATABASES ALL
	WAIT CLEAR
	NormalExit = .T.
CATCH TO oErr
	IF !NormalExit
		ASSERT .F. MESSAGE "At CATCH..."
		SET STEP ON

		tsubject = cCustname+" File Move Error ("+TRANSFORM(oErr.ERRORNO)+") at "+TTOC(DATETIME())
		tattach  = ""
		SELECT 0
		USE F:\3pl\DATA\mailmaster ALIAS mm
		LOCATE FOR mm.edi_type = "MISC" AND mm.taskname = "MJPLERROR"
		tsendto = IIF(mm.use_alt,TRIM(mm.sendtoalt),TRIM(mm.sendto))
		tcc = IIF(mm.use_alt,TRIM(mm.ccalt),TRIM(mm.cc))
		USE IN mm
		tmessage = cCustname+" File Move Error..... Please fix me........!"
		lcSourceMachine = SYS(0)
		lcSourceProgram = SYS(16)

		tmessage =tmessage+ "Try/CATCH Exeception Message:"+CHR(13)+;
			[  Message: ] + oErr.MESSAGE +CHR(13)+;
			[  Procedure: ] + oErr.PROCEDURE +CHR(13)+;
			[  LineNo: ] + STR(oErr.LINENO) +CHR(13)+;
			[  Error: ] + STR(oErr.ERRORNO) +CHR(13)+;
			[  Details: ] + oErr.DETAILS +CHR(13)+;
			[  StackLevel: ] + STR(oErr.STACKLEVEL) +CHR(13)+;
			[  LineContents: ] + oErr.LINECONTENTS+CHR(13)+;
			[  UserValue: ] + oErr.USERVALUE+CHR(13)+;
			[  Computer:  ] +lcSourceMachine+CHR(13)+;
			[  940 file:  ] +xfile+CHR(13)+;
			[  Program:   ] +lcSourceProgram

		tattach  = ""
		tFrom ="TGF EDI Operations <toll-edi-ops@tollgroup.com>"
		DO FORM M:\DEV\frm\dartmail2 WITH tsendto,tFrom,tsubject,tcc,tattach,tmessage,"A"
	ELSE
		WAIT WINDOW AT 10,10 "Normal Exit " TIMEOUT 2
	ENDIF
FINALLY
	CLOSE DATABASES ALL
ENDTRY
