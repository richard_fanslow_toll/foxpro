Parameters eventcode,IBD,ponum,asnnum,offset

If Inlist(Type("offset"),"U","L")
  offset = 0000000  && 86400 = one day
Else
  offset= offset*86400
Endif

If Inlist(eventcode,"2903","2904") && these events trigger by IBD or ASN
  lcIBD= IBD &&"6000026365"
  lcCode = eventcode
Endif

If Inlist(eventcode,"2800") && these events trigger by IBD or ASN
  lcIBD= IBD &&"6000026365"
  lcCode = eventcode
Endif

If Inlist(eventcode,"1500","1510")  && these events trigger by PO
  lcIBD= IBD &&"6000026365"
  lcPO= ponum &&"6000026365"
  lcCode = eventcode
Endif

*!*    lcCode= "2903"
*!*    lcCode= "2904"
*!*    lcCode= "1500"
*!*    lcCode= "1510"

Set Date YMD
lcTStamp = Strtran(Ttoc(Datetime()),"/","-")
lcTStamp = Strtran(lcTStamp," ","T")
lcTStamp = Strtran(lcTStamp,"TPM","")
lcTStamp = Alltrim(Strtran(lcTStamp,"TAM",""))

* 100000

lcEventTime = Strtran(Ttoc(Datetime()+offset),"/","-")
lcEventTime = Strtran(lcEventTime," ","T")
lcEventTime = Strtran(lcEventTime,"TPM","")
lcEventTime = Alltrim(Strtran(lcEventTime,"TAM",""))

lcHeader = Filetostr("f:\underarmour\xmltemplates\event.txt")

lcHeader = Strtran(lcHeader,"<DOCDATETIME>",lcTStamp)
If Inlist(lcCode,"1500","1510")
  lcHeader = Strtran(lcHeader,"<IBDNUMBER>",lcPO)
Else
  lcHeader = Strtran(lcHeader,"<IBDNUMBER>",lcIBD)
Endif
lcHeader = Strtran(lcHeader,"<EVENTDATETIME>",lcEventTime)
lcHeader = Strtran(lcHeader,"<EVENTLOCATION>","TOLLMIA")
lcHeader = Strtran(lcHeader,"<EVENTCODE>",lcCode) &&lcEventCodeUsed)
*!* For 214 file types:

lcFilestamp = Ttoc(Datetime())
lcFilestamp = Strtran(lcFilestamp,"/","")
lcFilestamp = Strtran(lcFilestamp,":","")
lcFilestamp = Strtran(lcFilestamp," ","")

If lcCode ="2800"
  Strtofile(lcHeader,"f:\ftpusers\underarmour\outbound\out2\LSPPOD_"+lcIBD+"_"+lcCode+"_"+lcFilestamp+".xml")
Else
  Strtofile(lcHeader,"f:\ftpusers\underarmour\outbound\out2\LSPEVT_"+lcIBD+"_"+lcCode+"_"+lcFilestamp+".xml")
Endif
