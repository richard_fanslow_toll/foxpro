**eodRpt.prg
**
**Daily Warehousing Stats
**
Parameters cAcctNum, cOffice, cBuilding, dStartDt, dEndDt, cRptType
**cAcctNum: acct no. (char)
*cRptType: "XLS", "RPT"

Wait window "Opening necessary databases..." nowait noclear

select * from csroffices where locale = coffice and priority = 1 into cursor xtemp
cofficename = city
use in xtemp

do case
case !empty(coffice) and !empty(cacctnum)
	cwhdir = wf(coffice, val(cacctnum))
case !empty(cbuilding)
	cwhdir = "f:\wh"+cbuilding+"\whdata\"
case !inlist(coffice,"N","C")
	cwhdir = "f:\wh"+coffice+"\whdata\"
otherwise
	return .f.
endcase
open database &cwhdir.wh

xmod = substr(cwhdir,6,1)

Wait window "Gathering data..." nowait noclear

cAcctFilter = Iif(Empty(cAcctNum), "", "accountid = "+cAcctNum)
do case
case !empty(cAcctNum) and (inlist(val(cAcctNum),&gmoretacctlist) or inlist(val(cAcctNum),&gmoretacctlist2)) and messagebox("Would you like to combine all Moret accounts?", 4+16, gcMsgTitle) = 6
	cAcctFilter = "(inlist(e.accountid,"+gmoretacctlist+") or inlist(e.accountid,"+gmoretacctlist2+"))"
endcase

if empty(cAcctFilter)
	cAcctFilter=".t."
endif
set step on 
xsqlexec("select * from eodinven e where "+cAcctFilter+" and between(eodDt,{"+dtoc(dStartDt)+"},{"+dtoc(dEndDt)+"}) and mod='"+xmod+"'",,,"wh")

select *, padr(acctname(accountid),30) as acctname from eodinven order by acctname, eoddt into cursor csreodinven

Wait Clear

Do case
  Case Eof()
	strMsg = "No data found for the specified criteria."
	=MessageBox(strMsg, 64, "Daily EODRPT Report")
  Case cRptType = "RPT"
	if !lemail
		Set Printer to default
		Report Form eodRpt preview
	else
		cFileName="eodrpt"+SUBSTR(SYS(2015),3,10)
		DO frxToPdf with .f., "eodrpt", tempfox, cFileName, .t.

		Dimension aAttach (1,1)
		aAttach(1,1) = tempfox+cFileName

		cSubject = "Daily Warehousing Stats"
		DO email with "", cSubject, "", aAttach, .t., "", "", .F.

		Release aAttach
	endif

  Otherwise &&"XLS"
	PUBLIC oExcel
	oExcel = CreateObject("Excel.Application")

	STORE tempfox+SUBSTR(SYS(2015), 3, 10)+".xls" TO cFile
	Copy To &cFile type xl5

	oWorkbook = oExcel.Workbooks.Open(cFile)
	oExcel.visible = .t.
	RELEASE oWorkbook, oExcel
EndCase


USE in eodInven

**close the database
Set Database To WH
Close Databases

Return
