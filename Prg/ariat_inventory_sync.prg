* Generate this file every Saturday at 9:30pm west coast time, 12:30am east cost time, per Gopi from Ariat.
*
* started by PG, finished by MB 05/26/2015
*
* EXE = F:\UTIL\ARIAT\ARIAT_INVENTORY_SYNC.EXE

runack("ARIAT_INVENTORY_SYNC")

LOCAL lnError, lcProcessName, loARIAT_INVENTORY_SYNC

DECLARE INTEGER CreateMutex IN Win32API ;
	STRING @lpMutexAttributes, ;
	STRING bInitialOwner, ;
	STRING @lpName

DECLARE INTEGER GetLastError IN Win32API

* if running in EXE check to see if already running
IF _VFP.STARTMODE > 0 THEN
	lcProcessName = "ARIAT_INVENTORY_SYNC"
	CreateMutex(0,CHR(1),@lcProcessName)
	lnError = GetLastError()
	IF lnError = 183
		WAIT WINDOW TIMEOUT 10 "Lifefactory Inventory, Build QTY & Schedule process is already running..."
		RETURN .F.
	ENDIF
ENDIF

utilsetup("ARIAT_INVENTORY_SYNC")

goffice="K"

loARIAT_INVENTORY_SYNC = CREATEOBJECT('ARIAT_INVENTORY_SYNC')
loARIAT_INVENTORY_SYNC.MAIN()

schedupdate()

CLOSE DATABASES ALL
RETURN

#DEFINE LOGIT 1
#DEFINE WAITIT 2
#DEFINE NOWAITIT 4
#DEFINE SENDIT 8
#DEFINE CRLF CHR(13) + CHR(10)
#DEFINE LF CHR(10)
#DEFINE RETURN_DATA_MANDATORY .F.
#DEFINE RETURN_DATA_NOT_MANDATORY .T.
#DEFINE DAILY_OT_MULTIPLIER 1.5
#DEFINE DAILY_OT20_MULTIPLIER 2.0
#DEFINE xlContinuous 1
#DEFINE xlMedium -4138
#DEFINE xlDiagonalDown 5
#DEFINE xlDiagonalUp 6
#DEFINE xlInsideHorizontal 12
#DEFINE xlInsideVertical 11
#DEFINE xlThick 4
#DEFINE xlThin 2
#DEFINE xlNone -4142
#DEFINE xlEdgeBottom 9
#DEFINE xlEdgeLeft 7
#DEFINE xlEdgeRight 10
#DEFINE xlEdgeTop 8

DEFINE CLASS ARIAT_INVENTORY_SYNC AS CUSTOM

	cProcessName = 'ARIAT_INVENTORY_SYNC'

	lTestMode = .F.  && .F. FOR NORMAL OPERATION, .T. TO SUPPRESS ALL BUT DEVELOPER EMAILS AND CHANGE INPUT TABLE, ETC.

	lAutoYield = .T.

	cStartTime = TTOC(DATETIME())

	* processing properties
	cProcessDesc = "Ariat Inventory Sync process"
	nAccountID = 6532
	cAccountID = "6532"

	* object properties
	oExcel = NULL
	oWorkbook = NULL
	osheet1 = NULL

	* file properties
	nFileHandle = 0
	cFiletoSaveAs = ""

	* date properties
	dtNow = DATETIME()

	dToday = DATE()

	cToday = DTOC(DATE())

	* wait window properties
	nWaitWindowTimeout = 2
	lWaitWindowIsOn = .T.

	* logfile properties
	lLoggingIsOn = .T.
	cLogFile = 'F:\UTIL\ARIAT\LOGFILES\ARIAT_INVENTORY_SYNC_LOG.TXT'

	* INTERNAL email properties
	lSendInternalEmailIsOn = .T.
	cFrom = 'Mark.Bennett@Tollgroup.com'
	cSendTo = 'mbennett@fmiint.com, doug.wachs@tollgroup.com'
	cCC = ''
	cSubject = ''
	cAttach = ''
	cBodyText = ''
	cTopBodyText = ''

	cCOMPUTERNAME = ''
	cUSERNAME = ''

	FUNCTION INIT
		IF NOT DODEFAULT()
			RETURN .F.
		ENDIF
		WITH THIS
			CLEAR
			*SET RESOURCE OFF
			CLOSE DATA
			SET CENTURY ON
			SET DATE AMERICAN
			SET HOURS TO 24
			SET ANSI ON
			SET TALK OFF
			SET DELETED ON
			SET CONSOLE OFF
			SET EXCLUSIVE OFF
			SET SAFETY OFF
			SET EXACT OFF
			SET STATUS BAR ON
			SET SYSMENU OFF
			SET ENGINEBEHAVIOR 70
			_VFP.AUTOYIELD = .lAutoYield
			.cCOMPUTERNAME = GETENV("COMPUTERNAME")
			.cUSERNAME = GETENV("USERNAME")
			IF .lTestMode THEN
				.cSendTo = 'mbennett@fmiint.com'
				.cCC = ''
				.cLogFile = 'F:\UTIL\ARIAT\ARIAT_INVENTORY_SYNC_LOG_TESTMODE.TXT'
			ENDIF
			.lLoggingIsOn = .lLoggingIsOn AND NOT EMPTY(.cLogFile)
		ENDWITH
	ENDFUNC


	FUNCTION DESTROY
		WITH THIS
			IF .lLoggingIsOn  THEN
				SET ALTERNATE OFF
				SET ALTERNATE TO
			ENDIF
		ENDWITH
		DODEFAULT()
	ENDFUNC


	FUNCTION MAIN

		WITH THIS
			LOCAL lnNumberOfErrors, lnAccountID, ldToday, lcAccountID
			LOCAL lcOffice, lcWHpath, lcINVENfilename, lcInvenOutPath

			TRY
				lnNumberOfErrors = 0

				IF .lLoggingIsOn THEN
					SET ALTERNATE TO (.cLogFile) ADDITIVE
					SET ALTERNATE ON
				ENDIF

				.cSubject = .cProcessDesc + ' for ' + DTOC(.dToday)

				.TrackProgress(.cProcessDesc + ' started....', LOGIT+SENDIT+NOWAITIT)
				.TrackProgress('COMPUTERNAME = ' + .cCOMPUTERNAME, LOGIT+SENDIT)
				.TrackProgress('USERNAME = ' + .cUSERNAME, LOGIT+SENDIT)
				.TrackProgress('PROJECT = ARIAT_INVENTORY_SYNC', LOGIT+SENDIT)
				IF .lTestMode THEN
					.TrackProgress('                        TEST MODE                        ', LOGIT+SENDIT)
				ENDIF

				lnAccountID = .nAccountID
				lcAccountID = .cAccountID

				ldToday = .dToday

				lcOffice = "K"

				lcWHpath = wf(lcOffice,lnAccountID)
				.TrackProgress('WH path = ' + lcWHpath, LOGIT+SENDIT)

				xsqlexec("select * from inven where mod='K' and units=1",,,"wh")
				xsqlexec("select * from invenloc where mod='K' and locqty#0",,,"wh")
				xsqlexec("select * from project where mod='K'","project",,"wh")

				IF .lTestMode THEN
					lcInvenOutPath = "F:\UTIL\ARIAT\TESTFTPOUT\"
				ELSE
					lcInvenOutPath = "F:\FTPUSERS\ARIAT\FTPOUT\"
				ENDIF
				
				lcINVENfilename = lcInvenOutPath + "ARIAT_INVENTORY_"+STRTRAN(DTOC(ldToday),"/")+".XLS"
				
				.TrackProgress('Inventory Filename = ' + lcINVENfilename, LOGIT+SENDIT)

				*!*	* wip detail
				*!*	Select outship.wo_num,ship_ref,PICKED,upc,Style,Color,Id,totqty From outship Left Outer Join outdet On outdet.outshipid = outship.outshipid;
				*!*	WHERE outship.accountid = 6532 And units and Empty(del_date) and !notonwip and totqty > 0 into Cursor ariatstats Readwrite
				*!*	Select ariatstats
				*!*	wipfilename= "f:\ftpusers\ARIAT\846out\wip_detail_"+Strtran(Dtoc(Date()),"/")+".xls"
				*!*	export to &wipfilename Type Xl5  &&f:\ftpusers\ariat\846out\wip_detail.xls type xl5


				* get inventory

				SELECT units,SPACE(12) AS upc, STYLE, COLOR, ID, PACK, totqty, availqty, allocqty, holdqty, 0000000 AS pickedqty, 0000000 AS spqty ;
					FROM inven WHERE (accountid = lnAccountID) AND units INTO CURSOR inventory READWRITE

				SELECT inventory
				INDEX ON TRANSFORM(units)+STYLE+COLOR+ID+PACK TAG match

				* inprocess is a cursor of all outship records where they are not yet delivered and also excluding "notonwip"

				xsqlexec("select * from outship where mod='"+goffice+"' and accountid="+transform(lnAccountID)+" " + ;
					"and del_date={} and notonwip=0 and qty#0 and pulled=1 order by wo_num","inprocess",,"wh")

				xsqlexec("select units, style, color, id, pack, totqty from outship, outdet " + ;
					"where outship.outshipid=outdet.outshipid " + ;
					"and outship.mod='"+goffice+"' and outship.accountid="+transform(lnAccountID)+" " + ;
					"and del_date={} and notonwip=0 and qty#0 and pulled=1","xdy",,"wh") 
					
				select units, style, color, id, pack, sum(totqty) as pickedqty ;
					from xdy group by units, style, color, id, pack into cursor xunshipped
				
				SELECT xunshipped
				SCAN
					IF SEEK(TRANSFORM(units)+STYLE+COLOR+ID+PACK,"inventory","match") THEN
						REPLACE pickedqty WITH pickedqty+xunshipped.pickedqty IN inventory
					ENDIF
				ENDSCAN

				* uncompleted special projects

				xsqlexec("select outdet.wo_num, units, style, color, id, pack, totqty " + ;
					"from outdet, project where outdet.mod='"+goffice+"' " + ;
					"and outdet.wo_num=project.wo_num and completeddt={} and outdet.accountid="+transform(lnAccountID),"xdy",,"wh")

				select wo_num, units, style, color, id, pack, sum(totqty) as spqty ;
					from xdy group by 1,2,3,4,5,6 into cursor xsp

				SELECT xsp
				SCAN
					IF SEEK(TRANSFORM(units)+STYLE+COLOR+ID+PACK,"inventory","match") THEN
						REPLACE spqty WITH xsp.spqty IN inventory
					ENDIF
				ENDSCAN


				SELECT inventory
				DELETE FOR availqty=0 AND pickedqty=0 AND holdqty=0 AND spqty=0

				* populate UPC
				SELECT inventory
				SCAN FOR EMPTY(upc)
					* passing accountid as char was crashing upcmastsql; changed to numeric accountid 6/19/2017
					*if upcmastsql(lcAccountID,STYLE,COLOR,ID)
					if upcmastsql(lnAccountID,STYLE,COLOR,ID)
						REPLACE inventory.upc WITH upcmast.upc IN inventory
					ENDIF
				ENDSCAN
************************************************** Added 08/13/15  TM  as per Cheri and Ariat - add non Quality hold back into availqty  
SELECT * FROM invenloc WHERE accountid=6532 AND locqty!=0 AND hold AND INLIST(whseloc,'QLTYUNT','QUALITY')  INTO CURSOR zz1 READWRITE

SELECT style,color,sum(locqty* INT(VAL(pack))) as actualholdqty FROM zz1 GROUP BY 1,2 INTO CURSOR zz2 READWRITE
SELECT a.*,actualholdqty FROM inventory a FULL JOIN zz2 b ON a.style=b.style AND a.color=b.color INTO CURSOR c1 READWRITE
REPLACE actualholdqty WITH 0 FOR ISNULL(actualholdqty)
REPLACE availqty WITH availqty+holdqty IN c1 FOR actualholdqty=0 AND holdqty!=0
REPLACE holdqty WITH 0 IN c1 FOR actualholdqty=0 AND holdqty!=0
REPLACE availqty WITH availqty+holdqty-actualholdqty FOR holdqty!=0 AND actualholdqty!=0
REPLACE holdqty WITH actualholdqty IN c1 FOR actualholdqty!=0
REPLACE totqty WITH totqty+pickedqty FOR pickedqty!=0
*SET STEP ON
SELECT units, upc,style,color,id,pack,totqty, availqty, allocqty, holdqty,pickedqty,spqty FROM c1 INTO CURSOR INVENTORY READWRITE

*SET STEP ON 
**************************************************End "HOLD"
				* copy and email inventory file
				COPY TO (lcINVENfilename) FIELDS upc,STYLE,COLOR,ID,PACK,totqty,availqty,allocqty,holdqty,pickedqty XL5


				IF FILE(lcINVENfilename) THEN
					.TrackProgress('Successfully created: ' + lcINVENfilename, LOGIT+SENDIT)
					.cAttach = lcINVENfilename
				ELSE
					.TrackProgress('!!! ERROR: there was a problem creating: ' + lcINVENfilename, LOGIT+SENDIT)
				ENDIF


				.TrackProgress(.cProcessDesc + ' ended normally....', LOGIT+SENDIT+NOWAITIT)

			CATCH TO loError

				.TrackProgress('There was an error.',LOGIT+SENDIT)
				.TrackProgress('The Error # is: ' + TRANSFORM(loError.ERRORNO), LOGIT+SENDIT)
				.TrackProgress('The Error Message is: ' + TRANSFORM(loError.MESSAGE), LOGIT+SENDIT)
				.TrackProgress('The Error occurred in Program: ' + TRANSFORM(loError.PROCEDURE), LOGIT+SENDIT)
				.TrackProgress('The Error occurred at line #: ' + TRANSFORM(loError.LINENO), LOGIT+SENDIT)
				lnNumberOfErrors = lnNumberOfErrors + 1
				IF TYPE('.oExcel') = 'O' AND NOT ISNULL(.oExcel) THEN
					.oExcel.QUIT()
				ENDIF

			ENDTRY

			WAIT CLEAR
			***************** INTERNAL email results ******************************
			.TrackProgress('About to send status email.',LOGIT)
			.TrackProgress('==================================================================================================================', SENDIT)
			.TrackProgress('The logfile is: ' + .cLogFile, LOGIT+SENDIT)
			.TrackProgress(.cProcessDesc + ' started: ' + .cStartTime, LOGIT+SENDIT)
			.TrackProgress(.cProcessDesc + ' finished: ' + TTOC(DATETIME()), LOGIT+SENDIT)

			IF .lSendInternalEmailIsOn THEN
				* try to trap error from not having dartmail dll's registered on user's pc...
				TRY
					.cBodyText = .cTopBodyText + CRLF + .cBodyText

					DO FORM dartmail2 WITH .cSendTo,.cFrom,.cSubject,.cCC,.cAttach,.cBodyText,"A"
					.TrackProgress('Sent status email.',LOGIT)
				CATCH TO loError
					.TrackProgress('There was an error sending the status email.',LOGIT)
					.TrackProgress('The Error # is: ' + TRANSFORM(loError.ERRORNO), LOGIT)
					.TrackProgress('The Error Message is: ' + TRANSFORM(loError.MESSAGE), LOGIT)
					.TrackProgress('The Error occurred in Program: ' + TRANSFORM(loError.PROCEDURE), LOGIT)
					.TrackProgress('The Error occurred at line #: ' + TRANSFORM(loError.LINENO), LOGIT)
					lnNumberOfErrors = lnNumberOfErrors + 1
				ENDTRY

			ELSE
				.TrackProgress('Did not send status email: THIS.lSendInternalEmailIsOn = FALSE.',LOGIT)
			ENDIF

		ENDWITH
		RETURN
	ENDFUNC && main


	PROCEDURE TrackProgress
		* do any combination of Wait Window, writing to logfile, and adding to body of email,
		* based on nFlags parameter.
		LPARAMETERS tcExpression, tnFlags
		WITH THIS
			IF BITAND(tnFlags,LOGIT) = LOGIT THEN
				IF .lLoggingIsOn THEN
					?
					? .cProcessName + "  " + TTOC(DATETIME()) + ": " + tcExpression
				ENDIF
			ENDIF
			IF .lWaitWindowIsOn THEN
				IF BITAND(tnFlags,NOWAITIT) = NOWAITIT THEN
					WAIT WINDOW tcExpression NOWAIT
				ENDIF
				IF BITAND(tnFlags,WAITIT) = WAITIT THEN
					WAIT WINDOW tcExpression TIMEOUT .nWaitWindowTimeout
				ENDIF
			ENDIF
			IF BITAND(tnFlags,SENDIT) = SENDIT THEN
				IF .lSendInternalEmailIsOn THEN
					.cBodyText = .cBodyText + tcExpression + CRLF + CRLF
				ENDIF
			ENDIF
		ENDWITH
	ENDPROC  &&  TrackProgress


ENDDEFINE

*!*	**** OLD CODE BELOW
*!*	RETURN

*!*	lcOffice = "K"
*!*	xaccountid = "6532"
*!*	xtest=.T.

*!*	utilsetup("ARIAT_INVENTORY_SYNC")

*!*	xscreencaption=_SCREEN.CAPTION
*!*	_SCREEN.CAPTION="ARIAT Inventory Sync Files for Account: "+xaccountid+" and Office: "+lcOffice

*!*	guserid="ARIAT_INVEN_SYNC"
*!*	gprocess=guserid
*!*	gdevelopment=.F.

*!*	xaccountid = VAL(xaccountid)
*!*	lcWHpath = wf(lcOffice,xaccountid)

*!*	IF !xtest
*!*		USE (lcWHpath+"outship") IN 0 ORDER acctpt
*!*		USE (lcWHpath+"outdet") IN 0 ORDER outshipid
*!*		USE (lcWHpath+"project") IN 0
*!*		USE (lcWHpath+"inven") IN 0
*!*	ELSE
*!*		USE F:\tmptmp\paulg\inven IN 0
*!*		USE F:\tmptmp\paulg\outship IN 0
*!*		USE F:\tmptmp\paulg\outdet IN 0
*!*		USE F:\tmptmp\paulg\PROJECT IN 0
*!*	ENDIF

*!*	USE F:\wh\upcmast IN 0

*!*	*!*	* wip detail
*!*	*!*	Select outship.wo_num,ship_ref,PICKED,upc,Style,Color,Id,totqty From outship Left Outer Join outdet On outdet.outshipid = outship.outshipid;
*!*	*!*	WHERE outship.accountid = 6532 And units and Empty(del_date) and !notonwip and totqty > 0 into Cursor ariatstats Readwrite
*!*	*!*	Select ariatstats
*!*	*!*	wipfilename= "f:\ftpusers\ARIAT\846out\wip_detail_"+Strtran(Dtoc(Date()),"/")+".xls"
*!*	*!*	export to &wipfilename Type Xl5  &&f:\ftpusers\ariat\846out\wip_detail.xls type xl5


*!*	* get inventory

*!*	SELECT units,SPACE(12) AS upc, STYLE, COLOR, ID, PACK, totqty, availqty, allocqty, holdqty, 0000000 AS pickedqty, 0000000 AS spqty ;
*!*		FROM inven WHERE accountid=6532 AND units INTO CURSOR inventory READWRITE

*!*	SELECT inventory
*!*	INDEX ON TRANSFORM(units)+STYLE+COLOR+ID+PACK TAG match

*!*	* inprocess is a cursor of all outship records where they are not yet delivered and also excluding "notonwip"
*!*	SELECT * FROM outship ;
*!*		WHERE accountid=6532 AND EMPTY(del_date) AND !notonwip AND qty#0 AND pulled ;
*!*		ORDER BY wo_num INTO CURSOR inprocess READWRITE

*!*	SELECT units, upc,STYLE, COLOR, ID, PACK, SUM(totqty) AS pickedqty ;
*!*		FROM outdet, inprocess ;
*!*		WHERE outdet.outshipid= inprocess.outshipid ;
*!*		GROUP BY units, STYLE, COLOR, ID, PACK ;
*!*		INTO CURSOR xunshipped

*!*	SELECT xunshipped
*!*	SCAN
*!*		IF SEEK(TRANSFORM(units)+STYLE+COLOR+ID+PACK,"inventory","match") THEN
*!*			REPLACE pickedqty WITH pickedqty+xunshipped.pickedqty IN inventory
*!*		ENDIF
*!*	ENDSCAN


*!*	* uncompleted special projects
*!*	SELECT outdet.wo_num, units, STYLE, COLOR, ID, PACK, SUM(totqty) AS spqty ;
*!*		FROM outdet, PROJECT ;
*!*		WHERE outdet.wo_num=PROJECT.wo_num ;
*!*		AND PROJECT.accountid=6532 ;
*!*		AND EMPTY(completeddt) ;
*!*		GROUP BY 1,2,3,4,5,6 ;
*!*		INTO CURSOR xsp

*!*	SELECT xsp
*!*	SCAN
*!*		IF SEEK(TRANSFORM(units)+STYLE+COLOR+ID+PACK,"inventory","match") THEN
*!*			REPLACE spqty WITH xsp.spqty IN inventory
*!*		ENDIF
*!*	ENDSCAN

*!*	* copy and email inventory file

*!*	SELECT inventory
*!*	DELETE FOR availqty=0 AND pickedqty=0 AND holdqty=0 AND spqty=0

*!*	SELECT inventory
*!*	SCAN FOR EMPTY(upc)
*!*		=SEEK("6532"+STYLE+COLOR+ID,"upcmast","stylecolid")
*!*		IF FOUND("upcmast")
*!*			REPLACE inventory.upc WITH upcmast.upc
*!*		ENDIF
*!*	ENDSCAN


*!*	*!*	lcfilename = "ARIAT_"+lcOffice+"_inventory"

*!*	*!*	lcfilename2 = "ARIAT_"+lcOffice+"_inproc_"+TTOC(DATETIME(),1)


*!*	*xlsfilename= "f:\ftpusers\ARIAT\846out\ariat_inventory_"+STRTRAN(DTOC(DATE()),"/")+".xls"
*!*	xlsfilename= "F:\FTPUSERS\ARIAT\FTPOUT\ARIAT_INVENTORY_"+STRTRAN(DTOC(DATE()),"/")+".XLS"



*!*	COPY TO &xlsfilename FIELDS upc,STYLE,COLOR,ID,PACK,totqty,availqty,allocqty,holdqty,pickedqty XLS

*!*	*!*	cFilename = "STOCK"+STRTRAN(STRTRAN(STRTRAN(TTOC(DATETIME()),"/",""),":","")," ","")+".str"
*!*	*!*	cFilename = STRTRAN(cFilename,"pm","")
*!*	*!*	cFilename = STRTRAN(cFilename,"am","")
*!*	*!*	cFilename = STRTRAN(cFilename,"PM","")
*!*	*!*	cFilename = STRTRAN(cFilename,"AM","")

*!*	*!*	cFilename = "f:\ftpusers\ariat\846out\"+cFilename


*!*	tmessage = "Sent updated inventory sync file to the Ariat FTP Site.............."+CHR(13)+CHR(13)+;
*!*		"This email is generated via an automated process"  && +CHR(13)+"For changes in content or distribution please contact: Paul Gaidis @ 732-750-9000x143 "
*!*	tsubject= "Ariat Inventory Sent: " +TTOC(DATETIME())

*!*	tattach = xlsfilename+","+wipfilename

*!*	tfrom ="FMI Corporate <fmicorporate@fmiint.com>"

*!*	*Set Step On

*!*	tsendto = 'mbennett@fmiint.com'

*!*	IF !EMPTY(tattach)
*!*		DO FORM dartmail2 WITH tsendto,tfrom,tsubject,"",tattach,tmessage,"A"
*!*	ENDIF

*!*	schedupdate()

*!*	ON ERROR
*!*	_SCREEN.CAPTION=xscreencaption