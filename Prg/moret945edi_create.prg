 *!* MORET 945 EDI (Whse. Shipping Advice) Creation Program
*!* Creation Date: 11.10.2009 by Joe

PARAMETERS cBOL,nWO_Num,cOffice,nAcctNum,cTime
WAIT WINDOW "In Moret STANDARD 945 process" TIMEOUT 2

PUBLIC ARRAY thisarray(1)
PUBLIC c_CntrlNum,c_GrpCntrlNum,cReturned,cWO_Num,cWO_NumStr,nWO_Num1,cPRONum,cTRNum,lFedEx,lAvnerMail,cISA_Num,cLoadID,lPutFiles
PUBLIC lEmail,lDoFilesout,dDateTimeCal,nFilenum,tsendto,tcc,tsendtoerrmor,tccerrmor,nPTCount,cTrkNumber,cFreight,nCuFT,lDelOff,lToys
PUBLIC cxCoCode,cUseDCNum,cRolledRecs,cStatus,cErrMsg,lDoUpdate,lSkipCharges,nRepLoops,lDillard,lVFO,nCutUCCs,lOverflow,lDoCatch
PUBLIC lJCPenney,lMacy,lWalmart,tsendtozero,tcczero,dRunDate,lDoCompare,lCloseOutput,cTimeout,lAmazon,dDaysBack
PUBLIC cMod,cMBOL,cFilenameShort,cFilenameOut,cFilenameArch,lParcelType,cWO_NumList,lcPath,cEDIType,cISA_Num

cErrMsg = ""
cRolledRecs = ""

lTesting   = .F.  && Set to .t. for testing (default = .f.)
lTestinput = .F.  && Set to .t. for test input files only! (default = .f.)
lFakeDelivery = .F.
IF !lTesting
	lFakeDelivery = .F. && If true, creates a 945 for undelivered BOL, does not move to 945OUT folder
ENDIF
dtRunDate = DATETIME(2017,11,07,01,45,00)
cTimeout = IIF(lTesting,"TIMEOUT 1","NOWAIT")
dDaysBack = IIF(lTesting,20,10)  && Restricts search period for BOLs, etc. to avoid issues with earlier shipments

dDaysBack = IIF(DATETIME()<DATETIME(2018,04,11,16,45,00),30,dDaysBack)  && By adjusting these DT params, non-Testing runs can have a longer extract period

lEmail = .T.
lTestMail = lTesting && Sends mail to Joe only
lDoFilesout = !lTesting && If true, copies output to FTP folder (default = .t.)
lDoCartons = IIF(DATETIME() < dtRunDate,.F.,.T.) && Set to false to scan SQL Labels file (default = .t.)
lDoCompare = IIF(DATETIME() < dtRunDate,.F.,.T.)

*	lTestMail = .T.
*	lDoFilesout = .F.

IF lFakeDelivery
	WAIT WINDOW "This is a FAKE DELIVERY process" &cTimeout
	dFakeDate = DATE() && {^2012-06-06}
	cOffice = "M"
	lDoCompare = .F.
	lDoCartons = .F.
ENDIF


IF EMPTY(cOffice) AND !lTesting AND !lTestinput
	cErrMsg = UPPER("Missing Office")
	DO ediupdate WITH cErrMsg,.T.
	THROW
ENDIF

lUCC = .T.  && Set to false to order SQL results by carton ID instead of UCC (default = .t.)
lPrepack = .T.
lPick = .F.
lDoSQL = .T.
lDoUpdate = .T.
nRepLoops = 1
lPutFiles = .T.  && Default = .t.
lIsError = .F.
lDoCatch = .T.
cShip_ref = ""
lCloseOutput = .T.
nFilenum = 0
ccustname = ""
cWO_NumStr = ""
cWO_NumList = ""
cFreight = ""
cxCoCode = ""
nNoParcelWO = 6192499
lOverflow = .F.
lDelOff = .F.

SET DELETED ON

cUseDCNum = ""
cStatus = ""
cLoadID = ""

DO m:\dev\prg\_setvars WITH lTesting
IF lTesting
	CLOSE DATA ALL
ELSE
	CLOSETABLES()
ENDIF

tattach = ""

WAIT WINDOW "At the start of MORET AMT EDI 945 process..." &cTimeout
*ASSERT .F. MESSAGE "Start...debug here"

TRY
	lFederated = .F.
	cPPName = "MoretEDI"
	cMailName = cPPName

	tfrom = "Toll EDI Operations <toll-edi-ops@tollgroup.com>"
	tmessage=""
	tcc = ""
	lParcelType = .F.
	lFedEx = .F.
	lDoBOLGEN = .F.
	lSamples = .F.
	cBOL1 = ""
	lISAFlag = .T.
	lSTFlag = .T.
	nLoadid = 1
	lSQLMail = .F.

*!* TEST DATA AREA
*			ASSERT .F. MESSAGE "In test data area"
	IF lTesting OR lTestinput
		lookups()
		SELECT 0
		IF !USED('edi_trigger')
			cEDIFolder = "F:\3PL\DATA\"
			USE (cEDIFolder+"edi_trigger") IN 0 ALIAS edi_trigger
		ENDIF
		nAcctNum = 5452
		cBOL = "746571890659"
		cOffice = "C"
		cTime = DATETIME()
	ENDIF

	WAIT WINDOW "Processing BOL# "+cBOL &cTimeout
	cOffice = IIF(cOffice = '7','C',cOffice)
	cMod = IIF(cOffice = "C","7",cOffice)
	goffice = cMod
	cEDIType = "945"
	cMBOL = ""

*	lDoCompare = IIF(cOffice = "X",.F.,.T.)
	IF cBOL='88784233000073238'
		lOverflow = .T.
		lDelOff = .T.
	ENDIF

*!* This section added to reuse SQL info for same WO# for parcel shipments
	CREATE CURSOR temp945 (accountid i,isa_num c(10),st_num c(10),wo_num i,bol_no c(20),ship_ref c(20),filename c(50),filedate T)
	cBOL=TRIM(cBOL)

*ASSERT .f. MESSAGE "At cUseFolder select"
	IF lTestinput
		cUseFolder = "F:\WHP\WHDATA\"
	ELSE
		cUseFolder = "f:\wh7\whdata\"
	ENDIF

*!* Copied this code block from Unix 945  proc. 02.21.2011
	WAIT CLEAR
	swc_cutctns(cBOL)

	WAIT WINDOW "SWC Records cursor... all processed...." NOWAIT

	IF USED('OUTSHIP')
		USE IN outship
	ENDIF

	xsqlexec("select * from outship where bol_no = '"+cBOL+"' and (accountid in ("+gMoretAcctList+") or accountid in ("+gMoretAcctList2+"))",,,"wh")
	INDEX ON bol_no TAG bol_no
	INDEX ON ship_ref TAG ship_ref
	INDEX ON wo_num TAG wo_num

	selectoutdet()
	SELECT outdet
	INDEX ON outdetid TAG outdetid
	LOCATE

	cRetMsg = "X"
	IF !lTesting AND lDoSQL AND lDoCompare
		DO m:\dev\prg\sqldata-comparemoret WITH cUseFolder,cBOL,cOffice,nAcctNum,cPPName
		IF cRetMsg<>"OK"
			ASSERT .F. MESSAGE "Return msg <> OK"
			SET STEP ON
			lCloseOutput = .T.
			cWO_Num = ALLTRIM(STR(nWO_Num1))
			cErrMsg = "SQL CTNS~OUTDET TOT. COMPARE ERROR"
			DO ediupdate WITH cErrMsg,.T.
			THROW
		ENDIF
	ENDIF

	lStandalone = lTesting

	IF (!lDoUpdate AND !lTesting) OR !lPutFiles
		lTestMail = .T.
		lDoFilesout = .F.
	ENDIF

	IF lFakeDelivery
		lDoFilesout = .F.
	ENDIF

	STORE "LB" TO cWeightUnit
	STORE "CF" TO cCubeUnit

	WAIT WINDOW "Now preparing tables and variables" NOWAIT

*!* SET CUSTOMER CONSTANTS
	ccustname = "MORET"  && Customer Identifier
	cX12 = "004010"  && X12 Standards Set used

	IF USED('mm')
		USE IN mm
	ENDIF
	SELECT 0
	USE F:\3pl\DATA\mailmaster ALIAS mm SHARED
	LOCATE FOR mm.edi_type = "MISC" AND mm.taskname = "GENERAL"
	lUseAlt = mm.use_alt
	tsendtotest = ALLTRIM(IIF(lUseAlt,mm.sendtoalt,mm.sendto))
	tcctest = ALLTRIM(IIF(lUseAlt,mm.ccalt,mm.cc))
	tsendtoerrmor = ALLTRIM(IIF(lUseAlt,mm.sendtoalt,mm.sendto))
	tccerrmor = ALLTRIM(IIF(lUseAlt,mm.ccalt,mm.cc))
	LOCATE FOR mm.edi_type = "945" AND mm.GROUP = "MORET" AND accountid = 25000 && AMT/EDI Mails
	IF !FOUND()
		cErrMsg = "MISS MAIL DATA"
		DO ediupdate WITH cErrMsg,.T.
		THROW
	ENDIF
	lUseAlt = mm.use_alt
	tsendto = ALLTRIM(IIF(lUseAlt,mm.sendtoalt,mm.sendto))
	tcc = ALLTRIM(IIF(lUseAlt,mm.ccalt,mm.cc))
	LOCATE
	LOCATE FOR office = 'X' AND mm.taskname = 'MORETZEROBOL'
	lUseAlt = mm.use_alt
	tsendtozero = ALLTRIM(IIF(lUseAlt,mm.sendtoalt,mm.sendto))
	tcczero = ALLTRIM(IIF(lUseAlt,mm.ccalt,mm.cc))
	USE IN mm

	IF lTesting OR lTestinput
		tsendto = tsendtotest
		tcc = tcctest
	ENDIF

	cfd = "*"
	csd = "%"
	csegd = ""

*!* SET OTHER CONSTANTS
	DO CASE
		CASE cOffice = "X"
			cCustLoc =  "CR"
			cFMIWarehouse = ""
			cDivision = "Carson"
			cSF_Addr1  = "2000 E CARSON ST"
			cSF_CSZ    = "CARSON"+cfd+"CA"+cfd+"90745"

		CASE cOffice = "L"
			cCustLoc =  "ML"
			cFMIWarehouse = ""
			cDivision = "Mira Loma"
			cSF_Addr1  = "3355 DULLES DR"
			cSF_CSZ    = "MIRA LOMA"+cfd+"CA"+cfd+"91752"

		CASE cOffice = "M"
			cCustLoc =  "FL"
			cFMIWarehouse = ""
			cDivision = "Florida"
			cSF_Addr1  = "11400 NW 32ND AVE"
			cSF_CSZ    = "MIAMI"+cfd+"FL"+cfd+"33167"

		CASE cOffice = "I"
			cCustLoc =  "NJ"
			cFMIWarehouse = ""
			cDivision = "New Jersey"
			cSF_Addr1  = "800 FEDERAL BLVD"
			cSF_CSZ    =  "CARTERET"+cfd+"NJ"+cfd+"07008"

		OTHERWISE
			cCustLoc =  "SP"
			cFMIWarehouse = ""
			cDivision = "San Pedro"
			cSF_Addr1  = "450 WESTMONT DRIVE"
			cSF_CSZ    = "SAN PEDRO"+cfd+"CA"+cfd+"90731"
	ENDCASE
	IF INLIST(cOffice,"O","X")
		cWhseAbbrev = "TOLLO"
	ELSE
		cWhseAbbrev = 'FMI'+cCustLoc
	ENDIF
	cCustMailLoc = cCustLoc
	cCustFolder = UPPER(ccustname)+"-"+cCustLoc
	lNonEDI = .F.  && Assumes not a Non-EDI 945
	STORE "" TO lcKey
	STORE 0 TO alength,nLength
	cDelTime = SUBSTR(TTOC(cTime,1),9,4)
	cCarrierType = "M" && Default as "Motor Freight"
	dapptnum = ""

*!* SET OTHER CONSTANTS
	IF INLIST(cOffice,'L','C','E','1','2','7','O','X')
		dDateTimeCal = (DATETIME()-(3*3600))
	ELSE
		dDateTimeCal = DATETIME()
	ENDIF
	dt1 = TTOC(dDateTimeCal,1)
	dtmail = TTOC(dDateTimeCal)
	dt2 = dDateTimeCal
	cString = ""

	csendqual = "ZZ"
	csendid = cWhseAbbrev

	=SEEK(cBOL,'outship','bol_no')
	alength = ALINES(apt,outship.SHIPINS,.T.,CHR(13))
	cxCoCode = ""
	cxCoCode = segmentget(@apt,"COCODE",alength)

	IF EMPTY(cxCoCode)
		IF USED('mdivs')
			USE IN mdivs
		ENDIF
		USE F:\3pl\DATA\moretedidivs IN 0 ALIAS mdivs
		IF nAcctNum = 5448
			IF !SEEK(6313,'mdivs','accountid')
				cErrMsg = "ACCT# "+ALLTRIM(STR(nAcctNum))+" NOT IN MDIVS"
				DO ediupdate WITH cErrMsg,.T.
				THROW
			ELSE
				cxCoCode = TRIM(mdivs.cocode)
				USE IN mdivs
			ENDIF
		ELSE
			IF !SEEK(nAcctNum,'mdivs','accountid')
				cErrMsg = "ACCT# "+ALLTRIM(STR(nAcctNum))+" NOT IN MDIVS"
				DO ediupdate WITH cErrMsg,.T.
				THROW
			ELSE
				cxCoCode = TRIM(mdivs.cocode)
				USE IN mdivs
			ENDIF
		ENDIF
	ENDIF

	crecqual = "ZZ"
	cRecID = ""
	cCoNum = ""

	IF lTesting
*		SET STEP ON
	ENDIF
	DO CASE
		CASE cxCoCode = "SBH"
			cRecID = "SBHINTMTS"
			cCoNum = "02"
		CASE cxCoCode = "HBE"
			cRecID = "HBESTLMTD"
			cCoNum = "03"
		CASE cxCoCode = "HPD"
			cRecID = "HIGHPOINT"
			cCoNum = "04"
		CASE cxCoCode = "REG"
			cRecID = "REGENTSUT"  && Regent-Sutton
			cCoNum = "05"
		CASE cxCoCode = "BOZ"
			cRecID = "BOZKEELLC"  && Bozkee
			cCoNum = "08"
		CASE cxCoCode = "SK"
			cRecID = "MORET00SK"  && Moret SK
			cCoNum = "06"
		CASE cxCoCode = "BMG"
			cRecID = "BOYMEETSG"  && Boy Meets Girl
			cCoNum = "07"
		CASE cxCoCode = "SPM"
			cRecID = "SPRAYMORET"  && Spray Moret
			cCoNum = "09"
		OTHERWISE
			cRecID = "JMORETINC"
			cCoNum = "01"
	ENDCASE
	RELEASE cxCoCode

	nSegCtr = 0
	cterminator = ">" && Used at end of ISA segment
	IF HOUR(DATETIME())<3
		cdate = DTOS(DATE()-1)
	ELSE
		cdate = DTOS(DATE()) && IIF(lFakeDelivery,DTOS(DATE()+3),DTOS(DATE()))
	ENDIF
	cTruncDate = RIGHT(cdate,6)
	cTruncTime = SUBSTR(dt1,9,4)
	cOrig = "J"
	nSTCount = 0
	cStyle = ""
	cPTString = ""
	nTotCtnWt = 0
	nTotCtnCount = 0
	nUnitSum = 0
	cTargetStyle = ""
	lUPS = .F.
	lFedEx = .F.

** PAD ID Codes
	csendidlong = PADR(csendid,15," ")
	crecidlong = PADR(cRecID,15," ")

	lcPath = ("F:\FTPUSERS\MORETGROUP\OUT\")
	cFilenameShort = cCoNum+"_"+cWhseAbbrev+"_945_"+TTOC(DATETIME(),1)+".edi"
	cFilenameHold = ("f:\ftpusers\"+cCustFolder+"\945-Staging\"+cFilenameShort)
	cFilenameOut = (lcPath+cFilenameShort)
	cFilenameArch = ("f:\ftpusers\"+cCustFolder+"\945OUT\945archive\"+cFilenameShort)
	CREATE CURSOR tempmissdet (ship_ref c(20),STYLE c(20))

	SELECT 0
	CREATE CURSOR zeroqty (consignee c(40),ship_ref c(20),cnee_ref c(25))

	IF USED('OUTWOLOG')
		USE IN outwolog
	ENDIF

	SELECT 0
	CREATE CURSOR tempov (ship_ref c(20))

	SELECT 0
	SELECT ship_ref ;
		FROM outship ;
		WHERE outship.bol_no = cBOL ;
		AND !("OV"$outship.ship_ref) ;
		INTO CURSOR tempout1

	SCAN
		cPTCheck = ALLTRIM(tempout1.ship_ref)
		SELECT outship
		COUNT TO N FOR outship.ship_ref = cPTCheck
		IF N > 1
			INSERT INTO tempov (ship_ref) VALUES (cPTCheck)
			lOverflow = .T.
		ENDIF
	ENDSCAN

	RELEASE cPTCheck
	USE IN tempout1
	IF lOverflow
		WAIT WINDOW "There are one or more OV PTs attached to this BOL" TIMEOUT 2
	ENDIF

	SELECT outship

	SELECT outship
	SUM outship.qty TO N FOR outship.bol_no = cBOL
	IF N = 0
		SET STEP ON
		cErrMsg = "ZERO QTY BOL"
		DO ediupdate WITH cErrMsg,.T.
		THROW
	ENDIF

	LOCATE
*	ASSERT .F. MESSAGE "At BOL lookup"
	IF !SEEK(cBOL,'outship','bol_no')
		ASSERT .F. MESSAGE "At BOL no-find"
		WAIT WINDOW "BOL not found in OUTSHIP" TIMEOUT 2
		cErrMsg = "BOL !FOUND O/S"
		DO ediupdate WITH cErrMsg,.T.
		THROW
	ELSE
		LOCATE FOR outship.bol_no = cBOL AND outship.del_date > DATE()-dDaysBack
		IF !FOUND()
		cErrMsg = "BOL !FOUND-DAYSBACK= "+TRANSFORM(dDaysBack)
		DO ediupdate WITH cErrMsg,.T.
		THROW
		endif
		
		SCAN FOR outship.bol_no = cBOL AND outship.del_date > DATE()-dDaysBack

			IF outship.qty = 0
				INSERT INTO zeroqty (consignee,ship_ref,cnee_ref) VALUES (outship.consignee,outship.ship_ref,outship.cnee_ref)
			ENDIF
		ENDSCAN
	ENDIF

*	ASSERT .F. MESSAGE "At nFilenum creation"
	nFilenum = FCREATE(cFilenameHold)
	IF nFilenum < 0
		SET STEP ON
	ENDIF

	=SEEK(cBOL,'outship','bol_no')
	IF lTesting
*SET STEP ON
	ENDIF
	STORE .F. TO lAmazon,lAmazonFBA
	DO CASE
		CASE UPPER(outship.consignee)="AMAZON FBA"
			lAmazonFBA = .T.
		CASE "AMAZON"$UPPER(outship.consignee)
			lAmazon = .T.
	ENDCASE
	nWO_Num = outship.wo_num
	cWO_Num = ALLTRIM(STR(nWO_Num))
	cShip_ref = ALLTRIM(outship.ship_ref)
	cKeyrec = ALLTRIM(outship.keyrec)
	IF EMPTY(cShip_ref)
		cErrMsg = "EMPTY PT @ WO "+cWO_Num
		DO ediupdate WITH cErrMsg,.T.
		THROW
	ENDIF
	cShip_refSH = IIF('-'$cShip_ref,LEFT(cShip_ref,AT("-",cShip_ref)-1),cShip_ref)


	IF !lAmazonFBA
		IF USED('pacas')
			USE IN pacas
		ENDIF
		USE F:\3pl\DATA\parcel_carriers ALIAS pacas IN 0
		IF SEEK(ALLTRIM(outship.scac),'pacas','scac')  && !("AMAZON"$outship.consignee)
			IF pacas.ups OR pacas.fedex
				WAIT "" TIMEOUT 1
				lUPS = .T.
				IF USED('SHIPMENT')
					USE IN shipment
				ENDIF

				IF USED('PACKAGE')
					USE IN package
				ENDIF

				csq1 = [select * from shipment where (accountid in (]+gMoretAcctList+[) or accountid in (]+gMoretAcctList2+[)) and shipdate >= {]+DTOC(DATE()-dDaysBack)+[}]
				xsqlexec(csq1,,,"wh")
				INDEX ON shipmentid TAG shipmentid
				INDEX ON pickticket TAG pickticket

				SELECT shipment
				LOCATE
				IF RECCOUNT("shipment") > 0
					xjfilter="shipmentid in ("
					SCAN
						nShipmentId = shipment.shipmentid
						xsqlexec("select * from package where shipmentid="+TRANSFORM(nShipmentId),,,"wh")

						IF RECCOUNT("package")>0
							xjfilter=xjfilter+TRANSFORM(shipmentid)+","
						ELSE
							xjfilter="1=0"
						ENDIF
					ENDSCAN
					xjfilter=LEFT(xjfilter,LEN(xjfilter)-1)+")"

					xsqlexec("select * from package where "+xjfilter,,,"wh")
				ELSE
					xsqlexec("select * from package where .f.",,,"wh")
				ENDIF
				SELECT package
				INDEX ON shipmentid TAG shipmentid
				SET ORDER TO TAG shipmentid

			ENDIF
			IF pacas.fedex
				lFedEx = .T.
			ENDIF
		ENDIF
		USE IN pacas
		lParcelType = IIF(lUPS OR lFedEx,.T.,.F.)
		lParcelType = IIF(nWO_Num = 6192499,.F.,lParcelType)
		lParcelType = IIF(cKeyrec = cBOL,.F.,lParcelType)
		lAmazon = IIF(nWO_Num = nNoParcelWO,.F.,lAmazon)
		lParcelType = IIF(cBOL = '735493040771',.F.,lParcelType)
		lParcelType = IIF(TRIM(outship.scac)="EMSY",.F.,lParcelType)
	ELSE
		lParcelType = .F.
	ENDIF
	IF !lTesting AND lParcelType
		IF USED('moret_wohold')
			USE IN moret_wohold
		ENDIF
		SELECT 0
		USE F:\3pl\DATA\moret_wohold
		nWO_numOld=moret_wohold.wo_num
		IF nWO_Num = nWO_numOld AND !("~"$cShip_ref)
			lDoSQL = .F.
		ELSE
			REPLACE wo_num WITH nWO_Num IN moret_wohold
		ENDIF
	ENDIF

	IF lParcelType
		IF USED("trknumbers")
			USE IN trknumbers
		ENDIF
		IF lTestinput
			cTrkNumber = IIF(lFedEx,'12345678','1ZQ99Q12345678')+ALLT(cShip_ref)
			nFreight = ABS(50*RAND(0))
			cFreight = ALLTRIM(STR(nFreight,10,2))
		ELSE
			nFreight = 0
			SELECT shipment
			LOCATE
*			BROWSE FOR accountid = 5452 AND INLIST(wo_num,6220748,6220749)
			cTrkNumber = ""
			cPaddedPT = ""
			IF DATETIME()<DATETIME(2017,12,06,23,45,00)
				cPaddedPT = PADR(cShip_ref,20) && IIF("~"$cShip_ref,LEFT(cShip_ref,AT("~",cShip_ref)-1),PADR(cShip_ref,20))
			ELSE
				cPaddedPT = IIF("~"$cShip_ref,PADR(LEFT(cShip_ref,AT("~",cShip_ref)-1),20),PADR(cShip_ref,20))
			ENDIF
			LOCATE FOR shipment.pickticket = cPaddedPT AND (INLIST(shipment.accountid,&gMoretAcctList) OR INLIST(shipment.accountid,&gMoretAcctList2))

			IF FOUND()
				SCAN FOR shipment.pickticket = cPaddedPT
					lSkipCharges = IIF(INLIST(shipment.billing,"COL","STP","B3P","C/B","BRC"),.T.,.F.)
					WAIT WINDOW "Skip charges? "+IIF(lSkipCharges,'YES','NO') NOWAIT && TIMEOUT 2

					nShipmentId = 0
					nShipmentId = shipment.shipmentid
					IF SEEK(nShipmentId,'package','shipmentid')
						cTrkNumber = ALLTRIM(package.trknumber)
						IF EMPTY(cTrkNumber)
							WAIT WINDOW "No Tracking number found for BOL "+cBOL TIMEOUT 2
							cTrkNumber = cBOL
						ENDIF
						SELECT package
						IF nFreight = 0
							SUM package.pkgcharge TO nFreight FOR package.shipmentid = nShipmentId
						ELSE
							SUM package.pkgcharge TO nFreight2 FOR package.shipmentid = nShipmentId
							nFreight = nFreight+nFreight2
						ENDIF
						cFreight = ALLTRIM(STR(nFreight,10,2))
					ELSE
						cShipmentid = ALLTRIM(STR(shipment.shipmentid))
						cErrMsg = "SHIPID "+cShipmentid+" ! IN PACKAGE"
						ediupdate(cErrMsg,.T.)
						THROW
					ENDIF
					IF lAmazon AND lParcelType && Added per Liana/Moret 06.03.2014
						IF !USED('trknumbers')
							SELECT trknumber FROM package WHERE package.shipmentid = nShipmentId INTO CURSOR trknumbers READWRITE
						ELSE
							SELECT trknumber FROM package WHERE package.shipmentid = nShipmentId INTO CURSOR trknumbers2
							SELECT trknumbers
							APPEND FROM DBF('trknumbers2')
						ENDIF
						SELECT trknumbers
						LOCATE
						IF lTesting
							BROWSE
						ENDIF
					ENDIF
				ENDSCAN
			ELSE
				LOCATE
				LOCATE FOR shipment.wo_num = nWO_Num AND (INLIST(shipment.accountid,&gMoretAcctList) OR INLIST(shipment.accountid,&gMoretAcctList2))
				IF FOUND()
					IF UPPER(ALLTRIM(shipment.pickticket)) = "MULTIPLE"
						lFred = shipment.fredmeyer
						SELECT package
						LOCATE
						LOCATE FOR package.shipmentid = shipment.shipmentid AND ;
							ALLTRIM(IIF(DATE()={^2017-05-17},package.ref2,package.ref4)) = cShip_ref
						IF FOUND()
							cTrkNumber = ALLTRIM(package.trknumber)
							IF EMPTY(cTrkNumber)
								cErrMsg = "TRKNUM NO FIND, PT "+cShip_ref
								ASSERT .F. MESSAGE cErrMsg
								ediupdate(cErrMsg,.T.)
								THROW
							ENDIF
							SUM package.pkgcharge TO nFreight FOR package.shipmentid=shipment.shipmentid
							cFreight = ALLTRIM(STR(nFreight,10,2))
						ELSE
							IF lFred
								SELECT ALLT(ref4)AS ship_ref,trknumber ;
									FROM package ;
									WHERE package.shipmentid=shipment.shipmentid ;
									GROUP BY 1 ;
									INTO CURSOR xmult
								LOCATE

								SUM package.pkgcharge TO nFreight FOR package.shipmentid=shipment.shipmentid
								cFreight = ALLTRIM(STR(nFreight,10,2))
								SCAN
									xsqlexec("select * from mpdet where wo_num="+TRANSFORM(shipment.wo_num)+" and ship_ref='"+xmult.ship_ref+"'",,,"wh")
									xmpid = mpdet.mpid

									LOCATE FOR mpid = xmpid AND wo_num=shipment.wo_num AND ship_ref = ALLTRIM(xmult.ship_ref)
									IF FOUND()
										cTrkNumber = ALLTRIM(xmult.trknumber)
										EXIT
									ENDIF
								ENDSCAN
								USE IN mpdet
								IF EMPTY(cTrkNumber)
									SELECT outship
									cErrMsg = "BAD UPS TRK#, PT "+cShip_ref
									WAIT WINDOW cErrMsg NOWAIT
*								ASSERT .F. MESSAGE cErrMsg
									ediupdate(cErrMsg,.T.)
									THROW
								ENDIF
							ENDIF
						ENDIF
					ELSE
						cErrMsg = "BAD UPS PT/WO "+ALLTRIM(STR(nWO_Num))
						ASSERT .F. MESSAGE cErrMsg
						ediupdate(cErrMsg,.T.)
						THROW
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF

	SELECT dcnum FROM outship WHERE outship.bol_no = cBOL GROUP BY 1 INTO CURSOR tempx
	LOCATE

	IF EOF()
		cErrMsg = "EMPTY DC/STORE#"
		ediupdate(cErrMsg,.T.)
		THROW
	ENDIF
	IF USED('tempx')
		USE IN tempx
	ENDIF

&& Added code blocks to the following to overcome BOL vs. WO selection issues, 12.23.2005
	SELECT outship
	LOCATE

	IF lDoSQL
		IF USED("sqlwomoret")
			USE IN sqlwomoret
		ENDIF

		DELETE FILE "F:\3pl\DATA\sqlwomoret.dbf"

		cRetMsg = ""
		IF lDoSQL
			DO m:\dev\prg\sqlconnectmoret_bol  WITH nAcctNum,cBOL,cPPName,lUCC,cOffice,lDoCartons
			IF cRetMsg<>"OK"
				SET STEP ON
				cErrMsg = cRetMsg
				DO ediupdate WITH cErrMsg,.T.
				THROW
			ENDIF
		ENDIF

*	ASSERT .F. MESSAGE "Run ID Check"
		SELECT vmoretedipp
		INDEX ON outshipid TAG outshipid
		INDEX ON ship_ref TAG ship_ref
		SELECT wo_num FROM vmoretedipp GROUP BY 1 INTO CURSOR temprunid1
		SELECT runid FROM vmoretedipp GROUP BY 1 INTO CURSOR temprunid2
		SELECT temprunid1
		STORE RECCOUNT() TO nWORecs
		SELECT temprunid2
		STORE RECCOUNT() TO nRIDRecs

		IF nWORecs#nRIDRecs
			cErrMsg = "MULTI RUNIDS"
			DO ediupdate WITH cErrMsg,.T.
			THROW
		ENDIF
		USE IN temprunid1
		USE IN temprunid2
	ENDIF

	IF !USED("scacs")
		xsqlexec("select * from scac","scacs",,"wh")
		INDEX ON scac TAG scac
	ENDIF

	SELECT outship
	LOCATE
	WAIT CLEAR
	WAIT WINDOW "Now creating Header information" NOWAIT

*!* HEADER LEVEL EDI DATA
	DO num_incr_isa

	cISA_Num = PADL(c_CntrlNum,9,"0")
	nISA_Num = INT(VAL(cISA_Num))

	IF lTesting
		cISACode = "T"
	ELSE
		cISACode = "P"
	ENDIF

	STORE "ISA"+cfd+"00"+cfd+"          "+cfd+"00"+cfd+"          "+cfd+csendqual+cfd+csendidlong+cfd+;
		crecqual+cfd+crecidlong+cfd+cTruncDate+cfd+cTruncTime+cfd+"U"+cfd+"00401"+cfd+;
		cISA_Num+cfd+"0"+cfd+cISACode+cfd+cterminator+csegd TO cString
	DO cstringbreak

	STORE "GS"+cfd+"SW"+cfd+csendid+cfd+cRecID+cfd+cdate+cfd+cTruncTime+cfd+c_CntrlNum+;
		cfd+"X"+cfd+cX12+csegd TO cString
	DO cstringbreak

	SELECT outship
*	SET ORDER TO TAG wo_num
	LOCATE

	IF !SEEK(nWO_Num,'outship','wo_num')
		WAIT CLEAR
		WAIT WINDOW "Invalid Work Order Number - Not Found in OUTSHIP!" TIMEOUT 2
		cErrMsg = "WO NOT FOUND"
		DO ediupdate WITH cErrMsg,.T.
		THROW
	ENDIF

	SET STEP ON
	IF !lTesting
		IF EMPTY(cBOL)  && May be empty if triggered from PT screen, or FedEx shipment
			WAIT CLEAR
			WAIT WINDOW "MISSING Bill of Lading Number!" TIMEOUT 2
			cErrMsg = "BOL# EMPTY"
			DO ediupdate WITH cErrMsg,.T.
			THROW
		ELSE
			IF !SEEK(PADR(TRIM(cBOL),20),"outship","bol_no")
				WAIT CLEAR
				WAIT WINDOW "Invalid Bill of Lading Number - Not Found!" TIMEOUT 2
				cErrMsg = "BOL# NOT FOUND"
				DO ediupdate WITH cErrMsg,.T.
				THROW
			ENDIF
		ENDIF
	ENDIF

*************************************************************************
*1	PTIDSOURCE/OUTSHIP RECORD MAIN LOOP
*	Loops through individual OUTSHIP lines for BOL#
*************************************************************************
	WAIT CLEAR
*	ASSERT .F. MESSAGE "At Outship scan..."
	WAIT WINDOW "Now creating BOL#-based information..." NOWAIT

	SELECT outship
	SET ORDER TO
	IF  lFakeDelivery
		STORE dFakeDate TO m.del_date
	ELSE
		COUNT TO N FOR outship.bol_no = cBOL AND !EMPTYnul(del_date)
		IF N=0
			cErrMsg = "INCOMP BOL>Miss DelDate"
			DO ediupdate WITH cErrMsg,.T.
			THROW
		ENDIF
	ENDIF

	LOCATE
	cMissDel = ""


	CREATE CURSOR temppt (ship_ref c(20))
	CREATE CURSOR holdsplits (ship_ref1 c(10))
	SELECT temppt
	INDEX ON ship_ref TAG ship_ref

	SELECT outship
	lSetOrder = .F.
	SCAN FOR outship.bol_no = cBOL AND del_date > DATE()-dDaysBack
		IF "~"$outship.ship_ref
*			SET STEP ON
			lSetOrder = .T.
			EXIT
		ENDIF
	ENDSCAN

	SELECT outship
	IF lSetOrder
		WAIT WINDOW "Setting order to SHIP_REF" TIMEOUT 2
		SET ORDER TO TAG ship_ref
	ELSE
		SET ORDER TO
	ENDIF

	IF lFakeDelivery
		oscanstr = "outship.bol_no = cBOL and qty > 0"
	ELSE
		oscanstr = "outship.bol_no = cBOL AND !EMPTYnul(del_date) and outship.del_date > DATE()-dDaysBack"
	ENDIF

	nPTCount = 0
	cShip_ref_out = ""
	SCAN FOR &oscanstr
		SCATTER MEMVAR MEMO
		m.del_date = IIF(lFakeDelivery,dFakeDate,m.del_date)
		alength = ALINES(apt,outship.SHIPINS,.T.,CHR(13))
		cShip_ref = ALLTRIM(m.ship_ref)
		lMasterpack = outship.masterpack
		IF EMPTY(outship.del_date)
			LOOP
		ENDIF

		IF "OV"$cShip_ref
			LOOP
		ENDIF
		IF !"~"$cShip_ref
			cShip_ref_out = cShip_ref
		ELSE
			IF EMPTY(cShip_ref_out)
				cShip_ref_out = ALLT(LEFT(cShip_ref,AT('~',cShip_ref)-1))
			ELSE
				IF cShip_ref_out = ALLT(LEFT(cShip_ref,AT('~',cShip_ref)-1))
*				SET STEP ON
					LOOP
				ENDIF
			ENDIF
		ENDIF

*!* If there is an overflow on another BOL, this section will force scan of all cartons
*!* in SQL for the original OUTDETIDs (Cross-added 10.11.2011, Joe)
		nRecx = RECNO()
		lDelOff = .F.
		SELECT COUNT(outshipid) AS count1 ;
			FROM outship ;
			WHERE outship.ship_ref = cShip_ref ;
			AND outship.accountid = nAcctNum ;
			INTO CURSOR tempptchk
		SELECT tempptchk
		IF tempptchk.count1>1
			lDelOff = .T.
		ENDIF
		USE IN tempptchk
		SELECT outship
		GO nRecx


		dDel_date = outship.del_date
		IF EMPTY(dDel_date)
			LOOP
		ENDIF

		IF outship.qty = 0 OR outship.ctnqty = 0
			IF !lMasterpack
				WAIT WINDOW "PT# "+cShip_ref+" has zero qty...looping" TIMEOUT 1
				LOOP
			ENDIF
		ENDIF

		nWO_Num = outship.wo_num
		cWO_Num = ALLTRIM(STR(nWO_Num))
		lRepeatPT = IIF("~"$cShip_ref,.T.,.F.)
*		lRepeatPT = IIF(cBOL#'1ZWW51740395769834',lRepeatPT,.F.)
		IF lRepeatPT
			WAIT WINDOW "This is a SPLIT PT" &cTimeout
		ELSE
			tx = 1
		ENDIF
*		lRepeatPT =  .f.  && If set to .f., will bypass split PT check...should not normally be used.
		IF "~"$cShip_ref AND lRepeatPT
			m.ship_ref1 = LEFT(cShip_ref,AT("~",cShip_ref)-1)
			SELECT holdsplits
			LOCATE FOR holdsplits.ship_ref1 = m.ship_ref1
			IF !FOUND()
				INSERT INTO holdsplits FROM MEMVAR
				SELECT outship
			ENDIF
		ENDIF

		IF lRepeatPT
			cShip_ref_out = ALLT(LEFT(cShip_ref,AT('~',cShip_ref)-1))
			cAltShip_ref = IIF("U"$cShip_ref,cShip_ref_out+" ~C",cShip_ref_out+" ~U")
			IF SEEK(cShip_ref_out,'temppt','ship_ref')
				LOOP
			ELSE
				INSERT INTO temppt (ship_ref) VALUES (cShip_ref_out)
			ENDIF
		ELSE
			STORE cShip_ref TO cShip_ref_out
		ENDIF

		cShip_refSH = IIF('-'$cShip_ref,LEFT(cShip_ref,AT("-",cShip_ref)-1),cShip_ref)
		cShip_refUse = cShip_ref_out

		cUCCHead = ALLTRIM(STRTRAN(MLINE(SHIPINS,ATLINE("UCC*",SHIPINS)),"UCC*",""))

		nCuFT = outship.cuft
		IF EMPTY(nCuFT)
			nCuFT = (2*outship.qty)
		ENDIF
		lMacy = IIF("MACY"$m.consignee,.T.,.F.)
		lJCPenney = IIF(("JCP"$m.consignee OR "PENNEY"$m.consignee),.T.,.F.)
		lWalmart = IIF((m.consignee="WAL" AND "MART"$m.consignee),.T.,.F.)
		lVFO = IIF((m.consignee="VF"),.T.,.F.)
		lToys = IIF(m.consignee="TOYS" OR ("TOYS"$m.consignee AND "US"$m.consignee),.T.,.F.)
		lDillard = IIF("DILLARD"$m.consignee,.T.,.F.)

		IF !(cWO_Num$cWO_NumStr)
			cWO_NumStr = IIF(EMPTY(cWO_NumStr),cWO_Num,cWO_NumStr+","+cWO_Num)
		ENDIF
		IF !(cWO_Num$cWO_NumList)
			cWO_NumList = IIF(EMPTY(cWO_NumList),cWO_Num,cWO_NumList+CHR(13)+cWO_Num)
		ENDIF

		xsqlexec("select * from outwolog where wo_num = "+TRANSFORM(nWO_Num)+"and (accountid in ("+gMoretAcctList+") or accountid in ("+gMoretAcctList2+"))",,,"wh")
		lPick = IIF(outwolog.picknpack # .T.,.F.,.T.)
		lPrepack = !lPick

		nTotCtnCount = m.qty
		cPO_Num = ALLTRIM(m.cnee_ref)
		nPTCount = nPTCount + 1

*!* Added this code to trap miscounts in OUTDET Units
		IF lDelOff
			SET DELETED OFF
*				ASSERT .F. MESSAGE "At set dele off"
		ENDIF
		IF lPrepack
			SET DELETED ON
			IF lOverflow
				SELECT tempov
				LOCATE FOR tempov.ship_ref = cShip_ref
				IF FOUND()
					SET DELETED OFF
				ENDIF
			ENDIF

			SELECT outdet
			SET ORDER TO
			SUM IIF(lOverflow,outdet.origqty,outdet.totqty) TO nUnitTot1 FOR units AND outdet.outshipid = outship.outshipid
			IF cOffice = "X" AND nAcctNum = 6313
				SUM (IIF(lOverflow,outdet.origqty,outdet.totqty)) TO nUnitTot2 FOR !units AND outdet.outshipid = outship.outshipid
			ELSE
				SUM (VAL(PACK)*IIF(lOverflow,outdet.origqty,outdet.totqty)) TO nUnitTot2 FOR !units AND outdet.outshipid = outship.outshipid
			ENDIF
			IF nUnitTot1<>nUnitTot2
				WAIT WINDOW "TOTQTY ERROR AT PT "+cShip_ref TIMEOUT 5
				SET STEP ON
				cErrMsg = "OUTDET TOTQTY ERR, PT "+cShip_ref
				DO ediupdate WITH cErrMsg,.T.
				THROW
			ENDIF


			SELECT vmoretedipp
			locate
			SUM totqty TO nUnitTot2 FOR vmoretedipp.outshipid = outship.outshipid ;
				AND (vmoretedipp.ucc # 'CUTS' AND vmoretedipp.totqty > 0)
			IF nUnitTot1<>nUnitTot2
				WAIT WINDOW "SQL QTY ERROR AT PT "+cShip_ref TIMEOUT 5
				SET STEP ON
				cErrMsg = "SQL/OD UNITQTY ERR, PT "+cShip_ref
				DO ediupdate WITH cErrMsg,.T.
				THROW
			ENDIF
		ENDIF
		SELECT outdet
		SET ORDER TO outdetid
		LOCATE

*!* End code addition

		IF ("PENNEY"$UPPER(outship.consignee) OR "DICK"$UPPER(outship.consignee) OR "KMART"$UPPER(outship.consignee) OR "K-MART"$UPPER(outship.consignee) OR "ZAPPO"$UPPER(outship.consignee))
			lApptFlag = .T.
		ELSE
			lApptFlag = .F.
		ENDIF

		dDel_date = outship.del_date
		dexpdel_date = outship.expdeldt  && Expected delivery date (at facility)
		IF EMPTYnul(dexpdel_date) OR ISNULL(dexpdel_date)
			dexpdel_date = dDel_Date
*!*				cErrMsg = "BAD EXPDELDT"
*!*				DO ediupdate WITH cErrMsg,.T.
*!*				THROW
		ELSE
			IF EMPTY(dexpdel_date)
				dexpdel_date = DATE()+3
			ENDIF
		ENDIF
		
		IF lFakeDelivery
			dDel_date = dFakeDate
			dexpdel_date = dFakeDate+3
		ENDIF

		IF lTestinput
			IF EMPTY(dDel_date)
				dDel_date = DATE()
			ENDIF
			dapptnum = "99999"
			dapptdate = DATE()
		ELSE
			IF EMPTY(dDel_date)
				cMissDel = IIF(EMPTY(cMissDel),"The following PTs had no Delivery Dates:"+CHR(13)+TRIM(cShip_ref),cMissDel+CHR(13)+TRIM(cShip_ref))
				LOOP
			ENDIF
			dapptnum = ALLTRIM(outship.appt_num)

			IF !lTesting AND !lParcelType
				IF EMPTY(dapptnum) && Penney/KMart/Zappos Appt Number check
					IF !(lApptFlag)
						dapptnum = ""
					ELSE
						cErrMsg = "EMPTY APPT #"
						DO ediupdate WITH cErrMsg,.T.
						THROW
					ENDIF
				ENDIF
			ELSE
				dapptnum = "99999"
			ENDIF
			dapptdate = outship.appt

			IF EMPTY(dapptdate)
				dapptdate = outship.del_date
			ENDIF
		ENDIF

		IF !lTesting
			IF lParcelType
				cBOL = TRIM(outship.bol_no)
			ELSE
				nWO_Num = outship.wo_num
				cWO_Num = ALLTRIM(STR(nWO_Num))
			ENDIF
		ENDIF

		IF ALLTRIM(outship.SForCSZ) = ","
			BLANK FIELDS outship.SForCSZ NEXT 1 IN outship
		ENDIF

		cTRNum = ""
		cPRONum = ""
		STORE STRTRAN(TRIM(outship.keyrec),"PR","") TO cPRONum  && Changed to always use as PRO# per Juan, 10.21.2013

		nOutshipid = m.outshipid
		IF lParcelType
			cPTString = IIF(EMPTY(cPTString),m.consignee+" "+cBOL+" "+cShip_ref,cPTString+CHR(13)+m.consignee+" "+cBOL+" "+cShip_ref)
		ELSE
			cPTString = IIF(EMPTY(cPTString),m.consignee+" "+cShip_ref,cPTString+CHR(13)+m.consignee+" "+cShip_ref)
		ENDIF
		m.CSZ = TRIM(m.CSZ)
		IF OCCURS(" ",m.CSZ) < 2
			cErrMsg = "TOO FEW SPACES-CSZ "+cShip_ref
			ediupdate(cErrMsg,.T.)
			THROW
		ENDIF
		cCountry = "USA"
		IF EMPTY(M.CSZ) OR ALLT(m.CSZ)=","
			IF "PAUL FASHION"$m.consignee
				m.CSZ = "OXFORD, UK OX20DP"
				cCountry = "UK"
			ELSE
				WAIT CLEAR
				WAIT WINDOW "No SHIP-TO City/State/ZIP info...exiting" TIMEOUT 2
				cErrMsg = "EMPTY CSZ "+cShip_ref
				ediupdate(cErrMsg,.T.)
				THROW
			ENDIF
		ENDIF

		m.CSZ = ALLTRIM(STRTRAN(m.CSZ,"  "," "))
		IF !","$m.CSZ
			SET STEP ON
			cErrMsg = "NO COMMA CSZ-"+cShip_ref
			DO ediupdate WITH cErrMsg,.T.
			THROW
		ENDIF
		nSpaces = OCCURS(" ",ALLTRIM(m.CSZ))
		IF nSpaces = 0
			WAIT WINDOW "Ship-to CSZ Segment Error" TIMEOUT 3
			cErrMsg = "Ship-to CSZ: "+cShip_ref
			DO ediupdate WITH cErrMsg,.T.
			THROW
		ELSE
			nCommaPos = AT(",",m.CSZ)
			nLastSpace = AT(" ",m.CSZ,nSpaces)
			IF ISALPHA(SUBSTR(TRIM(m.CSZ),AT(" ",m.CSZ,nSpaces-1)+1,2))
				cCity = LEFT(TRIM(m.CSZ),AT(",",m.CSZ)-1)
				cState = SUBSTR(m.CSZ,nCommaPos+2,2)
				cZip = TRIM(SUBSTR(m.CSZ,nLastSpace+1))
			ELSE
				WAIT CLEAR
				WAIT WINDOW "NOT ALPHA: "+SUBSTR(TRIM(m.CSZ),AT(" ",m.CSZ,nSpaces-1)+1,2) TIMEOUT 3
				cCity = LEFT(TRIM(m.CSZ),AT(",",m.CSZ)-1)
				cState = SUBSTR(TRIM(m.CSZ),AT(" ",m.CSZ,nSpaces-2)+1,2)
				cZip = TRIM(SUBSTR(m.CSZ,nLastSpace+1))
			ENDIF
		ENDIF

		IF lFederated
			cStoreName = segmentget(@apt,"STORENAME",alength)
		ENDIF

		m.outctnqty = IIF(lMasterpack,1,outship.ctnqty)
		m.outweight = outship.weight


		DO num_incr_st
		WAIT CLEAR
		WAIT WINDOW "Now creating Line Item information" NOWAIT

		INSERT INTO temp945 (accountid,isa_num,st_num,wo_num,bol_no,ship_ref,filename,filedate) ;
			VALUES (m.accountid,c_CntrlNum,c_GrpCntrlNum,m.wo_num,m.bol_no,m.ship_ref,cFilenameShort,dt2)

		STORE "ST"+cfd+"945"+cfd+PADL(c_GrpCntrlNum,9,"0")+csegd TO cString
		DO cstringbreak
		nSTCount = nSTCount + 1
		nSegCtr = 1

		IF EMPTY(cShip_refUse)
			WAIT WIND "MISSING SHIP REF IN W06...DEBUG!!" TIMEOUT 2
			SET STEP ON
		ENDIF
		cShip_refUse = IIF(cBOL#'00863233000393273',cShip_refUse,LEFT(cShip_refUse,8))
		cShip_refUse = IIF(!lRepeatPT AND "~"$cShip_refUse,LEFT(cShip_refUse,AT("~",cShip_refUse)-1),cShip_refUse)
		STORE "W06"+cfd+"N"+cfd+cShip_refUse+cfd+cdate+cfd+TRIM(cWO_Num)+cfd+cfd+cPO_Num+csegd TO cString
		DO cstringbreak
		nSegCtr = nSegCtr + 1
		nCtnNumber = 1  && Seed carton sequence count

		cDCNum = ALLTRIM(m.dcnum)
		cDCNumStored = ALLTRIM(segmentget(@apt,"STORENUM",alength))
		IF cDCNum # cDCNumStored
			cDCNum = cDCNumStored
		ENDIF
		IF EMPTY(TRIM(cDCNum))
			IF (!EMPTY(m.storenum) AND m.storenum<>0)
				cDCNum = ALLTRIM(STR(m.storenum))
			ENDIF
			IF EMPTY(cDCNum)
				DO ediupdate WITH cErrMsg,.T.
				THROW
			ENDIF
		ENDIF

		STORE "N1"+cfd+"SF"+cfd+"FMI INTERNATIONAL INC. -"+UPPER(cDivision)+cfd+"91"+cfd+"FMI"+cCustLoc+csegd TO cString
		DO cstringbreak
		nSegCtr = nSegCtr + 1

		IF !lFederated
			STORE "N3"+cfd+TRIM(cSF_Addr1)+csegd TO cString
			DO cstringbreak
			nSegCtr = nSegCtr + 1

			STORE "N4"+cfd+cSF_CSZ+cfd+cCountry+csegd TO cString
			DO cstringbreak
			nSegCtr = nSegCtr + 1
		ENDIF

		STORE "N1"+cfd+"ST"+cfd+ALLTRIM(m.consignee)+cfd+"92"+cfd+cDCNum+csegd TO cString
		DO cstringbreak
		nSegCtr = nSegCtr + 1

		STORE "N3"+cfd+TRIM(outship.address)+csegd TO cString
		DO cstringbreak
		nSegCtr = nSegCtr + 1

		STORE "N4"+cfd+cCity+cfd+cState+cfd+cZip+cfd+"USA"+csegd TO cString
		DO cstringbreak
		nSegCtr = nSegCtr + 1

		cSforstore = TRIM(m.sforstore)
		IF !EMPTY(m.sforstore) AND VAL(m.sforstore)>0
			STORE "N1"+cfd+"BY"+cfd+ALLTRIM(m.shipfor)+cfd+"91"+cfd+cSforstore+csegd TO cString
			DO cstringbreak
			nSegCtr = nSegCtr + 1

		ENDIF

		cBOLUse = IIF(lParcelType,"0"+cUCCHead+RIGHT(ALLTRIM(cBOL),10),cBOL)
		STORE "N9"+cfd+"BM"+cfd+ALLTRIM(cBOLUse)+csegd TO cString
		DO cstringbreak
		nSegCtr = nSegCtr + 1
*		ASSERT .F. MESSAGE "At N9BM segment post-creation...debug"

		IF !EMPTY(cPRONum)
			STORE "N9"+cfd+"CN"+cfd+cPRONum+csegd TO cString
			DO cstringbreak
			nSegCtr = nSegCtr + 1
		ELSE
			IF INLIST(outship.consignee,"AMAZON","BOSCOV") AND !lParcelType
				cErrMsg = "MISSING REQD PRO#"
				DO ediupdate WITH cErrMsg,.T.
				THROW
			ENDIF
		ENDIF

		IF !EMPTY(cTRNum)
			STORE "N9"+cfd+"TR"+cfd+cTRNum+csegd TO cString
			DO cstringbreak
			nSegCtr = nSegCtr + 1
		ENDIF

*!* For JCPenney orders only
		IF lTesting AND EMPTY(m.appt_num)
			cLoadID = PADR(ALLTRIM(STR(nLoadid)),6,"0")
			nLoadid = nLoadid + 1
		ELSE
			cLoadID = ALLTRIM(m.appt_num) && IIF((lParcelType and !"ZAPPO"$outship.consignee and !"AMAZON"$outship.consignee),cBOL,ALLTRIM(m.appt_num))
		ENDIF

*!*			IF (nWO_Num # nNoParcelWO) AND (cKeyrec # cBOL)
*!*				IF (!lParcelType AND (lJCPenney OR lWalmart OR lDillard)) OR (lToys OR lAmazon OR lVFO)
		IF (EMPTY(cLoadID) OR cLoadID=="C" OR cLoadID=="U")
			SET STEP ON
			cErrMsg = "BAD LOAD ID (OR C/U STATUS)"
			DO ediupdate WITH cErrMsg,.T.
			THROW
		ENDIF
*!*				ENDIF
*!*			ENDIF

		IF !EMPTY(cLoadID)
			STORE "N9"+cfd+"P8"+cfd+TRIM(cLoadID)+csegd TO cString
			DO cstringbreak
			nSegCtr = nSegCtr + 1
		ENDIF

		IF lParcelType
			STORE "N9"+cfd+"TN"+cfd+ALLTRIM(cTrkNumber)+csegd TO cString
			DO cstringbreak
			nSegCtr = nSegCtr + 1
		ENDIF

		STORE "G62"+cfd+"10"+cfd+TRIM(DTOS(dDel_date))+cfd+"D"+cfd+cDelTime+csegd TO cString  && Ship date/time
		DO cstringbreak
		nSegCtr = nSegCtr + 1

		STORE "G62"+cfd+"17"+cfd+TRIM(DTOS(dexpdel_date))+cfd+"D"+cfd+cDelTime+csegd TO cString  && Ship date/time
		DO cstringbreak
		nSegCtr = nSegCtr + 1

		SELECT scacs
		IF lTesting AND (EMPTY(m.scac) OR EMPTY(m.ship_via))
			STORE "RDWY" TO m.scac
			STORE "ROADWAY" TO m.ship_via
		ELSE
			IF !EMPTY(TRIM(outship.scac))
				STORE TRIM(outship.scac) TO m.scac
				STORE outship.ship_via TO m.ship_via
			ELSE
				WAIT CLEAR
				cErrMsg = "MISSING SCAC"
				DO ediupdate WITH cErrMsg,.T.
				THROW
			ENDIF
		ENDIF

		SELECT outship

		IF lParcelType
			cCarrierType = "U" && Parcel as UPS or FedEx
		ENDIF

		STORE "W27"+cfd+cCarrierType+cfd+TRIM(m.scac)+cfd+TRIM(m.ship_via)+csegd TO cString
		DO cstringbreak
		nSegCtr = nSegCtr + 1

		IF lParcelType AND !INLIST(outship.scac,"UPSL","FGC","UPGC","FDXG") AND !lSkipCharges
			IF EMPTY(cFreight)
				SELECT shipment
				IF SEEK(cShip_ref,"shipment","pickticket")
					nShipmentId = shipment.shipmentid
					SELECT package
					SUM package.pkgcharge TO nFreight FOR package.shipmentid=nShipmentId
					IF nFreight = 0
						IF !lAmazon
							WAIT WINDOW "Missing UPS Charges" TIMEOUT 2
							cErrMsg = "MISS UPS CHG"
							DO ediupdate WITH cErrMsg,.T.
							THROW
						ENDIF
					ELSE
						cFreight = STRTRAN(ALLTRIM(STR(nFreight,10,2)),".","")
						cFreight = ALLTRIM(STR(nFreight,10,2))
					ENDIF
				ENDIF
			ENDIF
			STORE "G72"+cfd+"503"+cfd+"CA"+cfd+cFreight+csegd TO cString
			DO cstringbreak
			nSegCtr = nSegCtr + 1
			cFreight = ""
		ENDIF

		IF lRepeatPT
			nRepLoops = 2
			SELECT outship
			nRec = RECNO()
			LOCATE

			LOCATE FOR outship.ship_ref = PADR(TRIM(cAltShip_ref),20) AND (!EMPTYnul(del_date)) && AND del_date=dDel_date)
			IF !FOUND()
				IF DATE() > {^2011-08-21}
					WAIT CLEAR
					WAIT WINDOW "Problem: Alternate PT "+cAltShip_ref+" not found" TIMEOUT 3
					cErrMsg = "ALT PT MISSING: "+cAltShip_ref
					ediupdate(cErrMsg,.T.)
					THROW
				ELSE
					m.ctnqty = m.ctnqty + m.outctnqty
					m.weight = m.weight + m.outweight
					nRepLoops = 1
				ENDIF
			ELSE
				IF m.outweight = 0
					REPLACE outship.weight WITH 5*m.outctnqty
				ENDIF
				m.ctnqty = m.ctnqty + m.outctnqty
				m.weight = m.weight + m.outweight
				nAltOutshipid = outship.outshipid
				IF lParcelType
					cPTString = IIF(EMPTY(cPTString),m.consignee+" "+cBOL+" "+cAltShip_ref,cPTString+CHR(13)+m.consignee+" "+cBOL+" "+cAltShip_ref)
				ELSE
					cPTString = IIF(EMPTY(cPTString),m.consignee+" "+cAltShip_ref,cPTString+CHR(13)+m.consignee+" "+cAltShip_ref)
				ENDIF
			ENDIF
			GO nRec
		ELSE
			nRepLoops = 1
		ENDIF

*************************************************************************
*2	DETAIL LOOP - MATCHING SHIP_REF FIELD (FROM OUTSHIP)
*************************************************************************
		WAIT CLEAR
		WAIT WINDOW "Now creating Detail information "+cShip_ref NOWAIT NOCLEAR
*		ASSERT .F. MESSAGE "In Detail loop"

		SET DELETED ON
		IF lOverflow
			SELECT tempov
			LOCATE FOR tempov.ship_ref = cShip_ref
			IF FOUND()
				SET DELETED OFF
			ENDIF
		ENDIF

		FOR tx = 1 TO nRepLoops
			IF lRepeatPT
				lPrepack=IIF("C"$cShip_ref,.T.,.F.)
				IF tx = 2
					SELECT outship
					LOCATE
					LOCATE FOR outship.ship_ref = PADR(TRIM(cAltShip_ref),20)
					lPrepack = IIF("~C"$cAltShip_ref,.T.,.F.)
					lPick = IIF(lPrepack,.F.,.T.)
				ENDIF
			ENDIF

			SELECT vmoretedipp

			IF lRepeatPT
				LOCATE
				LOCATE FOR vmoretedipp.outshipid = outship.outshipid
			ENDIF

*			COPY TO f:\3pl\data\vmoretedipp  && Backup copy

			IF lDelOff
				IF lPrepack
					SELECT ucc,outdetid,SUM(origqty) AS totqty ;
						FROM vmoretedipp ;
						WHERE totqty > 0 ;
						GROUP BY 1,2 ;
						INTO CURSOR vtemp
				ELSE
					SELECT ucc,outdetid,origqty ;
						FROM vmoretedipp ;
						INTO CURSOR vtemp
				ENDIF
			ELSE
				IF lPrepack
					SELECT ucc,outdetid,SUM(totqty) AS totqty ;
						FROM vmoretedipp ;
						WHERE totqty > 0 ;
						GROUP BY 1,2 ;
						INTO CURSOR vtemp
				ELSE
					SELECT ucc,outdetid,totqty ;
						FROM vmoretedipp ;
						INTO CURSOR vtemp
				ENDIF
			ENDIF
			LOCATE


			SELECT outdet
			SET ORDER TO TAG outdetid
			SELECT vmoretedipp
*!*				SET RELATION TO outdetid INTO outdet

			IF tx = 1
				scanstr = "vmoretedipp.ship_ref = cShip_ref" && and vmoretedipp.outshipid = nOutshipid"
			ELSE
				scanstr = "vmoretedipp.ship_ref = cAltShip_ref" && and vmoretedipp.outshipid = nAltOutshipid"
			ENDIF

			LOCATE
			LOCATE FOR &scanstr && vmoretedipp.outshipid = IIF(tx=1,nOutshipid,nAltOutshipid)
			IF !FOUND()
				IF !lTesting
					SET STEP ON
					WAIT WINDOW "PT "+cShip_ref+" NOT FOUND in vmoretedipp...ABORTING" TIMEOUT 2
					IF !lTesting
						lSQLMail = .T.
					ENDIF
					cErrMsg = "MISS PT-SQL: "+cShip_ref
					DO ediupdate WITH cErrMsg,.T.
					THROW
				ELSE
					LOOP
				ENDIF
			ENDIF

*		ASSERT .F. MESSAGE "At outdet line scan"
			scanstr = "vmoretedipp.outshipid = IIF(tx=1,nOutshipid,nAltOutshipid)" && and vmoretedipp.ship_ref = IIF(tx=1,cShip_ref,cAltShip_ref)

			SELECT vmoretedipp
			LOCATE
			LOCATE FOR &scanstr
			IF !FOUND()
				ASSERT .F. MESSAGE "At OSID loop seg error"
				SET STEP ON
				IF !lTesting
					WAIT CLEAR
					cOSID = ALLT(STR(outship.outshipid))
					WAIT WINDOW "OSID "+cOSID+" NOT FOUND in V"+ccustname+"...ABORTING" TIMEOUT 5
					IF !lTesting
						lSQLMail = .T.
					ENDIF
					cErrMsg = "MISS OSID-SQL: "+cOSID
					ediupdate(cErrMsg,.T.)
					THROW
				ELSE
					LOOP
				ENDIF
			ENDIF

			cCartonNum= "XXX"
			nCutUCCs = 0
			DO WHILE &scanstr
				cUCCLookup = ALLT(vmoretedipp.ucc)
				IF vmoretedipp.totqty = 0
					SKIP 1 IN vmoretedipp
					LOOP
				ENDIF

				nmorrec = RECNO()
				IF SEEK(cUCCLookup,'cutuccs','ucc')
					WAIT CLEAR
					WAIT WINDOW "Cutting UCC "+cUCCLookup &cTimeout
					nCutUCCs = nCutUCCs + 1
					SKIP 1 IN vmoretedipp
					LOOP
				ENDIF

				IF "CUTS"$cUCCLookup
*					ASSERT .F. MESSAGE "At CUTS message"
*					BROWSE TIMEOUT 10
					SKIP 1 IN vmoretedipp
					LOOP
				ENDIF

				IF TRIM(vmoretedipp.ucc) <> cCartonNum
					STORE TRIM(vmoretedipp.ucc) TO cCartonNum
				ENDIF

				STORE "LX"+cfd+ALLTRIM(STR(nCtnNumber))+csegd TO cString   && Carton Seq. # (up to Carton total)
				lDoManSegment = .T.
				lDoPALSegment = .F.
				DO cstringbreak
				nSegCtr = nSegCtr + 1

				SELECT * ;
					FROM vmoretedipp ;
					WHERE vmoretedipp.ucc = cCartonNum ;
					INTO CURSOR tempmoretctn READWRITE
				LOCATE

				ctnrollup()

				SELECT vmoretedipp
				GO nmorrec
				nOutdetid = outdet.outdetid
				WAIT WIND nOutdetid NOWAIT NOCLEAR

				SELECT tempmoretctn
				SET RELATION TO outdetid INTO outdet
				LOCATE
				lMANRec = .T.
				SCAN FOR !DELETED() AND totqty > 0
					IF EMPTY(ALLTRIM(outdet.printstuff))  && Added to trap re-added detail lines, 10.05.2012
						cErrMsg = "EMPTY PRINTSTUFF at OUTDETID "+TRANSFORM(outdet.outdetid)
						DO ediupdate WITH cErrMsg,.T.
						THROW
					ENDIF
					alength = ALINES(aptdet,outdet.printstuff,.T.,CHR(13))
					cUCCNumber = tempmoretctn.ucc
					IF EMPTY(cUCCNumber) OR ISNULL(cUCCNumber)
						WAIT CLEAR
						WAIT WINDOW "Empty UCC Number in vmoretedipp "+cShip_ref TIMEOUT 2
						lSQLMail = .T.
						cErrMsg = "EMPTY UCC# in "+cShip_ref
						DO ediupdate WITH cErrMsg,.T.
						THROW
					ENDIF
					cUCCNumber = TRIM(STRTRAN(TRIM(cUCCNumber)," ",""))

					IF lDoManSegment
						lDoManSegment = .F.
						IF lAmazon AND lParcelType
							cTrkNumber = ALLTRIM(trknumbers.trknumber)
							IF !EOF('trknumbers')
								SKIP 1 IN trknumbers
							ENDIF
						ENDIF

						IF lParcelType
							STORE "MAN"+cfd+"GM"+cfd+TRIM(cUCCNumber)+cfd+cTrkNumber+csegd TO cString
						ELSE
							STORE "MAN"+cfd+"GM"+cfd+TRIM(cUCCNumber)+csegd TO cString
						ENDIF

						IF EMPTY(outdet.ctnwt) OR outdet.ctnwt = 0
							cCtnWt = ALLTRIM(STR(CEILING(outship.weight/IIF(lMasterpack,1,outship.ctnqty))))
							IF EMPTY(cCtnWt) OR INT(VAL(cCtnWt)) = 0
								cCtnWt = "5"
							ENDIF
							nTotCtnWt = nTotCtnWt + CEILING(VAL(cCtnWt))
						ELSE
							STORE ALLTRIM(STR(outdet.ctnwt)) TO cCtnWt
							nTotCtnWt = nTotCtnWt + outdet.ctnwt
						ENDIF

						nShipDetQty = tempmoretctn.totqty
						IF ISNULL(nShipDetQty) OR nShipDetQty = 0
							nShipDetQty = outdet.totqty
						ENDIF
						IF nShipDetQty = 0
							IF lMANRec
								lDoManSegment = .T.
							ENDIF
							LOOP
						ENDIF

						lMANRec = .F.
						nUnitSum = nUnitSum + nShipDetQty
						DO cstringbreak
						nSegCtr = nSegCtr + 1
					ELSE
						nShipDetQty = tempmoretctn.totqty
						IF ISNULL(nShipDetQty) OR nShipDetQty = 0
							nShipDetQty = outdet.totqty
						ENDIF
						IF nShipDetQty = 0
							LOOP
						ENDIF
						nUnitSum = nUnitSum + nShipDetQty
					ENDIF

					cColor = TRIM(outdet.COLOR)
					cSize = TRIM(outdet.ID)
					cUPC = TRIM(outdet.upc)

					IF (ISNULL(cUPC) OR EMPTY(cUPC))
						cUPC = TRIM(tempmoretctn.upc)
					ENDIF

					cStyle = TRIM(outdet.STYLE)
					cUOM = "EA"
					cStyleOut = ""
					cColorout = TRIM(segmentget(@aptdet,"COLOROUT",alength))
					cStyleOut = TRIM(segmentget(@aptdet,"ORIGSTYLE",alength))

					IF !EMPTY(cStyleOut) && If a converted style
						cStyle = STRTRAN(cStyleOut,'-','/')
					ELSE
						cStyle = cStyle+'/'+cColorout
					ENDIF

					cItemNum = TRIM(outdet.custsku)

					nOrigDetQty = tempmoretctn.qty
					IF ISNULL(nOrigDetQty)
						nOrigDetQty = nShipDetQty
					ENDIF

					nShipStat = "SH"  && "Shipped", per Avner
					STORE "W12"+cfd+nShipStat+cfd+cfd+ALLTRIM(STR(nShipDetQty))+cfd+cfd+;
						cUOM+cfd+cfd+"VA"+cfd+cStyle+csegd TO cString
					DO cstringbreak
					nSegCtr = nSegCtr + 1
				ENDSCAN
				nCtnNumber = nCtnNumber + 1
				SELECT vmoretedipp
				DO WHILE vmoretedipp.ucc = cCartonNum
					SKIP 1 IN vmoretedipp
				ENDDO
			ENDDO
		ENDFOR

*************************************************************************
*2^	END DETAIL MATCHING SHIP_REF LOOP
*************************************************************************

		WAIT CLEAR
		WAIT WINDOW "Now creating Section Closure information" NOWAIT
*		SET DECIMALS TO 2
		nCuFT = CEILING(nCuFT)
		STORE "W03"+cfd+ALLTRIM(STR(nUnitSum))+cfd+ALLTRIM(STR(nTotCtnWt))+cfd+;
			cWeightUnit+cfd+ALLTRIM(STR(nCuFT,10,0))+cfd+cCubeUnit+csegd TO cString   && Units sum, Weight sum, carton count

		nTotCtnWt = 0
		nTotCtnCount = 0
		nUnitSum = 0
		FPUTS(nFilenum,cString)
*		SET DECIMALS TO

		SELECT outship
		WAIT CLEAR
		IF tx => 2 AND nRepLoops = 2
*			GO nRec
		ENDIF
		STORE  "SE"+cfd+ALLTRIM(STR(nSegCtr+2))+cfd+PADL(c_GrpCntrlNum,9,"0")+csegd TO cString
		FPUTS(nFilenum,cString)
	ENDSCAN

	IF nCutUCCs > 0
		WAIT CLEAR
		WAIT WINDOW "Cut a total of "+ALLT(STR(nCutUCCs)) &cTimeout
	ENDIF


*************************************************************************
*1^	END OUTSHIP MAIN LOOP
*************************************************************************
* ASSERT .f. MESSAGE "At close of outship loop...>>DEBUG<<"
	DO close945
	=FCLOSE(nFilenum)
	IF !lTesting
		SELECT edi_trigger
		DO ediupdate WITH "945 CREATED",.F.
		SELECT edi_trigger
		LOCATE

		IF !USED("ftpedilog")
			SELECT 0
			USE F:\edirouting\ftpedilog ALIAS ftpedilog
			INSERT INTO ftpedilog (TRANSFER,ftpdate,filename,acct_name,TYPE) VALUES ("945-"+ccustname+"-"+cCustLoc,dt2,cFilenameShort,UPPER(ccustname),"945")
			USE IN ftpedilog
		ENDIF
	ENDIF

	WAIT CLEAR
*	WAIT WINDOW cCustFolder+" 945 Process complete..." TIMEOUT 1

*!* Create eMail confirmation message

*	ASSERT .F. MESSAGE "At eMail confirmation message"
	cCustLocMail = IIF(cOffice = "E","SE",cCustLoc)
	tsubject = cMailName+" 945 "+IIF(lTesting,"TEST ","")+"Std. EDI File from Toll as of "+dtmail+" ("+cCustLocMail+")"
	tattach = " "
	IF lParcelType
		tmessage = "945 EDI Info from Toll, for division "+cDivision+", WO# "+ALLTRIM(cWO_Num)+CHR(13)
	ELSE
		tmessage = "945 EDI Info from Toll, for division "+cDivision+", BOL# "+ALLTRIM(cBOL)+CHR(13)
	ENDIF

	IF lFakeDelivery
		tmessage = tmessage+CHR(13)+"*** THIS IS A 'TEST' 945 PER YOUR INSTRUCTIONS ***"
		tmessage = tmessage+CHR(13)+"*** File(s) will be sent by separate email ***"+CHR(13)+CHR(13)
	ENDIF

	tmessage = tmessage + "Filename: "+cFilenameShort+CHR(13)
	tmessage = tmessage + "for Work Orders: "+cWO_NumStr+","+CHR(13)
	tmessage = tmessage + "containing these "+ALLTRIM(STR(nPTCount))+" picktickets:"+CHR(13)
	tmessage = tmessage + cPTString + CHR(13)
	tmessage = tmessage +"has been created and will be transmitted ASAP."+CHR(13)+CHR(13)

	IF !EMPTY(cMissDel) AND !lFakeDelivery
		tmessage = tmessage+CHR(13)+CHR(13)+cMissDel+CHR(13)+CHR(13)
	ENDIF

	IF lParcelType AND lSkipCharges
		tmessage = tmessage+CHR(13)+CHR(13)+"NOTE: There were no parcel charges; this is a Third Party Shipment"
	ENDIF

	tmessage = tmessage+CHR(13)+CHR(13)+ "If you have any questions, please eMail or call 732-750-9000 x217."
	IF lEmail
*		ASSERT .F. MESSAGE "At mail send"
		DO FORM m:\dev\frm\dartmail2 WITH tsendto,tfrom,tsubject,tcc,tattach,tmessage,"A"
	ENDIF

	SELECT zeroqty
	IF RECCOUNT()>0
		tsendto = tsendtozero
		tcc = tcczero
		tsubject = cMailName+" Zero Qty PTs in BOL "+cBOL
		tmessage = "The following picktickets on BOL# "+cBOL+" show zero quantity in the outbound table:"+CHR(13)
		tmessage = PADR("CONSIGNEE",42)+PADR("PT#",22)+"PO#"+CHR(13)
		SCAN
			tmessage = tmessage+CHR(13)+PADR(ALLT(zeroqty.consignee),42)+PADR(ALLT(zeroqty.ship_ref),22)+ALLT(zeroqty.cnee_ref)
		ENDSCAN
		IF lEmail
			DO FORM m:\dev\frm\dartmail2 WITH tsendto,tfrom,tsubject,tcc,tattach,tmessage,"A"
		ENDIF
	ENDIF

	RELEASE ALL LIKE c_CntrlNum,c_GrpCntrlNum
	WAIT CLEAR
	WAIT WINDOW cMailName+" 945 EDI File output complete" TIMEOUT 1

	IF !EMPTY(cRolledRecs)
		tsubject = cMailName+" 945 "+IIF(lTesting,"TEST ","")+"AMT EDI File from Toll as of "+dtmail+" ("+cCustLocMail+")"
		tsendto = tsendtotest
		tcc = tcctest
		tmessage = "The following PT(s) from BOL# "+cBOL+" had style rollups in the Cartons data"+CHR(13)+CHR(13)+cRolledRecs
		IF lEmail
			DO FORM m:\dev\frm\dartmail2 WITH tsendto,tfrom,tsubject,tcc,tattach,tmessage,"A"
		ENDIF
	ENDIF

*!* Transfers files to correct output folders
*	ASSERT .F. MESSAGE "At FilesOut Statement...debug"
	COPY FILE &cFilenameHold TO &cFilenameArch
	IF lDoFilesout AND !lTesting AND !lTestinput
		IF !lFakeDelivery
			TRY
				COPY FILE [&cFilenameHold] TO [&cFilenameOut]
				IF lCloseOutput
					=FCLOSE(nFilenum)
					DELETE FILE [&cFilenameHold]
				ENDIF
			CATCH TO oERR
				IF oERR.ERRORNO =  1102  && Can't create file
					DELETE FILE [&cFilenameHold]
					tsendto = tsendtoerrmor
					tcc = tccerrmor
					tsubject = "945 File Copy Error at "+TTOC(DATETIME())
					tattach  = ""
					tfrom    ="TOLL EDI 945 Poller Operations <toll-edi-ops@tollgroup.com>"
					tmessage = ALLTRIM(oERR.MESSAGE)
					DO FORM m:\dev\frm\dartmail2 WITH tsendto,tfrom,tsubject,tcc,tattach,tmessage,"A"
					THROW
				ENDIF
			ENDTRY
			SELECT temp945
			COPY TO "f:\3pl\data\temp945a.dbf"
			USE IN temp945
			SELECT 0
			USE "f:\3pl\data\temp945a.dbf" ALIAS temp945a
			SELECT 0
			USE "f:\3pl\data\pts_sent945.dbf" ALIAS pts_sent945
			APPEND FROM "f:\3pl\data\temp945a.dbf"
			USE IN pts_sent945
			USE IN temp945a
			DELETE FILE "f:\3pl\data\temp945a.dbf"
		ELSE
			WAIT WINDOW "Won't move files into 945OUT folder" TIMEOUT 2
		ENDIF
	ENDIF

	IF !lTesting
*		asn_out_data()
	ENDIF

CATCH TO oERR
	=FCLOSE(nFilenum)
	IF lDoCatch
		SET STEP ON
		IF oERR.DETAILS = "TRKNUMBERS"
			ediupdate("BAD PS BOL",.T.)
		ENDIF
		ASSERT .F. MESSAGE "In catch...debug"
*		lEmail = .F.
		tsubject = ccustname+" Error ("+TRANSFORM(oERR.ERRORNO)+") at "+TTOC(DATETIME())
		tattach  = ""
		IF lTesting
			SELECT 0
			USE F:\3pl\DATA\mailmaster ALIAS mm
			LOCATE FOR taskname = "GENERAL"
			lUseAlt = IIF(mm.use_alt,.T.,.F.)
			tsendto = ALLTRIM(IIF(lUseAlt,mm.sendtoalt,mm.sendto))
			tcc = ALLTRIM(IIF(lUseAlt,mm.ccalt,mm.cc))
			USE IN mm
		ELSE
			tsendto  = tsendtoerrmor
			tcc = tccerrmor
		ENDIF
		SET STEP ON
		tmessage = ccustname+" Error processing "+CHR(13)
		tmessage =tmessage+ "Try/CATCH Exeception Message:"+CHR(13)+;
			[  Error: ] + STR(oERR.ERRORNO) +CHR(13)+;
			[  LineNo: ] + STR(oERR.LINENO) +CHR(13)+;
			[  Message: ] + oERR.MESSAGE +CHR(13)+;
			[  Procedure: ] + oERR.PROCEDURE +CHR(13)+;
			[  Details: ] + oERR.DETAILS +CHR(13)+;
			[  StackLevel: ] + STR(oERR.STACKLEVEL) +CHR(13)+;
			[  LineContents: ] + oERR.LINECONTENTS+CHR(13)+;
			[  UserValue: ] + oERR.USERVALUE

		tmessage =tmessage+CHR(13)+CHR(13)+cErrMsg+CHR(13)+"BOL# "+cBOL

		tsubject = "945 EDI Poller Error at "+TTOC(DATETIME())
		tattach  = ""
		tfrom    ="Toll EDI 945 Poller Operations <toll-edi-ops@tollgroup.com>"
		DO FORM m:\dev\frm\dartmail2 WITH tsendto,tfrom,tsubject,tcc,tattach,tmessage,"A"
	ENDIF
FINALLY
	SET DELETED ON
	ON ERROR
	IF lTesting
		CLOSE DATABASES ALL
	ELSE
		CLOSETABLES()
		SELECT edi_trigger
		LOCATE
	ENDIF
ENDTRY

*** END OF CODE BODY

****************************
PROCEDURE close945
****************************
** Footer Creation

	STORE  "GE"+cfd+ALLTRIM(STR(nSTCount))+cfd+c_CntrlNum+csegd TO cString
	FPUTS(nFilenum,cString)

	STORE  "IEA"+cfd+"1"+cfd+PADL(c_CntrlNum,9,"0")+csegd TO cString
	FPUTS(nFilenum,cString)

	RETURN

****************************
PROCEDURE num_incr_isa
****************************

	IF !USED("serfile")
		USE ("f:\3pl\data\serial\"+ccustname+"945_serial") IN 0 ALIAS serfile
	ENDIF
	SELECT serfile
	IF lTesting AND lISAFlag
		nOrigSeq = serfile.seqnum
		lISAFlag = .F.
	ENDIF
	nISA_Num = serfile.seqnum
	c_CntrlNum = ALLTRIM(STR(serfile.seqnum))
	REPLACE serfile.seqnum WITH serfile.seqnum + 1 IN serfile
	SELECT outship
	RETURN

****************************
PROCEDURE num_incr_st
****************************

	IF !USED("serfile")
		USE ("f:\3pl\data\serial\"+ccustname+""945_serial") IN 0 ALIAS serfile
	ENDIF
	SELECT serfile
	IF lTesting AND lSTFlag
		nOrigGrpSeq = serfile.grpseqnum
		lSTFlag = .F.
	ENDIF
	c_GrpCntrlNum = ALLTRIM(STR(serfile.grpseqnum))
	REPLACE serfile.grpseqnum WITH serfile.grpseqnum + 1 IN serfile
	SELECT outship
ENDPROC

****************************
PROCEDURE segmentget
****************************

	PARAMETER thisarray,lcKey,nLength

	FOR i = 1 TO nLength
		IF i > nLength
			EXIT
		ENDIF
		lnEnd= AT("*",thisarray[i])
		IF lnEnd > 0
			lcThisKey =TRIM(SUBSTR(thisarray[i],1,lnEnd-1))
			IF OCCURS(lcKey,lcThisKey)>0
				RETURN SUBSTR(thisarray[i],lnEnd+1)
				i = 1
			ENDIF
		ENDIF
	ENDFOR

	RETURN ""


****************************
PROCEDURE closedown
****************************
	DO ediupdate WITH "Error in Moret 945 Process",.T.
	THROW
ENDPROC

****************************
PROCEDURE ediupdate
****************************
	PARAMETER cStatus,lIsError
	lDoCatch = .F.
*ASSERT .f. MESSAGE "In ediupdate...debug"
	IF !lTesting
		SELECT edi_trigger
		nRec = RECNO()
		IF !lIsError
			REPLACE processed WITH .T.,proc945 WITH .T.,file945 WITH UPPER(cFilenameArch),;
				fin_status WITH "945 CREATED",errorflag WITH .F.,when_proc WITH DATETIME() ;
				isa_num WITH cISA_Num ;
				FOR edi_trigger.bol = cBOL
		ELSE
			IF cStatus = "ZERO QTY BOL. CHECK!"
				REPLACE processed WITH .T.,proc945 WITH .F.,file945 WITH "",;
					fin_status WITH cStatus,errorflag WITH .T. ;
					FOR edi_trigger.bol = cBOL
				zeroqtybol()
			ELSE
				REPLACE processed WITH .T.,proc945 WITH .F.,file945 WITH "",;
					fin_status WITH cStatus,errorflag WITH .T. ;
					FOR edi_trigger.bol = cBOL
			ENDIF

		ENDIF
	ENDIF

*ASSERT .F. MESSAGE "At ERROR mail in EDIUPDATE...debug"
	IF lIsError AND lEmail AND !INLIST(cStatus,"SQL ERROR","ZERO QTY BOL")
		SET STEP ON
		tsubject = "945 Error in MORET BOL "+TRIM(cBOL)+"(At PT "+cShip_ref+")"
		IF cStatus = "There are missing detail PTs/Styles"
			tattach = "c:\tempfox\tempmissdet.xls"
		ELSE
			tattach = " "
		ENDIF
*!*			IF lTesting OR lTestinput
		SELECT 0
		USE F:\3pl\DATA\mailmaster ALIAS mm
		LOCATE
		LOCATE FOR taskname = "GENERAL"
		lUseAlt = IIF(mm.use_alt,.T.,.F.)
		tsendto = ALLTRIM(IIF(lUseAlt,mm.sendtoalt,mm.sendto))
		tcc = ALLTRIM(IIF(lUseAlt,mm.ccalt,mm.cc))
		USE IN mm

		tmessage = "AMT 945 Processing for BOL# "+cBOL+", WO# "+cWO_Num+" produced this error: (Office: "+cOffice+"): "+cStatus+CHR(13)+"Check EDI_TRIGGER and re-run"
		IF "TOTQTY ERR"$cStatus
			tmessage = tmessage + CHR(13) + "At OUTSHIPID: "+ALLTRIM(STR(m.outshipid))
		ENDIF

		DO FORM m:\dev\frm\dartmail2 WITH tsendto,tfrom,tsubject,tcc,tattach,tmessage,"A"
	ENDIF
	CLOSETABLES()
ENDPROC

************************
PROCEDURE CLOSETABLES
************************

	IF USED('outship')
		USE IN outship
	ENDIF
	IF USED('outdet')
		USE IN outdet
	ENDIF
	IF USED('outwolog')
		USE IN outwolog
	ENDIF

	IF USED('serfile')
		SELECT serfile
		IF lTesting
			REPLACE serfile.seqnum WITH nOrigSeq
			REPLACE serfile.grpseqnum WITH nOrigGrpSeq
		ENDIF
		USE IN serfile
	ENDIF
	IF USED('scacs')
		USE IN scacs
	ENDIF
	IF USED('mm')
		USE IN mm
	ENDIF
	IF USED('tempx')
		USE IN tempx
	ENDIF
	IF USED('shipment')
		USE IN shipment
	ENDIF
	IF USED('package')
		USE IN package
	ENDIF
	IF USED('vmoretedipp')
*		USE IN vmoretedipp
	ENDIF
ENDPROC

****************************
PROCEDURE cstringbreak
****************************
	cLen = LEN(ALLTRIM(cString))
	FPUTS(nFilenum,cString)
ENDPROC

****************************
PROCEDURE ctnrollup
****************************
	SELECT tempmoretctn
	nreccnt = RECCOUNT()
	SCAN FOR RECNO()<nreccnt
		nRect = RECNO()
		cStyle = tempmoretctn.STYLE
		LOCATE FOR tempmoretctn.STYLE = cStyle AND RECNO() > nRect
		IF FOUND()
			IF EMPTY(cRolledRecs)
				cRolledRecs = PADR("PICKTICKET",25)+PADR("UCC#",25)+"STYLE"
				cRolledRecs = cRolledRecs+CHR(13)+(PADR(cShip_ref,25)+PADR(cCartonNum,25)+tempmoretctn.STYLE)
			ELSE
				cRolledRecs = cRolledRecs+CHR(13)+(PADR(cShip_ref,25)+PADR(cCartonNum,25)+tempmoretctn.STYLE)
			ENDIF

			nAddQty = tempmoretctn.totqty
			DELETE NEXT 1 IN tempmoretctn
			GO nRect
			REPLACE tempmoretctn.totqty WITH tempmoretctn.totqty + nAddQty
			REPLACE tempmoretctn.qty WITH tempmoretctn.qty + nAddQty
		ENDIF
		GO nRect
	ENDSCAN
ENDPROC

****************************
PROCEDURE zeroqtybol
****************************
	tsubject = "Moret: Zero Qty. BOL "+TRIM(cBOL)
	SELECT 0
	USE F:\3pl\DATA\mailmaster ALIAS mm
	LOCATE FOR edi_type = "MISC" AND taskname = "MORETZEROBOL"
	tsendto = ALLTRIM(IIF(mm.use_alt,mm.sendtoalt,mm.sendto))
	tcc = ALLTRIM(IIF(mm.use_alt,mm.ccalt,mm.cc))
	USE IN mm
	tmessage = "945 Processing for BOL# "+cBOL+" was halted due to zero quantity. No 945 file will be produced for this shipment."
	tattach = ""
	DO FORM m:\dev\frm\dartmail2 WITH tsendto,tfrom,tsubject,tcc,tattach,tmessage,"A"
ENDPROC
