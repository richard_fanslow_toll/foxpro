**kpiLastModified.prg
**
**
DO setEnvi

cAppFile = Sys(16,1) &&path of currently running app
cAppPath = Left(cAppFile, Rat("\",cAppFile))

Create Cursor csrDir (fileName c(15), fileSize n(20,0), modifyDt d(1), modifyTm c(20), modifyDtTm t(1))
=ADir(aKpiDir, cAppPath+"sl*.exe")
Insert into csrDir from array aKpiDir
Replace all modifyDtTm with Ctot(Dtoc(modifyDt)+" "+Alltrim(modifyTm))
Delete For Lower(fileName) = "sl.exe"

Select * from csrDir order by modifyDtTm desc into cursor csrLastModified
cFile = cAppPath+csrLastModified.fileName

Try
  Run /N &cFile

Catch
  For i = 1 to 99
	cNewFile = cAppPath+"kpi"+Alltrim(Str(i))+".exe"
	If !File(cNewFile)
	  Exit
	EndIf
  EndFor

  Copy File &cFile to &cNewFile
  Run F:\mm\touch.exe &cNewFile
  Run /N &cNewFile
EndTry

Return
