*utilsetup("MJ_RETURNS_WHLSALE")

Set Exclusive Off
Set Deleted On
Set Talk Off
Set Safety Off
*!*	WITH _SCREEN
*!*		.AUTOCENTER = .T.
*!*		.WINDOWSTATE = 0
*!*		.BORDERSTYLE = 1
*!*		.WIDTH = 320
*!*		.HEIGHT = 210
*!*		.TOP = 290
*!*		.LEFT = 110
*!*		.CLOSABLE = .F.
*!*		.MAXBUTTON = .F.
*!*		.MINBUTTON = .F.
*!*		.SCROLLBARS = 0
*!*		.CAPTION = "MJ_RETURNS_WHLSALE"
*!*	ENDWITH
goffice='J'

xsqlexec("select * from cmtrans",,,"wh")
index on trolly tag trolly
set order to

*!*	If !Used("inwolog")
*!*		Use F:\whj\whdata\inwolog.Dbf In 0
*!*	ENDIF
SET STEP ON 
goffice='J'
xsqlexec("select * from pl where mod='"+goffice+"'",,,"wh")
Index On plid Tag plid
Set Order To

*!*	If !Used("indet")
*!*		Use F:\whj\whdata\indet.Dbf In 0
*!*	Endif

xsqlexec("select * from invenloc where mod='J'",,,"wh")

*****Missing WO for returns (PL scanned)
Select Distinct trolly From cmtrans Where !wo_created And scantime>(date()-90)  And trolly!='ML' Into Cursor temp60
If Reccount() > 0
	Export To S:\MarcJacobsData\TEMP\SCANNED_RETURNS_WO_NOT_CREATED Xls
	tsendto = "tmarg@fmiint.com,yenny.garcia@tollgroup.com"
	tattach = "S:\MarcJacobsData\TEMP\SCANNED_RETURNS_WO_NOT_CREATED.XLS"
	tcc =""
	tFrom ="Toll EDI System Operations <fmi-transload-ops@fmiint.com>"
	tmessage = "Scanned returns WO not created"
	tSubject = "Scanned returns WO not created"
	Do Form m:\dev\frm\dartmail2 With tsendto,tFrom,tSubject, tcc,tattach,tmessage  ,"A"
Else
*!*	  tsendto = "tmarg@fmiint.com,yenny.garcia@tollgroup.com"
*!*	  tattach = ""
*!*	  tcc =""
*!*	  tFrom ="Toll EDI System Operations <fmi-transload-ops@fmiint.com>"
*!*	  tmessage = "MJ_RETURNS_WHLSALE  Wholesale returns scanned pallet wo not created_DO NOT EXIST "+Ttoc(Datetime())
*!*	  tSubject = "NO Wholesale returns scanned pallet wo not created EXIST "
*!*	  Do Form m:\dev\frm\dartmail2 With tsendto,tFrom,tSubject, tcc,tattach,tmessage  ,"A"
Endif




****Invalid PL entered
Select * From cmtrans Where  trolly!='PL'  And trolly!='ML' And scantime>(date()-90)   Into Cursor temp23
Select temp23
If Reccount() > 0
	Export To S:\MarcJacobsData\TEMP\invalid_PL Xls
	tsendto = "tmarg@fmiint.com,yenny.garcia@tollgroup.com"
	tattach = "S:\MarcJacobsData\TEMP\invalid_PL.xls"
	tcc =""
	tFrom ="Toll EDI System Operations <fmi-transload-ops@fmiint.com>"
	tmessage = "MJ RETURNS  Invalid PL in cmtrans"
	tSubject = "Invalid PL in cmtrans"
	Do Form m:\dev\frm\dartmail2 With tsendto,tFrom,tSubject, tcc,tattach,tmessage  ,"A"
Else

Endif


***Multiple accounts exist on a PL
Select Distinct trolly,inwonum From cmtrans Where  !Empty(inwonum) And scantime>(date()-90)  Into Cursor temp44
Select Cnt(1) As Count , trolly From temp44 Group By trolly Into Cursor temp45 Readwrite
Select trolly From temp45 Where Count!=1 Into Cursor temp46 Readwrite
If Reccount() > 0
	Export To S:\MarcJacobsData\TEMP\Multiple_accts_exist_on_pl Xls
	tsendto = "tmarg@fmiint.com,yenny.garcia@tollgroup.com"
	tattach = "S:\MarcJacobsData\TEMP\multplie_accts_exist_on_pl.xls"
	tcc =""
	tFrom ="Toll EDI System Operations <fmi-transload-ops@fmiint.com>"
	tmessage = "Multiple accounts exist on a PL, DO NOT create WO"
	tSubject = "Multiple accounts exist on a PL, DO NOT create WO"
	Do Form m:\dev\frm\dartmail2 With tsendto,tFrom,tSubject, tcc,tattach,tmessage  ,"A"
Else
	tsendto = "yenny.garcia@tollgroup.com"
	tattach = ""
	tcc =""
	tFrom ="Toll EDI System Operations <fmi-transload-ops@fmiint.com>"
	tmessage = "NO Invalid PL  account combo exists EXIST "+Ttoc(Datetime())
	tSubject = "NO Invalid PL  account combo exists"
	Do Form m:\dev\frm\dartmail2 With tsendto,tFrom,tSubject, tcc,tattach,tmessage  ,"A"
Endif

***** identifies multiple RA's on a PL
Select Distinct trolly,invloc From cmtrans Where scantime>(date()-90)  Into Cursor temp80
Select Cnt(1) As Count , trolly From temp80 Group By trolly Into Cursor temp81 Readwrite
Select trolly From temp81 Where Count!=1 Into Cursor temp82 Readwrite
If Reccount() > 0
	Export To S:\MarcJacobsData\TEMP\multiple_RAs_exist_on_PL Xls
	tsendto = "tmarg@fmiint.com,yenny.garcia@tollgroup.com"
	tattach = "S:\MarcJacobsData\TEMP\multiple_RAs_exist_on_PL.xls"
	tcc =""
	tFrom ="Toll EDI System Operations <fmi-transload-ops@fmiint.com>"
	tmessage = "Multiple RA's on a PL exist, DO NOT create WO"
	tSubject = "Multiple RA's on a PL exist, DO NOT create WO"
	Do Form m:\dev\frm\dartmail2 With tsendto,tFrom,tSubject, tcc,tattach,tmessage  ,"A"
Else
	tsendto = "yenny.garcia@tollgroup.com"
	tattach = ""
	tcc =""
	tFrom ="Toll EDI System Operations <fmi-transload-ops@fmiint.com>"
	tmessage = "NO Multiple RA's on a PL exist "+Ttoc(Datetime())
	tSubject = "NO Multiple RA's on a PL exist"
	Do Form m:\dev\frm\dartmail2 With tsendto,tFrom,tSubject, tcc,tattach,tmessage  ,"A"
Endif


****identifies returns that need to be PUT AWAY to returns rack
Select accountid, Style,Color,Id,whseloc, locqty, adddt From invenloc Where whseloc='PL'  And locqty!=0 Into Cursor put1 Readwrite Order By whseloc
Select put1
If Reccount() > 0
	Export To S:\MarcJacobsData\TEMP\scanned_returns_to_put_away Xls
	tsendto = "tmarg@fmiint.com,yenny.garcia@tollgroup.com"
	tattach = "S:\MarcJacobsData\TEMP\scanned_returns_to_put_away.xls"
	tcc =""
	tFrom ="Toll EDI System Operations <fmi-transload-ops@fmiint.com>"
	tmessage = "Scanned returns to be put away exist"
	tSubject = "Scanned returns to be put away exist"
	Do Form m:\dev\frm\dartmail2 With tsendto,tFrom,tSubject, tcc,tattach,tmessage  ,"A"
Else
*!*	  tsendto = "tmarg@fmiint.com,yenny.garcia@tollgroup.com"
*!*	  tattach = ""
*!*	  tcc =""
*!*	  tFrom ="Toll EDI System Operations <fmi-transload-ops@fmiint.com>"
*!*	  tmessage = "NO Scanned returns to be put away exist "+Ttoc(Datetime())
*!*	  tSubject = "NO Scanned returns to be put away exist"
*!*	  Do Form m:\dev\frm\dartmail2 With tsendto,tFrom,tSubject, tcc,tattach,tmessage  ,"A"
Endif


****identifies returns that need to be PUT AWAY to general population
Select accountid, Style,Color,Id,whseloc, locqty, adddt From invenloc Where whseloc='RETPUT' And Inlist(accountid,6303,6325) And locqty!=0 Into Cursor put2 Readwrite Order By whseloc
Select put2
If Reccount() > 0
	Export To S:\MarcJacobsData\TEMP\returns_from_racks_to_put_away Xls
	tsendto = "tmarg@fmiint.com,yenny.garcia@tollgroup.com"
	tattach = "S:\MarcJacobsData\TEMP\returns_from_racks_to_put_away.xls"
	tcc =""
	tFrom ="Toll EDI System Operations <fmi-transload-ops@fmiint.com>"
	tmessage = "Returns from racks to be put away to general population exist"
	tSubject = "Returns from racks to be put away to general population exist"
	Do Form m:\dev\frm\dartmail2 With tsendto,tFrom,tSubject, tcc,tattach,tmessage  ,"A"
Else
*!*	  tsendto = "tmarg@fmiint.com,yenny.garcia@tollgroup.com"
*!*	  tattach = ""
*!*	  tcc =""
*!*	  tFrom ="Toll EDI System Operations <fmi-transload-ops@fmiint.com>"
*!*	  tmessage = "NO Returns from racks to be put away to general population exist "+Ttoc(Datetime())
*!*	  tSubject = "NO Returns from racks to be put away to general population exist"
*!*	  Do Form m:\dev\frm\dartmail2 With tsendto,tFrom,tSubject, tcc,tattach,tmessage  ,"A"
Endif


***identifies when the AX2012 Input button has not been hit
*!*	SELECT wo_num, CONTAINER,REFERENCE FROM inwolog where RETURNS AND INLIST(ACCOUNTID,6303,6325) AND !EMPTY(CONFIRMDT) AND EMPTY(AXDT) AND brokerref!='NO AX' AND BROKERREF!='BOL' INTO CURSOR temp44
*!*	SELECT temp44

*!*	IF reccount() > 0
*!*	EXPORT TO S:\MarcJacobsData\TEMP\ax2012_input_not_performed xls
*!*	  tsendto = "tmarg@fmiint.com,yenny.garcia@tollgroup.com"
*!*	  tattach = "S:\MarcJacobsData\TEMP\ax2012_input_not_performed.xls"
*!*	  tcc =""
*!*	  tFrom ="Toll EDI System Operations <fmi-transload-ops@fmiint.com>"
*!*	  tmessage = "AX2012 Input not performed"
*!*	  tSubject = "AX2012 Input not performed"
*!*	  Do Form m:\dev\frm\dartmail2 With tsendto,tFrom,tSubject, tcc,tattach,tmessage  ,"A"
*!*	ELSE
*!*	*!*	  tsendto = "tmarg@fmiint.com,yenny.garcia@tollgroup.com"
*!*	*!*	  tattach = ""
*!*	*!*	  tcc =""
*!*	*!*	  tFrom ="Toll EDI System Operations <fmi-transload-ops@fmiint.com>"
*!*	*!*	  tmessage = "NO AX2012 Input not performed "+Ttoc(Datetime())
*!*	*!*	  tSubject = "NO AX2012 Input not performed"
*!*	*!*	  Do Form m:\dev\frm\dartmail2 With tsendto,tFrom,tSubject, tcc,tattach,tmessage  ,"A"
*!*	ENDIF



***identifies WO's not created
Select Distinct trolly From cmtrans Where !wo_created And  scantime>(date()-90) And trolly='ML' Into Cursor temp60
If Reccount() > 0
	Export To S:\MarcJacobsData\TEMP\WO_not_created_for_ML Xls
	tsendto = "tmarg@fmiint.com,ana.hernandez@tollgroup.com,jimmy.saracay@tollgroup.com"
	tattach = "S:\MarcJacobsData\TEMP\WO_not_created_for_ML.xls"
	tcc =""
	tFrom ="Toll EDI System Operations <fmi-transload-ops@fmiint.com>"
	tmessage = "WO's not created"
	tSubject = "WO's not created"
	Do Form m:\dev\frm\dartmail2 With tsendto,tFrom,tSubject, tcc,tattach,tmessage  ,"A"
Else
*!*	  tsendto = "tmarg@fmiint.com,yenny.garcia@tollgroup.com"
*!*	  tattach = ""
*!*	  tcc =""
*!*	  tFrom ="Toll EDI System Operations <fmi-transload-ops@fmiint.com>"
*!*	  tmessage = "NO AX2012 Input not performed "+Ttoc(Datetime())
*!*	  tSubject = "NO AX2012 Input not performed"
*!*	  Do Form m:\dev\frm\dartmail2 With tsendto,tFrom,tSubject, tcc,tattach,tmessage  ,"A"
Endif



***identifies PL's missing RA's
Select Distinct trolly From cmtrans Where Empty(invloc) And  scantime>(date()-90) AND trolly='PL' Into Cursor temp70
If Reccount() > 0
	Export To S:\MarcJacobsData\TEMP\PL_missing_RA Xls
	tsendto = "tmarg@fmiint.com,yenny.garcia@tollgroup.com,doug.wachs@tollgroup.com"
	tattach = "S:\MarcJacobsData\TEMP\PL_missing_RA.xls"
	tcc =""
	tFrom ="Toll EDI System Operations <fmi-transload-ops@fmiint.com>"
	tmessage = "PL's missing RA, please supply RA and carton id"
	tSubject = "PL's missing RA"
	Do Form m:\dev\frm\dartmail2 With tsendto,tFrom,tSubject, tcc,tattach,tmessage  ,"A"
Else
*!*		tsendto = "yenny.garcia@tollgroup.com"
*!*		tattach = ""
*!*		tcc =""
*!*		tFrom ="Toll EDI System Operations <fmi-transload-ops@fmiint.com>"
*!*		tmessage = "NO AX2012 Input not performed "+Ttoc(Datetime())
*!*		tSubject = "NO AX2012 Input not performed"
*!*		Do Form m:\dev\frm\dartmail2 With tsendto,tFrom,tSubject, tcc,tattach,tmessage  ,"A"
Endif


Select Distinct trolly From cmtrans Where Empty(invloc) And  scantime>(date()-90) AND trolly='ML' Into Cursor temp70
If Reccount() > 0
	Export To S:\MarcJacobsData\TEMP\ML_missing_RA Xls
	tsendto = "tmarg@fmiint.com,ana.hernandez@tollgroup.com,jimmy.saracay@tollgroup.com"
	tattach = "S:\MarcJacobsData\TEMP\ML_missing_RA.xls"
	tcc =""
	tFrom ="Toll EDI System Operations <fmi-transload-ops@fmiint.com>"
	tmessage = "ML's missing RA, please supply RA and carton id"
	tSubject = "ML's missing RA"
	Do Form m:\dev\frm\dartmail2 With tsendto,tFrom,tSubject, tcc,tattach,tmessage  ,"A"
Else
*!*		tsendto = "yenny.garcia@tollgroup.com"
*!*		tattach = ""
*!*		tcc =""
*!*		tFrom ="Toll EDI System Operations <fmi-transload-ops@fmiint.com>"
*!*		tmessage = "NO AX2012 Input not performed "+Ttoc(Datetime())
*!*		tSubject = "NO AX2012 Input not performed"
*!*		Do Form m:\dev\frm\dartmail2 With tsendto,tFrom,tSubject, tcc,tattach,tmessage  ,"A"
Endif



Return ""

*!*	 Close Data All
*!*	schedupdate()
*!*	_Screen.Caption=gscreencaption




