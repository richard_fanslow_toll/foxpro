Parameters cFilter, cDateField, cCntField, cAddFields, cAddComment, cAddComment2, cCommentCol, lNotes, cDiffField, cStartRow, lAcctTotals
**cDiffField: specifies "dateDiff" field name - 2nd summary band (ie. edi timeliness) use datDiff2
**cStartRow: specifies start row - 2nd summary band (ie. edi timeliness) must be below the 1st (character)

	xxlsfile=iif(type("xxlsfile")="U","kpiTimeGeneric.XLS",xxlsfile)

	cAcctGroup = Iif(!lAcctTotals, "", "acctNumFld, ")
	cAcctOrder = Iif(!lAcctTotals, "", "acctNameFld, ")
	cAcctFlds = Iif(!lAcctTotals, "", "acctNumFld, acctNameFld,")
	lweekly=!empty(cacctnum) and (inlist(val(cAcctNum),&gmoretacctlist) or inlist(val(cAcctNum),&gmoretacctlist2))
	cgroupby = iif(lweekly,"year,week","year,month")
	cAddFields=iif(empty(cAddFields),"",", "+cAddFields)

	oworkbook.worksheets[1].range("A3") = Iif(lAcctTotals, "Account/Month", "Month")
	oworkbook.worksheets[1].columns[1].columnWidth = Iif(lAcctTotals, 33, 12)

	if lower(xxlsfile)="kpitimegeneric.xls"
		Store &cStartRow to nRow, nStartRow

		xloopcnt=1
		**for "Availability To Pick Up", need to show totals for all pick ups and then separately those for appt required and no appt required - mvw 01/27/16
		if crptname="Availability To Pick Up"
			xloopcnt=3
			oworkbook.worksheets[1].range("A3") = "Overall"
		endif

		for xloop = 1 to xloopcnt
			xxfilter=cfilter
			do case
			case xloop=2
				xxfilter="where emptynul(appt)"
				xfilterdesc="No Appointment Required"
			case xloop=3
				xxfilter="where !emptynul(appt)"
				xfilterdesc="Appointment Required"
			endcase

			Select &cAcctFlds (week(&cDateField)-week(dstartdt))+1 as week, Month(&cDateField) as month, Year(&cDateField) as year, &cDateField as date, ;
				   sum(&cCntField) as totCnt, sum(Iif(&cDiffField<=0, &cCntField, 0)) as tot0, ;
				   sum(Iif(&cDiffField=1, &cCntField, 0)) as tot1, sum(Iif(&cDiffField=2, &cCntField, 0)) as tot2, ;
				   sum(Iif(&cDiffField=3, &cCntField, 0)) as tot3, sum(Iif(&cDiffField>=4, &cCntField, 0)) as tot4 ;
				   &cAddFields ;
				from csrDetail group by &cAcctGroup &cgroupby order by &cAcctOrder &cgroupby ;
				&xxfilter ;
			  into cursor csrTotals

			if xloop>1
				oworkbook.worksheets[1].range("A"+transform(nrow+2))=xfilterdesc
				oworkbook.worksheets[1].Range("A"+transform(nrow+2)).Font.Bold = .t.
				oworkbook.worksheets[1].Range("A"+transform(nrow+2)).HorizontalAlignment = -4108
				For i = 97 to 109 &&a-m
				  oworkbook.worksheets[1].range(Chr(i)+transform(nrow+2)).borders(9).linestyle = 1
				EndFor
				nrow=nrow+3
				nstartrow=nrow

				if eof()
					crow=transform(nrow)
					oworkbook.worksheets[1].range("A"+crow)="No Data"
					For i = 97 to 109 &&a-m
					  oworkbook.worksheets[1].range(Chr(i)+cRow).borders(8).linestyle = 1
					  oworkbook.worksheets[1].range(Chr(i)+cRow).borders(9).linestyle = 1
					  oworkbook.worksheets[1].range(Chr(i)+cRow).borders(10).linestyle = Iif(!InList(i,99,101,103,105,107), 1, 0)
					EndFor
				endif
			endif

			Scan
				cRow = Alltrim(Str(nRow))
				oworkbook.worksheets[1].range("A"+cRow) = Iif(!lAcctTotals, "", Alltrim(acctNameFld)+" - ")+;
					iif(lweekly,"Week "+transform(week),Cmonth(date)+" '"+Right(Dtoc(date),2))
				oworkbook.worksheets[1].range("B"+cRow) = csrTotals.totCnt
				oworkbook.worksheets[1].range("C"+cRow) = csrTotals.tot0
				oworkbook.worksheets[1].range("D"+cRow) = "=C"+cRow+"/B"+cRow
				oworkbook.worksheets[1].range("E"+cRow) = csrTotals.tot1
				oworkbook.worksheets[1].range("F"+cRow) = "=E"+cRow+"/B"+cRow
				oworkbook.worksheets[1].range("G"+cRow) = csrTotals.tot2
				oworkbook.worksheets[1].range("H"+cRow) = "=G"+cRow+"/B"+cRow
				oworkbook.worksheets[1].range("I"+cRow) = csrTotals.tot3
				oworkbook.worksheets[1].range("J"+cRow) = "=I"+cRow+"/B"+cRow
				oworkbook.worksheets[1].range("K"+cRow) = csrTotals.tot4
				oworkbook.worksheets[1].range("L"+cRow) = "=K"+cRow+"/B"+cRow

				**add reschedule attempts for "Availability To Pick Up" - 01/29/16
				if type("csrtotals.reschedcnt")#"U" and inlist(xloop,1,3)
					oworkbook.worksheets[1].range("M"+cRow) = "Rescheduled Appts: "+transform(csrtotals.reschedcnt)
				else
					oworkbook.worksheets[1].range("M"+cRow) = &cCommentCol
				endif

				oworkbook.worksheets[1].range(Chr(97)+cRow).borders(7).linestyle = 1
				For nCol = 97 to 109 &&a-m
				  oworkbook.worksheets[1].range(Chr(nCol)+cRow).borders(10).linestyle = Iif(!InList(nCol,99,101,103,105,107), 1, 0)
				  oworkbook.worksheets[1].range(Chr(nCol)+cRow).borders(9).linestyle = Iif(Recno()=Reccount(), 1, 0)
				EndFor

				nRow=nRow+1
			EndScan

			If Reccount() > 1
				nRow = Reccount()+nStartRow

				cRow = Alltrim(Str(nRow))
				oworkbook.worksheets[1].range(Chr(97)+cRow).borders(7).linestyle = 1
				For i = 97 to 109 &&a-m
				  oworkbook.worksheets[1].range(Chr(i)+cRow).borders(8).linestyle = 1
				  oworkbook.worksheets[1].range(Chr(i)+cRow).borders(9).linestyle = 1
				  oworkbook.worksheets[1].range(Chr(i)+cRow).borders(10).linestyle = Iif(!InList(i,99,101,103,105,107), 1, 0)
				EndFor

				nRow=nRow+1
				cRow = Alltrim(Str(nRow))
				oworkbook.worksheets[1].range("A"+cRow) = "Total"
				oworkbook.worksheets[1].range("B"+cRow) = "=sum(B"+Alltrim(Str(nStartRow))+":B"+Alltrim(Str(Reccount()+nStartRow-1))+")"
				oworkbook.worksheets[1].range("C"+cRow) = "=sum(C"+Alltrim(Str(nStartRow))+":C"+Alltrim(Str(Reccount()+nStartRow-1))+")"
				oworkbook.worksheets[1].range("D"+cRow) = "=C"+cRow+"/B"+cRow
				oworkbook.worksheets[1].range("E"+cRow) = "=sum(E"+Alltrim(Str(nStartRow))+":E"+Alltrim(Str(Reccount()+nStartRow-1))+")"
				oworkbook.worksheets[1].range("F"+cRow) = "=E"+cRow+"/B"+cRow
				oworkbook.worksheets[1].range("G"+cRow) = "=sum(G"+Alltrim(Str(nStartRow))+":G"+Alltrim(Str(Reccount()+nStartRow-1))+")"
				oworkbook.worksheets[1].range("H"+cRow) = "=G"+cRow+"/B"+cRow
				oworkbook.worksheets[1].range("I"+cRow) = "=sum(I"+Alltrim(Str(nStartRow))+":I"+Alltrim(Str(Reccount()+nStartRow-1))+")"
				oworkbook.worksheets[1].range("J"+cRow) = "=I"+cRow+"/B"+cRow
				oworkbook.worksheets[1].range("K"+cRow) = "=sum(K"+Alltrim(Str(nStartRow))+":K"+Alltrim(Str(Reccount()+nStartRow-1))+")"
				oworkbook.worksheets[1].range("L"+cRow) = "=K"+cRow+"/B"+cRow

	*			oWorkbook.worksheets[1].Range("B"+cRow+":"+"L"+cRow).Interior.Color = RGB(255,255,128)

				oworkbook.worksheets[1].range(Chr(97)+cRow).borders(7).linestyle = 1
				For i = 97 to 109 &&a-m
				  oworkbook.worksheets[1].range(Chr(i)+cRow).borders(8).linestyle = 1
				  oworkbook.worksheets[1].range(Chr(i)+cRow).borders(9).linestyle = 1
				  oworkbook.worksheets[1].range(Chr(i)+cRow).borders(10).linestyle = Iif(!InList(i,99,101,103,105,107), 1, 0)
				EndFor
			EndIf
		endfor

		**need to open totals cursor
		if xloopcnt>1
			Select &cAcctFlds (week(&cDateField)-week(dstartdt))+1 as week, Month(&cDateField) as month, Year(&cDateField) as year, &cDateField as date, ;
				   sum(&cCntField) as totCnt, sum(Iif(&cDiffField<=0, &cCntField, 0)) as tot0, ;
				   sum(Iif(&cDiffField=1, &cCntField, 0)) as tot1, sum(Iif(&cDiffField=2, &cCntField, 0)) as tot2, ;
				   sum(Iif(&cDiffField=3, &cCntField, 0)) as tot3, sum(Iif(&cDiffField>=4, &cCntField, 0)) as tot4 ;
				   &cAddFields ;
				from csrDetail group by &cAcctGroup &cgroupby order by &cAcctOrder &cgroupby ;
				&cfilter ;
			  into cursor csrTotals
		endif

	else &&kpipassfailgeneric.xls
		**pass/fail type - within kpi or outside kpi
		Select &cAcctFlds (week(&cDateField)-week(dstartdt))+1 as week, Month(&cDateField) as month, Year(&cDateField) as year, &cDateField as date, ;
			   sum(&cCntField) as totCnt, sum(Iif(&cDiffField<=kpi, &cCntField, 0)) as totin, ;
			   sum(Iif(&cDiffField>kpi, &cCntField, 0)) as totOUT &cAddFields ;
			from csrDetail group by &cAcctGroup &cgroupby order by &cAcctOrder &cgroupby ;
			&cFilter ;
		  into cursor csrTotals

		Store &cStartRow to nRow, nStartRow
		Scan
			cRow = Alltrim(Str(nRow))
			oworkbook.worksheets[1].range("A"+cRow) = Iif(!lAcctTotals, "", Alltrim(acctNameFld)+" - ")+;
				iif(lweekly,"Week "+transform(week),Cmonth(date)+" '"+Right(Dtoc(date),2))
			oworkbook.worksheets[1].range("B"+cRow) = csrTotals.totCnt
			oworkbook.worksheets[1].range("C"+cRow) = csrTotals.totin
			oworkbook.worksheets[1].range("D"+cRow) = "=C"+cRow+"/B"+cRow
			oworkbook.worksheets[1].range("E"+cRow) = csrTotals.totout
			oworkbook.worksheets[1].range("F"+cRow) = "=E"+cRow+"/B"+cRow
			oworkbook.worksheets[1].range("M"+cRow) = &cCommentCol

			oworkbook.worksheets[1].range(Chr(97)+cRow).borders(7).linestyle = 1
			For nCol = 97 to 103 &&a-g
			  oworkbook.worksheets[1].range(Chr(nCol)+cRow).borders(10).linestyle = Iif(!InList(nCol,99,101,103,105,107), 1, 0)
			  oworkbook.worksheets[1].range(Chr(nCol)+cRow).borders(9).linestyle = Iif(Recno()=Reccount(), 1, 0)
			EndFor

			nRow=nRow+1
		EndScan

		If Reccount() > 1
			nRow = Reccount()+nStartRow

			cRow = Alltrim(Str(nRow))
			oworkbook.worksheets[1].range(Chr(97)+cRow).borders(7).linestyle = 1
			For i = 97 to 103 &&a-g
			  oworkbook.worksheets[1].range(Chr(i)+cRow).borders(8).linestyle = 1
			  oworkbook.worksheets[1].range(Chr(i)+cRow).borders(9).linestyle = 1
			  oworkbook.worksheets[1].range(Chr(i)+cRow).borders(10).linestyle = Iif(!InList(i,99,101,103,105,107), 1, 0)
			EndFor

			nRow=nRow+1
			cRow = Alltrim(Str(nRow))
			oworkbook.worksheets[1].range("A"+cRow) = "Total"
			oworkbook.worksheets[1].range("B"+cRow) = "=sum(B"+Alltrim(Str(nStartRow))+":B"+Alltrim(Str(Reccount()+nStartRow-1))+")"
			oworkbook.worksheets[1].range("C"+cRow) = "=sum(C"+Alltrim(Str(nStartRow))+":C"+Alltrim(Str(Reccount()+nStartRow-1))+")"
			oworkbook.worksheets[1].range("D"+cRow) = "=C"+cRow+"/B"+cRow
			oworkbook.worksheets[1].range("E"+cRow) = "=sum(E"+Alltrim(Str(nStartRow))+":E"+Alltrim(Str(Reccount()+nStartRow-1))+")"
			oworkbook.worksheets[1].range("F"+cRow) = "=E"+cRow+"/B"+cRow

			*oWorkbook.worksheets[1].Range("B"+cRow+":"+"F"+cRow).Interior.Color = RGB(255,255,128)

			oworkbook.worksheets[1].range(Chr(97)+cRow).borders(7).linestyle = 1
			For i = 97 to 103 &&a-g
			  oworkbook.worksheets[1].range(Chr(i)+cRow).borders(8).linestyle = 1
			  oworkbook.worksheets[1].range(Chr(i)+cRow).borders(9).linestyle = 1
			  oworkbook.worksheets[1].range(Chr(i)+cRow).borders(10).linestyle = Iif(!InList(i,99,101,103,105,107), 1, 0)
			EndFor
		EndIf
	endif

	If lNotes
		oworkbook.worksheets[1].range("A"+Alltrim(Str(Reccount()+nStartRow+7)),"A"+Alltrim(Str(Reccount()+nStartRow+10))).font.color = Rgb(255,0,0) &&red
		oworkbook.worksheets[1].range("A"+Alltrim(Str(Reccount()+nStartRow+7))) = "Notes:"
		oworkbook.worksheets[1].range("A"+Alltrim(Str(Reccount()+nStartRow+8))) = " - Totals exclude weekends & holidays."
		oworkbook.worksheets[1].range("A"+Alltrim(Str(Reccount()+nStartRow+9))) = cAddComment
		oworkbook.worksheets[1].range("A"+Alltrim(Str(Reccount()+nStartRow+10))) = cAddComment2
	EndIf

	Select * from csrTotals group by year, month into cursor csrTotals

Return &&End xlsTimeGenSummary