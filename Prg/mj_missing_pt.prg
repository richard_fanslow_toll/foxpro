utilsetup("MJ_MISSING_PT")
Set Exclusive Off
Set Deleted On
Set Talk Off
Set Safety Off 
WITH _SCREEN	
	.AUTOCENTER = .T.
	.WINDOWSTATE = 0
	.BORDERSTYLE = 1
	.WIDTH = 320
	.HEIGHT = 210
	.TOP = 290
	.LEFT = 110
	.CLOSABLE = .T.
	.MAXBUTTON = .T.
	.MINBUTTON = .T.
	.SCROLLBARS = 0
	.CAPTION = "MJ_MISSING_PT"
ENDWITH

	
SET DELETED off
If !Used("ediinlog")
  use F:\edipoller\ediinlog.Dbf In 0
ENDIF

If !Used("pthist")
  Use F:\3PL\data\pthist.Dbf In 0
ENDIF

GOFFICE='J'
xsqlexec("select * from pt where mod='"+GOFFICE+"'",,,"wh")
Index On ptid Tag ptid
Set Order To

*!*	If !Used("ptbak")
*!*	  Use F:\whj\whdata\ptbak.Dbf In 0
*!*	ENDIF

xsqlexec("select * from ptbak where mod='J'","ptbak",,"wh")

xsqlexec("select * from outship where mod='J' and wo_date>{"+dtoc(date())+"}",,,"wh")

****compares pt count from i/b file to pt table
SELECT * FROM ediinlog WHERE fdate>={09/06/2013} and acct_name='MARCJACOBSW' AND type='940' AND qty!=fileqty INTO CURSOR pt_missing
EXPORT TO C:\pt_missing XLS
SELECT pt_missing
If Reccount() > 0
  tsendto = "tmarg@fmiint.com,e.barnes@marcjacobs.com"
  tattach = "C:\pt_missing.XLS"
  tcc =""
  tFrom ="Toll EDI System Operations <fmi-transload-ops@fmiint.com>"
  tmessage = "PTs from I/B did not load completely"
  tSubject = "Missing PTs"
  Do Form m:\dev\frm\dartmail2 With tsendto,tFrom,tSubject, tcc,tattach,tmessage  ,"A"
ELSE 
  tsendto = "tmarg@fmiint.com"
  tattach = ""
  tcc =""
  tFrom ="Toll EDI System Operations <fmi-transload-ops@fmiint.com>"
  tmessage = "NO Missing PTs  "+Ttoc(Datetime())
  tSubject = "NO Missing PTs"
  Do Form m:\dev\frm\dartmail2 With tsendto,tFrom,tSubject, tcc,tattach,tmessage  ,"A"
ENDIF



****checksfor missing PTs in down stream tables
SELECT distinct pt FROM PTHIST WHERE DATELOADED=DATE() AND ACCOUNTID=6303 AND office='J' INTO CURSOR PTHIST11
SELECT * FROM pthist11 LEFT JOIN pt ON pt=ship_ref INTO CURSOR compare1
SELECT distinct pt FROM compare1 WHERE ISNULL(ptid) INTO CURSOR compare2
SELECT * FROM compare2 LEFT JOIN outship ON pt=ship_ref INTO CURSOR compare3
SELECT distinct pt FROM compare3 WHERE ISNULL(ship_ref) INTO CURSOR compare4
SELECT * FROM compare4 LEFT JOIN ptbak ON pt=ship_ref INTO CURSOR compare5
SELECT distinct pt FROM compare5 WHERE ISNULL(ptbakid) INTO CURSOR missing_pt
EXPORT TO C:\pt_missing2 XLS

SELECT missing_pt
If Reccount() > 0
  tsendto = "tmarg@fmiint.com,e.barnes@marcjacobs.com"
  tattach = "C:\pt_missing2.XLS"
  tcc =""
  tFrom ="Toll EDI System Operations <fmi-transload-ops@fmiint.com>"
  tmessage = "Missing PTs exist, see attached"
  tSubject = "Missing PTs exist"
  Do Form m:\dev\frm\dartmail2 With tsendto,tFrom,tSubject, tcc,tattach,tmessage  ,"A"
ELSE 
*!*	  tsendto = "tmarg@fmiint.com"
*!*	  tattach = ""
*!*	  tcc =""
*!*	  tFrom ="Toll EDI System Operations <fmi-transload-ops@fmiint.com>"
*!*	  tmessage = "NO Missing PTs exist  "+Ttoc(Datetime())
*!*	  tSubject = "NO Missing PTs exist"
*!*	  Do Form m:\dev\frm\dartmail2 With tsendto,tFrom,tSubject, tcc,tattach,tmessage  ,"A"
ENDIF



Close Data All
schedupdate()
_Screen.Caption=gscreencaption