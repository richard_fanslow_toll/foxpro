
* IMPORT 2xu STYLES.
* Author: Joe Bianchi?
* 12/18/2017 MB: built EXE as F:\3PL\PROJ\2XU832.EXE to support autoloading the styles. 
* 01/15/2018 MB: Rebuilt the EXE because we were getting unexplainable errors with it running on TASK5.


CLOSE DATA ALL

utilsetup("2XU832")

zuserid=UPPER(GETENV("USERNAME"))
lcpath = 'F:\FTPUSERS\2XU\Stylemaster\'
lcarchivepath = 'F:\FTPUSERS\2XU\Stylemaster\Archive\'

len1 = ADIR(ary1,lcpath+"*.*")
IF len1 = 0
	WAIT WINDOW "No files found...exiting" TIMEOUT 2
	CLOSE DATA ALL
	schedupdate()
	_SCREEN.CAPTION=gscreencaption
	ON ERROR
	RETURN
ENDIF
FOR i = 1 TO len1
	cfilename = ALLTRIM(ary1[i,1])
	xfile = lcpath+cfilename
	*	SET STEP ON
	carchivefile = (lcarchivepath+cfilename)
	cloadfile = (lcpath+cfilename)
	COPY FILE [&cLoadFile] TO [&cArchiveFile]
	*set step on
	tfile=ALLTRIM(ary1[i,1])
	tfilepath=lcpath+ALLTRIM(ary1[i,1])
	*	cfilename="ItemMasterReport.xls"
	*	xfilecsv = juststem(justfname(xfile))+".csv"
	xfilecsv = lcpath+JUSTSTEM(JUSTFNAME(xfile))+".csv"
	DO excel_to_csv WITH cfilename, .T.
	*SET STEP ON
	* reversed style and descript fields to match spreadsheet 12/15/2017 mb
	*CREATE CURSOR t1(upc c(14), DESCRIP c(50), STYLE c(20), COLOR c(10), colordesc c(20),ID c(10))
	CREATE CURSOR t1(upc c(14), STYLE c(20), DESCRIP c(50), COLOR c(10), colordesc c(20),ID c(10))
	SELECT t1
	APPEND FROM [&xfilecsv] TYPE CSV

	archivefile=lcarchivepath+cfilename
	*set step on
	DELETE FILE  [&xfile]
	DELETE FILE  [&xfilecsv]
	DELETE FROM t1 WHERE upc='SKU'

	SELECT DISTINCT upc, UPPER(DESCRIP) AS DESCRIP, UPPER(STYLE) AS STYLE, UPPER(COLOR) AS COLOR, UPPER(colordesc) AS colordesc, UPPER(ID) AS ID FROM t1  INTO CURSOR t2 READWRITE

	useca("upcmast","wh",,,"select * from upcmast where  accountid in (6665)")

	xsqlexec("select * from upcmast where accountid= 6665","xupcmast",,"wh")
	SELECT xupcmast
	INDEX ON upc TAG upc
	SET ORDER TO

	STORE "" TO m.info

	SELECT upcmast
	SCATTER MEMVAR MEMO BLANK

	SELECT t2
	recctr=0
	SCAN
		SCATTER MEMVAR MEMO
		m.info = "COLORDESC*"+ALLT(t2.colordesc)
		recctr = recctr +1
		WAIT WINDOW AT 10,10 "Checking Record # "+TRANSFORM(recctr) NOWAIT

		m.adddt=DATETIME()
		m.updatedt=DATETIME()
		m.addby=zuserid
		m.updateby=zuserid
		m.accountid=6665
		m.addproc='2XU832'
		m.updproc='2XU832'
		m.pnp=.T.

		IF !EMPTY(upc)
			*			xsqlexec("select * from upcmast where accountid= 6665 and upc = '"+alltrim(t1.upc)+"'","xupcmast",,"wh")
			*			if eof("xupcmast")
			IF !SEEK(ALLTRIM(t2.upc),"xupcmast","upc")
				gmasteroffice='Y'
				goffice='Y'
				insertinto("upcmast","wh",.T.)
			ELSE
				SELECT upcmast
				LOCATE FOR ALLTRIM(upc)=ALLTRIM(t2.upc)

				*!*					replace style                with t2.style   in upcmast
				*!*					replace color               with alltrim(t2.color)   in upcmast
				*!*					replace id                      with alltrim(t2.id)   in upcmast
				*!*					replace descrip           with t2.descrip   in upcmast
				*!*					replace info                  with m.info   in upcmast
				*!*					replace updatedt        with datetime()   in upcmast

				IF FOUND() THEN
					REPLACE STYLE WITH t2.STYLE, ;
						COLOR WITH ALLTRIM(t2.COLOR), ;
						ID WITH ALLTRIM(t2.ID), ;
						DESCRIP WITH t2.DESCRIP, ;
						INFO WITH m.info, ;
						updatedt WITH DATETIME() IN upcmast
				ENDIF
			ENDIF
		ENDIF
	ENDSCAN


	*set step on
	tu("upcmast")
	
	WAIT CLEAR

ENDFOR
CLOSE DATABASES ALL
































