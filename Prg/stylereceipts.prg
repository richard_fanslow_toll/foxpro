parameters xaccountid,xstartdt,xenddt,xunits

do case
case xaccountid=9999
	xfilter=".t."
case xaccountid>10000
	xfilter="inlist(accountid,"
	select acctdet
	scan for acctgrpid=xaccountid-10000
		xfilter=xfilter+transform(acctdet.accountid)+","
	endscan
	xfilter=left(xfilter,len(xfilter)-1)+")"
otherwise
	xfilter="accountid="+trans(xaccountid)
endcase

if xunits
	xunitfilter="units=.t."
else
	xunitfilter="units=.f."
endif

xsqlexec("select * from inwolog where between(wo_date,{"+dtoc(xstartdt)+"},{"+dtoc(xenddt)+"}) and "+xfilter,,,"wh")

xsqlexec("select * from indet where .f.",,,"wh")
select space(30) as acctname, style, color, id, pack, totqty from indet where .f. into cursor xrpt readwrite
index on acctname+style+color+id tag zorder

select inwolog
scan
	xsqlexec("select * from indet where inwologid="+transform(inwolog.inwologid)+" and "+xunitfilter,,,"wh")
	scan 
		xacctname=padr(acctname(inwolog.accountid),30)
		if !seek(xacctname+style+color+id,"xrpt","zorder")
			insert into xrpt (acctname, style, color, id, pack) values (xacctname, indet.style, indet.color, indet.id, indet.pack)
		endif
		replace xrpt.totqty with (xrpt.totqty + indet.totqty) in xrpt
	endscan
endscan

select xrpt
