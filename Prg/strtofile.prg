PARAMETER lcFStr,lcFileName
LOCAL lnFSize
SET COMPATIBLE ON

IF TYPE("lcFileName") $ "UL"
	STORE "" TO lcFileName
ENDIF

gnFileHandle = FCREATE(lcFileName)

IF gnFileHandle <= 0
	=MESSAGEBOX("Cant open file "+lcFileName,48,"Str to File")
	RETURN
ENDIF
lcFSize = FWRITE(gnFileHandle,lcFStr)
FCLOSE(gnFileHandle)
