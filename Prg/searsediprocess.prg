lEmail = .F.
lNewPT = .T.

lcPath        ="f:\ftpusers\searsedi\"
cFilemask     = "f:\ftpusers\searsedi\*.txt"
lcArchivePath = "f:\ftpusers\searsedi\archive\"

lnNum = Adir(tarray,cFilemask)

If lnNum = 0
  Wait Window At 10,20 "No Sears EDI files to Process......." Timeout 1
  NormalExit = .T.
  Throw
Endif

Assert .F. Message "At process looping"
For thisfile = 1  To XlnNum
  lLoop = .F.
  cFilename = Alltrim(tarray[thisfile,1])
  xfile = lcPath+cFilename
  archivefile = lcArchivePath+cFilename

  Wait Window "Importing file: "+xfile Timeout 2
  If File(xfile)
    Do createx856a
    Do loadedifile With xfile,Chr(07),"SEARSEDI","SEARSEDI"
    Select x856
    Locate
    Browse
    Do SearsEDIBkdn
    Copy File [&xfile] To [&archivefile]

    If !lTesting
      Select ediinlog
      Append Blank
      Replace ediinlog.acct_name  With cCustname In ediinlog
      Replace ediinlog.office     With cOffice In ediinlog
      Replace ediinlog.filename   With tarray(thisfile,1) In ediinlog
      Replace ediinlog.Size       With tarray(thisfile,2) In ediinlog
      Replace ediinlog.Fdate      With tarray(thisfile,3) In ediinlog
      Replace ediinlog.Ftime      With tarray(thisfile,4) In ediinlog
      Replace ediinlog.Type       With "940" In ediinlog
      Replace ediinlog.uploadtime With Datetime() In ediinlog
      Replace ediinlog.qty        With nPTQty In ediinlog
      LogCommentStr = "Pick tickets uploaded: "+Alltrim(cPickticket_num_start)+" - "+Alltrim(cPickticket_num_end)
      Replace ediinlog.comments   With LogCommentStr In ediinlog
    Endif

    If File(archivefile)
      If File(xfile)
        Delete File &xfile
      Endif
    Endif
  Endif
Next thisfile

Wait Window "ALL "+cMailName+" 940 files finished processing for: "+cOfficename+"..." Timeout 3

*************************************************************************************************
