* Calc avg Temp headcount for a month. To be run early each month against prior month.
* EXEs go in F:\UTIL\KRONOS\

* Version modified to get data for Mike Piacentine.

LOCAL loKronosTempHeadcountReport

loKronosTempHeadcountReport = CREATEOBJECT('KronosTempHeadcountReport')
loKronosTempHeadcountReport.MAIN()
CLOSE DATABASES ALL
RETURN

#DEFINE LOGIT 1
#DEFINE WAITIT 2
#DEFINE NOWAITIT 4
#DEFINE SENDIT 8
#DEFINE CRLF CHR(13) + CHR(10)
#DEFINE LF CHR(10)
#DEFINE RETURN_DATA_MANDATORY .F.
#DEFINE RETURN_DATA_NOT_MANDATORY .T.
#DEFINE DAILY_OT_MULTIPLIER 1.5
#DEFINE DAILY_OT20_MULTIPLIER 2.0
#DEFINE xlEdgeBottom 9
#DEFINE xlContinuous 1
#DEFINE xlMedium -4138

DEFINE CLASS KronosTempHeadcountReport AS CUSTOM

	cProcessName = 'KronosTempHeadcountReport'

	lTestMode = .T.  && .F. FOR NORMAL OPERATION, .T. TO SUPPRESS ALL BUT DEVELOPER EMAILS AND CHANGE INPUT TABLE, ETC.

	lAutoYield = .T.

	cStartTime = TTOC(DATETIME())
	
	* processing properties
	nMinTempsInDayToProcess = 100
	
	* connection properties
	nSQLHandle = 0

	* file properties
	nFileHandle = 0

	* date properties
	dtNow = DATETIME()

	dToday = DATE()
	* to force different pay weeks
	*dToday = {^2010-05-02}

	cToday = DTOC(DATE())
	
	* wait window properties
	nWaitWindowTimeout = 2
	lWaitWindowIsOn = .T.

	* logfile properties
	lLoggingIsOn = .T.
	cLogFile = 'F:\UTIL\KRONOS\Logfiles\KronosTempHeadcountReport_log.txt'

	* INTERNAL email properties
	lSendInternalEmailIsOn = .T.
	cFrom = 'FMI Corporate <fmicorporate@fmiint.com>'
	cSendTo = 'mbennett@fmiint.com'
	cCC = ''
	cSubject = ''
	cAttach = ''
	cBodyText = ''

	cCOMPUTERNAME = ''
	cUSERNAME = ''

	FUNCTION INIT
		IF NOT DODEFAULT()
			RETURN .F.
		ENDIF
		WITH THIS
			CLEAR
			*SET RESOURCE OFF
			CLOSE DATA
			SET CENTURY ON
			SET DATE AMERICAN
			SET DECIMALS TO 1
			SET HOURS TO 24
			SET ANSI ON
			SET TALK OFF
			SET DELETED ON
			SET CONSOLE OFF
			SET EXCLUSIVE OFF
			SET SAFETY OFF
			SET EXACT OFF
			SET STATUS BAR ON
			SET SYSMENU OFF
			SET ENGINEBEHAVIOR 70
			_VFP.AUTOYIELD = .lAutoYield
			.cCOMPUTERNAME = GETENV("COMPUTERNAME")
			.cUSERNAME = GETENV("USERNAME")
			IF .lTestMode THEN
				.cSendTo = 'mbennett@fmiint.com'
				.cLogFile = 'F:\UTIL\KRONOS\Logfiles\KronosTempHeadcountReport_log_TESTMODE.txt'
			ENDIF
			.lLoggingIsOn = .lLoggingIsOn AND NOT EMPTY(.cLogFile)
			IF .lLoggingIsOn THEN
				SET ALTERNATE TO (.cLogFile) ADDITIVE
				SET ALTERNATE ON
			ENDIF
		ENDWITH
	ENDFUNC


	FUNCTION DESTROY
		WITH THIS
			IF .lLoggingIsOn  THEN
				SET ALTERNATE OFF
				SET ALTERNATE TO
			ENDIF
		ENDWITH
		DODEFAULT()
	ENDFUNC


	FUNCTION MAIN
		WITH THIS
			LOCAL lnNumberOfErrors, lcSQL, ldToday, ldFromDate, ldToDate, lcSQLFromDate, lcSQLToDate
			LOCAL lcMonthYear, lcOutputFile,lnDays, lnHeadcount, lcWorksiteWhere, lcWorksiteName

			TRY
				lnNumberOfErrors = 0

				*.TrackProgress('==================================================================================================================', LOGIT+SENDIT)
				.TrackProgress('KRONOS Temp Headcount Report process started....', LOGIT+SENDIT+NOWAITIT)
				.TrackProgress('COMPUTERNAME = ' + .cCOMPUTERNAME, LOGIT+SENDIT)
				.TrackProgress('USERNAME = ' + .cUSERNAME, LOGIT+SENDIT)
				.TrackProgress('PROJECT = KRONOSTEMPHEADCOUNT', LOGIT+SENDIT)
				
				IF .lTestMode THEN
					.TrackProgress('                        TEST MODE                        ', LOGIT+SENDIT)
				ENDIF
						

				ldToday = .dToday
				ldToDate = ldToday - DAY(ldToday)  && this is last day of prior month
				ldFromDate = GOMONTH(ldToDate + 1,-1)
				
				
				********************************
				* to force a date range
				ldFromDate = {^2012-08-01}
				ldToDate = {^2013-07-31}
				********************************
				
				lcMonthYear = PROPER(CMONTH(ldFromDate)) + " " + TRANSFORM(YEAR(ldFromDate))

				.cSubject = 'Kronos Temp Headcount Report for: ' + lcMonthYear
*!*					lcOutputFile = "F:\UTIL\KRONOS\TEMPTIMEREPORTS\Temp Headcount Report for " + lcMonthYear + ".XLS"
				
				lcSQLFromDate = .GetSQLStartDateTimeString( ldFromDate )
				lcSQLToDate = .GetSQLEndDateTimeString( ldToDate )	
				
				IF USED('CURTEMPS') THEN
					USE IN CURTEMPS
				ENDIF

				IF USED('CURTEMPSUM') THEN
					USE IN CURTEMPSUM
				ENDIF
								
*!*					lcWorksiteWhere = " AND (D.LABORLEV4NM IN ('CA2','CA4','SP2','NJ1') ) "
*!*					lcWorksiteName = "All"

*!*					lcWorksiteWhere = " AND (D.LABORLEV4NM = 'CA2' ) "
*!*					lcWorksiteName = "Rialto"

*!*					lcWorksiteWhere = " AND (D.LABORLEV4NM = 'CA4' ) "
*!*					lcWorksiteName = "Mira Loma"

*!*					lcWorksiteWhere = " AND (D.LABORLEV4NM = 'SP2' ) "
*!*					lcWorksiteName = "San Pedro"

				lcWorksiteWhere = " AND (D.LABORLEV4NM = 'NJ1' ) "
				lcWorksiteName = "Carteret"

				lcOutputFile = "C:\A\MIKEPIACENTINE\TempHeadcounts" + lcWorksiteName + ".XLS"

			SET TEXTMERGE ON
			TEXT TO	lcSQL NOSHOW
			
SELECT  
C.FULLNM AS EMPLOYEE,  
(A.DURATIONSECSQTY / 3600.00) AS HOURS,
A.APPLYDTM  
FROM WFCTOTAL A  
JOIN WTKEMPLOYEE B  
ON B.EMPLOYEEID = A.EMPLOYEEID  
JOIN PERSON C  
ON C.PERSONID = B.PERSONID  
JOIN LABORACCT D  
ON D.LABORACCTID = A.LABORACCTID  
WHERE (D.LABORLEV1NM = 'TMP')  
AND (A.APPLYDTM >= <<lcSQLFromDate>>) 
AND (A.APPLYDTM <= <<lcSQLToDate>>) 
<<lcWorksiteWhere>>
ORDER BY 1, 2 
				
			ENDTEXT
			SET TEXTMERGE OFF
					
				IF .lTestMode THEN
					.TrackProgress('', LOGIT+SENDIT)
					.TrackProgress('lcSQL =' + lcSQL, LOGIT+SENDIT)
				ENDIF

				.nSQLHandle = SQLCONNECT("WTK2","sa","kr0n1tes")

				IF .nSQLHandle > 0 THEN

					IF .ExecSQL(lcSQL, 'CURTEMPS', RETURN_DATA_MANDATORY) THEN	

						SELECT CURTEMPS
						GOTO TOP
						IF EOF() THEN
							.TrackProgress("There was no data to report!", LOGIT+SENDIT)
						ELSE
						
							* summarize by day
							SELECT PADR(lcWorksiteName,10,' ') AS WORKSITE, ;
								SPACE(10) AS DAY, ;
								TTOD(APPLYDTM) AS DATE, ;
								COUNT(DISTINCT EMPLOYEE) AS HEADCOUNT, ;
								SUM(HOURS) AS TOTHOURS ;
								FROM CURTEMPS ;
								INTO CURSOR CURTEMPSUM ;
								GROUP BY 1,2, 3 ;
								ORDER BY 3 ;
								READWRITE
							
							SELECT CURTEMPSUM
							REPLACE ALL DAY WITH CDOW(DATE)
														
							lnDays = 0
							lnHeadcount = 0
							
							SELECT CURTEMPSUM
							SCAN FOR (HEADCOUNT >= .nMinTempsInDayToProcess)
								lnDays = lnDays + 1
								lnHeadcount = lnHeadcount + CURTEMPSUM.HEADCOUNT
							ENDSCAN
							
							.TrackProgress('Total # of Working Days (with headcount >= ' + TRANSFORM(.nMinTempsInDayToProcess) + ') = ' + TRANSFORM(lnDays), LOGIT+SENDIT)
							.TrackProgress('Total # of Temps working on those days = ' + TRANSFORM(lnHeadcount), LOGIT+SENDIT)
							.TrackProgress('Avg # of Temps working = ' + TRANSFORM(lnHeadcount/lnDays,'9999.9'), LOGIT+SENDIT)							
							
							SELECT CURTEMPSUM
							COPY TO (lcOutputFile) XL5

							IF FILE(lcOutputFile) THEN
								.TrackProgress('Output to spreadsheet: ' + lcOutputFile, LOGIT+SENDIT)
								* attach output file to email
								.cAttach = lcOutputFile
							ELSE
								.TrackProgress('ERROR creating spreadsheet: ' + lcOutputFile, LOGIT+SENDIT)
							ENDIF

						ENDIF  && EOF() CURTEMPS

					ENDIF  &&  .ExecSQL(lcSQL, 'CURWTKEMPLOYEE', RETURN_DATA_MANDATORY)

					CLOSE DATABASES ALL


				ELSE
					* connection error
					.TrackProgress('Unable to connect to WTK System.', LOGIT+SENDIT)
				ENDIF   &&  .nSQLHandle > 0
				
			CATCH TO loError

				.TrackProgress('There was an error.',LOGIT+SENDIT)
				.TrackProgress('The Error # is: ' + TRANSFORM(loError.ERRORNO), LOGIT+SENDIT)
				.TrackProgress('The Error Message is: ' + TRANSFORM(loError.MESSAGE), LOGIT+SENDIT)
				.TrackProgress('The Error occurred in Program: ' + TRANSFORM(loError.PROCEDURE), LOGIT+SENDIT)
				.TrackProgress('The Error occurred at line #: ' + TRANSFORM(loError.LINENO), LOGIT+SENDIT)
				lnNumberOfErrors = lnNumberOfErrors + 1
				IF TYPE('oExcel') = 'O' AND NOT ISNULL(oExcel) THEN
					oExcel.QUIT()
				ENDIF
				CLOSE DATA
				CLOSE ALL

			ENDTRY

			CLOSE DATA
			WAIT CLEAR
			***************** INTERNAL email results ******************************
			.TrackProgress('About to send status email.',LOGIT)
			.TrackProgress('==================================================================================================================', SENDIT)
			.TrackProgress('The logfile is: ' + .cLogFile, SENDIT)
			.TrackProgress('KRONOS Temp Headcount Report process started: ' + .cStartTime, LOGIT+SENDIT)
			.TrackProgress('KRONOS Temp Headcount Report process finished: ' + TTOC(DATETIME()), LOGIT+SENDIT)

			IF .lSendInternalEmailIsOn THEN
				* try to trap error from not having dartmail dll's registered on user's pc...
				TRY
					DO FORM dartmail2 WITH .cSendTo,.cFrom,.cSubject,.cCC,.cAttach,.cBodyText,"A"
					.TrackProgress('Sent status email.',LOGIT)
				CATCH TO loError
					*=MESSAGEBOX('There was an error sending the status email. Please contact Mark Bennett.',0+64,"KRONOS Temp Time")
					.TrackProgress('There was an error sending the status email.',LOGIT)
					.TrackProgress('The Error # is: ' + TRANSFORM(loError.ERRORNO), LOGIT)
					.TrackProgress('The Error Message is: ' + TRANSFORM(loError.MESSAGE), LOGIT)
					.TrackProgress('The Error occurred in Program: ' + TRANSFORM(loError.PROCEDURE), LOGIT)
					.TrackProgress('The Error occurred at line #: ' + TRANSFORM(loError.LINENO), LOGIT)
					lnNumberOfErrors = lnNumberOfErrors + 1
					CLOSE DATA
				ENDTRY

			ELSE
				.TrackProgress('Did not send status email: THIS.lSendInternalEmailIsOn = FALSE.',LOGIT)
			ENDIF

		ENDWITH
		RETURN
	ENDFUNC && main
	

	FUNCTION GetSQLStartDateTimeString
		* returns a timestamp string for use in SQL queries
		LPARAMETERS tdDate
		LOCAL lcSetDate, lcSQLDateTimeString
		lcSetDate = SET('DATE')
		SET DATE YMD
		lcSQLDateTimeString   = "{ts '" + TRANSFORM(YEAR(tdDate)) + "-" + PADL(MONTH(tdDate),2,"0") + "-" + PADL(DAY(tdDate),2,"0") + " 00:00:00.0'}"
		SET DATE &lcSetDate.
		RETURN lcSQLDateTimeString
	ENDFUNC
	

	FUNCTION GetSQLEndDateTimeString
		* returns a timestamp string for use in SQL queries
		LPARAMETERS tdDate
		LOCAL lcSetDate, lcSQLDateTimeString
		lcSetDate = SET('DATE')
		SET DATE YMD
		lcSQLDateTimeString = "{ts '" + TRANSFORM(YEAR(tdDate)) + "-" + PADL(MONTH(tdDate),2,"0") + "-" + PADL(DAY(tdDate),2,"0") + " 23:59:59.9'}"
		SET DATE &lcSetDate.
		RETURN lcSQLDateTimeString
	ENDFUNC
	

	FUNCTION ExecSQL
		LPARAMETERS tcSQL, tcCursorName, tlNoDataReturnedIsOkay
		LOCAL llRetval, lnResult
		WITH THIS
			* close target cursor if it's open
			IF USED(tcCursorName)
				USE IN (tcCursorName)
			ENDIF
			WAIT WINDOW "Fetching remote data; please wait..." NOWAIT
			lnResult = SQLEXEC(.nSQLHandle, tcSQL, tcCursorName)
			llRetval = ( lnResult > 0 )
			IF llRetval THEN
				* see if any data came back
				IF NOT tlNoDataReturnedIsOkay THEN
					IF (NOT USED(tcCursorName)) OR EOF(tcCursorName) THEN
						llRetval = .F.
						.TrackProgress('No data was returned by this query: ' +CRLF + tcSQL, LOGIT+SENDIT)
					ENDIF
				ENDIF
			ELSE
				.TrackProgress("Result Code [" + TRANSFORM(lnResult) + "] returned by this query: " + tcSQL, LOGIT+SENDIT)
			ENDIF
			WAIT CLEAR
			RETURN llRetval
		ENDWITH
	ENDFUNC


	PROCEDURE TrackProgress
		* do any combination of Wait Window, writing to logfile, and adding to body of email,
		* based on nFlags parameter.
		LPARAMETERS tcExpression, tnFlags
		WITH THIS
			IF BITAND(tnFlags,LOGIT) = LOGIT THEN
				IF .lLoggingIsOn THEN
					?
					? .cProcessName + "  " + TTOC(DATETIME()) + ": " + tcExpression
				ENDIF
			ENDIF
			IF .lWaitWindowIsOn THEN
				IF BITAND(tnFlags,NOWAITIT) = NOWAITIT THEN
					WAIT WINDOW tcExpression NOWAIT
				ENDIF
				IF BITAND(tnFlags,WAITIT) = WAITIT THEN
					WAIT WINDOW tcExpression TIMEOUT .nWaitWindowTimeout
				ENDIF
			ENDIF
			IF BITAND(tnFlags,SENDIT) = SENDIT THEN
				IF .lSendInternalEmailIsOn THEN
					.cBodyText = .cBodyText + tcExpression + CRLF + CRLF
				ENDIF
			ENDIF
		ENDWITH
	ENDPROC  &&  TrackProgress

ENDDEFINE
