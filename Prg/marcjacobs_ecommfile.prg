*!* Marc Jacobs Retail ECOMM "Outslip" Creation Program
PARAMETER cBOL,cOffice

PUBLIC tfrom,cMailname,cCustName,cCustPrefix,lTesting,cFin,dt1,lDoMail,cArchiveFile,lcShipDate,NormalExit,nBOL
PUBLIC cWO_Num,cErrMsg,tsendto,tcc,lClose,cAddMsg,lIsError,cShipmentID,lTestOutput,cInclude
PUBLIC tsendtoerr,tccerr,cProgname,cDellocID
cProcName = "marcjacobs_ecommfile"
lnHandle2 = 0

tTrigtime = DATETIME()
cArchiveFile = ""
cInclude = "office = cOffice"

nAcctNum = 6305
cOffice = IIF(coffice = "J","N",cOffice)
cMod = IIF(cOffice = "N","J",cOffice)
lTesting = .F.

DO m:\dev\prg\_setvars WITH lTesting
cProgname = "mjretailoutslip_create"
ON ERROR DEBUG
SET ESCAPE ON
ON ESCAPE CANCEL
NormalExit = .T.

*Assert .F. Message "At start of MJ Retail Outslip program"

TRY
	lTestmail = lTesting
	lTestOutput = .T.  && Leave as .t. until verification is ok
	lTestCounter = lTesting
	lDoMail = .T.
	lPassFile = !lTesting && If .f., files don't get dropped into OUT folder
	tattach = ""
	IF lTesting OR lTestmail
		CLOSE DATABASES ALL
	ENDIF
	SELECT 0
	USE F:\3pl\DATA\mailmaster ALIAS mm
	IF lTesting OR lTestmail
		LOCATE FOR mm.edi_type = "MISC" AND mm.taskname = "GENERAL"
		lUseAlt = mm.use_alt
		tsendto = IIF(lUseAlt,sendtoalt,sendto)
		tcc = IIF(lUseAlt,ccalt,cc)
		tsendtoerr = tsendto
		tccerr = tcc
		SET STATUS BAR ON
	ELSE
		cOffice = IIF(INLIST(cOffice,"1","2","5","6"),"C",cOffice)
		LOCATE FOR mm.edi_type = '945' AND mm.accountid = 6304 AND mm.office = cOffice
		lUseAlt = mm.use_alt
		tsendto = IIF(lUseAlt,sendtoalt,sendto)
		tcc = IIF(lUseAlt,ccalt,cc)
		LOCATE FOR mm.office = "X" AND mm.accountid = 9999
		lUseAlt = mm.use_alt
		tsendtoerr = IIF(lUseAlt,sendtoalt,sendto)
		tccerr = IIF(lUseAlt,ccalt,cc)
	ENDIF
	USE IN mm

	IF VARTYPE(cBOL) = "L"
		IF lTesting
			CLOSE DATABASES ALL
			WAIT WINDOW "This is a TEST process" TIMEOUT 2
			cBOL = "04907316304100001"
			l945M = .F.
			tTrigtime = DATETIME()
		ELSE
			DO ediupdate WITH .T.,"BAD PARAM TYPE"
			THROW
		ENDIF
	ENDIF
	SET CENTURY ON
	SET DATE YMD

	cBOL = ALLTRIM(cBOL)

	lClose = .F.
	tfrom = "TGF EDI Operations <fmi-edi-ops@fmiint.com>"
	cMailname = "Marc Jacobs"
	cCustName = "MARCJACOBSR"
	cCustPrefix = "MJR"
	dt1 = TTOC(DATETIME(),1)
	lIsError = .F.
	cErrMsg = ""
	cAddMsg = ""
	closedata()

	IF lTesting
		cUseFolder = "F:\WHP\WHDATA\"
	ELSE
		xReturn = "XX"
		DO m:\dev\prg\wf_alt WITH cMod,nAcctNum
		cUseFolder = UPPER(xReturn)
	ENDIF
	upcmastsql(6303)

	lnSlipAccount = 0
	
	IF xsqlexec("select * from outship where mod='"+cMod+"' and bol_no='"+cBOL+"'",,,"wh") = 0
		WAIT WINDOW AT 10,10 "BOL not found in OUTSHIP:--> "+cBOL TIMEOUT 2
		DO ediupdate WITH .T.,"WO# "+cBOL+" NOT IN OUTSHIP"
		THROW
	ELSE
		lnSlipAccount = outship.accountid
		nAcctNum =  outship.accountid
	ENDIF

	selectoutdet()
	
	SELECT outship
	nWO_Num = outship.wo_num

	cFilename = "eircdt"+STRTRAN(STRTRAN(STRTRAN(TTOC(DATETIME()),"/",""),":","")," ","")+".rdl"
	cFilename = STRTRAN(cFilename,"pm","")
	cFilename = STRTRAN(cFilename,"am","")
	cFilename = STRTRAN(cFilename,"PM","")
	cFilename = STRTRAN(cFilename,"AM","")

	cFin = "File "+cFilename+" had errors"  && Default value, will correct upon clean file creation
	cFilenameOut = ("F:\FTPUSERS\MJ_RETAIL\WMS_ECOM_OUTBOUND\ASN\"+cFilename)
	cArchiveFile = ("F:\FTPUSERS\MJ_RETAIL\WMS_ECOM_OUTBOUND\ASN\ARCHIVE\"+cFilename)
	cFilenameHold = ("F:\FTPUSERS\MJ_RETAIL\WMS_ECOM_OUTBOUND\ASN\STAGING\"+cFilename)
	lnHandle2=FCREATE(cFilenameHold)

	USE F:\WH\mjecomm IN 0
	SELECT mjecomm
	LOCATE FOR adddt = DATE()
	IF !FOUND()
		INSERT INTO mjecomm (bol,adddt,counter) VALUES (cBOL,DATE(),3)
	ELSE
		fcounter = mjecomm.counter+1
		REPLACE mjecomm.counter WITH fcounter
	ENDIF
	USE IN mjecomm

*  fcounter = 3

	lDoHeader = .T.

	nSlipnum = 1
	nLineNum = 1

***********  main scan loop for the ship creation  ******************************************************8
	linectr=1
	scanfor = "outship.bol_no = cBOL And outship.qty>0 "
	SELECT outship

*	SET STEP ON 
	SCAN FOR &scanfor

		nLineNum = 1
		alength = ALINES(apt,outship.shipins,.T.,CHR(13))
		cShip_ref = ALLTRIM(outship.ship_ref)
		IF " OV"$cShip_ref
			LOOP
		ENDIF
		dDel_date = outship.del_date
		cDCNum = ALLTRIM(outship.dcnum)

		lcFileDate = TRANSFORM(YEAR(outship.delenterdt))+"-"+PADL(TRANSFORM(MONTH(outship.delenterdt)),2,"0")+"-";
			+PADL(TRANSFORM(DAY(outship.delenterdt)),2,"0")+"-"+TRANSFORM(HOUR(outship.delenterdt))+".30.03"

		ldYesterday = outship.del_date-1
		ldPlus      = outship.del_date+3
		lcToday = outship.del_date
		lcYesterday =  SUBSTR(TRANSFORM(YEAR(ldYesterday)),3,2)+PADL(TRANSFORM(MONTH(ldYesterday)),2,"0")+PADL(TRANSFORM(DAY(ldYesterday)),2,"0")
		lcPlus4     =  SUBSTR(TRANSFORM(YEAR(ldPlus)),3,2)+PADL(TRANSFORM(MONTH(ldPlus)),2,"0")+PADL(TRANSFORM(DAY(ldPlus)),2,"0")

		lcOrigDetail = "797|P|PONUM|TODAY|PLUS4|TOLL GLOBAL FORWARDING|UPC|QTY||||||FILEDATE|224"

		SELECT outdet
		SCAN FOR outdet.outshipid = outship.outshipid AND units AND outdet.totqty >0
			linectr = linectr+1
			lcLineDetail = lcOrigDetail
			lcLineDetail = STRTRAN(lcLineDetail,"PONUM",RIGHT(ALLTRIM(outship.bol_no),7))
			lcLineDetail = STRTRAN(lcLineDetail,"TODAY",lcYesterday)
			lcLineDetail = STRTRAN(lcLineDetail,"PLUS4",lcPlus4)
			lcLineDetail = STRTRAN(lcLineDetail,"UPC",ALLTRIM(outdet.upc))
			lcLineDetail = STRTRAN(lcLineDetail,"QTY",ALLTRIM(TRANSFORM(outdet.totqty)))
			lcLineDetail = STRTRAN(lcLineDetail,"FILEDATE",ALLTRIM(lcFileDate))
			NumWrite = FPUTS(lnHandle2,lcLineDetail)
		ENDSCAN

	ENDSCAN
* write out the XML footer elements
	lcFooter = "TRAILER|LINECOUNT|FDATE||||||||||||||"
	lcFooter = STRTRAN(lcFooter,"LINECOUNT",ALLTRIM(TRANSFORM(linectr)))
	xxfdate = SUBSTR(ALLTRIM(lcFileDate),1,10)
	xxfdate = STRTRAN(xxfdate,"-","")
	xxfdate =SUBSTR(xxfdate,3,6)
	xxfdate = ALLTRIM(xxfdate)+PADL(ALLTRIM(TRANSFORM(fcounter)),3,"0")
	lcFooter = STRTRAN(lcFooter,"FDATE",xxfdate)

	NumWrite = FPUTS(lnHandle2,lcFooter)
	RELEASE lcFooter

	IF lnHandle2 > 0
		FCLOSE(lnHandle2)
	ENDIF

	INSERT INTO F:\WH\MJFILES (TYPE,voucher,pickticket,bol,filename,WHEN) VALUES ("ECOMM","GSI-FILE","",cBOL,cFilename,DATETIME())
	IF NormalExit = .F.
		WAIT "MJR Outslip Creation error file deleted:"+CHR(13)+cFilename WINDOW AT 45,60 TIMEOUT 1
		DELETE FILE &cFilenameHold
	ELSE
		WAIT "MJR Outslip Creation process complete:"+CHR(13)+cFilename WINDOW AT 45,60 TIMEOUT 1
		IF FILE(cFilenameHold)
			mailproc()
			COPY FILE &cFilenameHold TO &cFilenameOut
			COPY FILE &cFilenameHold TO &cArchiveFile
			ERASE &cFilenameHold
		ENDIF
	ENDIF

	WAIT CLEAR


*  If !lTesting &&And lnSlipAccount = 6304  && just move over the Retail Slips
	IF !USED("ftpedilog")
		SELECT 0
		USE F:\edirouting\ftpedilog ALIAS ftpedilog
		INSERT INTO ftpedilog (ftpdate,filename,acct_name,TYPE) VALUES (DATETIME(),cFilename,cCustName,"945")
		USE IN ftpedilog
	ENDIF

	IF !USED("edi_trigger")
		USE F:\3pl\DATA\edi_trigger IN 0
	ENDIF
	SELECT edi_trigger
	LOCATE
	REPLACE edi_trigger.processed WITH .T.,edi_trigger.created WITH .T.,edi_trigger.when_proc WITH DATETIME(),;
		edi_trigger.fin_status WITH "GSI FILE-CREATED",edi_trigger.errorflag WITH .F.,file945 WITH cFilename ;
		FOR edi_trigger.bol = cBOL AND edi_trigger.accountid = nAcctNum AND edi_trigger.edi_type = "945"
	LOCATE
*  Endif

	IF NormalExit = .T.

	ENDIF


CATCH TO oErr
	IF !NormalExit
	SET STEP ON 
		tsendto  = "pgaidis@fmiint.com"
		tcc = "joe.bianchi@tollgroup.com"
		tmessage = cCustName+" Error processing "+CHR(13)
		tmessage = tmessage+TRIM(PROGRAM())+CHR(13)

		tmessage =tmessage+ "Try/CATCH Exeception Message:"+CHR(13)+;
			[  Error: ] + STR(oErr.ERRORNO) +CHR(13)+;
			[  LineNo: ] + STR(oErr.LINENO) +CHR(13)+;
			[  Message: ] + oErr.MESSAGE +CHR(13)+;
			[  Procedure: ] + oErr.PROCEDURE +CHR(13)+;
			[  Details: ] + oErr.DETAILS +CHR(13)+;
			[  StackLevel: ] + STR(oErr.STACKLEVEL) +CHR(13)+;
			[  LineContents: ] + oErr.LINECONTENTS+CHR(13)+;
			[  UserValue: ] + oErr.USERVALUE
		tmessage = tmessage+CHR(13)+cProgname
		tmessage = tmessage+CHR(13)+"Error Desc: "+cErrMsg
		tsubject = "Marc Jacobs Retail Outslip Creation Error at "+TTOC(DATETIME())
		tattach  = ""
		tfrom    ="Toll EDI Outslip Poller Operations <fmi-transload-ops@fmiint.com>"
		DO FORM m:\dev\frm\dartmail2 WITH tsendto,tfrom,tsubject,tcc,tattach,tmessage,"A"
	ENDIF

FINALLY
	tmessage = cCustName+" Error processing "+CHR(13)
	tmessage = tmessage+TRIM(PROGRAM())+CHR(13)
	SET DATE AMERICAN
	closedata()
ENDTRY

****************************
PROCEDURE ediupdate
****************************
	PARAMETERS lIsError,cFin,lDiffMail
*  ASSERT .F. MESSAGE "In EDIUPDATE section"
*  SET STEP ON
	lNormalExit = .T.

	IF !lTesting
		IF !USED('edi_trigger')
			USE F:\3pl\DATA\edi_trigger IN 0 ALIAS edi_trigger
		ENDIF
		SELECT edi_trigger
		LOCATE

		IF lIsError  && Replaces unprocessed POs in BOL with error info.
			REPLACE processed WITH .T.,fin_status WITH cFin,errorflag WITH .T.,created WITH .F. ;
				FOR bol = cBOL AND edi_trigger.accountid = nAcctNum AND edi_type = "945F" AND !processed

			BLANK FIELDS edi_trigger.file945 FOR bol = cBOL AND edi_type = "945F" ;
				AND edi_trigger.accountid = nAcctNum AND !processed
			errormail(cFin)
		ELSE
			REPLACE processed WITH .T.,fin_status WITH cFin,when_proc WITH DATETIME(),errorflag WITH .F.,;
				created WITH .T.,file945 WITH cArchiveFile,trig_by WITH "PROC945",comments WITH "FILE945ORIG*"+cArchiveFile ;
				FOR bol = cBOL AND edi_type = "945F" AND edi_trigger.accountid = nAcctNum AND !processed AND &cInclude
		ENDIF
	ENDIF

	LOCATE
	IF lClose OR lIsError
		closedata()
	ENDIF
ENDPROC

*******************
PROCEDURE mailproc
*******************
	tattach = ""
	IF lTesting
		tsubject = cMailname+" *TEST* Outslip Created at "+TTOC(DATETIME())
	ELSE
		tsubject = cMailname+" GSI-File Created at "+TTOC(DATETIME())
	ENDIF
	tmessage = "MJ(R) GSI FILE:"+CHR(13)+cFilename+CHR(13)
	tmessage = tmessage+CHR(13)+"for Office "+cOffice+", BOL# "+cBOL
	tmessage = tmessage+CHR(13)+"has been created and will be transmitted shortly."
	DO FORM m:\dev\frm\dartmail2 WITH tsendto,tfrom,tsubject,tcc,tattach,tmessage,"A"

ENDPROC

****************************
PROCEDURE CLOSEOUT
****************************
	IF lClose
		IF !EMPTY(lnHandle2)
			=FCLOSE(lnHandle2)
		ENDIF
	ENDIF
	closedata()
ENDPROC

****************************
PROCEDURE closedata
****************************
	IF USED('errfile')
		USE IN errfile
	ENDIF
	IF USED('shipment')
		USE IN shipment
	ENDIF
	IF USED('package')
		USE IN package
	ENDIF
	IF USED('fcounter')
		USE IN fcounter
	ENDIF
	IF USED('outship')
		USE IN outship
	ENDIF
	IF USED('outdet')
		USE IN outdet
	ENDIF
	IF USED('upcmast')
		USE IN upcmast
	ENDIF

	WAIT CLEAR
ENDPROC

****************************
PROCEDURE errormail
****************************
	PARAMETERS cxErrMsg
	cFin = UPPER(cFin)
	IF lClose
		FCLOSE(lnHandle2)
	ENDIF
	IF FILE('cFilenameHold')
		DELETE FILE &cFilenameHold
	ENDIF

	lDoMail = .T.
	IF cxErrMsg # "945 CREATED"
		tsendto = tsendtoerr
		tcc = tccerr

		tsubject = cMailname+" EDI ERROR, Transload WO "+ALLT(STR(manifest.wo_num))
		tattach = " "
		tmessage = "Trucking WO #: "+cWO_Num+CHR(10)
		IF EMPTY(cErrMsg)
			tmessage = tmessage + cxErrMsg
		ELSE
			tmessage = tmessage + cErrMsg
		ENDIF
		tmessage = tmessage + CHR(13)+"PO/PT#: "+cPO
		IF lTesting OR lTestmail
			tmessage = tmessage+ CHR(13)+ CHR(13) + "Date/Time Triggered: "+cTrigTime
		ENDIF
		IF lDoMail
			DO FORM m:\dev\frm\dartmail2 WITH tsendto,tfrom,tsubject,tcc,tattach,tmessage,"A"
		ENDIF
	ENDIF
ENDPROC


****************************
PROCEDURE segmentget
****************************

	PARAMETER thisarray,lcKey,nLength

	FOR i = 1 TO nLength
		IF i > nLength
			EXIT
		ENDIF
		lnEnd= AT("*",thisarray[i])
		IF lnEnd > 0
			lcThisKey =TRIM(SUBSTR(thisarray[i],1,lnEnd-1))
			IF OCCURS(lcKey,lcThisKey)>0
				RETURN SUBSTR(thisarray[i],lnEnd+1)
				i = 1
			ENDIF
		ENDIF
	ENDFOR

	RETURN ""
