lparameters xaccountid, xstyle, xcolor, xid, xpack

xdone=.f.

if inlist(xaccountid,4677,5102)		&& Courtaulds & Bugaboo has to be picked FIFO, so let's try this old code first	dy 9/14/07
	create cursor xppindet (indetid i, wo_num n(7), po c(10), totqty n(6), pack c(10))

	create cursor xdiffpack (pack c(10), date_rcvd d, qty n(6))
	index on pack+dtos(date_rcvd) tag pack

	xsqlexec("select * from indet where remainqty>0 and accountid="+transform(xaccountid)+" and units=1 " + ;
		"and style='"+xstyle+"' and color='"+xcolor+"' and id='"+xid+"' and pack='"+xpack+"'","xdyindet",,"wh")
	
	scan
		xunitsindetid=indetid
		xpo=po
		xwo_num=wo_num
		
		xsqlexec("select * from indet where wo_num="+transform(xwo_num)+" and po='"+xpo+"' and units=0","xdyindet2",,"wh")
		scan
			if !seek(pack+dtos(date_rcvd),"xdiffpack","pack")
				insert into xdiffpack (pack, date_rcvd) values (xdyindet2.pack, xdyindet2.date_rcvd)
			endif
			replace qty with xdiffpack.qty+xdyindet2.remainqty in xdiffpack
		endscan
	endscan

	select xdiffpack
	index on date_rcvd tag date_rcvd

	select xdiffpack
	scan 
		xsqlexec("select * from indet where remainqty>0 and accountid="+transform(xaccountid)+" and units=0 " + ;
			"and style='"+xstyle+"' and color='"+xcolor+"' and id='"+xid+"' and pack='"+xdiffpack.pack+"' " + ;
			"and date_rcvd={"+dtoc(xdiffpack.date_rcvd)+"}","xdyindet3",,"wh")

		scan
			xaaindetid=indetid
			scatter memvar fields wo_num, po, pack
			
			if xsqlexec("select * from indet where wo_num="+transform(m.wo_num)+" and po='"+m.po+"' " + ;
				"and style='"+xstyle+"' and color='"+xcolor+"' and id='"+xid+"' and pack='"+xpack+"'","xdyindet4",,"wh") = 0

				email("Dyoung@fmiint.com","INFO: ppindet not found",,,,,.t.,,,,,.t.,,.t.)
			else
				scatter memvar fields indetid, totqty
				insert into xppindet from memvar
			endif
			
			select xdyindet3
			locate for indetid=xaaindetid
		endscan
	endscan
		
	xdone = reccount("xppindet")>0
endif

* the new way works for musicals by calling ppxref	dy 9/14/07

if !xdone
	ppxref(xaccountid,.t.,xstyle,xcolor,xid,xpack)
	select indetid, wo_num, po, totqty, pack from xppxrefrpt where !units and remainqty>0 into cursor xppindet readwrite
endif