* HRCHECKTEST.PRG
* run on Monday - list active employees who did not receive checks last Friday

* Build EXE as F:\UTIL\ADPREPORTS\HRCHECKTEST.exe

LOCAL loHRCHECKTEST

runack("HRCHECKTEST")

utilsetup("HRCHECKTEST")

loHRCHECKTEST = CREATEOBJECT('HRCHECKTEST')

loHRCHECKTEST.MAIN()

schedupdate()

CLOSE DATABASES ALL
RETURN


#DEFINE LOGIT 1
#DEFINE WAITIT 2
#DEFINE NOWAITIT 4
#DEFINE SENDIT 8
#DEFINE CRLF CHR(13) + CHR(10)
#DEFINE RETURN_DATA_MANDATORY .F.
#DEFINE RETURN_DATA_NOT_MANDATORY .T.
#DEFINE CSVFORMAT 6
#DEFINE WRITE_ONLY_UNBUFFERED 11
#DEFINE COMMA ","
#DEFINE xlEdgeBottom 9
#DEFINE xlContinuous 1
#DEFINE xlMedium -4138
#DEFINE xlFONT_RED -16776961

DEFINE CLASS HRCHECKTEST AS CUSTOM

	cProcessName = 'HRCHECKTEST'

	lTestMode = .F.  && .F. FOR NORMAL OPERATION, .T. TO SUPPRESS ALL BUT DEVELOPER EMAILS AND CHANGE INPUT TABLE, ETC.

	dToday = DATE()

	lAutoYield = .T.

	cStartTime = TTOC(DATETIME())

	* connection properties
	nSQLHandle = 0

	* wait window properties
	nWaitWindowTimeout = 2
	lWaitWindowIsOn = .T.

	* logfile properties
	lLoggingIsOn = .T.
	cLogFile = 'F:\UTIL\ADPREPORTS\LOGFILES\HRCHECKTEST_log.txt'

	* INTERNAL email properties
	lSendInternalEmailIsOn = .T.
	cFrom = 'Mark.Bennett@Tollgroup.com'
	cSendTo = 'Marie.Freiberger@Tollgroup.com'
	cCC = 'Lauren.Klaver@Tollgroup.com, mbennett@fmiint.com'
	cSubject = ''
	cAttach = ''
	cBodyText = ''

	cCOMPUTERNAME = ''
	cUSERNAME = ''


	FUNCTION INIT
		IF NOT DODEFAULT()
			RETURN .F.
		ENDIF
		WITH THIS
			*SET RESOURCE OFF
			CLOSE DATA
			SET DECIMALS TO 4
			SET CENTURY ON
			SET DATE AMERICAN
			SET HOURS TO 24
			SET ANSI ON
			SET TALK OFF
			SET DELETED ON
			SET CONSOLE OFF
			SET EXCLUSIVE OFF
			SET SAFETY OFF
			SET EXACT OFF
			SET STATUS BAR ON
			SET SYSMENU OFF
			SET ENGINEBEHAVIOR 70
			_VFP.AUTOYIELD = .lAutoYield
			.cCOMPUTERNAME = GETENV("COMPUTERNAME")
			.cUSERNAME = GETENV("USERNAME")
			IF .lTestMode THEN
				.cLogFile = 'F:\UTIL\ADPREPORTS\LOGFILES\HRCHECKTEST_log_TESTMODE.txt'
				.cSendTo = 'mbennett@fmiint.com'
				.cCC = ''
			ENDIF
			.lLoggingIsOn = .lLoggingIsOn AND NOT EMPTY(.cLogFile)
			IF .lLoggingIsOn THEN
				SET ALTERNATE TO (.cLogFile) ADDITIVE
				SET ALTERNATE ON
			ENDIF
			SET PROCEDURE TO VALIDATIONS ADDITIVE
		ENDWITH
	ENDFUNC


	FUNCTION DESTROY
		WITH THIS
			IF .lLoggingIsOn  THEN
				SET ALTERNATE OFF
				SET ALTERNATE TO
			ENDIF
		ENDWITH
		DODEFAULT()
	ENDFUNC


	FUNCTION MAIN
		WITH THIS
			LOCAL lnNumberOfErrors, lcSQL, lcSQL2, loError, ldFromDate
			LOCAL lcFiletoSaveAs, lcSpreadsheetTemplate
			LOCAL ldFromDate, ldToDate, lcADPFromDate, lcADPToDate, ldTermCheckDate, lcADPTermCheckDate
			LOCAL lcDateSuffix, lnTotalFSA, ldEligDate, lcHomeDept
			LOCAL i, oExcel, oWorkbook, oWorksheet, lnStartRow, lcStartRow, lnRow, lcRow
			LOCAL lcFirstName, lcNewFirstName, lcLastName, lnSpacePos, lcMI, lnMaxAmount
			LOCAL llSalariedsToo, lcCompanyList

			TRY
				lnNumberOfErrors = 0

				.TrackProgress("HR CHECK TEST process started....", LOGIT+SENDIT+NOWAITIT)
				.TrackProgress('COMPUTERNAME = ' + .cCOMPUTERNAME, LOGIT+SENDIT)
				.TrackProgress('USERNAME = ' + .cUSERNAME, LOGIT+SENDIT)
				.TrackProgress('PROJECT = HRCHECKTEST', LOGIT+SENDIT)
				IF .lTestMode THEN
					.TrackProgress('                        TEST MODE                        ', LOGIT+SENDIT)
				ENDIF

				* determine prior Friday
				ldFromDate = .dToday
				
				*!*	******************************************
				*!*	** activate to force specific date 
*!*					ldFromDate = {^2013-12-30}
				*!*	******************************************
				
				
				DO WHILE DOW(ldFromDate,1) <> 6
					ldFromDate = ldFromDate - 1
				ENDDO


				lcDateSuffix = DTOC(ldFromDate)
				lcADPFromDate = "DATE'" + TRANSFORM(YEAR(ldFromDate)) + "-" + PADL(MONTH(ldFromDate),2,'0') + "-" + PADL(DAY(ldFromDate),2,'0') + "'"

				.cSubject = 'HR CHECK TEST for ' + lcDateSuffix

				lcFiletoSaveAs = 'F:\UTIL\ADPREPORTS\REPORTS\CHECKTEST ' + STRTRAN(lcDateSuffix,"/","")
				lcSaveFile = lcFiletoSaveAs + '.XLS'

				.TrackProgress('Creating spreadsheet: ' + lcSaveFile, LOGIT+SENDIT+NOWAITIT)


				* delete output file if it already exists...
				IF FILE(lcSaveFile) THEN
					DELETE FILE (lcSaveFile)
				ENDIF

				* now connect to ADP...

				.nSQLHandle = SQLCONNECT("PCPAYWIN","MBENNETT","VP737A")

				IF .nSQLHandle > 0 THEN

					lcWhere = ""


					SET TEXTMERGE ON
					TEXT TO	lcSQL NOSHOW
SELECT
COUNT(*) AS CHECKCNT
FROM REPORTS.V_CHK_VW_INFO
WHERE CHECKVIEWPAYDATE = <<lcADPFromDate>>
AND COMPANYCODE IN ('E88','E89')

					ENDTEXT
					SET TEXTMERGE OFF




					IF USED('CUREEINFOPRE') THEN
						USE IN CUREEINFOPRE
					ENDIF
					IF USED('CUREEINFO') THEN
						USE IN CUREEINFO
					ENDIF

					IF .lTestMode THEN
						.TrackProgress('lcSQL = ' + lcSQL, LOGIT+SENDIT)
					ENDIF

					IF .ExecSQL(lcSQL, 'CUREEINFOPRE', RETURN_DATA_MANDATORY) THEN

						SELECT CUREEINFOPRE
						llSalariedsToo = (CUREEINFOPRE.CHECKCNT > 50)

						IF llSalariedsToo THEN
							lcCompanyList = "('E87','E88','E89')"
							.TrackProgress('- It was a salaried paydate -', LOGIT+SENDIT)
						ELSE
							lcCompanyList = "('E87')"
						ENDIF

						SET TEXTMERGE ON
						TEXT TO	lcSQL2 NOSHOW
SELECT
A.COMPANYCODE AS ADP_COMP,
A.NAME,
A.FILE# AS EMP_NUM,
A.STATUS,
A.HIREDATE AS PAYDATE,
(SELECT MAX(CHECKVIEWPAYDATE) FROM REPORTS.V_CHK_VW_INFO WHERE NAME = A.NAME) AS LASTCHECK
FROM REPORTS.V_EMPLOYEE A
WHERE A.STATUS = 'A'
AND A.COMPANYCODE IN <<lcCompanyList>>
AND A.FILE# NOT IN
(SELECT FILE# FROM REPORTS.V_CHK_VW_INFO WHERE CHECKVIEWPAYDATE = <<lcADPFromDate>>)
ORDER BY 1, 2

						ENDTEXT
						SET TEXTMERGE OFF
						
						IF .lTestMode THEN
							.TrackProgress('lcSQL2 = ' + lcSQL2, LOGIT+SENDIT)
						ENDIF

						IF .ExecSQL(lcSQL2, 'CUREEINFO', RETURN_DATA_NOT_MANDATORY) THEN

							IF USED('CUREEINFO') AND NOT EOF('CUREEINFO') THEN
							
								.cBodyText = 'See attached list of active employees who did not receive checks last Friday!' + CRLF + CRLF + '<<REPORT LOG FOLLOWS>>' + .cBodyText
														
								SELECT CUREEINFO
								SCAN
									REPLACE CUREEINFO.PAYDATE WITH ldFromDate IN CUREEINFO
								ENDSCAN
								SELECT CUREEINFO
								COPY TO (lcFiletoSaveAs) XL5

								IF FILE(lcSaveFile) THEN
									.cAttach = lcSaveFile
								ELSE
									.TrackProgress("ERROR attaching " + lcSaveFile, LOGIT+SENDIT+NOWAITIT)
								ENDIF
								
							ELSE
								.cBodyText = 'All active employees received checks last Friday!' + CRLF + CRLF + '<<REPORT LOG FOLLOWS>>' + .cBodyText						
							ENDIF

						ENDIF  &&  .ExecSQL(lcSQL2, 'SQLCURSOR1', RETURN_DATA_MANDATORY)

					ENDIF  &&  .ExecSQL(lcSQL, 'SQLCURSOR1', RETURN_DATA_MANDATORY)

					=SQLDISCONNECT(.nSQLHandle)
				ELSE
					* connection error
					.TrackProgress('Unable to connect to ADP Payroll System.', LOGIT+SENDIT)
				ENDIF   &&  .nSQLHandle > 0

				.TrackProgress("HR CHECK TEST process ended normally.", LOGIT+SENDIT+NOWAITIT)

			CATCH TO loError

				.TrackProgress('There was an error.',LOGIT+SENDIT)
				.TrackProgress('The Error # is: ' + TRANSFORM(loError.ERRORNO), LOGIT+SENDIT)
				.TrackProgress('The Error Message is: ' + TRANSFORM(loError.MESSAGE), LOGIT+SENDIT)
				.TrackProgress('The Error occurred in Program: ' + TRANSFORM(loError.PROCEDURE), LOGIT+SENDIT)
				.TrackProgress('The Error occurred at line #: ' + TRANSFORM(loError.LINENO), LOGIT+SENDIT)
				lnNumberOfErrors = lnNumberOfErrors + 1

				IF TYPE('oWorkbook') = "O" AND NOT ISNULL(oWorkbook) THEN
					oWorkbook.SAVE()
				ENDIF
				IF TYPE('oExcel') = "O" AND NOT ISNULL(oExcel) THEN
					oExcel.QUIT()
				ENDIF

				CLOSE DATABASES ALL

			ENDTRY

			WAIT CLEAR
			***************** INTERNAL email results ******************************


			.TrackProgress('About to send status email.',LOGIT)
			.TrackProgress('The logfile is: ' + .cLogFile, SENDIT)
			.TrackProgress("HR CHECK TEST process started: " + .cStartTime, LOGIT+SENDIT)
			.TrackProgress("HR CHECK TEST process finished: " + TTOC(DATETIME()), LOGIT+SENDIT)

			IF .lSendInternalEmailIsOn THEN
				DO FORM dartmail2 WITH .cSendTo,.cFrom,.cSubject,.cCC,.cAttach,.cBodyText,"A"
				.TrackProgress('Sent status email.',LOGIT)
			ELSE
				.TrackProgress('Did not send status email: THIS.lSendInternalEmailIsOn = FALSE.',LOGIT)
			ENDIF

		ENDWITH
		RETURN
	ENDFUNC && main


	FUNCTION ExecSQL
		LPARAMETERS tcSQL, tcCursorName, tlNoDataReturnedIsOkay
		LOCAL llRetval, lnResult
		WITH THIS
			* close target cursor if it's open
			IF USED(tcCursorName)
				USE IN (tcCursorName)
			ENDIF
			WAIT WINDOW "Fetching remote data; please wait..." NOWAIT
			lnResult = SQLEXEC(.nSQLHandle, tcSQL, tcCursorName)
			llRetval = ( lnResult > 0 )
			IF llRetval THEN
				* see if any data came back
				IF NOT tlNoDataReturnedIsOkay THEN
					IF (NOT USED(tcCursorName)) OR EOF(tcCursorName) THEN
						llRetval = .F.
						.TrackProgress('No data was returned by this query: ' +CRLF + tcSQL, LOGIT+SENDIT)
						.cSendTo = 'Mark Bennett <mbennett@fmiint.com>'
					ENDIF
				ENDIF
			ELSE
				.TrackProgress("Result Code [" + TRANSFORM(lnResult) + "] returned by this query: " + tcSQL, LOGIT+SENDIT)
				.cSendTo = 'Mark Bennett <mbennett@fmiint.com>'
			ENDIF
			WAIT CLEAR
			RETURN llRetval
		ENDWITH
	ENDFUNC


	PROCEDURE TrackProgress
		* do any combination of Wait Window, writing to logfile, and adding to body of email,
		* based on nFlags parameter.
		LPARAMETERS tcExpression, tnFlags
		WITH THIS
			IF BITAND(tnFlags,LOGIT) = LOGIT THEN
				IF .lLoggingIsOn THEN
					?
					? .cProcessName + "  " + TTOC(DATETIME()) + ": " + tcExpression
				ENDIF
			ENDIF
			IF .lWaitWindowIsOn THEN
				IF BITAND(tnFlags,NOWAITIT) = NOWAITIT THEN
					WAIT WINDOW tcExpression NOWAIT
				ENDIF
				IF BITAND(tnFlags,WAITIT) = WAITIT THEN
					WAIT WINDOW tcExpression TIMEOUT .nWaitWindowTimeout
				ENDIF
			ENDIF
			IF BITAND(tnFlags,SENDIT) = SENDIT THEN
				IF .lSendInternalEmailIsOn THEN
					.cBodyText = .cBodyText + tcExpression + CRLF + CRLF
				ENDIF
			ENDIF
		ENDWITH
	ENDPROC  &&  TrackProgress


	FUNCTION GetSQLStartDateTimeString
		* returns a timestamp string for use in SQL queries
		LPARAMETERS tdDate
		LOCAL lcSetDate, lcSQLDateTimeString
		lcSetDate = SET('DATE')
		SET DATE YMD
		lcSQLDateTimeString   = "{ts '" + TRANSFORM(YEAR(tdDate)) + "-" + PADL(MONTH(tdDate),2,"0") + "-" + PADL(DAY(tdDate),2,"0") + " 00:00:00.0'}"
		SET DATE &lcSetDate.
		RETURN lcSQLDateTimeString
	ENDFUNC


	FUNCTION GetSQLEndDateTimeString
		* returns a timestamp string for use in SQL queries
		LPARAMETERS tdDate
		LOCAL lcSetDate, lcSQLDateTimeString
		lcSetDate = SET('DATE')
		SET DATE YMD
		lcSQLDateTimeString = "{ts '" + TRANSFORM(YEAR(tdDate)) + "-" + PADL(MONTH(tdDate),2,"0") + "-" + PADL(DAY(tdDate),2,"0") + " 23:59:59.9'}"
		SET DATE &lcSetDate.
		RETURN lcSQLDateTimeString
	ENDFUNC


ENDDEFINE
