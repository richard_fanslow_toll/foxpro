* go through Building Maintenance work orders and summarize all costs for reporting purposes
LPARAMETER tdStartDate, tdEndDate, tcCostCenter, tlShowExtraInfo

#DEFINE CRLF CHR(13) + CHR(10)
#DEFINE HDECS 2
#DEFINE BLDG_MAINT_RATE 30

SET SAFETY OFF
SET EXCLUSIVE OFF
SET DELETED ON
SET STRICTDATE TO 0

LOCAL lnRow, lcRow, lcReportPeriod, lcFiletoSaveAs, lcSpreadsheetTemplate
LOCAL oExcel, oWorkbook, oWorksheet, lcErr, lcTitle, loError, lcRightMostColumn
LOCAL lnTotalCost, lnCostPerCenter, lnNumberOfCostCenters, lcFirstSubRow, lcLastSubRow
LOCAL lcCostCenter, llShowExtraInfo, lcCostCenterFilter, lcFileNameCostCenter, lnHourlyRate


lcCostCenter = tcCostCenter
llShowExtraInfo = tlShowExtraInfo

IF llShowExtraInfo THEN
	lcRightMostColumn = "AM"
ELSE
	lcRightMostColumn = "AJ"
ENDIF

DO CASE
	CASE lcCostCenter = "ALL"
		lcCostCenterFilter = ""
		lcFileNameCostCenter = ""
	CASE lcCostCenter = "CIP"
		lcCostCenterFilter = "AND LCIP"
		lcFileNameCostCenter = "_CIP"
	OTHERWISE
		lcCostCenterFilter = "AND LDIV" + lcCostCenter
		lcFileNameCostCenter = "_DIV_" + lcCostCenter
ENDCASE

* make these visible in called procs
PRIVATE pdStartDate, pdEndDate

pdStartDate = tdStartDate
pdEndDate = tdEndDate
IF (MONTH(pdStartDate) = MONTH(pdEndDate)) AND (YEAR(pdStartDate) = YEAR(pdEndDate)) THEN
	lcReportPeriod = CMONTH(pdStartDate) + "_" + TRANSFORM(YEAR(pdStartDate))
ELSE
	lcReportPeriod = STRTRAN(DTOC(pdStartDate),"/","-") + "_thru_" + STRTRAN(DTOC(pdEndDate),"/","-")
ENDIF

*lcSpreadsheetTemplate = "F:\SHOP\SHDATA\BLDGMAINT_TEMPLATE8.XLS"
*lcSpreadsheetTemplate = "F:\SHOP\SHDATA\BLDGMAINT_TEMPLATE9.XLS"
*lcSpreadsheetTemplate = "F:\SHOP\SHDATA\BLDGMAINT_TEMPLATE10.XLS"
*lcSpreadsheetTemplate = "F:\SHOP\SHDATA\BLDGMAINT_TEMPLATE11.XLS"
lcSpreadsheetTemplate = "F:\SHOP\SHDATA\BLDGMAINT_TEMPLATE12.XLS"

lcFiletoSaveAs = "F:\SHOP\AllocationReports\Bldg_Maint_WO_Allocation_for_" + lcReportPeriod + lcFileNameCostCenter + ".XLS"
lcTitle = "Building Maintenance Allocation Spreadsheet"

*!*	* for testing
*!*	OPEN DATABASE M:\DEV\SHDATA\SH

IF NOT USED('ordhdr')
	USE SH!ordhdr IN 0
ENDIF

IF NOT USED('repdetl')
	USE SH!repdetl IN 0 ORDER repdate
ENDIF

CheckWOsums()

*SET STEP ON
TRY

	SELECT *, 0000.00 AS RATE ;
		FROM ordhdr ;
		INTO CURSOR curBMORDERS ;
		WHERE (crdate >= pdStartDate) ;
		AND (crdate <= pdEndDate) ;
		AND (TYPE = 'BLDG') ;
		AND (NOT EMPTY(ORDNO)) ;
		&lcCostCenterFilter. ;
		ORDER BY crdate ;
		READWRITE

	IF USED('ordhdr')
		USE IN ordhdr
	ENDIF

	IF USED('repdetl')
		USE IN repdetl
	ENDIF

	IF USED('curBMORDERS') AND RECCOUNT('curBMORDERS') > 0 THEN

		*********************************************************
		*********************************************************
		SELECT curBMORDERS
		SCAN
			*REPLACE totlcost WITH (tothours * BLDG_MAINT_RATE)
			*(lnHourlyRate = GetHourlyBMRateByCRDate( curBMORDERS.crdate )
			lnHourlyRate = GetHourlyBMRateByCRDate( curBMORDERS.crdate, curBMORDERS.ldiv50 ) && 7/10/13 MB per JO
			REPLACE rate WITH lnHourlyRate, totlcost WITH (tothours * lnHourlyRate)
		ENDSCAN
		*********************************************************
		*********************************************************

		oExcel = CREATEOBJECT("excel.application")
		oExcel.VISIBLE = .F.
		oWorkbook = oExcel.workbooks.OPEN(lcSpreadsheetTemplate)
		oWorkbook.SAVEAS(lcFiletoSaveAs)

		oWorksheet = oWorkbook.Worksheets[1]
		oWorksheet.RANGE("A7","AM2000").clearcontents()

		IF (MONTH(pdStartDate) = MONTH(pdEndDate)) AND (YEAR(pdStartDate) = YEAR(pdEndDate)) THEN
			oWorksheet.RANGE("J2").VALUE = CMONTH(pdStartDate) + " " + TRANSFORM(YEAR(pdStartDate))
		ELSE
			oWorksheet.RANGE("J2").VALUE = DTOC(pdStartDate)+" to "+DTOC(pdEndDate)
		ENDIF

		IF llShowExtraInfo THEN
			oWorksheet.RANGE("AK6").VALUE = 'Mechanic'
			oWorksheet.RANGE("AL6").VALUE = 'Hours'
			oWorksheet.RANGE("AM6").VALUE = 'Work Description'
		ENDIF

		oWorksheet.RANGE("A75","AM2000").FONT.bold = .F.

		lnRow = 6
		lcFirstSubRow = ALLTRIM(STR(lnRow + 1))

		SELECT curBMORDERS
		SCAN

			lnRow = lnRow + 1
			lcRow = ALLTRIM(STR(lnRow))

			lcLastSubRow = ALLTRIM(STR(lnRow))

			* populate Date, WO# spreadsheet columns
			oWorksheet.RANGE("A"+lcRow).VALUE = curBMORDERS.crdate
			oWorksheet.RANGE("B"+lcRow).VALUE = curBMORDERS.ORDNO

			* calc total cost
			lnTotalCost = curBMORDERS.totlcost + curBMORDERS.totpcost + curBMORDERS.outscost

			* populate Totals spreadsheet columns
			oWorksheet.RANGE("AF"+lcRow).VALUE = curBMORDERS.totlcost
			oWorksheet.RANGE("AG"+lcRow).VALUE = curBMORDERS.rate
			oWorksheet.RANGE("AH"+lcRow).VALUE = curBMORDERS.totpcost
			oWorksheet.RANGE("AI"+lcRow).VALUE = curBMORDERS.outscost
			oWorksheet.RANGE("AJ"+lcRow).VALUE = lnTotalCost

			IF llShowExtraInfo THEN
				oWorksheet.RANGE("AK"+lcRow).VALUE = ALLTRIM(curBMORDERS.mechname)
				oWorksheet.RANGE("AL"+lcRow).VALUE = curBMORDERS.tothours
				oWorksheet.RANGE("AM"+lcRow).VALUE = ALLTRIM(curBMORDERS.workdesc)
				oWorksheet.RANGE("AM"+lcRow).WrapText = .T.
			ENDIF

			* HANDLE CIP; IF CIP, THEN IT GETS TOTAL COST AND SKIP COST CENTERS BECAUSE THEY ARE EXCLUSIVE
			IF curBMORDERS.LCIP THEN
				oWorksheet.RANGE("AE"+lcRow).VALUE = lnTotalCost
			ELSE
				* COUNT # OF CENTERS TO DIVIDE TOTAL COST BETWEEN
				lnNumberOfCostCenters = 0
				lnNumberOfCostCenters = lnNumberOfCostCenters + IIF(curBMORDERS.lDiv01,1,0)
				lnNumberOfCostCenters = lnNumberOfCostCenters + IIF(curBMORDERS.lDiv02,1,0)
				lnNumberOfCostCenters = lnNumberOfCostCenters + IIF(curBMORDERS.lDiv03,1,0)
				lnNumberOfCostCenters = lnNumberOfCostCenters + IIF(curBMORDERS.lDiv04,1,0)
				lnNumberOfCostCenters = lnNumberOfCostCenters + IIF(curBMORDERS.lDiv05,1,0)
				lnNumberOfCostCenters = lnNumberOfCostCenters + IIF(curBMORDERS.lDiv06,1,0)
				lnNumberOfCostCenters = lnNumberOfCostCenters + IIF(curBMORDERS.lDiv07,1,0)
				lnNumberOfCostCenters = lnNumberOfCostCenters + IIF(curBMORDERS.lDiv14,1,0)
				lnNumberOfCostCenters = lnNumberOfCostCenters + IIF(curBMORDERS.lDiv15,1,0)
				lnNumberOfCostCenters = lnNumberOfCostCenters + IIF(curBMORDERS.lDiv21,1,0)
				lnNumberOfCostCenters = lnNumberOfCostCenters + IIF(curBMORDERS.lDiv26,1,0)
				lnNumberOfCostCenters = lnNumberOfCostCenters + IIF(curBMORDERS.lDiv32,1,0)
				lnNumberOfCostCenters = lnNumberOfCostCenters + IIF(curBMORDERS.lDiv38,1,0)
				lnNumberOfCostCenters = lnNumberOfCostCenters + IIF(curBMORDERS.lDiv50,1,0)
				lnNumberOfCostCenters = lnNumberOfCostCenters + IIF(curBMORDERS.lDiv51,1,0)
				lnNumberOfCostCenters = lnNumberOfCostCenters + IIF(curBMORDERS.lDiv52,1,0)
				lnNumberOfCostCenters = lnNumberOfCostCenters + IIF(curBMORDERS.lDiv53,1,0)
				lnNumberOfCostCenters = lnNumberOfCostCenters + IIF(curBMORDERS.lDiv54,1,0)
				lnNumberOfCostCenters = lnNumberOfCostCenters + IIF(curBMORDERS.lDiv55,1,0)
				lnNumberOfCostCenters = lnNumberOfCostCenters + IIF(curBMORDERS.lDiv56,1,0)
				lnNumberOfCostCenters = lnNumberOfCostCenters + IIF(curBMORDERS.lDiv57,1,0)
				lnNumberOfCostCenters = lnNumberOfCostCenters + IIF(curBMORDERS.lDiv58,1,0)
				lnNumberOfCostCenters = lnNumberOfCostCenters + IIF(curBMORDERS.lDiv59,1,0)
				lnNumberOfCostCenters = lnNumberOfCostCenters + IIF(curBMORDERS.lDiv60,1,0)
				lnNumberOfCostCenters = lnNumberOfCostCenters + IIF(curBMORDERS.lDiv61,1,0)
				lnNumberOfCostCenters = lnNumberOfCostCenters + IIF(curBMORDERS.lDiv62,1,0)
				lnNumberOfCostCenters = lnNumberOfCostCenters + IIF(curBMORDERS.lDiv66,1,0)
				lnNumberOfCostCenters = lnNumberOfCostCenters + IIF(curBMORDERS.lDiv69,1,0)
				lnNumberOfCostCenters = lnNumberOfCostCenters + IIF(curBMORDERS.lDiv71,1,0)
				lnNumberOfCostCenters = lnNumberOfCostCenters + IIF(curBMORDERS.lDiv75,1,0)
				lnNumberOfCostCenters = lnNumberOfCostCenters + IIF(curBMORDERS.lDiv91,1,0)
				lnNumberOfCostCenters = lnNumberOfCostCenters + IIF(curBMORDERS.lDiv08,1,0)

				** default cost centers to $0
				*oWorksheet.RANGE("C"+lcRow+":X"+lcRow).VALUE = 0.00

				IF lnNumberOfCostCenters > 0 THEN
					lnCostPerCenter = lnTotalCost / lnNumberOfCostCenters

					* populate spreadsheet cost center columns
					IF curBMORDERS.lDiv01 THEN
						oWorksheet.RANGE("C"+lcRow).VALUE = lnCostPerCenter
					ENDIF
					IF curBMORDERS.lDiv02 THEN
						oWorksheet.RANGE("D"+lcRow).VALUE = lnCostPerCenter
					ENDIF
					IF curBMORDERS.lDiv03 THEN
						oWorksheet.RANGE("E"+lcRow).VALUE = lnCostPerCenter
					ENDIF
					IF curBMORDERS.lDiv04 THEN
						oWorksheet.RANGE("F"+lcRow).VALUE = lnCostPerCenter
					ENDIF
					IF curBMORDERS.lDiv05 THEN
						oWorksheet.RANGE("G"+lcRow).VALUE = lnCostPerCenter
					ENDIF
					IF curBMORDERS.lDiv06 THEN
						oWorksheet.RANGE("H"+lcRow).VALUE = lnCostPerCenter
					ENDIF
					IF curBMORDERS.lDiv07 THEN
						oWorksheet.RANGE("I"+lcRow).VALUE = lnCostPerCenter
					ENDIF
					IF curBMORDERS.lDiv14 THEN
						oWorksheet.RANGE("J"+lcRow).VALUE = lnCostPerCenter
					ENDIF
*!*						IF curBMORDERS.lDiv32 THEN
*!*							oWorksheet.RANGE("J"+lcRow).VALUE = lnCostPerCenter
*!*						ENDIF
*!*						IF curBMORDERS.lDiv38 THEN
*!*							oWorksheet.RANGE("K"+lcRow).VALUE = lnCostPerCenter
*!*						ENDIF
					IF curBMORDERS.lDiv50 THEN
						oWorksheet.RANGE("K"+lcRow).VALUE = lnCostPerCenter
					ENDIF
					IF curBMORDERS.lDiv51 THEN
						oWorksheet.RANGE("L"+lcRow).VALUE = lnCostPerCenter
					ENDIF
					IF curBMORDERS.lDiv52 THEN
						oWorksheet.RANGE("M"+lcRow).VALUE = lnCostPerCenter
					ENDIF
					IF curBMORDERS.lDiv53 THEN
						oWorksheet.RANGE("N"+lcRow).VALUE = lnCostPerCenter
					ENDIF
					IF curBMORDERS.lDiv54 THEN
						oWorksheet.RANGE("O"+lcRow).VALUE = lnCostPerCenter
					ENDIF
					IF curBMORDERS.lDiv55 THEN
						oWorksheet.RANGE("P"+lcRow).VALUE = lnCostPerCenter
					ENDIF
					IF curBMORDERS.lDiv56 THEN
						oWorksheet.RANGE("Q"+lcRow).VALUE = lnCostPerCenter
					ENDIF
					IF curBMORDERS.lDiv57 THEN
						oWorksheet.RANGE("R"+lcRow).VALUE = lnCostPerCenter
					ENDIF
					IF curBMORDERS.lDiv58 THEN
						oWorksheet.RANGE("S"+lcRow).VALUE = lnCostPerCenter
					ENDIF
*!*						IF curBMORDERS.lDiv59 THEN
*!*							oWorksheet.RANGE("U"+lcRow).VALUE = lnCostPerCenter
*!*						ENDIF
					IF curBMORDERS.lDiv60 THEN
						oWorksheet.RANGE("T"+lcRow).VALUE = lnCostPerCenter
					ENDIF
					IF curBMORDERS.lDiv61 THEN
						oWorksheet.RANGE("U"+lcRow).VALUE = lnCostPerCenter
					ENDIF
					IF curBMORDERS.lDiv62 THEN
						oWorksheet.RANGE("V"+lcRow).VALUE = lnCostPerCenter
					ENDIF
					IF curBMORDERS.lDiv71 THEN
						oWorksheet.RANGE("W"+lcRow).VALUE = lnCostPerCenter
					ENDIF
					IF curBMORDERS.lDiv21 THEN
						oWorksheet.RANGE("X"+lcRow).VALUE = lnCostPerCenter
					ENDIF
					IF curBMORDERS.lDiv26 THEN
						oWorksheet.RANGE("Y"+lcRow).VALUE = lnCostPerCenter
					ENDIF
					IF curBMORDERS.lDiv15 THEN
						oWorksheet.RANGE("Z"+lcRow).VALUE = lnCostPerCenter
					ENDIF
					IF curBMORDERS.lDiv91 THEN
						oWorksheet.RANGE("AA"+lcRow).VALUE = lnCostPerCenter
					ENDIF
					IF curBMORDERS.lDiv66 THEN
						oWorksheet.RANGE("AB"+lcRow).VALUE = lnCostPerCenter
					ENDIF
					IF curBMORDERS.lDiv08 THEN
						oWorksheet.RANGE("AC"+lcRow).VALUE = lnCostPerCenter
					ENDIF
					IF curBMORDERS.lDiv69 THEN
						oWorksheet.RANGE("AD"+lcRow).VALUE = lnCostPerCenter
					ENDIF

					*!*					* format cells in row as currency
					*!*					oWorksheet.RANGE("C"+lcRow+":AA"+lcRow).NumberFormat = "$#,##0.00"

				ELSE
					* nothing
				ENDIF  &&  lnNumberOfCostCenters > 0
			ENDIF  &&  curBMORDERS.LCIP

			* format cells in row as currency
			oWorksheet.RANGE("C"+lcRow+":AJ"+lcRow).NumberFormat = "$#,##0.00"
			* format hours
			oWorksheet.RANGE("AL"+lcRow).NumberFormat = "#0.00"

		ENDSCAN

		* totals
		lnRow = lnRow + 2
		lcRow = ALLTRIM(STR(lnRow))
		oWorksheet.RANGE("B"+lcRow).VALUE = "Totals:"

		oWorksheet.RANGE("C" + lcRow).VALUE = "=SUM(C" + lcFirstSubRow + ":C" + lcLastSubRow + ")"
		oWorksheet.RANGE("D" + lcRow).VALUE = "=SUM(D" + lcFirstSubRow + ":D" + lcLastSubRow + ")"
		oWorksheet.RANGE("E" + lcRow).VALUE = "=SUM(E" + lcFirstSubRow + ":E" + lcLastSubRow + ")"
		oWorksheet.RANGE("F" + lcRow).VALUE = "=SUM(F" + lcFirstSubRow + ":F" + lcLastSubRow + ")"
		oWorksheet.RANGE("G" + lcRow).VALUE = "=SUM(G" + lcFirstSubRow + ":G" + lcLastSubRow + ")"
		oWorksheet.RANGE("H" + lcRow).VALUE = "=SUM(H" + lcFirstSubRow + ":H" + lcLastSubRow + ")"
		oWorksheet.RANGE("I" + lcRow).VALUE = "=SUM(I" + lcFirstSubRow + ":I" + lcLastSubRow + ")"
		oWorksheet.RANGE("J" + lcRow).VALUE = "=SUM(J" + lcFirstSubRow + ":J" + lcLastSubRow + ")"
		oWorksheet.RANGE("K" + lcRow).VALUE = "=SUM(K" + lcFirstSubRow + ":K" + lcLastSubRow + ")"
		oWorksheet.RANGE("L" + lcRow).VALUE = "=SUM(L" + lcFirstSubRow + ":L" + lcLastSubRow + ")"
		oWorksheet.RANGE("M" + lcRow).VALUE = "=SUM(M" + lcFirstSubRow + ":M" + lcLastSubRow + ")"
		oWorksheet.RANGE("N" + lcRow).VALUE = "=SUM(N" + lcFirstSubRow + ":N" + lcLastSubRow + ")"
		oWorksheet.RANGE("O" + lcRow).VALUE = "=SUM(O" + lcFirstSubRow + ":O" + lcLastSubRow + ")"
		oWorksheet.RANGE("P" + lcRow).VALUE = "=SUM(P" + lcFirstSubRow + ":P" + lcLastSubRow + ")"
		oWorksheet.RANGE("Q" + lcRow).VALUE = "=SUM(Q" + lcFirstSubRow + ":Q" + lcLastSubRow + ")"
		oWorksheet.RANGE("R" + lcRow).VALUE = "=SUM(R" + lcFirstSubRow + ":R" + lcLastSubRow + ")"
		oWorksheet.RANGE("S" + lcRow).VALUE = "=SUM(S" + lcFirstSubRow + ":S" + lcLastSubRow + ")"
		oWorksheet.RANGE("T" + lcRow).VALUE = "=SUM(T" + lcFirstSubRow + ":T" + lcLastSubRow + ")"
		oWorksheet.RANGE("U" + lcRow).VALUE = "=SUM(U" + lcFirstSubRow + ":U" + lcLastSubRow + ")"
		oWorksheet.RANGE("V" + lcRow).VALUE = "=SUM(V" + lcFirstSubRow + ":V" + lcLastSubRow + ")"
		oWorksheet.RANGE("W" + lcRow).VALUE = "=SUM(W" + lcFirstSubRow + ":W" + lcLastSubRow + ")"
		oWorksheet.RANGE("X" + lcRow).VALUE = "=SUM(X" + lcFirstSubRow + ":X" + lcLastSubRow + ")"
		oWorksheet.RANGE("Y" + lcRow).VALUE = "=SUM(Y" + lcFirstSubRow + ":Y" + lcLastSubRow + ")"
		oWorksheet.RANGE("Z" + lcRow).VALUE = "=SUM(Z" + lcFirstSubRow + ":Z" + lcLastSubRow + ")"
		oWorksheet.RANGE("AA" + lcRow).VALUE = "=SUM(AA" + lcFirstSubRow + ":AA" + lcLastSubRow + ")"
		oWorksheet.RANGE("AB" + lcRow).VALUE = "=SUM(AB" + lcFirstSubRow + ":AB" + lcLastSubRow + ")"
		oWorksheet.RANGE("AC" + lcRow).VALUE = "=SUM(AC" + lcFirstSubRow + ":AC" + lcLastSubRow + ")"
		oWorksheet.RANGE("AD" + lcRow).VALUE = "=SUM(AD" + lcFirstSubRow + ":AD" + lcLastSubRow + ")"
		
		oWorksheet.RANGE("AF" + lcRow).VALUE = "=SUM(AF" + lcFirstSubRow + ":AF" + lcLastSubRow + ")"
		oWorksheet.RANGE("AH" + lcRow).VALUE = "=SUM(AH" + lcFirstSubRow + ":AH" + lcLastSubRow + ")"
		oWorksheet.RANGE("AI" + lcRow).VALUE = "=SUM(AI" + lcFirstSubRow + ":AI" + lcLastSubRow + ")"
		oWorksheet.RANGE("AJ" + lcRow).VALUE = "=SUM(AJ" + lcFirstSubRow + ":AJ" + lcLastSubRow + ")"

		* add a totals row up top for Jim ONeil 10/1/13 MB
		oWorksheet.RANGE("C4").VALUE = "=SUM(C" + lcFirstSubRow + ":C" + lcLastSubRow + ")"
		oWorksheet.RANGE("D4").VALUE = "=SUM(D" + lcFirstSubRow + ":D" + lcLastSubRow + ")"
		oWorksheet.RANGE("E4").VALUE = "=SUM(E" + lcFirstSubRow + ":E" + lcLastSubRow + ")"
		oWorksheet.RANGE("F4").VALUE = "=SUM(F" + lcFirstSubRow + ":F" + lcLastSubRow + ")"
		oWorksheet.RANGE("G4").VALUE = "=SUM(G" + lcFirstSubRow + ":G" + lcLastSubRow + ")"
		oWorksheet.RANGE("H4").VALUE = "=SUM(H" + lcFirstSubRow + ":H" + lcLastSubRow + ")"
		oWorksheet.RANGE("I4").VALUE = "=SUM(I" + lcFirstSubRow + ":I" + lcLastSubRow + ")"
		oWorksheet.RANGE("J4").VALUE = "=SUM(J" + lcFirstSubRow + ":J" + lcLastSubRow + ")"
		oWorksheet.RANGE("K4").VALUE = "=SUM(K" + lcFirstSubRow + ":K" + lcLastSubRow + ")"
		oWorksheet.RANGE("L4").VALUE = "=SUM(L" + lcFirstSubRow + ":L" + lcLastSubRow + ")"
		oWorksheet.RANGE("M4").VALUE = "=SUM(M" + lcFirstSubRow + ":M" + lcLastSubRow + ")"
		oWorksheet.RANGE("N4").VALUE = "=SUM(N" + lcFirstSubRow + ":N" + lcLastSubRow + ")"
		oWorksheet.RANGE("O4").VALUE = "=SUM(O" + lcFirstSubRow + ":O" + lcLastSubRow + ")"
		oWorksheet.RANGE("P4").VALUE = "=SUM(P" + lcFirstSubRow + ":P" + lcLastSubRow + ")"
		oWorksheet.RANGE("Q4").VALUE = "=SUM(Q" + lcFirstSubRow + ":Q" + lcLastSubRow + ")"
		oWorksheet.RANGE("R4").VALUE = "=SUM(R" + lcFirstSubRow + ":R" + lcLastSubRow + ")"
		oWorksheet.RANGE("S4").VALUE = "=SUM(S" + lcFirstSubRow + ":S" + lcLastSubRow + ")"
		oWorksheet.RANGE("T4").VALUE = "=SUM(T" + lcFirstSubRow + ":T" + lcLastSubRow + ")"
		oWorksheet.RANGE("U4").VALUE = "=SUM(U" + lcFirstSubRow + ":U" + lcLastSubRow + ")"
		oWorksheet.RANGE("V4").VALUE = "=SUM(V" + lcFirstSubRow + ":V" + lcLastSubRow + ")"
		oWorksheet.RANGE("W4").VALUE = "=SUM(W" + lcFirstSubRow + ":W" + lcLastSubRow + ")"
		oWorksheet.RANGE("X4").VALUE = "=SUM(X" + lcFirstSubRow + ":X" + lcLastSubRow + ")"
		oWorksheet.RANGE("Y4").VALUE = "=SUM(Y" + lcFirstSubRow + ":Y" + lcLastSubRow + ")"
		oWorksheet.RANGE("Z4").VALUE = "=SUM(Z" + lcFirstSubRow + ":Z" + lcLastSubRow + ")"
		oWorksheet.RANGE("AA4").VALUE = "=SUM(AA" + lcFirstSubRow + ":AA" + lcLastSubRow + ")"
		oWorksheet.RANGE("AB4").VALUE = "=SUM(AB" + lcFirstSubRow + ":AB" + lcLastSubRow + ")"
		oWorksheet.RANGE("AC4").VALUE = "=SUM(AC" + lcFirstSubRow + ":AC" + lcLastSubRow + ")"
		oWorksheet.RANGE("AD4").VALUE = "=SUM(AD" + lcFirstSubRow + ":AD" + lcLastSubRow + ")"
		
		oWorksheet.RANGE("AF4").VALUE = "=SUM(AF" + lcFirstSubRow + ":AF" + lcLastSubRow + ")"
		oWorksheet.RANGE("AH4").VALUE = "=SUM(AH" + lcFirstSubRow + ":AH" + lcLastSubRow + ")"
		oWorksheet.RANGE("AI4").VALUE = "=SUM(AI" + lcFirstSubRow + ":AI" + lcLastSubRow + ")"
		oWorksheet.RANGE("AJ4").VALUE = "=SUM(AJ" + lcFirstSubRow + ":AJ" + lcLastSubRow + ")"

		* format cells in row as currency
		oWorksheet.RANGE("C"+lcRow+":AJ"+lcRow).NumberFormat = "$#,##0.00"
		
		* format hours
		oWorksheet.RANGE("AL"+lcRow).NumberFormat = "#0.00"

		* set print area
		*oWorksheet.RANGE("A1",lcRightMostColumn+lcRow).SELECT
		oWorksheet.PageSetup.PrintArea = "$A$1:$" + lcRightMostColumn + "$" + lcRow

		WAIT CLEAR

		MESSAGEBOX("The Allocation Spreadsheet has been populated.",0 + 64,lcTitle)
		oWorkbook.SAVE()
		oExcel.VISIBLE = .T. 
	ELSE
		MESSAGEBOX("No Building Maintenance Work Orders were found for that date range.",0 + 64,lcTitle)
	ENDIF  && USED('curBMORDERS') AND RECCOUNT('curBMORDERS') > 0

CATCH TO loError

	lcErr = 'There was an error.' + CRLF
	lcErr = lcErr + 'The Error # is: ' + TRANSFORM(loError.ERRORNO) + CRLF
	lcErr = lcErr + 'The Error Message is: ' + TRANSFORM(loError.MESSAGE) + CRLF
	lcErr = lcErr + 'The Error occurred in Program: ' + TRANSFORM(loError.PROCEDURE) + CRLF
	lcErr = lcErr + 'The Error occurred at line #: ' + TRANSFORM(loError.LINENO)
	MESSAGEBOX(lcErr,0+16,lcTitle)
	*oExcel.VISIBLE = .T.
	IF TYPE('oExcel') = "O" AND NOT ISNULL(oExcel) THEN
		oExcel.QUIT()
	ENDIF

FINALLY

	IF USED('curBMORDERS')
		USE IN curBMORDERS
	ENDIF

ENDTRY

RETURN



********************************************************************************************
PROCEDURE CheckWOsums()

	LOCAL lnRepairPrice

	WAIT WINDOW "Summing up repair parts..." NOWAIT

	SELECT repdetl
	SET ORDER TO repdate
	SCAN FOR (repdate  >= pdStartDate) AND (repdate <= pdEndDate)
		REPLACE extprice WITH (repqty * unitprice)
	ENDSCAN
	SELECT repdetl
	LOCATE


	SELECT ordhdr
	SCAN FOR (crdate >= pdStartDate) AND (crdate <= pdEndDate) AND (TYPE = 'BLDG') AND (NOT EMPTY(ORDNO))

		WAIT WINDOW "checking Work Order Sums for Order #" + TRANSFORM(ordhdr.ORDNO) + "..." NOWAIT

		SELECT repdetl
		GOTO TOP
		SUM ALL repdetl.extprice FOR repdetl.ORDNO == ordhdr.ORDNO TO lnRepairPrice
		SELECT repdetl
		LOCATE
		REPLACE ordhdr.partcost WITH lnRepairPrice IN ordhdr

		REPLACE ordhdr.totpcost WITH lnRepairPrice IN ordhdr

	ENDSCAN

ENDPROC
********************************************************************************************

FUNCTION GetHourlyBMRateByCRDate
	LPARAMETERS tdCRdate, tlDiv50
	DO CASE
		CASE BETWEEN(tdCRdate,{^2013-07-01},{^2099-12-31}) AND tlDiv50
			lnRate = 0.00
		CASE tdCRdate < {^2008-09-01}
			lnRate = 30.00
		CASE tdCRdate >= {^2011-08-01}
			lnRate = 25.00
		OTHERWISE
			lnRate = 40.00  
	ENDCASE
	RETURN lnRate
ENDFUNC

* PREVIOUS TO 7/10/13 CHANGES...
*!*	FUNCTION GetHourlyBMRateByCRDate
*!*		LPARAMETERS tdCRdate
*!*		DO CASE
*!*			CASE tdCRdate < {^2008-09-01}
*!*				lnRate = 30.00
*!*			CASE tdCRdate >= {^2011-08-01}
*!*				lnRate = 25.00
*!*			OTHERWISE
*!*				lnRate = 40.00  
*!*		ENDCASE
*!*		RETURN lnRate
*!*	ENDFUNC


*!*	********************************************************************************************
*!*	********************************************************************************************
*!*	*** OLD CODE BELOW
* go through Building Maintenance work orders and summarize all costs for reporting purposes
*!*	LPARAMETER tdStartDate, tdEndDate, tcCostCenter, tlShowExtraInfo

*!*	#DEFINE CRLF CHR(13) + CHR(10)
*!*	#DEFINE HDECS 2
*!*	#DEFINE BLDG_MAINT_RATE 30

*!*	SET SAFETY OFF
*!*	SET EXCLUSIVE OFF
*!*	SET DELETED ON
*!*	SET STRICTDATE TO 0

*!*	LOCAL lnRow, lcRow, lcReportPeriod, lcFiletoSaveAs, lcSpreadsheetTemplate
*!*	LOCAL oExcel, oWorkbook, oWorksheet, lcErr, lcTitle, loError, lcRightMostColumn
*!*	LOCAL lnTotalCost, lnCostPerCenter, lnNumberOfCostCenters, lcFirstSubRow, lcLastSubRow
*!*	LOCAL lcCostCenter, llShowExtraInfo, lcCostCenterFilter, lcFileNameCostCenter, lnHourlyRate


*!*	lcCostCenter = tcCostCenter
*!*	llShowExtraInfo = tlShowExtraInfo

*!*	IF llShowExtraInfo THEN
*!*		lcRightMostColumn = "AE"
*!*	ELSE
*!*		lcRightMostColumn = "AB"
*!*	ENDIF

*!*	*!*	IF (lcCostCenter = "ALL") THEN
*!*	*!*		lcCostCenterFilter = ""
*!*	*!*		lcFileNameCostCenter = ""
*!*	*!*	ELSE
*!*	*!*		lcCostCenterFilter = "AND LDIV" + lcCostCenter
*!*	*!*		lcFileNameCostCenter = "_DIV_" + lcCostCenter
*!*	*!*	ENDIF

*!*	DO CASE
*!*		CASE lcCostCenter = "ALL"
*!*			lcCostCenterFilter = ""
*!*			lcFileNameCostCenter = ""
*!*		CASE lcCostCenter = "CIP"
*!*			lcCostCenterFilter = "AND LCIP"
*!*			lcFileNameCostCenter = "_CIP"
*!*		OTHERWISE
*!*			lcCostCenterFilter = "AND LDIV" + lcCostCenter
*!*			lcFileNameCostCenter = "_DIV_" + lcCostCenter
*!*	ENDCASE

*!*	* make these visible in called procs
*!*	PRIVATE pdStartDate, pdEndDate

*!*	pdStartDate = tdStartDate
*!*	pdEndDate = tdEndDate
*!*	IF (MONTH(pdStartDate) = MONTH(pdEndDate)) AND (YEAR(pdStartDate) = YEAR(pdEndDate)) THEN
*!*		lcReportPeriod = CMONTH(pdStartDate) + "_" + TRANSFORM(YEAR(pdStartDate))
*!*	ELSE
*!*		lcReportPeriod = STRTRAN(DTOC(pdStartDate),"/","-") + "_thru_" + STRTRAN(DTOC(pdEndDate),"/","-")
*!*	ENDIF

*!*	lcSpreadsheetTemplate = "F:\SHOP\SHDATA\BLDGMAINT_TEMPLATE4.XLS"

*!*	lcFiletoSaveAs = "F:\SHOP\AllocationReports\Bldg_Maint_WO_Allocation_for_" + lcReportPeriod + lcFileNameCostCenter + ".XLS"
*!*	lcTitle = "Building Maintenance Allocation Spreadsheet"

*!*	*!*	* for testing
*!*	*!*	OPEN DATABASE M:\DEV\SHDATA\SH

*!*	IF NOT USED('ordhdr')
*!*		USE SH!ordhdr IN 0
*!*	ENDIF

*!*	IF NOT USED('repdetl')
*!*		USE SH!repdetl IN 0 ORDER repdate
*!*	ENDIF

*!*	CheckWOsums()

*!*	*SET STEP ON
*!*	TRY

*!*		SELECT *, 0000.00 AS RATE ;
*!*			FROM ordhdr ;
*!*			INTO CURSOR curBMORDERS ;
*!*			WHERE (crdate >= pdStartDate) ;
*!*			AND (crdate <= pdEndDate) ;
*!*			AND (TYPE = 'BLDG') ;
*!*			AND (NOT EMPTY(ORDNO)) ;
*!*			&lcCostCenterFilter. ;
*!*			ORDER BY crdate ;
*!*			READWRITE

*!*		IF USED('ordhdr')
*!*			USE IN ordhdr
*!*		ENDIF

*!*		IF USED('repdetl')
*!*			USE IN repdetl
*!*		ENDIF

*!*		IF USED('curBMORDERS') AND RECCOUNT('curBMORDERS') > 0 THEN

*!*			*********************************************************
*!*			*********************************************************
*!*			SELECT curBMORDERS
*!*			SCAN
*!*				*REPLACE totlcost WITH (tothours * BLDG_MAINT_RATE)
*!*				lnHourlyRate = GetHourlyBMRateByCRDate( curBMORDERS.crdate )
*!*				REPLACE rate WITH lnHourlyRate, totlcost WITH (tothours * lnHourlyRate)
*!*			ENDSCAN
*!*			*********************************************************
*!*			*********************************************************

*!*			oExcel = CREATEOBJECT("excel.application")
*!*			oExcel.VISIBLE = .F.
*!*			oWorkbook = oExcel.workbooks.OPEN(lcSpreadsheetTemplate)
*!*			oWorkbook.SAVEAS(lcFiletoSaveAs)

*!*			oWorksheet = oWorkbook.Worksheets[1]
*!*			oWorksheet.RANGE("A5","AE2000").clearcontents()

*!*			IF (MONTH(pdStartDate) = MONTH(pdEndDate)) AND (YEAR(pdStartDate) = YEAR(pdEndDate)) THEN
*!*				oWorksheet.RANGE("J2").VALUE = CMONTH(pdStartDate) + " " + TRANSFORM(YEAR(pdStartDate))
*!*			ELSE
*!*				oWorksheet.RANGE("J2").VALUE = DTOC(pdStartDate)+" to "+DTOC(pdEndDate)
*!*			ENDIF

*!*			IF llShowExtraInfo THEN
*!*				oWorksheet.RANGE("AC4").VALUE = 'Mechanic'
*!*				oWorksheet.RANGE("AD4").VALUE = 'Hours'
*!*				oWorksheet.RANGE("AE4").VALUE = 'Work Description'
*!*			ENDIF

*!*			oWorksheet.RANGE("A5","AE2000").FONT.bold = .F.

*!*			lnRow = 4
*!*			lcFirstSubRow = ALLTRIM(STR(lnRow + 1))

*!*			SELECT curBMORDERS
*!*			SCAN

*!*				lnRow = lnRow + 1
*!*				lcRow = ALLTRIM(STR(lnRow))

*!*				lcLastSubRow = ALLTRIM(STR(lnRow))

*!*				* populate Date, WO# spreadsheet columns
*!*				oWorksheet.RANGE("A"+lcRow).VALUE = curBMORDERS.crdate
*!*				oWorksheet.RANGE("B"+lcRow).VALUE = curBMORDERS.ORDNO

*!*				* calc total cost
*!*				lnTotalCost = curBMORDERS.totlcost + curBMORDERS.totpcost + curBMORDERS.outscost

*!*				* populate Totals spreadsheet columns
*!*				oWorksheet.RANGE("X"+lcRow).VALUE = curBMORDERS.totlcost
*!*				oWorksheet.RANGE("Y"+lcRow).VALUE = curBMORDERS.rate
*!*				oWorksheet.RANGE("Z"+lcRow).VALUE = curBMORDERS.totpcost
*!*				oWorksheet.RANGE("AA"+lcRow).VALUE = curBMORDERS.outscost
*!*				oWorksheet.RANGE("AB"+lcRow).VALUE = lnTotalCost

*!*				IF llShowExtraInfo THEN
*!*					oWorksheet.RANGE("AC"+lcRow).VALUE = ALLTRIM(curBMORDERS.mechname)
*!*					oWorksheet.RANGE("AD"+lcRow).VALUE = curBMORDERS.tothours
*!*					oWorksheet.RANGE("AE"+lcRow).VALUE = ALLTRIM(curBMORDERS.workdesc)
*!*					oWorksheet.RANGE("AE"+lcRow).WrapText = .T.
*!*				ENDIF

*!*				* HANDLE CIP; IF CIP, THEN IT GETS TOTAL COST AND SKIP COST CENTERS BECAUSE THEY ARE EXCLUSIVE
*!*				IF curBMORDERS.LCIP THEN
*!*					oWorksheet.RANGE("W"+lcRow).VALUE = lnTotalCost
*!*				ELSE
*!*					* COUNT # OF CENTERS TO DIVIDE TOTAL COST BETWEEN
*!*					lnNumberOfCostCenters = 0
*!*					lnNumberOfCostCenters = lnNumberOfCostCenters + IIF(curBMORDERS.lDiv01,1,0)
*!*					lnNumberOfCostCenters = lnNumberOfCostCenters + IIF(curBMORDERS.lDiv02,1,0)
*!*					lnNumberOfCostCenters = lnNumberOfCostCenters + IIF(curBMORDERS.lDiv03,1,0)
*!*					lnNumberOfCostCenters = lnNumberOfCostCenters + IIF(curBMORDERS.lDiv04,1,0)
*!*					lnNumberOfCostCenters = lnNumberOfCostCenters + IIF(curBMORDERS.lDiv05,1,0)
*!*					lnNumberOfCostCenters = lnNumberOfCostCenters + IIF(curBMORDERS.lDiv06,1,0)
*!*					lnNumberOfCostCenters = lnNumberOfCostCenters + IIF(curBMORDERS.lDiv07,1,0)
*!*					lnNumberOfCostCenters = lnNumberOfCostCenters + IIF(curBMORDERS.lDiv14,1,0)
*!*					lnNumberOfCostCenters = lnNumberOfCostCenters + IIF(curBMORDERS.lDiv32,1,0)
*!*					lnNumberOfCostCenters = lnNumberOfCostCenters + IIF(curBMORDERS.lDiv38,1,0)
*!*					lnNumberOfCostCenters = lnNumberOfCostCenters + IIF(curBMORDERS.lDiv50,1,0)
*!*					lnNumberOfCostCenters = lnNumberOfCostCenters + IIF(curBMORDERS.lDiv51,1,0)
*!*					lnNumberOfCostCenters = lnNumberOfCostCenters + IIF(curBMORDERS.lDiv52,1,0)
*!*					lnNumberOfCostCenters = lnNumberOfCostCenters + IIF(curBMORDERS.lDiv53,1,0)
*!*					lnNumberOfCostCenters = lnNumberOfCostCenters + IIF(curBMORDERS.lDiv54,1,0)
*!*					lnNumberOfCostCenters = lnNumberOfCostCenters + IIF(curBMORDERS.lDiv55,1,0)
*!*					lnNumberOfCostCenters = lnNumberOfCostCenters + IIF(curBMORDERS.lDiv56,1,0)
*!*					lnNumberOfCostCenters = lnNumberOfCostCenters + IIF(curBMORDERS.lDiv57,1,0)
*!*					lnNumberOfCostCenters = lnNumberOfCostCenters + IIF(curBMORDERS.lDiv58,1,0)
*!*					lnNumberOfCostCenters = lnNumberOfCostCenters + IIF(curBMORDERS.lDiv59,1,0)
*!*					lnNumberOfCostCenters = lnNumberOfCostCenters + IIF(curBMORDERS.lDiv60,1,0)
*!*					lnNumberOfCostCenters = lnNumberOfCostCenters + IIF(curBMORDERS.lDiv61,1,0)
*!*					lnNumberOfCostCenters = lnNumberOfCostCenters + IIF(curBMORDERS.lDiv62,1,0)
*!*					lnNumberOfCostCenters = lnNumberOfCostCenters + IIF(curBMORDERS.lDiv75,1,0)

*!*					** default cost centers to $0
*!*					*oWorksheet.RANGE("C"+lcRow+":W"+lcRow).VALUE = 0.00

*!*					IF lnNumberOfCostCenters > 0 THEN
*!*						lnCostPerCenter = lnTotalCost / lnNumberOfCostCenters

*!*						* populate spreadsheet cost center columns
*!*						IF curBMORDERS.lDiv01 THEN
*!*							oWorksheet.RANGE("C"+lcRow).VALUE = lnCostPerCenter
*!*						ENDIF
*!*						IF curBMORDERS.lDiv02 THEN
*!*							oWorksheet.RANGE("D"+lcRow).VALUE = lnCostPerCenter
*!*						ENDIF
*!*						IF curBMORDERS.lDiv03 THEN
*!*							oWorksheet.RANGE("E"+lcRow).VALUE = lnCostPerCenter
*!*						ENDIF
*!*						IF curBMORDERS.lDiv04 THEN
*!*							oWorksheet.RANGE("F"+lcRow).VALUE = lnCostPerCenter
*!*						ENDIF
*!*						IF curBMORDERS.lDiv05 THEN
*!*							oWorksheet.RANGE("G"+lcRow).VALUE = lnCostPerCenter
*!*						ENDIF
*!*						IF curBMORDERS.lDiv06 THEN
*!*							oWorksheet.RANGE("H"+lcRow).VALUE = lnCostPerCenter
*!*						ENDIF
*!*						IF curBMORDERS.lDiv07 THEN
*!*							oWorksheet.RANGE("I"+lcRow).VALUE = lnCostPerCenter
*!*						ENDIF
*!*						IF curBMORDERS.lDiv14 THEN
*!*							oWorksheet.RANGE("J"+lcRow).VALUE = lnCostPerCenter
*!*						ENDIF
*!*	*!*						IF curBMORDERS.lDiv32 THEN
*!*	*!*							oWorksheet.RANGE("J"+lcRow).VALUE = lnCostPerCenter
*!*	*!*						ENDIF
*!*	*!*						IF curBMORDERS.lDiv38 THEN
*!*	*!*							oWorksheet.RANGE("K"+lcRow).VALUE = lnCostPerCenter
*!*	*!*						ENDIF
*!*						IF curBMORDERS.lDiv50 THEN
*!*							oWorksheet.RANGE("K"+lcRow).VALUE = lnCostPerCenter
*!*						ENDIF
*!*						IF curBMORDERS.lDiv51 THEN
*!*							oWorksheet.RANGE("L"+lcRow).VALUE = lnCostPerCenter
*!*						ENDIF
*!*						IF curBMORDERS.lDiv52 THEN
*!*							oWorksheet.RANGE("M"+lcRow).VALUE = lnCostPerCenter
*!*						ENDIF
*!*						IF curBMORDERS.lDiv53 THEN
*!*							oWorksheet.RANGE("N"+lcRow).VALUE = lnCostPerCenter
*!*						ENDIF
*!*						IF curBMORDERS.lDiv54 THEN
*!*							oWorksheet.RANGE("O"+lcRow).VALUE = lnCostPerCenter
*!*						ENDIF
*!*						IF curBMORDERS.lDiv55 THEN
*!*							oWorksheet.RANGE("P"+lcRow).VALUE = lnCostPerCenter
*!*						ENDIF
*!*						IF curBMORDERS.lDiv56 THEN
*!*							oWorksheet.RANGE("Q"+lcRow).VALUE = lnCostPerCenter
*!*						ENDIF
*!*						IF curBMORDERS.lDiv57 THEN
*!*							oWorksheet.RANGE("R"+lcRow).VALUE = lnCostPerCenter
*!*						ENDIF
*!*						IF curBMORDERS.lDiv58 THEN
*!*							oWorksheet.RANGE("S"+lcRow).VALUE = lnCostPerCenter
*!*						ENDIF
*!*	*!*						IF curBMORDERS.lDiv59 THEN
*!*	*!*							oWorksheet.RANGE("U"+lcRow).VALUE = lnCostPerCenter
*!*	*!*						ENDIF
*!*						IF curBMORDERS.lDiv60 THEN
*!*							oWorksheet.RANGE("T"+lcRow).VALUE = lnCostPerCenter
*!*						ENDIF
*!*						IF curBMORDERS.lDiv61 THEN
*!*							oWorksheet.RANGE("U"+lcRow).VALUE = lnCostPerCenter
*!*						ENDIF
*!*						IF curBMORDERS.lDiv62 THEN
*!*							oWorksheet.RANGE("V"+lcRow).VALUE = lnCostPerCenter
*!*						ENDIF
*!*	*!*						IF curBMORDERS.lDiv75 THEN
*!*	*!*							oWorksheet.RANGE("X"+lcRow).VALUE = lnCostPerCenter
*!*	*!*						ENDIF

*!*						*!*					* format cells in row as currency
*!*						*!*					oWorksheet.RANGE("C"+lcRow+":AA"+lcRow).NumberFormat = "$#,##0.00"

*!*					ELSE
*!*						* nothing
*!*					ENDIF  &&  lnNumberOfCostCenters > 0
*!*				ENDIF  &&  curBMORDERS.LCIP

*!*				* format cells in row as currency
*!*				oWorksheet.RANGE("C"+lcRow+":AB"+lcRow).NumberFormat = "$#,##0.00"
*!*				* format hours
*!*				oWorksheet.RANGE("AD"+lcRow).NumberFormat = "#0.00"

*!*			ENDSCAN

*!*			* totals
*!*			lnRow = lnRow + 2
*!*			lcRow = ALLTRIM(STR(lnRow))
*!*			oWorksheet.RANGE("B"+lcRow).VALUE = "Totals:"

*!*			oWorksheet.RANGE("C" + lcRow).VALUE = "=SUM(C" + lcFirstSubRow + ":C" + lcLastSubRow + ")"
*!*			oWorksheet.RANGE("D" + lcRow).VALUE = "=SUM(D" + lcFirstSubRow + ":D" + lcLastSubRow + ")"
*!*			oWorksheet.RANGE("E" + lcRow).VALUE = "=SUM(E" + lcFirstSubRow + ":E" + lcLastSubRow + ")"
*!*			oWorksheet.RANGE("F" + lcRow).VALUE = "=SUM(F" + lcFirstSubRow + ":F" + lcLastSubRow + ")"
*!*			oWorksheet.RANGE("G" + lcRow).VALUE = "=SUM(G" + lcFirstSubRow + ":G" + lcLastSubRow + ")"
*!*			oWorksheet.RANGE("H" + lcRow).VALUE = "=SUM(H" + lcFirstSubRow + ":H" + lcLastSubRow + ")"
*!*			oWorksheet.RANGE("I" + lcRow).VALUE = "=SUM(I" + lcFirstSubRow + ":I" + lcLastSubRow + ")"
*!*			oWorksheet.RANGE("J" + lcRow).VALUE = "=SUM(J" + lcFirstSubRow + ":J" + lcLastSubRow + ")"
*!*			oWorksheet.RANGE("K" + lcRow).VALUE = "=SUM(K" + lcFirstSubRow + ":K" + lcLastSubRow + ")"
*!*			oWorksheet.RANGE("L" + lcRow).VALUE = "=SUM(L" + lcFirstSubRow + ":L" + lcLastSubRow + ")"
*!*			oWorksheet.RANGE("M" + lcRow).VALUE = "=SUM(M" + lcFirstSubRow + ":M" + lcLastSubRow + ")"
*!*			oWorksheet.RANGE("N" + lcRow).VALUE = "=SUM(N" + lcFirstSubRow + ":N" + lcLastSubRow + ")"
*!*			oWorksheet.RANGE("O" + lcRow).VALUE = "=SUM(O" + lcFirstSubRow + ":O" + lcLastSubRow + ")"
*!*			oWorksheet.RANGE("P" + lcRow).VALUE = "=SUM(P" + lcFirstSubRow + ":P" + lcLastSubRow + ")"
*!*			oWorksheet.RANGE("Q" + lcRow).VALUE = "=SUM(Q" + lcFirstSubRow + ":Q" + lcLastSubRow + ")"
*!*			oWorksheet.RANGE("R" + lcRow).VALUE = "=SUM(R" + lcFirstSubRow + ":R" + lcLastSubRow + ")"
*!*			oWorksheet.RANGE("S" + lcRow).VALUE = "=SUM(S" + lcFirstSubRow + ":S" + lcLastSubRow + ")"
*!*			oWorksheet.RANGE("T" + lcRow).VALUE = "=SUM(T" + lcFirstSubRow + ":T" + lcLastSubRow + ")"
*!*			oWorksheet.RANGE("U" + lcRow).VALUE = "=SUM(U" + lcFirstSubRow + ":U" + lcLastSubRow + ")"
*!*			oWorksheet.RANGE("V" + lcRow).VALUE = "=SUM(V" + lcFirstSubRow + ":V" + lcLastSubRow + ")"
*!*			oWorksheet.RANGE("X" + lcRow).VALUE = "=SUM(X" + lcFirstSubRow + ":X" + lcLastSubRow + ")"
*!*	*!*			oWorksheet.RANGE("Y" + lcRow).VALUE = "=SUM(Y" + lcFirstSubRow + ":Y" + lcLastSubRow + ")"
*!*			oWorksheet.RANGE("Z" + lcRow).VALUE = "=SUM(Z" + lcFirstSubRow + ":Z" + lcLastSubRow + ")"
*!*			oWorksheet.RANGE("AA" + lcRow).VALUE = "=SUM(AA" + lcFirstSubRow + ":AA" + lcLastSubRow + ")"
*!*			oWorksheet.RANGE("AB" + lcRow).VALUE = "=SUM(AB" + lcFirstSubRow + ":AB" + lcLastSubRow + ")"
*!*	*!*			oWorksheet.RANGE("AA" + lcRow).VALUE = "=SUM(AA" + lcFirstSubRow + ":AA" + lcLastSubRow + ")"
*!*	*!*			oWorksheet.RANGE("AB" + lcRow).VALUE = "=SUM(AB" + lcFirstSubRow + ":AB" + lcLastSubRow + ")"
*!*	*!*			oWorksheet.RANGE("AC" + lcRow).VALUE = "=SUM(AC" + lcFirstSubRow + ":AC" + lcLastSubRow + ")"
*!*	*!*			oWorksheet.RANGE("AD" + lcRow).VALUE = "=SUM(AD" + lcFirstSubRow + ":AD" + lcLastSubRow + ")"
*!*	*!*			oWorksheet.RANGE("AE" + lcRow).VALUE = "=SUM(AE" + lcFirstSubRow + ":AE" + lcLastSubRow + ")"

*!*			* format cells in row as currency
*!*			oWorksheet.RANGE("C"+lcRow+":AB"+lcRow).NumberFormat = "$#,##0.00"
*!*			
*!*			* format hours
*!*			oWorksheet.RANGE("AD"+lcRow).NumberFormat = "#0.00"

*!*			* set print area
*!*			*oWorksheet.RANGE("A1",lcRightMostColumn+lcRow).SELECT
*!*			oWorksheet.PageSetup.PrintArea = "$A$1:$" + lcRightMostColumn + "$" + lcRow

*!*			WAIT CLEAR

*!*			MESSAGEBOX("The Allocation Spreadsheet has been populated.",0 + 64,lcTitle)
*!*			oWorkbook.SAVE()
*!*			oExcel.VISIBLE = .T.
*!*		ELSE
*!*			MESSAGEBOX("No Building Maintenance Work Orders were found for that date range.",0 + 64,lcTitle)
*!*		ENDIF  && USED('curBMORDERS') AND RECCOUNT('curBMORDERS') > 0

*!*	CATCH TO loError

*!*		lcErr = 'There was an error.' + CRLF
*!*		lcErr = lcErr + 'The Error # is: ' + TRANSFORM(loError.ERRORNO) + CRLF
*!*		lcErr = lcErr + 'The Error Message is: ' + TRANSFORM(loError.MESSAGE) + CRLF
*!*		lcErr = lcErr + 'The Error occurred in Program: ' + TRANSFORM(loError.PROCEDURE) + CRLF
*!*		lcErr = lcErr + 'The Error occurred at line #: ' + TRANSFORM(loError.LINENO)
*!*		MESSAGEBOX(lcErr,0+16,lcTitle)
*!*		*oExcel.VISIBLE = .T.
*!*		IF TYPE('oExcel') = "O" AND NOT ISNULL(oExcel) THEN
*!*			oExcel.QUIT()
*!*		ENDIF

*!*	FINALLY

*!*		IF USED('curBMORDERS')
*!*			USE IN curBMORDERS
*!*		ENDIF

*!*	ENDTRY

*!*	RETURN



*!*	********************************************************************************************
*!*	PROCEDURE CheckWOsums()

*!*		LOCAL lnRepairPrice

*!*		WAIT WINDOW "Summing up repair parts..." NOWAIT

*!*		SELECT repdetl
*!*		SET ORDER TO repdate
*!*		SCAN FOR (repdate  >= pdStartDate) AND (repdate <= pdEndDate)
*!*			REPLACE extprice WITH (repqty * unitprice)
*!*		ENDSCAN
*!*		SELECT repdetl
*!*		LOCATE


*!*		SELECT ordhdr
*!*		SCAN FOR (crdate >= pdStartDate) AND (crdate <= pdEndDate) AND (TYPE = 'BLDG') AND (NOT EMPTY(ORDNO))

*!*			WAIT WINDOW "checking Work Order Sums for Order #" + TRANSFORM(ordhdr.ORDNO) + "..." NOWAIT

*!*			SELECT repdetl
*!*			GOTO TOP
*!*			SUM ALL repdetl.extprice FOR repdetl.ORDNO == ordhdr.ORDNO TO lnRepairPrice
*!*			SELECT repdetl
*!*			LOCATE
*!*			REPLACE ordhdr.partcost WITH lnRepairPrice IN ordhdr

*!*			REPLACE ordhdr.totpcost WITH lnRepairPrice IN ordhdr

*!*		ENDSCAN

*!*	ENDPROC
*!*	********************************************************************************************

*!*	FUNCTION GetHourlyBMRateByCRDate
*!*		LPARAMETERS tdCRdate
*!*		DO CASE
*!*			CASE tdCRdate < {^2008-09-01}
*!*				lnRate = 30.00
*!*			CASE tdCRdate >= {^2011-08-01}
*!*				lnRate = 25.00
*!*			OTHERWISE
*!*				lnRate = 40.00  
*!*		ENDCASE
*!*		RETURN lnRate
*!*	ENDFUNC
