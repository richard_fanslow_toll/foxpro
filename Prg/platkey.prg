lparameter xid, xacct

if xacct
	if xid>4095
		xreturn = right(transform(xid,"@0"),4)
		if padl(trans(val(xreturn)),4,"0") = xreturn
			xreturn = "0000"+xreturn
		endif
	else
		xreturn = right(transform(xid,"@0"),3)
		if padl(trans(val(xreturn)),3,"0") = xreturn
			xreturn = "00000"+xreturn
		endif
	endif
else
	xreturn = "0000000" + padl(transform(xid),5,"0")
endif

return xreturn