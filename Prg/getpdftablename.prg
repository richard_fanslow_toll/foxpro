* This is a replacement for dt2Month.prg which will break the pdf tables into 3 parts
* for invoices after the cut-off date.

* Before the cut-off date it will return the same value as dt2Month() did.
* After the cut-off date, it will return the same retval, but appended with
* either "A", "B" or "C" depending on the day of the month. So the invoices
* for each month will end up getting stored in 3 tables instead of one.

* This is to prevent possible problems with the PDF tables exceeding the 2gb
* VFP size limitation. 10/20/03 MB.

LPARAMETER tdInvoiceDate

LOCAL lnDay, lcRetVal, lcSuffix

IF VARTYPE(tdInvoiceDate) = "D" THEN
	lcRetVal = PADL(MONTH(tdInvoiceDate),2,"0") + PADL(MOD(YEAR(tdInvoiceDate),100),2,"0")
	IF tdInvoiceDate > {^2003-10-31} THEN
		* add suffix to retval which will create 3 tables, not one, for the month.
		lnDay = DAY(tdInvoiceDate)
		DO CASE
			CASE lnDay < 11
				lcSuffix = "A"
			CASE lnDay < 21
				lcSuffix = "B"
			OTHERWISE
				lcSuffix = "C"
		ENDCASE
		lcRetVal = lcRetVal + lcSuffix
	ENDIF
ELSE
	lcRetVal = ""
ENDIF

RETURN lcRetVal


