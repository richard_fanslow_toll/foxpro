Parameters lcBol,xoffice

Public lcOrder,lcOrdDetailsAndCartons,lcCartonOut

ldCreatedatetime = Datetime()
*close data all 

  lcQuery = lcBol

  lcquery = [select ship_ref,Space(4) as scac, Space(15) as trailer,Space(15) as ponum,Space(12) as ibd,Space(15) as seal,outshipid from outship where accountid = 5687 and bol_no = ']+lcBol+[']

  lcquery = [select ship_ref,Space(4) as scac, Space(15) as trailer,Space(15) as ponum,Space(12) as ibd,Space(15) as seal,outshipid from outship where accountid = 5687 and bol_no = ']+lcBol+[']
  xsqlexec(lcquery,"xpos",,"wh")

  Select xpos
  Scan
    lcquery = [select style,color from outdet where accountid = 5687 and outshipid = ]+Transform(xpos.outshipid)
    xsqlexec(lcquery,"xpodet",,"wh")
    replace xpos.ibd   With xpodet.color In xpos
    replace xpos.ponum With xpodet.style In xpos
  endscan

  lcquery = [select scac,trailer,seal,totqty from bl where accountid = 5687 and bol_no =']+lcBol+[']
  xsqlexec(lcquery,"xbl",,"wh")
  
  If Empty(xbl.scac) Or Empty(xbl.trailer)
    =Messagebox("UA File cannot be created, need a SCAC and Trailer value......."+Chr(13)+"Operation Aborted.......",0,"UA Outbound File Creation")
    return
  Endif 

  Select xpos
  replace All scac    With xbl.scac In xpos
  replace All trailer With xbl.trailer In xpos
  replace All seal    With xbl.seal In xpos
 

*Scan outship For lcBol by 

IF !USED("uaoutbounds")
  USE F:\3pl\DATA\uaoutbounds IN 0
ENDIF


If !Used("ua_counter")
  USE F:\3PL\DATA\UA_COUNTER In 0
Endif 

lnCounter = ua_counter.seqnum 
replace ua_counter.seqnum With ua_counter.seqnum+1 In ua_counter

SELECT xpos

Select * From xpos Group By ibd Into Cursor just_ibds readwrite

lnRun= 1

Select just_ibds
Go top

*set step On 
SCAN
**  lcQuery = "select * from ctnucc where accountid = 5687 and reference ='"+ALLTRIM(xpos.ibd)+"'"

  lcQuery = "select * from ctnucc where accountid = 5687 and reference ='"+ALLTRIM(just_ibds.ibd)+"'"

**in case need to trigger again for a specific palletid - 2 other places need to be un commented
*lcQuery = "select * from ctnucc where accountid = 5687 and reference ='"+Alltrim(xibds.ibd)+"' and outwonum = "+Transform(Manifestnum)+" and palletid = 'PLT010693'"

  xsqlexec(lcQuery,"xctn",,"wh")
  Select xctn
  lcASNNumber = getmemodata("suppdata","ASN")
  num1= RECCOUNT("xctn")
  SELECT xctn
  GO TOP

  gcDocNumber = xctn.REFERENCE

  lnNumCtns = num1
  lnNumCtns = PADL(ALLTRIM(TRANSFORM(lnNumCtns)),5,"0")
&& now for the header of the combined file

* Replace all the predefined data elements
  SET DATE YMD
  SET CENTURY ON

  lcdate =STRTRAN(DTOC(DATE()),"/","")

  SELECT xctn
  GO TOP
  lcShipToID = xctn.shipid
  lcShipFrom = ALLTRIM(whseloc)
***lnNumCtns = Padl(Alltrim(Transform(Reccount("xctn"))),5,"0")

**  lcDocnumber = xpos.ibd
  lcDocnumber = just_ibds.ibd

lcHeader = Filetostr("f:\underarmour\xmltemplates\transload_outbound_bypass_header.txt")
lcHeader = Strtran(lcHeader,"<DOCNUMBER>",Alltrim(just_ibds.ibd))  &&+"_"+Padl(Transform(lnRun),2,"0"))
lcHeader = Strtran(lcHeader,"<EXTDOCNUMBER>",Alltrim(lcASNNumber))  && was asnnum
lcHeader = Strtran(lcHeader,"<SUBBOL>","")
lcHeader = Strtran(lcHeader,"<CARRIER>",Alltrim(just_ibds.scac))  && Needs to be a 4 digit SCAC code
lcHeader = Strtran(lcHeader,"<DOCTYPE>","IBDUPD")  && this for Bypass only

lcHeader = STRTRAN(lcHeader,"<NUMOFCARTONS>",Transform(xbl.totqty))  && total from BOL

lcHeader = Strtran(lcHeader,"<TRAILER>","")  && outbound trailer from BL
If Empty(xpos.trailer)
  lcHeader = Strtran(lcHeader,"<TRAILER2>",Alltrim(just_ibds.trailer))  && outbound trailer from manifest
Else
  lcHeader = Strtran(lcHeader,"<TRAILER2>",Alltrim(lcBOL))  && outbound trailer from manifest
endif
lcHeader = Strtran(lcHeader,"<SEAL>","")  && outbound trailer from manifest
lcHeader = Strtran(lcHeader,"<TRACKNUM>",Alltrim(lcBOL))
lcHeader = Strtran(lcHeader,"<SHIPDATE>",lcDate)

&& header written out, next the Order data

*!*    USE IN xctn

lcOutStr = lcHeader
  lcEndOrder = FILETOSTR("f:\underarmour\xmltemplates\transload_outbound_endorder.txt")


  lcOrder=""
  lcOrdDetailsAndCartons=""
  lcCartonOut =""

  m.podata =""

  DO transload_OrderData WITH ALLTRIM(just_ibds.ibd)

  lcOutStr = lcOutStr+lcOrder+lcOrdDetailsAndCartons

  lcOutStr = lcOutStr &&+lcEndOrder  && close the high level Order loop

  cDate = DTOS(DATE())
  cTruncDate = RIGHT(cDate,6)
  cTruncTime = SUBSTR(TTOC(DATETIME(),1),9,6)
  cfiledate = cDate+cTruncTime
  lcFooter = FILETOSTR("f:\underarmour\xmltemplates\transload_outbound_bypass_footer.txt")

  lcOutStr = lcOutStr+lcFooter

  set step On 

  xxfilename = "F:\FTPUSERS\UNDERARMOUR\OUTBOUND\LSPDLU_"+ALLTRIM(gcDocNumber)+"_"+right(alltrim(lcBOL),6)+"_"+cfiledate+".XML"
  STRTOFILE(lcOutStr,upper(xxfilename))

 
  lnCtnQty = Occurs("<Carton>",lcOutStr)
  lnPOQty = Occurs("<OrderDetails>",lcOutStr)
  WAIT WINDOW AT 10,10 "Creating file: "+xxfilename TIMEOUT 1

  SELECT uaoutbounds
  m.bol = lcBol
  m.scac = just_ibds.scac
  m.seal = just_ibds.seal
  m.ibd =ALLTRIM(gcDocNumber)
  m.shipdt = DATE()
  m.shiptm = DATETIME()
  m.shipto = lcShipToID
  m.shipfrom= lcShipFrom
  m.qty =  num1
  m.trailer =just_ibds.trailer
  m.carrier = just_ibds.scac
  m.filename = xxfilename
  m.poqty =lnPOQty
  m.CtnQty =lnCtnQty
  m.adddt = ldCreatedatetime 
  m.podata = "CTNS*"+Transform(lnCtnQty)
  INSERT INTO uaoutbounds FROM MEMVAR
  lnRun = lnRun+1
Endscan


Select uaoutbounds
Sum ctnqty To bltotal For bol = lcBOL And adddt = ldCreatedatetime 

replace bol_total With bltotal For bol = lcbol


INSERT INTO F:\edirouting\sftpjobs.DBF (jobname, USERID) VALUES ('PUT_XML_TO_UNDERARMOUR','PGAIDIS')

*!*  USE IN xibds
*!*  USE IN manifest
*!*  USE IN uaoutbounds
*!*  USE IN sftpjobs

*!*  lIsError = .F.
*!*  ediupdate("XML CREATED",lIsError)

*********************************************************************************************************8
PROCEDURE transload_OrderData

  PARAMETERS IBDnum

  m.podata = ""

  xsqlexec("select * from ctnucc where accountid = 5687 and reference ='"+ALLTRIM(ibdnum)+['],"xctn",,"wh")

**in case need to trigger again for a specific palletid - 2 other places need to be un commented
*xsqlexec("select * from ctnucc where accountid = 5687 and reference ='"+Alltrim(xibds.ibd)+"' and outwonum = "+Transform(Manifestnum)+" and palletid = 'PLT010693'","xctn",,"wh")

  SELECT xctn

  lcOrder = FILETOSTR("f:\underarmour\xmltemplates\transload_outbound_order.txt")
  lcOrder = STRTRAN(lcOrder,"<IBDORDER>",xctn.REFERENCE)
  lcOrder = STRTRAN(lcOrder,"<ASNTYPE>",xctn.upc)
  IF xctn.STYLE="NA"
    lcOrder = STRTRAN(lcOrder,"<CUSTOMERPO>","")
  ELSE
    lcOrder = STRTRAN(lcOrder,"<CUSTOMERPO>",xctn.STYLE)
  ENDIF

  lcOrdDetailsAndCartons=""

  SELECT xctn
*set step On 
  SELECT ponum,serialno AS linenum FROM xctn  Where ponum in (Select ponum From xpos) INTO CURSOR thesepos GROUP BY ponum,linenum

  SELECT thesepos
  SCAN
    m.podata = m.podata+"PO*"+ALLTRIM(thesepos.ponum)+CHR(13)+"POLINE*"+thesepos.linenum+CHR(13)
    SELECT xctn
    LOCATE FOR ponum = thesepos.ponum AND serialno = thesepos.linenum
    lcOrderDetails = xctn.orddetails
    lccarton = FILETOSTR("f:\underarmour\xmltemplates\transload_outbound_carton.txt")
    lcCartonOut = ""

****

*set step On
    SELECT xctn
    lnCtn = 0
    SCAN FOR ponum = thesepos.ponum AND serialno = thesepos.linenum
      lcCartonOut = lcCartonOut+xctn.ctndetails
      lnCtn=lnCtn+1
    ENDSCAN
    m.podata = m.podata+"POQTY*"+ALLTRIM(TRANSFORM(lnCtn))+CHR(13)

    lcOrdDetailsAndCartons=lcOrdDetailsAndCartons+lcOrderDetails+CHR(13)+lcCartonOut
    lcOrderDetails=""
    lcCartonOut=""
  ENDSCAN

ENDPROC
***************************************************************************************************
 
*********************************************************************************************************8
Procedure transload_OrderData3333

Parameters IBDnum

xsqlexec("select * from ctnucc where accountid = 5687 and reference ='"+Alltrim(IBDnum)+"'","xctn",,"wh")
Select xctn

lcOrder = Filetostr("f:\underarmour\xmltemplates\transload_outbound_order.txt")
lcOrder = Strtran(lcOrder,"<IBDORDER>",xctn.reference)
lcOrder = Strtran(lcOrder,"<ASNTYPE>",xctn.upc)
If xctn.style="NA"
  lcOrder = Strtran(lcOrder,"<CUSTOMERPO>","")
Else
  lcOrder = Strtran(lcOrder,"<CUSTOMERPO>",xctn.style)
Endif 

lcOrdDetailsAndCartons=""

Select xctn
Select ponum,serialno as linenum From xctn Into Cursor thesepos Group By ponum,linenum


Select thesepos
Scan
  Select xctn
  Locate For ponum = thesepos.ponum And serialno = thesepos.linenum 
  lcOrderDetails = xctn.orddetails
  lccarton = Filetostr("f:\underarmour\xmltemplates\transload_outbound_carton.txt")
  lcCartonOut = ""

****

*set step On
  Select xctn
  Scan For ponum = thesepos.ponum And serialno = thesepos.linenum 
    lcCartonOut = lcCartonOut+xctn.ctndetails
  Endscan
 
 lcOrdDetailsAndCartons=lcOrdDetailsAndCartons+lcOrderDetails+Chr(13)+lcCartonOut
 lcOrderDetails=""
 lcCartonOut=""
Endscan

Endproc
***************************************************************************************************
