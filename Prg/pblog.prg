lparameters xoffice, xin0, xnobrow, xauto

if xin0
	xin0="in 0"
else
	xin0=""
endif

if xauto
	xpbfolder=""
else
	xpbfolder="m:\temp\"
endif

use (wf(xoffice)+"pblog") &xin0
select pblog
copy to (xpbfolder+"pblog")
use in pblog

use (xpbfolder+"pblog") &xin0 exclusive 
select pblog
index on putbackid tag putbackid

if !xnobrow
	browse
endif