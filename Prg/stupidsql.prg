
* get the daily pick data from  the SQL server
*******************************************************************************************************
** OK here we get logged into the server
*******************************************************************************************************

select 0
use f:\wh\sqlpassword
csqlpass = alltrim(sqlpassword.password)
use in sqlpassword

cserver="tgfnjsql01"
cdatabase = "PICKPACK"

nset=sqlsetprop(0,"displogin", 3)
nset=sqlsetprop(0,"dispwarnings", .f.)

try

	do case
	case cserver = "EDI1\ECS"
		lcdsnless="driver=SQL Server;server=&cServer;Trusted_Connection=Yes;DATABASE="+cdatabase
	case cserver = "SUM-SQL1"
		lcdsnless="driver=SQL Server;server=&cServer;uid=administrator;pwd=donggeorgewwf;DATABASE="+cdatabase
		lcdsnless="driver=SQL Server;server=&cServer;uid=nji\administrator;pwd=spider06;DATABASE="+cdatabase
		lcdsnless="driver=SQL Server;server=&cServer;Trusted_Connection=Yes;DATABASE="+cdatabase
	case cserver = "SUM-EDI1"
		lcdsnless="driver=SQL Server;server=&cServer;uid=administrator;pwd=donggeorgewwf;DATABASE="+cdatabase
		lcdsnless="driver=SQL Server;server=&cServer;uid=nji\administrator;pwd=spider06;DATABASE="+cdatabase
		lcdsnless="driver=SQL Server;server=&cServer;Trusted_Connection=Yes;DATABASE="+cdatabase
	case cserver = "SQL2"
		lcdsnless="driver=SQL Server;server=&cServer;uid=SA;pwd=&cSQLPass;DATABASE="+cdatabase
	case cserver = "SQL3"
		lcdsnless="driver=SQL Server;Trusted_Connection=Yes;server=&cServer;uid=nji\administrator;pwd=fm!879B;DATABASE="+cdatabase
	case cserver = "SQL3"
		lcdsnless="driver=SQL Server;Trusted_Connection=Yes;server=&cServer;uid=nji\administrator;pwd=fm!879B;DATABASE="+cdatabase
	otherwise
		lcdsnless="driver=SQL Server;server=&cServer;uid=SA;pwd=&cSQLPass;DATABASE="+cdatabase
	endcase

	wait window at 10,10 "Querying "+cserver+"........" nowait
	connhandle=sqlstringconnect(m.lcdsnless,.t.)

*Wait window at 10,10 "ConnHandle = "+Str(thisform.connhandle)
	if connhandle<=0 && bailout
		local array laerror[1]
		aerror(laerror)
		messagebox(laerror[2],16, "Error "+transform(laerror[5]))
		wait window at 10,10 "NO SQL" nowait
		return
	endif

*Wait WINDOW AT 10,10 "Connected: "+lcDSNless

catch
	wait window at 10,10 "NO SQL at the catch level"
endtry

*******************************************************************************************************
** OK here we are logged into the server
*******************************************************************************************************
** now the query

startdate = whichdate &&gdQDate &&Date()-1
enddate = whichdate &&gdQDate &&Date()-1

lcdate = dtoc(startdate)
yr = substr(lcdate,7,4)
mon =substr(lcdate,4,2)
dy = substr(lcdate,1,2)
sdate = alltrim(str(&yr))+[-]+padl(alltrim(str(&dy)),2,"0")+[-]+padl(alltrim(str(&mon)),2,"0")


lcdate = dtoc(enddate)
yr = substr(lcdate,7,4)
mon =substr(lcdate,4,2)
dy = substr(lcdate,1,2)
edate = alltrim(str(&yr))+[-]+padl(alltrim(str(&dy)),2,"0")+[-]+padl(alltrim(str(&mon)),2,"0")


xsql1= [select ship_ref,style,color,id,sum(totqty) as picks,{fn DAYOFMONTH(insdttm)} as DAY,]
xsql2= [{fn MONTH(insdttm)}  as mon, {fn YEAR(insdttm)} as YR, accountid from cartons ]
xsql3= [ where insdttm >= {ts ']+sdate+[ 00:00:00.0'} and insdttm <= {ts ']+ edate +[ 23:59:59.0'} ]
xsql4= [ group by ship_ref,style,color,id,{fn DAYOFMONTH(insdttm)} ,{fn MONTH(insdttm)}, {fn YEAR(insdttm)}, accountid]

lcquery = xsql1+xsql2+xsql3+xsql4

lcresult = "results"

*MessageBox(lcQuery)

lnstr = 0
lnretval=0

wait window at 10,10 "Performing SQL Query for the picked data........." nowait

if usesql()
	xsqlexec("select accountid, ship_ref, style, color, id, insdttm, sum(totqty) as picks from cartons " + ;
		"where between(insdttm,{"+dtoc(startdate)+"},{"+dtoc(enddate+1)+"}) " + ;
		"group by ship_ref, style, color, id, insdttm, accountid","xdy",,"pickpack")

	select *, day(insdttm) as day, month(insdttm) as mon, year(insdttm) as yr from xdy into cursor results readwrite

else
	nok = sqlexec(connhandle,lcquery,lcresult)

	if nok != 1
		local array laerror[1]
		aerror(laerror)
		messagebox(laerror[2],16, "Error "+transform(laerror[5]))
		sqldisconnect(connhandle)
		return
	endif
endif
wait window at 10,10 "Picked Data Query complete........." timeout 1

select results
goto top

if reccount()=0		&& dy 8/14/15 Steel Series no data yet
	return .f.
endif

copy to f:\auto\pickedtoday for &xpnpaccount

********************************************************************************************************
xsql1= [select ship_ref,style,color,id,sum(totqty) as picks,{fn DAYOFMONTH(insdttm)} as DAY,]
xsql2= [{fn MONTH(insdttm)}  as mon, {fn YEAR(insdttm)} as YR, accountid from labels ]
xsql3= [ where insdttm >= {ts ']+sdate+[ 00:00:00.0'} and insdttm <= {ts ']+ edate +[ 23:59:59.0'}]
xsql4= [ group by ship_ref,style,color,id,{fn DAYOFMONTH(insdttm)} ,{fn MONTH(insdttm)}, {fn YEAR(insdttm)}, accountid]

if goffice="7"
	insert into stupid (step, start) values ("GET DATA", datetime())
endif

lcquery = xsql1+xsql2+xsql3+xsql4

wait window at 10,10 "Performing SQL Query for the Open Pick Data........." nowait

if usesql()
	xsqlexec("select accountid, ship_ref, style, color, id, insdttm, sum(totqty) as picks from labels " + ;
		"where between(insdttm,{"+dtoc(startdate)+"},{"+dtoc(enddate+1)+"}) " + ;
		"group by ship_ref, style, color, id, insdttm, accountid","xdy",,"pickpack")

	select *, day(insdttm) as day, month(insdttm) as mon, year(insdttm) as yr from xdy into cursor results readwrite
	
else
	nok = sqlexec(connhandle,lcquery,lcresult)

	if nok != 1
		local array laerror[1]
		aerror(laerror)
		messagebox(laerror[2],16, "Error "+transform(laerror[5]))
		sqldisconnect(connhandle)
		return
	endif
endif

select results
goto top
copy to f:\auto\tobepickedtoday for &xpnpaccount

wait window at 10,10 "Open Pick Data Query complete........." timeout 1

sqldisconnect(connhandle)
