* ANF_315_TO_ICON
* copies ANF 315 files to the ICON folder
*
* build EXE as F:\UTIL\ANF\ANF_315_TO_ICON.EXE
*
* MB 09/25/2017

LOCAL loANF_315_TO_ICON

utilsetup("ANF_315_TO_ICON")

loANF_315_TO_ICON = CREATEOBJECT('ANF_315_TO_ICON')

loANF_315_TO_ICON.MAIN()

schedupdate()

CLOSE DATABASES ALL
RETURN

#DEFINE LOGIT 1
#DEFINE WAITIT 2
#DEFINE NOWAITIT 4
#DEFINE SENDIT 8
#DEFINE CRLF CHR(13) + CHR(10)
#DEFINE LF CHR(10)
#DEFINE RETURN_DATA_MANDATORY .F.
#DEFINE RETURN_DATA_NOT_MANDATORY .T.

DEFINE CLASS ANF_315_TO_ICON AS CUSTOM

	cProcessName = 'ANF_315_TO_ICON'

	lTestMode = .F.  && .F. FOR NORMAL OPERATION, .T. TO SUPPRESS ALL BUT DEVELOPER EMAILS AND CHANGE INPUT TABLE, ETC.

	lAutoYield = .T.

	cStartTime = TTOC(DATETIME())

	* date properties
	dtNow = DATETIME()

	dToday = DATE()

	* wait window properties
	nWaitWindowTimeout = 2
	lWaitWindowIsOn = .T.

	* logfile properties
	lLoggingIsOn = .T.
	cLogFile = 'F:\UTIL\ANF\Logfiles\ANF_315_TO_ICON_log.txt'

	* INTERNAL email properties
	lSendInternalEmailIsOn = .T.
	cFrom = "TGF EDI Ops <fmicorporate@fmiint.com>"
	cSendTo = 'mbennett@fmiint.com'
	cCC = ''
	cSubject = 'ANF 315 to ICON Process for: ' + DTOC(DATE())
	cAttach = ''
	cBodyText = ''

	cCOMPUTERNAME = ''
	cUSERNAME = ''

	FUNCTION INIT
		IF NOT DODEFAULT()
			RETURN .F.
		ENDIF
		WITH THIS
			CLEAR
			*SET RESOURCE OFF
			CLOSE DATA
			SET CENTURY ON
			SET DATE AMERICAN
			SET HOURS TO 24
			SET ANSI ON
			SET TALK OFF
			SET DELETED ON
			SET CONSOLE OFF
			SET EXCLUSIVE OFF
			SET SAFETY OFF
			SET EXACT OFF
			SET STATUS BAR ON
			SET SYSMENU OFF
			SET ENGINEBEHAVIOR 70
			_VFP.AUTOYIELD = .lAutoYield
			.cCOMPUTERNAME = GETENV("COMPUTERNAME")
			.cUSERNAME = GETENV("USERNAME")
			IF .lTestMode THEN
				.cSendTo = 'mbennett@fmiint.com'
				.cLogFile = 'F:\UTIL\ANF\Logfiles\ANF_315_TO_ICON_log_TESTMODE.txt'
			ENDIF
			.lLoggingIsOn = .lLoggingIsOn AND NOT EMPTY(.cLogFile)
			IF .lLoggingIsOn THEN
				SET ALTERNATE TO (.cLogFile) ADDITIVE
				SET ALTERNATE ON
			ENDIF
		ENDWITH
	ENDFUNC


	FUNCTION DESTROY
		WITH THIS
			IF .lLoggingIsOn  THEN
				SET ALTERNATE OFF
				SET ALTERNATE TO
			ENDIF
		ENDWITH
		DODEFAULT()
	ENDFUNC


	FUNCTION MAIN
		WITH THIS
			LOCAL lnNumberOfErrors, lcSourceFolder, lcArchivedFolder, lnNumFiles, lnNumFiles, lcSourceFile, lcArchivedFile, lcTargetFile, llArchived
			LOCAL loWshnetwork, lcICONDrive, llUpdateProfile, llConnect, loNetworkDriveList, llForceConnection, lnDrive, lnDriveCount

			TRY
				lnNumberOfErrors = 0

				.TrackProgress('ANF 315 to ICON process started....', LOGIT+SENDIT+NOWAITIT)
				.TrackProgress('COMPUTERNAME = ' + .cCOMPUTERNAME, LOGIT+SENDIT)
				.TrackProgress('USERNAME = ' + .cUSERNAME, LOGIT+SENDIT)
				.TrackProgress('PROJECT = ANF_315_TO_ICON', LOGIT+SENDIT)
				IF .lTestMode THEN
					.TrackProgress('====>TEST MODE', LOGIT+SENDIT)
				ENDIF
				
				*SET STEP ON 

				lcSourceFolder = 'F:\FTPUSERS\ANF\TGF315\'
				lcArchivedFolder = 'F:\FTPUSERS\ANF\TGF315\ARCHIVE\'

				lnNumFiles = ADIR(laFiles,(lcSourceFolder + "*.xml"))

				.TrackProgress('Found ' + TRANSFORM(lnNumFiles) + ' files to be copied.', LOGIT+SENDIT)

				IF lnNumFiles > 0 THEN

					*!*					* send an email only if files were found
					*!*					.lSendInternalEmailIsOn = .T.

					**************************************************************************************************
					* use windows shell scripting to map to the ICON folder - code derived from DOMTOICON.PRG by MW
					loWshnetwork = CREATEOBJECT("wscript.network")

					**use drive Q
					lcICONDrive = "Q:"
					llUpdateProfile = .T.
					llConnect = .T.
					IF DIRECTORY(lcICONDrive)
						**if drive letter already mapped, make sure its mapped to correct place
						loNetworkDriveList = loWshnetwork.EnumNetworkDrives
						lnDriveCount = loNetworkDriveList.COUNT
						FOR lnDrive = 0 TO lnDriveCount STEP 2
							*if loNetworkDriveList.item(lnDrive) = "Q:"
							IF loNetworkDriveList.ITEM(lnDrive) = lcICONDrive THEN
								IF LOWER(loNetworkDriveList.ITEM(lnDrive+1))="\\10.250.240.193\eadapterglb"
									llConnect = .F.
								ENDIF
								EXIT
							ENDIF
						ENDFOR

						IF llConnect
							**if drive letter exists already, disconnect
							llForceConnection = .T.
							loWshnetwork.removenetworkdrive(lcICONDrive,llForceConnection,llUpdateProfile)
						ENDIF
					ENDIF

					IF llConnect
						loWshnetwork.mapnetworkdrive(lcICONDrive,"\\10.250.240.193\eAdapterGLB",llUpdateProfile,"tollgroup\scsd","TxUJ2yQS")
					ENDIF
					**************************************************************************************************

					* copy the files
					FOR lnCurrentFile = 1 TO lnNumFiles

						lcSourceFile = lcSourceFolder + laFiles[lnCurrentFile,1]
						lcArchivedFile = lcArchivedFolder + laFiles[lnCurrentFile,1]
						lcTargetFile = lcICONDrive + '\' + laFiles[lnCurrentFile,1]

						.TrackProgress('lcSourceFile = ' + lcSourceFile,LOGIT+SENDIT)
						.TrackProgress('lcArchivedFile = ' + lcArchivedFile,LOGIT+SENDIT)
						.TrackProgress('lcTargetFile = ' + lcTargetFile,LOGIT+SENDIT)


						* copy the source file to the ICON folder
						.TrackProgress('Copying ' + lcSourceFile + ' to ' + lcTargetFile, LOGIT+SENDIT+NOWAITIT)
						IF NOT FILE(lcTargetFile) THEN
							COPY FILE (lcSourceFile) TO (lcTargetFile)
						ENDIF

						* if the file already exists in the archive folder, make sure it is not read-only.
						* this is to prevent errors we get copying into archive on a resend of an already-archived file.
						* Necessary because the files in the archive folders were sometimes marked as read-only (not sure why)
						llArchived = FILE(lcArchivedFile)
						IF llArchived THEN
							RUN ATTRIB -R &lcArchivedFile.
							.TrackProgress('   Used ATTRIB -R for re-archiving : ' + lcSourceFile,LOGIT+SENDIT)
						ENDIF

						* archive the source file
						COPY FILE (lcSourceFile) TO (lcArchivedFile)

						* delete the source file if the copy was successful
						IF FILE(lcTargetFile) THEN
							DELETE FILE (lcSourceFile)
						ENDIF
						WAIT CLEAR

					ENDFOR && lnCurrentFile = 1 TO lnNumFiles
				ELSE
					.TrackProgress('Found no 315 files in the source folder', LOGIT+SENDIT)
				ENDIF

				.TrackProgress('ANF 315 to ICON process ended normally', LOGIT+SENDIT+NOWAITIT)

			CATCH TO loError

				.TrackProgress('There was an error.',LOGIT+SENDIT)
				.TrackProgress('The Error # is: ' + TRANSFORM(loError.ERRORNO), LOGIT+SENDIT)
				.TrackProgress('The Error Message is: ' + TRANSFORM(loError.MESSAGE), LOGIT+SENDIT)
				.TrackProgress('The Error occurred in Program: ' + TRANSFORM(loError.PROCEDURE), LOGIT+SENDIT)
				.TrackProgress('The Error occurred at line #: ' + TRANSFORM(loError.LINENO), LOGIT+SENDIT)
				lnNumberOfErrors = lnNumberOfErrors + 1
				IF TYPE('oExcel') = 'O' AND NOT ISNULL(oExcel) THEN
					oExcel.QUIT()
				ENDIF
				CLOSE DATA

			ENDTRY

			CLOSE DATA
			WAIT CLEAR
			***************** INTERNAL email results ******************************
			.TrackProgress('About to send status email.',LOGIT)
			.TrackProgress('==================================================================================================================', SENDIT)
			.TrackProgress('The logfile is: ' + .cLogFile, SENDIT)
			.TrackProgress('ANF 315 to ICON process started: ' + .cStartTime, LOGIT+SENDIT)
			.TrackProgress('ANF 315 to ICON process finished: ' + TTOC(DATETIME()), LOGIT+SENDIT)

			IF .lSendInternalEmailIsOn THEN
				* try to trap error from not having dartmail dll's registered on user's pc...
				TRY
					DO FORM dartmail2 WITH .cSendTo,.cFrom,.cSubject,.cCC,.cAttach,.cBodyText,"A"
					.TrackProgress('Sent status email.',LOGIT)
				CATCH TO loError
					*=MESSAGEBOX('There was an error sending the status email. Please contact Mark Bennett.',0+64,"KRONOS Temp Time")
					.TrackProgress('There was an error sending the status email.',LOGIT)
					.TrackProgress('The Error # is: ' + TRANSFORM(loError.ERRORNO), LOGIT)
					.TrackProgress('The Error Message is: ' + TRANSFORM(loError.MESSAGE), LOGIT)
					.TrackProgress('The Error occurred in Program: ' + TRANSFORM(loError.PROCEDURE), LOGIT)
					.TrackProgress('The Error occurred at line #: ' + TRANSFORM(loError.LINENO), LOGIT)
					lnNumberOfErrors = lnNumberOfErrors + 1
					CLOSE DATA
				ENDTRY

			ELSE
				.TrackProgress('Did not send status email: THIS.lSendInternalEmailIsOn = FALSE.',LOGIT)
			ENDIF

		ENDWITH
		RETURN
	ENDFUNC && main


	FUNCTION ExecSQL
		LPARAMETERS tcSQL, tcCursorName, tlNoDataReturnedIsOkay
		LOCAL llRetval, lnResult
		WITH THIS
			* close target cursor if it's open
			IF USED(tcCursorName)
				USE IN (tcCursorName)
			ENDIF
			WAIT WINDOW "Fetching remote data; please wait..." NOWAIT
			lnResult = SQLEXEC(.nSQLHandle, tcSQL, tcCursorName)
			llRetval = ( lnResult > 0 )
			IF llRetval THEN
				* see if any data came back
				IF NOT tlNoDataReturnedIsOkay THEN
					IF (NOT USED(tcCursorName)) OR EOF(tcCursorName) THEN
						llRetval = .F.
						.TrackProgress('No data was returned by this query: ' +CRLF + tcSQL, LOGIT+SENDIT)
					ENDIF
				ENDIF
			ELSE
				.TrackProgress("Result Code [" + TRANSFORM(lnResult) + "] returned by this query: " + tcSQL, LOGIT+SENDIT)
			ENDIF
			WAIT CLEAR
			RETURN llRetval
		ENDWITH
	ENDFUNC


	PROCEDURE TrackProgress
		* do any combination of Wait Window, writing to logfile, and adding to body of email,
		* based on nFlags parameter.
		LPARAMETERS tcExpression, tnFlags
		WITH THIS
			IF BITAND(tnFlags,LOGIT) = LOGIT THEN
				IF .lLoggingIsOn THEN
					?
					? .cProcessName + "  " + TTOC(DATETIME()) + ": " + tcExpression
				ENDIF
			ENDIF
			IF .lWaitWindowIsOn THEN
				IF BITAND(tnFlags,NOWAITIT) = NOWAITIT THEN
					WAIT WINDOW tcExpression NOWAIT
				ENDIF
				IF BITAND(tnFlags,WAITIT) = WAITIT THEN
					WAIT WINDOW tcExpression TIMEOUT .nWaitWindowTimeout
				ENDIF
			ENDIF
			IF BITAND(tnFlags,SENDIT) = SENDIT THEN
				IF .lSendInternalEmailIsOn THEN
					.cBodyText = .cBodyText + tcExpression + CRLF + CRLF
				ENDIF
			ENDIF
		ENDWITH
	ENDPROC  &&  TrackProgress



ENDDEFINE
