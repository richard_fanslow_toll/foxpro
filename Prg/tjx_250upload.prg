utilsetup("TJX_250")
Close Data All

Public lTesting
Public Array a856(1)

lTesting = .F.
Do m:\dev\prg\_setvars With lTesting

useca("poroute","wo")
use f:\wo\wodata\detail in 0

If !lTesting
  Use F:\edirouting\ftpsetup Shared In 0
  cTransfer = "250-TJX"
  Locate For ftpsetup.transfer = cTransfer And ftpsetup.Type = "GET"
  If ftpsetup.chkbusy And !lOverridebusy
    Wait Window "Process is flagged busy...returning" Timeout 3
    NormalExit = .T.
    Return
  Else
    Replace chkbusy With .T. For ftpsetup.transfer = cTransfer In ftpsetup
  Endif
Endif

*set Step On

NumAdded = 0
NumUpdated = 0

*Select * From poroute Where .F. Into Cursor temppo Readwrite
lcCurrDir = ""

*Use F:\3pl\Data\ackdata In 0

If !lTesting
  lcPath = 'F:\FTPUSERS\TJMAX\250IN\'
  lcArchivePath = 'F:\FTPUSERS\TJMAX\250IN\\ARCHIVE\'
  lc997Path = "f:\FTPUSERS\TJMAX\997trans\"
  tsendto="PGAIDIS@FMIINT.COM,mwinter@fmiint.com"
  tcc="PGAIDIS@FMIINT.COM"
Else
  lcPath = 'F:\FTPUSERS\TJMAX\250IN\'
  lcArchivePath = 'F:\FTPUSERS\TJMAX\250IN\\ARCHIVE\'
  lc997Path = "f:\FTPUSERS\TJMAX\997trans\"
  tsendto="PGAIDIS@FMIINT.COM"
  tcc="PGAIDIS@FMIINT.COM"
Endif

len1 = Adir(ary1,'F:\FTPUSERS\TJMAX\250IN\*.*')
If len1 = 0
  Wait Window "No files found...exiting" Timeout 2
  Select ftpsetup
  Replace chkbusy With .F. For ftpsetup.transfer = "250-TJX" In ftpsetup
  Close Data All
  schedupdate()
  _Screen.Caption=gscreencaption
  On Error
  Return
Endif

lcAddedStr  =""
xladderdtupd=""
ii=1

**put the array of files in a cursor in order to put in order by datetime stamp - mvw 05/24/16
create cursor xfiles (file c(50), x c(10), moddt c(8), modtm c(10), moddttm t(1))
append from array ary1
replace all moddttm with ctot(moddt+" "+modtm)
select * from xfiles order by moddttm into cursor xfiles

thisfile=0
scan
  thisfile=thisfile+1
  wait "Processing file "+transform(thisfile)+" of "+transform(len1) window nowait noclear

  cFilename = alltrim(xfiles.file)
  to997file = lc997Path+alltrim(xfiles.file)
  xfile = lcPath+cFilename
  Do m:\dev\prg\createx856a
  Do m:\dev\prg\loadedifile With xfile,"*","TILDE"

  m.addby = "FILE"+Alltrim(Transform(Thisfile))
  m.adddt = Datetime()
  m.addproc = "FILE"+Alltrim(Transform(Thisfile))  &&"ARIAT832"
  m.accountid = 6565
  m.info = ""

  Select poroute
  Scatter Memvar Memo Blank


  Select x856
  Goto Top

  Do filter_x856 With "PV"

  Select x856
  Goto Top
  Scan
    Do Case
    Case Trim(x856.segment) = "ISA"
      cisa_num = Alltrim(x856.f13)
      m.isanum = Alltrim(x856.f13)
      m.routed = .F.
      lcDate = "20"+Alltrim(x856.f9)
      lxDate = Substr(lcDate,5,2)+"/"+Substr(lcDate,7,2)+"/"+Left(lcDate,4)
      ldDate = Ctod(lxDate)
      m.isadate = ldDate

    Case Trim(x856.segment) = "GS"
      cgs_num = Alltrim(x856.f6)
      lcCurrentGroupNum = Alltrim(x856.f6)
      m.groupnum = Alltrim(x856.f6)

    Case Trim(x856.segment) = "BGN"
*      SET STEP ON
 *     Insert Into ackdata (groupnum,isanum,transnum,edicode,accountid,loaddt,loadtime,filename) Values (lcCurrentGroupNum,cisa_num,Alltrim(x856.f2),"PV",6565,Date(),Datetime(),xfile)
      m.type  = Upper(Allt(x856.f1))  && Orig or Edit
      m.refid = Upper(Allt(x856.f2))
      m.routed = .F.

      lcDate = Alltrim(x856.f3)
      lxDate = Substr(lcDate,5,2)+"/"+Substr(lcDate,7,2)+"/"+Left(lcDate,4)
      ldDate = Ctod(lxDate)
      Store ldDate To m.edidate

    Case x856.segment = "PRF"
      m.ponum   = Upper(Allt(x856.f1))
      m.origdest= Upper(Allt(x856.f5))

    Case x856.segment = "REF" And Allt(x856.f1) = "19"
      m.div = Upper(Allt(x856.f2))

    Case x856.segment = "REF" And Allt(x856.f1) = "DP"
      m.dept = Upper(Allt(x856.f2))

    Case x856.segment = "REF" And Allt(x856.f1) = "ZZ"
      m.poflag = Upper(Allt(x856.f2))

    Case x856.segment = "REF" And Allt(x856.f1) = "S5"
      m.rrcnum = Upper(Allt(x856.f2))

    Case x856.segment = "REF" And Allt(x856.f1) = "EV"
      m.destloc = Upper(Allt(x856.f2))
      m.routed = .T.

    Case x856.segment = "FOB"
      m.fob= Upper(Allt(x856.f1))

    Case x856.segment = "G05"
      m.qty= Val(Transform(x856.f1))

    Case Trim(x856.segment) = "DTM" And Trim(x856.f1) == "256"  && cancel date
      lcDate = Alltrim(x856.f2)
      lxDate = Substr(lcDate,5,2)+"/"+Substr(lcDate,7,2)+"/"+Left(lcDate,4)
      ldDate = Ctod(lxDate)
      m.startdt =ldDate

    Case Trim(x856.segment) = "DTM" And Trim(x856.f1) == "037"  && cancel date
      lcDate = Alltrim(x856.f2)
      lxDate = Substr(lcDate,5,2)+"/"+Substr(lcDate,7,2)+"/"+Left(lcDate,4)
      ldDate = Ctod(lxDate)
      m.shipready= ldDate

    Case Trim(x856.segment) = "DTM" And Trim(x856.f1) == "001"  && cancel date
      lcDate = Alltrim(x856.f2)
      lxDate = Substr(lcDate,5,2)+"/"+Substr(lcDate,7,2)+"/"+Left(lcDate,4)
      ldDate = Ctod(lxDate)
      m.canceldt = ldDate

    Case Trim(x856.segment) = "DTM" And Trim(x856.f1) == "027"  && cancel date
      lcDate = Alltrim(x856.f2)
      lxDate = Substr(lcDate,5,2)+"/"+Substr(lcDate,7,2)+"/"+Left(lcDate,4)
      ldDate = Ctod(lxDate)
      m.ladderdt = ldDate

    Case x856.segment = "N1" And Upper(Allt(x856.f1)) = "VN"
      m.vendor= Upper(Allt(x856.f2))

    Case x856.segment = "SE"
      Do case
        Case m.type="00"
          lcstatus = "ADD"
        Case m.type="05"
          lcstatus = "REPLACE"
        Case m.type="01"
          lcstatus = "DELETE"

      Endcase 
      If m.routed
        lcAddedStr = lcAddedStr+"File: "+xfile+"-"+lcStatus+"      Routed:   "+m.ponum +Chr(13)
      Else
        lcAddedStr = lcAddedStr+"File: "+xfile+"-"+lcStatus+"   UnRouted: "+m.ponum +Chr(13)
      Endif
      m.porouteid = dygenpk("poroute","wo")
      m.startdt=empty2nul(m.startdt)
      m.shipready=empty2nul(m.shipready)
      m.canceldt=empty2nul(m.canceldt)
      m.ladderdt=empty2nul(m.ladderdt)
      m.isadate=empty2nul(m.isadate)
      m.routedt=empty2nul(m.routedt)

	  **delete the po if previously sent
      lcquery = [delete from poroute where ponum =']+Alltrim(m.ponum)+[' and div = ']+Alltrim(m.div)+[']
      xsqlexec(lcquery,"xtemp",,"wo")

      If inlist(m.type,"00","05")
        Insert Into poroute From Memvar
        tu("poroute")
      endif

	  **update ladderdt in detail - mvw 05/06/16
	  select detail
	  xponum=padr(poroute.ponum,len(detail.po_num))
	  locate for accountid=6565 and po_num=xponum
	  if !found()
		**for certain pos chars 3&4 are removed from the po # coming in the 250. those chars should always be "00", stuff them back in for search
		xponum=padr(left(poroute.ponum,2)+"00"+right(poroute.ponum,len(poroute.ponum)-2),len(detail.po_num))
		locate for accountid=6565 and po_num=xponum
		if !found()
			xponum=""
		endif
	  endif

	  if !empty(xponum)
		**only update if date has changed - mvw 06/06/16
		xeventcode=upper(left(cmonth(poroute.ladderdt),3))+" "+right(transform(ladderdt),2)
		select detail
*		locate for wo_date>date()-60 and accountid=6565 and po_num=xponum and whse_loc=alltrim(poroute.div)
		locate for accountid=6565 and po_num=xponum and whse_loc=alltrim(poroute.div)
		if found() and eventcode#xeventcode
			xladderdtupd=xladderdtupd+"PO "+xponum+": Eventcode updated from "+detail.eventcode+" to "+xeventcode+chr(13)+chr(10)
*			replace eventcode with xeventcode for wo_date>date()-60 and accountid=6565 and po_num=xponum and whse_loc=alltrim(poroute.div) in detail
			replace eventcode with xeventcode for accountid=6565 and po_num=xponum and whse_loc=alltrim(poroute.div) in detail
		endif
	  endif
	  **end detail update - mvw 05/06/16
	  
	  select poroute
      Scatter Memvar Memo Blank
      m.info = ""
    Endcase
  Endscan

  tu("poroute")
  Use In poroute
  useca("poroute","wo")

  cLoadFile = (lcPath+cFilename)
  cArchiveFile = (lcArchivePath+cFilename)
  Copy File [&cLoadFile] To [&cArchiveFile]
  Copy File [&cLoadFile] To [&to997file]  && Server EDI1 will take care of the 997s........
  Delete File [&cLoadFile]
Endscan

use in xfiles

**send email warning of eventcode update - 06/06/16
if !empty(xladderdtupd)
	tattach=""
	tFrom="TGFSYSTEM"
	xmsg=xladderdtupd
	tsubject="Important! TJX 250 Upload updated Eventcode for listed POs"
	xto="mwinter@fmiint.com,juan@fmiint.com"
	xcc=""
	Do Form m:\dev\frm\dartmail2 With xto,tFrom,tSubject,xcc,tattach,xmsg,"A"
endif

tattach = ""
tFrom ="Toll EDI System Operations <toll-edi-ops@tollgroup.com>"
tmessage = "TJX 250 Upload updated at: "+Ttoc(Datetime())+Chr(13)+Chr(13)+lcAddedStr
tSubject = "TJX 250 Updated"
Do Form m:\dev\frm\dartmail2 With tsendto,tFrom,tSubject,tcc,tattach,tmessage,"A"

Wait Window "All 250 Files finished processing...exiting" Timeout 2

Select ftpsetup
Replace chkbusy With .F. For ftpsetup.transfer = "250-TJX" In ftpsetup

Close Data All

schedupdate()
*_Screen.Caption=gscreencaption
On Error
