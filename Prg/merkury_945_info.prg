* This program extracts Merkury 945 PT's, BOL, and 945 filename from previous day

utilsetup("MERKURY_945_INFO")
Set Exclusive Off
Set Deleted On
Set Talk Off
Set Safety Off 


WITH _SCREEN	
	.AUTOCENTER = .T.
	.WINDOWSTATE = 0
	.BORDERSTYLE = 1
	.WIDTH = 320
	.HEIGHT = 210
	.TOP = 290
	.LEFT = 110
	.CLOSABLE = .T.
	.MAXBUTTON = .T.
	.MINBUTTON = .T.
	.SCROLLBARS = 0
	.CAPTION = "MERKURY_945_INFO"
ENDWITH	

close databases all

If !Used("edi_trigger")
use f:\3pl\data\edi_trigger SHARED IN 0
ENDIF

xsqlexec("select ship_ref as pt, del_date as shipdate, bol_no as bol, consignee from outship " + ;
	"where mod='2' and del_date={"+dtoc(date()-1)+"} and accountid=6561","t1",,"wh")

SELECT a.*,fin_status,file945 FROM t1 a LEFT JOIN edi_trigger b ON a.pt=b.ship_ref INTO CURSOR t2 READWRITE
SELECT *, SUBSTR(file945,33,23) as filename FROM t2 WHERE SUBSTR(file945,21,3)='945' INTO CURSOR t3 READWRITE
SELECT *, SUBSTR(file945,30,23) as filename FROM t2 WHERE SUBSTR(file945,21,3)='mbol' INTO CURSOR t4 READWRITE
SELECT * FROM t3 UNION ALL SELECT * FROM t4 INTO CURSOR t5 READWRITE
SELECT pt,shipdate,bol,consignee,filename, '945 CREATED' as status FROM t5 WHERE ALLTRIM(fin_status)='945 CREATED' INTO CURSOR T6 READWRITE
SELECT PT,SHIPDATE,BOL,CONSIGNEE,FILE945 AS FILENAME, 'ERROR, TOLL INVESTIGATING' AS STATUS FROM T2 WHERE FIN_STATUS!='945 CREATED' AND !ISNULL(FIN_STATUS) INTO CURSOR T7 READWRITE
SELECT PT,SHIPDATE,BOL,CONSIGNEE,FILE945 AS FILENAME, 'MISSING 945' AS STATUS FROM T2 WHERE ISNULL(FIN_STATUS) INTO CURSOR T8 READWRITE
SELECT * FROM T6 UNION ALL SELECT * FROM T7 UNION ALL SELECT * FROM T8 INTO CURSOR T9 READWRITE



	If Reccount() > 0 
		export TO "S:\Merkury\temp\merkury_945_info"  TYPE xls
	
		tsendto = "alma.navarro@tollgrou.com,maria.estrella@tollgroup.com,viren@merkuryinnovations.com"
		tattach = "S:\Merkury\temp\merkury_945_info.xls" 
		tcc =""                                                                                                                               
		tFrom ="Toll EDI System Operations <fmi-transload-ops@fmiint.com>"                                
		tmessage = "Merkury 945 info:  "+Ttoc(Date()-1)
		tSubject = "Merkury 945 info"                                                           
		Do Form m:\dev\frm\dartmail2 With tsendto,tFrom,tSubject, tcc,tattach,tmessage  ,"A"     
			
	ELSE 


	ENDIF
close data all 

Wait window at 10,10 "all done...." timeout 2


schedupdate()
_screen.Caption=gscreencaption
on error

