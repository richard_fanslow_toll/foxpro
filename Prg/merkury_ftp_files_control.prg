utilsetup("MERKURY_FTP_FILES_CONTROL")
Set Exclusive Off
Set Deleted On
Set Talk Off
Set Safety Off
With _Screen
	.AutoCenter = .T.
	.WindowState = 0
	.BorderStyle = 1
	.Width = 320
	.Height = 210
	.Top = 290
	.Left = 110
	.Closable = .T.
	.MaxButton = .T.
	.MinButton = .T.
	.ScrollBars = 0
	.Caption = "MERKURY_FTP_FILES_CONTROL"
Endwith


If !Used("edi_trigger")
	Use F:\3pl\Data\edi_trigger Shared In 0
Endif

If !Used("ftplog")
	Use F:\wh\ftplog Shared In 0
ENDIF

Select * From ftplog Where dd>=Date()-5 Into Cursor ftplog1
Use In ftplog

*SET STEP ON 
Select space(25) as file_945 ,file945 As trig_945 From edi_trigger Where Inlist(accountid,6561) And edi_type='945 ' And WHEN_proc > Date()-2;
	and WHEN_proc<Datetime()-6000 Into CURSOR trig1 READWRITE 
	DELETE FOR EMPTY(trig_945) IN trig1
	DELETE FOR trig_945 !='945c'  IN trig1	
scan
pos1=ATC('.txt',trig_945)
replace file_945  with substr(trig_945,pos1-18,22)
endscan
Select Distinct file_945 From trig1 Where !Empty(trig_945) Into Cursor trig2 READWRITE 

Select  SPACE(25) as newlogdata ,  logdata From ftplog1  Where 'merkury\' $logdata AND '\945c'  $logdata  And '.txt successfully' $logdata Into Cursor ftplog2 READWRITE
scan
pos1=ATC('.txt successfully',logdata)
replace newlogdata  with substr(logdata,pos1-18,22)
endscan
*!*	SELECT * FROM ftplog2 WHERE log_945 !='945' INTO CURSOR ftplogprob readwrite
*!*	If Reccount() > 0
*!*		Export To "S:\Merkury\temp\ftplogprob"  Type Xls
*!*		tsendto = "tmarg@fmiint.com"
*!*		tsendto = "tmarg@fmiint.com,todd.margolin@tollgroup.com"
*!*		tattach = "S:\Merkury\temp\ftplogprob.xls"
*!*		tcc =""
*!*		tFrom ="Toll EDI System Operations <fmi-transload-ops@fmiint.com>"
*!*		tmessage = "Merkury FTPlog data shifted, fix MERKURY_FTP_FILES_CONTROL "+Ttoc(Datetime())
*!*		tSubject = "Merkury FTPlog data shifted, fix MERKURY_FTP_FILES_CONTROL"
*!*		Do Form m:\dev\frm\dartmail2 With tsendto,tFrom,tSubject, tcc,tattach,tmessage  ,"A"
*!*	Else
*!*	Endif

SELECT * FROM  trig2 a LEFT JOIN ftplog2 b  ON a.file_945=b.newlogdata INTO CURSOR  compare945 READWRITE
Select file_945 From compare945 Where Isnull(newlogdata )Into Cursor merk_missing945 Readwrite

If Reccount() > 0
	Export To "S:\Merkury\temp\MISSING_945_FTP"  Type Xls
	tsendto = "tmarg@fmiint.com"
	tsendto = "tmarg@fmiint.com,todd.margolin@tollgroup.com"
	tattach = "S:\Merkury\temp\MISSING_945_FTP.xls"
	tcc =""
	tFrom ="Toll EDI System Operations <fmi-transload-ops@fmiint.com>"
	tmessage = "Merkury missing 945s from FTPlog "+Ttoc(Datetime())+"    Todd check HOLD folder"
	tSubject = "Merkury Missing 945s from FTPlog"
	Do Form m:\dev\frm\dartmail2 With tsendto,tFrom,tSubject, tcc,tattach,tmessage  ,"A"
Else
	tsendto = "tmarg@fmiint.com"
	tattach = ""
	tcc =""
	tFrom ="Toll EDI System Operations <fmi-transload-ops@fmiint.com>"
	tmessage = "NO_Merkury missing 945s from FTPlog "+Ttoc(Datetime())
	tSubject = "NO_Merkury missing 945s from FTPlog_exist"
	Do Form m:\dev\frm\dartmail2 With tsendto,tFrom,tSubject, tcc,tattach,tmessage  ,"A"
Endif



Close Data All
schedupdate()
_Screen.Caption=gscreencaption




