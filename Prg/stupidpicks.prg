if goffice="7"
	replace end with datetime(), ztime with end-start in stupid
	insert into stupid (step, start) values ("BRING PICKS", datetime())
endif

xsqlexec("select * from pt where mod='"+goffice+"'",,,"wh")
xsqlexec("select * from ptdet where mod='"+goffice+"'",,,"wh")

useca("picks","wh",,,"mod='"+goffice+"'")

if !xfromslotscreen
	xsqlexec("select * from inven where mod='"+goffice+"'",,,"wh")
	xsqlexec("select * from invenloc where mod='"+goffice+"'",,,"wh")
	index on str(accountid,4)+trans(units)+style+color+id+pack tag match
	
	xsqlexec("select * from upcloc where mod='"+goffice+"'","upcloc",,"wh")
	index on str(accountid,4)+style+color+id tag stylecolid
	set order to
endif

select * from picks into cursor xpicks where .f. readwrite
index on style+color+id tag stylecolid

use in picks

*

wait window at 10,10 "Bringing in todays picks.........." nowait
 
use f:\auto\pickedtoday in 0 exclusive
select pickedtoday
try		&& dy 9/22/15
	alter table pickedtoday add column picked d
catch
endtry
replace all picked with ctod(alltrim(str(mon))+"/"+alltrim(str(day))+"/"+alltrim(str(yr)))
use

use f:\auto\tobepickedtoday in 0 exclusive
select tobepickedtoday
try
	alter table tobepickedtoday add column picked d
catch
endtry
replace all picked with ctod(alltrim(str(mon))+"/"+alltrim(str(day))+"/"+alltrim(str(yr)))
use

** OK we have todays picks as well as future picks
** now get the master file and delete todays picks

wait window at 10,10 "Deleting the old picks from the master table.........." nowait

use ("f:\wh"+goffice+"\whdata\alreadypicked") in 0 exclusive 
select alreadypicked
delete for picked = whichdate or picked < whichdate-30
if dow(date())=7
	pack
endif
use
use ("f:\wh"+goffice+"\whdata\alreadypicked")

append from f:\auto\pickedtoday

*

use ("f:\wh"+goffice+"\whdata\tobepicked") in 0 exclusive 
select tobepicked
delete for picked = whichdate or picked < whichdate-30
if dow(date())=7
	pack
endif
use
use ("f:\wh"+goffice+"\whdata\tobepicked")

use f:\auto\tobepickedtoday in 0 
select tobepickedtoday
scan 
	if seek(ship_ref+style+color+id,"tobepicked","ptstyle")
		delete in tobepicked
	endif
endscan
use in tobepickedtoday

select tobepicked
append from f:\auto\tobepickedtoday

*

if goffice="7"
	replace end with datetime(), ztime with end-start in stupid
	insert into stupid (step, start) values ("PURGE", datetime())
endif

purgestupid("alreadypicked")	&& remove deleted PTs
purgestupid("tobepicked")


* add in records for all styles that actually had some picks

if goffice="7"
	replace end with datetime(), ztime with end-start in stupid
	insert into stupid (step, start) values ("BUILD STYLE", datetime())
endif

wait window at 10,10 "Building the styles from all the pick data........." nowait

select style, color, id, accountid from alreadypicked group by 1,2,3 into cursor styles
scan
	xpicksid=dygenpk("picks","whall")
	insert into xpicks (picksid, accountid, style, color, id, zfrom) values (xpicksid, styles.accountid, styles.style, styles.color, styles.id, "A")
endscan

select style, color, id, accountid from tobepicked group by 1,2,3 into cursor styles
scan
	if !seek(styles.style+styles.color+styles.id,"xpicks","stylecolid")
		xpicksid=dygenpk("picks","whall")
		insert into xpicks (picksid, accountid, style, color, id, zfrom) values (xpicksid, styles.accountid, styles.style, styles.color, styles.id, "T")
	endif
endscan 

* now add in everything else in RACK inventory

if goffice="7"
	replace end with datetime(), ztime with end-start in stupid
	insert into stupid (step, start) values ("ADD RACK", datetime())
endif

wait window at 10,10 "Adding RACKs........." nowait

select invenloc
scan for &xpnpaccount and units and whseloc="RACK"
	if !seek(invenloc.style+invenloc.color+invenloc.id,"xpicks","stylecolid")
		xpicksid=dygenpk("picks","whall")
		insert into xpicks (picksid, accountid, style, color, id, zfrom) values (xpicksid, invenloc.accountid, invenloc.style, invenloc.color, invenloc.id, "I")
	endif
endscan

*

if goffice="7"
	replace end with datetime(), ztime with end-start in stupid
	insert into stupid (step, start) values ("BUILD CONS", datetime())
endif

wait window at 10,10 "Building the consolidated table........" nowait

select style, color, id, sum(picks) as beenpicked, picked, 00000000 as topick, 00000000 as needtopick, 000 as ddy ;
	from alreadypicked ;
	group by style, color, id, picked ;
	into cursor apicks readwrite
index on style+color+id+dtoc(picked) tag stydate
 
select style, color, id, sum(picks) as allpicks, picked ;
	from tobepicked ;
	group by style, color, id, picked ;
	into cursor tpicks readwrite
index on style+color+id+dtoc(picked) tag stydate

* now update what the TO-Pick data in the consolodated table

if goffice="7"
	replace end with datetime(), ztime with end-start in stupid
	insert into stupid (step, start) values ("UPD TOPICK", datetime())
endif

select tpicks
scan
	if seek(style+color+id+dtoc(tpicks.picked),"apicks","stydate")
		replace topick with tpicks.allpicks in apicks
	else
		insert into apicks (style, color, id, picked, topick) values (tpicks.style, tpicks.color, tpicks.id, tpicks.picked, tpicks.allpicks)
	endif
endscan

select style, color, id, sum(beenpicked) as pickedtot, 00000000 as openpicks from apicks into cursor opicks readwrite group by style, color, id

select style, color, id, sum(allpicks) as openpicks from tpicks into cursor zpicks readwrite group by style, color, id
index on style+color+id tag stylecolid

select opicks
scan
	if seek(style+color+id,"zpicks","stylecolid")
		replace openpicks with opicks.pickedtot-zpicks.openpicks in opicks
	endif
endscan

replace all ddy with date()-apicks.picked in apicks

*

if goffice="7"
	replace end with datetime(), ztime with end-start in stupid
	insert into stupid (step, start) values ("LAST 30", datetime())
endif

wait window at 10,10 "Computing the last 30 day pick data........." nowait

select apicks
scan
	if seek(style+color+id,"xpicks","stylecolid")
		qfield = "day"+alltrim(str(apicks.ddy))
		try
			replace &qfield with apicks.beenpicked in xpicks
		catch
		endtry
	endif
endscan

select xpicks
scan
	totalpicks = 0
	for i = 1 to 30
		qfield = "day"+alltrim(str(i))
		totalpicks = totalpicks + xpicks.&qfield
	next
	replace totpicks with totalpicks in xpicks

	if totalpicks#0
		replace status with "A" in xpicks

	else
		replace status with "D" in xpicks
	
		if seek(str(xpicks.accountid,4)+".T."+xpicks.style+xpicks.color+xpicks.id+"1","invenloc","match") and invenloc.locqty>0

			xsqlexec("select style, color, id from outdet, outship " + ;
				"where outship.mod='"+goffice+"' and outship.outshipid=outdet.outshipid " + ;
				"and style='"+xpicks.style+"' and color='"+xpicks.color+"' and id='"+xpicks.id+"' " + ;
				"and outship.wo_date>{"+dtoc(gomonth(date(),-2))+"} and notonwip=0","xdy1",,"wh")
										
			if reccount()>0
				replace status with "N" in xpicks
			else
				xsqlexec("select style, color, id from indet, inwolog where inwolog.inwologid=indet.inwologid and " + ;
					"inwolog.mod='"+goffice+"' and style='"+xpicks.style+"' and color='"+xpicks.color+"' and id='"+xpicks.id+"' and " + ;
					"date_rcvd>{"+dtoc(gomonth(date(),-3))+"} and unbilled=.f.","xdy2",,"wh")
					
				if reccount()>0		
					replace status with "N" in xpicks
				endif
			endif
		endif
	endif
endscan

*

if goffice="7"
	replace end with datetime(), ztime with end-start in stupid
	insert into stupid (step, start) values ("UPD LOC", datetime())
endif

wait window at 10,10 "Updating the Style Locations........" nowait

select xpicks
scan
	if upcmastsql(accountid,style,color,id)
		if !upcmast.pnp
			delete in xpicks
			loop
		else
			replace frtype with upcmast.frtype in xpicks
		endif
	endif
	
	picksset()
endscan

* now compute the SKAC

if goffice="7"
	replace end with datetime(), ztime with end-start in stupid
	insert into stupid (step, start) values ("SCAC", datetime())
endif

wait window at 10,10 "Computing the SKAC........" nowait

max_per_day = 0.0
max_days_shipped=0.0

select xpicks
scan
	days_shipped= 0.0
	totshipped =0.0

	for i = 1 to 30
		qfield = "day"+alltrim(str(i))
		if xpicks.&qfield > 0
			days_shipped = days_shipped+1
			totshipped = totshipped+ xpicks.&qfield
		endif
	next
	replace dayshipped with days_shipped in xpicks

	if days_shipped > 0
		replace perday with totshipped/days_shipped in xpicks
	endif
	if xpicks.perday > max_per_day
		max_per_day = xpicks.perday
	endif
	if xpicks.dayshipped > max_days_shipped
		max_days_shipped = days_shipped
	endif
endscan

*

wait window at 10,10 "Computing the dayspicked and picks per day data........." nowait

select xpicks
scan
	if xpicks.dayshipped > 0
		dayfactor = (xpicks.dayshipped*1.0)/(max_days_shipped*1.0)
	else
		dayfactor =0.0
	endif

	if xpicks.perday = 0
		replace skac with 0 in xpicks
	else
		replace skac with (xpicks.perday/max_per_day)*10.0*dayfactor in xpicks
		replace skac with (xpicks.perday/30)*10.0*dayfactor in xpicks
	endif
endscan

*

if goffice="7"
	replace end with datetime(), ztime with end-start in stupid
	insert into stupid (step, start) values ("UPD LOC2", datetime())
endif

Wait Window At 10,10 "Updating the Style Locations........" Nowait

select xpicks
scan
	select upcloc
	locate for str(accountid,4)+style+color+id=str(xpicks.accountid,4)+xpicks.style+xpicks.color+xpicks.id
	if found()
		replace loc with upcloc.whseloc in xpicks
	endif
endscan

*

wait window at 10,10 "Updating the location quantity........" nowait

select xpicks
scan
	select invenloc
	locate for style=xpicks.style and color=xpicks.color and id=xpicks.id and invenloc.whseloc=xpicks.loc
	if found()
		replace locqty with invenloc.locqty in xpicks
	endif
endscan

* dimensions

if goffice="7"
	replace end with datetime(), ztime with end-start in stupid
	insert into stupid (step, start) values ("DIMS", datetime())
endif

select xpicks
scan 
	select ;
		length, width, depth, ctncube, val(pack) as valpack ;
		from inven ;
		where !units and style=xpicks.style and color=xpicks.color and id=xpicks.id and totqty>0 ;
		order by 5 descending ;
		into cursor xtemp
		
	if reccount("xtemp")>0
		replace length with xtemp.length, ;
				width with xtemp.width, ;
				depth with xtemp.depth, ;
				ctncube with xtemp.ctncube in xpicks
	endif
endscan

* yet to pick

if goffice="7"
	replace end with datetime(), ztime with end-start in stupid
	insert into stupid (step, start) values ("TUSHIES", datetime())
endif

wait window at 10,10 "Finishing tushies........." nowait

select style, color, id, sum(beenpicked), sum(topick), sum(needtopick) from apicks group by style, color, id into cursor needs readwrite
replace all sum_needtopick with sum_topick - sum_beenpicked
replace sum_needtopick with 0 for sum_needtopick < 0

select needs
scan
	if seek(style+color+id,"xpicks","stylecolid")
		replace yettopick with needs.sum_needtopick in xpicks
		if needs.sum_needtopick>0 and inlist(xpicks.status,"D","N")
			replace status with "A" in xpicks
		endif
	endif
endscan

select pt
scan for !finis
	select ptdet
	scan for ptid=pt.ptid
		if seek(style+color+id,"xpicks","stylecolid")	
			replace ptqty with xpicks.ptqty+ptdet.totqty, status with "A" in xpicks
		endif
	endscan
endscan

xsqlexec("select outwolog.outwologid, outwolog.wo_num from outwolog, outship " +;
	"where outwolog.mod='"+goffice+"' and outwolog.outwologid=outship.outwologid " + ;
	"and outship.notonwip=0 and del_date={} and cartonized=0","xdytemp",,"wh")

select outwologid, wo_num from xdytemp group by 1 into cursor xrpt

scan 	
	xsqlexec("select * from outdet where outwologid="+transform(xrpt.outwologid)+" and units=1",,,"wh")
	scan
		if seek(outdet.style+outdet.color+outdet.id,"xpicks","stylecolid")
			replace yettopick with xpicks.yettopick + outdet.totqty in xpicks
			if inlist(xpicks.status,"D","N")
				replace status with "A" in xpicks
			endif
		endif
	endscan
endscan

select xpicks
replace all openpicks with yettopick+ptqty

*

if used("picks")
	use in picks
endif

useca("picks","wh",,,"mod='"+goffice+"'")
delete all in picks

select xpicks
export to sortedpicks.xls type xls

scan 
	scatter memvar memo
	m.office=gmasteroffice
	m.perday=min(99999,m.perday)
	m.skac=min(9999,m.skac)
	m.mod=goffice
	insert into picks from memvar
endscan

tu("picks")

*

use in pt
use in ptdet

if used("indet")
	use in indet
endif
if used("inwolog")
	use in inwolog
endif

use in alreadypicked
use in tobepicked

if !xfromslotscreen
	if used("outdet")
		use in outdet
	endif
	if used("outship")
		use in outship
	endif
	if used("outwolog")
		use in outwolog
	endif
	use in upcloc
	use in inven
	use in invenloc
	use in picks
endif

if goffice="7"
	replace end with datetime(), ztime with end-start in stupid
	use in stupid
endif
