**This scriptwill identify files not FTP'd successfully
utilsetup("MJ_FTP_FILES_CONTROL")
Set Exclusive Off
Set Deleted On
Set Talk Off
Set Safety Off
With _Screen
	.AutoCenter = .T.
	.WindowState = 0
	.BorderStyle = 1
	.Width = 320
	.Height = 210
	.Top = 290
	.Left = 110
	.Closable = .T.
	.MaxButton = .T.
	.MinButton = .T.
	.ScrollBars = 0
	.Caption = "MJ_FTP_FILES_CONTROL"
Endwith


If !Used("edi_trigger")
	Use F:\3pl\Data\edi_trigger Shared In 0
Endif

If !Used("ftplog")
	Use F:\wh\ftplog Shared In 0
ENDIF

Select * From ftplog Where dd>=Date()-20 and 'Sent' $(logdata) AND 'mj' $(logdata)  Into Cursor mjftplog
Use In ftplog

SET STEP ON 
**use ATC to replace substr
Select Substr(file945,54,22) As trig_945 From edi_trigger Where Inlist(accountid,6303,6304,6305,6320,6321,6322,6323,6543) And edi_type='945 ' And WHEN_proc > Date()-2;
	and WHEN_proc<Datetime()-6000 AND office ='J' Into Cursor lookup945nj_2 READWRITE 
	DELETE FOR EMPTY(trig_945) IN lookup945nj_2
	
Select Distinct trig_945 From lookup945nj_2 Where !Empty(trig_945) Into Cursor lookup945nj READWRITE 
Select *, SPACE(40) as file945 From mjftplog Where 'Sent file' $logdata And 'mj_wholesale\out\945j' $logdata And '.txt successfully' $logdata Into Cursor log945nj READWRITE 
scan
pos1=ATC('\945',logdata)
replace file945 with substr(logdata,pos1+1,35) 
endscan
SELECT * FROM  lookup945nj a LEFT JOIN  log945nj b  ON a.trig_945=b.file945 INTO CURSOR  compare945nj READWRITE
Select trig_945 From compare945nj Where Isnull(file945)Into Cursor mj_missing945nj Readwrite
*******copy missing file to be resent
Select mj_missing945nj
Scan
	xfilename=Alltrim(trig_945)

	Copy File F:\FTPUSERS\MJ_Wholesale\WMS_Outbound\945Out\Archive\&xfilename   To  F:\FTPUSERS\MJ_Wholesale\Out\HOLD\&xfilename
Endscan
**************Mira Loma
Select Substr(file945,52,22) As trig_945 From edi_trigger Where Inlist(accountid,6303,6304,6305,6320,6321,6322,6323,6543) And edi_type='945 ' And WHEN_proc > Date()-2;
	and WHEN_proc<Datetime()-4000 AND office ='L' Into Cursor lookup945ca_2 READWRITE 
Select Distinct trig_945 From lookup945ca_2 Where !Empty(trig_945) Into Cursor lookup945ca READWRITE 
Select *, SPACE(40) as file945 From mjftplog Where 'Sent file' $logdata And 'mj_wholesale_ca\out\945c' $logdata And '.txt successfully' $logdata Into Cursor log945ca READWRITE 
scan
pos1=ATC('\945',logdata)
replace file945 with substr(logdata,pos1+1,35) 
endscan

SELECT * FROM  lookup945ca a LEFT JOIN  log945ca b  ON a.trig_945=b.file945 INTO CURSOR  compare945ca READWRITE 
Select trig_945 From compare945ca Where Isnull(file945)Into Cursor mj_missing945ca Readwrite

*******copy missing file to be resent
Select mj_missing945ca
Scan
	xfilename=Alltrim(trig_945)

	Copy File F:\FTPUSERS\MJ_Wholesale_CA\WMS_Out\945Out\Archive\&xfilename   To  F:\FTPUSERS\MJ_Wholesale_CA\OUT\HOLD\&xfilename
Endscan



Select * FROM mj_missing945nj UNION select * FROM mj_missing945ca INTO CURSOR MISSING_945_FTP READWRITE 
If Reccount() > 0
	Export To "S:\MarcJacobsData\TEMP\MISSING_945_FTP"  Type Xls
	tsendto = "tmarg@fmiint.com"
	tsendto = "tmarg@fmiint.com,todd.margolin@tollgroup.com"
	tattach = "S:\MarcJacobsData\TEMP\MISSING_945_FTP.xls"
	tcc =""
	tFrom ="Toll EDI System Operations <fmi-transload-ops@fmiint.com>"
	tmessage = "missing 945s from FTPlog "+Ttoc(Datetime())+"    Todd check HOLD folder"
	tSubject = "Missing 945s from FTPlog"
	Do Form m:\dev\frm\dartmail2 With tsendto,tFrom,tSubject, tcc,tattach,tmessage  ,"A"
Else
	tsendto = "tmarg@fmiint.com,J.EDOUARD@marcjacobs.com,S.LARA@marcjacobs.com"
	tattach = ""
	tcc =""
	tFrom ="Toll EDI System Operations <fmi-transload-ops@fmiint.com>"
	tmessage = "NO_Missing successful 945 FTP "+Ttoc(Datetime())
	tSubject = "NO_Missing successful 945 FTP_exist"
	Do Form m:\dev\frm\dartmail2 With tsendto,tFrom,tSubject, tcc,tattach,tmessage  ,"A"
Endif

*************************************************************   944 CHECK
*!*	xcount=1
*!*	SELECT *,'0000000' as wo_num FROM mjftplog WHERE '.944' $(logdata) AND 'Sent' $(logdata) AND 'mj_wholesale' $(logdata)  INTO CURSOR v944 READWRITE
*!*	COUNT TO tt
*!*	For xcount = 1 TO tt
*!*	pos1=ATC('_0',logdata)
*!*	REPLACE wo_num with substr(logdata,pos1-8,7) for wo_num='0000000' IN v944
*!*	xcount=xcount+1
*!*	endfor

*!*	SELECT distinct(wo_num) as wo_num FROM inwolog WHERE confirmdt>=DATE()-20 AND !zreturns AND 'FILE943*OUT_943' $(comments) INTO CURSOR vinwo READWRITE

*************************************************************   944 FTP CHECK
SELECT WO_NUM,TRANSFORM(wo_num)+'_'+TRANSFORM(bol) as tt1, SPACE(5) as status FROM edi_trigger WHERE edi_type='944' AND INLIST(accountid,6303,6325,6543) AND fin_status='944 C' AND when_proc >DATE()-5 AND when_proc <DATETIME()-1000 INTO CURSOR vtrig1 READWRITE
Select vtrig1                                       
Scan                                            
SELECT mjftplog                                 
 LOCATE FOR ALLTRIM(vtrig1.tt1) $(mjftplog.logdata)             
 IF FOUND("mjftplog")                            
replace status with 'FOUND' IN vtrig1
 Else                                           
 EndIf                                          
ENDSCAN
SELECT WO_NUM FROM  VTRIG1 WHERE STATUS !='FOUND'  INTO CURSOR vmiss944 READWRITE 
If Reccount() > 0
	Export To "S:\MarcJacobsData\TEMP\MISSING_944_FTP"  Type Xls
	tsendto = "tmarg@fmiint.com"
	tsendto = "tmarg@fmiint.com,todd.margolin@tollgroup.com"
	tattach = "S:\MarcJacobsData\TEMP\MISSING_944_FTP.xls"
	tcc =""
	tFrom ="Toll EDI System Operations <fmi-transload-ops@fmiint.com>"
	tmessage = "missing 944 from FTPlog "+Ttoc(Datetime())+"    Todd check HOLD folder"
	tSubject = "Missing 944 from FTPlog"
	Do Form m:\dev\frm\dartmail2 With tsendto,tFrom,tSubject, tcc,tattach,tmessage  ,"A"
Else
*!*		tsendto = "tmarg@fmiint.com,S.LARA@marcjacobs.com"
*!*		tattach = ""
*!*		tcc =""
*!*		tFrom ="Toll EDI System Operations <fmi-transload-ops@fmiint.com>"
*!*		tmessage = "NO_unsuccessful 944 FTP "+Ttoc(Datetime())
*!*		tSubject = " NO_unsuccessful 944 FTP exist"
*!*		Do Form m:\dev\frm\dartmail2 With tsendto,tFrom,tSubject, tcc,tattach,tmessage  ,"A"
Endif


Close Data All
schedupdate()
_Screen.Caption=gscreencaption



