if !inlist(xoffice," ","N","C")
	return .f.
endif

if empty(xaccountid)
	xtables="inbound"
else
	xtables="inbound, bl"
endif

select ;
	inbound.wo_num, ;
	deliverdt as wo_date, ;
	iif(inbound.office="N","NJ","CA") as office, ;
	"Z" as source, ;
	"OB" as type, ;
	inbound.accountid, ;
	iif(empty(xaccountid),inbound.acctname,padr(acctname(xaccountid),30)) as acctname, ;
	iif(len(trim(pu_trlr))<8,padr(pu_trlr,12),dispcont(pu_trlr)) as reference, ;
	revenue, ;
	left(strtran(strtran(comments,chr(13)," "),chr(10)," "),100) as comments ;
	from &xtables ;
	where empty(invnum) ;
	and !unbilled ;
	and &xob2consfilter1 ;
	and &xob2consfilter2 ;
	and &xob2consfilter3 ;
	and !emptynul(deliverdt) ;
	and inbound.pu_date>{1/1/2007} ;
	group by 1 ;
	into cursor xtemp

select xtemp
scan 
	scatter memvar
	insert into xrpt from memvar
endscan
