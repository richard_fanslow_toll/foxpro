lparameters xinitwonum, xbkdn, xconfirming, xmanifesting, xmodal

if empty(xinitwonum)
	xinitwonum=0
endif

xdy = ascan(oforms.iaforminstances,"WO2",1,alen(oforms.iaforminstances,1),1,6)

if xdy=0
*	xparm = transform(xinitwonum)+","+transform(xbkdn)+","+transform(xconfirming)+","+transform(xmanifesting)+","+transform(xmodal)
	osource = oforms.doform("WO2",'F',.f.)
else
	xdy = asubscript(oforms.iaforminstances,xdy,1)
	osource = oforms.iaforminstances[xdy,1]
endif

if osource.getpprop("icMode")="DEFAULT"
	osource.zzsetupscreen(xinitwonum, xbkdn, xconfirming, xmanifesting, xmodal)
	osource.pgfx.page1.ctrlist.txtgetwono.value=trans(xinitwonum,"@Z 9999999")
	osource.zzgetname()
	osource.show()
else
	x3winmsg("Can't proceed - you must save or cancel your changes first.")
endif
