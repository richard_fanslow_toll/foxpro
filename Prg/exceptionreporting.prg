***********************************************************************
* Program....: EXCEPTIONREPORTING.PRG
* Date.......: 25 July 06
* Notice.....: Copyright (c) 2006 Kevin R. Loder
* Compiler...: Visual FoxPro 09.00.0000.2412 for Windows 
* Purpose....: sends an email message when a try/catch is triggered
***********************************************************************
 
lPARAMETERS pMessage,plcQuery

DO case
   case PCOUNT()=0
     tmessage="Cartonize error"
   case PCOUNT()=1
     tMessage=pMessage
    CASE  PCOUNT()=3
   IF pMessage=''
        tmessage="Cartonize error"
    ELSE
     tMessage=pMessage
    ENDIF
 endcase    
 
     
pterror = .t.
 tsubject = "Pick Pack Error ("+transform(oerr.errorno)+") at "+ttoc(datetime())

 tmessage =tmessage+ chr(13)+;
    [  Error: ] + str(oerr.errorno) +chr(13)+;
    [  LineNo: ] + str(oerr.lineno) +chr(13)+;
    [  Message: ] + oerr.message +chr(13)+;
    [  Procedure: ] + oerr.procedure +chr(13)+;
    [  Details: ] + oerr.details +chr(13)+;
    [  StackLevel: ] + str(oerr.stacklevel) +chr(13)+;
    [  LineContents: ] + oerr.linecontents+chr(13)+;
    [  Current User: ] + guserid+chr(13)+;
    [  UserValue: ] + oerr.uservalue+chr(13)
* IIF(PCOUNT()=0,'',[  Query String: ]+lcquery)
tsubject = "Cartonization Error "+ttoc(datetime())
tattach  = ""
 tcc=""
 tsendto = "pgaidis@fmiint.com"


 tfrom    ="TGF WMS Pick Pack System Operations <tgf-transload-ops@fmiint.com>"
do form dartmail2 with tsendto,tfrom,tsubject,tcc,tattach,tmessage,"A"

 * tsendto = "kloder@fmiint.com,pgaidis@fmiint.com"
