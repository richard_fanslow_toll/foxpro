parameters xparam

if set("default") # "M"		&& don't update daysched if running in the development environment 
	**close the table if already used - mvw 01/25/18
	if used("daysched")
		use in daysched
	endif

*made this a try/catch because of this strange error   dy 3/13/18
*Connectivity error: [Microsoft][SQL Server Native Client 10.0][SQL Server]Conversion failed when converting date and/or time from character string. in daysched

	xfailed=.f.
	
	try
		useca("daysched","bt",,,"zdate={"+dtoc(date())+"} and projname='"+padr(gsystemmodule,30)+"' and disable=0",,,,,"schedendtime")
		
		select daysched
		xxfilter=iif(empty(xparam),".t."," parm = '"+xparam+"'")
		locate for empty(actualend) and !notified and &xxfilter
		
		if !found() and getenv("COMPUTERNAME")#"MBENNETT"
			scatter memvar blank
			m.projname=gsystemmodule
			m.zdate=date()
			m.actualend=time2chr(time())
			insertinto("daysched","bt",.t.)
		else
			replace actualend with time2chr(time()) in daysched
		endif
		
		tu("daysched")
	catch
		xfailed=.t.
	endtry
	
	if xfailed	&& strange that I don't get this email  dy 4/18/18
		email("Dyoung@fmiint.com","INFO: daysched error in "+gsystemmodule,,,,,.t.,,,,,.t.,,.t.)
	endif
endif
