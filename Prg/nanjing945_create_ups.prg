*!* NANJING USA 945 (Whse. Shipping Advice) Creation Program
*!* Creation Date: 06.01.2005 by Joe
*!* Last update: 08.01.2005 by Joe

PARAMETERS cBOL,nWO_Num,cShip_ref,cOffice

TRY
	PUBLIC ARRAY thisarray(1)
	PUBLIC cSCCNumber,nSCCSuffix,cCustname,nUnitSum,cFreight,tsendto,tsendtoerr,tcc,tccerr,nTrkRecs,lDoSQL,cErrMsg,lTesting,lDoCatch
	PUBLIC cMod,cMBOL,cFilenameShort,cFilenameOut,cFilenameArch,cWO_NumList,lcPath,cEDIType,cISA_Num,cProgName
	PUBLIC lcArchivePath,lcHoldPath,lOverflow
	cProgName = "nanjing945_create_ups.prg"

	lTesting = .F.
	lTestInput = .F. && lTesting

	DO m:\dev\prg\_setvars WITH lTesting
	ON ESCAPE CANCEL

	lIsError = .F.
	lDoCatch = .T.
	lCloseOutput = .T.
	nFilenum = 0
	lPrepack = .T.
	lDoSQL = .T.
	tfrom = "Toll EDI Operations <toll-edi-ops@tollgroup.com>"
	cErrMsg = ""
	nSCCSuffix = 0
	nFreight = 0
	cFreight = ""
	nAcctNum = 4694
	cPPName = "Nanjing"
	lOverflow = .f.

	cShip_ref = ""
	IF TYPE("cOffice") = "L"
		IF !lTesting
			lCloseOutput = .F.
			WAIT WINDOW "Office not provided...terminating" TIMEOUT 3
			cErrMsg = "No OFFICE"
			DO ediupdate WITH cErrMsg,.T.
			THROW
		ELSE
			CLOSE DATABASES ALL
			nWO_Num =  4045828
			cBOL = "927564571716817"
			cOffice = "C"
			cShip_ref = "1930420000"
		ENDIF
	ENDIF

	cMBOL = ""
	cEDIType = "945"
	lParcelType = .T.
	cBOL = ALLTRIM(cBOL)
	cMod = IIF(cOffice = "C","1",cOffice)
	gMasterOffice = IIF(cOffice = "I","N",cOffice)
	gOffice = cMod

	IF !lTesting
		CREATE CURSOR temp945 (accountid i,isa_num c(10),st_num c(10),wo_num i,bol_no c(20),ship_ref c(20),filename c(50),filedate T)
	ENDIF

	IF TYPE("nWO_Num")<> "N"
		lCloseOutput = .F.
		cErrMsg = "BAD WO#"
		DO ediupdate WITH cErrMsg,.T.
		THROW
	ENDIF
	cWO_Num = ALLTRIM(STR(nWO_Num))
	cWO_NumList = cWO_Num

	lEmail = .T.
	lTestMail = lTesting && Sends mail to Joe only
	lStandalone = lTesting
	cUseFolder = "F:\WHY\WHDATA\"
	IF lTestInput
		cUseFolder="F:\WHP\WHDATA\"
	ELSE
		xReturn = "XXX"
		DO m:\dev\prg\wf_alt WITH cOffice,nAcctNum
		cUseFolder = UPPER(xReturn)
	ENDIF
	DO m:\dev\prg\swc_cutctns WITH cBOL

*!*	lTestMail = .t.

	IF !lTesting
		IF !USED('nanjing_wohold')
			USE F:\3pl\DATA\nanjing_wohold IN 0
		ENDIF

		nWO_numOld=nanjing_wohold.wo_num
		IF nWO_Num = nWO_numOld
			lDoSQL = .F.
		ELSE
			REPLACE wo_num WITH nWO_Num IN nanjing_wohold
		ENDIF
	ENDIF

	lPrepack = .T.
	lPick = .F.

	PUBLIC c_CntrlNum,c_GrpCntrlNum
	WAIT WINDOW "Now preparing tables and variables" NOWAIT NOCLEAR

*!* SET CUSTOMER CONSTANTS
	cCustname = "NANJING"  && Customer Identifier
	cX12 = "004050"  && X12 Standards Set used
	cMailName = "Nanjing USA"
	SELECT 0
	USE F:\3pl\DATA\mailmaster ALIAS mm
	LOCATE FOR edi_type = '945' AND mm.office = cOffice AND mm.accountid = nAcctNum
	tsendto = IIF(mm.use_alt,sendtoalt,mm.sendto)
	tcc = IIF(mm.use_alt,mm.ccalt,mm.cc)
	lcPath = ALLTRIM(mm.basepath)
	lcArchivePath = ALLTRIM(mm.archpath)
	lcHoldPath = ALLTRIM(mm.holdpath)

	LOCATE
	LOCATE FOR mm.office = 'X' AND mm.accountid = 9999
	tsendtoerr = IIF(mm.use_alt,sendtoalt,mm.sendto)
	tccerr = IIF(mm.use_alt,mm.ccalt,mm.cc)
	LOCATE
	LOCATE FOR mm.edi_type = 'MISC' AND mm.taskname = 'GENERAL'
	tsendtotest = IIF(mm.use_alt,sendtoalt,mm.sendto)
	tcctest = IIF(mm.use_alt,mm.ccalt,mm.cc)
	LOCATE
	LOCATE FOR mm.edi_type = 'MISC' AND mm.taskname = 'NANQTYERR'
	tsendtoqty = IIF(mm.use_alt,sendtoalt,mm.sendto)
	tccqty = IIF(mm.use_alt,mm.ccalt,mm.cc)

*!* SET OTHER CONSTANTS
	DO CASE
		CASE cOffice = "M"
			cCustLoc =  "FL"
			cFMIWarehouse = ""
			cCustPrefix = "945"+"f"
			cDivision = "Florida"
*		cFolder = "WHM"
			cSF_Addr1  = "11400 NW 32ND AVE"
			cSF_CSZ    = "MIAMI*FL*33167"

		CASE cOffice = "N"
			cCustLoc =  "NJ"
			cFMIWarehouse = ""
			cCustPrefix = "945"+"j"
			cDivision = "New Jersey"
*		cFolder = "WHN"
			cSF_Addr1  = "800 FEDERAL BLVD"
			cSF_CSZ    =  "CARTERET*NJ*07008"

		OTHERWISE
			cCustLoc =  "C2"
			cFMIWarehouse = ""
			cCustPrefix = "945"+"y"
			cDivision = "Carson2"
*		cFolder = "WHC"
			cSF_Addr1  = "1000 E 223RD ST"
			cSF_CSZ    = "CARSON*CA*90745"
	ENDCASE
	cCustFolder = UPPER(cCustname)  && +"-"+cCustLoc
	lNonEDI = .F.  && Assumes not a Non-EDI 945
	lDoPAL = .T.
	STORE "" TO lcKey
	STORE 0 TO alength,nLength

*!* SET OTHER CONSTANTS
	dt1 = TTOC(DATETIME(),1)
	dtmail = TTOC(DATETIME())
	dt2 = DATETIME()
	cString = ""

	csendqual = "ZZ"
	csendid = "FMIF"
	crecqual = "01"
	crecid = "654351030"

	cfd = CHR(0x07)
	csegd = "~"
	nSegCtr = 0
	cterminator = ">" && Used at end of ISA segment
	cdate = DTOS(DATE())
	ctruncdate = RIGHT(cdate,6)
	ctime = LEFT(TIME(DATE()),2)+SUBSTR(TIME(DATE()),4,2)
	cfiledate = cdate+ctime
	cOrig = "J"
	nSTCount = 0
	cStyle = ""
	cPTString = ""
	nTotCtnWt = 0
	nTotCtnCount = 0
	nUnitSum = 0
	cTargetStyle = ""

** PAD ID Codes
	csendidlong = PADR(csendid,15," ")
	crecidlong = PADR(crecid,15," ")

	lcPath = IIF(lTesting,("F:\FTPUSERS\Nanjing\945out\test\"),lcPath)
	cFilenameHold = (lcHoldPath+cCustPrefix+dt1+".txt")
	cFilenameShort = JUSTFNAME(cFilenameHold)
	cFilenameArch = UPPER(lcArchivePath+cCustPrefix+dt1+".txt")
	cFilenameOut = (lcPath+cFilenameShort)
	nFilenum = FCREATE(cFilenameHold)

	IF USED('outship')
		USE IN outship
	ENDIF
	IF USED('outdet')
		USE IN outdet
	ENDIF

	xsqlexec("select * from outship where accountid = "+TRANSFORM(nAcctNum)+" and bol_no ='"+cBOL+"'",,,"wh")
	SELECT outship
	IF RECCOUNT()=0
		cErrMsg = "No records for BOL# "+cBOL
		DO ediupdate WITH cErrMsg,.T.
		THROW
	ENDIF
	INDEX ON bol_no TAG bol_no
	INDEX ON outshipid TAG outshipid
	INDEX ON wo_num TAG wo_num

	selectoutdet()
	SELECT outdet
	INDEX ON outdetid TAG outdetid

	SELECT outdet
	LOCATE

	IF !USED("scacs")
		xsqlexec("select * from scac","scacs",,"wh")
		INDEX ON scac TAG scac
	ENDIF

	IF USED('SHIPMENT')
		USE IN SHIPMENT
	ENDIF

	IF USED('PACKAGE')
		USE IN PACKAGE
	ENDIF

	csq1 = [select * from shipment where accountid = ]+TRANSFORM(nAcctNum)
	xsqlexec(csq1,,,"wh")
	INDEX ON shipmentid TAG shipmentid
	INDEX ON pickticket TAG pickticket

	csq1 = [select * from package where accountid = ]+TRANSFORM(nAcctNum)
	xsqlexec(csq1,,,"wh")
	INDEX ON shipmentid TAG shipmentid
	SET ORDER TO TAG shipmentid

	IF USED('outwolog')
		USE IN outwolog
	ENDIF

*!*	Added this code block to use SQL UCC data (did away with SCC# per Nanjing), 11.18.2008
	IF lDoSQL
		cRetMsg = "X"
		DO m:\dev\prg\sqlconnectnanjing_wo  WITH nWO_Num,nAcctNum,cCustname,cPPName,.T.,cOffice && Amended for UCC number sequencing	IF cRetMsg<>"OK"
		IF cRetMsg<>"OK"
			cErrMsg = cRetMsg
			DO ediupdate WITH cErrMsg,.T.
			THROW
		ENDIF
	ENDIF

*!* This removes zero-qty cartons from SQL cursor
	SELECT ucc,SUM(totqty) AS uccqty FROM vnanjingpp GROUP BY 1 INTO CURSOR tempchk HAVING uccqty=0
	SELECT tempchk
	LOCATE
	IF !EOF()
		SCAN
			STORE ALLT(ucc) TO cUCC
			SELECT vnanjingpp
			LOCATE
			DELETE FOR ucc = cUCC
		ENDSCAN
	ENDIF
	USE IN tempchk

	SELECT vnanjingpp
	SET RELATION TO outdetid INTO outdet

	SELECT outship
	LOCATE
	LOCATE FOR wo_num = nWO_Num
	xsqlexec("select * from outwolog where accountid = "+TRANSFORM(nAcctNum)+" and outwologid = "+TRANSFORM(outship.outwologid),,,"wh")

	IF outwolog.picknpack = .T.
		lPick = .T.
		lPrepack = .F.
	ELSE
		lPick = .F.
		lPrepack = .T.
	ENDIF
	SELECT outship
	LOCATE

	WAIT CLEAR
	WAIT WINDOW "Now creating Header information" NOWAIT NOCLEAR

*!* HEADER LEVEL EDI DATA
	DO num_incr_isa

	cISA_Num = PADL(c_CntrlNum,9,"0")
	nISA_Num = INT(VAL(cISA_Num))

	STORE "ISA"+cfd+"00"+cfd+"          "+cfd+"00"+cfd+"          "+cfd+csendqual+cfd+csendidlong+cfd+;
		crecqual+cfd+crecidlong+cfd+ctruncdate+cfd+ctime+cfd+"U"+cfd+"00401"+cfd+;
		cISA_Num+cfd+"0"+cfd+IIF(lTesting,"T","P")+cfd+cterminator+csegd TO cString
	DO cstringbreak

	STORE "GS"+cfd+"SW"+cfd+csendid+cfd+crecid+cfd+cdate+cfd+ctime+cfd+c_CntrlNum+;
		cfd+"X"+cfd+cX12+csegd TO cString
	DO cstringbreak

	SELECT outship
	LOCATE

	IF !SEEK(nWO_Num,"outship","wo_num")
		WAIT CLEAR
		SET STEP ON
		WAIT WINDOW "Invalid Work Order Number - Not Found in OUTSHIP!" TIMEOUT 2
		cErrMsg = "WO# NOT FOUND"
		DO ediupdate WITH cErrMsg,.T.
		THROW
	ENDIF

*************************************************************************
*1	PTIDSOURCE/OUTSHIP RECORD MAIN LOOP
*	Loops through individual OUTSHIP lines for BOL#
*************************************************************************

*!* Updated this section to make scan EDI_TRIGGER- vs. OUTSHIP-based unless in Testing, 08/01/05
*!* This allows only PT's which have included BOLs to process, and skips unshipped or unmarked BOLs in OUTSHIP.

*!*		IF !lTesting
*!*			SELECT edi_trigger
*!*			scanstr = "edi_trigger.wo_num = nWO_Num and !processed"
*!*		ELSE
*!*			SELECT outship

	scanstr = "outship.bol_no = cBOL and accountid = 4694"

*!*		ENDIF

	LOCATE
	SET ORDER TO
	WAIT CLEAR
	WAIT WINDOW "Now creating BOL-based information..." NOWAIT NOCLEAR

	nqqq = 0
	SELECT outship
	LOCATE
	SCAN FOR &scanstr
		nqqq = nqqq+1
		WAIT WINDOW "LOOP "+TRANSFORM(nqqq) NOWAIT
		WAIT WINDOW "" TIMEOUT 2
		IF lTesting
			LOCATE FOR ship_ref = cShip_ref AND accountid=4694
			SCATTER MEMVAR
		ELSE
			LOCATE FOR ship_ref = TRIM(edi_trigger.ship_ref) AND accountid=4694
		ENDIF
		IF !FOUND() AND !lTesting
			WAIT WINDOW "No such PT: "+TRIM(edi_trigger.ship_ref) TIMEOUT 2
			SET STEP ON
			SELECT edi_trigger
			LOCATE
			REPLACE fin_status WITH "MISSING PT#",processed WITH .T.  FOR edi_trigger.bol = cBOL ;
				AND edi_trigger.ship_ref = cShip_ref AND INLIST(edi_trigger.accountid,4610,4694)
			SELECT edi_trigger
			LOOP
		ENDIF

		cBOL = outship.bol_no
		cShip_ref = outship.ship_ref
		IF " OV"$cShip_ref
			LOOP
		ENDIF

		lWalMart = IIF(("WALMART"$UPPER(m.consignee)) OR ("WAL-MART"$UPPER(m.consignee)),.T.,.F.)

		IF EMPTY(cBOL)  AND !lTesting && May be empty if triggered from PT screen, or FedEx shipment
			WAIT CLEAR
			WAIT WINDOW "MISSING BOL# at PT# "+cShip_ref+"!" NOWAIT
			SELECT edi_trigger
			REPLACE fin_status WITH "MISSING BOL#",processed WITH .T.  FOR edi_trigger.bol = cBOL ;
				AND edi_trigger.ship_ref = cShip_ref
			SELECT edi_trigger
			LOOP
		ENDIF

		cBOL2 = ALLTRIM(outship.bol_no)
		IF !lTesting
			IF SEEK(cBOL2,'edi_trigger','bol')
				IF edi_trigger.wo_num = outship.wo_num AND edi_trigger.processed
					LOOP
				ENDIF
			ENDIF
		ENDIF

		IF EMPTYnul(outship.del_date) AND !lTesting
			cErrMsg = "EMPTY DEL_DATE"
			DO ediupdate WITH cErrMsg,.T.
			THROW
		ENDIF

		SCATTER MEMVAR MEMO
		cBOL = TRIM(outship.bol_no)

		alength = ALINES(apt,outship.shipins,.T.,CHR(13))
		nOutshipID = outship.outshipid
		nTotCtnCount = m.qty
		cPO_Num = ALLTRIM(outship.cnee_ref)

		IF lTestInput
			cFreight = "22.50"
		ELSE
			SELECT SHIPMENT
			LOCATE
			IF lTesting OR DATE() < {^2011-02-12}
				LOCATE FOR INLIST(SHIPMENT.accountid,4610,4694,9999) AND SHIPMENT.pickticket = cShip_ref
			ELSE
				LOCATE FOR SHIPMENT.accountid = nAcctNum AND SHIPMENT.pickticket = cShip_ref
			ENDIF
			IF FOUND()
				SELECT PACKAGE
				SUM pkgcharge TO nFreight FOR PACKAGE.shipmentid = SHIPMENT.shipmentid
				cFreight = ALLTRIM(STR(nFreight,10,2))
				SELECT trknumber FROM PACKAGE WHERE PACKAGE.shipmentid = SHIPMENT.shipmentid INTO CURSOR temptrknumber
			ENDIF
			SELECT outship
		ENDIF

		IF "!!"$cShip_ref
			cBaseShip_ref = LEFT(cShip_ref,AT("!",cShip_ref,1)-1)
			cShip_ref2 = LEFT(cShip_ref,AT("!",cShip_ref,1)-4)
			nSuffix = INT(VAL(RIGHT(cBaseShip_ref,3)))
			DO CASE
				CASE ("!!D"$cShip_ref)
					cSuffix = PADL(ALLTRIM(STR(nSuffix+4)),3,'0')
				CASE ("!!C"$cShip_ref)
					cSuffix = PADL(ALLTRIM(STR(nSuffix+3)),3,'0')
				CASE ("!!B"$cShip_ref)
					cSuffix = PADL(ALLTRIM(STR(nSuffix+2)),3,'0')
				OTHERWISE
					cSuffix = PADL(ALLTRIM(STR(nSuffix+1)),3,'0')
			ENDCASE
			cShip_refedi = ALLTRIM(cShip_ref2+cSuffix)
		ELSE
			cShip_refedi = ALLTRIM(cShip_ref)
		ENDIF

		cPTString = IIF(EMPTY(cPTString),m.consignee+" "+cShip_ref+" "+cBOL,cPTString+CHR(13)+m.consignee+" "+cShip_ref+" "+cBOL)
		m.CSZ = TRIM(m.CSZ)

		IF EMPTY(M.CSZ)
			WAIT CLEAR
			WAIT WINDOW "No SHIP-TO City/State/ZIP info...exiting" TIMEOUT 2
			cErrMsg = "BAD ADDRESS INFO"
			DO ediupdate WITH cErrMsg,.T.
			THROW
		ENDIF

		cCity = segmentget(@apt,"CITY",alength)
		cState = segmentget(@apt,"STATE",alength)
		cZip = segmentget(@apt,"ZIPCODE",alength)

		IF EMPTY(cCity) OR EMPTY(cState) OR EMPTY(cZip)
			m.CSZ = ALLTRIM(STRTRAN(m.CSZ,"  "," "))
			nSpaces = OCCURS(" ",ALLTRIM(m.CSZ))
			nCommaPos = AT(",",m.CSZ)
			nLastSpace = AT(" ",m.CSZ,nSpaces)

			IF nSpaces > 0
				IF ISALPHA(SUBSTR(TRIM(m.CSZ),AT(" ",m.CSZ,nSpaces-1)+1,2))
					cCity = LEFT(TRIM(m.CSZ),AT(",",m.CSZ)-1)
					cState = SUBSTR(m.CSZ,nCommaPos+2,2)
					cZip = TRIM(SUBSTR(m.CSZ,nLastSpace+1))
				ELSE
					WAIT CLEAR
					WAIT WINDOW "NOT ALPHA: "+SUBSTR(TRIM(m.CSZ),AT(" ",m.CSZ,nSpaces-1)+1,2) TIMEOUT 3
					cCity = LEFT(TRIM(m.CSZ),AT(",",m.CSZ)-1)
					cState = SUBSTR(TRIM(m.CSZ),AT(" ",m.CSZ,nSpaces-2)+1,2)
					cZip = TRIM(SUBSTR(m.CSZ,nLastSpace+1))
				ENDIF
			ENDIF
		ENDIF

		nCtnNumber = 1  && Begin sequence count

		DO num_incr_st
		WAIT CLEAR
		WAIT WINDOW "Now creating Line Item information" NOWAIT NOCLEAR

		IF !lTesting
			INSERT INTO temp945 (accountid,isa_num,st_num,wo_num,bol_no,ship_ref,filename,filedate) ;
				VALUES (m.accountid,c_CntrlNum,c_GrpCntrlNum,m.wo_num,m.bol_no,m.ship_ref,cFilenameShort,dt2)
		ENDIF

		STORE "ST"+cfd+"945"+cfd+PADL(c_GrpCntrlNum,9,"0")+csegd TO cString
		DO cstringbreak
		nSTCount = nSTCount + 1
		nSegCtr = 1

		cShipmentID = ALLTRIM(cWO_Num)
		STORE "W06"+cfd+"N"+cfd+cShip_refedi+cfd+cdate+cfd+TRIM(cShipmentID)+cfd+TRIM(cWO_Num)+cfd+cPO_Num+csegd TO cString
		DO cstringbreak
		nSegCtr = nSegCtr + 1

		cStoreNum = segmentget(@apt,"STORENUM",alength)
		IF EMPTY(cStoreNum)
			cStoreNum = ALLTRIM(m.dcnum)
		ENDIF
		STORE "N1"+cfd+"ST"+cfd+ALLTRIM(m.consignee)+cfd+"92"+cfd+cStoreNum+csegd TO cString
		DO cstringbreak
		nSegCtr = nSegCtr + 1

		STORE "N9"+cfd+"BM"+cfd+TRIM(cBOL)+csegd TO cString
		DO cstringbreak
		nSegCtr = nSegCtr + 1

		cInvoice = IIF(EMPTY(ALLTRIM(m.keyrec)),ALLTRIM(m.ship_ref),ALLTRIM(m.keyrec))
		STORE "N9"+cfd+"CN"+cfd+TRIM(cInvoice)+csegd TO cString
		DO cstringbreak
		nSegCtr = nSegCtr + 1

		cMR = segmentget(@apt,"MR",alength)
		STORE "N9"+cfd+"MR"+cfd+TRIM(cMR)+csegd TO cString
		DO cstringbreak
		nSegCtr = nSegCtr + 1

		STORE "N9"+cfd+"DP"+cfd+TRIM(outship.dept)+csegd TO cString
		DO cstringbreak
		nSegCtr = nSegCtr + 1

		STORE "N9"+cfd+"IA"+cfd+TRIM(outship.vendor_num)+csegd TO cString
		DO cstringbreak
		nSegCtr = nSegCtr + 1

		ddel_date = outship.del_date
		IF EMPTY(ddel_date)
			IF lTesting
				ddel_date = DATE()
			ELSE
				cErrMsg = "MISSING DELDATE"
				DO ediupdate WITH cErrMsg,.T.
				THROW
			ENDIF
		ENDIF

		dapptnum = IIF(EMPTY(outship.appt_num),"UNK",outship.appt_num)
		dapptdate = outship.appt
		IF EMPTY(dapptdate)
			dapptdate = outship.del_date
		ENDIF

		STORE "N9"+cfd+"AO"+cfd+dapptnum+csegd TO cString
		DO cstringbreak
		nSegCtr = nSegCtr + 1

		cUseTime = "153000"
		STORE "G62"+cfd+"11"+cfd+DTOS(ddel_date)+cfd+"9"+cfd+cUseTime+csegd TO cString  && Ship date
		DO cstringbreak
		nSegCtr = nSegCtr + 1

		SELECT scacs
		IF lTesting AND EMPTY(m.scac)
			STORE "FMIX" TO m.scac
		ELSE
			IF !EMPTY(TRIM(outship.scac))
				STORE outship.scac TO m.scac
			ELSE
				cErrMsg = "MISSING SCAC"
				DO ediupdate WITH cErrMsg,.T.
				THROW
			ENDIF
		ENDIF

		SELECT outship

		cShipType="U"
		STORE "W27"+cfd+cShipType+cfd+TRIM(m.scac)+cfd+TRIM(m.SHIP_VIA)+csegd TO cString
		DO cstringbreak
		nSegCtr = nSegCtr + 1

		IF !EMPTY(cFreight) AND !lWalMart
			STORE "G72"+cfd+"516"+cfd+"15"+REPLICATE(cfd,6)+ALLT(cFreight)+csegd TO cString
			DO cstringbreak
			nSegCtr = nSegCtr + 1
			cFreight = ""
		ENDIF

*************************************************************************
*2	OUTDET LOOP - MATCHING OUTSHIPID FIELD (FROM OUTSHIP)
*************************************************************************
		WAIT CLEAR
		WAIT WINDOW "Now creating Detail information "+cShip_ref NOWAIT NOCLEAR
*		ASSERT .F. MESSAGE "In detail creation loop"
		SELECT outdet

		scanstr = "outdet.outshipid = outship.outshipid"
		SELECT vnanjingpp
		LOCATE
		LOCATE FOR ship_ref = TRIM(cShip_ref) AND outshipid = nOutshipID

		IF !FOUND()
			IF !lTesting
				WAIT WINDOW "PT "+cShip_ref+" NOT FOUND in VNANJINGPP...ABORTING" TIMEOUT 2
				IF !lTesting
					cErrMsg = "MISS SQL PT: "+cShip_ref
					DO ediupdate WITH cErrMsg,.T.
				ENDIF
				THROW
			ELSE
				LOOP
			ENDIF
		ENDIF

*!* Added this code to trap unmatched OUTDETIDs in OUTDET, Joe 11.22.2005
		SCAN FOR ship_ref = TRIM(cShip_ref) AND outshipid = nOutshipID
			IF EMPTY(outdet.outdetid)
				ASSERT .F. MESSAGE "In Missing ODID in outdet vs. Vnanjingpp"
				WAIT WINDOW "OUTDETID "+TRANSFORM(vnanjingpp.outdetid)+" NOT FOUND IN OUTDET!...CLOSING" TIMEOUT 3
				cErrMsg = "MISS OD-ID: "+TRANSFORM(vnanjingpp.outdetid)
				DO ediupdate WITH cErrMsg,.T.
				THROW
			ENDIF
		ENDSCAN

		scanstr = "vnanjingpp.ship_ref = cShip_ref and vnanjingpp.outshipid = nOutshipid"
		SELECT vnanjingpp
		LOCATE FOR &scanstr
		cUCC = "XXX"
*	SCAN FOR &scanstr
		DO WHILE &scanstr
			IF vnanjingpp.totqty = 0
				SKIP 1 IN vnanjingpp
				LOOP
			ENDIF

			IF vnanjingpp.ucc = 'CUTS'
				SKIP 1 IN vnanjingpp
				LOOP
			ENDIF

			IF TRIM(vnanjingpp.ucc) <> cUCC
				STORE TRIM(vnanjingpp.ucc) TO cUCC
			ENDIF

			STORE "LX"+cfd+ALLTRIM(STR(nCtnNumber))+csegd TO cString   && Carton Seq. # (up to Carton total)
			lDoManSegment = .T.
			DO cstringbreak
			nSegCtr = nSegCtr + 1
			DO WHILE vnanjingpp.ucc = cUCC
				IF lDoManSegment
					IF lTestInput
						cTrk = cBOL
					ELSE
						SELECT temptrknumber
						cTrk = ALLTRIM(temptrknumber.trknumber)
						IF !EOF()
							SKIP 1 IN temptrknumber
						ENDIF
					ENDIF
					SELECT vnanjingpp
					IF !EMPTY(cUCC) AND cUCC # "XXX"
						lDoManSegment = .F.
						IF !EMPTY(cTrk)
							STORE "MAN"+cfd+"GM"+cfd+TRIM(cUCC)+cfd+"CP"+cTrk+csegd TO cString  && UCC+Tracking Number
						ELSE
							STORE "MAN"+cfd+"GM"+cfd+TRIM(cUCC)+csegd TO cString  && UCC Number (Wal-mart DSDC only)
						ENDIF
						DO cstringbreak
						nSegCtr = nSegCtr + 1
						nDetQty = INT(VAL(outdet.PACK))
					ELSE
						WAIT CLEAR
						WAIT WINDOW "Empty UCC Number in vNanjingPP "+cShip_ref TIMEOUT 2
						IF lTesting
							CLOSE DATABASES ALL
							=FCLOSE(nFilenum)
							ERASE &cFilenameOut
							THROW
						ELSE
							cErrMsg = "EMPTY UCC# in "+cShip_ref
							DO ediupdate WITH cErrMsg,.T.
							=FCLOSE(nFilenum)
							ERASE &cFilenameOut
							THROW
						ENDIF
					ENDIF
				ENDIF

				SELECT outdet
				cColor = ""
				IF "ORIG_COLOR*"$outdet.printstuff
					valreturn("orig_color")
					STORE cOrig_color TO cColor
				ENDIF

				cSize = ""
				IF "ORIG_ID*"$outdet.printstuff
					valreturn("orig_id")
					STORE cOrig_id TO cSize
				ELSE
					IF EMPTY(TRIM(cSize))
						STORE TRIM(outdet.ID) TO cSize
					ENDIF
				ENDIF

				cStyle = ""
				IF "ORIG_STYLE*"$outdet.printstuff
					valreturn("orig_style")
					STORE cOrig_style TO cStyle
				ELSE
					IF EMPTY(cStyle)
						cStyle = TRIM(outdet.STYLE)
					ENDIF
				ENDIF

				cUPC = TRIM(outdet.upc)
				valreturn("PRINTSTYLE")
				nDetQty = outdet.totqty

				STORE "LB" TO cWeightUnit
				IF lTesting AND EMPTY(outdet.ctnwt)
					cCtnWt = "2"
					nTotCtnWt = nTotCtnWt + 2
				ELSE
					STORE ALLTRIM(STR(outdet.ctnwt)) TO cCtnWt
					nTotCtnWt = nTotCtnWt + outdet.ctnwt
				ENDIF

				IF EMPTY(cCtnWt) OR outdet.ctnwt=0
					nCtnWt = outship.weight/outship.ctnqty
					cCtnWt = ALLTRIM(STR(nCtnWt))
					nTotCtnWt = nTotCtnWt + nCtnWt
					IF EMPTY(cCtnWt)
						cErrMsg = "MISS CTNWT "+TRANSFORM(vnanjingpp.outdetid)
						DO ediupdate WITH cErrMsg,.T.
						=FCLOSE(nFilenum)
						ERASE &cFilenameOut
						THROW
					ENDIF
				ENDIF

				valreturn("UNITSTYPE")
				valreturn("W0104")
				valreturn("W0106")
				valreturn("W0115")
				cItemNum = outdet.custsku

				IF EMPTY(cUnitsType)
					cUnitsType = "EA"
				ENDIF

				IF EMPTY(cW0104)
					cW0104 = "IN"
				ENDIF

				IF EMPTY(cW0106)
					cW0106 = "UP"
				ENDIF

				IF EMPTY(cW0115)
					cW0115 = "VN"
				ENDIF

				IF lPrepack
					nDetQty = 1
				ENDIF

				IF nDetQty>0
					nUnitSum = nUnitSum + nDetQty
					STORE "W12"+cfd+"CL"+cfd+ALLTRIM(STR(nDetQty))+cfd+ALLTRIM(STR(nDetQty))+cfd+cfd+cUnitsType+cfd+cUPC+;
						cfd+cW0104+cfd+TRIM(cItemNum)+cfd+cfd+cCtnWt+cfd+cWeightUnit+REPLICATE(cfd,6)+cW0106+cfd+cUPC+;
						REPLICATE(cfd,3)+cW0115+cfd+cStyle+csegd TO cString  && Eaches/Style
					DO cstringbreak
					nSegCtr = nSegCtr + 1

					IF !EMPTY(cColor)
						STORE "N9"+cfd+"VCL"+cfd+UPPER(cColor)+csegd TO cString
						DO cstringbreak
						nSegCtr = nSegCtr + 1
					ENDIF

					IF !lPrepack
						IF !EMPTY(cSize)
							STORE "N9"+cfd+"VSZ"+cfd+UPPER(cSize)+csegd TO cString
							DO cstringbreak
							nSegCtr = nSegCtr + 1
						ENDIF
					ENDIF
				ENDIF
				nCtnNumber = nCtnNumber + 1
				SELECT vnanjingpp
				SKIP 1 IN vnanjingpp
			ENDDO
			nCtnNumber = nCtnNumber + 1
		ENDDO

WAIT WINDOW "At end of outdet loop" nowait
*************************************************************************
*2^	END OUTDET MATCHING OUTSHIPID LOOP
*************************************************************************
		WAIT CLEAR
		WAIT WINDOW "Now creating Section Closure information" NOWAIT NOCLEAR
		STORE "W03"+cfd+ALLTRIM(STR(nUnitSum))+cfd+ALLTRIM(STR(nTotCtnWt))+cfd+;
			cWeightUnit+csegd TO cString   && Units sum, Weight sum, carton count

		nTotCtnWt = 0
		nTotCtnCount = 0
		nUnitSum = 0
		FPUTS(nFilenum,cString)

		STORE  "SE"+cfd+ALLTRIM(STR(nSegCtr+2))+cfd+PADL(c_GrpCntrlNum,9,"0")+csegd TO cString
		FPUTS(nFilenum,cString)

		IF lTesting
			SELECT outship
		ELSE
			SELECT edi_trigger
		ENDIF
		WAIT CLEAR
	ENDSCAN

*************************************************************************
*1^	END OUTSHIP MAIN LOOP
*************************************************************************

	DO close945
	=FCLOSE(nFilenum)
	IF !lTesting
		SELECT edi_trigger
		DO ediupdate WITH "945 CREATED",.F.
		SELECT edi_trigger
		LOCATE

		IF !USED("ftpedilog")
			SELECT 0
			USE F:\edirouting\ftpedilog ALIAS ftpedilog
			INSERT INTO ftpedilog (TRANSFER,ftpdate,filename,acct_name,TYPE) VALUES ("945-"+cCustname+"-"+cCustLoc,dt2,cFilenameOut,UPPER(cCustname),"945")
			USE IN ftpedilog
		ENDIF
	ENDIF

	WAIT CLEAR
	WAIT WINDOW cCustFolder+" 945 Process complete..." TIMEOUT 1

*!* Create eMail confirmation message
	IF lTestMail
		tsendto = tsendtotest
		tcc = tcctest
	ENDIF

	cOutFolder = "FMI"+cCustLoc

	tsubject = cMailName+" 945 EDI File from TGF as of "+dtmail+" ("+cCustLoc+")"
	tattach = " "
	tmessage = "945 EDI Info from TGF for "+cDivision+", UPS WO# "+TRIM(cWO_Num)+CHR(13)
	tmessage = tmessage + "(File: "+cFilenameShort+")"+CHR(13)
	tmessage = tmessage + "containing these picktickets:"+CHR(13)
	tmessage = tmessage + cPTString + CHR(13)
	tmessage = tmessage + +"has been created and will be transmitted ASAP."+CHR(13)+CHR(13)
	tmessage = tmessage + "If you have any questions, please eMail or call 732-750-9000 x217."
	tmessage = tmessage+CHR(13)+"Run from prog: "+cProgName


	IF lEmail
		DO FORM m:\dev\frm\dartmail2 WITH tsendto,tfrom,tsubject,tcc,tattach,tmessage,"A"
	ENDIF


	RELEASE ALL LIKE c_CntrlNum,c_GrpCntrlNum
	WAIT CLEAR
	WAIT WINDOW cMailName+" 945 EDI File output complete"+CHR(13)+"for WO# "+TRIM(cWO_Num) TIMEOUT 1

*!* Transfers files to correct output folders
*COPY FILE &cFilenameOut TO &cFilenameArch

	IF lTesting
		SET STEP ON
	ENDIF

	COPY FILE &cFilenameHold TO &cFilenameOut
	DELETE FILE &cFilenameHold
	SELECT temp945
	COPY TO "f:\3pl\data\temp945a.dbf"
	USE IN temp945
	SELECT 0
	USE "f:\3pl\data\temp945a.dbf" ALIAS temp945a
	SELECT 0
	USE "f:\3pl\data\pts_sent945.dbf" ALIAS pts_sent945
	APPEND FROM "f:\3pl\data\temp945a.dbf"
	USE IN pts_sent945
	USE IN temp945a
	DELETE FILE "f:\3pl\data\temp945a.dbf"

	IF !lTesting
		asn_out_data()
	ENDIF

CATCH TO oErr
	IF lDoCatch
		SET STEP ON
		lEmail = .F.
		IF EMPTY(cErrMsg)
			DO ediupdate WITH ALLTRIM(oErr.MESSAGE),.T.
			tsubject = cCustname+" Error ("+TRANSFORM(oErr.ERRORNO)+") at "+TTOC(DATETIME())
			tattach  = ""
			tsendto = IIF(lTesting,tsendtotest,tsendtoerr)
			tcc = IIF(lTesting,tcctest,tccerr)

			tmessage = cCustname+" Error processing "+CHR(13)

			tmessage =tmessage+ "Try/CATCH Exeception Message:"+CHR(13)+;
				[  Error: ] + STR(oErr.ERRORNO) +CHR(13)+;
				[  LineNo: ] + STR(oErr.LINENO) +CHR(13)+;
				[  Message: ] + oErr.MESSAGE +CHR(13)+;
				[  Procedure: ] + oErr.PROCEDURE +CHR(13)+;
				[  Details: ] + oErr.DETAILS +CHR(13)+;
				[  StackLevel: ] + STR(oErr.STACKLEVEL) +CHR(13)+;
				[  LineContents: ] + oErr.LINECONTENTS+CHR(13)+;
				[  UserValue: ] + oErr.USERVALUE

			tsubject = "945 EDI Poller Error at "+TTOC(DATETIME())
			tattach  = ""
			tfrom = "Toll EDI Operations <toll-edi-ops@tollgroup.com>"
			DO FORM m:\dev\frm\dartmail2 WITH tsendto,tfrom,tsubject,tcc,tattach,tmessage,"A"
		ENDIF
	ENDIF
FINALLY
*	RELEASE ALL LIKE t*
ENDTRY

*** END OF CODE BODY

****************************
PROCEDURE close945
****************************
** Footer Creation

	STORE  "GE"+cfd+ALLTRIM(STR(nSTCount))+cfd+c_CntrlNum+csegd TO cString
	FPUTS(nFilenum,cString)

	STORE  "IEA"+cfd+"1"+cfd+PADL(c_CntrlNum,9,"0")+csegd TO cString
	FPUTS(nFilenum,cString)

	RETURN

****************************
PROCEDURE num_incr_isa
****************************

	IF !USED("serfile")
		USE ("f:\3pl\data\serial\"+cCustname+"945_serial") IN 0 ALIAS serfile
	ENDIF
	SELECT serfile
	nISA_Num = serfile.seqnum
	c_CntrlNum = ALLTRIM(STR(serfile.seqnum))
	REPLACE serfile.seqnum WITH serfile.seqnum + 1 IN serfile
	SELECT outship
	RETURN

****************************
PROCEDURE num_incr_st
****************************

	IF !USED("serfile")
		USE ("f:\3pl\data\serial\"+cCustname+"945_serial") IN 0 ALIAS serfile
	ENDIF
	SELECT serfile
	c_GrpCntrlNum = ALLTRIM(STR(serfile.grpseqnum))
	REPLACE serfile.grpseqnum WITH serfile.grpseqnum + 1 IN serfile
	SELECT outship
	RETURN

****************************
PROCEDURE segmentget
****************************

	PARAMETER thisarray,lcKey,nLength

	FOR i = 1 TO nLength
		IF i > nLength
			EXIT
		ENDIF
		lnEnd= AT("*",thisarray[i])
		IF lnEnd > 0
			lcThisKey =TRIM(SUBSTR(thisarray[i],1,lnEnd-1))
			IF OCCURS(lcKey,lcThisKey)>0
				RETURN SUBSTR(thisarray[i],lnEnd+1)
				i = 1
			ENDIF
		ENDIF
	ENDFOR

	RETURN ""

****************************
PROCEDURE ediupdate
****************************
	PARAMETER cStatus,lIsError
	lDoCatch = .F.

	IF !lTesting
		SELECT edi_trigger
		LOCATE
		nRec = RECNO()
		IF !lIsError
			REPLACE processed WITH .T.,proc945 WITH .T.,file945 WITH cFilenameOut,;
				fin_status WITH "945 CREATED",errorflag WITH .F.,when_proc WITH DATETIME() ;
				FOR edi_trigger.bol = cBOL AND INLIST(accountid,4610,4694) ;
				AND !processed
		ELSE
			REPLACE processed WITH .T.,proc945 WITH .F.,file945 WITH "",;
				fin_status WITH cStatus,errorflag WITH .T.  ;
				FOR edi_trigger.bol = cBOL AND INLIST(accountid,4610,4694) ;
				AND !processed
			IF lCloseOutput
				=FCLOSE(nFilenum)
				ERASE &cFilenameOut
			ENDIF
		ENDIF
	ENDIF

	IF lIsError AND lEmail
		tsubject = "945 Error in NANJING/EA WO# "+TRIM(cWO_Num)+"(At PT "+cShip_ref+")"
		tattach = " "
		tsendto = tsendtoerr
		tcc = tccerr

		tmessage = "Generated from program: nanjing945_create_ups, within EDI Outbound Poller"
		tmessage = tmessage+CHR(13)+"945 Processing for WO# "+cWO_Num+" produced this error: (Office: "+cOffice+"): "+cStatus+CHR(13)+"Check EDI_TRIGGER and re-run"
		IF TIME(DATE())>"20:00:00" OR INLIST(DOW(DATE()),1,7)
*			tcc = tcc+",jsb1956@yahoo.com"
		ENDIF
		DO FORM m:\dev\frm\dartmail2 WITH tsendto,tfrom,tsubject,tcc,tattach,tmessage,"A"
	ENDIF

	IF USED('outship')
		USE IN outship
	ENDIF
	IF USED('outdet')
		USE IN outdet
	ENDIF
	IF USED('outwolog')
		USE IN outwolog
	ENDIF
	IF USED('serfile')
		USE IN serfile
	ENDIF
	IF USED('scacs')
		USE IN scacs
	ENDIF
	IF USED('mm')
		USE IN mm
	ENDIF
	IF USED('shipment')
		USE IN SHIPMENT
	ENDIF
	IF USED('package')
		USE IN PACKAGE
	ENDIF
	IF !lTesting
		SELECT edi_trigger
		LOCATE
	ENDIF
ENDPROC

****************************
PROCEDURE cstringbreak
****************************
	cLen = LEN(ALLTRIM(cString))

	FPUTS(nFilenum,cString)
ENDPROC

**************************************************************************************
PROCEDURE scc14generate
**************************************************************************************
	STORE "" TO cartonid,cSCCNumber,cCHKDigit
	nSCCSuffix = nSCCSuffix + 1

	cPkgIndicator = "1"  && Prepack -- PickPack  is 0
	cSCCPrefix = cPkgIndicator+" "+PADL(LEFT(outdet.upc,1),2,"0") + SUBSTR(outdet.upc,2,5)

	cSCCSuffix = PADL(ALLTRIM(STR(nSCCSuffix)),5,"0")
	cSCCNumber2 = cSCCPrefix+" "+cSCCSuffix

	nValue1 = 0  && "ODD" Digits
	nValue2 = 0  && "EVEN" Digits
	nValue3 = 0  && SUMMARY Value
	nChkDigit = 0

	cSCCNumber = STRTRAN(cSCCNumber2," ","")

	FOR i = LEN(TRIM(cSCCNumber)) TO 1 STEP -2
		nValue1 = nValue1 + INT(VAL(SUBSTR(cSCCNumber,i,1)))
	ENDFOR
	nValue1 = nValue1 * 3

	FOR i = LEN(TRIM(cSCCNumber))-1 TO 1 STEP -2
		nValue2 = nValue2 + INT(VAL(SUBSTR(cSCCNumber,i,1)))
	ENDFOR
	nValue3 = nValue1 + nValue2
	nChkDigit = 10-(nValue3%10)  && <--- The output check digit here
	IF nChkDigit = 10
		nChkDigit = 0
	ENDIF

	cCHKDigit = ALLTRIM(STR(nChkDigit))
	cSCCNumber = cSCCNumber + cCHKDigit
	cSCCNumber = TRIM(cSCCNumber)

ENDPROC

****************************
PROCEDURE valreturn
****************************
	PARAMETERS cIdentifier
	cReturned = "c"+cIdentifier
	RELEASE ALL LIKE &cReturned
	PUBLIC &cReturned
	nGetline = ATCLINE(cIdentifier,outdet.printstuff)
	dataline = MLINE(outdet.printstuff,nGetline)
	STORE SUBSTR(dataline,AT("*",dataline)+1) TO &cReturned
	RETURN &cReturned
ENDPROC

****************************
PROCEDURE totqtyerr
****************************
	SET STEP ON
	tsubject = "Cartons/Units Discrepancy, Outbound vs. Cartons Data"
	tmessage = "The following PT(s) had unmatched counts between the outbound ('O') and cartonization ('C') data:"+CHR(13)+CHR(13)
	tmessage = tmessage+PADR("PT#",20)+PADR("O.Ctns",10)+PADR("C.Ctns",10)+PADR("O.Units",10)+PADR("C.Units",10)+CHR(13)
	SELECT tempqtyerr
	SCAN
		cSRUse = ALLTRIM(tempqtyerr.ship_ref)
		cnctnqty = STR(tempqtyerr.ctnqty)
		cncqty = STR(tempqtyerr.cqty)
		cntotqty = STR(tempqtyerr.totqty)
		cnuqty = STR(tempqtyerr.uqty)
		DO CASE
			CASE (ctnqty # cqty) AND (totqty # uqty)
				cErrorLine = "Both Carton and Unit totals don't match"
			CASE (ctnqty # cqty)
				cErrorLine = "Only Carton totals don't match"
			CASE (totqty # uqty)
				cErrorLine = "Only Unit totals don't match"
		ENDCASE
		tmessage = tmessage+PADR(cSRUse,20)+PADR(cnctnqty,10)+PADR(cncqty,10)+PADR(cntotqty,10)+PADR(cnuqty,10)+" "+cErrorLine+CHR(13)
	ENDSCAN

	tmessage = tmessage+CHR(13)+"Please contact Toll I.T./EDI IMMEDIATELY to confirm which numbers are correct."
	tsendto = IIF(lTesting,tsendtotest,tsendtoqty)
	tcc = IIF(lTesting,tcctest,tccqty)
	tattach = ""
	DO FORM m:\dev\frm\dartmail2 WITH tsendto,tfrom,tsubject,tcc,tattach,tmessage,"A"
	USE IN tempqtyerr
ENDPROC
