DO M:\DEV\PRG\_SETVARS WITH .T.
PUBLIC cfilename,cfileoutname,NormalExit,cErrMsg,tfrom
PUBLIC ARRAY a856(1)
CLOSE DATA ALL

TRY
	lTesting = .F.
	lOverrideBusy = lTesting
	NormalExit = .F.
	cErrMsg = ""
	tfrom = "Toll EDI Operations <toll-edi-ops@tollgroup.com>"

*	lOverrideBusy = .t.

	IF !lTesting
		SELECT 0
		USE F:\edirouting\ftpsetup SHARED
		cTransfer = "MERKURY-MOVE"
		LOCATE FOR ftpsetup.transfer = cTransfer
		IF ftpsetup.chkbusy = .T. AND !lOverrideBusy
			WAIT WINDOW "File is processing...exiting" TIMEOUT 2
			NormalExit = .T.
			THROW
		ENDIF
		REPLACE chkbusy WITH .T. FOR ftpsetup.transfer = cTransfer
		USE IN ftpsetup
	ENDIF

	SELECT 0
	USE F:\3pl\DATA\mailmaster ALIAS mm SHARED
	LOCATE
	LOCATE FOR mm.edi_type = "MISC" AND mm.taskname = "GENERAL"
	STORE TRIM(IIF(mm.use_alt,mm.sendtoalt,mm.sendto)) TO tsendtoerr
	STORE TRIM(IIF(mm.use_alt,mm.ccalt,mm.cc)) TO tccerr
	USE IN mm

	cDirIn = "f:\ftpusers\merkury\in\"
	CD &cDirIn

	_SCREEN.WINDOWSTATE=IIF(lTesting,2,1)
	_SCREEN.CAPTION="Merkury File Move Process"

	nFound = ADIR(ary1,'*.*')
	IF nFound = 0
		CLOSE DATABASES ALL
		WAIT WINDOW "No Merkury files found to process...exiting" TIMEOUT 2
		NormalExit = .T.
		THROW
	ENDIF

	SET STEP ON 
	lenmerk = ALEN(ary1,1)

	FOR qwq = 1 TO lenmerk
*ASSERT .F. MESSAGE "In move loop"
		cfilename = ALLT(ary1[qwq,1])
		xfile = (cDirIn+cfilename)

		IF UPPER(JUSTEXT(cfilename)) = "CSV" && Moved this section up for inbounds
			cDirOut = ("F:\ftpusers\merkury\inbounds\")
			cfileoutname = (cDirOut+cfilename)
			COPY FILE [&xfile] TO [&cFileOutName]
			IF FILE(cfileoutname)
				IF FILE(xfile)
					DELETE FILE [&xfile]
				ENDIF
			ENDIF
			LOOP
		ENDIF

		cfileholdname = ("F:\ftpusers\merkury\940in\hold\"+cfilename)
		cfiletestname = ("F:\ftpusers\merkury\testing\"+cfilename)
		
		DO M:\DEV\PRG\createx856a
		DO M:\DEV\PRG\loadedifile WITH xfile,"*","CARROT","MERKURY"

		SELECT x856
		LOCATE FOR x856.segment = "ISA"
		IF x856.f15 = "T"  && Test files
			COPY FILE [&xfile] TO [&cFileTestName]
			DELETE FILE [&xfile]
			LOOP
		ENDIF

*SET STEP ON
		STORE "" TO cGS1
		LOCATE
		LOCATE FOR x856.segment = 'GS'
		cGS1 = ALLTRIM(x856.f1)
		l997 = IIF(cGS1 = "FA",.T.,.F.)
		l940 = IIF(cGS1 = "OW",.T.,.F.)
		SCAN FOR x856.segment = 'GS'
			IF ALLTRIM(x856.f1) # cGS1  && Mixed file types
				cErrMsg = "Mixed file type, file name: "+cfilename+". Correct the file and reload it."
				cErrMsg = cErrMsg+CHR(13)+"File is in folder F:\ftpusers\merkury\940in\hold\"
				COPY FILE [&xfile] TO [&cFileHoldName]
				DELETE FILE [&xfile]
				THROW
			ENDIF
		ENDSCAN

		WAIT WINDOW "Processing "+cfilename TIMEOUT 1
		IF l997
			cDirOut = ("F:\ftpusers\merkury\997IN\")
			cfileoutname = (cDirOut+cfilename)
			COPY FILE [&xfile] TO [&cFileOutName]
			IF FILE(cfileoutname)
				IF FILE(xfile)
					DELETE FILE [&xfile]
				ENDIF
			ENDIF
			LOOP
		ELSE
			cDirOut = ("F:\ftpusers\merkury\940IN\")
			cfileoutname = (cDirOut+cfilename)
			cMerkArchiveFile = ('F:\FTPUSERS\Merkury\IN\archive\')+cfilename
			COPY FILE [&xfile] TO [&cFileOutName]
			COPY FILE [&xfile] TO [&cMerkArchiveFile]
			IF FILE(cfileoutname)
				IF FILE(cfilename)
					DELETE FILE [&xfile]
				ENDIF
			ENDIF
		ENDIF
	ENDFOR

	WAIT WINDOW "Merkury move process complete" TIMEOUT 1
	SELECT 0
	USE F:\edirouting\ftpsetup SHARED
	cTransfer = "MERKURY-MOVE"
	REPLACE chkbusy WITH .F. FOR ftpsetup.transfer = cTransfer
	CLOSE DATABASES ALL
	WAIT CLEAR
	NormalExit = .T.
	WAIT WINDOW "All Merkury files moved" TIMEOUT 1

CATCH TO oErr
	IF NormalExit = .F.
		ASSERT .F. MESSAGE "In Catch section..."
		tsubject = "Merkury 940 Upload Error ("+TRANSFORM(oErr.ERRORNO)+") at "+TTOC(DATETIME())
		tsendto  = tsendtoerr
		tcc = tccerr
		tmessage = "Merkury 940 Upload Error:"

		IF !EMPTY(cErrMsg)
			tmessage =tmessage+CHR(13)+cErrMsg
		ELSE
			lcSourceMachine = SYS(0)
			lcSourceProgram = "from merkury940 group"
			tmessage =tmessage+ "Try/CATCH Exeception Message:"+CHR(13)+;
				[  Error: ] + STR(oErr.ERRORNO) +CHR(13)+;
				[  LineNo: ] + STR(oErr.LINENO) +CHR(13)+;
				[  Message: ] + oErr.MESSAGE +CHR(13)+;
				[  Procedure: ] + oErr.PROCEDURE +CHR(13)+;
				[  Details: ] + oErr.DETAILS +CHR(13)+;
				[  StackLevel: ] + STR(oErr.STACKLEVEL) +CHR(13)+;
				[  LineContents: ] + oErr.LINECONTENTS+CHR(13)+;
				[  UserValue: ] + oErr.USERVALUE+CHR(13)+;
				[  Computer:  ] +lcSourceMachine+CHR(13)+;
				[  940 file:  ] +xfile+CHR(13)+;
				[  Program:   ] +lcSourceProgram
		ENDIF
		tattach = ""

		DO FORM M:\DEV\frm\dartmail2 WITH tsendto,tfrom,tsubject,tcc,tattach,tmessage,"A"
	ELSE
		WAIT WINDOW AT 10,10 "Normal Exit " TIMEOUT 2
	ENDIF
FINALLY
	ON ERROR
	SET STATUS BAR ON
	CLOSE DATABASES ALL
ENDTRY

