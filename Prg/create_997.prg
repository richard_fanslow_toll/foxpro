Parameters nAcctNum
Close Data All

nAcctnum = Val(Alltrim(Transform(nAcctNum)))

*Parameters DATE,nAcctNum
Public c_CntrlNum,c_GrpCntrlNum,cWO_Num,cGroupName,cCustName,cFilename,div214,cST_CSZ
Public tsendto,tcc,tsendtoerr,tccerr,cDelName,cMacysnet,cTripid,cMailName,cFin_status

*If Inlist(Type("whichgroupnum"),"L","U")
*Wait Window At 10,10 "Need a group Num to create a 997" timeout 1
*return
*Endif

If Inlist(Type("nAcctNum"),"L","U")
  Wait Window At 10,10 "Need an account a 997" Timeout 1
  Return
Endif

Close Data All

DO m:\dev\prg\_setvars WITH .T.

ltesting = .f.

xsqlexec("select * from ackdata where accountid="+transform(nAcctNum)+" and processed=0","temp",,"wh")

Select groupnum,edicode From temp Group By groupnum,edicode Into Cursor groupps
SET STEP ON 
If nAcctnum= 6532  && setup for Ariat
  cterminator = ">"  && Used at end of ISA segment
  cfd = "*"  && Field delimiter
  csegd = "~" && CR
  csendqual = "ZZ"  && Sender qualifier
  csendid = "TGFPROD940"  && Sender ID code
  crecqual = "01"  && Recip qualifier
  crecid = "789995313"   && Recip ID Code, production (05.11.2010)
  lputCR = .f. 
  dt1 = Ttoc(Datetime(),1)
  cFilename = ("997"+"-"+"TOLL"+"-"+dt1+".edi")
  cFilenameShort = Justfname(cFilename)
  cFilename2 = ("f:\ftpusers\ariat\997out\archive\"+cFilenameShort)
  cFilename3 = ("f:\ftpusers\ariat\OUT\"+cFilenameShort)
  nHandle = Fcreate(cFilename2)

Else
  cterminator = ">"  && Used at end of ISA segment
  cfd = "*"  && Field delimiter
  csegd = Chr(13) && CR
  csendqual = "ZZ"  && Sender qualifier
  csendid = "FMIF"  && Sender ID code
  crecqual = "01"  && Recip qualifier
  crecid = "TLI94EP"   && Recip ID Code, production (05.11.2010)
  lputCR = .t. 
Endif



cDate = Dtos(Date())
cTruncDate = Right(cDate,6)
cTruncTime = Substr(Ttoc(Datetime(),1),9,4)
cfiledate = cDate+cTruncTime
csendidlong = Padr(csendid,15," ")
crecidlong = Padr(crecid,15," ")
cString = ""

nSTCount = 0
nSegCtr = 0
nLXNum = 1

cISACode = Iif(ltesting,"T","P")

Do num_incr_isa

cISA_Num = Padl(c_CntrlNum,9,"0")

Store "ISA"+cfd+"00"+cfd+"          "+cfd+"00"+cfd+"          "+cfd+csendqual+cfd+csendidlong+cfd+;
crecqual+cfd+crecidlong+cfd+cTruncDate+cfd+cTruncTime+cfd+"U"+cfd+"00200"+cfd+;
cISA_Num+cfd+"0"+cfd+cISACode+cfd+cterminator+csegd To cString
Do cstringbreak

Store "GS"+cfd+"FA"+cfd+csendid+cfd+crecid+cfd+cDate+cfd+cTruncTime+cfd+c_CntrlNum+;
cfd+"X"+cfd+"004010"+csegd To cString
Do cstringbreak



Select groupps
Scan
  Do num_incr_st
  Store "ST"+cfd+"997"+cfd+Padl(c_GrpCntrlNum,9,"0")+csegd To cString
  Do cstringbreak
  nSTCount = nSTCount + 1
  nSegCtr = nSegCtr + 1

  Store "AK1"+cfd+Alltrim(groupps.edicode)+cfd+Alltrim(groupps.groupnum)+csegd To cString
  Do cstringbreak
  nSegCtr = nSegCtr + 1

  Select temp
  recctr = 0

  Scan For temp.groupnum = groupps.groupnum

    Do Case 
      Case groupps.edicode = "SC"
        Store "AK2"+cfd+"832"+cfd+Alltrim(temp.transnum)+csegd To cString
      Case groupps.edicode = "OW"
        Store "AK2"+cfd+"940"+cfd+Alltrim(temp.transnum)+csegd To cString
    Endcase 
     
    Do cstringbreak
    nSegCtr = nSegCtr + 1

    Store "AK5"+cfd+"A"+csegd To cString
    Do cstringbreak
    nSegCtr = nSegCtr + 1
    recctr = recctr+1
  Endscan

  NumOK = Alltrim(Transform(recctr))

  Store "AK9"+cfd+"A"+cfd+NumOK+cfd+NumOK+cfd+NumOK+csegd To cString
  Do cstringbreak
  nSegCtr = nSegCtr + 1

  Store "SE"+cfd+Alltrim(Str(nSegCtr+1))+cfd+Padl(c_GrpCntrlNum,9,"0")+csegd To cString
  Do cstringbreak

  nSegCtr=0

Endscan


Store  "GE"+cfd+Alltrim(Str(nSTCount))+cfd+c_CntrlNum+csegd To cString
Do cstringbreak

Store  "IEA"+cfd+"1"+cfd+Padl(c_CntrlNum,9,"0")+csegd To cString
Do cstringbreak

lClosedFile=Fclose(nHandle)

Select groupps
Go Top
scan
	xsqlexec("update ackdata set processed=1, fafilename='"+cfilename2+"' where accountid="+transform(nAcctNum)+" and groupnum='"+groupps.groupnum+"'",,,"wh")
Endscan

Copy File &cfilename2 To &cfilename3

&&Wait Window At 10,10 "first time: "+Transform(duration)

****************************
Procedure num_incr_isa
****************************

If !Used("serfile")
  Use ("F:\3pl\data\serial\all997_serial") In 0 Alias serfile
Endif
Select serfile
If ltesting
  nOrigSeq = serfile.seqnum
  nOrigGrpSeq = serfile.grpseqnum
  lISAFlag = .F.
Endif
nISA_Num = serfile.seqnum
c_CntrlNum = Alltrim(Str(serfile.seqnum))
Replace serfile.seqnum With serfile.seqnum + 1 In serfile
Endproc

****************************
Procedure num_incr_st
****************************

If !Used("serfile")
  Use ("F:\3pl\data\serial\all997_serial") In 0 Alias serfile
Endif
Select serfile
If ltesting
  nOrigSeq = serfile.seqnum
  nOrigGrpSeq = serfile.grpseqnum
  lSTFlag = .F.
Endif
c_GrpCntrlNum = Alltrim(Str(serfile.grpseqnum))
Replace serfile.grpseqnum With serfile.grpseqnum + 1 In serfile
Endproc

****************************
Procedure segmentget
****************************

Parameter thisarray,lcKey,nLength

For i = 1 To nLength
  If i > nLength
    Exit
  Endif
  lnEnd= At("*",thisarray[i])
  If lnEnd > 0
    lcThisKey =Trim(Substr(thisarray[i],1,lnEnd-1))
    If Occurs(lcKey,lcThisKey)>0
      Return Substr(thisarray[i],lnEnd+1)
      i = 1
    Endif
  Endif
Endfor

Return ""

****************************
Procedure close214
****************************
Store  "SE"+cfd+Alltrim(Str(nSegCtr+1))+cfd+Padl(c_GrpCntrlNum,9,"0")+csegd To cString
Do cstringbreak

Store  "GE"+cfd+Alltrim(Str(nSTCount))+cfd+c_CntrlNum+csegd To cString
Do cstringbreak

Store  "IEA"+cfd+"1"+cfd+Padl(c_CntrlNum,9,"0")+csegd To cString
Do cstringbreak

Endproc


****************************
Procedure cstringbreak
****************************
cLen = Len(Alltrim(cString))
If lPutCR
 FPut(nHandle,cString)
Else
 Fwrite(nHandle,cString)
Endif 
 
Return
