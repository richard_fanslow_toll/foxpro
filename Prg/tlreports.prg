**tlreports.prg
**
parameters cdetail, ctest



ldetail=iif(empty(cdetail) or cdetail="0", .f., .t.)
ltest=iif(empty(ctest) or ctest="0", .f., .t.)

do macros
do set_envi

if !ltest
	utilsetup("TLRPT")
endif

use f:\kpi\data\emailaddr in 0

set sysmenu off

*lTest = .f.

select emailaddr
do case
case ltest
	locate for test
case ldetail
	locate for report = 'Detailed Sorter Stat'
otherwise
	locate for report = 'Sorter Stats'
endcase

if !found()
	if !ltest
		schedupdate()
	endif
  return
endif

cemaillist = emailaddr.toaddrs

dstart=Date()-1
dend=Date()-1

if ltest
	do form inputrange with "Daily Sorter Stats","Input a date range:","Dates:",dstart,dend to xrange
	if inlist(xrange,'ABORT','ERROR')
		return
	endif
set step on 

	dstart=ctod(left(xrange,at("-",xrange)-1))
	dend=ctod(right(xrange,len(xrange)-at("-",xrange)))
endif

do case
case ldetail
	DO dailyStats-detail with .t., 1000, "San Pedro", dstart, dend, "", "", .t., cEmailList
otherwise
	DO dailyStats with .t., 1000, "San Pedro", dstart, dend, .f., "", "", 1, .t., cEmailList
endcase

if !ltest
	schedupdate()
endif

Quit
