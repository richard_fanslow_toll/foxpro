* create headcount / fte by TGF data elements against prior month

* Build EXE as F:\UTIL\HR\TGFSTAFFRPT\HRTGFSTAFFRPT.exe

runack("HRTGFSTAFFRPT")

LOCAL loTGFStaffingReport
PRIVATE oExcel, oWorkbook

oExcel = CREATEOBJECT("excel.application")
oExcel.displayalerts = .F.
oExcel.VISIBLE = .F.
oWorkbook = .F.

loTGFStaffingReport = CREATEOBJECT('TGFStaffingReport')

* DON'T CHANGE THE ORDER OF THESE: 'FF' MUST BE FIRST, 'ALL' MUST BE LAST!
loTGFStaffingReport.MAIN('FF')
loTGFStaffingReport.MAIN('SCS')
loTGFStaffingReport.MAIN('ALL')

CLOSE DATABASES ALL
RETURN

#DEFINE LOGIT 1
#DEFINE WAITIT 2
#DEFINE NOWAITIT 4
#DEFINE SENDIT 8
#DEFINE CRLF CHR(13) + CHR(10)
#DEFINE RETURN_DATA_MANDATORY .F.
#DEFINE RETURN_DATA_NOT_MANDATORY .T.
#DEFINE CSVFORMAT 6
#DEFINE WRITE_ONLY_UNBUFFERED 11
#DEFINE COMMA ","
#DEFINE xlEdgeBottom 9
#DEFINE xlContinuous 1
#DEFINE xlMedium -4138
#DEFINE xlFONT_RED -16776961

DEFINE CLASS TGFStaffingReport AS CUSTOM

	cProcessName = 'TGFStaffingReport'

	lTestMode = .T.  && .F. FOR NORMAL OPERATION, .T. TO SUPPRESS ALL BUT DEVELOPER EMAILS AND CHANGE INPUT TABLE, ETC.

	dToday = DATE()

	lAutoYield = .T.

	cStartTime = TTOC(DATETIME())

	* report properties
	cBadData = ''
	
	*** usually set these to .F.
	lShowHours = .F.
	lSpecialAllWhere = .F.

	* connection properties
	nSQLHandle = 0

	* wait window properties
	nWaitWindowTimeout = 2
	lWaitWindowIsOn = .T.

	* logfile properties
	lLoggingIsOn = .T.
	cLogFile = 'F:\UTIL\HR\TGFSTAFFRPT\LOGFILES\TGFSTAFF_Report_log.txt'

	* INTERNAL email properties
	lSendInternalEmailIsOn = .T.
	cFrom = 'Mark.Bennett@Tollgroup.com'
	cSendTo = 'lucille.waldrip@Tollgroup.com, KKausner@fmiint.com, Tracy.Wang@Tollgroup.com, MarieDesaye@fmiint.com'
	cCC = 'mbennett@fmiint.com'
	cSubject = 'TGF Staffing Report for ' + DTOC(DATE())
	cAttach = ''
	cBodyText = ''

	cCOMPUTERNAME = ''
	cUSERNAME = ''


	FUNCTION INIT
		IF NOT DODEFAULT()
			RETURN .F.
		ENDIF
		WITH THIS
			*SET RESOURCE OFF
			CLOSE DATA
			SET CENTURY ON
			SET DATE AMERICAN
			SET HOURS TO 24
			SET ANSI ON
			SET TALK OFF
			SET DELETED ON
			SET CONSOLE OFF
			SET EXCLUSIVE OFF
			SET SAFETY OFF
			SET EXACT OFF
			SET STATUS BAR ON
			SET SYSMENU OFF
			SET ENGINEBEHAVIOR 70
			_VFP.AUTOYIELD = .lAutoYield
			.cCOMPUTERNAME = GETENV("COMPUTERNAME")
			.cUSERNAME = GETENV("USERNAME")
			IF .lTestMode THEN
				.cLogFile = 'F:\UTIL\HR\TGFSTAFFRPT\LOGFILES\TGFSTAFF_Report_log_TESTMODE.txt'
				.cSendTo = 'mbennett@fmiint.com'
			ENDIF
			.lLoggingIsOn = .lLoggingIsOn AND NOT EMPTY(.cLogFile)
			IF .lLoggingIsOn THEN
				SET ALTERNATE TO (.cLogFile) ADDITIVE
				SET ALTERNATE ON
			ENDIF
		ENDWITH
	ENDFUNC


	FUNCTION DESTROY
		WITH THIS
			IF .lLoggingIsOn  THEN
				SET ALTERNATE OFF
				SET ALTERNATE TO
			ENDIF
		ENDWITH
		DODEFAULT()
	ENDFUNC


	FUNCTION MAIN
		LPARAMETERS tcMode
		WITH THIS
			LOCAL lnNumberOfErrors, lcSQL, lcSQL2, lcSQL3, loError, ldToday
			LOCAL lcFiletoSaveAs, lcSpreadsheetTemplate, lcGrandTotalRow, lcWhere
			LOCAL lnDay, ldFromDate, ldToDate, lcSQLFromDate, lcSQLToDate, lcADPFromDate, lcADPToDate
			LOCAL lnFTEHours_Hourly, lcSQLKronos, lcSuffix, lcDivWhere, lcDateSuffix
			LOCAL i, oWorksheet, lnStartRow, lcStartRow, lnRow, lcRow
			LOCAL lnMgmtStartRow, lcMgmtStartRow, lnMgmtEndRow, lcMgmtEndRow, lnPreCasualRow, lcKronosDivWhere
			LOCAL lnNumHourlyPaydates, lnNumSalariedPaydates
			
			TRY
				lnNumberOfErrors = 0
				
				.TrackProgress("TGF Staffing Report process started....", LOGIT+SENDIT+NOWAITIT)
				.TrackProgress('COMPUTERNAME = ' + .cCOMPUTERNAME, LOGIT+SENDIT)
				.TrackProgress('USERNAME = ' + .cUSERNAME, LOGIT+SENDIT)
				.TrackProgress('tcMode = ' + tcMode, LOGIT+SENDIT)
				.TrackProgress('PROJECT = HRTGFSTAFFRPT', LOGIT+SENDIT)
				IF .lTestMode THEN
					.TrackProgress('                        TEST MODE                        ', LOGIT+SENDIT)
				ENDIF
				
				IF NOT USED('NOTPAYDATES') THEN
					USE F:\UTIL\HR\DATA\NOTPAYDATES.DBF IN 0 ALIAS NOTPAYDATES
				ENDIF
				
				lnStartRow = 3
				lcStartRow = ALLTRIM(STR(lnStartRow))
				
				DO CASE
					CASE tcMode = "FF"
						lcDivWhere = "AND {fn LEFT(CHECKVIEWHOMEDEPT,2)} IN ('20','21','22','23','24','25','26','27','28','29','30','31','35','36','37','38','71','72','73','91') "
						lcKronosDivWhere =            "AND D.LABORLEV2NM IN ('20','21','22','23','24','25','26','27','28','29','30','31','35','36','37','38','71','72','73','91') "
						lcSuffix = "_FF"
					CASE tcMode = "SCS"
						lcDivWhere = "AND {fn LEFT(CHECKVIEWHOMEDEPT,2)} NOT IN ('20','21','22','23','24','25','26','27','28','29','30','31','35','36','37','38','71','72','73','91') "
						lcKronosDivWhere =            "AND D.LABORLEV2NM NOT IN ('20','21','22','23','24','25','26','27','28','29','30','31','35','36','37','38','71','72','73','91') "
						lcSuffix = "_NON_FF"
					CASE tcMode = "ALL"
						lcDivWhere = ""	
						lcKronosDivWhere = ""
						IF .lSpecialAllWhere THEN
							lcDivWhere = "AND {fn LEFT(CHECKVIEWHOMEDEPT,2)} IN ('21','36','73') "
							lcKronosDivWhere = "AND D.LABORLEV2NM IN ('21','36','73') "
						ENDIF
						lcSuffix = "_ALL"		
					OTHERWISE
						THROW
				ENDCASE
				
				.TrackProgress('lcSuffix = ' + lcSuffix, LOGIT+SENDIT)
				.TrackProgress('lcDivWhere = ' + lcDivWhere, LOGIT+SENDIT)

				ldToday = .dToday

*!*	********************************************				
*!*	* ACTIVATE TO FORCE DIFFERENT MONTHS
*!*	ldToday = {^2012-01-05}
*!*	********************************************				

				lnDay = DAY(ldToday)

				ldToDate = ldToday - lnDay  && this is last day of prior month
				ldFromDate = ldToDate + 1  && this is 1st day of current month
				ldFromDate = GOMONTH(ldFromDate,-1)  && this is 1st day of prior month
				
				lcDateSuffix = "_" + DTOS(ldFromDate) + "-" + DTOS(ldToDate)
				lcSuffix = lcSuffix + lcDateSuffix
				.cSubject = 'TGF Staffing Report for ' + DTOC(ldFromDate) + " - " + DTOC(ldToDate)


				lcADPFromDate = "DATE'" + TRANSFORM(YEAR(ldFromDate)) + "-" + PADL(MONTH(ldFromDate),2,'0') + "-" + PADL(DAY(ldFromDate),2,'0') + "'"
				lcADPToDate = "DATE'" + TRANSFORM(YEAR(ldToDate)) + "-" + PADL(MONTH(ldToDate),2,'0') + "-" + PADL(DAY(ldToDate),2,'0') + "'"

				lcSpreadsheetTemplate = 'F:\UTIL\HR\TGFSTAFFRPT\TEMPLATES\TGFSTAFFING_TEMPLATE.XLS'
				lcFiletoSaveAs = 'F:\UTIL\HR\TGFSTAFFRPT\REPORTS\TGFSTAFFING' + lcDateSuffix + '.XLS'
				.TrackProgress('Creating spreadsheet: ' + lcFiletoSaveAs, LOGIT+SENDIT+NOWAITIT)


				* this block must only run once, at the beginning of the process...
				IF tcMode = "FF" THEN				
				
					* delete output file if it already exists...
					IF FILE(lcFiletoSaveAs) THEN
						DELETE FILE (lcFiletoSaveAs)
					ENDIF

					oWorkbook = oExcel.workbooks.OPEN(lcSpreadsheetTemplate)
					oWorkbook.SAVEAS(lcFiletoSaveAs)

				ENDIF && tcMode = "FF" 
				
				* determine current worksheet
				DO CASE
					CASE tcMode = "FF"
						oWorksheet = oWorkbook.Worksheets[1]
					CASE tcMode = "SCS"
						oWorksheet = oWorkbook.Worksheets[2]
					CASE tcMode = "ALL"
						oWorksheet = oWorkbook.Worksheets[3]
				ENDCASE				

				.nSQLHandle = SQLCONNECT("PCPAYWIN","MBENNETT","VP737A")

				IF .nSQLHandle > 0 THEN

					lcWhere = ""

					SET TEXTMERGE ON
					TEXT TO	lcSQL NOSHOW
SELECT
STATUS,
COMPANYCODE AS ADP_COMP,
NAME,
FILE# AS FILE_NUM,
GENDER,
{fn ifnull({fn SUBSTRING(CUSTAREA1,1,1)},'?') AS MGMTTYPE,
{fn ifnull({fn SUBSTRING(CUSTAREA1,2,3)},'???') AS TGFDEPT,
{fn ifnull({fn SUBSTRING(CUSTAREA2,1,1)},'F') AS TIMETYPE
FROM REPORTS.V_EMPLOYEE
WHERE COMPANYCODE IN ('E87','E88','E89')
ORDER BY 1, 2, 3
					ENDTEXT
					SET TEXTMERGE OFF




					SET TEXTMERGE ON
					TEXT TO	lcSQL2 NOSHOW
SELECT
COMPANYCODE AS ADP_COMP,
FILE# AS FILE_NUM,
CHECKVIEWHOMEDEPT AS HOMEDEPT,
CHECKVIEWPAYDATE,
{FN IFNULL(SUM(CHECKVIEWHOURSAMT),0.00)} AS CODEHOURS,
{FN IFNULL(MAX(CHECKVIEWREGHOURS),0.00)} AS REGHOURS,
{FN IFNULL(MAX(CHECKVIEWOTHOURS),0.00)} AS OTHOURS
FROM REPORTS.V_CHK_VW_HOURS
WHERE CHECKVIEWPAYDATE >= <<lcADPFromDate>>
AND CHECKVIEWPAYDATE <= <<lcADPToDate>>
<<lcDivWhere>>
GROUP BY COMPANYCODE, FILE#, CHECKVIEWHOMEDEPT, CHECKVIEWPAYDATE
ORDER BY 1,2,4
					ENDTEXT
					SET TEXTMERGE OFF



					SET TEXTMERGE ON
					TEXT TO	lcSQL3 NOSHOW
SELECT
DISTINCT
COMPANYCODE AS ADP_COMP,
SOCIALSECURITY# AS SS_NUM,
CHECKVIEWPAYDATE AS PAYDATE
FROM REPORTS.V_CHK_VW_INFO
WHERE CHECKVIEWPAYDATE >= <<lcADPFromDate>>
AND CHECKVIEWPAYDATE <= <<lcADPToDate>>
ORDER BY 1,2,3
					ENDTEXT
					SET TEXTMERGE OFF

					IF .lTestMode THEN
						.TrackProgress('                        TEST MODE                        ', LOGIT+SENDIT)
						.TrackProgress('lcSQL =' + lcSQL, LOGIT+SENDIT)
						.TrackProgress('lcSQL2 =' + lcSQL2, LOGIT+SENDIT)
						.TrackProgress('lcSQL3 =' + lcSQL3, LOGIT+SENDIT)
					ENDIF

					IF USED('CURCUSTOM') THEN
						USE IN CURCUSTOM
					ENDIF
					IF USED('CURHOURSPRE') THEN
						USE IN CURHOURSPRE
					ENDIF
					IF USED('CURPAYDATESPRE') THEN
						USE IN CURPAYDATESPRE
					ENDIF
					IF USED('CURPAYDATES') THEN
						USE IN CURPAYDATES
					ENDIF
					IF USED('CURHOURS') THEN
						USE IN CURHOURS
					ENDIF
					IF USED('CURHOURS2') THEN
						USE IN CURHOURS2
					ENDIF
					IF USED('CURWHITE') THEN
						USE IN CURWHITE
					ENDIF
					IF USED('CURBLUE') THEN
						USE IN CURBLUE
					ENDIF
					IF USED('CURCNTTYPE') THEN
						USE IN CURCNTTYPE
					ENDIF
					IF USED('CURCNTGENDER') THEN
						USE IN CURCNTGENDER
					ENDIF
					IF USED('CURTEMPSPRE') THEN
						USE IN CURTEMPSPRE
					ENDIF
					IF USED('CURTEMPS') THEN
						USE IN CURTEMPS
					ENDIF
					IF USED('CURTMPHEADCNT') THEN
						USE IN CURTMPHEADCNT
					ENDIF				

					IF .ExecSQL(lcSQL, 'CURCUSTOM', RETURN_DATA_MANDATORY) AND ;
							.ExecSQL(lcSQL2, 'CURHOURSPRE', RETURN_DATA_MANDATORY) AND ;
							.ExecSQL(lcSQL3, 'CURPAYDATESPRE', RETURN_DATA_MANDATORY) THEN

						*!*				SELECT CURCUSTOM
						*!*				BROWSE
						*!*				*COPY TO F:\UTIL\HR\TGFSTAFFRPT\CUSTOMADP.XLS XL5
						*!*				SELECT CURHOURSPRE
						*!*				BROWSE
						
						* DETERMINE # OF HOURLY AND SALARIED PAYDATES IN THE RANGE, USE TO DETERMINE FTE #S						
						
*!*							SELECT ;
*!*								ADP_COMP, ;
*!*								PAYDATE, ;
*!*								COUNT(*) AS HEADCOUNT ;
*!*								FROM CURPAYDATESPRE ;
*!*								INTO CURSOR CURPAYDATES ;
*!*								GROUP BY ADP_COMP, PAYDATE ;
*!*								ORDER BY ADP_COMP, PAYDATE 
						
						* add WHERE to exclude bogus paydates that inflate expected fte hours	
						SELECT ;
							ADP_COMP, ;
							PAYDATE, ;
							COUNT(*) AS HEADCOUNT ;
							FROM CURPAYDATESPRE ;
							INTO CURSOR CURPAYDATES ;
							WHERE TTOD(PAYDATE) NOT IN ;
							(SELECT PAYDATE FROM NOTPAYDATES WHERE FLAGGED) ;
							GROUP BY ADP_COMP, PAYDATE ;
							ORDER BY ADP_COMP, PAYDATE 
							
*!*							SELECT CURPAYDATES
*!*							BROWSE

						
						SELECT CURPAYDATES
						COUNT FOR (ADP_COMP = 'E87') AND (HEADCOUNT > 400) TO lnNumHourlyPaydates
						COUNT FOR (ADP_COMP = 'E88') AND (HEADCOUNT > 150) TO lnNumSalariedPaydates
				
						.TrackProgress('lnNumHourlyPaydates = ' + TRANSFORM(lnNumHourlyPaydates), LOGIT+SENDIT)
						.TrackProgress('lnNumSalariedPaydates = ' + TRANSFORM(lnNumSalariedPaydates), LOGIT+SENDIT)
							

						lnFTEHours_Hourly = lnNumHourlyPaydates * 40
						lnFTEHours_Salaried = lnNumSalariedPaydates * 80

						.TrackProgress('lnFTEHours_Hourly = ' + TRANSFORM(lnFTEHours_Hourly), LOGIT+SENDIT)
						.TrackProgress('lnFTEHours_Salaried = ' + TRANSFORM(lnFTEHours_Salaried), LOGIT+SENDIT)
						
						
						* DEFAULT SALARIED TO 80 HOURS FOR EACH SALARIED PAY BEFORE WE ROLL UP THE DATES
						SELECT CURHOURSPRE
						SCAN
							IF (CURHOURSPRE.ADP_COMP <> 'E87') THEN
								REPLACE CURHOURSPRE.REGHOURS WITH 80
							ENDIF
						ENDSCAN

*!*			SELECT CURHOURSPRE
*!*			BROW						

						* ROLL UP HOURS INFO 
						SELECT ALLTRIM(ADP_COMP) AS ADP_COMP, ;
							'                              ' AS NAME, ;
							FILE_NUM, ;
							ALLTRIM(HOMEDEPT) AS HOMEDEPT, ;
							SUM(CODEHOURS + REGHOURS + OTHOURS) AS TOTHOURS, ;
							1.00 AS HEADCNT, ;
							1.00 AS FTE, ;
							' ' AS GENDER, ;
							' ' AS MGMTTYPE, ;
							' ' AS TIMETYPE, ;
							'   ' AS TGFDEPT ;
							FROM CURHOURSPRE ;
							INTO CURSOR CURHOURS ;
							GROUP BY 1, 2, 3, 4 ;
							ORDER BY 1, 2, 3, 4 ;
							READWRITE

						*!*				SELECT CURHOURS
						*!*				BROWSE

						* POPULATE MISSING INFO FROM OTHER CURSOR AND ALSO CALC FTE

						SELECT CURHOURS
						SCAN						
						
							SELECT CURCUSTOM
							LOCATE FOR ;
								ALLTRIM(ADP_COMP) = ALLTRIM(CURHOURS.ADP_COMP) ;
								AND FILE_NUM = CURHOURS.FILE_NUM ;
								AND ALLTRIM(STATUS) <> 'T'

							IF FOUND() THEN
								REPLACE CURHOURS.TIMETYPE WITH ALLTRIM(CURCUSTOM.TIMETYPE), ;
									CURHOURS.MGMTTYPE WITH ALLTRIM(CURCUSTOM.MGMTTYPE), ;
									CURHOURS.GENDER WITH ALLTRIM(CURCUSTOM.GENDER), ;
									CURHOURS.NAME WITH ALLTRIM(CURCUSTOM.NAME), ;
									CURHOURS.TGFDEPT WITH ALLTRIM(CURCUSTOM.TGFDEPT) ;
									IN CURHOURS
									
								* for full-time / part-time flag, if it's not a P, make it an F
								IF NOT UPPER(ALLTRIM(CURCUSTOM.TIMETYPE)) == "P" THEN
									REPLACE CURHOURS.TIMETYPE WITH "F" IN CURHOURS
								ENDIF
									
							ELSE
								* active status not found, try terminated in case they are recent term
								SELECT CURCUSTOM
								LOCATE FOR ;
									ALLTRIM(ADP_COMP) = ALLTRIM(CURHOURS.ADP_COMP) ;
									AND FILE_NUM = CURHOURS.FILE_NUM ;
									AND ALLTRIM(STATUS) = 'T'

								IF FOUND() THEN
									REPLACE CURHOURS.TIMETYPE WITH ALLTRIM(CURCUSTOM.TIMETYPE), ;
										CURHOURS.MGMTTYPE WITH ALLTRIM(CURCUSTOM.MGMTTYPE), ;
										CURHOURS.GENDER WITH ALLTRIM(CURCUSTOM.GENDER), ;
										CURHOURS.NAME WITH ALLTRIM(CURCUSTOM.NAME), ;
										CURHOURS.TGFDEPT WITH ALLTRIM(CURCUSTOM.TGFDEPT) ;
										IN CURHOURS
									
									* for full-time / part-time flag, if it's not a P, make it an F
									IF NOT UPPER(ALLTRIM(CURCUSTOM.TIMETYPE)) == "P" THEN
										REPLACE CURHOURS.TIMETYPE WITH "F" IN CURHOURS
									ENDIF
									
								ELSE
									* NOTHING CAN BE DONE
									SELECT CURCUSTOM
									LOCATE
								ENDIF

							ENDIF

						ENDSCAN
						
						
						* CALC FTE
						SELECT CURHOURS
						SCAN
							DO CASE
								CASE (CURHOURS.ADP_COMP = 'E87') AND (NOT CURHOURS.HOMEDEPT = '020655')
									* EMPLOYEE IS HOURLY, AND NOT AN OTR DRIVER -- OVERWRITE DEFAULT FTE
									REPLACE CURHOURS.FTE WITH ( CURHOURS.TOTHOURS / lnFTEHours_Hourly ) IN CURHOURS
								CASE INLIST(CURHOURS.ADP_COMP,'E88','E89')
									REPLACE CURHOURS.FTE WITH ( CURHOURS.TOTHOURS / lnFTEHours_Salaried ) IN CURHOURS
								OTHERWISE
									* NOTHING
							ENDCASE
						ENDSCAN
						
*!*	SELECT CURHOURS
*!*	BROW


						* add dept descriptions
						SELECT A.*, IIF(ISNULL(B.DESC),'?',B.DESC) AS DEPTDESC ;
							FROM CURHOURS A ;
							LEFT OUTER JOIN ;
							F:\UTIL\ADPREPORTS\DATA\TGFDEPTS B ;
							ON B.CODE = ALLTRIM(A.TGFDEPT) ;
							INTO CURSOR CURHOURS2 ;
							ORDER BY ADP_COMP, NAME ;
							READWRITE
							
							
						IF (tcMode = "ALL") THEN
							* track some detail info for people with bad/missing data
							SELECT CURHOURS2
							SCAN FOR ('?' $ MGMTTYPE) OR ('?' $ TGFDEPT) OR ('?' $ deptdesc)
								.cBadData = .cBadData + ALLTRIM(CURHOURS2.adp_comp) + ', ' + ALLTRIM(CURHOURS2.name) + ', ' + TRANSFORM(CURHOURS2.FILE_NUM) + ;
									+ ', ' + CURHOURS2.MGMTTYPE + ', ' + CURHOURS2.TGFDEPT+ ', ' + CURHOURS2.deptdesc + CRLF					
							ENDSCAN
						ENDIF
						

						SELECT CURHOURS2
						COPY TO ( "F:\UTIL\HR\TGFSTAFFRPT\REPORTS\DETAIL\tgfhours" + lcSuffix + ".XLS" ) XL5

						* summarize for White Collar
						SELECT "WHITE COLLAR" AS MGMTTYPE, ;
							TGFDEPT, ;
							DEPTDESC, ;
							SUM(FTE) AS FTE, ;
							SUM(HEADCNT) AS HEADCNT, ;
							SUM(TOTHOURS) AS TOTHOURS ;
							FROM CURHOURS2 ;
							INTO CURSOR CURWHITE ;
							WHERE CURHOURS2.MGMTTYPE = "W" ;
							GROUP BY 1, 2, 3 ;
							ORDER BY 1, 3

						SELECT CURWHITE
						COPY TO ( "F:\UTIL\HR\TGFSTAFFRPT\REPORTS\DETAIL\tgfwhitecollar" + lcSuffix + ".XLS" ) XL5

						* summarize for Blue Collar
						SELECT "BLUE COLLAR" AS MGMTTYPE, ;
							TGFDEPT, ;
							DEPTDESC, ;
							SUM(FTE) AS FTE, ;
							SUM(HEADCNT) AS HEADCNT, ;
							SUM(TOTHOURS) AS TOTHOURS ;
							FROM CURHOURS2 ;
							INTO CURSOR CURBLUE ;
							WHERE CURHOURS2.MGMTTYPE <> "W" ;
							GROUP BY 1, 2, 3 ;
							ORDER BY 1, 3

						SELECT CURBLUE
						COPY TO ( "F:\UTIL\HR\TGFSTAFFRPT\REPORTS\DETAIL\tgfblueecollar" + lcSuffix + ".XLS" ) XL5

						* summarize for part-time / full-ime
						SELECT TIMETYPE, ;
							SUM(FTE) AS FTE, ;
							SUM(HEADCNT) AS HEADCNT ;
							FROM CURHOURS2 ;
							INTO CURSOR CURCNTTYPE ;
							GROUP BY 1 ;
							ORDER BY 1

						SELECT CURCNTTYPE
						COPY TO ( "F:\UTIL\HR\TGFSTAFFRPT\REPORTS\DETAIL\tgftimetype" + lcSuffix + ".XLS" ) XL5


						* summarize BY GENDER
						SELECT GENDER, ;
							SUM(FTE) AS FTE, ;
							SUM(HEADCNT) AS HEADCNT ;
							FROM CURHOURS2 ;
							INTO CURSOR CURGENDER ;
							GROUP BY 1 ;
							ORDER BY 1

						SELECT CURGENDER
						COPY TO ( "F:\UTIL\HR\TGFSTAFFRPT\REPORTS\DETAIL\tgfgender"  + lcSuffix + ".XLS" ) XL5

						=SQLDISCONNECT(.nSQLHandle)
						
						
						**********************************************************************************
						**********************************************************************************
						**********************************************************************************
						
						* populate the spreadsheet
						lnRow = lnStartRow
						lcRow = ALLTRIM(STR(lnRow))
						lnMgmtStartRow = lnRow + 1
						lcMgmtStartRow = ALLTRIM(STR(lnMgmtStartRow))
						oWorksheet.RANGE("A"+lcRow).VALUE = "Mgmt. Type"
						oWorksheet.RANGE("B"+lcRow).VALUE = "TGF Dept."
						oWorksheet.RANGE("C"+lcRow).VALUE = "Dept. Description"
						oWorksheet.RANGE("D"+lcRow).VALUE = "FTE"
						oWorksheet.RANGE("E"+lcRow).VALUE = "Headcount"
						IF .lShowHours THEN
							oWorksheet.RANGE("F"+lcRow).VALUE = "Tot Hours"
						ENDIF
						oWorksheet.RANGE("A"+lcRow+":F"+lcRow).FONT.BOLD = .T.
						
						IF (tcMode = "ALL") THEN						
							oWorksheet.RANGE("H3").VALUE = "# of Hourly Paydates = " + TRANSFORM(lnNumHourlyPaydates)
							oWorksheet.RANGE("H5").VALUE = "# of Salaried Paydates = " + TRANSFORM(lnNumSalariedPaydates)
							oWorksheet.RANGE("H7").VALUE = "Hourly FTE Hours = " + TRANSFORM(lnFTEHours_Hourly)
							oWorksheet.RANGE("H9").VALUE = "Salaried FTE Hours = " + TRANSFORM(lnFTEHours_Salaried)
						ENDIF
						
						SELECT CURWHITE
						SCAN
							lnRow = lnRow + 1
							lcRow = ALLTRIM(STR(lnRow))
							oWorksheet.RANGE("A"+lcRow).VALUE = CURWHITE.mgmttype
							oWorksheet.RANGE("B"+lcRow).VALUE = CURWHITE.tgfdept
							oWorksheet.RANGE("C"+lcRow).VALUE = CURWHITE.deptdesc
							oWorksheet.RANGE("D"+lcRow).VALUE = CURWHITE.fte
							oWorksheet.RANGE("E"+lcRow).VALUE = CURWHITE.headcnt
							IF .lShowHours THEN
								oWorksheet.RANGE("F"+lcRow).VALUE = CURWHITE.TOTHOURS
							ENDIF
						ENDSCAN

						SELECT CURBLUE
						SCAN
							lnRow = lnRow + 1
							lcRow = ALLTRIM(STR(lnRow))
							oWorksheet.RANGE("A"+lcRow).VALUE = CURBLUE.mgmttype
							oWorksheet.RANGE("B"+lcRow).VALUE = CURBLUE.tgfdept
							oWorksheet.RANGE("C"+lcRow).VALUE = CURBLUE.deptdesc
							oWorksheet.RANGE("D"+lcRow).VALUE = CURBLUE.fte
							oWorksheet.RANGE("E"+lcRow).VALUE = CURBLUE.headcnt
							IF .lShowHours THEN
								oWorksheet.RANGE("F"+lcRow).VALUE = CURBLUE.TOTHOURS
							ENDIF
						ENDSCAN
												
						lnMgmtEndRow = lnRow
						lcMgmtEndRow = ALLTRIM(STR(lnMgmtEndRow))
						
						lnRow = lnRow + 1
						lcRow = ALLTRIM(STR(lnRow))

						* Ken wants totals for the Mgmt Type section...
						oWorksheet.RANGE("D" + lcRow).VALUE = "=SUM(D" + lcMgmtStartRow + ":D" + lcMgmtEndRow + ")"
						oWorksheet.RANGE("E" + lcRow).VALUE = "=SUM(E" + lcMgmtStartRow + ":E" + lcMgmtEndRow + ")"

						* UNDERLINE cell columns to be totaled...
						oWorksheet.RANGE("D" + lcMgmtEndRow + ":E" + lcMgmtEndRow).BORDERS(xlEdgeBottom).LineStyle = xlContinuous
						oWorksheet.RANGE("D" + lcMgmtEndRow + ":E" + lcMgmtEndRow).BORDERS(xlEdgeBottom).Weight = xlMedium

						lnRow = lnRow + 2
						lcRow = ALLTRIM(STR(lnRow))
						oWorksheet.RANGE("A"+lcRow).VALUE = "Emp. Type"
						oWorksheet.RANGE("B"+lcRow).VALUE = "FTE"
						oWorksheet.RANGE("C"+lcRow).VALUE = "Headcount"
						oWorksheet.RANGE("A"+lcRow+":C"+lcRow).FONT.BOLD = .T.

						SELECT CURCNTTYPE
						SCAN
							lnRow = lnRow + 1
							lcRow = ALLTRIM(STR(lnRow))
							oWorksheet.RANGE("A"+lcRow).VALUE = IIF(CURCNTTYPE.TIMETYPE="F","Full Time","Part Time")
							oWorksheet.RANGE("B"+lcRow).VALUE = CURCNTTYPE.fte
							oWorksheet.RANGE("C"+lcRow).VALUE = CURCNTTYPE.headcnt
						ENDSCAN
						
						lnPreCasualRow = lnRow  && save row for filling in temp data later
						


						**********************************************************************************
						**********************************************************************************
						**********************************************************************************
						

						* GATHER TEMP INFO

						*************************************************************************************
						* NOW GET TEMP INFO FROM KRONOS
						*************************************************************************************
						.nSQLHandle = SQLCONNECT("WTK2","sa","kr0n1tes")

						IF .nSQLHandle > 0 THEN

							lcSQLFromDate = .GetSQLStartDateTimeString( ldFromDate )
							lcSQLToDate = .GetSQLEndDateTimeString( ldToDate )

							SET TEXTMERGE ON
							TEXT TO	lcSQLKronos NOSHOW
SELECT
A.APPLYDTM,
D.LABORLEV5NM AS AGENCYNUM,
D.LABORLEV1NM AS ADP_COMP,
D.LABORLEV2NM AS DIVISION,
C.FULLNM AS EMPLOYEE,
C.PERSONNUM AS FILE_NUM,
E.NAME AS PAYCODEDESC,
(A.DURATIONSECSQTY / 3600.00) AS HOURS
FROM WFCTOTAL A
JOIN WTKEMPLOYEE B
ON B.EMPLOYEEID = A.EMPLOYEEID
JOIN PERSON C
ON C.PERSONID = B.PERSONID
JOIN LABORACCT D
ON D.LABORACCTID = A.LABORACCTID
JOIN PAYCODE E
ON E.PAYCODEID = A.PAYCODEID
WHERE (A.APPLYDTM >= <<lcSQLFromDate>>)
AND (A.APPLYDTM <= <<lcSQLToDate>>)
AND (D.LABORLEV1NM IN ('TMP'))
<<lcKronosDivWhere>>
ORDER BY 1,2,3,4,5
							ENDTEXT
							SET TEXTMERGE OFF

							IF .lTestMode THEN
								.TrackProgress('lcSQLKronos = ' + lcSQLKronos, LOGIT+SENDIT)
							ENDIF

							IF .ExecSQL(lcSQLKronos, 'CURTEMPSPRE', RETURN_DATA_NOT_MANDATORY) THEN

								*SELECT CURTEMPSPRE
								*BROWSE
								
								IF USED('CURTEMPSPRE') AND NOT EOF('CURTEMPSPRE') THEN

									SELECT FILE_NUM, 1.00 AS HEADCNT, 0.00 AS FTE, SUM(HOURS) AS TOTHOURS ;
										FROM CURTEMPSPRE ;
										INTO CURSOR CURTEMPS ;
										GROUP BY 1 ;
										ORDER BY 1 ;
										READWRITE

									* calc FTE
									SELECT CURTEMPS
									SCAN
										REPLACE CURTEMPS.FTE WITH ( CURTEMPS.TOTHOURS / lnFTEHours_Hourly ) IN CURTEMPS
									ENDSCAN

									* final roll up
									SELECT ;
										"Casual" AS TYPE, ;
										SUM(FTE) AS FTE, ;
										SUM(HEADCNT) AS HEADCNT ;
										FROM CURTEMPS ;
										INTO CURSOR CURTMPHEADCNT ;

									SELECT CURTMPHEADCNT
									COPY TO ("F:\UTIL\HR\TGFSTAFFRPT\REPORTS\DETAIL\TGFTEMPS"  + lcSuffix + ".XLS") XL5								
									
									lnRow = lnPreCasualRow			
									SELECT CURTMPHEADCNT
									SCAN
										lnRow = lnRow + 1
										lcRow = ALLTRIM(STR(lnRow))
										oWorksheet.RANGE("A"+lcRow).VALUE = "Casual"
										oWorksheet.RANGE("B"+lcRow).VALUE = CURTMPHEADCNT.fte
										oWorksheet.RANGE("C"+lcRow).VALUE = CURTMPHEADCNT.headcnt
									ENDSCAN
								
								ELSE
									lnRow = lnPreCasualRow			
									lnRow = lnRow + 1
									lcRow = ALLTRIM(STR(lnRow))
									oWorksheet.RANGE("A"+lcRow).VALUE = "Casual"
									oWorksheet.RANGE("B"+lcRow).VALUE = 0.0
									oWorksheet.RANGE("C"+lcRow).VALUE = 0
								
								ENDIF && USED('CURTEMPSPRE') AND NOT EOF('CURTEMPSPRE')
								
								lnRow = lnRow + 2
								lcRow = ALLTRIM(STR(lnRow))
								oWorksheet.RANGE("A"+lcRow).VALUE = "Gender"
								oWorksheet.RANGE("B"+lcRow).VALUE = "FTE"
								oWorksheet.RANGE("C"+lcRow).VALUE = "Headcount"
								oWorksheet.RANGE("A"+lcRow+":C"+lcRow).FONT.BOLD = .T.

								SELECT CURGENDER
								SCAN
									lnRow = lnRow + 1
									lcRow = ALLTRIM(STR(lnRow))
									oWorksheet.RANGE("A"+lcRow).VALUE = CURGENDER.gender
									oWorksheet.RANGE("B"+lcRow).VALUE = CURGENDER.fte
									oWorksheet.RANGE("C"+lcRow).VALUE = CURGENDER.headcnt
								ENDSCAN
								

							ENDIF  && .ExecSQL

							=SQLDISCONNECT(.nSQLHandle)

						ENDIF && .nSQLHandle > 0 THEN

						CLOSE DATABASES ALL
					ENDIF  &&  .ExecSQL(lcSQL, 'SQLCURSOR1', RETURN_DATA_MANDATORY)
					
					.TrackProgress("TGF Staffing Report process ended normally.", LOGIT+SENDIT+NOWAITIT)
					
				ELSE
					* connection error
					.TrackProgress('Unable to connect to ADP Payroll System.', LOGIT+SENDIT)
				ENDIF   &&  .nSQLHandle > 0

			CATCH TO loError

				.TrackProgress('There was an error.',LOGIT+SENDIT)
				.TrackProgress('The Error # is: ' + TRANSFORM(loError.ERRORNO), LOGIT+SENDIT)
				.TrackProgress('The Error Message is: ' + TRANSFORM(loError.MESSAGE), LOGIT+SENDIT)
				.TrackProgress('The Error occurred in Program: ' + TRANSFORM(loError.PROCEDURE), LOGIT+SENDIT)
				.TrackProgress('The Error occurred at line #: ' + TRANSFORM(loError.LINENO), LOGIT+SENDIT)
				lnNumberOfErrors = lnNumberOfErrors + 1
				
				IF (tcMode = "ALL") THEN
					IF TYPE('oWorkbook') = "O" THEN
						oWorkbook.SAVE()
					ENDIF
					IF TYPE('oExcel') = "O" THEN
						oExcel.QUIT()
					ENDIF
					CLOSE DATABASES ALL
				ENDIF
				
			ENDTRY

			CLOSE DATA
			WAIT CLEAR
			***************** INTERNAL email results ******************************

			* this block must get done only once, at the end of the process...
			IF (tcMode = "ALL") THEN

				IF (lnNumberOfErrors = 0) THEN
					* SAVE AND QUIT EXCEL
					oWorkbook.SAVE()
					oExcel.QUIT()
				ENDIF

				IF FILE(lcFiletoSaveAs) THEN
					.cAttach = lcFiletoSaveAs
					.cBodyText = "See attached report." + CRLF + CRLF + "<<REPORT LOG FOLLOWS>>" + CRLF + .cBodyText
				ELSE
					.TrackProgress("ERROR attaching " + lcFiletoSaveAs, LOGIT+SENDIT+NOWAITIT)
				ENDIF

				IF NOT EMPTY(.cBadData) THEN
					.cBadData = 'WARNING: the following people had bad or missing data elements:' + CRLF + CRLF + ;
						'ADP_COMP, NAME, FILE_NUM, MGMTTYPE, TGFDEPT, DEPTDESC' + CRLF + .cBadData
					.cBodyText = .cBadData + CRLF + .cBodyText
				ENDIF

				.TrackProgress('About to send status email.',LOGIT)
				.TrackProgress('The logfile is: ' + .cLogFile, SENDIT)
				.TrackProgress("TGF Staffing Report process started: " + .cStartTime, LOGIT+SENDIT)
				.TrackProgress("TGF Staffing Report process finished: " + TTOC(DATETIME()), LOGIT+SENDIT)
				
				IF .lSendInternalEmailIsOn THEN
					DO FORM dartmail2 WITH .cSendTo,.cFrom,.cSubject,.cCC,.cAttach,.cBodyText,"A"
					.TrackProgress('Sent status email.',LOGIT)
				ELSE
					.TrackProgress('Did not send status email: THIS.lSendInternalEmailIsOn = FALSE.',LOGIT)
				ENDIF && tcMode = "ALL"
			ENDIF

		ENDWITH
		RETURN
	ENDFUNC && main


	FUNCTION ExecSQL
		LPARAMETERS tcSQL, tcCursorName, tlNoDataReturnedIsOkay
		LOCAL llRetval, lnResult
		WITH THIS
			* close target cursor if it's open
			IF USED(tcCursorName)
				USE IN (tcCursorName)
			ENDIF
			WAIT WINDOW "Fetching remote data; please wait..." NOWAIT
			lnResult = SQLEXEC(.nSQLHandle, tcSQL, tcCursorName)
			llRetval = ( lnResult > 0 )
			IF llRetval THEN
				* see if any data came back
				IF NOT tlNoDataReturnedIsOkay THEN
					IF (NOT USED(tcCursorName)) OR EOF(tcCursorName) THEN
						llRetval = .F.
						.TrackProgress('No data was returned by this query: ' +CRLF + tcSQL, LOGIT+SENDIT)
						.cSendTo = 'Mark Bennett <mbennett@fmiint.com>'
					ENDIF
				ENDIF
			ELSE
				.TrackProgress("Result Code [" + TRANSFORM(lnResult) + "] returned by this query: " + tcSQL, LOGIT+SENDIT)
				.cSendTo = 'Mark Bennett <mbennett@fmiint.com>'
			ENDIF
			WAIT CLEAR
			RETURN llRetval
		ENDWITH
	ENDFUNC


	PROCEDURE TrackProgress
		* do any combination of Wait Window, writing to logfile, and adding to body of email,
		* based on nFlags parameter.
		LPARAMETERS tcExpression, tnFlags
		WITH THIS
			IF BITAND(tnFlags,LOGIT) = LOGIT THEN
				IF .lLoggingIsOn THEN
					?
					? .cProcessName + "  " + TTOC(DATETIME()) + ": " + tcExpression
				ENDIF
			ENDIF
			IF .lWaitWindowIsOn THEN
				IF BITAND(tnFlags,NOWAITIT) = NOWAITIT THEN
					WAIT WINDOW tcExpression NOWAIT
				ENDIF
				IF BITAND(tnFlags,WAITIT) = WAITIT THEN
					WAIT WINDOW tcExpression TIMEOUT .nWaitWindowTimeout
				ENDIF
			ENDIF
			IF BITAND(tnFlags,SENDIT) = SENDIT THEN
				IF .lSendInternalEmailIsOn THEN
					.cBodyText = .cBodyText + tcExpression + CRLF + CRLF
				ENDIF
			ENDIF
		ENDWITH
	ENDPROC  &&  TrackProgress


	FUNCTION GetSQLStartDateTimeString
		* returns a timestamp string for use in SQL queries
		LPARAMETERS tdDate
		LOCAL lcSetDate, lcSQLDateTimeString
		lcSetDate = SET('DATE')
		SET DATE YMD
		lcSQLDateTimeString   = "{ts '" + TRANSFORM(YEAR(tdDate)) + "-" + PADL(MONTH(tdDate),2,"0") + "-" + PADL(DAY(tdDate),2,"0") + " 00:00:00.0'}"
		SET DATE &lcSetDate.
		RETURN lcSQLDateTimeString
	ENDFUNC


	FUNCTION GetSQLEndDateTimeString
		* returns a timestamp string for use in SQL queries
		LPARAMETERS tdDate
		LOCAL lcSetDate, lcSQLDateTimeString
		lcSetDate = SET('DATE')
		SET DATE YMD
		lcSQLDateTimeString = "{ts '" + TRANSFORM(YEAR(tdDate)) + "-" + PADL(MONTH(tdDate),2,"0") + "-" + PADL(DAY(tdDate),2,"0") + " 23:59:59.9'}"
		SET DATE &lcSetDate.
		RETURN lcSQLDateTimeString
	ENDFUNC


ENDDEFINE
