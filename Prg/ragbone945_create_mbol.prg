*!* Rag&Bone 945 MBOL Creation Program
*!* Creation Date: 08.15.2017, Joe from BBC 945 MBOL prg

PARAMETERS cBOL

PUBLIC ARRAY thisarray(1)
PUBLIC c_CntrlNum,c_GrpCntrlNum,cReturned,cWO_Num,cWO_NumStr,nWO_Num1,lROFilesOut,lPrepack
PUBLIC nUnitsum,nCtnCount,tsendto,tcc,tsendtoerr,tccerr,lTesting,lTestinput,nPTCount,cMBOL,nAcctNum,cSubBOLDetail
PUBLIC nOrigSeq,nOrigGrpSeq,cProgname,cISA_Num,cRefBOL,cMailName,cOutPath,cHoldPath,cArchivePath
PUBLIC cMod,cEDIType,lParcelType,cFilenameShort,cFilenameOut,cFilenameArch,lcPath,cWO_NumList,lRBFilesOut
WAIT WINDOW "R&B MBOL 945 Process" TIMEOUT 2

lOverflow = .F.

lTesting   = .f.  && Set to .t. for testing
lTestinput = .F. && lTesting  &&  Set to .t. for test input files only!
lEmail = .t. && !lTesting
lTestmail = lTesting

DO m:\dev\prg\_setvars WITH lTesting

IF !lTesting
	SET ASSERTS OFF
ENDIF

SET DELETED ON
cProgname = "ragbone945_create_mbol"
IF lTesting
	ASSERT .F. MESSAGE "At beginning of R&B 945 creation"
ENDIF
SET ESCAPE ON
ON ESCAPE CANCEL
*ON ERROR debug
lIsError = .F.
lDoCatch = .T.
cShip_ref = ""
lCloseOutput = .T.
STORE 0 TO nFilenum,nOrigSeq,nOrigGrpSeq,nPTCount
cCarrierType = "M" && Default as "Motor Freight"
ccustname = ""
cWO_NumStr = ""
cWO_NumList = ""
cStatus=""
cRefBOL = '9999'

TRY
	lFederated = .F.
	cPPName = "RAGBONE"
	tfrom = "TGF Warehouse Operations <toll-edi-ops@tollgroup.com>"
	tmessage=""
	tcc = ""
	lDoBOLGEN = .F.

	lISAFlag = .T.
	lSTFlag = .T.
	nLoadid = 1
	lSQLMail = .F.
	lDoCompare = IIF(lTesting OR lTestinput,.F.,.T.)

	nAcctNum = 6699
	lDoCompare = .F.
	cOffice = "N"
	cMod = "I"
	gMasterOffice = cOffice
	goffice = cMod
	cEDIType = "945"
	lParcelType = .F.

	IF VARTYPE(cBOL) = "L"
		IF !lTesting  && AND DATE()<>{^2006-10-20}
			WAIT WINDOW "Office not provided...terminating" TIMEOUT 3
			lCloseOutput = .F.
			DO ediupdate WITH "No OFFICE",.T.
			THROW
		ELSE
			CLOSE DATABASES ALL
			IF !USED('edi_trigger')
				cEDIFolder = "F:\3PL\DATA\"
				USE (cEDIFolder+"edi_trigger") IN 0 ALIAS edi_trigger
			ENDIF
			DO m:\dev\prg\lookups
*!* TEST DATA AREA
			cBOL = "08660002200029142"
			cTime = DATETIME()
		ENDIF
	ENDIF

	cBOL=TRIM(cBOL)
	cMBOL =ALLTRIM(cBOL)
	cSubBOLDetail =  ""

	CREATE CURSOR temp945 (accountid i,isa_num c(10),st_num c(10),wo_num i,bol_no c(20),ship_ref c(20),filename c(50),filedate T)
	cSCAC = ""

	swc_cutctns(cBOL)

	IF USED('OUTSHIP')
		USE IN outship
	ENDIF

		csq1 = [select * from outship where accountid = 6699 and office = ']+cOffice+[' and bol_no = ']+cBOL+[']
		xsqlexec(csq1,,,"wh")
		IF RECCOUNT() = 0
			cErrMsg = "MISS BOL "+cBOL
			DO ediupdate WITH cErrMsg,.T.
			THROW
		ENDIF
		INDEX ON bol_no TAG bol_no
		INDEX ON ship_ref TAG ship_ref
		INDEX ON wo_num TAG wo_num

  IF !USED('BL')
		csq1 = [select * from bl where mblnum = ']+cMBOL+[' and accountid = 6699]
		xsqlexec(csq1,,,"wh")
		SELECT bl
		LOCATE
		COUNT TO nSubBillCnt FOR bl.mblnum = cMBOL
		LOCATE
	ENDIF

	SELECT outship
	IF SEEK(cBOL,'outship','bol_no')
		cSCAC = TRIM(outship.scac)
	ELSE
		SET STEP ON
		DO ediupdate WITH "BAD SCAC",.T.
		THROW
	ENDIF
	SELECT outship
	SCAN FOR outship.bol_no = cBOL
		IF EMPTY(outship.dcnum)
			REPLACE outship.dcnum WITH '9999' IN outship NEXT 1
			REPLACE outship.storenum WITH 9999 IN outship NEXT 1
		ENDIF
	ENDSCAN
	LOCATE

	SELECT DISTINCT ship_ref FROM outship WHERE bol_no = cBOL INTO CURSOR temp1ms
	SELECT temp1ms
	SELECT 0
	CREATE CURSOR ovpts (outshipid N(10))
	SELECT ovpts
	INDEX ON outshipid TAG outshipid

	SELECT temp1ms
	SCAN
		cShip_ref = ALLTRIM(temp1ms.ship_ref)
		SELECT outship
		SCAN FOR outship.ship_ref = cShip_ref
			IF "OV"$outship.ship_ref
				INSERT INTO ovpts (outshipid) VALUES (outship.outshipid)
				WAIT WINDOW "Overflow PT Found at Ship_ref: "+ALLTRIM(outship.ship_ref) NOWAIT
				EXIT
			ENDIF
		ENDSCAN
	ENDSCAN
	USE IN temp1ms
	cRefBOL = IIF(lOverflow,cBOL,"9999")

	SELECT bol_no,wo_num ;
		FROM outship WHERE bol_no = cBOL ;
		AND accountid = nAcctNum ;
		GROUP BY 1,2 ;
		ORDER BY 1,2 ;
		INTO CURSOR sqldc
	LOCATE
	SCAN
		nWO_Num = sqldc.wo_num
		IF lDoCompare
			cRetMsg = "X"
			DO m:\dev\prg\sqldata-COMPARE WITH "",cBOL,nWO_Num,cOffice,nAcctNum,cPPName
			IF cRetMsg<>"OK"
				lCloseOutput = .T.
				cWO_Num = ALLTRIM(STR(nWO_Num1))
				DO ediupdate WITH "QTY COMPARE ERROR",.T.
				THROW
			ENDIF
		ENDIF
	ENDSCAN

	cMailName = "Rag&Bone"
	SELECT 0
	USE F:\3pl\DATA\mailmaster ALIAS mm SHARED
	IF lTesting OR lTestmail
		LOCATE FOR mm.edi_type = "MISC" AND mm.taskname = "GENERAL"
		lUseAlt = mm.use_alt
		tsendto = IIF(lUseAlt,mm.sendtoalt,mm.sendto)
		tcc = IIF(lUseAlt,mm.ccalt,mm.cc)
		tsendtoerr = tsendto
		tccerr = tcc
		LOCATE
		LOCATE FOR mm.edi_type = "945" AND mm.office = cOffice AND mm.accountid = nAcctNum
		cOutPath = ALLTRIM(mm.basepath)
		cArchivePath = ALLTRIM(mm.archpath)
		cHoldPath = ALLTRIM(mm.holdpath)+"mbolhold\"
	ELSE
		LOCATE FOR mm.edi_type = "945" AND mm.office = "N" AND mm.accountid = nAcctNum
		IF !FOUND()
			DO ediupdate WITH "MAILMASTER ACCT PROBLEM",.T.
			THROW
		ENDIF
		lUseAlt = mm.use_alt
		tsendto = IIF(lUseAlt,mm.sendtoalt,mm.sendto)
		tcc = IIF(lUseAlt,mm.ccalt,mm.cc)
		cOutPath = ALLTRIM(mm.basepath)
		cArchivePath = ALLTRIM(mm.archpath)
		cHoldPath = ALLTRIM(mm.holdpath)+"mbolhold\"
	ENDIF

	CD [&cHoldPath]
	DELETE FILE *.*

	LOCATE
	LOCATE FOR office = 'X' AND accountid = 9999
	lUseAlt = mm.use_alt
	tsendtoerr = IIF(lUseAlt,mm.sendtoalt,mm.sendto)
	tccerr = IIF(lUseAlt,mm.ccalt,mm.cc)
	LOCATE
	LOCATE FOR mm.edi_type = "MISC" AND taskname = "GENERAL"
	lUseAlt = mm.use_alt
	tsendtotest = IIF(lUseAlt,mm.sendtoalt,mm.sendto)
	tcctest = IIF(lUseAlt,mm.ccalt,mm.cc)
	USE IN mm

*!* Mail sending for all MBOL 945s
	IF !lTesting
		tsubject2 = "NOTICE: R&B 945 Process for MBOL"
		tattach2 = " "
		tsendto2 = tsendtotest
		tcc2 = tcctest
		tmessage2 = "Potential files for MBOL# "+cBOL+"...monitor output for correct number of files."
		tmessage2 = tmessage2 + CHR(13) + "Expected number of 945 files: "+ALLTRIM(STR(nSubBillCnt))
		tmessage2 = tmessage2 + CHR(13) + "Processed at "+TTOC(DATETIME())
		DO FORM m:\dev\frm\dartmail2 WITH tsendto2,tfrom,tsubject2,tcc2,tattach2,tmessage2,"A"
	ENDIF
*!* End Mailing section

	cCarrierType = IIF(lParcelType,"U","M")

	IF TYPE("nWO_Num")<> "N"
		lCloseOutput = .T.
		DO ediupdate WITH "BAD WO#",.T.
		THROW
	ENDIF
	cWO_Num = ALLTRIM(STR(nWO_Num))

	lTestmail = lTesting && Sends mail to Joe only
	lRBFilesOut = !lTesting && If true, copies output to FTP folder (default = .t.)
	lStandalone = lTesting

*!*		lTestMail = .T.
*!*	lRBFilesOut = .F.

	STORE "LB" TO cWeightUnit
	lPrepack = .F.

	WAIT WINDOW "Now preparing tables and variables" NOWAIT NOCLEAR

*!* SET CUSTOMER CONSTANTS
	ccustname = UPPER("RAGBONE")  && Customer Identifier
	cX12 = "004050"  && X12 Standards Set used

*!* SET OTHER CONSTANTS
	DO CASE
		CASE cOffice = "M"
			cCustLoc =  "FL"
			cFMIWarehouse = ""
			cCustPrefix = "TG"
			cDivision = "Florida"
			cFolder = "WHM"
			cSF_Addr1  = "11400 NW 32ND AVE"
			cSF_CSZ    = "MIAMI+FL+33167"

		CASE cOffice = "N"
			cCustLoc =  "NJ"
			cFMIWarehouse = ""
			cCustPrefix = "TG"
			cDivision = "New Jersey"
			cFolder = "WHI"
			cSF_Addr1  = "800 FEDERAL BLVD"
			cSF_CSZ    =  "CARTERET+NJ+07008"

		CASE cOffice = "C"
			cCustLoc =  "CA"
			cFMIWarehouse = ""
			cCustPrefix = "945"+"c"
			cDivision = "San Pedro"
			cFolder = "WH2"
			cSF_Addr1  = "450 WESTMONT DRIVE"
			cSF_CSZ    = "SAN PEDRO+CA+90731"
		OTHERWISE
			cCustLoc =  "ML"
			cFMIWarehouse = ""
			cCustPrefix = "TG"
			cDivision = "Mira Loma"
			cFolder = "WHL"
			cSF_Addr1  = "3355 DULLES DR"
			cSF_CSZ    = "MIRA LOMA+CA+91752"

	ENDCASE
	cCustFolder = UPPER(ccustname)
	lNonEDI = .F.  && Assumes not a Non-EDI 945
	STORE "" TO lcKey
	STORE 0 TO alength,nLength
	cTime = DATETIME()
	cDelTime = SUBSTR(TTOC(cTime,1),9,4)
	cApptNum = ""

*!* SET OTHER CONSTANTS
	cString = ""

	csendqual = "ZZ"
	csendid = "TGFUSA"
	crecqual = "12"
	crecid = "2122788218"

	cfd = "|"
	csegd = ""
	cterminator = ">" && Used at end of ISA segment

	nSegCtr = 0
	cdate = DTOS(DATE())
	cTruncDate = RIGHT(cdate,6)
	cTruncTime = SUBSTR(TTOC(DATETIME(),1),9,4)
	cfiledate = cdate+cTruncTime
	cOrig = "J"
	nSTCount = 0
	cStyle = ""
	cPTString = ""
	nTotCtnWt = 0
	nTotCtnCount = 0
	nUnitsum = 0
	cTargetStyle = ""

** PAD ID Codes

	IF !SEEK(cBOL,'outship','bol_no')
		WAIT WINDOW "BOL not found in OUTSHIP" TIMEOUT 2
		DO ediupdate WITH "BOL NOT FOUND",.T.
		THROW
	ENDIF

	SELECT dcnum FROM outship WHERE outship.bol_no = cBOL GROUP BY 1 INTO CURSOR tempx
	LOCATE

	IF EOF()
		DO ediupdate WITH "EMPTY DC/STORE#",.T.
		THROW
	ENDIF

	IF EMPTY(tempx.dcnum)
		USE IN tempx
		SELECT storenum FROM outship WHERE outship.bol_no = cBOL GROUP BY 1 INTO CURSOR tempx
		IF EOF()
			DO ediupdate WITH "EMPTY DC/STORE#",.T.
			THROW
		ENDIF

		IF EMPTY(tempx.storenum)
			DO ediupdate WITH "EMPTY DC/STORE#",.T.
			THROW
		ELSE
			IF RECCOUNT('tempx')  > 1 AND cBOL<>"04907314677854548"
				DO ediupdate WITH "MULTIPLE DC #'s",.T.
				THROW
			ENDIF
		ENDIF
	ENDIF

	IF USED('tempx')
		USE IN tempx
	ENDIF

	IF USED('OUTDET')
		USE IN outdet
	ENDIF
		selectoutdet()
		SELECT outdet
		INDEX ON outdetid TAG outdetid

&& Added code blocks to the following to overcome BOL vs. WO selection issues, 12.23.2005
	SELECT outship
	LOCATE
	IF USED("sqlwo")
		USE IN sqlwo
	ENDIF
	DELETE FILE "F:\3pl\DATA\sqlwo.dbf"
	SELECT bol_no,wo_num ;
		FROM outship ;
		WHERE bol_no = cBOL ;
		AND INLIST(accountid,nAcctNum) ;
		GROUP BY 1,2 ;
		ORDER BY 1,2 ;
		INTO DBF F:\3pl\DATA\sqlwo
	USE IN sqlwo
	SELECT 0
	USE F:\3pl\DATA\sqlwo ALIAS sqlwo
	IF lTesting
		LOCATE
		IF EOF()
			BROWSE
		ENDIF
	ENDIF

	cRetMsg = ""

	DO m:\dev\prg\sqlconnect_bol  WITH nAcctNum,ccustname,cPPName,.T.,cOffice

	IF cRetMsg<>"OK"
		DO ediupdate WITH "SQLCONN: "+cRetMsg,.T.
		THROW
	ENDIF

	SELECT vragbonepp
	IF lTesting
		cWinMsg = "Browsing SQL Data file"
*ASSERT .F. MESSAGE cWinMsg
*WAIT WINDOW cWinMsg TIMEOUT 2
*BROWSE
	ENDIF

	#IF 0
		SELECT wo_num FROM vragbonepp GROUP BY 1 INTO CURSOR temprunid1
		SELECT runid FROM vragbonepp GROUP BY 1 INTO CURSOR temprunid2
		SELECT temprunid1
		STORE RECCOUNT() TO nWORecs
		SELECT temprunid2
		STORE RECCOUNT() TO nRIDRecs

		IF nWORecs#nRIDRecs
			DO ediupdate WITH "MULTIPLE RUNIDS",.T.
			THROW
		ENDIF
		USE IN temprunid1
		USE IN temprunid2
	#ENDIF

	IF !USED("scacs")
		xsqlexec("select * from scac","scacs",,"wh")
		INDEX ON scac TAG scac
	ENDIF

	SELECT outship
	LOCATE
	WAIT CLEAR
	WAIT WINDOW "Now creating Header information" NOWAIT NOCLEAR

	SELECT outship
	LOCATE

	IF !SEEK(nWO_Num,'outship','wo_num')
		WAIT CLEAR
		WAIT WINDOW "Invalid Work Order Number - Not Found in OUTSHIP!" TIMEOUT 2
		DO ediupdate WITH "WO "+TRANSFORM(nWO_Num)+" NOT FOUND",.T.
		THROW
	ENDIF

	IF !lTesting
		IF EMPTY(cBOL)  && May be empty if triggered from PT screen, or FedEx shipment
			WAIT CLEAR
			WAIT WINDOW "MISSING Bill of Lading Number!" TIMEOUT 2
			DO ediupdate WITH "BOL# EMPTY",.T.
			THROW
		ELSE
			IF !SEEK(PADR(TRIM(cBOL),20),"outship","bol_no")
				WAIT CLEAR
				WAIT WINDOW "Invalid Bill of Lading Number - Not Found!" TIMEOUT 2
				DO ediupdate WITH "BOL# NOT FOUND",.T.
				THROW
			ENDIF
		ENDIF
	ENDIF

*************************************************************************
*1  PTIDSOURCE/OUTSHIP RECORD MAIN LOOP
*  Loops through individual OUTSHIP lines for BOL#
*************************************************************************
	WAIT CLEAR
	WAIT WINDOW "Now creating MBOL#-based information..." NOWAIT NOCLEAR

	SELECT bl
	LOCATE
	cMissDel = ""

	oscanstr = "bl.mblnum = cMBOL"

	nBLID = 0
	nSubRec = 0
	ASSERT .F. MESSAGE "At MBOL scan"
	SCAN FOR &oscanstr
		cBOL = TRIM(bl.bol_no)
		IF cMBOL = '9999' AND cBOL # '08660002200029074'  && to select only a single sub-BOL to reprocess
			LOOP
		ENDIF
		nBLID = bl.blid
		WAIT WINDOW "Now processing Sub-BOL #"+cBOL TIMEOUT 2
*!* HEADER LEVEL EDI DATA
		DO num_incr_isa

		cISA_Num = PADL(c_CntrlNum,9,"0")
		nISA_Num = INT(VAL(cISA_Num))

		cISACode = IIF(lTesting,"T","P")
		dt2 = DATETIME()
		dt1 = TTOC(dt2,1)
		dtmail = TTOC(dt2)

		csendidlong = PADR(csendid,15," ")
		crecidlong = PADR(crecid,15," ")

		lcPath = cOutPath
		cfilenamehold = (cHoldPath+cCustPrefix+dt1+".945")
		cFilenameShort = JUSTFNAME(cfilenamehold)
		cFilenameOut = (lcPath+cFilenameShort)
		cFilenameArch = (cArchivePath+cFilenameShort)
		nFilenum = FCREATE(cfilenamehold)


		STORE "ISA"+cfd+"00"+cfd+"          "+cfd+"00"+cfd+"          "+cfd+csendqual+cfd+csendidlong+cfd+;
			crecqual+cfd+crecidlong+cfd+cTruncDate+cfd+cTruncTime+cfd+"U"+cfd+"00401"+cfd+;
			cISA_Num+cfd+"0"+cfd+cISACode+cfd+cterminator+csegd TO cString
		DO cstringbreak

		STORE "GS"+cfd+"SW"+cfd+csendid+cfd+crecid+cfd+cdate+cfd+cTruncTime+cfd+c_CntrlNum+;
			cfd+"X"+cfd+cX12+csegd TO cString
		DO cstringbreak

		xsqlexec("select * from bldet where mod='"+goffice+"' and blid="+TRANSFORM(nBLID),,,"wh")

		SELECT bldet
		SCAN FOR bldet.blid = nBLID AND bldet.accountid = nAcctNum

			cShip_ref = ALLTRIM(bldet.ship_ref)
			IF INLIST(cShip_ref,'593433','593436','593431','593438')
			LOOP
			ENDIF
			
			nPTCount = nPTCount+1

			SELECT outship
			SET ORDER TO TAG wo_num
			LOCATE
			IF !SEEK(nWO_Num)
				WAIT CLEAR
				WAIT WINDOW "Invalid Work Order Number - Not Found in OUTSHIP!" TIMEOUT 2
				DO ediupdate WITH "WO# NOT FOUND",.T.
				THROW
			ENDIF
			SET ORDER TO
			LOCATE
			LOCATE FOR outship.ship_ref = cShip_ref AND accountid =nAcctNum
			IF !FOUND()
				SET STEP ON 
				DO ediupdate WITH "MISS SHIP_REF: "+cShip_ref,.T.
				THROW
			ENDIF
			SCATTER MEMVAR MEMO

				csq1 = [select * from outwolog where accountid = ]+TRANSFORM(nAcctNum)+[ and wo_num = ]+TRANSFORM(outship.wo_num)
				xsqlexec(csq1,,,"wh")
				IF RECCOUNT() = 0
					cErrMsg = "MISS OUTWOLOG REC FOR WO# "+cWO_Num
					DO ediupdate WITH cErrMsg,.T.
					THROW
				ENDIF
				SELECT outwolog
				INDEX ON wo_num TAG wo_num

			IF SEEK(m.wo_num,"outwolog","wo_num")
				lPick = IIF(outwolog.picknpack,.T.,.F.)
				lPrepack = IIF(lPick,.F.,.T.)
			ENDIF


*!* Added this code to trap miscounts in OUTDET Units
			IF lPrepack
				SELECT outdet
				SET ORDER TO
				SUM totqty TO nUnitTot1 FOR units AND outdet.outshipid = outship.outshipid
				SUM (VAL(PACK)*totqty) TO nUnitTot2 FOR !units AND outdet.outshipid = outship.outshipid
				IF nUnitTot1<>nUnitTot2
					DO ediupdate WITH "OUTDET TOTQTY ERR",.T.
					THROW
				ENDIF
				SELECT vcourtpp
				SUM totqty TO nUnitTot2 FOR units AND vcourtpp.outshipid = outship.outshipid
				IF nUnitTot1<>nUnitTot2
					DO ediupdate WITH "SQL TOTQTY ERR",.T.
					THROW
				ENDIF
				LOCATE
			ENDIF
			SELECT outdet
			SET ORDER TO outdetid
			LOCATE
			SELECT outship
*!* End code addition
			cPO_Num = ALLTRIM(m.cnee_ref)


			SELECT outship
			IF !lTesting
				COUNT TO N FOR outship.bol_no = cMBOL AND !emptynul(del_date)
			ELSE
				COUNT TO N FOR outship.bol_no = cMBOL
			ENDIF

			IF N=0
				DO ediupdate WITH "INCOMP BOL",.T.
				THROW
			ENDIF

			LOCATE
			cMissDel = ""

*	oscanstr = "outship.bol_no = PADR(cBOL,20) AND !EMPTYnul(outship.del_date) AND INLIST(outship.accountid,nAcctNum)"

			SELECT outship
			nWO_Num = m.wo_num
			cWO_Num = ALLTRIM(STR(nWO_Num))
			nAcctNum = outship.accountid
			IF !(cWO_Num$cWO_NumStr)
				cWO_NumStr = IIF(EMPTY(cWO_NumStr),cWO_Num,cWO_NumStr+","+cWO_Num)
			ENDIF
			IF !(cWO_Num$cWO_NumList)
				cWO_NumList = IIF(EMPTY(cWO_NumList),cWO_Num,cWO_NumList+CHR(13)+cWO_Num)
			ENDIF

			cPO_Num = ALLTRIM(m.cnee_ref)
			cShip_ref = ALLTRIM(m.ship_ref)

*!* Added this code to trap miscounts in OUTDET/SQL\
			WAIT WINDOW "Now summing outdet/SQL by outshipid" NOWAIT

			IF lPrepack
				IF SEEK(outship.outshipid,'ovpts','outshipid') OR cBOL = '04907315910258581'
					SET DELETED OFF
					cRefBOL = cBOL
				ELSE
					SET DELETED ON
				ENDIF
				SELECT outdet
				IF SET("Deleted") = "OFF"
					SUM origqty TO nUnitTot1 ;
						FOR !units ;
						AND outdet.outshipid = outship.outshipid ;
						AND outdet.wo_num = outship.wo_num
				ELSE
					SUM totqty TO nUnitTot1 ;
						FOR !units ;
						AND outdet.outshipid = outship.outshipid ;
						AND outdet.wo_num = outship.wo_num
				ENDIF
				SELECT vragbonepp
				IF INLIST(nWO_Num,5049591)
					SUM (totqty/INT(VAL(PACK))) TO nUnitTot2 FOR vragbonepp.outshipid = outship.outshipid
				ELSE
					COUNT TO nUnitTot2 FOR vragbonepp.outshipid = outship.outshipid AND vragbonepp.totqty > 0 AND vragbonepp.ucc # 'CUTS'
				ENDIF

				IF nUnitTot1<>nUnitTot2
					SET STEP ON
					ASSERT .F. MESSAGE "At SQL UNITQTY ERR"
					DO ediupdate WITH "SQL UNITQTY ERR-OSID "+TRANSFORM(outship.outshipid),.T.
					THROW
				ENDIF
			ELSE
*      ASSERT .f. MESSAGE "At OSID Qty scanning main loop"
				SELECT outdet
				SUM totqty TO nUnitTot1 ;
					FOR units ;
					AND outdet.outshipid = outship.outshipid ;
					AND outdet.wo_num = outship.wo_num
				SELECT vragbonepp
				SUM totqty TO nUnitTot2 ;
					FOR vragbonepp.outshipid = outship.outshipid ;
					AND vragbonepp.totqty > 0 ;
					AND vragbonepp.ucc # 'CUTS'
				IF nUnitTot1<>nUnitTot2
					ASSERT .F. MESSAGE "At SQL TOTQTY ERR"
					SET STEP ON
					DO ediupdate WITH "SQL TOTQTY ERR-PIK",.T.
					THROW
				ENDIF
			ENDIF
			SELECT outdet
			SET ORDER TO outdetid
			LOCATE

*!* End code addition
			
			SELECT outship
			
			IF ("PENNEY"$UPPER(outship.consignee) OR "KMART"$UPPER(outship.consignee) OR "K-MART"$UPPER(outship.consignee) OR "AMAZON"$UPPER(outship.consignee))
				lApptFlag = .T.
			ELSE
				lApptFlag = .F.
			ENDIF
			IF lTestinput
				ddel_date = DATE()
				cApptNum = "99999"
				dapptdate = DATE()
			ELSE
				ddel_date = outship.del_date
        			cdel_date = DTOS(ddel_date)
				IF EMPTY(ddel_date)
					cMissDel = IIF(EMPTY(cMissDel),"The following PTs had no Delivery Dates:"+CHR(13)+TRIM(cShip_ref),cMissDel+CHR(13)+TRIM(cShip_ref))
				ENDIF
				cApptNum = ALLTRIM(outship.appt_num)

				IF EMPTY(cApptNum) && Penney/KMart Appt Number check
					IF (!(lApptFlag) OR (DATE()={^2006-05-18} AND cBOL = "04907314677812395"))
						cApptNum = ""
					ELSE
						DO ediupdate WITH "EMPTY APPT #",.T.
						THROW
					ENDIF
				ENDIF
				dapptdate = outship.appt

				IF EMPTY(dapptdate)
					dapptdate = outship.del_date
				ENDIF
			ENDIF

			IF ALLTRIM(outship.SForCSZ) = ","
				BLANK FIELDS outship.SForCSZ NEXT 1 IN outship
			ENDIF

			alength = ALINES(apt,outship.shipins,.T.,CHR(13))

			cTRNum = ""
			cPRONum = ""

			IF (("WALMART"$outship.consignee) OR ("WAL-MART"$outship.consignee) ;
					OR ("WAL MART"$outship.consignee) OR ("AMAZON"$outship.consignee) OR m.scac = "ABFS")
				cKeyRec = ALLTRIM(outship.keyrec)
				cKeyRec = TRIM(STRTRAN(cKeyRec,"#",""))
				cPRONum = TRIM(STRTRAN(cKeyRec,"PR",""))

				IF EMPTY(cPRONum)
					IF lParcelType
						cPRONum = ALLTRIM(outship.bol_no)
					ENDIF
				ENDIF
			ENDIF

			nOutshipid = m.outshipid
			IF lParcelType
				cPTString = IIF(EMPTY(cPTString),m.consignee+" "+cBOL+" "+cShip_ref,cPTString+CHR(13)+m.consignee+" "+cBOL+" "+cShip_ref)
			ELSE
				cPTString = IIF(EMPTY(cPTString),m.consignee+" "+cShip_ref,cPTString+CHR(13)+m.consignee+" "+cShip_ref)
			ENDIF
			m.CSZ = TRIM(m.CSZ)
			DO CASE
				CASE m.CSZ = ","
					DO ediupdate WITH "MISSING CSZ "+cShip_ref,.T.
					THROW
				CASE !(", "$m.CSZ)
					DO ediupdate WITH "UNSPACED COMMA IN CSZ",.T.
					THROW
			ENDCASE

			IF EMPTY(M.CSZ)
				WAIT CLEAR
				WAIT WINDOW "No SHIP-TO City/State/ZIP info...exiting" TIMEOUT 2
				DO ediupdate WITH "NO CSZ ADDRESS INFO",.T.
				THROW
			ENDIF
			m.CSZ = ALLTRIM(STRTRAN(m.CSZ,"  "," "))
			nSpaces = OCCURS(" ",ALLTRIM(m.CSZ))
			IF nSpaces = 0
				WAIT WINDOW "SHIP-TO CSZ Segment Error" TIMEOUT 3
				DO ediupdate WITH "SHIP-TO CSZ ERR: "+cShip_ref,.T.
				THROW
			ELSE
				nCommaPos = AT(",",m.CSZ)
				nLastSpace = AT(" ",m.CSZ,nSpaces)
				IF ISALPHA(SUBSTR(TRIM(m.CSZ),AT(" ",m.CSZ,nSpaces-1)+1,2))
					cCity = LEFT(TRIM(m.CSZ),AT(",",m.CSZ)-1)
					cState = SUBSTR(m.CSZ,nCommaPos+2,2)
					cZip = TRIM(SUBSTR(m.CSZ,nLastSpace+1))
				ELSE
					WAIT CLEAR
					WAIT WINDOW "NOT ALPHA: "+SUBSTR(TRIM(m.CSZ),AT(" ",m.CSZ,nSpaces-1)+1,2) TIMEOUT 3
					cCity = LEFT(TRIM(m.CSZ),AT(",",m.CSZ)-1)
					cState = SUBSTR(TRIM(m.CSZ),AT(" ",m.CSZ,nSpaces-2)+1,2)
					cZip = TRIM(SUBSTR(m.CSZ,nLastSpace+1))
				ENDIF
			ENDIF

			STORE "" TO cSForCity,cSForState,cSForZip
			cStoreName = segmentget(@apt,"STORENAME",alength)
			IF !lFederated
				IF !EMPTY(M.SForCSZ)
					m.SForCSZ = ALLTRIM(STRTRAN(m.SForCSZ,"  "," "))
					nSpaces = OCCURS(" ",ALLTRIM(m.SForCSZ))
					IF nSpaces = 0
						m.SForCSZ = STRTRAN(m.SForCSZ,",","")
						cSForCity = ALLTRIM(m.SForCSZ)
						cSForState = ""
						cSForZip = ""
					ELSE
						nCommaPos = AT(",",m.SForCSZ)
						nLastSpace = AT(" ",m.SForCSZ,nSpaces)
						nMinusSpaces = IIF(nSpaces=1,0,1)
						IF ISALPHA(SUBSTR(TRIM(m.SForCSZ),AT(" ",TRIM(m.SForCSZ),nSpaces-nMinusSpaces)+1,2))
							cSForCity = LEFT(TRIM(m.SForCSZ),AT(",",m.SForCSZ)-1)
							cSForState = SUBSTR(m.SForCSZ,nCommaPos+2,2)
							cSForZip = ALLTRIM(SUBSTR(m.SForCSZ,nCommaPos+4))
							IF ISALPHA(cSForZip)
								cSForZip = ""
							ENDIF
						ELSE
							WAIT CLEAR
							WAIT WINDOW "NOT ALPHA: "+SUBSTR(TRIM(m.SForCSZ),AT(" ",m.SForCSZ,nSpaces-1)+1,2) TIMEOUT 3
							cSForCity = LEFT(TRIM(m.SForCSZ),AT(",",m.SForCSZ)-1)
							cSForState = SUBSTR(TRIM(m.SForCSZ),AT(" ",m.SForCSZ,nSpaces-2)+1,2)
							cSForZip = ALLTRIM(m.SForCSZ)
							IF ISALPHA(cSForZip)
								cSForZip = ""
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ELSE
				STORE "" TO cCity,cState,cZip
			ENDIF

			WAIT CLEAR
			WAIT WINDOW "Now creating Line Item information" NOWAIT NOCLEAR
****added 10/12/17 TMARG			

	DO num_incr_st

	CREATE CURSOR temp945 (accountid i,isa_num c(10),st_num c(10),wo_num i,bol_no c(20),ship_ref c(20),filename c(50),filedate T)
			INSERT INTO temp945 (accountid,isa_num,st_num,wo_num,bol_no,ship_ref,filename,filedate) ;
				VALUES (m.accountid,c_CntrlNum,c_GrpCntrlNum,m.wo_num,m.bol_no,m.ship_ref,cFilenameShort,dt2)
			
			SELECT outship

			STORE "ST"+cfd+"945"+cfd+PADL(c_GrpCntrlNum,9,"0")+csegd TO cString
			DO cstringbreak
			nSTCount = nSTCount + 1
			nSegCtr = 1

			cSalesOrder = segmentget(@apt,"SALESORDER",alength)
**changed to accommodate changes made to the non MBOL version  TMARG  10/13/17
*			STORE "W06"+cfd+"J"+cfd+cShip_ref+cfd+cdate+cfd+ALLTRIM(cWO_Num)+cfd+cBOL+cfd+cPO_Num+csegd TO cString
		**** cdel_date	
		      STORE "W06"+cfd+"J"+cfd+cship_ref+cfd+cdel_date+cfd+cbol+cfd+ALLTRIM(cwo_num)+cfd+cpo_num+cfd+cship_ref+cfd+"111111"+csegd TO cstring
			DO cstringbreak
			nSegCtr = nSegCtr + 1
			nCtnNumber = 1  && Seed carton sequence count

			cStoreNum = segmentget(@apt,"STORENUM",alength)
			IF EMPTY(cStoreNum)
				cStoreNum = ALLTRIM(m.dcnum)
			ENDIF
			STORE "N1"+cfd+"ST"+cfd+ALLTRIM(m.consignee)+cfd+"93"+cfd+cStoreNum+csegd TO cString
			DO cstringbreak
			nSegCtr = nSegCtr + 1
			IF EMPTY(M.ADDRESS2)
				STORE "N3"+cfd+ALLTRIM(M.ADDRESS)+csegd TO cString
			ELSE
				STORE "N3"+cfd+ALLTRIM(M.ADDRESS)+cfd+ALLTRIM(M.ADDRESS2)+csegd TO cString
			ENDIF
			DO cstringbreak
			nSegCtr = nSegCtr + 1

			cCountry = segmentget(@apt,"COUNTRY",alength)
			cCountry = ALLT(cCountry)
			IF EMPTY(cCountry)
				cCountry = "US"
			ENDIF
			STORE "N4"+cfd+cCity+cfd+cState+cfd+cZip+cfd+cCountry+csegd TO cString
			DO cstringbreak
			nSegCtr = nSegCtr + 1

			IF !EMPTY(ALLTRIM(m.shipfor))
				STORE "N1"+cfd+"Z7"+cfd+ALLTRIM(m.shipfor)+cfd+"92"+cfd+ALLTRIM(m.sforstore)+csegd TO cString
				DO cstringbreak
				nSegCtr = nSegCtr + 1
				IF EMPTY(M.ADDRESS2)
					STORE "N3"+cfd+ALLTRIM(M.SFORADDR1)+csegd TO cString
				ELSE
					STORE "N3"+cfd+ALLTRIM(M.SFORADDR1)+cfd+ALLTRIM(M.SFORADDR2)+csegd TO cString
				ENDIF
				DO cstringbreak
				nSegCtr = nSegCtr + 1
				IF !EMPTY(cSForState)
					STORE "N4"+cfd+cSForCity+cfd+cSForState+cfd+cSForZip+cfd+"US"+csegd TO cString
				ELSE
					STORE "N4"+cfd+cSForCity+csegd TO cString
				ENDIF
				DO cstringbreak
				nSegCtr = nSegCtr + 1
			ENDIF

			STORE "N9"+cfd+"MB"+cfd+ALLTRIM(cMBOL)+csegd TO cString
			DO cstringbreak
			nSegCtr = nSegCtr + 1

			STORE "N9"+cfd+"BM"+cfd+ALLTRIM(cBOL)+csegd TO cString
			DO cstringbreak
			nSegCtr = nSegCtr + 1

			IF lParcelType
				DO CASE
					CASE outship.terms = "BS"
						cBilling = "PP"
					CASE outship.terms = "BTP"
						cBilling = "TP"
					CASE outship.terms = "BR"
						cBilling = "CC"
					OTHERWISE
						cBilling = "PP"
				ENDCASE
				STORE "W27"+cfd+"M"+cfd+ALLTRIM(outship.scac)+cfd+ALLTRIM(carrcode)+cfd+ALLTRIM(cBilling)+csegd TO cString
				DO cstringbreak
				nSegCtr = nSegCtr + 1
			ENDIF


*************************************************************************
*2  DETAIL LOOP - MATCHING SHIP_REF FIELD (FROM OUTSHIP)
*************************************************************************
*    ASSERT .F. MESSAGE "In Detail Loop"
			STORE "LB" TO cWeightUnit
			WAIT CLEAR
			WAIT WINDOW "Now creating Detail information "+cShip_ref NOWAIT NOCLEAR
			SELECT vragbonepp
			LOCATE
			SELECT outdet
			SET ORDER TO TAG outdetid

			IF cBOL = cRefBOL
				SET DELETED OFF
			ELSE
				SET DELETED ON
			ENDIF
			SELECT vragbonepp
			SET RELATION TO outdetid INTO outdet

			LOCATE
			LOCATE FOR ship_ref = TRIM(cShip_ref) AND outshipid = nOutshipid
			IF !FOUND()
				IF !lTesting
					WAIT WINDOW "PT "+cShip_ref+" NOT FOUND in vragbonepp...ABORTING" TIMEOUT 2
					IF !lTesting
						lSQLMail = .T.
					ENDIF
					DO ediupdate WITH "MISS PT-SQL: "+cShip_ref,.T.
					THROW
				ELSE
					LOOP
				ENDIF
			ENDIF

			scanstr = "vragbonepp.ship_ref = cShip_ref and vragbonepp.outshipid = nOutshipid and ucc # 'CUTS'"
			SELECT vragbonepp
			LOCATE
			LOCATE FOR &scanstr
			cCartonNum= "XXX"

			DO WHILE &scanstr
				lDoManSegment = .T.
				alength = ALINES(apt,outdet.printstuff,.T.,CHR(13))
				lSkipBack = .T.

				IF TRIM(vragbonepp.ucc) <> cCartonNum
					STORE TRIM(vragbonepp.ucc) TO cCartonNum
				ENDIF

				STORE "LX"+cfd+ALLTRIM(STR(nCtnNumber))+csegd TO cString   && Carton Seq. # (up to Carton total)
				DO cstringbreak
				nSegCtr = nSegCtr + 1

				DO WHILE vragbonepp.ucc = cCartonNum
					IF vragbonepp.totqty < 1
						SKIP 1 IN vragbonepp
						LOOP
					ENDIF

					cUCCNumber = vragbonepp.ucc
					IF EMPTY(cUCCNumber) OR ISNULL(cUCCNumber)
						WAIT CLEAR
						WAIT WINDOW "Empty UCC Number in vragbonepp "+cShip_ref TIMEOUT 2
						lSQLMail = .T.
						DO ediupdate WITH "EMPTY UCC# in "+cShip_ref,.T.
						THROW
					ENDIF
					cUCCNumber = TRIM(STRTRAN(TRIM(cUCCNumber)," ",""))
					cCtnwt = '0'
					cCtnwt = ALLTRIM(STR(vragbonepp.ctnwt,5,2))

					IF lDoManSegment
						nCtnNumber = nCtnNumber + 1
						lDoManSegment = .F.

						STORE "MAN"+cfd+"GM"+cfd+TRIM(cUCCNumber)+REPLICATE(cfd,5)+cCtnwt+csegd TO cString
						DO cstringbreak
						nSegCtr = nSegCtr + 1
						nTotCtnCount = nTotCtnCount + 1
						ntotctnwt = ntotctnwt + outdet.ctnwt
					ENDIF

					WAIT WINDOW "OD OUTDETID: "+TRANSFORM(outdet.outdetid)+CHR(13)+"V- OUTDETID: "+TRANSFORM(vragbonepp.outdetid) NOWAIT
					cColor = TRIM(outdet.COLOR)
					cSize = TRIM(outdet.ID)
					cUPC = TRIM(outdet.upc)
					IF (ISNULL(cUPC) OR EMPTY(cUPC))
						cUPC = TRIM(vragbonepp.upc)
					ENDIF
					cLineNum = ALLTRIM(outdet.linenum)
					cStyle = TRIM(outdet.STYLE)
					cItemNum = TRIM(outdet.custsku)
					nODID = outdet.outdetid

					IF EMPTY(cLineNum)
						ASSERT .F. MESSAGE "In LINENUM error"
						SET STEP ON
						DO ediupdate WITH "EMPTY LINE #s-ODID "+TRANSFORM(nODID),.T.
						THROW
					ENDIF

					nShipDetQty = vragbonepp.totqty
					IF ISNULL(nShipDetQty)
						nShipDetQty = outdet.totqty
					ENDIF
					nUnitsum = nUnitsum + nShipDetQty

					nOrigDetQty = vragbonepp.qty
					IF ISNULL(nOrigDetQty)
						nOrigDetQty = nShipDetQty
					ENDIF

*!* Changed the following to utilize original 940 unit codes from Printstuff field
					cUnitCode = TRIM(segmentget(@apt,"UNITCODE",alength))
					IF EMPTY(cUnitCode)
						cUnitCode = "EA"
					ENDIF

					nShipStat = "CL"

					STORE "W12"+cfd+nShipStat+cfd+ALLTRIM(STR(nOrigDetQty))+cfd+ALLTRIM(STR(nShipDetQty))+cfd+cfd+cUnitCode+cfd+TRIM(cUPC)+csegd TO cString
					DO cstringbreak
					nSegCtr = nSegCtr + 1

					STORE "N9"+cfd+"LI"+cfd+cLineNum+csegd TO cString
					DO cstringbreak
					nSegCtr = nSegCtr + 1

					STORE "N9"+cfd+"IX"+cfd+cStyle+csegd TO cString
					DO cstringbreak
					nSegCtr = nSegCtr + 1

					STORE "N9"+cfd+"CO"+cfd+ALLTRIM(outdet.COLOR)+csegd TO cString
					DO cstringbreak
					nSegCtr = nSegCtr + 1

					STORE "N9"+cfd+"IZ"+cfd+ALLTRIM(outdet.ID)+csegd TO cString
					DO cstringbreak
					nSegCtr = nSegCtr + 1

					SKIP 1 IN vragbonepp
				ENDDO
				lSkipBack = .T.

*      ASSERT .F. MESSAGE "At end of detail looping-debug"
			ENDDO

*************************************************************************
*2^  END DETAIL MATCHING SHIP_REF LOOP
*************************************************************************
			WAIT CLEAR
			WAIT WINDOW "Now creating Section Closure information" NOWAIT NOCLEAR
			STORE "W03"+cfd+ALLTRIM(STR(nUnitsum))+cfd+ALLTRIM(STR(nTotCtnWt))+cfd+;
				cWeightUnit+cfd+ALLTRIM(STR(outship.cuft,10,2))+cfd+"CF"+cfd+ALLTRIM(STR(nTotCtnCount))+cfd+"CT"+csegd TO cString   && Units sum, Weight sum, carton count

			nTotCtnWt = 0
			nTotCtnCount = 0
			nUnitsum = 0
			DO cstringbreak

			STORE  "SE"+cfd+ALLTRIM(STR(nSegCtr+2))+cfd+PADL(c_GrpCntrlNum,9,"0")+csegd TO cString
			DO cstringbreak

			SELECT bldet
			WAIT CLEAR

		ENDSCAN
		ASSERT .F. MESSAGE "After sub-bill det scan"


*************************************************************************
*1^  END OUTSHIP MAIN LOOP
*************************************************************************

		DO close945
		=FCLOSE(nFilenum)
*  ASSERT .F. MESSAGE "At end of main loop...debug here"
		WAIT CLEAR

		nSubRec = nSubRec+1
*!* Create eMail confirmation message
		tsubject = cMailName+" 945 EDI File from TGF as of "+dtmail+" ("+cCustLoc+")"
		tattach = " "
		tmessage = "945 EDI Info from TGF, file "+cFilenameShort+CHR(13)
		tmessage = tmessage + "Division "+cDivision+", MBOL# "+cMBOL+", BOL# "+TRIM(cBOL)+CHR(13)
		tmessage = tmessage + "File "+ALLTRIM(STR(nSubRec))+" of "+ALLTRIM(STR(nSubBillCnt))+CHR(13)
		tmessage = tmessage + "For Work Orders: "+cWO_NumStr+","+CHR(13)
		tmessage = tmessage + "containing these "+TRANSFORM(nPTCount)+" picktickets:"+CHR(13)
		tmessage = tmessage + cPTString + CHR(13)
		tmessage = tmessage +"has been created and will be transmitted ASAP."+CHR(13)+CHR(13)
		IF !EMPTY(cMissDel)
			tmessage = tmessage+CHR(13)+CHR(13)+cMissDel+CHR(13)+CHR(13)
		ENDIF
		tmessage = tmessage + "If you have any questions, please eMail or call 732-750-9000 x217."

		IF lEmail
			DO FORM m:\dev\frm\dartmail2 WITH tsendto,tfrom,tsubject,tcc,tattach,tmessage,"A"
		ENDIF
		nPTCount = 0
		cPTString = ""

		WAIT CLEAR
		WAIT WINDOW cMailName+" 945 EDI File sub-BOL output complete" TIMEOUT 1

*!* Transfers files to correct output folders
*		COPY FILE [&cFilenameHold] TO [&cFilenameArch]
			oFSO=CREATEOBJECT("Scripting.FileSystemObject")
			oFSO.CopyFile("&cFilenameHold","&cFilenameArch",.T.)

		IF lRBFilesOut AND !lTesting
				oFSO=CREATEOBJECT("Scripting.FileSystemObject")
				oFSO.CopyFile("&cFilenameHold","&cFilenameOUT",.T.)
*			COPY FILE [&cFilenameHold] TO [&cFilenameOut]
			DELETE FILE [&cFilenameHold]
			SELECT temp945
			COPY TO "f:\3pl\data\temp945rb.dbf"
			USE IN temp945
			SELECT 0
			USE "f:\3pl\data\temp945rb.dbf" ALIAS temp945rb
			SELECT 0
			USE "f:\3pl\data\pts_sent945.dbf" ALIAS pts_sent945
			APPEND FROM "f:\3pl\data\temp945rb.dbf"
			USE IN pts_sent945
			USE IN temp945rb
			DELETE FILE "f:\3pl\data\temp945rb.dbf"
		ENDIF
	
		ASSERT .F. MESSAGE "At end of sub-bill main scan"

		IF !lTesting
	*!* asn_out_data()()
		ENDIF

		cWO_NumList = ""
	ENDSCAN
	RELEASE ALL LIKE c_CntrlNum,c_GrpCntrlNum
	IF !lTesting
		SELECT edi_trigger
		DO ediupdate WITH "945 CREATED",.F.
		SELECT edi_trigger
		LOCATE

		IF !USED("ftpedilog")
			SELECT 0
			USE F:\edirouting\ftpedilog ALIAS ftpedilog
			INSERT INTO ftpedilog (TRANSFER,ftpdate,filename,acct_name,TYPE) VALUES ("945-"+ccustname+"-"+cCustLoc,dt2,cfilenamehold,UPPER(ccustname),"945")
			USE IN ftpedilog
		ENDIF
	ENDIF

	lDoCatch = .F.

CATCH TO oErr
	IF lDoCatch
		ASSERT .F. MESSAGE "In error CATCH..."
		SET STEP ON
		lEmail = .F.
		DO ediupdate WITH ALLTRIM(oErr.message),.T.
		tsubject = ccustname+" Error ("+TRANSFORM(oErr.ERRORNO)+") at "+TTOC(DATETIME())
		tattach  = ""
		tsendto  = "pgaidis@fmiint.com"&&tsendtoerr
		tcc = tccerr

		tmessage = ccustname+" Error processing "+CHR(13)

		tmessage =tmessage+ "Try/CATCH Exeception Message:"+CHR(13)+;
			[  Error: ] + STR(oErr.ERRORNO) +CHR(13)+;
			[  LineNo: ] + STR(oErr.LINENO) +CHR(13)+;
			[  Message: ] + oErr.MESSAGE +CHR(13)+;
			[  Procedure: ] + oErr.PROCEDURE +CHR(13)+;
			[  Details: ] + oErr.DETAILS +CHR(13)+;
			[  StackLevel: ] + STR(oErr.STACKLEVEL) +CHR(13)+;
			[  LineContents: ] + oErr.LINECONTENTS+CHR(13)+;
			[  UserValue: ] + oErr.USERVALUE
		tmessage = tmessage + CHR(13)+"PROGRAM: "+cProgname
		IF !EMPTY(cStatus)
			tmessage = tmessage + CHR(13)+"Error: "+cStatus
		ENDIF
		tsubject = "945 EDI Poller Error at "+TTOC(DATETIME())
		tattach  = ""
		tfrom    ="TGF EDI 945 Poller Operations <transload-ops@tollgroup.com>"
		DO FORM m:\dev\frm\dartmail2 WITH tsendto,tfrom,tsubject,tcc,tattach,tmessage,"A"
	ENDIF
ENDTRY

*** END OF CODE BODY

****************************
PROCEDURE close945
****************************
** Footer Creation

	STORE  "GE"+cfd+ALLTRIM(STR(nSTCount))+cfd+c_CntrlNum+csegd TO cString
	DO cstringbreak

	STORE  "IEA"+cfd+"1"+cfd+PADL(c_CntrlNum,9,"0")+csegd TO cString
	DO cstringbreak
	RETURN

****************************
PROCEDURE num_incr_isa
****************************

	IF !USED("serfile")
		USE ("f:\3pl\data\serial\"+ccustname+"945_serial") IN 0 ALIAS serfile
	ENDIF
	SELECT serfile
	IF lTesting AND lISAFlag
		ASSERT .F. MESSAGE "At group seqnum...debug"
		nOrigSeq = serfile.seqnum
		lISAFlag = .F.
	ENDIF
	nISA_Num = serfile.seqnum
	c_CntrlNum = ALLTRIM(STR(serfile.seqnum))
	REPLACE serfile.seqnum WITH serfile.seqnum + 1 IN serfile
	SELECT outship
	RETURN

****************************
PROCEDURE num_incr_st
****************************

	IF !USED("serfile")
		USE ("f:\3pl\data\serial\"+ccustname+""945_serial") IN 0 ALIAS serfile
	ENDIF
	SELECT serfile
	IF lTesting AND lSTFlag
		nOrigGrpSeq = serfile.grpseqnum
		lSTFlag = .F.
	ENDIF
	c_GrpCntrlNum = ALLTRIM(STR(serfile.grpseqnum))
	REPLACE serfile.grpseqnum WITH serfile.grpseqnum + 1 IN serfile
	SELECT outship
	RETURN


****************************
PROCEDURE segmentget
****************************

	PARAMETER thisarray,lcKey,nLength

	FOR i = 1 TO nLength
		IF i > nLength
			EXIT
		ENDIF
		lnEnd= AT("*",thisarray[i])
		IF lnEnd > 0
			lcThisKey =TRIM(SUBSTR(thisarray[i],1,lnEnd-1))
			IF OCCURS(lcKey,lcThisKey)>0
				RETURN SUBSTR(thisarray[i],lnEnd+1)
				i = 1
			ENDIF
		ENDIF
	ENDFOR

	RETURN ""

****************************
PROCEDURE ediupdate
****************************
	PARAMETER cStatus,lIsError
*  lDoCatch = IIF(lIsError,.T.,.F.)
	lDoCatch = .F.
*	SET STEP ON

	IF !lTesting
		SELECT edi_trigger
		IF !lIsError
			REPLACE processed WITH .T.,proc945 WITH .T.,file945 WITH cfilenamehold,isa_num WITH cISA_Num,;
				fin_status WITH "945 CREATED",errorflag WITH .F.,when_proc WITH DATETIME() ;
				FOR edi_trigger.bol = cMBOL AND INLIST(accountid,&gragboneaccounts)
*!*				xsqlexec("update edi_trigger set processed=1, proc945=1, file945='"+cFilenameHold+"',isa_num='"+cISA_Num+"' fin_status='945 CREATED', errorflag=0, when_proc={"+TTOC(DATETIME())+"} where bol='"+cMBOL+"' and accountid in ("+gragboneaccounts+")",,,"stuff")
		ELSE
			REPLACE processed WITH .T.,proc945 WITH .F.,file945 WITH "",;
				fin_status WITH cStatus,errorflag WITH .T. ;
				FOR edi_trigger.bol = cMBOL AND INLIST(accountid,&gragboneaccounts)
*!*				xsqlexec("update edi_trigger set processed=1, proc945=0, file945=' ', fin_status='"+cStatus+"', errorflag=1 where bol='"+cMBOL+"' and accountid in ("+gragboneaccounts+")",,,"stuff")
			IF lCloseOutput
				=FCLOSE(nFilenum)
				ERASE &cfilenamehold
			ENDIF
		ENDIF
	ENDIF


	IF lIsError AND lEmail && AND cStatus # "SQL ERROR"
		tsubject = "945 Error in RAG & BONE BOL "+TRIM(cBOL)+"(At PT "+cShip_ref+")"
		tattach = " "
		tsendto = ALLTRIM(tsendtoerr)
		tcc = ALLTRIM(tccerr)
		tmessage = "945 Processing for BOL# "+cBOL+", WO# "+cWO_Num+" produced this error: (Office: "+cOffice+"): "+cStatus+CHR(13)+"Check EDI_TRIGGER and re-run"
		IF "TOTQTY ERR"$cStatus
			tmessage = tmessage + CHR(13) + "At OUTSHIPID: "+ALLTRIM(STR(m.outshipid))
		ENDIF
		DO FORM m:\dev\frm\dartmail2 WITH tsendto,tfrom,tsubject,tcc,tattach,tmessage,"A"
	ENDIF

	IF USED('outship')
		USE IN outship
	ENDIF
	IF USED('outdet')
		USE IN outdet
	ENDIF
	IF USED('shipment')
		USE IN shipment
	ENDIF
	IF USED('package')
		USE IN package
	ENDIF

	IF USED('serfile')
		SELECT serfile
		IF lTesting
			IF nOrigSeq > 0
				REPLACE serfile.seqnum WITH nOrigSeq
			ENDIF
			IF nOrigGrpSeq > 0
				REPLACE serfile.grpseqnum WITH nOrigGrpSeq
			ENDIF
		ENDIF
		USE IN serfile
	ENDIF
	IF USED('scacs')
		USE IN scacs
	ENDIF
	IF USED('mm')
		USE IN mm
	ENDIF
	IF USED('tempx')
		USE IN tempx
	ENDIF
	IF USED('vragbonepp')
		USE IN vragbonepp
	ENDIF

	IF !lTesting
		SELECT edi_trigger
		LOCATE
	ENDIF
	IF lIsError
	ENDIF
	RETURN

****************************
PROCEDURE cstringbreak
****************************
	cLen = LEN(ALLTRIM(cString))

	TRY
		FPUTS(nFilenum,cString)
	CATCH TO oErr1
		SET STEP ON
		tmessage = ccustname+" Error processing "+CHR(13)

		tmessage =tmessage+ "Try/CATCH Exeception Message:"+CHR(13)+;
			[  Error: ] + STR(oErr1.ERRORNO) +CHR(13)+;
			[  LineNo: ] + STR(oErr1.LINENO) +CHR(13)+;
			[  Message: ] + oErr1.MESSAGE +CHR(13)+;
			[  Procedure: ] + oErr1.PROCEDURE +CHR(13)+;
			[  Details: ] + oErr1.DETAILS +CHR(13)+;
			[  StackLevel: ] + STR(oErr1.STACKLEVEL) +CHR(13)+;
			[  LineContents: ] + oErr1.LINECONTENTS+CHR(13)+;
			[  UserValue: ] + oErr1.USERVALUE
	ENDTRY

*	FWRITE(nFilenum,cString)

	endproc