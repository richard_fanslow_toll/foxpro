* GetXLAlphaColumn, MB 4/28/15

* returns the Excel column name like A or AA, AB, etc. from passed column #
* works for up to AZ (column 52)

* written to be called from ctraging.prg but could be used elsewhere.

LPARAMETERS tnColumn

LOCAL lnColumn, lnString, lnExtra
lnColumn = tnColumn
lnString = ""

DO CASE
	CASE lnColumn < 27
		lnString = CHR(64 + lnColumn)
	OTHERWISE
		lnExtra = lnColumn - 26
		lnString = "A" + CHR(64 + lnExtra)
ENDCASE


*? lnString
RETURN lnString