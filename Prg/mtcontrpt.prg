* empty container report

utilsetup("MTCONTRPT")

if holiday(date())
	return
endif

if usesqlmt()
	xsqlexec("select * from empties",,,"wo")
else
	use f:\wo\wodata\empties
endif

select iif("R"$size,"YES","   ") as reefer, wo_num, acctname, dispcont(container) as container, ;
	size, chassis, yardloc, ssco, retloc, pickedup, mtinyard, outdays, remarks ;
	from empties where office="N" into cursor xrpt readwrite

index on iif(reefer="YES","0","1")+str(9999-outdays,4) tag vv

copy to h:\fox\emptycontainers xls

tsendto="Joe.Damato@tollgroup.com,Mike.Drew@tollgroup.com,Tony.Branco@tollgroup.com,mdivirgilio@fmiint.com,"
tsendto=tsendto+"thomas.keaveney@tollgroup.com,Stephen.Zembryski@tollgroup.com,Nadine.Donovan@Tollgroup.com,Irena.Pagurek@tollgroup.com,Olga.Cruz@tollgroup.com,Michael.divirgilio@tollgroup.com"

*tsendto="Dyoung@fmiint.com"

tcc=""
tattach="h:\fox\emptycontainers.xls"
tFrom="TGF EDI System Operations <transload-ops@fmiint.com>"
tmessage=""
tSubject="Empty Container List"

Do Form dartmail2 With tsendto,tFrom,tSubject,tcc,tattach,tmessage,"A"

*

schedupdate()

_screen.Caption=gscreencaption
on error
