lparameters xcsunbilledwo, xbillableonly

* trucking moves

if xbillableonly
	xxbillableonlyfilter='(!emptynul(pickedup) or !emptynul(delivered) or !emptynul(returned) or inlist(office,"V","M") or accountid=701) and !(worevenue=0 and type="D" and empty(container))'
else
	xxbillableonlyfilter=".t."
endif

select ;
	wo_num, ;
	wo_date, ;
	iif(office="N","NJ",iif(office="C","CA",iif(office="M","FL",iif(office="V","VA","??")))) as office, ;
	"T" as source, ;
	type+iif(type="O" and loose,"L"," ") as type, ;
	worevenue2 as revenue, ;
	accountid, ;
	acctname, ;
	iif(!empty(container),dispcont(container),dispawb(awb)) as reference, ;
	space(50) as comments ;
	from wolog ;
	where accountid#9999 ;
	and invnum="      " ;
	and !unbilled ;
	and &xfilter1 ;
	and &xfilter2 ;
	and &xfilter3 ;
	and &xfilter4 ;
	and &xxbillableonlyfilter ;
union select ;
	wo_num, ;
	wo_date, ;
	iif(office="N","NJ",iif(office="C","CA",iif(office="M","FL",iif(office="V","VA","??")))) as office, ;
	"Q" as source, ;
	type+iif(type="O" and loose,"L"," ") as type, ;
	worevenue-worevenue2 as revenue, ;
	accountid, ;
	acctname, ;
	iif(!empty(container),dispcont(container),dispawb(awb)) as reference, ;
	space(50) as comments ;
	from wolog ;
	where accountid#9999 ;
	and invnum="      " ;
	and !unbilled ;
	and &xfilter1 ;
	and &xfilter2 ;
	and &xfilter3 ;
	and &xfilter4 ;
	and &xxbillableonlyfilter ;
	and worevenue>worevenue2 ;
	into cursor xtemp readwrite

select xtemp
scan
	select daily
	locate for wo_num=xtemp.wo_num and inlist(action,"EP","PU")
	if found()
		if pickedup#{}
			replace comments with "PICKED UP "+dtoc(daily.pickedup) in xtemp
		else
			replace comments with "PICKUP IN PROGRESS" in xtemp
		endif
	else
		select tdaily
		locate for wo_num=xtemp.wo_num and inlist(action,"EP","PU")
		if found()
			if pickedup#{}
				replace comments with "PICKED UP "+dtoc(tdaily.pickedup) in xtemp
			else
				replace comments with "PICKUP IN PROGRESS" in xtemp
			endif
		else
			if xsqlexec("select availlogid from availlog where wo_num="+transform(xtemp.wo_num)+" and avail#{}",,,"wo") # 0
				replace comments with "AVAILABLE" in xtemp
			endif
		endif
	endif
endscan

select xtemp
scan 
	scatter memvar
	insert into xrpt from memvar
endscan

* pier move differentials

select ;
	wo_num, ;
	wo_date, ;
	iif(office="N","NJ",iif(office="C","CA",iif(office="M","FL",iif(office="V","VA","??")))) as office, ;
	"T" as source, ;
	secondinvtype as type, ;
	worevenue2 as revenue, ;
	accountid, ;
	acctname, ;
	iif(!empty(container),dispcont(container),dispawb(awb)) as reference, ;
	"DIFFERENTIAL" as comments ;
	from wolog ;
	where accountid#9999 ;
	and secondinvtype="DF" ;
	and invnum2="      " ;
	and !unbilled ;
	and &xfilter1 ;
	and &xfilter2 ;
	and &xfilter3 ;
	and &xfilter4 ;
	into cursor xtemp readwrite

select xtemp
scan 
	if xbillableonly
		=seek(wo_num,"wolog","wo_num")
		if !(!empty(wolog.pickedup) or !empty(wolog.delivered) or !empty(wolog.returned) or inlist(wolog.office,"V","M") or wolog.accountid=701)
			loop
		endif
	endif
	scatter memvar
	insert into xrpt from memvar
endscan

* charge slips

xsqlexec("select * from cs where empty(invnum) and totchg#0 and unbilled=0",,,"wo")

select ;
	wo_num, ;
	csdt as wo_date, ;
	iif(office="N","NJ",iif(office="C","CA",iif(office="M","FL",iif(office="V","VA","??")))) as office, ;
	"T" as source, ;
	"CS" as type, ;
	totchg as revenue, ;
	accountid, ;
	padr(acctname(accountid),30) as acctname, ;
	"" as reference, ;
	padr(cstype,50) as comments ;
	from cs ;
	where &xfilter1 and &xfilter2 and &xcsfilter3 and &xfilter4 ;
	into cursor xtemp readwrite

select xtemp
scan 
	if xcsunbilledwo
		=seek(wo_num,"wolog","wo_num")
		if empty(wolog.invnum)
			loop
		endif
	endif

	if xbillableonly
		=seek(wo_num,"wolog","wo_num")
		if !(!empty(wolog.pickedup) or !empty(wolog.delivered) or !empty(wolog.returned) or inlist(wolog.office,"V","M") or wolog.accountid=701)
			loop
		endif
	endif

	scatter memvar
	insert into xrpt from memvar
endscan

* outbound to consolidators

if inlist(xoffice," ","N")
	if empty(xaccountid)
		xtables="inbound"
	else
		xtables="inbound, bl"
	endif

	select ;
		inbound.wo_num, ;
		deliverdt as wo_date, ;
		iif(inbound.office="N","NJ","CA") as office, ;
		"Z" as source, ;
		"OB" as type, ;
		inbound.accountid, ;
		iif(empty(xaccountid),inbound.acctname,padr(acctname(xaccountid),30)) as acctname, ;
		iif(len(trim(pu_trlr))<8,padr(pu_trlr,12),dispcont(pu_trlr)) as reference, ;
		revenue, ;
		left(strtran(strtran(comments,chr(13)," "),chr(10)," "),100) as comments ;
		from &xtables ;
		where empty(invnum) ;
		and !unbilled ;
		and &xob2consfilter1 ;
		and &xob2consfilter2 ;
		and &xob2consfilter3 ;
		and !emptynul(deliverdt) ;
		group by 1 ;
		into cursor xtemp

	select xtemp
	scan 
		scatter memvar
		insert into xrpt from memvar
	endscan
endif

* filter

select xrpt
do case
case xwhtype="WORK ORDERS"
	delete for type="CS"
case xwhtype="AIR"
	delete for type#"A"
case xwhtype="OCEAN"
	delete for type#"O"
case xwhtype="DOMESTIC"
	delete for type#"D"
case xwhtype="CHARGE SLIP"
	delete for type#"CS"
case xwhtype="DIFFERENTIAL"
	delete for type#"DF"
case xwhtype="SEARS $40"
	delete for type#"OS"
endcase	