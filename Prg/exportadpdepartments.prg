* export department list from ADP into Foxpro (for use by revtime.scx)

runack("EXPDEPTS")

LOCAL loExportADPDepartmentList
*!*	IF NOT INLIST(UPPER(ALLTRIM(GETENV("COMPUTERNAME"))),"MBENNETT","HR1") THEN
*!*		=MESSAGEBOX("The Employee Export must be run from Lucille's PC!",0+16,"Export FoxPro Employees")
*!*		RETURN
*!*	ENDIF
loExportADPDepartmentList = CREATEOBJECT('ExportADPDepartmentList')
loExportADPDepartmentList.MAIN()
CLOSE DATABASES ALL
RETURN

#DEFINE LOGIT 1
#DEFINE WAITIT 2
#DEFINE NOWAITIT 4
#DEFINE SENDIT 8
#DEFINE CRLF CHR(13) + CHR(10)
#DEFINE RETURN_DATA_MANDATORY .F.
#DEFINE RETURN_DATA_NOT_MANDATORY .T.
#DEFINE CSVFORMAT 6
#DEFINE WRITE_ONLY_UNBUFFERED 11
#DEFINE COMMA ","

DEFINE CLASS ExportADPDepartmentList AS CUSTOM

	cProcessName = 'ExportADPDepartmentList'

	lTestMode = .F.  && .F. FOR NORMAL OPERATION, .T. TO SUPPRESS ALL BUT DEVELOPER EMAILS AND CHANGE INPUT TABLE, ETC.

	lAutoYield = .T.

	cStartTime = TTOC(DATETIME())

	* export behavior properties
	lExcludeOldADPDepartments = .T.  && set this to true once we have added the new Labor Type Codes as departments

	* export statistics properties
	nAddCount = 0
	nUpdateCount = 0
	nOldDivNamesNotFound = 0

	* Data Table properties
	cNewFoxProDepartmentTable = "F:\UTIL\ADPEDI\DATA\ADPDEPTS.DBF"
	cOldFoxProDepartmentTable = "F:\HR\HRDATA\DEPARTME.DBF"
	*cOldFoxProDepartmentTable = "F:\UTIL\ADPEDI\TESTDATA\DEPARTME.DBF"
	cDivnameCompanyLookupTable = "F:\UTIL\ADPEDI\TESTDATA\DIVNAMES.DBF"

	* connection properties
	nSQLHandle = 0

	* wait window properties
	nWaitWindowTimeout = 2
	lWaitWindowIsOn = .T.

	* logfile properties
	lLoggingIsOn = .T.
	cLogFile = 'F:\UTIL\ADPEDI\Logfiles\ExportADPDepartmentList_log.txt'

	* INTERNAL email properties
	lSendInternalEmailIsOn = .T.
	cFrom = 'FMI Corporate <fmicorporate@fmiint.com>'
	cSendTo = 'Mark Bennett <mbennett@fmiint.com>'
	*cSendTo = 'Lucille Waldrip <lwaldrip@fmiint.com>, Lori Guiliano <lguiliano@fmiint.com>, Mark Bennett <mbennett@fmiint.com>'
	cCC = ''
	cSubject = 'EXPORT ADP DEPARTMENT LIST TO FOXPRO, Results for: ' + TTOC(DATETIME())
	cAttach = ''
	cBodyText = ''

	cCOMPUTERNAME = ''
	cUSERNAME = ''

	FUNCTION INIT
		IF NOT DODEFAULT()
			RETURN .F.
		ENDIF
		WITH THIS
			*SET RESOURCE OFF
			CLOSE DATA
			SET CENTURY OFF
			SET DATE AMERICAN
			SET HOURS TO 24
			SET ANSI ON
			SET TALK OFF
			SET DELETED ON
			SET CONSOLE OFF
			SET EXCLUSIVE OFF
			SET SAFETY OFF
			SET EXACT OFF
			SET STATUS BAR OFF
			SET SYSMENU OFF
			SET ENGINEBEHAVIOR 70
			_VFP.AUTOYIELD = .lAutoYield
			.cCOMPUTERNAME = GETENV("COMPUTERNAME")
			.cUSERNAME = GETENV("USERNAME")
			IF .lTestMode THEN
				.cSendTo = 'Mark Bennett <mbennett@fmiint.com>'
				.cLogFile = 'F:\UTIL\ADPEDI\Logfiles\ExportADPDepartmentList_log_TESTMODE.txt'
				.cOldFoxProDepartmentTable = "F:\UTIL\ADPEDI\TESTDATA\DEPARTME.DBF"
			ENDIF
			.lLoggingIsOn = .lLoggingIsOn AND NOT EMPTY(.cLogFile)
			IF .lLoggingIsOn THEN
				SET ALTERNATE TO (.cLogFile) ADDITIVE
				SET ALTERNATE ON
			ENDIF
		ENDWITH
	ENDFUNC


	FUNCTION DESTROY
		WITH THIS
			IF .lLoggingIsOn  THEN
				SET ALTERNATE OFF
				SET ALTERNATE TO
			ENDIF
			SET STATUS BAR ON
		ENDWITH
		DODEFAULT()
	ENDFUNC


	FUNCTION MAIN
		WITH THIS
			LOCAL lnNumberOfErrors, lcSQL, loError, llNewRecord, lcInsert, lcErrorMsg
			LOCAL lcNewFoxProDepartmentTable, lcOldFoxProDepartmentTable, lcDivnameCompanyLookupTable
			LOCAL lcDeptWhere
			TRY
				lnNumberOfErrors = 0

				.TrackProgress('', LOGIT)
				.TrackProgress('', LOGIT)
				.TrackProgress('=========================================================', LOGIT+SENDIT)
				.TrackProgress('EXPORT ADP DEPARTMENT LIST TO FOXPRO process started....', LOGIT+SENDIT+NOWAITIT)
				.TrackProgress('COMPUTERNAME = ' + .cCOMPUTERNAME, LOGIT+SENDIT)
				.TrackProgress('USERNAME = ' + .cUSERNAME, LOGIT+SENDIT)
				.TrackProgress('=========================================================', LOGIT+SENDIT)
				IF .lTestMode THEN
					.TrackProgress('', LOGIT)
					.TrackProgress('=========================================================', LOGIT+SENDIT)
					.TrackProgress('                        TEST MODE                        ', LOGIT+SENDIT)
					.TrackProgress('=========================================================', LOGIT+SENDIT)
				ENDIF

				lcNewFoxProDepartmentTable = .cNewFoxProDepartmentTable
				lcOldFoxProDepartmentTable = .cOldFoxProDepartmentTable
				lcDivnameCompanyLookupTable = .cDivnameCompanyLookupTable

				* create Target table if necessary (should only be done once, initially)
				IF NOT FILE(lcNewFoxProDepartmentTable) THEN
					CREATE TABLE (lcNewFoxProDepartmentTable) ;
						( ADPCOMP C(3), DIVISION C(2), LABORTYPE C(4), DESCRIPT C(40), CSHIFTS C(3), CUSERS C(50), UPDATED T )
					SELECT ADPDEPTS
					INDEX ON ADPCOMP TAG ADPCOMP
					INDEX ON DIVISION TAG DIVISION
					INDEX ON LABORTYPE TAG LABORTYPE
					.TrackProgress('=========================================================', LOGIT+SENDIT)
					.TrackProgress('WARNING: Target FoxPro Department Table was not found!', LOGIT+SENDIT+NOWAITIT)
					.TrackProgress('Created table: ' + lcNewFoxProDepartmentTable, LOGIT+SENDIT+NOWAITIT)
					.TrackProgress('=========================================================', LOGIT+SENDIT)
					.TrackProgress('', LOGIT+SENDIT+NOWAITIT)
					* close table just created...
					IF USED('ADPDEPTS') THEN
						USE IN ADPDEPTS
					ENDIF
				ENDIF

				* get ss#s from ADP
				OPEN DATABASE F:\UTIL\ADPCONNECTION\CONNECT1

				.nSQLHandle = SQLCONNECT("PCPAYWIN","MBENNETT","VP737A")

				IF .nSQLHandle > 0 THEN

					lcSQL = ;
						"SELECT " + ;
						" {fn IFNULL(COMPANYCODE,'???')} AS ADPCOMP, " + ;
						" {fn IFNULL(DEPARTMENT,'??????')} AS HOMEDEPT, " + ;
						" {fn IFNULL({fn LEFT(DEPARTMENT,2)},'??')} AS DIVISION, " + ;
						" {fn IFNULL({fn SUBSTRING(DEPARTMENT,3,4)},'????')} AS LABORTYPE, " + ;
						" {fn IFNULL(DESCRIPTION,'                                        ')} AS DESCRIPT " + ;
						" FROM REPORTS.V_DEPARTMENT "

					IF USED('SQLCURSOR1') THEN
						USE IN SQLCURSOR1
					ENDIF

					IF .ExecSQL(lcSQL, 'SQLCURSOR1', RETURN_DATA_MANDATORY) THEN

						IF NOT USED('SQLCURSOR1') OR EOF('SQLCURSOR1') THEN
							.TrackProgress('There were no ADP Departments to export!', LOGIT+SENDIT+NOWAITIT)
						ELSE

							************************* MAIN PROCESSING FOLLOWS ***********************************

							* remove items that are not really departments, such as those with LABORTYPE = 9999.
							* More importantly, if desired, exclude old departments which were all greater than 1000 numerically.
							IF .lExcludeOldADPDepartments THEN
								*lcDeptWhere = " AND VAL(LABORTYPE) < 1000 "
								lcDeptWhere = " AND ( (VAL(LABORTYPE) < 1000) OR (HOMEDEPT = '044400') ) "
							ELSE
								lcDeptWhere = ""
							ENDIF

							IF USED('CURDEPTLIST') THEN
								USE IN CURDEPTLIST
							ENDIF

							SELECT * ;
								FROM SQLCURSOR1 ;
								INTO CURSOR CURDEPTLIST ;
								WHERE LABORTYPE <> '9999' ;
								&lcDeptWhere. ;
								ORDER BY ADPCOMP, DIVISION, LABORTYPE

							*******************************************************************
							* open FoxPro tables
							*******************************************************************

							* open OLD target FoxPro Department Table
							IF USED('DEPARTME') THEN
								USE IN DEPARTME
							ENDIF
							USE (lcOldFoxProDepartmentTable) AGAIN IN 0 ALIAS DEPARTME


							* Open table that gives us old Div Names and Company for OLD Department Table
							IF USED('DIVNAMES') THEN
								USE IN DIVNAMES
							ENDIF
							USE (lcDivnameCompanyLookupTable) AGAIN IN 0 ALIAS DIVNAMES


							* open NEW target FoxPro Department Table
							IF USED('ADPDEPTS') THEN
								USE IN ADPDEPTS
							ENDIF
							USE (lcNewFoxProDepartmentTable) EXCLUSIVE IN 0 ALIAS ADPDEPTS

							* add new departments, assigning CUSERS based on ADP_COMP
							* at the same time unconditionally updating dept descriptions for those that are not new.
							SELECT CURDEPTLIST
							SCAN
								SCATTER MEMVAR
								m.UPDATED = DATETIME()
								m.ADPCOMP = LEFT(m.ADPCOMP,3)
								m.LPAYEXPORT = .T.

								* For now 09/08/05, don't specify any shifts
								*m.CSHIFTS = "123"
								m.CSHIFTS = ""

								* determine if new or existing department in NEW FoxPro Department table
								SELECT ADPDEPTS
								LOCATE FOR ;
									ADPCOMP == m.ADPCOMP AND ;
									DIVISION == m.DIVISION AND ;
									LABORTYPE == m.LABORTYPE

								llNewRecord = NOT FOUND()

								* determine CUSERS based on ADPCOMP, DIV.
								DO CASE
									CASE INLIST(CURDEPTLIST.ADPCOMP,'E89','E88')
										m.CUSERS = "'TEACH','MARK','GREG'"
									CASE INLIST(CURDEPTLIST.DIVISION,'03','38')
										m.CUSERS = "'TEACH','MARK','GREG','HR1','HR2','HR3','HR4','RICH'"
									CASE INLIST(CURDEPTLIST.DIVISION,'04')
										m.CUSERS = "'TEACH','MARK','GREG','HR1','HR2','HR3','HR4','BILL','ARTURO','JOHNK'"
									CASE INLIST(CURDEPTLIST.DIVISION,'05','50','51','52','53','54','55','56','57','58','59')
										m.CUSERS = "'TEACH','MARK','GREG','HR1','HR2','HR3','HR4','OSCAR','ML3','SP40','TIMMY'"
									CASE INLIST(CURDEPTLIST.DIVISION,'06')
										m.CUSERS = "'TEACH','MARK','GREG','HR1','HR2','HR3','HR4','FMLLC19'"
									CASE INLIST(CURDEPTLIST.DIVISION,'07')
										m.CUSERS = "'TEACH','MARK','GREG','HR1','HR2','HR3','HR4','FL10','GARY'"
									CASE INLIST(CURDEPTLIST.DIVISION,'60')
										m.CUSERS = "'TEACH','MARK','GREG','HR1','HR2','HR3','HR4','JONEILL'"
									OTHERWISE
										m.CUSERS = "'TEACH','MARK','GREG','HR1','HR2','HR3','HR4'"
								ENDCASE

								* modify CUSERS based on DEPT
								DO CASE
									CASE INLIST(CURDEPTLIST.LABORTYPE,'0680') AND NOT INLIST(CURDEPTLIST.DIVISION,'50')
										IF (NOT 'JONEILL' $ m.CUSERS)  THEN
											m.CUSERS = m.CUSERS + ",'JONEILL'"
										ENDIF
									CASE INLIST(CURDEPTLIST.LABORTYPE,'0645') AND INLIST(CURDEPTLIST.DIVISION,'04')
										IF (NOT 'PHIL' $ m.CUSERS)  THEN
											m.CUSERS = m.CUSERS + ",'PHIL'"
										ENDIF
									OTHERWISE
										* nothing
								ENDCASE

								IF llNewRecord THEN
									* insert new record into target dept table
									INSERT INTO ADPDEPTS FROM MEMVAR
									lcInsert = "ADPDEPTS.DBF: New Dept: [" + ;
										m.ADPCOMP + "] [" + m.DIVISION + "] [" + m.LABORTYPE + "] Desc.: [" + ;
										LEFT(m.DESCRIPT,30) + "] Users: [" + m.CUSERS + "] When: [" + TTOC(m.UPDATED) + "]"
									.TrackProgress(lcInsert, LOGIT+SENDIT+NOWAITIT)
									.TrackProgress('**** This Department must be added in Kronos ****', LOGIT+SENDIT)
									.nAddCount = .nAddCount + 1
								ELSE
									* not new, but update DESCRIPT to make sure it always matches the latest in ADP;
									* AND ALWAYS UPDATE CUSERS 09/12/05 MB!
									REPLACE ADPDEPTS.DESCRIPT WITH ALLTRIM(m.DESCRIPT), ADPDEPTS.CUSERS WITH m.CUSERS, UPDATED WITH m.UPDATED
									lcInsert = "ADPDEPTS.DBF: Updated Dept: [" + ;
										m.ADPCOMP + "] [" + m.DIVISION + "] [" + m.LABORTYPE + "] Desc.: [" + ;
										LEFT(m.DESCRIPT,30) + "] When: [" + TTOC(m.UPDATED) + "]"
									.TrackProgress(lcInsert, LOGIT+SENDIT)
									.nUpdateCount = .nUpdateCount + 1
								ENDIF


								***** now update OLD FoxPro Department Table
								* first look up Div Name and Company
								SELECT DIVNAMES
								LOCATE FOR DIV_NUM = VAL(m.DIVISION)
								IF FOUND() THEN

									m.DIV_NAME = DIVNAMES.DIV_NAME
									m.COMPANY = DIVNAMES.COMPANY

									m.DIV_NUM = VAL(m.DIVISION)
									m.DEPT_NUM = VAL(m.LABORTYPE)
									m.DEPT_NAME = m.DESCRIPT

									* see if new or existing in old FoxPro Dept table
									SELECT DEPARTME
									LOCATE FOR ;
										DIV_NUM = m.DIV_NUM AND ;
										DEPT_NUM == m.DEPT_NUM

									llNewRecord = NOT FOUND()

									IF llNewRecord THEN
										* insert new record into target dept table
										INSERT INTO DEPARTME FROM MEMVAR
										lcInsert = "DEPARTME.DBF: New Dept - Company: [" + m.COMPANY + ;
											"] Div: [" + TRANSFORM(m.DIV_NUM) + ;
											"] [" + m.DIV_NAME + ;
											"] Dept: [" + TRANSFORM(m.DEPT_NUM) + ;
											"] [" + LEFT(m.DEPT_NAME,30) + ;
											"] When: [" + TTOC(m.UPDATED) + "]"
										.TrackProgress(lcInsert, LOGIT+SENDIT)
									ELSE
										* not new, but update DEPT_NAME to make sure it always matches the latest in ADP,
										* and update division name and company
										REPLACE DEPARTME.COMPANY WITH m.COMPANY, ;
											DEPARTME.DIV_NAME WITH m.DIV_NAME, ;
											DEPARTME.DEPT_NAME WITH m.DEPT_NAME
										lcInsert = "DEPARTME.DBF: Updated Dept - Company: [" + m.COMPANY + ;
											"] Div: [" + TRANSFORM(m.DIV_NUM) + ;
											"] [" + m.DIV_NAME + ;
											"] Dept: [" + TRANSFORM(m.DEPT_NUM) + ;
											"] [" + LEFT(m.DEPT_NAME,30) + ;
											"] When: [" + TTOC(m.UPDATED) + "]"
										.TrackProgress(lcInsert, LOGIT+SENDIT)
									ENDIF
								ELSE
									* unable to get Div Name and Company -- log error
									lcErrorMsg = "**** ERROR: For Division [" + m.DIVISION + "]:  Unable to Determine Division Name and Company Code"
									.TrackProgress(lcErrorMsg, LOGIT+SENDIT)
									.nOldDivNamesNotFound = .nOldDivNamesNotFound + 1
								ENDIF
								.TrackProgress("-------------------------------------", LOGIT+SENDIT)
								.TrackProgress("", LOGIT+SENDIT)

							ENDSCAN


							* report on FoxPro departments no longer in ADP ?????


							************************* END OF MAIN PROCESSING ***********************************

						ENDIF   &&  NOT USED('SQLCURSOR1') OR EOF('SQLCURSOR1')

						CLOSE DATABASES ALL
					ENDIF  &&  .ExecSQL(lcSQL, 'SQLCURSOR1', RETURN_DATA_MANDATORY)

					.TrackProgress('', LOGIT+SENDIT)
					.TrackProgress('EXPORT ADP DEPARTMENT LIST TO FOXPRO process ended normally.', LOGIT+SENDIT+NOWAITIT)

					* prepend export stats onto body text
					.cBodyText = "Number of New Departments (WHICH MUST BE ADDED IN KRONOS): " + TRANSFORM(.nAddCount) + CRLF + ;
						"# of Updated Departments: " + TRANSFORM(.nUpdateCount) + CRLF + ;
						"# of Old Division Names Not FOUND (ERRORS): " + TRANSFORM(.nOldDivNamesNotFound) + CRLF + CRLF + .cBodyText

				ELSE
					* connection error
					.TrackProgress('Unable to connect to ADP Payroll System.', LOGIT+SENDIT)
				ENDIF   &&  .nSQLHandle > 0

			CATCH TO loError

				.TrackProgress('There was an error.',LOGIT+SENDIT)
				.TrackProgress('The Error # is: ' + TRANSFORM(loError.ErrorNo), LOGIT+SENDIT)
				.TrackProgress('The Error Message is: ' + TRANSFORM(loError.MESSAGE), LOGIT+SENDIT)
				.TrackProgress('The Error occurred in Program: ' + TRANSFORM(loError.PROCEDURE), LOGIT+SENDIT)
				.TrackProgress('The Error occurred at line #: ' + TRANSFORM(loError.LINENO), LOGIT+SENDIT)
				lnNumberOfErrors = lnNumberOfErrors + 1
				CLOSE DATA

			ENDTRY

			CLOSE DATA
			WAIT CLEAR
			***************** INTERNAL email results ******************************
			.TrackProgress('About to send status email.',LOGIT)
			.TrackProgress('The logfile is: ' + .cLogFile, SENDIT)
			.TrackProgress('EXPORT ADP DEPARTMENT LIST TO FOXPRO process started: ' + .cStartTime, LOGIT+SENDIT)
			.TrackProgress('EXPORT ADP DEPARTMENT LIST TO FOXPRO process finished: ' + TTOC(DATETIME()), LOGIT+SENDIT)

			*!*				* put some text at top of body
			*!*				.cBodyText = lcTopBodyText + CRLF + CRLF + ;
			*!*					"(Report log follows)" + ;
			*!*					CRLF + CRLF + .cBodyText

			IF .lSendInternalEmailIsOn THEN
				* try to trap error from not having dartmail dll's registered on user's pc...
				TRY
					DO FORM dartmail2 WITH .cSendTo,.cFrom,.cSubject,.cCC,.cAttach,.cBodyText,"A"
					.TrackProgress('Sent status email.',LOGIT)
				CATCH TO loError
					*=MESSAGEBOX('There was an error sending the status email. Please contact Mark Bennett.',0+64,"EXPORT ADP DEPARTMENT LIST TO FOXPRO")
					.TrackProgress('There was an error sending the status email.',LOGIT)
					.TrackProgress('The Error # is: ' + TRANSFORM(loError.ErrorNo), LOGIT)
					.TrackProgress('The Error Message is: ' + TRANSFORM(loError.MESSAGE), LOGIT)
					.TrackProgress('The Error occurred in Program: ' + TRANSFORM(loError.PROCEDURE), LOGIT)
					.TrackProgress('The Error occurred at line #: ' + TRANSFORM(loError.LINENO), LOGIT)
					lnNumberOfErrors = lnNumberOfErrors + 1
					CLOSE DATA
				ENDTRY
			ELSE
				.TrackProgress('Did not send status email: THIS.lSendInternalEmailIsOn = FALSE.',LOGIT)
			ENDIF

		ENDWITH
		RETURN
	ENDFUNC && main


	FUNCTION ExecSQL
		LPARAMETERS tcSQL, tcCursorName, tlNoDataReturnedIsOkay
		LOCAL llRetval, lnResult
		WITH THIS
			* close target cursor if it's open
			IF USED(tcCursorName)
				USE IN (tcCursorName)
			ENDIF
			WAIT WINDOW "Fetching remote data; please wait..." NOWAIT
			lnResult = sqlexec(.nSQLHandle, tcSQL, tcCursorName)
			llRetval = ( lnResult > 0 )
			IF llRetval THEN
				* see if any data came back
				IF NOT tlNoDataReturnedIsOkay THEN
					IF (NOT USED(tcCursorName)) OR EOF(tcCursorName) THEN
						llRetval = .F.
						.TrackProgress('No data was returned by this query: ' +CRLF + tcSQL, LOGIT+SENDIT)
					ENDIF
				ENDIF
			ELSE
				.TrackProgress("Result Code [" + TRANSFORM(lnResult) + "] returned by this query: " + tcSQL, LOGIT+SENDIT)
			ENDIF
			WAIT CLEAR
			RETURN llRetval
		ENDWITH
	ENDFUNC


	PROCEDURE TrackProgress
		* do any combination of Wait Window, writing to logfile, and adding to body of email,
		* based on nFlags parameter.
		LPARAMETERS tcExpression, tnFlags
		WITH THIS
			IF BITAND(tnFlags,LOGIT) = LOGIT THEN
				IF .lLoggingIsOn THEN
					?
					? .cProcessName + "  " + TTOC(DATETIME()) + ": " + tcExpression
				ENDIF
			ENDIF
			IF .lWaitWindowIsOn THEN
				IF BITAND(tnFlags,NOWAITIT) = NOWAITIT THEN
					WAIT WINDOW tcExpression NOWAIT
				ENDIF
				IF BITAND(tnFlags,WAITIT) = WAITIT THEN
					WAIT WINDOW tcExpression TIMEOUT .nWaitWindowTimeout
				ENDIF
			ENDIF
			IF BITAND(tnFlags,SENDIT) = SENDIT THEN
				IF .lSendInternalEmailIsOn THEN
					.cBodyText = .cBodyText + tcExpression + CRLF + CRLF
				ENDIF
			ENDIF
		ENDWITH
	ENDPROC  &&  TrackProgress

ENDDEFINE

