parameters lwebsite, lworksht, cdelloc, xspecialxls, xignoreret, xschedrpt
**lWebsite - called from website
**lWorkSht - speifies format, excel worksheet or pdf
**cDelloc  - list of dellocs to include separated by commas
**			 (ie. federated only wants aeropostale data - ACI or ADI)
**xschedrpt - .t./.f. indicates run from kpirpt.prg for scheduled emailing of report
**

cdelloc=iif(type("cdelloc")#"C","",cdelloc)

store "" to clblfield1, ctxtfield1

if lwebsite or xschedrpt
	use f:\wo\wodata\daily in 0
	use f:\wo\wodata\wolog in 0
	use f:\wo\wodata\detail in 0
	use f:\wo\wodata\manifest in 0

	xsqlexec("select * from pudlocs",,,"wo")
	index on accountid tag accountid
	set order to

	xacctname=acctname(xaccountid)

	select offices
	locate for locale=xoffice and priority=1
	goffcity=offices.city

	store iif(latest,"Activity from: Current Status (8 day window)", ;
		"Activity from:            " + dtoc(xstartdt) + "  to  " + dtoc(xenddt)) ;
		to creporttitle
	store creporttitle+"  Locations: "+iif(empty(cdelloc), "All", cdelloc) to creporttitle

	**accounts now in the extended format (xspecialxls)
	**5837 - Sears
	**6493 - Office Depot
	**6565 - TJX
	**6221/6521 - Synclaire/BCNY
	**1285 - Age Group
	**4484 - Sun Express
	xspecialxls=iif(lworksht and xoffice="C" and inlist(xaccountid,6493,6565,6221,6521,5837,1285,4484),.t.,.f.)
endif

do case
case xoffice="ALL" and empty(xaccountid)
	cfilter = "between(w.wo_date, xstartdt, xenddt)"
case xoffice="ALL"
	cfilter = "w.accountid = xaccountid and between(w.wo_date, xstartdt, xenddt)"
case empty(xaccountid)
	cfilter = "between(w.wo_date, xstartdt, xenddt) and w.office = xoffice"
otherwise
	cfilter = "w.accountid = xaccountid and between(w.wo_date, xstartdt, xenddt) and w.office = xoffice"
endcase

do case
case upper(cfrttype) = "ALL"
	**delete DOMESTICS for the "special" report
	if xspecialxls
		cfilter = cfilter + " and w.type # 'D'"
	endif
case upper(cfrttype) = "AIR"
	cfilter = cfilter + " and w.type = 'A'"
case upper(cfrttype) = "DOMESTIC"
	cfilter = cfilter + " and w.type = 'D'"
case upper(cfrttype) = "RAIL"
	cfilter = cfilter + " and w.type = 'R'"
otherwise &&ocean
	cfilter = cfilter + " and w.type = 'O'"
endcase

if latest &&only show 8 day window for p/u's and d/l's
	cfilter = cfilter + " and (empty(w.pickedup) or w.pickedup>date()-8)"+;
		" and (empty(w.delivered) or w.delivered>date()-8)"
endif

**need to pull all w/o's for the given date range for this acct
**  "Called" - if AIR freight and wolog.pickedup is NOT empty,
**		use wolog.pickedup, otheriwise use the last called date of availlog
**  "Remarks" - if current record found in daily, "P/U in Progress",
**		if AIR and pickup is NOT empty, "Available
**		else use remarks last call in availlog
**  each TYPE need different fields and there for labels... hence all the IIF()'s
	
ctxtfield1 = iif(!empty(ctxtfield1), ctxtfield1, 'Iif(w.type = "A", Iif(IsNull(W.FWDR), "", LEFT(W.FWDR, 15)), '+;
	'Iif(w.type = "D", IIF(w.SIZE = "CONSOL", PADR("Delivery",15), PADR("Pick Up",15)), LEFT(w.VESSEL, 15)))')

xadditflds=iif(xspecialxls and inlist(xaccountid,&gJonesAcctList,1747),", space(120) as ibd","")

wait "Gathering data..." window nowait noclear

select w.*, {} as ftexp, det.delloc as det_delloc, {} as called, space(30) as xstatus, ;
	iif(!empty(clblfield1), clblfield1, iif(w.type = "A", padr("Forwarder",10), ;
	iif(w.type = "D", padr("Type",10), padr("Vessel",10)))) as lbl_field1, ;
	&ctxtfield1 as txt_field1, ;
	iif(w.accountid=6420,padr("Cust"+chr(13)+"Ref No",15),iif(inlist(w.accountid,&gJonesAcctList,1747) and w.type#"D",padr(chr(13)+"Hot",15),iif(w.type = "D", padr(chr(13)+"Ref No",15), padr("Broker"+chr(13)+"Ref No",15)))) as lbl_field2, ;
	iif(w.accountid=6420,padr(w.cust_ref,15),iif(w.type = "D", padr(w.brokref,15), padr(w.brokref,15))) as txt_field2, ;
	iif(w.type = "A", padr("Master AWB",10), iif(w.type = 'D', padr("From Loc",10), padr("Container",10))) as lbl_field3, ;
	iif(w.type = "A", padr(w.awb,20), iif(w.type = 'D', space(20), ;
	padr(dispcont(w.container),20))) as txt_field3, ;
	iif(w.type = "D", padr("Origin",15), padr("P/U From",15)) as lbl_field4, ;
	iif(w.type = "D", space(30), padr(w.puloc,30)) as txt_field4, ;
	padr(alltrim(acct.address)+", "+alltrim(acct.city)+", "+alltrim(acct.state),50) as destination, ;
	padr(alltrim(acct.city),30) as delcity, ;
	{} as dldisp, {} as mfstdeliv &xadditflds;
	from wolog w ;
	left join detail det on det.wo_num = w.wo_num ;
	left join account acct on acct.accountid = w.accountid ;
	where &cfilter ;
	group by w.wo_num ;
	order by w.office, w.acctname, w.type, w.wo_num ;
	into cursor temp readwrite

scan
	**update availability info (called, xstatus, ftexp)
	xsqlexec("select * from availlog where wo_num="+transform(temp.wo_num)+" order by adddt desc","xxtemp",,"wo")
	
	xxstatus=iif((temp.type="A" or eof("xxtemp")) and !empty(temp.pickedup), "AVAILABLE", iif(eof("xxtemp"),"NOT AVAILABLE",xxtemp.remarks))
	replace xstatus with xxstatus in temp

	**for most reports want to show the last called date for avail - mvw 04/05/16
	replace called with nul2empty(xxtemp.called) in temp
 
	locate for !emptynul(ftexpires)
	if found()
		replace ftexp with xxtemp.ftexpires in temp
	endif

	**for the extended format what the first call for availabilty and it will be ststed as such in the column header - mvw 04/05/16
	if xspecialxls and !inlist(xaccountid,&gjonesacctlist,1747)
		go bottom &&go to bottom for first call made
		**Changed to alwasy grab 1st called date, not pickedup for AIR... i dont understand that logic - mvw 03/11/16
	*	replace called with iif(temp.type="A" and !empty(temp.pickedup), temp.pickedup, xxtemp.called) in temp
		replace called with nul2empty(xxtemp.called) in temp
	endif

	select daily
	locate for wo_num = temp.wo_num and dispatched=date() and action='PU' and !undel and emptynul(pickedup)
	if found()
		replace xstatus with "P/U IN PROGRESS" in temp
	endif

	select daily
	locate for wo_num = temp.wo_num and action='DL' and !undel
	if found()
		replace dldisp with daily.dispatched in temp
	endif

	if seek(temp.wo_num,"manifest","orig_wo")
		replace mfstdeliv with manifest.delivered in temp
	endif
endscan

wait "Creating spreadsheet..." window nowait noclear

**had pudlocs joined into cursor, but took forever with join on accountid+dash (punum tag)
scan for type = 'D'
	select pudlocs
	locate for accountid = temp.accountid and dash = temp.toloc
	if found()
		replace destination with alltrim(pudlocs.address)+", "+alltrim(pudlocs.city)+", "+alltrim(pudlocs.state), ;
				delcity with pudlocs.city in temp
	endif

	locate for accountid = temp.accountid and dash = temp.fromloc
	if found()
		replace temp.txt_field3 with pudlocs.acct_name, temp.txt_field4 with pudlocs.city
	endif
endscan

if xignoreret
	delete for !emptynul(returned) in temp
endif

if !empty(cdelloc)
	select * from temp where inlist(det_delloc, &cdelloc) into cursor temp
endif

select temp
locate

if !lwebsite and !xschedrpt and eof()
	wait clear
	messagebox("No data found for the selected criteria.",16,"No Data")
	return
endif

if !eof() and lworksht
	xfile = "h:\pdf\"+'oceanrpt.xls'

	do case
	case inlist(xaccountid,&gvfacctlist)
		select acctname, wo_num, puloc, destination, iif(type = 'A', txt_field3, space(len(txt_field3))) as awb, ;
			"TGF" as carrier, iif(type # 'A', txt_field3, space(len(txt_field3))) as ctr, ftexp, ;
			dldisp, iif(isnull(mfstdeliv), delivered, mfstdeliv) as delivered, left(alltrim(wostatus)+" / "+alltrim(notes),250) as comments ;
			from temp into cursor csrtemp

		copy to (xfile) type xl5

		*

		oexcel = createobject("Excel.Application")
		oexcel.displayalerts = .f.

		oworkbook = oexcel.workbooks.open(xfile)

		oworkbook.worksheets[1].range("A1:K1").horizontalalignment = -4108
		oworkbook.worksheets[1].range("A1:K1").font.bold = .t.
		oworkbook.worksheets[1].columns[1].columnwidth = 18
		oworkbook.worksheets[1].columns[2].columnwidth = 9
		oworkbook.worksheets[1].columns[3].columnwidth = 28
		oworkbook.worksheets[1].columns[4].columnwidth = 28
		oworkbook.worksheets[1].columns[5].columnwidth = 14
		oworkbook.worksheets[1].columns[6].columnwidth = 7
		oworkbook.worksheets[1].columns[7].columnwidth = 14
		oworkbook.worksheets[1].columns[8].columnwidth = 10
		oworkbook.worksheets[1].columns[9].columnwidth = 10
		oworkbook.worksheets[1].columns[10].columnwidth = 10
		oworkbook.worksheets[1].columns[11].columnwidth = 10
		oworkbook.worksheets[1].columns[12].columnwidth = 50

		oworkbook.worksheets[1].range("A1") = "Coalition"
		oworkbook.worksheets[1].range("B1") = "W/O No."
		oworkbook.worksheets[1].range("C1") = "Pickup Location"
		oworkbook.worksheets[1].range("D1") = "Destination"
		oworkbook.worksheets[1].range("E1") = "Awb"
		oworkbook.worksheets[1].range("F1") = "Carrier"
		oworkbook.worksheets[1].range("G1") = "Container"
		oworkbook.worksheets[1].range("H1") = "Last Free"
		oworkbook.worksheets[1].range("I1") = "Departure"
		oworkbook.worksheets[1].range("J1") = "Appt"
		oworkbook.worksheets[1].range("K1") = "Delivered"
		oworkbook.worksheets[1].range("L1") = "Comments"

	case xspecialxls and inlist(xaccountid,&gjonesacctlist,1747)
		scan
			select arrival from manifest where orig_wo=temp.wo_num group by arrival into cursor xtemp2
			**grab from detail if entered there manually if not manifested yet - mvw 10/16/15
			if eof()
				select arrival from detail where wo_num=temp.wo_num group by arrival into cursor xtemp2
			endif
			
			xibd=""
			scan for !empty(arrival)
				xibd=xibd+iif(empty(xibd),"",", ")+alltrim(arrival)
			endscan
			use in xtemp2

			replace ibd with xibd in temp

			**add delloc - MVW 10/16/15
			select delloc from detail where wo_num=temp.wo_num group by delloc into cursor xtemp2
			replace det_delloc with iif(reccount("xtemp2")>1,"SPLIT",xtemp2.delloc) in temp
			use in xtemp2
		endscan

		select det_delloc, wo_num, vessel, brokref, txt_field3 as ctrawb, size, quantity, left(qty_type,1) as qtytype, iif(isnull(called),space(5),left(transform(called),5)) as calldt, ;
			wostatus, remarks, ibd, left(transform(ftexp),5), left(transform(appt),5), appttm, left(transform(pickedup),5), left(transform(delivered),5), ;
			left(transform(returned),5) from temp into cursor csrtemp

		copy to (xfile) type xl5

		*

		oexcel = createobject("Excel.Application")
		oexcel.displayalerts = .f.

		oworkbook = oexcel.workbooks.open(xfile)

		oworkbook.worksheets[1].range("A1"+":R1").horizontalalignment = -4108
		oworkbook.worksheets[1].range("A1:R1").font.bold = .t.

		oworkbook.worksheets[1].range("A1") = "Dest"
		oworkbook.worksheets[1].range("B1") = "W/O No."
		oworkbook.worksheets[1].range("C1") = "Vessel"
		oworkbook.worksheets[1].range("D1") = "Hot"
		oworkbook.worksheets[1].range("E1") = "Ctr / AWB"
		oworkbook.worksheets[1].range("F1") = "Size"
		oworkbook.worksheets[1].range("G1") = "Qty"
		oworkbook.worksheets[1].range("H1") = ""
		oworkbook.worksheets[1].range("I1") = "Call Dt"
		oworkbook.worksheets[1].range("J1") = "Status"
		oworkbook.worksheets[1].range("K1") = "Remarks"
		oworkbook.worksheets[1].range("L1") = "IBD"
		oworkbook.worksheets[1].range("M1") = "F/T Exp"
		oworkbook.worksheets[1].range("N1") = "Appt"
		oworkbook.worksheets[1].range("O1") = "Appt Tm"
		oworkbook.worksheets[1].range("P1") = "P/U"
		oworkbook.worksheets[1].range("Q1") = "Deliv"
		oworkbook.worksheets[1].range("R1") = "Return"

		oworkbook.worksheets[1].columns[4].columnwidth = 4
		oworkbook.worksheets[1].columns[6].columnwidth = 7
		oworkbook.worksheets[1].columns[7].columnwidth = 6
		oworkbook.worksheets[1].columns[8].columnwidth = 1.57
		oworkbook.worksheets[1].columns[9].columnwidth = 7
		oworkbook.worksheets[1].columns[13].columnwidth = 7
		oworkbook.worksheets[1].columns[14].columnwidth = 6
		oworkbook.worksheets[1].columns[16].columnwidth = 6
		oworkbook.worksheets[1].columns[17].columnwidth = 6
		oworkbook.worksheets[1].columns[18].columnwidth = 7

	case xspecialxls &&new format for tjx, office depot, bcny, etc
		**need to capture chassis over 2 days for TJX (6565) only, otherwise do not want the field at all - mvw 04/13/16
		xchassisfld=iif(xaccountid=6565,"000 as chassistm,","")
		select acctname, wo_num, wo_date, txt_field1, txt_field2, txt_field3, size, quantity, qty_type, called, eta, ;
				iif(called<eta,000,called-eta) as daystocall, availdt, ftexp, appt as appt1, {} as appt2, {} as appt3, appttm, pickedup, ;
				xstatus, iif(pickedup<availdt,000,pickedup-availdt) as availtopu, iif(pickedup<ftexp,000,pickedup-ftexp) as overftexp, ;
				pickedup as deltotoll, iif(delivered<pickedup,000,delivered-pickedup) as daysinyard, appt2 as delappt, appttm2 as delappttm, delivered, mtinyard, ;
				iif(mtinyard<delivered,000,mtinyard-delivered) as deltostrip, returned, iif(returned<mtinyard,000,returned-mtinyard) as striptoret, ;
				iif(returned<delivered,000,returned-delivered) as deltoret, iif(returned<pickedup,000,returned-pickedup) as putoret, &xchassisfld;
				000 as over5days, txt_field4 ;
			from temp into cursor csrtemp readwrite

		**changed over5days to show days over 5 days even if not returned yet - mvw 03/11/16
		replace all over5days with iif(!emptynul(returned),iif(putoret<5,0,putoret-5),iif(date()-pickedup<5,0,(date()-pickedup)-5)),;
				xstatus with iif(emptynul(pickedup),xstatus,iif(!emptynul(returned),"RETURNED",iif(!emptynul(mtinyard),"EMPTY IN YARD",iif(!emptynul(delivered),"DELIVERED","DROPPED TO TOLL YARD"))))

		if type('csrtemp.chassistm')#"U"
			replace all chassistm with iif(!emptynul(returned),iif(putoret<2,0,putoret-2),iif(date()-pickedup<2,0,(date()-pickedup)-2))
		endif

		**get all appt reschedules
		scan
			**only grab most recent 2 records - order by needs to be zdatetime descending
			xsqlexec("select top 2 * from info where wo_num = "+transform(csrtemp.wo_num)+" and message Like 'P/U Appt Rescheduled%' order by zdatetime desc","xxinfo",,"wo")
			store {} to xdate1, xdate2
			scan
				if empty(xdate1)
					xdate1=ctod(substr(details,at("from",details)+5,10))
				else
					xdate2=ctod(substr(details,at("from",details)+5,10))
				endif
			endscan
			use in xxinfo

			do case
			case !empty(xdate2)
				replace appt3 with appt1, appt2 with xdate1, appt1 with xdate2 in csrtemp
			case !empty(xdate1)
				replace appt2 with appt1, appt1 with xdate1 in csrtemp
			endcase
		endscan

		copy to (xfile) type xl5

		oexcel = createobject("Excel.Application")
		oexcel.displayalerts = .f.

		oworkbook = oexcel.workbooks.open(xfile)

		xlastcolumn=iif(type('csrtemp.chassistm')#"U","AJ","AI")

		**change colors/fonts of column headers
		oworkbook.worksheets[1].range("A1:"+xlastcolumn+"1").horizontalalignment = -4108
		oworkbook.worksheets[1].range("A1:"+xlastcolumn+"1").wraptext = .t.
		oworkbook.worksheets[1].range("A1:"+xlastcolumn+"1").rowheight = 38.25
		oworkbook.worksheets[1].range("A1:"+xlastcolumn+"1").font.bold = .t.
		oworkbook.worksheets[1].range("A1:"+xlastcolumn+"1").Interior.Color=Rgb(51,102,255)
		oworkbook.worksheets[1].range("A1:"+xlastcolumn+"1").font.Color=Rgb(255,255,255)
		
		oworkbook.worksheets[1].range("A1") = "Account"
		oworkbook.worksheets[1].range("B1") = "W/O No."
		oworkbook.worksheets[1].range("C1") = "W/O Creation"
		oworkbook.worksheets[1].range("D1") = "Vessel"
		oworkbook.worksheets[1].range("E1") = "Reference No"
		oworkbook.worksheets[1].range("F1") = "Ctr/Awb"
		oworkbook.worksheets[1].range("G1") = "Size"
		oworkbook.worksheets[1].range("H1") = "Qty"
		oworkbook.worksheets[1].range("I1") = "Type"
		oworkbook.worksheets[1].range("J1") = "1st Call For Availability"
		oworkbook.worksheets[1].range("K1") = "ETA"
		oworkbook.worksheets[1].range("L1") = "Days From ETA To Call"
		oworkbook.worksheets[1].range("M1") = "Container Avail"
		oworkbook.worksheets[1].range("N1") = "F/T Exp"
		oworkbook.worksheets[1].range("O1") = "1st Appt Scheduled"
		oworkbook.worksheets[1].range("P1") = "2nd Appt Scheduled"
		oworkbook.worksheets[1].range("Q1") = "3rd Appt Scheduled"
		oworkbook.worksheets[1].range("R1") = "Appt Tm"
		oworkbook.worksheets[1].range("S1") = "Actual P/U Date"
		oworkbook.worksheets[1].range("T1") = "Status"
		oworkbook.worksheets[1].range("U1") = "Days From Avail To P/U"
		oworkbook.worksheets[1].range("V1") = "Days Beyond FT/Exp"
		oworkbook.worksheets[1].range("W1") = "Dropped To Toll Yard"
		oworkbook.worksheets[1].range("X1") = "Days in Off-Site Yard"
		oworkbook.worksheets[1].range("Y1") = "Del Appt"
		oworkbook.worksheets[1].range("Z1") = "Del Appt Tm"
		oworkbook.worksheets[1].range("AA1") = "Delivered"
		oworkbook.worksheets[1].range("AB1") = "Unloaded"
		oworkbook.worksheets[1].range("AC1") = "Days In On-Site Yard"
		oworkbook.worksheets[1].range("AD1") = "Returned"
		oworkbook.worksheets[1].range("AE1") = "Days From Unload To Return"
		oworkbook.worksheets[1].range("AF1") = "Days From Deliv To Return"
		oworkbook.worksheets[1].range("AG1") = "Days From P/U To Return"

		**if chassistm field exists (see above), columns change - mvw 04/13/16
		if type('csrtemp.chassistm')#"U"
			oworkbook.worksheets[1].range("AH1") = "Chassis Over 2 Days"
			oworkbook.worksheets[1].range("AI1") = "Days Beyond 5 Day Allowable"
			oworkbook.worksheets[1].range("AJ1") = "P/U Loc"
		else
			oworkbook.worksheets[1].range("AH1") = "Days Beyond 5 Day Allowable"
			oworkbook.worksheets[1].range("AI1") = "P/U Loc"
		endif

		oworkbook.worksheets[1].Range("A1:"+xlastcolumn+Alltrim(Str(Reccount("csrtemp")+1))).Columns.autoFit()

		**need to adjust manually some of the column widths
		oworkbook.worksheets[1].range("E1").columnwidth = max(oworkbook.worksheets[1].range("E1").columnwidth,10)
		oworkbook.worksheets[1].range("J1").columnwidth = max(oworkbook.worksheets[1].range("J1").columnwidth,11)
		oworkbook.worksheets[1].range("L1").columnwidth = max(oworkbook.worksheets[1].range("L1").columnwidth,10)
		oworkbook.worksheets[1].range("M1").columnwidth = max(oworkbook.worksheets[1].range("M1").columnwidth,9)
		oworkbook.worksheets[1].range("O1").columnwidth = max(oworkbook.worksheets[1].range("O1").columnwidth,10)
		oworkbook.worksheets[1].range("P1").columnwidth = max(oworkbook.worksheets[1].range("P1").columnwidth,10)
		oworkbook.worksheets[1].range("Q1").columnwidth = max(oworkbook.worksheets[1].range("Q1").columnwidth,10)
		oworkbook.worksheets[1].range("S1").columnwidth = max(oworkbook.worksheets[1].range("S1").columnwidth,9)
		oworkbook.worksheets[1].range("U1").columnwidth = max(oworkbook.worksheets[1].range("U1").columnwidth,11)
		oworkbook.worksheets[1].range("V1").columnwidth = max(oworkbook.worksheets[1].range("V1").columnwidth,9)
		oworkbook.worksheets[1].range("W1").columnwidth = max(oworkbook.worksheets[1].range("W1").columnwidth,11)
		oworkbook.worksheets[1].range("X1").columnwidth = max(oworkbook.worksheets[1].range("X1").columnwidth,9)
		oworkbook.worksheets[1].range("Z1").columnwidth = max(oworkbook.worksheets[1].range("Z1").columnwidth,9)
		oworkbook.worksheets[1].range("AA1").columnwidth = max(oworkbook.worksheets[1].range("AA1").columnwidth,11)
		oworkbook.worksheets[1].range("AB1").columnwidth = max(oworkbook.worksheets[1].range("AB1").columnwidth,9)
		oworkbook.worksheets[1].range("AC1").columnwidth = max(oworkbook.worksheets[1].range("AC1").columnwidth,9)
		oworkbook.worksheets[1].range("AD1").columnwidth = max(oworkbook.worksheets[1].range("AD1").columnwidth,9)
		oworkbook.worksheets[1].range("AE1").columnwidth = max(oworkbook.worksheets[1].range("AE1").columnwidth,11)
		oworkbook.worksheets[1].range("AF1").columnwidth = max(oworkbook.worksheets[1].range("AF1").columnwidth,11)
		oworkbook.worksheets[1].range("AG1").columnwidth = max(oworkbook.worksheets[1].range("AG1").columnwidth,11)

		oworkbook.worksheets[1].range("AH1").columnwidth = max(oworkbook.worksheets[1].range("AH1").columnwidth,10)
		**if chassistm field exists (see above), columns change - mvw 04/13/16
		if type('csrtemp.chassistm')#"U"
			oworkbook.worksheets[1].range("AI1").columnwidth = max(oworkbook.worksheets[1].range("AI1").columnwidth,10)
		endif

		**change the backcolor of day calculation cells if > 0, ie if over the free time allowed
		for i=1 to reccount("csrtemp")
			xrow=i+1
			oworkbook.worksheets[1].range("V"+transform(xrow)).interior.color = iif(oworkbook.worksheets[1].range("V"+transform(xrow)).value=0,rgb(255,255,255),rgb(255,101,101))
			oworkbook.worksheets[1].range("AH"+transform(xrow)).interior.color = iif(oworkbook.worksheets[1].range("AH"+transform(xrow)).value=0,rgb(255,255,255),rgb(255,101,101))
			**if chassistm field exists (see above), columns change - mvw 04/13/16
			if type('csrtemp.chassistm')#"U"
				oworkbook.worksheets[1].range("AI"+transform(xrow)).interior.color = iif(oworkbook.worksheets[1].range("AI"+transform(xrow)).value=0,rgb(255,255,255),rgb(255,101,101))
			endif
		endfor
		
		oworkbook.worksheets[1].range("A1").EntireRow.Insert()
		oworkbook.worksheets[1].range("A1").EntireRow.Insert()
		oworkbook.worksheets[1].range("A1").EntireRow.Insert()
		oworkbook.worksheets[1].range("A1").EntireRow.Insert()
		oworkbook.worksheets[1].range("A1").EntireRow.Insert()

		oworkbook.worksheets[1].range("A1:"+xlastcolumn+"1").merge()
		oworkbook.worksheets[1].range("A1:"+xlastcolumn+"1").horizontalalignment = -4108
		oworkbook.worksheets[1].range("A1:"+xlastcolumn+"1").font.Color=Rgb(255,255,255)

		oworkbook.worksheets[1].range("A1:"+xlastcolumn+"1").Interior.Color=Rgb(51,102,255)

		oworkbook.worksheets[1].range("A1:"+xlastcolumn+"1").rowheight = 24
		oworkbook.worksheets[1].range("A1:"+xlastcolumn+"1").font.bold = .t.
		oworkbook.worksheets[1].range("A1:"+xlastcolumn+"1").font.size=14

		oworkbook.worksheets[1].range("A2:"+xlastcolumn+"5").Interior.Color=Rgb(75,172,198)

		if !empty(xaccountid)
			oworkbook.worksheets[1].columns(1).Delete
		endif

*		oworkbook.worksheets[1].range("A1:"+xlastcolumn+"1").value="Account Activity Report"
		oworkbook.worksheets[1].range("A1").value="Account Activity Report"

		oworkbook.worksheets[1].range("A3:B4").rowheight=16
		oworkbook.worksheets[1].range("A3:B4").font.bold=.t.
		oworkbook.worksheets[1].range("A3:B4").font.size=12
		oworkbook.worksheets[1].range("A3").value="Account:"
		oworkbook.worksheets[1].range("B3").value=iif(empty(xaccountid),"All Accounts",alltrim(xacctname))
		oworkbook.worksheets[1].range("A4").value="Dates: "

		if latest
			oworkbook.worksheets[1].range("B4").value="Picked Up and Delivered Last 8 Days"
		else
			oworkbook.worksheets[1].range("B4").value=transform(xstartdt)+" - "+transform(xenddt)
		endif

		oworkbook.worksheets[1].range("A1").columnwidth = max(oworkbook.worksheets[1].range("A1").columnwidth,11)

	otherwise
		select office, wo_num, wo_date, txt_field1, txt_field2, txt_field3, size, quantity, qty_type, called, xstatus, eta, availdt, ftexp, appt, appttm, pickedup, ;
				pickedup-availdt as aviltopu, iif(pickedup>ftexp,pickedup-ftexp,0) as overlfd, appt2 as delappt, appttm2 as delappttm, delivered, returned, ;
				returned-pickedup as putoret, txt_field4, stripped ;
			from temp into cursor csrtemp

		copy to (xfile) type xl5

		oexcel = createobject("Excel.Application")
		oexcel.displayalerts = .f.

		oworkbook = oexcel.workbooks.open(xfile)

		oworkbook.worksheets[1].range("A1"+":Z1").horizontalalignment = -4108
		oworkbook.worksheets[1].range("A1"+":Z1").wraptext = .t.
		oworkbook.worksheets[1].range("A1"+":Z1").rowheight = 38.25
		oworkbook.worksheets[1].range("A1"+":Z1").font.bold = .t.
		
		oworkbook.worksheets[1].range("A1") = "Office"
		oworkbook.worksheets[1].range("B1") = "W/O No."
		oworkbook.worksheets[1].range("C1") = "Date"
		oworkbook.worksheets[1].range("D1") = "Vessel"
		oworkbook.worksheets[1].range("E1") = iif(inlist(xaccountid,&gJonesAcctList,1747),"Hot","Reference No")
		oworkbook.worksheets[1].range("F1") = "Ctr/Awb"
		oworkbook.worksheets[1].range("G1") = "Size"
		oworkbook.worksheets[1].range("H1") = "Qty"
		oworkbook.worksheets[1].range("I1") = "Type"
		oworkbook.worksheets[1].range("J1") = "Last Call For Availability"
		oworkbook.worksheets[1].range("K1") = "Status"
		oworkbook.worksheets[1].range("L1") = "ETA"
		oworkbook.worksheets[1].range("M1") = "Avail"
		oworkbook.worksheets[1].range("N1") = "F/T Exp"
		oworkbook.worksheets[1].range("O1") = "Appt"
		oworkbook.worksheets[1].range("P1") = "Appt Tm"
		oworkbook.worksheets[1].range("Q1") = "P/U Date"
		oworkbook.worksheets[1].range("R1") = "Days From Avail To P/U"
		oworkbook.worksheets[1].range("S1") = "Days Beyond LFD"
		oworkbook.worksheets[1].range("T1") = "Del Appt"
		oworkbook.worksheets[1].range("U1") = "Del Appt Tm"
		oworkbook.worksheets[1].range("V1") = "Delivered"
		oworkbook.worksheets[1].range("W1") = "Returned"
		oworkbook.worksheets[1].range("X1") = "Days From P/U To Return"
		oworkbook.worksheets[1].range("Y1") = "P/U Loc"
		oworkbook.worksheets[1].range("Z1") = "Stripped"

		oworkbook.worksheets[1].Range("A1:Z"+Alltrim(Str(Reccount("csrtemp")+1))).Columns.autoFit()

		**need to adjust manually some of the column widths
		oworkbook.worksheets[1].range("I1").columnwidth = max(oworkbook.worksheets[1].range("I1").columnwidth,10.5)
		oworkbook.worksheets[1].range("S1").columnwidth = max(oworkbook.worksheets[1].range("S1").columnwidth,9)
		oworkbook.worksheets[1].range("U1").columnwidth = max(oworkbook.worksheets[1].range("U1").columnwidth,9)
		oworkbook.worksheets[1].range("V1").columnwidth = max(oworkbook.worksheets[1].range("V1").columnwidth,9)
	endcase

	if latest
		oworkbook.worksheets[1].name = "Activity - Current Status"
	else
		oworkbook.worksheets[1].name = "Activity From "+strtran(left(transform(xstartdt),5),"/","-")+" To "+strtran(left(transform(xenddt),5),"/","-")
	endif
	oworkbook.save()

	use in csrtemp

	if !lwebsite and !xschedrpt
		oexcel.visible=.t.
	else
		oexcel.quit()
		release oworkbook, oexcel
	endif
endif

select temp
locate

wait clear


if lwebsite &&create .pdf file put into MEMO field of PDF_FILE.DBF\
	if lworksht
		insert into pdf_file (unique_id) values (reports.unique_id)
		xwebfile = tempfox+alltrim(str(reports.unique_id))+".xls"

		if eof() &&need something to distinguish from "poller down"
			select pdf_file
			replace memo with "NO DATA"
		else
			copy file &xfile to &xwebfile

			select pdf_file
			**in order to avoid "VFPOLEDB" "provider ran out of memory" errors for large files (>2mb) no longer move file 
			**  thru linked server, just copy file directly to \\iis2\apps\CargoTrackingWeb\reports\ - mvw 03/08/13
			*append memo memo from &xwebfile overwrite
			copy file "&xwebfile" to \\iis2\apps\CargoTrackingWeb\reports\*.*
			replace memo with "FILE COPIED TO REPORT FOLDER" in pdf_file
		endif

	else
		loadwebpdf("OCEANRPT")
	endif
endif

if lwebsite or xschedrpt
	use in temp
	use in daily
	use in wolog
	use in detail
	use in manifest
	use in pudlocs
endif
