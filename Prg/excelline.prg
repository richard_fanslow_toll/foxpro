lparameters xrange, xside, xweight

with &xsheet

do case
case xside="left"
	xsidenum=7
case xside="right"
	xsidenum=10
case xside="top"
	xsidenum=8
case xside="bottom"
	xsidenum=9
otherwise
	set step on 
endcase

if empty(xweight) 
	xtypenum=1
	xweightnum=0
else
	do case
	case xweight="double"
		xtypenum=-4119
		xweightnum=0
	case xweight="extra dark"
		xtypenum=1
		xweightnum=4
	case xweight="dark"
		xtypenum=1
		xweightnum=-4138
	case xweight="medium"
		xtypenum=1
		xweightnum=2
	case xweight="light"
		xtypenum=1
		xweightnum=1
	otherwise
		set step on 
	endcase
endif

.range(xrange).borders(xsidenum).linestyle = xtypenum

if xweightnum#0
	.range(xrange).borders(xsidenum).weight = xweightnum
endif

endwith