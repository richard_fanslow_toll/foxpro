lparameters xform

whdorefresh(xform,"WO")
whdorefresh(xform,"FSHEET")
whdorefresh(xform,"TABLEWO")
*whdorefresh(xform,"DCLMTS")
whdorefresh(xform,"TRACKWO")
whdorefresh(xform,"DAY")

****
procedure whdorefresh

lparameters xform, xcheckform

if xform#xcheckform
	xdy = ascan(oforms.iaforminstances,xcheckform,1)
	if xdy#0
		xdy = asubscript(oforms.iaforminstances,xdy,1)
		osource = oforms.iaforminstances[xdy,1]
		do case
		case xcheckform="WO"
			osource.zzgetname(.t.)
		case xcheckform="TABLEWO"
			osource.zzdata()
		otherwise
			osource.zzgetname()
		endcase
	endif
endif
