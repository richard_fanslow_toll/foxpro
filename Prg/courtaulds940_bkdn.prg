*!* m:\dev\prg\courtaulds_bkdn.prg
CLEAR
lBrowfiles = .F.

lCheckStyle = .T.
lPrepack = .F.
lPick = .T.
lSolidPack = .F.
lJCP = .F.
units = .T.
ptctr = 0
m.isa_num = ""
m.color=""

SELECT x856
COUNT FOR TRIM(segment) = "ST" TO nSegCnt
cSegCnt = ALLTRIM(STR(nSegCnt))

WAIT WINDOW "Now in 940 Breakdown" TIMEOUT 1
WAIT "There are "+cSegCnt+" P/Ts in this file" WINDOW TIMEOUT 1
SELECT x856
LOCATE

*!* Constants
m.acct_name = "COURTAULDS"
m.acct_num = nAcctNum
m.careof = " "
m.sf_addr1 = "16 EAST 34TH STREET"
m.sf_csz = "NEW YORK, NY 10016"
m.printed = .F.
m.print_by = ""
m.printdate = DATE()
*!* End constants

*!*	IF !USED("uploaddet")
*!*		IF lTesting
*!*			USE F:\ediwhse\test\uploaddet IN 0 ALIAS uploaddet
*!*		ELSE
*!*			USE F:\ediwhse\DATA\uploaddet IN 0 SHARED ALIAS uploaddet
*!*		ENDIF
*!*	ENDIF

SELECT xpt
DELETE ALL
SELECT xptdet
DELETE ALL

STORE 0 TO pt_total_eaches
STORE 0 TO pt_total_cartons
STORE 0 TO nCtnCount

SELECT x856
SET FILTER TO
GOTO TOP

WAIT WINDOW "NOW IN INITAL HEADER BREAKDOWN" TIMEOUT 1
WAIT WINDOW "NOW IN INITAL HEADER BREAKDOWN" NOWAIT
DO WHILE !EOF("x856")
	cSegment = TRIM(x856.segment)
	cCode = TRIM(x856.f1)

	IF TRIM(x856.segment) = "ISA"
		m.isa_num = ALLTRIM(x856.f13)
		STORE m.isa_num TO cISA_Num
		SELECT x856
		SKIP
		LOOP
	ENDIF

	IF TRIM(x856.segment) = "GS"
		SELECT x856
		SKIP
		LOOP
	ENDIF

	IF TRIM(x856.segment) = "ST" AND TRIM(x856.f1) = "940"
		SELECT x856
		SKIP
		LOOP
	ENDIF

	IF TRIM(x856.segment) = "W05"  && new shipment
		nCtnCount = 0
		SELECT xpt
		nDetCnt = 0
		APPEND BLANK
		REPLACE xpt.qty WITH 0 IN xpt
		REPLACE xpt.origqty WITH 0 IN xpt
		replace xpt.goh WITH .t. IN xpt
		STORE 0 TO pt_total_eaches
		STORE 0 TO pt_total_cartons
		ptctr = ptctr +1
		WAIT WINDOW AT 10,10 "Now processing PT# "+ ALLTRIM(x856.f2)+" Number "+CHR(13)+TRANSFORM(ptctr)+" of "+cSegCnt NOWAIT

		REPLACE xpt.ptid       WITH ptctr
		REPLACE xpt.accountid  WITH m.acct_num
		REPLACE xpt.ptdate     WITH DATE()
		cShip_ref = ALLTRIM(x856.f2)
		m.ship_ref = ALLTRIM(x856.f2)
		m.cnee_ref = ALLTRIM(x856.f3)
		REPLACE xpt.ship_ref   WITH m.ship_ref  && Pick Ticket
		REPLACE xpt.cnee_ref   WITH m.cnee_ref
		m.pt = ALLTRIM(x856.f2)
		IF lTesting
			REPLACE xpt.cacctnum   WITH "999"
		ENDIF
		SELECT x856
		SKIP
		LOOP
	ENDIF

	IF TRIM(x856.segment) = "N1" AND TRIM(x856.f1) = "ST" && Ship-to data
		lD2S = .F.
*!*			SELECT uploaddet
*!*			LOCATE
*!*			IF !EMPTY(M.isa_num)
*!*				INSERT INTO uploaddet (office,cust_name,acct_num,date,isa_num,startpt,runid) ;
*!*					VALUES (cOffice,UPPER(TRIM(x856.f2)),m.acct_num,DATETIME(),m.isa_num,xpt.ship_ref,lnRunID)
*!*			ENDIF

		SELECT xpt
		m.st_name = TRIM(x856.f2)
		REPLACE xpt.NAME WITH UPPER(m.st_name) IN xpt
		REPLACE xpt.consignee WITH UPPER(m.st_name) IN xpt
		IF "MACY"$UPPER(m.st_name)
			lFederated = .T.
		ELSE
			lFederated = .F.
		ENDIF

		IF "PENNEY"$UPPER(m.st_name)
			lJCP = .T.
		ENDIF

		cStoreNum = ALLTRIM(x856.f4)
		IF cStoreNum = "D2S"  && Direct to Store indicator
			lD2S = .T.
			REPLACE xpt.scac WITH "UPSN" IN xpt
			REPLACE xpt.ship_via WITH "UPS GROUND" IN xpt
		ENDIF

		REPLACE xpt.shipins WITH IIF(EMPTY(xpt.shipins),"FILENAME*"+cFilename,xpt.shipins+CHR(13)+"FILENAME*"+cFilename) IN xpt
		REPLACE xpt.shipins WITH xpt.shipins+CHR(13)+"STORENUM*"+cStoreNum IN xpt
		REPLACE xpt.storenum  WITH VAL(cStoreNum) IN xpt
		REPLACE xpt.dcnum WITH cStoreNum IN xpt

		SKIP 1 IN x856
		IF TRIM(x856.segment) = "N3"  && address info
			m.st_addr1 = UPPER(TRIM(x856.f1))
			m.st_addr2 = UPPER(TRIM(x856.f2))
			REPLACE xpt.address  WITH m.st_addr1 IN xpt
			REPLACE xpt.address2 WITH m.st_addr2 IN xpt
		ENDIF
		SKIP 1 IN x856
		IF TRIM(x856.segment) = "N4"  && address info
			IF !EMPTY(TRIM(x856.f3))
				m.zipbar = TRIM(x856.f3)
			ENDIF
			IF !EMPTY(TRIM(x856.f2))
				m.st_csz = UPPER(TRIM(x856.f1))+", "+UPPER(TRIM(x856.f2))+" "+TRIM(x856.f3)
			ELSE
				m.st_csz = UPPER(TRIM(x856.f1))
			ENDIF
			REPLACE xpt.csz WITH m.st_csz IN xpt
		ENDIF
		SELECT x856
		SKIP
		LOOP
	ENDIF

	IF TRIM(x856.segment) = "N1" AND TRIM(x856.f1) = "Z7" && Ship-for data
		IF !lD2S  && If a standard 940
			SELECT xpt
			REPLACE xpt.shipfor WITH UPPER(ALLTRIM(x856.f2)) IN xpt
			REPLACE xpt.sforstore WITH ALLTRIM(x856.f4) IN xpt
			SKIP 1 IN x856
			IF TRIM(x856.segment) = "N3"  && address info
				IF lFederated
					REPLACE xpt.shipins WITH xpt.shipins+CHR(13)+"STORENAME*"+UPPER(TRIM(x856.f1)) IN xpt
				ELSE
					m.sforaddr1 = UPPER(TRIM(x856.f1))
					m.sforaddr2 = UPPER(TRIM(x856.f2))
					REPLACE xpt.sforaddr1 WITH m.sforaddr1 IN xpt
					REPLACE xpt.sforaddr2 WITH m.sforaddr2 IN xpt
				ENDIF
			ENDIF
			IF !lFederated
				SKIP 1 IN x856
				IF TRIM(x856.segment) = "N4"  && address info
					IF !EMPTY(TRIM(x856.f3))
						m.zipbar = TRIM(x856.f3)
					ENDIF
					IF !EMPTY(TRIM(x856.f2))
						m.sforcsz = UPPER(TRIM(x856.f1))+", "+UPPER(TRIM(x856.f2))+" "+TRIM(x856.f3)
					ELSE
						m.sforcsz = UPPER(TRIM(x856.f1))
					ENDIF
					REPLACE xpt.sforcsz WITH m.sforcsz
					SELECT x856
					SKIP
					LOOP
				ENDIF
			ENDIF
		ELSE  && If Direct-to-Store
			SELECT xpt
			m.st_name = TRIM(x856.f2)
			REPLACE xpt.NAME WITH UPPER(m.st_name) IN xpt
			REPLACE xpt.consignee WITH UPPER(m.st_name) IN xpt
			IF "MACY"$UPPER(m.st_name)
				lFederated = .T.
			ELSE
				lFederated = .F.
			ENDIF

			IF "PENNEY"$UPPER(m.st_name)
				lJCP = .T.
			ENDIF

			cStoreNum = ALLTRIM(x856.f4)
			REPLACE xpt.shipins WITH IIF(EMPTY(xpt.shipins),"STORENUM*"+cStoreNum,xpt.shipins+CHR(13)+"STORENUM*"+cStoreNum) IN xpt
			REPLACE xpt.storenum  WITH VAL(cStoreNum) IN xpt
			REPLACE xpt.dcnum WITH cStoreNum IN xpt

			SKIP 1 IN x856
			IF TRIM(x856.segment) = "N3"  && address info
				m.st_addr1 = UPPER(TRIM(x856.f1))
				m.st_addr2 = UPPER(TRIM(x856.f2))
				REPLACE xpt.address  WITH m.st_addr1 IN xpt
				REPLACE xpt.address2 WITH m.st_addr2 IN xpt
			ENDIF
			SKIP 1 IN x856
			IF TRIM(x856.segment) = "N4"  && address info
				IF !EMPTY(TRIM(x856.f3))
					m.zipbar = TRIM(x856.f3)
				ENDIF
				IF !EMPTY(TRIM(x856.f2))
					m.st_csz = UPPER(TRIM(x856.f1))+", "+UPPER(TRIM(x856.f2))+" "+TRIM(x856.f3)
				ELSE
					m.st_csz = UPPER(TRIM(x856.f1))
				ENDIF
				REPLACE xpt.csz WITH m.st_csz IN xpt
			ENDIF
		ENDIF
	ENDIF

	IF TRIM(x856.segment) = "N9" AND TRIM(x856.f1) = "DP"  && customer dept
		m.dept = ALLTRIM(x856.f2)
		REPLACE xpt.dept WITH m.dept
		SELECT x856
		SKIP
		LOOP
	ENDIF

	IF TRIM(x856.segment) = "N9" AND TRIM(x856.f1) = "VN"  && JCP Suplier #
		m.vendor_num = ALLTRIM(x856.f2)
		REPLACE xpt.vendor_num WITH m.vendor_num
		SELECT x856
		SKIP
		LOOP
	ENDIF

	IF TRIM(x856.segment) = "N9" AND TRIM(x856.f1) = "19"
		cShipStr = "TYPE*"+UPPER(ALLTRIM(x856.f2))
		cShipStr = "FOB*"+UPPER(ALLTRIM(x856.f2))
		REPLACE xpt.shipins WITH IIF(EMPTY(xpt.shipins),cShipStr,xpt.shipins+CHR(13)+cShipStr)
		SELECT x856
		SKIP
		LOOP
	ENDIF

	IF TRIM(x856.segment) = "G61" && Ship dept. eMail
		cShipStr = "CONTACT*"+UPPER(ALLTRIM(x856.f2))
		REPLACE xpt.shipins WITH IIF(EMPTY(xpt.shipins),cShipStr,xpt.shipins+CHR(13)+cShipStr)
		IF ALLTRIM(x856.f3) = "EM"
			REPLACE xpt.shipins WITH xpt.shipins+CHR(13)+"EMAIL*"+UPPER(ALLTRIM(x856.f4))
		ENDIF
		SELECT x856
		SKIP
		LOOP
	ENDIF

	IF TRIM(x856.segment) = "G62" AND TRIM(x856.f1) == "37"  && start date
		lcDate = ALLTRIM(x856.f2)
		lxDate = SUBSTR(lcDate,5,2)+"/"+SUBSTR(lcDate,7,2)+"/"+LEFT(lcDate,4)
		ldDate = CTOD(lxDate)
		REPLACE xpt.START WITH ldDate
		SELECT x856
		SKIP
		LOOP
	ENDIF

	IF TRIM(x856.segment) = "G62" AND TRIM(x856.f1) == "38"  && cancel date
		lcDate = ALLTRIM(x856.f2)
		lxDate = SUBSTR(lcDate,5,2)+"/"+SUBSTR(lcDate,7,2)+"/"+LEFT(lcDate,4)
		ldDate = CTOD(lxDate)
		REPLACE xpt.CANCEL WITH ldDate
		SELECT x856
		SKIP
		LOOP
	ENDIF

	IF TRIM(x856.segment) = "NTE" AND INLIST(TRIM(x856.f1),"OTH","DEL")  && delivery instructions
		REPLACE xpt.shipins WITH xpt.shipins+CHR(13)+"INST*"+UPPER(ALLTRIM(x856.f2))
		SELECT x856
		SKIP
		LOOP
	ENDIF

	IF TRIM(x856.segment) = "LX" && start of PT detail, stay here until this PT is complete
		WAIT "NOW IN DETAIL LOOP PROCESSING" WINDOW NOWAIT
		SELECT xptdet
		m.linenum = TRIM(x856.f1)
		CALCULATE MAX(ptdetid) TO m.ptdetid
		m.ptdetid = m.ptdetid + 1
		APPEND BLANK
		REPLACE xptdet.linenum WITH m.linenum
		REPLACE xptdet.printstuff WITH ""
		REPLACE xptdet.ptid      WITH ptctr
		REPLACE xptdet.ptdetid   WITH m.ptdetid
		REPLACE xptdet.accountid WITH xpt.accountid
		SKIP 1 IN x856

		llStoredtheStorenum = .F.

		DO WHILE ALLTRIM(x856.segment) != "SE"
			cSegment = TRIM(x856.segment)
			cCode = TRIM(x856.f1)

			IF TRIM(x856.segment) = "LX"
				m.linenum = TRIM(x856.f1)
			ENDIF

			IF TRIM(x856.segment) = "W01" && Destination Quantity
				IF EMPTY(TRIM(xptdet.printstuff))
					REPLACE xptdet.printstuff WITH "UNITCODE*"+ALLTRIM(x856.f2)
				ELSE
					REPLACE xptdet.printstuff WITH xptdet.printstuff+CHR(13)+"UNITCODE*"+ALLTRIM(x856.f2)
				ENDIF
				m.casepack = ALLTRIM(x856.f1)
				nCasePack = INT(VAL(m.casepack))
				STORE "" TO m.style,m.id,m.color,m.pack,m.upc,m.custsku
				m.custsku = ALLTRIM(x856.f16)
				IF ALLTRIM(x856.f4) = "VA"
					STORE ALLTRIM(x856.f5) TO m.style
				ELSE
					SET STEP ON
					tsubject= "Missing Style/Code in Courtaulds 940"
					tattach = ""
					tcc = allt(tccerr)
					tsendto = allt(tsendtoerr)
					tmessage = "File "+cFilename+" was missing a Style code segment in PT# "+cShip_ref+", at LX segment "+TRANSFORM(m.linenum)+". Rejected."
					DO FORM dartmail2 WITH tsendto,tFrom,tsubject,tcc,tattach,tmessage,"A"

					archivefile  = ("F:\0-picktickets\archive\"+cCustname+"\"+JUSTFNAME(currfile))
					IF !FILE(archivefile)
						COPY FILE [&xfile] TO [&archivefile]
						DELETE FILE  [&xfile]
					ENDIF
					NormalExit = .f.
					THROW
				ENDIF
				DO CASE
					CASE ALLTRIM(x856.f6) = "CL"
						STORE ALLTRIM(x856.f7) TO m.color
					CASE ALLTRIM(x856.f6) = "UP"
						STORE ALLTRIM(x856.f7) TO m.upc
						IF EMPTY(m.upc)
							SET STEP ON 
							tsubject= "Missing UPC Code in Courtaulds 940"
							tattach = ""
							tcc = tccerr
							tsendto = tsendtoerr
							tmessage = "File "+cFilename+" was missing a UPC code segment. Rejected."
							tFrom ="TGF EDI Operations <toll-edi-ops@tollgroup.com>"
							DO FORM dartmail2 WITH tsendto,tFrom,tsubject,tcc,tattach,tmessage,"A"

							archivefile  = ("F:\0-picktickets\archive\"+cCustname+"\"+JUSTFNAME(currfile))
							IF !FILE(archivefile)
								COPY FILE [&xfile] TO [&archivefile]
								DELETE FILE [&xfile]
							ENDIF
							NormalExit = .f.
							THROW
						ENDIF
				ENDCASE
				IF lPick
					STORE "1" TO m.pack  && Added to resolve 'pack' issue per Paul, 11/16/05
				ELSE
					STORE ALLTRIM(TRANSFORM(INT(VAL(x856.f1)))) TO m.pack
				ENDIF
				SCATTER FIELDS ptid,accountid MEMVAR

				IF nDetCnt = 0
					nDetCnt = 1
					IF lTesting
						REPLACE xptdet.ship_ref WITH xpt.ship_ref
					ENDIF
					REPLACE xptdet.casepack   WITH nCasePack
					REPLACE xptdet.totqty WITH nCasePack
					REPLACE xptdet.origqty WITH xptdet.totqty
					replace xpt.qty WITH xpt.qty+xptdet.totqty IN xpt
				
					GATHER MEMVAR FIELDS ptid,accountid,STYLE,COLOR,PACK,ID,sku_count,sku_bkdn,units,upc,custsku
					SELECT xptdet
				ELSE
					IF lPrepack
						LOCATE FOR xptdet.ptid = xpt.ptid AND xptdet.STYLE = PADR(m.style,20) AND xptdet.upc = m.upc
					ELSE
						LOCATE FOR xptdet.ptid = xpt.ptid AND xptdet.STYLE = PADR(m.style,20) ;
							AND xptdet.COLOR = m.color AND xptdet.ID = m.id AND xptdet.upc = m.upc
					ENDIF
					IF FOUND()
						m.oldpack = m.pack
						IF lPrepack
							REPLACE xptdet.totqty  WITH (xptdet.totqty + 1)
						ELSE
							REPLACE xptdet.totqty  WITH (xptdet.totqty + nCasePack)
						ENDIF
						REPLACE xptdet.origqty WITH xptdet.totqty
						replace xpt.qty WITH xpt.qty+xptdet.totqty IN xpt
						SELECT xptdet
					ELSE
						APPEND BLANK
						m.ptdetid = m.ptdetid + 1
						GATHER MEMVAR FIELDS ptid,accountid,STYLE,COLOR,PACK,ID,sku_count,sku_bkdn,units,linenum,upc,custsku
						REPLACE xptdet.totqty  WITH nCasePack
						REPLACE xptdet.origqty WITH xptdet.totqty
						replace xpt.qty WITH xpt.qty+xptdet.totqty IN xpt
						
					ENDIF
					REPLACE xptdet.ptdetid WITH m.ptdetid
					SCATTER FIELDS ptid,ptdetid,accountid MEMVAR
					REPLACE xptdet.casepack   WITH nCasePack
					SELECT xptdet
				ENDIF
				IF !("STORENUM*"$xptdet.printstuff)
					REPLACE xptdet.printstuff WITH "STORENUM*"+cStoreNum
				ENDIF
			ENDIF

			IF TRIM(x856.segment) = "G69" && shipstyle
				cPrtstr = "SHIPSTYLE*"+UPPER(ALLTRIM(x856.f1))
				REPLACE xptdet.printstuff WITH IIF(EMPTY(xptdet.printstuff),cPrtstr,xptdet.printstuff+CHR(13)+cPrtstr)
			ENDIF
			IF TRIM(x856.segment) = "W76" && Ctn Qty
				*REPLACE xpt.qty WITH INT(VAL(x856.f1))
				REPLACE xpt.origqty WITH xpt.qty IN xpt
			ENDIF

			SKIP 1 IN x856
		ENDDO

		SELECT xptdet
	ENDIF

	IF TRIM(x856.segment) = "LX"
		m.linenum = TRIM(x856.f1)
	ENDIF

	SELECT x856
	IF !EOF()
		SKIP 1 IN x856
	ENDIF
ENDDO

DO m:\dev\prg\setuppercase940

IF lBrowfiles
	SELECT xpt
*!*		COPY TO h:\xpt TYPE XL5
	LOCATE
	BROWSE
	SELECT xptdet
*!*		COPY TO h:\xptdet TYPE XL5
	LOCATE
	BROWSE
	IF MESSAGEBOX("Do you wish to continue?",4+16+256,"CONTINUE")=7
		CANCEL
		NormalExit = .T.
		THROW
	ENDIF
ENDIF

WAIT cCustname+" Breakdown Round complete..." WINDOW TIMEOUT 2
IF USED('x856')
USE IN x856
endif
IF USED("PT")
	USE IN pt
ENDIF
IF USED("PTDET")
	USE IN ptdet
ENDIF
WAIT CLEAR

