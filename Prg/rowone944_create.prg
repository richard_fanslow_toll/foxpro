*!* ROWONE944_CREATE.PRG
PARAMETERS nWO_Num,nAcctNum,cOffice
*SET STEP ON
DO m:\dev\prg\_setvars
PUBLIC lXfer944,cWhse,cFilename,cFilename2,tsendto,tcc,tsendtoerr,tccerr,lIsError,lJoeMail,lOSD
PUBLIC leMail,tfrom,cWhseMod,gOffice,gMasteroffice

tfrom = "Toll EDI Operations <toll-edi-ops@tollgroup.com>"

lTesting = .t. && If TRUE, disables certain functions
lTestinput = .F.
lUsePL = .F.
lIsError = .F.
ON ERROR THROW
DO m:\dev\prg\lookups

IF TYPE("nWO_Num") = "L" AND lTesting
	nWO_Num =  7143561
	nAcctNum = 6468
	cOffice = "C"
	lUsePL = .T.
	CLOSE DATABASES ALL
ENDIF

cOffice=IIF(INLIST(cOffice,"1","2","5"),"C",cOffice)
cContainer = ""

IF lTestinput
	cUseFolder = "F:\WHP\WHDATA\"
ELSE
	xReturn = "XXX"
	DO m:\dev\prg\wf_alt WITH cOffice,nAcctNum
	IF xReturn = "XXX"
		WAIT WINDOW "No Bldg. # Provided...closing" TIMEOUT 3
		DO ediupdate WITH .T.,"NO BLDG #"
		RETURN
	ENDIF
	cUseFolder = UPPER(xReturn)
ENDIF

IF lTesting
	cIntUsage = "T"
ELSE
	cIntUsage = "P"
ENDIF

SELECT 0
USE F:\3pl\DATA\mailmaster ALIAS mm SHARED
LOCATE FOR mm.accountid = 6468 ;
	AND mm.office = cOffice ;
	AND mm.edi_type = "944"
IF !FOUND()
	ASSERT .F. MESSAGE "At missing Office/Acct"
	WAIT WINDOW "Office/Loc/Acct not found!" TIMEOUT 2
	DO ediupdate WITH .T.,"ACCT/LOC !FOUND"
	NormalExit = .F.
	THROW
ENDIF
STORE TRIM(mm.acctname) TO cCustName
STORE TRIM(mm.basepath) TO lcOutPath
STORE TRIM(mm.holdpath) TO lcHoldPath
STORE TRIM(mm.archpath) TO lcArchivePath
lUseAlt = mm.USE_ALT
tsendto = IIF(lUseAlt,mm.sendtoalt,mm.sendto)
tcc = IIF(lUseAlt,mm.ccalt,mm.cc)
LOCATE
LOCATE FOR mm.edi_type = "MISC" AND mm.taskname = "GENERAL"
lUseAlt = mm.USE_ALT
tsendtoerr = IIF(lUseAlt,mm.sendtoalt,mm.sendto)
tccerr = IIF(lUseAlt,mm.ccalt,mm.cc)
tsendtotest = IIF(lUseAlt,mm.sendtoalt,mm.sendto)
tcctest = IIF(lUseAlt,mm.ccalt,mm.cc)
USE IN mm

IF lTesting
	tsendto = tsendtotest
	tcc = tcctest
ENDIF

DIMENSION thisarray(1)
cSuffix = ""
DO CASE
	CASE INLIST(cOffice,"I","N")
		cWhse = "CARTERET"
	CASE cOffice = "M"
		cWhse = "MIAMI"
	OTHERWISE
		cWhse = "SAN PEDRO"
ENDCASE

cMod = IIF(cOffice = "C","5",cOffice)
gOffice = cMod
gMasteroffie = cOffice
cWhseMod = LOWER("wh"+cMod)
gsystemmodule = UPPER(cWhseMod)


IF TYPE("nWO_Num") <> "N"  OR EMPTY(nWO_Num) && If WO is not numeric or equals Zero
	WAIT WINDOW "No WO# Provided...closing" TIMEOUT 3
	DO ediupdate WITH .T.,"NO WO_NUM"
	RETURN
ENDIF

lOSD = .F.
OSDCheck()

lOverrideXcheck = .T. && If true, doesn't check ASN carton count against barcode scans.
lHoldFile = .F. && If true, keeps output file in 944HOLD folder, otherwise into 944OUT
leMail = .T.  && If .t., sends mail to WAREHOUSES

cUPC = ""
nSTSets = 0
XFERUPDATE = .T.
nFilenum = 0
lBackup = .F.
lOverage = .F.
lJoeMail = .T.
lCopyCA = .T.
cErrMsg = ""
lIsError = .T.
lXfer944 = .F.
cTrk_WO = ""
nCTNTotal = 0

cWO_Num = ALLTRIM(STR(nWO_Num))

IF !lTesting
	IF !USED("edi_xfer")
		USE F:\edirouting\DATA\edi_xfer IN 0 ALIAS edi_xfer ORDER TAG inven_wo
		SELECT edi_xfer
		SET FILTER TO edi_type = "944" AND accountid = nAcctNum
		IF SEEK(nWO_Num)
			WAIT WINDOW "Work Order "+cWO_Num+" already created and logged in EDI_XFER" TIMEOUT 2
			IF MESSAGEBOX("944 for Work Order "+cWO_Num+" already created...Overwrite Date?",4+32+256,"WO# FOUND",8000) = 7 &&No
				USE IN edi_xfer
				DO ediupdate WITH .T.,"DUPLICATE WO#"
				RETURN
			ELSE
				XFERUPDATE = .F.
			ENDIF
		ENDIF
	ENDIF
ENDIF


CREATE CURSOR temp1 (wo_num N(6), ;
	po_num c(5), ;
	STYLE c(20), ;
	expected i, ;
	actual i)

*!* SET CUSTOMER CONSTANTS
cCustName = "ROWONE"  && Customer Identifier (for folders, etc.)
cMailName = "Row One"  && Proper name for eMail, etc.
cZLAddress = "661 HIGHLAND AVE - SUITE 104"
cZLCity = "NEEDHAM"
cZLState = "MA"
cZLZip = "02494"
cCustPrefix = "RO" && Customer Prefix (for output file names)
cX12 = "004010"  && X12 Standards Set used
crecid = "ROWONE"  && Recipient EDI address

dt1 = TTOC(DATETIME(),1)
dt2 = DATETIME()
dtmail = TTOC(DATETIME())

cFilestem = cCustPrefix+dt1+".944"
cFilename = (lcHoldPath+cFilestem) && Holds file here until complete and ready to send
cFilename2 = (lcOutPath+cFilestem)
cArchiveFile = (lcArchivePath+cFilestem)

STORE "" TO c_CntrlNum,c_GrpCntrlNum
nFilenum = FCREATE(cFilename)

*!* SET OTHER CONSTANTS
nCtnNumber = 1
cString = ""
csendqual = "ZZ"
csendid = "TOLL"
crecqual = "ZZ"
cfd = "*"  && Field/element delimiter
csegd = "" && Line/segment delimiter
nSegCtr = 0
cterminator = ">" && Used at end of ISA segment
cdate = DTOS(DATE())
ctruncdate = RIGHT(cdate,6)
ctime = LEFT(TIME(DATE()),2)+SUBSTR(TIME(DATE()),4,2)
cfiledate = cdate+ctime
cOrig = "J"
cdate = DTOS(DATE())
ctruncdate = RIGHT(cdate,6)
ctime = LEFT(TIME(DATE()),2)+SUBSTR(TIME(DATE()),4,2)
cQtyRec = "1"
cCTypeUsed = ""
nQtyExpected = 0
nCTNDamaged = 0
nCTNShortage = 0
nCTNExpected = 0  && From INWOLOG count
nCTNActual = 0  && From TLDATA\Barcodes count
cMissingCtn = "Missing Cartons: "+CHR(13)
cPO = ""
cStyle = ""
nPOExpected = 0
nPOActual = 0

** PAD ID Codes
csendidlong = PADR(csendid,15," ")
crecidlong = PADR(crecid,15," ")

IF USED('account')
	USE IN account
ENDIF

xsqlexec("select * from account where inactive=0","account",,"qq")
INDEX ON accountid TAG accountid
SET ORDER TO

*!* Serial number incrementing table
IF USED("serfile")
	USE IN serfile
ENDIF
USE ("F:\3pl\data\serial\"+cCustName+"944_serial") IN 0 ALIAS serfile

*!* Trigger Data
IF !USED('edi_trigger')
	cEDIFolder = "F:\3PL\DATA\"
	USE (cEDIFolder+"edi_trigger") IN 0 ALIAS edi_trigger ORDER TAG wo_num
ENDIF
*!* Search table (parent)
IF USED('inwolog')
	USE IN inwolog
ENDIF
csq1 = [select * from inwolog where accountid = 6468]
xsqlexec(csq1,,,"wh")

*!* Search table (xref for PO's)
IF USED('INDET')
	USE IN indet
ENDIF

SET STEP ON 
csq1 = [select * from pl where accountid = 6468]
xsqlexec(csq1,,,"wh")

nRCnt = 0
nISACount = 0
nSTCount = 1
nSECount = 1

SELECT inwolog
WAIT WINDOW "" TIMEOUT 2
LOCATE FOR inwolog.wo_num = nWO_Num
IF !FOUND()
	WAIT WINDOW "WO# "+cWO_Num+" not found in INWOLOG...Terminating" TIMEOUT 3
	IF !lTesting
		lCopyCA = .T.
		DO ediupdate WITH .T.,"MISS INWOLOG WO"
		RETURN
	ENDIF
ENDIF

nTRK_WO = INT(VAL(inwolog.comments))  && Extracts Trucking WO# from INWOLOG
cTrk_WO = ALLTRIM(STR(nTRK_WO))
cContainer = IIF(!EMPTY(inwolog.CONTAINER),TRIM(inwolog.CONTAINER),TRIM(inwolog.REFERENCE)) && Extracts Container ID from INWOLOG

IF EMPTY(TRIM(cContainer))
	WAIT WINDOW "There is no container attached to "+cWO_Num TIMEOUT 3
	lCopyChris = .T.
	DO ediupdate WITH .T.,"NO CONTAINER"
	RETURN
ENDIF

SELECT indet
IF !lTesting
	IF !SEEK(nWO_Num,"indet","wo_num")
		waitstr = "WO# "+cWO_Num+" Not Found in Inbd Detail Table..."+CHR(13)+;
			"WO has not been confirmed...Terminating"
		WAIT WINDOW waitstr TIMEOUT 3
		DO ediupdate WITH .T.,"WO NOT CONF. IN INDET"
		RETURN
	ENDIF
ENDIF
SELECT inwolog
cAcctname = TRIM(inwolog.acctname)
cContainer = TRIM(inwolog.CONTAINER)
cCtrType = IIF(SUBSTR(cContainer,4,1)="U","SQ","AW")
cAcct_ref = TRIM(inwolog.acct_ref)
alenhead = ALINES(apthead,inwolog.comments,.T.,CHR(13))

IF nISACount = 0
	DO num_incr
	STORE "ISA"+cfd+"00"+cfd+"          "+cfd+"00"+cfd+"          "+cfd+csendqual+cfd+csendidlong+cfd+;
		crecqual+cfd+crecidlong+cfd+ctruncdate+cfd+ctime+cfd+"U"+cfd+"00401"+cfd+;
		PADL(c_CntrlNum,9,"0")+cfd+"0"+cfd+cIntUsage+cfd+cterminator+csegd TO cString
	DO cstringbreak

	STORE "GS"+cfd+"RE"+cfd+csendid+cfd+crecid+cfd+cdate+cfd+ctime+cfd+c_CntrlNum+;
		cfd+"X"+cfd+cX12+csegd TO cString
	DO cstringbreak

	nSegCtr = 0
	nISACount = 1
ENDIF

IF nSECount = 1
	STORE "ST"+cfd+"944"+cfd+PADL(c_GrpCntrlNum,9,"0")+csegd TO cString
	DO cstringbreak
	nSegCtr = nSegCtr + 1

	IF lTesting AND EMPTY(cAcct_ref)
		cAcct_ref = "9999999"
	ENDIF
	STORE "W17"+cfd+cOrig+cfd+cdate+cfd+IIF(EMPTY(cAcct_ref),cWO_Num,cAcct_ref)+csegd TO cString
	DO cstringbreak
	nSegCtr = nSegCtr + 1

	cCoName = ALLTRIM(segmentget(@apthead,"CONAME",alenhead))
	cCoName = IIF(EMPTY(cCoName),UPPER(cMailName),cCoName)
	STORE "N1"+cfd+"ZL"+cfd+UPPER(cCoName)+csegd TO cString
	DO cstringbreak
	nSegCtr = nSegCtr + 1

	STORE "N3"+cfd+cZLAddress+csegd TO cString
	DO cstringbreak
	nSegCtr = nSegCtr + 1

	STORE "N4"+cfd+cZLCity+cfd+cZLState+cfd+cZLZip+cfd+"USA"+cfd+"93"+cfd+cWhse+csegd TO cString
	DO cstringbreak
	nSegCtr = nSegCtr + 1

	STORE "PER"+cfd+"CN"+cfd+"PAUL GAIDIS"+cfd+"TE"+cfd+"732-750-9000"+;
		cfd+"FX"+cfd+"732-750-4338"+cfd+"EM"+cfd+"Paul.Gaidis@tollgroup.COM"+csegd TO cString
	DO cstringbreak
	nSegCtr = nSegCtr + 1

	STORE "N9"+cfd+cCtrType+cfd+cContainer+csegd TO cString  && Used for either AWB or container type
	DO cstringbreak
	nSegCtr = nSegCtr + 1

	STORE "N9"+cfd+"ACC"+cfd+IIF(lOSD,"O","N")+csegd TO cString  && Used as a marker for OS&D files.
	DO cstringbreak
	nSegCtr = nSegCtr + 1

	cDiv = TRIM(segmentget(@apthead,"DIVCODE",alenhead))
	IF !EMPTY(cDiv)
		STORE "N9"+cfd+"6P"+cfd+cDiv+csegd TO cString  && Added Division info per Joe S.
		DO cstringbreak
		nSegCtr = nSegCtr + 1
	ENDIF

	STORE "G62"+cfd+"09"+cfd+DTOS(DATE())+csegd TO cString
	DO cstringbreak
	nSegCtr = nSegCtr + 1
	nSECount = 0
ENDIF

SELECT indet
SCAN FOR indet.inwologid = inwolog.inwologid AND !units
	nPO1 = 0
	nOPO = OCCURS("ORIGPOINFO",indet.ECHO) && Added to split out detail by PO#, per RJ @ ModShoe 06.18.2013
	lOPO = IIF(nOPO > 1, .T.,.F.)
	nOPO = IIF(nOPO = 0,1,nOPO)

	cStyle = ALLTRIM(indet.STYLE)
	cUnits = indet.PACK
	alendet = ALINES(apt,indet.ECHO,.T.,CHR(13))

	FOR posused = 1 TO nOPO
		IF lOPO
			SET STEP ON
			IF nPO1 = 0
				nPO1 = ATLINE("ORIGPOINFO",indet.ECHO)
			ELSE
				nPO1 = nPO1+1
			ENDIF
			cPOInfo = STRTRAN(MLINE(indet.ECHO,nPO1),"ORIGPOINFO*","")
			nPipe1 = AT("|",cPOInfo,1)
			nPipe2 = AT("|",cPOInfo,2)-1
			nPipe2a = AT("|",cPOInfo,2)+1
			nLenMid = nPipe2 - nPipe1
			cPO = LEFT(cPOInfo,nPipe1-1)
			cLI = SUBSTR(cPOInfo,nPipe1+1,nLenMid)
			cCTNs = ALLT(SUBSTR(cPOInfo,nPipe2a))
			nCTNExpected = nCTNExpected + INT(VAL(cCTNs))
		ELSE
			cPO = TRIM(segmentget(@apt,"PO",alendet))
			cLI = TRIM(segmentget(@apt,"LI",alendet))
			cCTNs = ALLTRIM(STR(indet.totqty))
			nCTNExpected = nCTNExpected + indet.totqty
		ENDIF

		nCTNActual = nCTNExpected
		nCTNShortage = 0

		STORE "W07"+cfd+cCTNs+cfd+"CT"+cfd+cUPC+cfd+"VN"+cfd+;
			cStyle+csegd TO cString
		DO cstringbreak
		nSegCtr = nSegCtr + 1

		IF lTesting AND EMPTY(cPO)
			STORE "N9"+cfd+"PO"+cfd+"PONUM"+csegd TO cString
		ELSE
			IF EMPTY(cPO)
				ASSERT .F. MESSAGE "At EMPTY PO...debug"
				DO ediupdate WITH .F.,"NO PO DATA IN ECHO"  && May need to be changed to error in future

				RETURN
			ENDIF
			STORE "N9"+cfd+"PO"+cfd+cPO+csegd TO cString
		ENDIF
		DO cstringbreak
		nSegCtr = nSegCtr + 1

		STORE "N9"+cfd+"LI"+cfd+cLI+csegd TO cString
		DO cstringbreak
		nSegCtr = nSegCtr + 1

		STORE "W20"+cfd+ALLTRIM(indet.PACK)+cfd+cfd+cfd+ALLTRIM(STR(ctnwt))+cfd+"FR"+cfd+"L"+cfd+cfd+;
			ALLTRIM(STR(ctncube,6,2))+cfd+"CF"+csegd TO cString
		DO cstringbreak
		nSegCtr = nSegCtr + 1
	ENDFOR

ENDSCAN
WAIT WINDOW "END OF SCAN/CHECK PHASE ROUND..." NOWAIT

nSTCount = 0
DO close944
lXfer944 = .T.
=FCLOSE(nFilenum)

IF USED('serfile')
	USE IN serfile
ENDIF

IF !lTesting
	IF !USED("ftpedilog")
		SELECT 0
		USE F:\edirouting\ftpedilog ALIAS ftpedilog
		INSERT INTO ftpedilog (ftpdate,filename,acct_name,TYPE) VALUES (dt2,cFilename,"cCustname","944")
		USE IN ftpedilog
	ENDIF
ENDIF


SELECT edi_trigger

WAIT "944 Creation process complete:"+CHR(13)+cFilename WINDOW AT 45,60 TIMEOUT 3
*	DO F:\polorl\prg\total944.prg WITH lXfer944,cFilename  && Checks expected vs. actual totals
cFin = "944 FILE "+cFilename2+" CREATED WITHOUT ERRORS"
IF !lHoldFile AND !lTesting
	COPY FILE &cFilename TO &cFilename2
	ERASE &cFilename
ENDIF

IF !lTesting
	SELECT edi_trigger
	LOCATE
	REPLACE edi_trigger.processed WITH .T.,edi_trigger.created WITH .T.,edi_trigger.when_proc WITH DATETIME(),;
		edi_trigger.fin_status WITH "944 CREATED",edi_trigger.errorflag WITH .F.,file944crt WITH cFilename ;
		FOR edi_trigger.wo_num = nWO_Num AND edi_trigger.accountid = nAcctNum AND edi_trigger.edi_type = "944"
	LOCATE
ENDIF
lIsError = .F.

IF leMail
	joemail()
ENDIF
RELEASE ALL LIKE T*
closedata()

************************************************************************
*!* END OF MAIN PROGRAM
************************************************************************

****************************
PROCEDURE stclose
****************************
	nCTNShortage = (nCTNExpected - nCTNActual - nCTNDamaged)
	nCTNTotal = nCTNTotal + nCTNExpected
	IF nCTNShortage < 0
		WAIT CLEAR
		lOverage = .T.
		tsendto = tsendtoerr
		tcc = tccerr
		tsubject = cMailName+" 944 EDI File OVERAGE, WO "+cWO_Num
		tattach = " "
		tmessage = "PO "+cPONumOld+" OVERAGE (More cartons found than expected for a PO)! Check EDI File immediately!"
		tmessage = tmessage + "Expected: "+TRANSFORM(nCTNExpected)+", Actual: "+TRANSFORM(nCTNActual)
		tfrom = "Toll EDI Operations <toll-edi-ops@tollgroup.com>"
		DO FORM m:\dev\frm\dartmail2 WITH tsendto,tfrom,tsubject,tcc,tattach,tmessage,"A"
	ENDIF

*!* For 944 process only!
	IF nSECount = 0
		STORE "W14"+cfd+ALLTRIM(STR(nCTNExpected))+;
			cfd+ALLTRIM(STR(nCTNExpected-nCTNShortage))+;
			cfd+ALLTRIM(STR(nCTNShortage))+csegd TO cString
		DO cstringbreak
		nSegCtr = nSegCtr + 1
*!* End 944 addition

		STORE  "SE"+cfd+ALLTRIM(STR(nSegCtr+1))+cfd+PADL(c_GrpCntrlNum,9,"0")+csegd TO cString
		FPUTS(nFilenum,cString)
		nSTCount = 1
		nSECount = 1
	ENDIF
	nSegCtr = 0
	nSTSets = nSTSets + 1

	SELECT serfile
	REPLACE serfile.grpseqnum WITH serfile.grpseqnum + 1
	c_GrpCntrlNum = ALLTRIM(STR(serfile.grpseqnum))
*? c_GrpCntrlNum
	SELECT indet1
	LXCount = 1
	STORE 0 TO nCTNExpected,nCTNActual,nCTNDamaged,nCTNShortage
	RETURN

****************************
PROCEDURE close944
****************************
** Footer Creation

	IF nSTCount = 0
*!* For 944 process only!
		nCTNShortage = (nCTNExpected - nCTNActual - nCTNDamaged)
		STORE "W14"+cfd+ALLTRIM(STR(nCTNExpected))+;
			cfd+ALLTRIM(STR(nCTNExpected-nCTNShortage))+;
			cfd+ALLTRIM(STR(nCTNShortage))+csegd TO cString
		DO cstringbreak
		nSegCtr = nSegCtr + 1
*!* End 944 addition

		STORE  "SE"+cfd+ALLTRIM(STR(nSegCtr+1))+cfd+PADL(c_GrpCntrlNum,9,"0")+csegd TO cString
		FPUTS(nFilenum,cString)
		nSegCtr = 0
		nSTSets = nSTSets + 1
	ENDIF

	STORE  "GE"+cfd+ALLTRIM(STR(nSTSets))+cfd+c_CntrlNum+csegd TO cString
	FPUTS(nFilenum,cString)

	STORE  "IEA"+cfd+"1"+cfd+PADL(c_CntrlNum,9,"0")+csegd TO cString
	FPUTS(nFilenum,cString)

	RETURN

****************************
PROCEDURE cstringbreak
****************************
	FPUTS(nFilenum,cString)
	RETURN

****************************
PROCEDURE num_incr
****************************
	SELECT serfile
	REPLACE serfile.seqnum WITH serfile.seqnum + 1 IN serfile
	REPLACE serfile.grpseqnum WITH serfile.grpseqnum + 1 IN serfile
	c_CntrlNum = ALLTRIM(STR(serfile.seqnum))
	c_GrpCntrlNum = ALLTRIM(STR(serfile.grpseqnum))
	RETURN

****************************
PROCEDURE ediupdate
****************************
	PARAMETERS lIsError,cFin
	IF cFin = "NO PO DATA IN ECHO"
		cFin = "NO 944 POSSIBLE: 'Echo' field empty"
		archivefile = ""
	ENDIF
*SET STEP ON
	IF !lTesting
		SELECT edi_trigger
		LOCATE
		REPLACE processed WITH .T.,created WITH .F.,fin_status WITH cFin,errorflag WITH .F. ;
			FOR edi_trigger.wo_num = nWO_Num AND edi_trigger.accountid = nAcctNum AND edi_trigger.edi_type = "944"
		LOCATE
		IF !lIsError
			REPLACE when_proc WITH DATETIME(),file944crt WITH archivefile ;
				FOR edi_trigger.wo_num = nWO_Num ;
				AND edi_trigger.accountid = nAcctNum AND edi_trigger.edi_type = "944"
		ELSE
			REPLACE processed WITH .T.,created WITH .F.,fin_status WITH cFin,errorflag WITH .T. ;
				edi_trigger.file944crt WITH "" ;
				FOR edi_trigger.wo_num = nWO_Num AND edi_trigger.accountid = nAcctNum ;
				AND edi_trigger.edi_type = "944"
		ENDIF
		IF lJoeMail AND leMail
			joemail()
		ENDIF
	ENDIF
	closeout()
ENDPROC

****************************
PROCEDURE closeout
****************************
	IF !EMPTY(nFilenum)
		=FCLOSE(nFilenum)
		ERASE &cFilename
	ENDIF
	IF USED('barcodes')
		SELECT BARCODES
		SET FILTER TO
		USE IN BARCODES
	ENDIF
	closedata()
ENDPROC

****************************
PROCEDURE closedata
****************************
	IF USED('barcodes')
		SELECT BARCODES
		SET FILTER TO
		USE IN BARCODES
	ENDIF
	IF USED('edi_xfer')
		USE IN edi_xfer
	ENDIF
	IF USED('csrBCdata')
		USE IN csrBCdata
	ENDIF
	IF USED('detail')
		USE IN DETAIL
	ENDIF
	IF USED('indet')
		USE IN indet
	ENDIF
	IF USED('inwolog')
		USE IN inwolog
	ENDIF
	RELEASE ALL LIKE cContainer
	ON ERROR
	WAIT CLEAR
	_SCREEN.CAPTION = "INBOUND POLLER - EDI"
ENDPROC

****************************
PROCEDURE joemail
****************************
	IF lIsError OR cFin = "NO PO DATA IN ECHO"
		tsubject = cMailName+" EDI ERROR (944), Inv. WO"+cWO_Num
		IF cFin = "NO PO DATA IN ECHO"
			SELECT 0
			USE F:\3pl\DATA\mailmaster ALIAS mm
			LOCATE FOR mm.edi_type = "MISC" AND mm.taskname = "MODSHOEPODATA"
			lUseAlt= IIF(mm.USE_ALT,.T.,.F.)
			tsendto = IIF(lUseAlt,sendtoalt,sendto)
			tcc = IIF(lUseAlt,ccalt,cc)
			USE IN mm
		ENDIF
	ELSE
		tsubject = cMailName+" EDI FILE (944) Created, Inv. WO"+cWO_Num
	ENDIF

	tattach = " "
	IF (cTrk_WO = "0" OR EMPTY(cTrk_WO))
		cThisTrk_WO = "MISSING"
	ELSE
		cThisTrk_WO = cTrk_WO
	ENDIF
	tmessage = "Inbd. WO #: "+cWO_Num+CHR(13)
	cAcctname = IIF(SEEK(nAcctNum,'account','accountid'),ALLTRIM(account.acctname),"")
	tmessage = tmessage+CHR(13)+"Acct ID: "+TRANSFORM(nAcctNum)+", Account: "+cAcctname

	IF EMPTY(cErrMsg)
		tmessage = tmessage+CHR(13)+cFin
	ELSE
		tmessage = tmessage+CHR(13)+cErrMsg
	ENDIF
	IF cFin = "NO PO DATA IN ECHO"
		tmessage = tmessage + CHR(13)+CHR(13)+"Container/Airbill: "+cContainer
	ENDIF

	DO FORM m:\dev\frm\dartmail2 WITH tsendto,tfrom,tsubject,tcc,tattach,tmessage,"A"
	IF !lIsError
*		transitionmail()
	ENDIF
ENDPROC


****************************
PROCEDURE transitionmail
****************************
	RETURN

	tsendto = tsendtotest
	tcc = tcctest
	tfrom = "Toll EDI Operations <toll-edi-ops@tollgroup.com>"
	tsubject = cCustName+" 944 output ready"
	tmessage = "A 944 for WO# "+cWO_Num+"was created as"
	tmessage = tmessage + "f:\ftpusers\"+cCustName+"\944OUT\transition\prl"+dt1+".944."
	tmessage = tmessage + "Check file and drop into 944OUT folder if correctly created."

	DO FORM m:\dev\frm\dartmail2 WITH tsendto,tfrom,tsubject,tcc,tattach,tmessage,"A"
ENDPROC

****************************
PROCEDURE segmentget
****************************

	PARAMETER thisarray,lcKey,nLength

	FOR i = 1 TO nLength
		IF i > nLength
			EXIT
		ENDIF
		lnEnd= AT("*",thisarray[i])
		IF lnEnd > 0
			lcThisKey =TRIM(SUBSTR(thisarray[i],1,lnEnd-1))
			IF OCCURS(lcKey,lcThisKey)>0
				RETURN SUBSTR(thisarray[i],lnEnd+1)
				i = 1
			ENDIF
		ENDIF
	ENDFOR

	RETURN ""
ENDPROC

****************************
PROCEDURE OSDCheck
****************************

	SELECT 0
	csq1 = [select * from pl where inlist(accountid,]+gtkohighlineaccounts+[)]
	xsqlexec(csq1,,,"wh")

	USE (cUseFolder+"indet") IN 0

	SELECT * FROM pl WHERE wo_num=nWO_Num INTO CURSOR xpl
	INDEX ON TRANS(units)+STYLE+COLOR+ID+PACK TAG match
	USE IN pl

	SELECT * FROM indet WHERE wo_num=nWO_Num INTO CURSOR xindet
	INDEX ON TRANS(units)+STYLE+COLOR+ID+PACK TAG match
	USE IN indet

	CREATE CURSOR xrpt (date_rcvd d, units l, STYLE c(20), COLOR c(10), ID c(10), PACK c(10), plqty N(6), indetqty N(6), diffqty N(7), DIMEN c(10), ctncube N(6,2), ctnwt N(4,0))
	INDEX ON TRANS(units)+STYLE+COLOR+ID+PACK TAG match

	SELECT xpl
	SCAN
		SCATTER MEMVAR FIELDS units, STYLE, COLOR, ID, PACK, ctncube, ctnwt

		DO CASE
			CASE !SEEK(TRANS(xpl.units)+xpl.STYLE+xpl.COLOR+xpl.ID+xpl.PACK,"xindet","match")
				m.indetqty=0
			CASE xindet.totqty#xpl.totqty
				SELECT xindet
				m.indetqty=xindet.totqty
			OTHERWISE
				LOOP
		ENDCASE

		m.dimen=TRANS(LENGTH)+"x"+TRANS(WIDTH)+"x"+TRANS(depth)
		m.plqty=xpl.totqty
		m.diffqty=m.indetqty-m.plqty

		IF SEEK(m.style+m.color+m.id+m.pack,"xrpt","match")
			REPLACE plqty WITH xrpt.xplqty+m.plqty, indetqty WITH xrpt.indetqty+m.indetqty IN xrpt
		ELSE
			INSERT INTO xrpt FROM MEMVAR
		ENDIF
	ENDSCAN

	SELECT xindet
	SCAN
		IF !SEEK(TRANS(xindet.units)+xindet.STYLE+xindet.COLOR+xindet.ID+xindet.PACK,"xpl","match")
			m.plqty=0
			m.indetqty=xindet.totqty

			SCATTER MEMVAR FIELDS units, STYLE, COLOR, ID, PACK, ctncube, ctnwt, date_rcvd
			m.dimen=TRANS(LENGTH)+"x"+TRANS(WIDTH)+"x"+TRANS(depth)
			m.diffqty=m.indetqty-m.plqty

			IF SEEK(m.style+m.color+m.id+m.pack,"xrpt","match")
				REPLACE xrpt.plqty WITH xrpt.plqty+m.plqty, indetqty WITH xrpt.indetqty+m.indetqty IN xrpt
			ELSE
				INSERT INTO xrpt FROM MEMVAR
			ENDIF
		ENDIF
	ENDSCAN

	lOSD = IIF(RECCOUNT("xrpt")#0,.T.,.F.)
	USE IN xpl
	USE IN xindet
	USE IN xrpt
ENDPROC
