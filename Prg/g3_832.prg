utilsetup("G3_832")
Close Data All

Public lTesting
Public Array a856(1)

lTesting = .F.
Do m:\dev\prg\_setvars With lTesting

*Use F:\wh\upcmast Alias upcmast In 0 Shared

useca("upcmast","wh",,,"select * from upcmast where accountid in (6614,6621,6622,6623,6639,6657)",,"upcmastsql")

Set Step On

NumAdded = 0
NumUpdated = 0

*!*  Select * ;
*!*    FROM upcmast ;
*!*    WHERE .F. ;
*!*    INTO Cursor tempmast Readwrite
*!*  Alter Table tempmast Drop Column upcmastid

goffice="6"
gmasteroffice="C"

lcCurrDir = ""

* removed dy 3/28/17
*!*	if usesqlack()
*!*		useca("ackdata","wh")
*!*	else
*!*		If !Used("ackdata")
*!*		  Use F:\3pl\Data\ackdata In 0
*!*		Endif
*!*	endif

If !lTesting
  lcPath = 'F:\FTPUSERS\G-III\Stylemaster\'
  lcArchivePath = 'F:\FTPUSERS\G-III\Stylemaster\archive\'
  tsendto="todd.margolin@tollgroup.com"
  tcc="PGAIDIS@FMIINT.COM"
Else
  lcPath = 'F:\FTPUSERS\samsung\Stylemaster\'
  lcArchivePath = 'F:\FTPUSERS\G-III\Stylemaster\archive\'
  tsendto="tmarg@fmiint.com,PGAIDIS@FMIINT.COM"
  tcc=""
Endif

Cd &lcPath
len1 = Adir(ary1,"*.*")
If len1 = 0
  Wait Window "No files found...exiting" Timeout 2
  Close Data All
  schedupdate()
  _Screen.Caption=gscreencaption
  On Error
  Return
Endif

lcAddedStr  ="Styles Added:"+Chr(13)
lcUpdateStr ="Styles Updated:"+Chr(13)
ii=1

For Thisfile = 1 To len1
  cFilename = Alltrim(ary1[thisfile,1])
  xfile = lcPath+cFilename
  Do m:\dev\prg\createx856a
  Do m:\dev\prg\loadedifile With xfile,"*","TILDE"

  m.addby = "FILE"+Alltrim(Transform(Thisfile))
  m.adddt = Datetime()
  m.updatedt = Datetime()
  m.addproc = "G3832"
*  m.accountid = 6649
  m.pnp = .T.
  m.uom = "EA"
  m.info = ""
  lcItemType =""

  Select x856
  Goto Top
  Scan
    Do Case
    Case Trim(x856.segment) = "ISA"
      cisa_num = Alltrim(x856.f13)
    Case Trim(x856.segment) = "GS"
      cgs_num = Alltrim(x856.f6)
      lcCurrentGroupNum = Alltrim(x856.f6)
*!*      Case Trim(x856.segment) = "ST" And Trim(x856.f1) = "832"
*!*        Insert Into ackdata (groupnum,isanum,transnum,edicode,accountid,loaddt,loadtime,filename) Values (lcCurrentGroupNum,cisa_num,Alltrim(x856.f2),"SC",6614,Date(),Datetime(),xfile)
    Case x856.segment = "BCT" And Substr((x856.f3),1,2) = "??"
      m.accountid = 6614
      lcItemType = Upper(Allt(x856.f9))
      m.itemtype= lcItemType
    Case x856.segment = "BCT" And Substr((x856.f3),1,2) = "??"
      m.accountid = 6621
      lcItemType = Upper(Allt(x856.f9))
      m.itemtype= lcItemType
    Case x856.segment = "BCT" And Substr((x856.f3),1,2) = "??"
      m.accountid = 6622
      lcItemType = Upper(Allt(x856.f9))
      m.itemtype= lcItemType
    Case x856.segment = "BCT" And Substr((x856.f3),1,2) = "??"
      m.accountid = 6634
      lcItemType = Upper(Allt(x856.f9))
      m.itemtype= lcItemType
    Case x856.segment = "BCT" And Substr((x856.f3),1,2) = "??"
      m.accountid = 6639
      lcItemType = Upper(Allt(x856.f9))
      m.itemtype= lcItemType
    Case x856.segment = "BCT" And Substr((x856.f3),1,2) = "??"
      m.accountid = 6657
      lcItemType = Upper(Allt(x856.f9))
      m.itemtype= lcItemType
*      m.info = m.info+Chr(13)+"TYPE*"+lcItemType
    Case x856.segment = "LIN"
      Insert Into tempmast From Memvar
      Scatter Memvar Memo Blank
      m.info = ""
      m.itemtype= lcItemType
      m.info = m.info+Chr(13)+"TYPE*"+lcItemType
      m.upc = Allt(x856.f5)
      m.style=  Allt(x856.f3)
*      m.info = m.info+Chr(13)+"ORIGSTYLE*"+Allt(x856.f3)
**reset in case segments do not exist for upc - mvw 03/19/14
      Store "" To m.sid,m.descrip
      Store 0 To m.price,m.rprice

      If x856.f4 = "UP"
        m.upc = Allt(x856.f5)
      Endif
      If x856.f2 = "VC"
        m.style = Allt(x856.f3)
      Endif
      If x856.f6 = "CM"
        m.info = m.info+Chr(13)+"NRFCOLOR*"+Allt(x856.f7)
      Endif
      If x856.f8 = "SM"
        m.info = m.info+Chr(13)+"NRFSIZE*"+Allt(x856.f9)
      Endif

*!*        If x856.f22 = "SE"
*!*          m.info = "SEASON*"+Allt(x856.f23)
*!*        Endif

    Case x856.segment = "PID" And x856.f1 = "F" And x856.f2 = "08"
      m.descrip = Allt(x856.f5)
      m.info = m.info+Chr(13)+"STYLEDESC*"+Allt(x856.f5)
    Case x856.segment = "PID" And x856.f1 = "F" And x856.f2 = "73"
      m.color = Allt(x856.f5)
    Case x856.segment = "PID" And x856.f1 = "F" And x856.f2 = "74"
      m.id= x856.f5
    Case x856.segment = "PID" And x856.f1 = "F" And x856.f2 = "TRN"
      m.info = m.info+Chr(13)+"PRODGROUP*"+Allt(x856.f5)


*!*      Case x856.segment = "PID" And x856.f1 = "F" And x856.f2 = "LO"
*!*        m.htscode =Allt(x856.f5)
*!*        m.info = m.info+Chr(13)+"HTSCODE*"+Allt(x856.f5)

*!*      Case x856.segment = "PID" And x856.f1 = "F" And x856.f2 = "SC"
*!*        m.coo=Allt(x856.f5)

*!*      Case x856.segment = "CTP" And x856.f2 = "UCP"
*!*        m.price = Val(x856.f3)

*!*      Case x856.segment = "CTP" And x856.f2 = "MSR"
*!*        m.rprice = Val(x856.f3)

*!*      Case x856.segment = "G55"
*!*        m.weight = Val(Alltrim(Transform(f23)))

*!*      Case x856.segment = "SLN" And x856.f9 = "UP"
*!*        m.info = m.info+Chr(13)+"SUBLINE*"+Allt(x856.f1)
*!*        m.info = m.info+Chr(13)+"SUBCUSTSKU*"
*!*        m.info = m.info+Chr(13)+"SUBSTYLE*"
*!*        m.info = m.info+Chr(13)+"SUBUOM*"+Allt(x856.f5)
*!*        m.info = m.info+Chr(13)+"SUBPACK*"+Allt(x856.f4)
*!*        m.info = m.info+Chr(13)+"SUBUPC*"+Allt(x856.f10)
*!*        m.info = m.info+Chr(13)+"SUBCOLOR*"
*!*        m.info = m.info+Chr(13)+"SUBSIZE*"
*!*        m.info = m.info+Chr(13)


      Insert Into tempmast From Memvar
*      Scatter Memvar Memo Blank


    Endcase
  Endscan
  Select tempmast
  Delete From tempmast Where Empty(Style)
  Locate

  Set Step On
*  BROWSE

*** we have read ion this file into tempmast
***ow check for 6303

  RecCtr = 0
  Select tempmast && has all new records.......
  Scan
    RecCtr = RecCtr +1
    Select upcmast
    Locate For accountid =tempmast.accountid And upc=tempmast.upc

    Wait Window At 10,10 "Checking Record # "+Transform(RecCtr) Nowait

    If !Found()
      NumAdded = NumAdded +1
      Select tempmast
      Scatter Fields Except upcmastid Memvar Memo
*!*        Select upcmast
*!*        Go Bott
      m.adddt = Datetime()
      m.upcmastid=dygenpk('UPCMAST','WHALL')
      ii=ii+1
*!*        Insert Into upcmast  From Memvar
      Select upcmastsql  && add the recored to SQL
      m.adddt    = Datetime()
	  m.upcmastid=sqlgenpk("upcmast","wh")
      m.updatedt = Datetime()

      Select upcmastsql  && add the recored to SQL
      Insert Into upcmastsql From Memvar
      lcAddedStr = lcAddedStr +m.upc+"-"+m.style+"-"+m.color+"-"+m.id+Chr(13)
    Else
      Select upcmastsql
      Locate For upcmastsql.accountid =tempmast.accountid And upcmastsql.upc=tempmast.upc
      If Found("upcmastsql")
        Replace upcmastsql.price    With tempmast.price   In upcmastsql
        Replace upcmastsql.rprice   With tempmast.rprice  In upcmastsql
        Replace upcmastsql.Style    With tempmast.Style   In upcmastsql
        Replace upcmastsql.Color    With tempmast.Color   In upcmastsql
        Replace upcmastsql.Id       With tempmast.Id      In upcmastsql
        Replace upcmastsql.updatedt With tempmast.adddt   In upcmastsql
        Replace upcmastsql.Info     With tempmast.Info    In upcmastsql
        Replace upcmastsql.Descrip  With tempmast.Descrip  In upcmastsql
      Else
        Set Step On
        Select tempmast
        Scatter Fields Except upcmastid Memvar Memo
        m.adddt    = Datetime()
		m.upcmastid=sqlgenpk("upcmast","wh")
        m.updatedt = Datetime()
        Select upcmastsql  && add the recored to SQL
        Insert Into upcmastsql From Memvar
      Endif
      NumUpdated = NumUpdated +1
      lcUpdateStr = lcUpdateStr +tempmast.upc+"-"+tempmast.Style+"-"+tempmast.Color+"-"+tempmast.Id+Chr(13)
&& add in all the updates
    Endif
  Endscan

*Set Step On

  Select tempmast
  Zap

  cLoadFile = (lcPath+cFilename)
  cArchiveFile = (lcArchivePath+cFilename)
  Copy File [&cLoadFile] To [&cArchiveFile]
  Delete File [&cLoadFile]

Endfor

tu("upcmastsql")  && push new records and record updates back into sql

tattach = ""
tFrom ="Toll EDI System Operations <toll-edi-ops@tollgroup.com>"
tmessage = "G3 Stylemaster updated at: "+Ttoc(Datetime())+Chr(13)+;
Chr(13)+lcAddedStr+Chr(13)+;
Chr(13)+lcUpdateStr

tSubject = "G3 Stylemaster Update"
Do Form m:\dev\frm\dartmail2 With tsendto,tFrom,tSubject,tcc,tattach,tmessage,"A"
Wait Window "All 832 Files finished processing...exiting" Timeout 2

*Close Data All

schedupdate()
*_Screen.Caption=gscreencaption
On Error
