* create or find PPADJ inbound WO when adding P&P carton/unit recrds into indet
* called from inven, ppdropariat, rcinven, wave, wavedropariat, wavelxe, wavelxeariat
* SQL version of ppinwo

lparameters xaccountid, xstyle, xcolor, xid, xpack, xwhseloc, xctnqty, xwavenum, xadjinven, xrackonhold, xwavepartial, xholdtype

store 0 to 	m.inwologid, m.wo_num

xpoadd = ppadjinwo(xaccountid, xctnqty*val(xpack), xctnqty)

* add carton to inbound

m.accountid=xaccountid
m.units=.f.
m.style=xstyle
m.color=xcolor
m.id=xid
m.pack=xpack
m.totqty=xctnqty
if xwavepartial
	addsql("update inwolog set removeqty=removeqty+"+transform(m.totqty)+", " + ;
		"remainqty=remainqty-"+transform(m.totqty)+" where wo_num="+transform(m.wo_num),"wh")
	m.removeqty=m.totqty
	m.remainqty=0
else
	m.removeqty=0
	m.remainqty=m.totqty
endif
m.po=xpoadd
m.date_rcvd=qdate()
store 9 to m.ctnwt, m.length, m.width, m.depth
m.ctncube=.42
m.totwt=m.ctnwt*m.totqty
m.totcube=m.ctncube*m.totqty
m.wavenum=xwavenum
insertinto("indet","wh",.t.)

m.indetid=indet.indetid
m.whseloc=xwhseloc
m.locqty=m.totqty
select inloc
insertinto("inloc","wh",.t.)

if m.totqty<0
	if m.color="34 32"
		set step on 
	endif
	xunusedqty = indetremove(-m.totqty, m.accountid, m.units, m.style, m.color, m.id, m.pack, m.whseloc,,,.t.)
endif

* add units to inbound, adjust vinvenloc

xinvenrecno=recno(xinven)
xinvenlocrecno=recno(xinvenloc)

if m.totqty<0	&& removal
	select xremindet
	sum totqty to xxremindettotqty
	
	scan
		select &xinven
		locate for units and style=xremindet.style and color=xremindet.color and id=xremindet.id and pack=xremindet.pack 
		if !found()
			assert .f.
			x3winmsg("You must have the units displayed on the screen in order to make this change"+crlf+crlf+"Remove one or more of the filters")
			set deleted &xdeleted
			return .f.
		endif

		if xunusedqty#0 and xwavenum#0	&& problem with removeqty
*			set step on 
		endif

		m.totqty=xremindet.totqty/xxremindettotqty*xctnqty*val(xpack)
		m.removeqty=0
		m.remainqty=m.totqty
		scatter memvar fields units, style, color, id, pack
		store 0 to m.ctnwt, m.length, m.width, m.depth, m.ctncube
		m.wavenum=xwavenum
		insertinto("indet","wh",.t.)
		m.indetid=indet.indetid
		
		ppinworest(!empty(xwavenum))
	endscan
	
else	&& add  
	m.units=.t.
	m.pack="1         "

	m.totqty=indet.totqty*val(indet.pack)
	if xwavepartial
		addsql("update inwolog set unitsoutqty=unitsoutqty+"+transform(m.totqty)+", " + ;
			"unitsopenqty=unitsopenqty-"+transform(m.totqty)+" where wo_num="+transform(m.wo_num),"wh")
		m.removeqty=m.totqty
		m.remainqty=0
	else
		m.removeqty=0
		m.remainqty=m.totqty
	endif
	store 0 to m.ctnwt, m.length, m.width, m.depth, m.ctncube
	m.wavenum=xwavenum
	insertinto("indet","wh",.t.)

	ppinworest(!empty(xwavenum) or !xadjinven, xrackonhold, xwavepartial, xholdtype)
endif

try
	go xinvenrecno in &xinven
	go xinvenlocrecno in &xinvenloc
catch
endtry

set deleted &xdeleted
