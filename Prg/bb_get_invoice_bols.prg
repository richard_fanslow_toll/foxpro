* bb_get_invoice_bols
*
* created 01/31/2018
*
* Take a spreadsheet with a list of overdue BedBath invoices from Jim Campanelli in Finance, and look up the BOL associated with each invoice.
* Populate the spreadsheet with the BOLs.
*
* Author: Mark Bennett

SET SAFETY OFF

LOCAL loExcel, loWorkbook, loWorksheet
LOCAL lcSpreadsheetIN, lcSpreadsheetOUT, lcRow, lnRow, i, lcInvNum

lcSpreadsheetIN = 'F:\FTPUSERS\BEDBATH\XLS\BBB INVOICE REQUEST JANUARY 2018.XLSX'
lcSpreadsheetOUT = 'F:\FTPUSERS\BEDBATH\XLS\BBB INVOICE REQUEST JANUARY 2018-WITH BOLS.XLSX' 

loExcel = CREATEOBJECT("excel.application")
loExcel.displayalerts = .F.
loExcel.VISIBLE = .F.

loWorkbook = loExcel.workbooks.OPEN(lcSpreadsheetIN)
loWorkbook.SAVEAS(lcSpreadsheetOUT)
loWorksheet = loWorkbook.Worksheets[1]

* open the foxpro table that has both inv nums and BOL #s.
* This was gotten via:
*	xsqlexec("select distinct invnum, bol from bbin order by invnum","xbbin",,"ar")

USE F:\FTPUSERS\BEDBATH\XLS\IN_INV_BOLS IN 0 ALIAS IN_INV_BOLS

*BROWSE

FOR lnRow = 2 TO 222
	lcRow = ALLTRIM(STR(lnRow))
	lcInvNum = loWorksheet.RANGE("C" + lcRow).VALUE 
	SELECT IN_INV_BOLS
	LOCATE FOR ALLTRIM(INVNUM) == ALLTRIM(lcInvNum)
	IF FOUND() THEN
		loWorksheet.RANGE("F" + lcRow).VALUE = "'" + IN_INV_BOLS.BOL
	ENDIF
ENDFOR

loWorkbook.SAVE() 
loExcel.QUIT()

CLOSE DATABASES ALL
RETURN
