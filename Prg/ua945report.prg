PARAMETERS whichoffice,whichuser, xfromwebrpt
DO m:\dev\prg\_setvars WITH .T.
SET ESCAPE ON
ON ESCAPE CANCEL

IF !xfromwebrpt
	utilsetup("UA945REPORT")
ENDIF

PUBLIC fieldlist

numdays = 5

* set some defaults
tsendto ="pgaidis@fmiint.com"
tcc = ""
tbcc=""

TRY
	PUBLIC normalexit
	normalexit = .F.
	whichoffice = ALLTRIM(TRANSFORM(whichoffice))
	whichuser = ALLTRIM(TRANSFORM(whichuser))
	_SCREEN.WINDOWSTATE = IIF(INLIST(whichuser,"JOEB","JBIANCHI"),1,2)
*!* Added the following to limit weekend processing to 1PM and 5PM only from Joe's PC
	IF INLIST(DOW(DATE()),1,7) AND INLIST(whichuser,"JOEB","JBIANCHI") AND !INLIST(HOUR(DATETIME()),13,17)
		IF !xfromwebrpt
			schedupdate()
			CLOSE DATABASES ALL
			normalexit = .T.
			THROW
		ENDIF
	ENDIF

*MessageBox("945 report:+ P1 = "+Transform(whichoffice)+" P2 = "+Transform(whichuser),16,"Debugger")

	SET EXCLUSIVE OFF
	SET SAFETY OFF
	CLOSE DATA ALL
	SET DELETED ON
	SET EngineBehavior 70

	WAIT WINDOW AT 10,10 "Parsing the FTP logs... Please wait......." NOWAIT

	DO parse_all_ftp_logs

	use (wf("N",5687)+"outship") IN 0 ALIAS outship_nj
	use (wf("C",5687)+"outship") IN 0 ALIAS outship_ca
	use (wf("M",5687)+"outship") IN 0 ALIAS outship_fl

	USE F:\wo\wodata\manifest IN 0 ALIAS manifest
	USE F:\wo\wodata\DETAIL   IN 0 ALIAS DETAIL


	IF !USED('edi_trigger')
		cEDIFolder = IIF(DATE()<{^2010-01-24},"F:\JAG3PL\DATA\","F:\3PL\DATA\")
		USE (cEDIFolder+"edi_trigger") IN 0 ALIAS edi_trigger
	ENDIF
	USE F:\ftplogs\DATA\ftplog IN 0

	WAIT WINDOW AT 10,10 "Loading in the outship records from CA, FL and NJ..... Please wait......." NOWAIT

* OK, get the CA  data from outship

	SELECT wo_num AS FMIWorkOrder,consignee,wo_date AS workorderdate,"CA" AS office,ship_ref AS PONumber,bol_no AS BOL,scac,START,CANCEL,del_date,appt,SPACE(10) AS delloc,SPACE(32) AS ShipmentFile,;
		SPACE(15) AS EDIStatus,SPACE(15) AS UA_Status,SPACE(20) AS Xfertimetxt,{  /  /       :  :  } AS trigtime,{  /  /       :  :  } AS proctime, {  /  /       :  :  } AS xfertime,SPACE(8) AS hour_delay ;
		FROM outship_ca WHERE accountid = 5687 AND (appt = DATE() OR EMPTYnul(del_date) OR del_date >= DATE()-numdays) INTO CURSOR bols READWRITE

* from outship_ca where accountid = 5687 and (del_date > Date()-15 or Emptynul(del_date)) into cursor bols readwrite

	SELECT bols
	GOTO TOP
	SCAN
		DELETE FOR workorderdate < {^2009-06-30}
	ENDSCAN

	USE IN outship_ca

* OK, get the Miami data from outship

	SELECT wo_num AS FMIWorkOrder,consignee,wo_date AS workorderdate,"FL" AS office,ship_ref AS PONumber,bol_no AS BOL,scac,START,CANCEL,del_date,appt,SPACE(32) AS ShipmentFile,;
		SPACE(15) AS EDIStatus,SPACE(15) AS UA_Status,SPACE(20) AS Xfertimetxt,{  /  /       :  :  } AS trigtime,{  /  /       :  :  } AS proctime, {  /  /       :  :  } AS xfertime,SPACE(8) AS hour_delay ;
		FROM outship_fl WHERE accountid = 5687 AND (appt= DATE() OR EMPTYnul(del_date) OR del_date >= DATE()-numdays) INTO CURSOR bols_fl READWRITE

* OK, get the NJ data from outship

	SELECT wo_num AS FMIWorkOrder,consignee,wo_date AS workorderdate,"NJ" AS office,ship_ref AS PONumber,bol_no AS BOL,scac,START,CANCEL,del_date,appt,SPACE(32) AS ShipmentFile,;
		SPACE(15) AS EDIStatus,SPACE(15) AS UA_Status,SPACE(20) AS Xfertimetxt,{  /  /       :  :  } AS trigtime,{  /  /       :  :  } AS proctime, {  /  /       :  :  } AS xfertime,SPACE(8) AS hour_delay ;
		FROM outship_nj WHERE accountid = 5687 AND (appt= DATE() OR EMPTYnul(del_date) OR del_date >= DATE()-numdays) INTO CURSOR bols_nj READWRITE

	SELECT getmemodata("suppdata","SHIPID") AS isair,PADR(getmemodata("suppdata","COMPLETEDT"),19," ") AS compdate,{  /  /    } AS closeddate,wo_num AS FMIWorkOrder,;
		wo_date AS workorderdate,"TRNSLOAD-"+ALLTRIM(delloc) AS consignee,office,ALLTRIM(TRANSFORM(wo_num)) AS BOL,delivered AS del_date,delloc,;
		SPACE(32) AS ShipmentFile,{  /  /       :  :  } AS START, {  /  /       :  :  } AS CANCEL,"FMIX" AS scac,;
		SPACE(15) AS EDIStatus,SPACE(15) AS UA_Status,SPACE(20) AS Xfertimetxt,{  /  /       :  :  } AS trigtime,{  /  /       :  :  } AS proctime, {  /  /       :  :  } AS xfertime,SPACE(8) AS hour_delay ;
		FROM manifest WHERE accountid = 5687 AND ((delivered >= DATE()-5) OR EMPTYnul(delivered)) INTO CURSOR bols_transload READWRITE

	SELECT getmemodata("suppdata","DELLOCID") AS del,wo_num AS FMIWorkOrder,wo_date AS workorderdate,"FMI WEST PICK-PACK" AS consignee,office,;
		ALLTRIM(TRANSFORM(wo_num)) AS BOL,wo_date AS del_date,;
		SPACE(32) AS ShipmentFile,{  /  /       :  :  } AS START, {  /  /       :  :  } AS CANCEL,"FMIPNP" AS scac,;
		SPACE(15) AS EDIStatus,SPACE(15) AS UA_Status,SPACE(20) AS Xfertimetxt,{  /  /       :  :  } AS trigtime,{  /  /       :  :  } AS proctime, {  /  /       :  :  } AS xfertime,SPACE(8) AS hour_delay;
		FROM DETAIL WHERE (accountid = 5687 AND delloc = "FMIW PNP" AND wo_date >= DATE()-10) GROUP BY FMIWorkOrder INTO CURSOR bols_pnp READWRITE

	SELECT bols_transload
	GOTO TOP
	DELETE FOR compdate ="NA"

	SCAN
		REPLACE  closeddate WITH CTOD(SUBSTR(compdate,1,10))
		REPLACE del_date WITH closeddate
		REPLACE delloc WITH STRTRAN(delloc,"DH-","00")
	ENDSCAN
	GOTO TOP
	DELETE FOR del_date < DATE()-10


	SELECT* FROM  bols_transload GROUP BY FMIWorkOrder,delloc INTO CURSOR bols_transload READWRITE
	SELECT bols_transload
	DELETE FOR isair ="NA"  && no shipid in suppdata means probably all air and no 945 sent

* from outship_fl where accountid = 5687 and (del_date > Date()-15 or Emptynul(del_date)) into cursor bols_fl readwrite
	SELECT bols
	GOTO TOP
	SCAN
		DELETE FOR workorderdate < {^2009-06-30}
	ENDSCAN

	USE IN outship_fl
	USE IN outship_nj
	USE IN manifest
	USE IN DETAIL

	SELECT bols_fl
	SCAN
		SELECT bols_fl
		SCATTER MEMVAR
		SELECT bols
		APPEND BLANK
		GATHER MEMVAR
	ENDSCAN

	SELECT bols_nj
	SCAN
		SELECT bols_nj
		SCATTER MEMVAR
		SELECT bols
		APPEND BLANK
		GATHER MEMVAR
	ENDSCAN

	SELECT bols_transload
	SCAN
		SELECT bols_transload
		SCATTER MEMVAR
		SELECT bols
		APPEND BLANK
		GATHER MEMVAR
	ENDSCAN

	SELECT bols_pnp
	SCAN
		SELECT bols_pnp
		SCATTER MEMVAR
		SELECT bols
		APPEND BLANK
		GATHER MEMVAR
	ENDSCAN

	WAIT WINDOW AT 10,10 "Updating the Crossdock EDI Trigger information..... Please wait......." NOWAIT
	SELECT bols
	GOTO TOP
	SCAN FOR !"TRNSLOAD"$consignee AND !"FMI WEST PICK-PACK"$consignee
		IF !EMPTY(bols.BOL)
			SELECT edi_trigger
			SET ORDER TO BOL
			SEEK bols.BOL
			IF FOUND()
				REPLACE bols.ShipmentFile WITH JUSTFNAME(file945)
				REPLACE bols.proctime WITH when_proc
				REPLACE bols.trigtime WITH trig_time
				REPLACE bols.EDIStatus WITH fin_status
			ENDIF
		ENDIF
	ENDSCAN

	WAIT WINDOW AT 10,10 "Updating the PNP EDI Trigger information..... Please wait......." NOWAIT
	SELECT edi_trigger
	SET ORDER TO
	SELECT bols
	GOTO TOP
	SCAN FOR consignee = "FMI WEST PICK-PACK"
		SELECT edi_trigger
		LOCATE FOR edi_trigger.edi_type = "945P" AND edi_trigger.wo_num = bols.FMIWorkOrder
		IF FOUND()
			REPLACE bols.ShipmentFile WITH JUSTFNAME(file945)
			REPLACE bols.proctime WITH when_proc
			REPLACE bols.trigtime WITH trig_time
			REPLACE bols.EDIStatus WITH fin_status
		ENDIF
	ENDSCAN

	SELECT edi_trigger
	SET ORDER TO

Set Step On 

	WAIT WINDOW AT 10,10 "Updating Transload EDI Trigger information..... Please wait......." NOWAIT
	SELECT bols
	GOTO TOP
	SCAN FOR "TRNSLOAD"$consignee
		IF !EMPTY(bols.BOL)
			SELECT edi_trigger
			LOCATE FOR bols.BOL = edi_trigger.BOL AND Alltrim(bols.delloc) = Alltrim(edi_trigger.dellocid)
			IF FOUND()
				REPLACE bols.ShipmentFile WITH JUSTFNAME(file945)
				REPLACE bols.proctime WITH when_proc
				REPLACE bols.trigtime WITH trig_time
				REPLACE bols.EDIStatus WITH fin_status
			ENDIF
		ENDIF
	ENDSCAN

	SELECT bols
	LOCATE
*!* This section added by Joe to skip any duplicated files, 07.02.2010
	SCAN
		IF LEFT(ALLTRIM(bols.EDIStatus),6) = "NO 945"
			ASSERT .F.
			DELETE NEXT 1 IN bols
		ENDIF
	ENDSCAN

	GOTO TOP
	SCAN
		SELECT ftplog
		LOCATE FOR UPPER(ALLTRIM(bols.ShipmentFile))$UPPER(ftplog.filename)
		IF FOUND()
			REPLACE bols.UA_Status WITH "PickedUp" IN bols
			REPLACE bols.Xfertimetxt WITH ftplog.xfertime IN bols
		ELSE
			REPLACE bols.UA_Status WITH "NO" IN bols
		ENDIF
	ENDSCAN

	SELECT bols
	lcErrMessage = "No Undelivered Shipment files........."
	SELECT * FROM bols WHERE !EMPTY(ShipmentFile) AND UA_Status = "NO" INTO CURSOR errs
	IF RECCOUNT("errs") >=1
		lcErrMessage ="Please check the following shipment files that were NOT picked up:"+CHR(13)+CHR(13)
		SELECT errs
		GOTO TOP
		SCAN
			lcErrMessage = lcErrMessage + "   Shipment File: "+ALLTRIM(errs.ShipmentFile)+"   PO: "+ALLTRIM(errs.PONumber)+CHR(13)
		ENDSCAN
	ENDIF
	SELECT bols
	GOTO TOP
	SCAN
		REPLACE xfertime WITH txttotime(Xfertimetxt)
		REPLACE hour_delay WITH STR((xfertime-trigtime)/3600,5,2)
	ENDSCAN

	EXPORT TO c:\tempfox\UA_Shipment_Status.XLS TYPE XLS

	SELECT * FROM bols GROUP BY BOL,delloc ORDER BY del_date DESCEN INTO CURSOR temp READWRITE
	SELECT temp
	DELETE FOR EMPTY(BOL)
	rptok = .F.

	DO reporttopdf WITH "ua945report_RevB","c:\tempfox\UA945Report",rptok

	WAIT WINDOW AT 10,10 "Sending the Email..... Please wait......." TIMEOUT 1
	tcc=""
	DO CASE
		CASE  whichoffice = "M"
			tsendto ="jabbate@fmiint.com,bvaldes@fmiint.com,kzelaya@fmiint.con"
			tcc = "twinneberger@underarmour.com,pgaidis@fmiint.com"
		CASE  INLIST(whichoffice,"5","C")
			tsendto ="mvega@fmiint.com,eruiz@fmiint.com"
			tcc = "twinneberger@underarmour.com,pgaidis@fmiint.com"
		OTHERWISE
			tcc = ""
			tsendto ="pgaidis@fmiint.com"
	ENDCASE

	DO CASE
		CASE  UPPER(whichuser) = "JUAN"
			tsendto ="juan@fmiint.com"
			tcc = "pgaidis@fmiint.com"
		CASE  UPPER(whichuser) = "JOEB"
			tsendto ="joe.bianchi@tollgroup.com"
			tcc = "pgaidis@fmiint.com"
		CASE  UPPER(whichuser) = "JIMK"
			tsendto ="jkillen@fmiint.com"
			tcc = "pgaidis@fmiint.com"
		CASE  UPPER(whichuser) = "PAULG"
			tsendto ="pgaidis@fmiint.com"
			tcc = ""
		CASE  UPPER(whichuser) = "GOLDBERG"
			tsendto ="mgoldberg@fmiint.com"
			tcc = "pgaidis@fmiint.com"
	ENDCASE




	lcpath = "f:\ftpusers\underarmour\tpmshipments\pending resolution\"
	pendfiles = .F.
	xfile_pending_message=""
	lnNum = ADIR(tarray,lcpath+"*.xml")
	IF lnNum > 0
		pendfiles = .T.
		xfile_pending_message=CHR(13)+"Check these File(s) in Pending Resolution:"+CHR(13)
		FOR thisfile = 1  TO lnNum
			xfile = tarray[thisfile,1]
			xfile_pending_message =xfile_pending_message+ "     "+xfile+CHR(13)
		ENDFOR
	ENDIF


* email for those without the attachment
	tsubject = "Updated Under Armour Shipment Status"
	tmessage = "See attached report file" + CHR(13) + CHR(13) + " For content or distribution changes contact "+CHR(13)+" Paul Gaidis 732-750-9000 x143"+CHR(13)+CHR(13)
	tmessage = tmessage +lcErrMessage+CHR(13)+" Triggered by: "+UPPER(whichuser)+" Warehouse: "+whichoffice
	IF pendfiles = .T.
		tmessage = tmessage +CHR(13)+xfile_pending_message
	ENDIF
	tattach  = "c:\tempfox\UA945Report.pdf"
	tfrom    ="TGF EDI Processing Center <transload-ops@fmiint.com>"
	DO FORM m:\dev\FRM\dartmail2 WITH tsendto,tfrom,tsubject,tcc,tattach,tmessage,"A"

* email for those with the attachment
*!*	  tsubject = "Updated Under Armour Shipment Status"
*!*	  tmessage = "See attached report file" + Chr(13) + Chr(13) + " For content or distribution changes contact "+Chr(13)+" Paul Gaidis 732-750-9000 x143"+Chr(13)+Chr(13)
*!*	  tmessage = tmessage +lcErrMessage+Chr(13)+"triggered by: "+Upper(whichuser)+" Warehouse: "+whichoffice

*!*	  tattach  = "c:\tempfox\UA_Shipment_Status.xls"
*!*	  tfrom    ="FMI EDI Processing Center <fmi-transload-ops@fmiint.com>"
*!*	  Do Form m:\dev\FRM\dartmail2 With tsendto,tfrom,tsubject,tcc,tattach,tmessage,"A"

	USE IN edi_trigger
	USE IN ftplog

CATCH TO oErr
	IF !normalexit
		ptError = .T.
		tsubject = "Report Program Error ("+TRANSFORM(oErr.ERRORNO)+") at "+TTOC(DATETIME())
		tmessage = "Report Program Error processing "

		tmessage =tmessage+ "Try/CATCH Exeception Message:"+CHR(13)+;
			[  Error: ] + STR(oErr.ERRORNO) +CHR(13)+;
			[  LineNo: ] + STR(oErr.LINENO) +CHR(13)+;
			[  Message: ] + oErr.MESSAGE +CHR(13)+;
			[  Procedure: ] + oErr.PROCEDURE +CHR(13)+;
			[  Details: ] + oErr.DETAILS +CHR(13)+;
			[  StackLevel: ] + STR(oErr.STACKLEVEL) +CHR(13)+;
			[  LineContents: ] + oErr.LINECONTENTS+CHR(13)+;
			[  UserValue: ] + oErr.USERVALUE+CHR(13)


		tsubject = "Report Processing Error at "+TTOC(DATETIME())
		tattach  = ""
		tcc="  "
		tsendto  = "pgaidis@fmiint.com,joe.bianchi@tollgroup.com"
		tfrom    ="TGF WMS Report Engine <transload-ops@fmiint.com>"
		DO FORM dartmail2 WITH tsendto,tfrom,tsubject,tcc,tattach,tmessage,"A"
	ENDIF
ENDTRY

IF !xfromwebrpt
	schedupdate()
ENDIF
