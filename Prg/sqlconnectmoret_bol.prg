PARAMETERS nAcctNum,cBOL,cPPName,lUCC,cOffice,lDoCartons,cSQL
IF VARTYPE(cOffice) # "C"
	cOffice = "N"
ENDIF

lVPPfile = .T. && Default = .t. (Creates VMORETPP working file)

SET ESCAPE ON
ON ESCAPE CANCEL

*ASSERT .F. MESSAGE "In SQL BOL program"
IF USED("tempmor1")
	USE IN tempmor1
ENDIF

cFileOutName="V"+TRIM(cPPName)+"pp"
*wait window "File out name is: "+cFileOutName time 2
cFileOutName1 = "F:\3pl\data\V"+TRIM(cPPName)+"pp.dbf"
nAcct = ALLTRIM(STR(nAcctNum))
IF EMPTY(cSQL)
	cSQL = "tgfnjsql01"
ENDIF

SELECT 0
USE F:\wh\sqlpassword
cSQLPass = ALLTRIM(sqlpassword.PASSWORD)
USE IN sqlpassword

*ASSERT .F. MESSAGE "In SQL BOL Connect"
IF lTestinput
	SELECT IIF("SQL3"$shipins,.F.,.T.) AS scanpack,bol_no,wo_num ;
		FROM outship WHERE bol_no = cBOL ;
		AND accountid = nAcctNum ;
		GROUP BY 1,2,3 ;
		ORDER BY 1,2,3 ;
		INTO DBF F:\3pl\DATA\sqlwomoret
ELSE		
	SELECT IIF("FLS2"$shipins OR "MLS1"$shipins OR "SQL5"$shipins,.F.,.T.) AS scanpack,bol_no,wo_num ;
		FROM outship WHERE bol_no = cBOL ;
		GROUP BY 1,2,3 ;
		ORDER BY 1,2,3 ;
		INTO DBF F:\3pl\DATA\sqlwomoret
ENDIF

USE IN sqlwomoret
USE F:\3pl\DATA\sqlwomoret IN 0 ALIAS sqlwomoret
SELECT sqlwomoret
LOCATE
IF DATETIME() < DATETIME(2011,11,08,23,15,00) OR !lDoUpdate
	BROWSE TIMEOUT 60
ENDIF

lAppend = .F.
FOR iz = 1 TO 2
	SELECT sqlwomoret
	LOCATE
	IF iz = 1
		lSQL3 = .F.
	ELSE
		lSQL3 = .T.
	ENDIF
	SET FILTER TO sqlwomoret.scanpack = lSQL3
	
*!*		DO CASE
*!*			CASE lTestinput
*!*				lcDSNLess="driver=SQL Server;server=sql3;uid=SA;pwd=&cSQLPass;DATABASE=PICKPACK"
*!*			CASE INLIST(cOffice,"C","X")
*!*				lcDSNLess="driver=SQL Server;server=sql5;uid=SA;pwd=&cSQLPass;DATABASE=PICKPACK"	&& dy 2/18/18
*!*	*			lcDSNLess="driver=SQL Server;server=10.16.1.16;uid=SA;pwd=&cSQLPass;DATABASE=PICKPACK"
*!*			CASE INLIST(cOffice,"N","I","J")
*!*				lcDSNLess="driver=SQL Server;server=&cSQL;uid=SA;pwd=&cSQLPass;DATABASE=PICKPACK"
*!*			CASE INLIST(cOffice,"M")
*!*				lcDSNLess="driver=SQL Server;server=fls2;uid=SA;pwd=&cSQLPass;DATABASE=PICKPACK"
*!*			CASE INLIST(cOffice,"L")
*!*				lcDSNLess="driver=SQL Server;server=sql5;uid=SA;pwd=&cSQLPass;DATABASE=PICKPACK"
*!*			OTHERWISE
*!*				lcDSNLess="driver=SQL Server;server=sql5;uid=SA;pwd=&cSQLPass;DATABASE=PICKPACK"	&& dy 2/18/18
*!*	*			
*!*		ENDCASE

	lcDSNLess="driver=SQL Server;server=tgfnjsql01;uid=SA;pwd=&cSQLPass;DATABASE=PICKPACK"

	nHandle=SQLSTRINGCONNECT(m.lcDSNLess,.T.)
	SQLSETPROP(0,'DispLogin',3)
	SQLSETPROP(0,"dispwarnings",.F.)
	*	WAIT WINDOW "Now processing "+cPPName+" SQL view...please wait" NOWAIT
	IF nHandle=0 && bailout
		WAIT WINDOW "Could not make SQL connection" TIMEOUT 3
		cRetMsg = "NO SQL CONNECT"
		RETURN cRetMsg
		THROW
	ENDIF

	SELECT sqlwomoret
	LOCATE
	
	SCAN
		nWO_Num = sqlwomoret.wo_num
		nWo   = ALLTRIM(STR(nWO_Num))

		IF !lDoCartons OR (lTesting AND lTestinput) OR DATETIME()<DATETIME(2017,10,5,22,00,00)
			lcQ1=[SELECT * FROM dbo.labels Labels]
			lcQ2 = [ WHERE Labels.wo_num = ]
			lcQ3 = " &nWo "
			IF lUCC
				lcQ6 = [order by Labels.ship_ref,Labels.outshipid,Labels.ucc]
			ELSE
				lcQ6 = [order by Labels.ship_ref,Labels.outshipid,Labels.CartonNum]
			ENDIF
			lcsql = lcQ1+lcQ2+lcQ3+lcQ6

		else
			if usesql()
				if lucc
					xorderby="order by ship_ref, outshipid, ucc"
				else
					xorderby="order by ship_ref, outshipid, cartonnum"
				endif
				
				xsqlexec("select * from cartons where wo_num="+nwo+" "+xorderby,cFileOutName,,"pickpack")
			else
				lcQ1 = [SELECT * FROM dbo.cartons Cartons]
				lcQ2 = [ WHERE Cartons.wo_num = ]
				lcQ3 = " &nWo "
				IF lUCC
					lcQ6 = [order by Cartons.ship_ref,Cartons.outshipid,Cartons.ucc]
				ELSE
					lcQ6 = [order by Cartons.ship_ref,Cartons.outshipid,Cartons.CartonNum]
				ENDIF

				lcsql = lcQ1+lcQ2+lcQ3+lcQ6
			endif
		ENDIF

		if usesql()
		else
			llSuccess=SQLEXEC(nHandle,lcsql,cFileOutName)

			IF llSuccess<1  && no records 0 indicates no records and 1 indicates records found
				WAIT WINDOW "No "+cPPName+" records found" TIMEOUT 2
				cRetMsg = "NO SQL CONNECT"
				RETURN cRetMsg
			ENDIF
		endif
		
		IF lAppend = .F.
			lAppend = .T.
			SELECT &cFileOutName
			LOCATE
			COPY TO ("F:\3pl\DATA\tempmor1")
			USE ("F:\3pl\DATA\tempmor1") IN 0 ALIAS tempmor1
		ELSE
			SELECT &cFileOutName
			SCAN
				SCATTER MEMVAR
				INSERT INTO tempmor1 FROM MEMVAR
			ENDSCAN
		ENDIF
	ENDSCAN
	SQLCANCEL(nHandle)
	SQLDISCONNECT(nHandle)
ENDFOR

SELECT sqlwomoret
SET FILTER TO

IF USED(cFileOutName)
	USE IN &cFileOutName
ENDIF
IF USED(cFileOutName1)
	USE IN &cFileOutName1
ENDIF

SELECT tempmor1
LOCATE
*BROWSE TIMEOUT 5

IF lVPPfile
	IF lUCC
		SELECT * FROM tempmor1 ;
			ORDER BY ship_ref,outshipid,ucc ;
			INTO DBF &cFileOutName1
	ELSE
		SELECT * FROM tempmor1 ;
			ORDER BY ship_ref,outshipid,cartonnum ;
			INTO DBF &cFileOutName1
	ENDIF
	IF lTesting
		*		SELECT &cFileOutName
		*		BROWSE TIMEOUT 10
	ENDIF
	GO BOTT
ENDIF

USE IN tempmor1

cRetMsg = "OK"
RETURN cRetMsg
