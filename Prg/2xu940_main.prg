PARAMETERS cOfficeIn
*!* m:\dev\prg\2xu940_main.prg

PUBLIC nAcctNum,cDelimiter,nRun_num,cTranslateOption,cWhichFile,cOffice,cCustname,cProperName,cStorenum,lOK
PUBLIC cFilter,nRunID,lComplete,lTesting,m.UCCNUMBER,lEmail,lTestImport,emailcommentstr,cUseFolder
PUBLIC ptid,ptctr,ptqty,xfile,cfilename,nUploadCount,nFileCount,units,cUCCNumber,archivefile,llEmailTest
PUBLIC lcPath,lcArchivepath,tsendto,tcc,NormalExit,lTestmail,LogCommentStr,lPick,lPrepack,tsendtoerr,tccerr
PUBLIC m.addby,m.adddt,m.addproc,cTransfer,lHighline,tfrom,tattach,cisa_num,thisfile,tsendtotest,tcctest
PUBLIC lBrowfiles,lLoadSQL,lLoadEDIInlog,cMod,nFileSize,cErrorMessage,lNextDay,lMonday,l4Day
PUBLIC ARRAY thisarray(1)
PUBLIC ARRAY a856(1)
PUBLIC lReload
Public tendto,tsendtoerr,tcc,tccerr
Public llEmailtest

CLOSE DATABASES ALL
CLEAR
ON ERROR THROW

tsendto    ="pgaidis@fmiint.com"
tsendtoerr ="pgaidis@fmiint.com"
tcc        ="joe.bianchi@tollgroup.com"
tccerr     ="joe.bianchi@tollgroup.com"


xfile="No file yet"
cProperName = "2XU 940 UPLOAD"
cOfficeIn = "Y"
cErrorMessage ="Early Error"
DO m:\dev\prg\lookups
llEmailTest = .t.

** when testing, set the scac,carrier codesm ,\put in a phony BOL, trking# and make sure labels is copied to cartons.

*Set Step On

TRY
	lTesting = .f.

*  gldor = .f. && set this to false to use test SQL server, make sure no ther SQL conenctions exist.........DO C 

	lReload = .F.  && this works with a list in the bkdn module to filter out all but what is wanted to reload
&& set this true and then at the end of bkdn edit the list to include the PTS you want to reload.
	goffice="C"

	DO m:\dev\prg\_setvars WITH lTesting

	lTestImport   = lTesting
	lTestmail     = lTesting
	lOverridebusy = lTesting
	lBrowfiles    = lTesting
	lBrowfiles = .f.
*	lOverridebusy = .t.

 	 lLoadSQL = .t.
 	lLoadEDIInlog= .T.

	NormalExit = .F.
	nAcctNum  = 6665
	lEmail = .T.
	_SCREEN.WINDOWSTATE= IIF(lTesting OR lOverridebusy,2,1)


	IF VARTYPE(cOfficeIn)="L"
		cOfficeIn = "Y"
	ENDIF
	cCustname = "2XU"
	cMod = "P"
	ASSERT .F. MESSAGE "At start of 2XU 940 processing"

	IF !lTesting
		SELECT 0
		USE F:\edirouting\ftpsetup SHARED
		cTransfer = "940-2XU"
		LOCATE FOR ftpsetup.transfer = cTransfer AND ftpsetup.TYPE = "GET"
		IF ftpsetup.chkbusy AND !lOverridebusy
			WAIT WINDOW "Process is flagged busy...returning" TIMEOUT 3
			NormalExit = .T.
			THROW
		ELSE
			REPLACE chkbusy WITH .T.,trig_time WITH DATETIME() FOR ftpsetup.transfer = cTransfer IN ftpsetup
			USE IN ftpsetup
		ENDIF
	ENDIF

	LogCommentStr = ""

	STORE cOfficeIn TO cOffice
	cMod = cOffice
	tfrom = "Toll EDI Operations <toll-edi-ops@tollgroup.com>"
	STORE " " TO tattach,tsendto,tcc

	SELECT 0
	USE F:\3pl\DATA\mailmaster ALIAS mm SHARED
	LOCATE FOR mm.edi_type = "940" AND mm.accountid = nAcctNum AND mm.office = cOffice
	IF FOUND()
		STORE TRIM(mm.acctname) TO cCustname
		STORE TRIM(mm.basepath) TO lcPath
		STORE TRIM(mm.archpath) TO lcArchivepath
		_SCREEN.CAPTION = ALLT(mm.scaption)
*!*			STORE mm.testflag 	   TO lTest
		STORE TRIM(IIF(mm.use_alt,mm.sendtoalt,mm.sendto)) TO tsendto
		STORE TRIM(IIF(mm.use_alt,mm.ccalt,mm.cc)) TO tcc
		LOCATE
		LOCATE FOR mm.edi_type = "MISC" AND mm.taskname = "GENERAL"
		STORE TRIM(IIF(mm.use_alt,mm.sendtoalt,mm.sendto)) TO tsendtotest
		STORE TRIM(IIF(mm.use_alt,mm.ccalt,mm.cc)) TO tcctest
				LOCATE
		LOCATE FOR mm.edi_type = "MISC" AND mm.taskname = "ERRMARIA"
		STORE TRIM(IIF(mm.use_alt,mm.sendtoalt,mm.sendto)) TO tsendtoerr
		STORE TRIM(IIF(mm.use_alt,mm.ccalt,mm.cc)) TO tccerr
		USE IN mm
	ELSE
		USE IN mm
		WAIT WINDOW AT 10,10  "No parameters set for this acct# "+ALLTRIM(STR(nAcctNum))+"  ---> Office "+cOffice TIMEOUT 2
		RELEASE ALL
		NormalExit = .F.
		THROW
	ENDIF

	IF lTesting
		STORE "f:\ftpusers\2xu\940IN\940test\"         TO lcPath
		STORE "f:\ftpusers\2xu\940IN\940test\archive\" TO lcArchivepath
	ENDIF

	cUseName = "2XU"
	cProperName = PROPER(cUseName)

	_SCREEN.CAPTION = cProperName+" 940 Process"
*	_SCREEN.WINDOWSTATE = IIF(lTesting,2,1)
	CLEAR
	WAIT WINDOW "Now setting up "+cProperName+" 940 process..." TIMEOUT 2
*	SET STEP ON
	DO m:\dev\prg\createx856a
	SELECT x856

	cfile = ""
	dXdate1 = ""
	dXxdate2 = DATE()
	nFileCount = 0

	nRun_num = 999
	lnRunID = nRun_num

	IF lTesting
		cUseFolder= "F:\WHP\WHDATA\"
	ELSE
		xReturn="XXX"
		DO m:\dev\prg\wf_alt WITH cOffice,nAcctNum
		cUseFolder=xReturn
	ENDIF

	DO ("m:\dev\PRG\"+cUseName+"940_PROCESS")

	IF !lTesting
		cTransfer = "940-2XU"
		SELECT 0
		USE F:\edirouting\ftpsetup SHARED
		REPLACE chkbusy WITH .F. ;
			FOR ftpsetup.transfer = cTransfer ;
			IN ftpsetup
	ENDIF

	NormalExit = .T.

CATCH TO oErr
	IF NormalExit = .F.
		ASSERT .F. MESSAGE "In Catch section..."
		SET STEP ON
		tsubject = cProperName+" 940 Upload Error ("+TRANSFORM(oErr.ERRORNO)+") at "+TTOC(DATETIME())
		tsendto  = tsendtoerr
		tcc = tccerr
		tattach  = ""
		tmessage = cProperName+" 940 Upload Error..... Please fix me........!"
		lcSourceMachine = SYS(0)
		lcSourceProgram = "from 2XU940 group"

		tmessage =tmessage+ "Try/CATCH Exeception Message:"+CHR(13)+;
			[  Error: ] + STR(oErr.ERRORNO) +CHR(13)+;
			[  LineNo: ] + STR(oErr.LINENO) +CHR(13)+;
			[  Message: ] + oErr.MESSAGE +CHR(13)+;
			[  Procedure: ] + oErr.PROCEDURE +CHR(13)+;
			[  Details: ] + oErr.DETAILS +CHR(13)+;
			[  StackLevel: ] + STR(oErr.STACKLEVEL) +CHR(13)+;
			[  LineContents: ] + oErr.LINECONTENTS+CHR(13)+;
			[  UserValue: ] + oErr.USERVALUE+CHR(13)+;
			[  Computer:  ] +lcSourceMachine+CHR(13)+;
			[  940 file:  ] +xfile+CHR(13)+;
			[  Program:   ] +lcSourceProgram+Chr(13)+Chr(13)+cErrorMessage

		DO FORM m:\dev\frm\dartmail2 WITH tsendto,tfrom,tsubject,tcc,tattach,tmessage,"A"
	ELSE
		WAIT WINDOW AT 10,10 "Normal Exit " TIMEOUT 2
	ENDIF
FINALLY
	ON ERROR
	SET STATUS BAR ON
	CLOSE DATABASES ALL
ENDTRY
