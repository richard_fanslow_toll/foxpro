* S3 832 STYLE IMPORT 8/21/2017 MB

utilsetup("S3832")
CLOSE DATA ALL

SET DATE MDY
SET CENTURY ON

PUBLIC lTesting
PUBLIC ARRAY a856(1)

lTesting = .T.
DO m:\dev\prg\_setvars WITH lTesting

LOCAL lnAccountID, lcUpdateSQL
lnAccountID = 6803

* This cursor will be indexed and used for the locates later on, to determine if we are doing an insert or an update, to speed things up
xsqlexec("select * from upcmast where accountid = " + ALLTRIM(STR(lnAccountID,4,0)),"upclookup",,"wh")
SELECT upclookup
INDEX ON accountid TAG accountid
INDEX ON upc TAG upc


* this cursor will be empty and will only be used for inserts - updates will be done with SQL updates commands
useca("upcmast","wh",,,"select * from upcmast where 1 = 0",,"upcmastsql")
SELECT "upcmastsql"
GO bottom


IF lTesting
	lcPath = 'F:\FTPUSERS\S3\832INTest\'
	lcArchivePath = 'F:\FTPUSERS\S3\832INtest\archive\'
	tsendto = "mbennett@fmiint.com"
	tcc = ""
ELSE
	lcPath = 'F:\FTPUSERS\S3\832IN\'
	lcArchivePath = 'F:\FTPUSERS\S3\832IN\archive\'
	tsendto = "mbennett@fmiint.com"
	tcc = ""
ENDIF


CD &lcPath
len1 = ADIR(ary1,"EDI832*TXT")
IF len1 = 0
	WAIT WINDOW "No files found...exiting" TIMEOUT 2
	CLOSE DATA ALL
	schedupdate()
	_SCREEN.CAPTION=gscreencaption
	ON ERROR
	RETURN
ENDIF

lcAddedStr  ="Styles Added:"+CHR(13)
lcUpdateStr ="Styles Updated:"+CHR(13)
*Set Step On
FOR xtodd = 1 TO len1
	cFilename = ALLTRIM(ary1[xtodd,1])
	xfile = lcPath+cFilename
	*****TM added
	SELECT * ;
		FROM upcmastsql ;
		WHERE .F. ;
		INTO CURSOR tempmast READWRITE
		
	ALTER TABLE tempmast DROP COLUMN upcmastid
	
	DO m:\dev\prg\createx856a
	DO m:\dev\prg\loadedifile WITH xfile,"*","NOTILDE"

	m.addby = "FMI-PROC"
	m.adddt = DATETIME()
	m.addproc = "S3832"
	m.accountid = lnAccountID
	m.pnp = .T.
	*m.uom = "EA"

	SELECT x856

	*browse

	LOCATE
	SCAN
		*SET STEP ON
		DO CASE
			CASE x856.segment = "LIN"
				**reset in case segments do not exist for upc - mvw 03/19/14
				STORE "" TO m.sid,m.info,m.descrip, m.color, m.id, m.upc
				STORE 0 TO m.price,m.rprice

				IF x856.f2 = "UP"
					m.upc = ALLT(x856.f3)
				ENDIF
				
				*IF x856.f4 = "ST"
				IF x856.f4 = "VC"
					m.style = ALLT(x856.f5)
				ENDIF
				
				IF x856.f6 = "CL"
					m.color = ALLT(x856.f7)
				ENDIF
				
				*!*	IF x856.f8 = "SZ"
				*!*		m.id = ALLT(x856.f9)
				*!*	ENDIF

				*!*	IF x856.f22 = "SE"
				*!*		m.info = "SEASON*"+ALLT(UPPER(x856.f23))  &&Force uppercase
				*!*	ENDIF

			CASE x856.segment = "PID" AND x856.f1 = "F" AND x856.f2 = "08"
				m.descrip = ALLT(x856.f5)

			CASE x856.segment = "PID" AND x856.f1 = "F" AND x856.f2 = "73"
				*m.info = m.info+Chr(13)+"COLORDESC*"+Alltrim(Upper(x856.f5))
				m.color = ALLTRIM(UPPER(x856.f5))

			CASE x856.segment = "PID" AND x856.f1 = "F" AND x856.f2 = "74"
				m.id = ALLTRIM(UPPER(x856.f5))
				
				* activate below to format the ID as 99.9 when they supply a size. They are sometimes giving values like 'PPK1' instead of a number.
				**not sure why this was removed, put back in so the size always contains a decimal - mvw 01/18/18
				IF (VAL(m.id) > 0) THEN
					m.id = VAL(m.id)
					m.id = ALLTRIM(STR(m.id,4,1))
				ENDIF
				
				INSERT INTO tempmast FROM MEMVAR

		ENDCASE

	ENDSCAN

	*SELECT tempmast
	*BROWSE


	*Set Step On
	RecCtr = 0
	NumAdded = 0
	NumUpdated = 0
	m.problem=.F.
	
	SELECT upcmastsql
	SCATTER MEMVAR MEMO BLANK

*SET STEP ON 

	SELECT tempmast 
	SCAN
		RecCtr = RecCtr +1
		WAIT WINDOW AT 10,10 "Checking Record # "+TRANSFORM(RecCtr) NOWAIT
		SCATTER MEMVAR MEMO
		
		SELECT upclookup
		LOCATE FOR (accountid = lnAccountID) AND (upc = tempmast.upc)

		IF NOT FOUND() THEN
			* insert into the useca cursor
			NumAdded = NumAdded +1

			m.adddt = DATETIME()
			m.updatedt = DATETIME()
			
			IF lTesting THEN
				m.upcmastid=0
			ELSE
				m.upcmastid=sqlgenpk("upcmast","wh")
			ENDIF
			
			INSERT INTO upcmastsql FROM MEMVAR
			
		ELSE
			* do a sql update
			
			*!*	m.updatedt = DATETIME()
			*!*	REPLACE upcmastsql.updatedt WITH m.updatedt, ;
			*!*		upcmastsql.STYLE WITH tempmast.STYLE, ;
			*!*		upcmastsql.COLOR WITH tempmast.COLOR, ;
			*!*		upcmastsql.ID WITH tempmast.ID, ;
			*!*		upcmastsql.DESCRIP WITH UPPER(tempmast.DESCRIP) IN upcmastsql
			
			m.updatedt = DATETIME()

			lcUpdateSQL = ;
				"update upcmast set " + ;
				"UPDATEDT={" + DTOC(m.updatedt) + "}," + ;
				"STYLE='" + ALLTRIM(tempmast.STYLE) + "'," + ;
				"COLOR='" + ALLTRIM(tempmast.COLOR) + "'," + ;
				"ID='" + ALLTRIM(tempmast.ID) + "'," + ;
				"DESCRIP='" + UPPER(ALLTRIM(tempmast.DESCRIP)) + "' " + ;
				"where upcmastid=" + TRANSFORM(upclookup.upcmastid) + " and accountid=" + TRANSFORM(lnAccountID)

			*.TrackProgress('lcUpdateSQL = ' + lcUpdateSQL, LOGIT+SENDIT)
				
			IF NOT lTesting THEN
				lnResult = xsqlexec(lcUpdateSQL,,,"wh")
			ENDIF
				
			NumUpdated = NumUpdated + 1
		ENDIF
	ENDSCAN

	tmessage = "S3 832 Stylemaster updated at: "+TTOC(DATETIME())+CHR(13)+;
		CHR(13)+lcAddedStr + TRANSFORM(NumAdded)+ CHR(13)+;
		CHR(13)+lcUpdateStr + TRANSFORM(NumUpdated)

	IF lTesting THEN
		SELECT upcmastsql
		BROW
	ELSE
		WAIT WINDOW AT 10,10 "Doing Table Update..." NOWAIT
		IF NOT tu("upcmastsql") THEN
			tmessage = "====> Error: table update of upcmastsql failed <====" + CHR(13)+ tmessage
		ENDIF
	ENDIF
	
	WAIT CLEAR

	tattach = ""
	tFrom ="fmi-transload-ops@fmiint.com"

	tSubject = "S3 832 Stylemaster Update"
	DO FORM m:\dev\frm\dartmail2 WITH tsendto,tFrom,tSubject,tcc,tattach,tmessage,"A"
	WAIT WINDOW "All 832 Files finished processing...exiting" TIMEOUT 2
	cLoadFile = (lcPath+cFilename)
	USE IN tempmast
	*Set Step On
	cArchiveFile = (lcArchivePath+cFilename)
	COPY FILE [&cLoadFile] TO [&cArchiveFile]

	IF !lTesting
		DELETE FILE [&cLoadFile]
	ENDIF
ENDFOR
CLOSE DATA ALL

schedupdate()
_SCREEN.CAPTION=gscreencaption
ON ERROR
