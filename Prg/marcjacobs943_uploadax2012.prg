*!* MARC JACOBS WHOLESALE 943 CREATION PROGRAM

Parameters thisoffice
Close Databases All
*!*	Clear All
*!*	Release All

Public lxfer943,cwhse,cfilename,cfilename2,tsendto,tcc,tsendtoerr,tccerr,liserror,norigacctnum,ndivloops,cinfolder,tsendtostyle,tccstyle
Public ldomail,tfrom,tattach,cxerrmsg,normalexit,nacctnum,acctname,coffice,lpick,lbrowfiles,m.filedata,xacct_ref,cwo_num,tsendtotest,tcctest
Public ax2012, inboundtype,choldfilepath,cwhseused
_setvars()

Do m:\dev\prg\lookups
ax2012 = .T.
inboundtype =""

If !Type("thisOffice")="C"
	Wait Window At 10,10 "Must pass an office "  Timeout 2
	Return
Else
	coffice = thisoffice
	goffice=coffice
	cmod = coffice
	gmasteroffice = Iif(coffice = "J","N",coffice)
Endif
cwhseused = Lower("wh"+cmod)

Store "" To tmessage,tsubject,tsendto,tcc,tattach,tsendtoerr,tccerr,tfrom

ltesting =.F. && If TRUE, disables certain functions
*_SCREEN.WINDOWSTATE =IIF(lTesting,2,1)
_Screen.Caption = "Marc Jacobs 943 Upload"

loverridebusy = ltesting
loverridebusy = .T.
lbrowfiles = ltesting
lbrowfiles = .F.
Do m:\dev\prg\_setvars With ltesting
On Error Catch


If ltesting
	Wait Window "This is a "+Iif(ltesting,"TEST","PRODUCTION")+" upload" Timeout 2
Endif

Try

	tfrom = "Toll EDI Operations <toll-edi-ops@tollgroup.com>"
	tattach = " "
*	cOffice = "J"  && Seed only

	liserror = .F.
	ldomail = .T.  && If .t., sends mail
	ltestmail = .F.
	normalexit = .F.
	lpick = .F.
	ndivloops = 0
	cmastercomments = ""
	xacct_ref = ""
	nacctnum = 6303
	cxerrmsg = ""
	cwo_num = ""
	ccustname = "MARCJACOBS"  && Customer Identifier (for folders, etc.)
	cmailname = "Marc Jacobs (W)"  && Proper name for eMail, etc.
	If coffice='L'
		ctransfer = "943-MARCJACOBSW-CA " && removed -ALL TM 01/29/15 for CA setup
	Endif
	If coffice='J'
		ctransfer = "943-MARCJACOBSW-ALL" && removed -ALL TM 01/29/15 for CA setup
	Endif
	Select 0
	Use F:\edirouting\ftpsetup
	Locate For ftpsetup.transfer = ctransfer
	If ftpsetup.chkbusy And !ltesting And !loverridebusy
		Wait Window "Process is busy...exiting" Timeout 2
		Close Data All
		Return
	Endif
	Replace chkbusy With .T.,trig_time With Datetime()  For transfer = ctransfer
	Use In ftpsetup
	xsqlexec("select * from account where inactive=0","account",,"qq")
	Index On accountid Tag accountid
	Set Order To

	Create Cursor tempupcmast (Style c(20),upc c(20))
	cupcmasthdr = Padr("STYLE",22)+"UPC"+Chr(13)

	If ltesting
		cinfolder = ("F:\FTPUSERS\MJ_Wholesale_Test\WMS_Inbound\943IN\")
	Else
		If coffice='L'
			cinfolder = ("F:\FTPUSERS\MJ_Wholesale_CA\WMS_Inbound\943IN\")
		Else
			cinfolder = ("F:\FTPUSERS\MJ_Wholesale\WMS_Inbound\943IN\")
		Endif
	Endif

	Use F:\3pl\Data\mailmaster In 0 Alias mm

	If  coffice='L'
		choldfilepath ="F:\FTPUSERS\MJ_WHOLESALE_CA\WMS_Inbound\943IN\hold\"
	Else
		choldfilepath ="F:\FTPUSERS\MJ_WHOLESALE\WMS_Inbound\943IN\hold\"
	Endif
	Cd [&cInFolder]
	Delete File (cinfolder+'*.txt')
	Delete File (cinfolder+'foxuser.*')
	If ltesting
		len1 = Adir(ary1,"*.*")
	Else
		len1 = Adir(ary1,"*.int")
	Endif
	If len1 = 0
		Wait Window "There are no MJW 943 files to process...exiting" Timeout 2
		Close Databases All

		normalexit = .T.

		Throw
	Endif
	Wait Window "There are "+Transform(len1)+" MJW 943 files to process" Timeout 2

	m.filedate = Date()
	Set Step On
	For kk = 1 To len1
		cfilename = Alltrim(ary1[kk,1])
		cconum = Left(cfilename,2)
		dfiledate = ary1[kk,3]
		cfilename2 = Juststem(cfilename)+"2.TXT"
		cstring = Filetostr(cfilename)
		If ltesting And Upper(Justext(cfilename)) = "TXT"
			cstring = Strtran(cstring,"~","")
		Else
			cstring = Strtran(cstring,"~",Chr(13))
		Endif
		If coffice='L'
			Strtofile(cstring,cfilename2)
			cmissupcfolder = "F:\FTPUSERS\MJ_Wholesale_CA\WMS_Inbound\943IN\missingupc\"
			cmissupcfile = (cmissupcfolder+cfilename)
			carchivefolder = "F:\FTPUSERS\MJ_Wholesale_CA\WMS_Inbound\943IN\Archive\"
			carchivefile = (carchivefolder+cfilename)
			setuprcvmail()

		Else
			Strtofile(cstring,cfilename2)
			cmissupcfolder = "F:\FTPUSERS\MJ_Wholesale\WMS_Inbound\943IN\missingupc\"
			cmissupcfile = (cmissupcfolder+cfilename)
			carchivefolder = "F:\FTPUSERS\MJ_Wholesale\WMS_Inbound\943IN\Archive\"
			carchivefile = (carchivefolder+cfilename)
			setuprcvmail()

		Endif

		Select 0
		Do m:\dev\prg\createx856a
		Select x856
		Append From [&cFilename2] Type Delimited With Character "*"
		Locate
		If !ltesting
			Locate For x856.segment = "GS" And x856.f1 = "AR"
			If !Found()
				cxerrmsg = cfilename+" is NOT a 943 document"
				Wait Window cxerrmsg Timeout 2
				errormail()
			Endif
		Endif

		Locate
		If ltesting
*      BROWSE
		Endif

		For uu = 1 To 1
			If ltesting
				cusefolder = "F:\WHP\WHDATA\"
			Else
				xreturn = "XXX"
				Do m:\dev\prg\wf_alt With coffice,nacctnum
				If xreturn = "XXX"
					cxerrmsg = "NO WHSE FOUND IN WMS"
					errormail()
					Return
				Endif
				cusefolder = Upper(xreturn)
			Endif
			If Used('mj943hist')
				Use In mj943hist
			Endif
*			If Used('whgenpk')
*				Use In whgenpk
*			Endif
			If Used('inwolog')
				Use In inwolog
			Endif
			If Used('pl')
				Use In pl
			Endif

			If !Used("mj_943_errors")
				Use F:\3pl\Data\mj_943_errors In 0
			Endif
*SET STEP ON
			If Used('whjgenpk')
				Use In whjgenpk
			Endif
*			If !Used('whgenpk')
*				Use (cusefolder+"WHGENPK") In 0 Alias whgenpk
*			Endif

			useca("inwolog","wh",,,"mod='"+goffice+"' and  accountid in (6303,6325,6543) and confirmdt>{"+Dtoc(Date()-2)+"}")

			useca("pl","wh")
			Select * From pl Where .F. Into Cursor xpl Readwrite
			Select pl
			Use F:\3pl\Data\mj943hist In 0 Alias mj943hist

			Select *, .F. As errorflag From inwolog Where .F. Into Cursor xinwolog Readwrite
			Select xinwolog

			Scatter Memvar Blank
			Select xpl
			Scatter Memvar Blank

			Store "FILE943*"+cfilename To m.comments
			Store m.comments+Chr(13)+"FILETIME*"+Ttoc(Datetime()) To m.comments
			Store 0 To m.inwologid,m.plid,npo
			Store "" To m.echo,m.po
			Store "FMI-PROC" To m.addby
			Store Datetime() To m.adddt
			Store "MJW943" To m.addproc
			Store Date() To m.wo_date,m.date_rcvd,m.proc_date

			Select x856
			cheaderline = Padr("Acct. Ref: ",17)+Padr("TGF WO#",12)+Padr("CTNS: ",7)+"UNITS: "
			csubheader = ""


			Scan
				Do Case
				Case Inlist(x856.segment,"ISA","GS","ST")
					Loop
				Case x856.segment = "W06"
					lluploaderrorflag = .F.
					m.inwologid = m.inwologid + 1
					m.container= Alltrim(x856.f2)
					m.reference = Alltrim(x856.f4)
					m.acct_ref = Alltrim(x856.f5)  && Shipment ID number
					Store m.acct_ref  To m.ship_num
					Store m.acct_ref  To cacct_ref
					Loop

				Case x856.segment = "N1" And x856.f1 = "SF"
					Loop
				Case x856.segment = "N1" And x856.f1 = "ST"
*SET STEP ON
*!*						If cOffice="J"
*!*							Do Case
*!*							Case Alltrim(x856.f4) ="1000" && AX2012 acct# for Wholsale
*!*								nAcctNum = 6303
*!*								m.acct_name = "MARC JACOBS WHOLESALE"
*!*							Case Alltrim(x856.f4) ="1200" && AX2012 acct# for Retail
*!*								nAcctNum = 6304
*!*								m.acct_name = "MARC JACOBS RETAIL"
*!*							Case Alltrim(x856.f4) ="1320" && AX2012 acct# for ECOMM
*!*								nAcctNum = 6305
*!*								m.acct_name = "MARC JACOBS ECOMM"
*!*							Case Alltrim(x856.f4) ="1310" && AX2012 acct# for Partner Stores
*!*								nAcctNum = 6320
*!*								m.acct_name = "MARC JACOBS PARTNER STORES"
*!*							Case Alltrim(x856.f4) ="1221" && AX2012 acct# for UK-Retail
*!*								nAcctNum = 6321
*!*								m.acct_name = "MARC JACOBS UK-RETAIL"
*!*							Case Alltrim(x856.f4) ="1222" && AX2012 acct# for FR-Retail
*!*								nAcctNum = 6322
*!*								m.acct_name = "MARC JACOBS FR-RETAIL"
*!*							Case Alltrim(x856.f4) ="1223" && AX2012 acct# for IT-Retail
*!*								nAcctNum = 6323
*!*								m.acct_name = "MARC JACOBS IT-RETAIL"
*!*							Case Alltrim(x856.f4) ="1250" && AX2012 acct# for Retail DMG
*!*								nAcctNum = 6324
*!*								m.acct_name = "MARC JACOBS RETAIL DMG"
*!*							Case Alltrim(x856.f4) ="1151" && AX2012 acct# for Wholesale DMG
*!*								nAcctNum = 6325
*!*								m.acct_name = "MARC JACOBS WHOLESALE DMG"
*!*							Case Alltrim(x856.f4) ="1400" && AX2012 acct# for Online sale
*!*								nAcctNum = 6364
*!*								m.acct_name = "MARC JACOBS ONLINE SALE"
*!*							Case Alltrim(x856.f4) ="1600" && AX2012 acct# for Press Samples
*!*								nAcctNum = 6543
*!*								m.acct_name = "MARC JACOBS PRESS SAMPLE"
*!*							Otherwise
*!*								Set Step On
*!*								cxErrMsg = "INVALID ACCOUNT NUMBER"
*!*								Throw
*!*							Endcase
*!*						Endif

*!*						If cOffice="L"
*!*							Do Case
*!*							Case Alltrim(x856.f4) ="1001" && AX2012 acct# for Wholsale
*!*								nAcctNum = 6303
*!*								m.acct_name = "MARC JACOBS WHOLESALE"
*!*							Case Alltrim(x856.f4) ="1206" && AX2012 acct# for Retail
*!*								nAcctNum = 6304
*!*								m.acct_name = "MARC JACOBS RETAIL"
*!*							Case Alltrim(x856.f4) ="1320" && AX2012 acct# for ECOMM
*!*								nAcctNum = 6305
*!*								m.acct_name = "MARC JACOBS ECOMM"
*!*							Case Alltrim(x856.f4) ="1310" && AX2012 acct# for Partner Stores
*!*								nAcctNum = 6320
*!*								m.acct_name = "MARC JACOBS PARTNER STORES"
*!*							Case Alltrim(x856.f4) ="1221" && AX2012 acct# for UK-Retail
*!*								nAcctNum = 6321
*!*								m.acct_name = "MARC JACOBS UK-RETAIL"
*!*							Case Alltrim(x856.f4) ="1222" && AX2012 acct# for FR-Retail
*!*								nAcctNum = 6322
*!*								m.acct_name = "MARC JACOBS FR-RETAIL"
*!*							Case Alltrim(x856.f4) ="1223" && AX2012 acct# for IT-Retail
*!*								nAcctNum = 6323
*!*								m.acct_name = "MARC JACOBS IT-RETAIL"
*!*							Case Alltrim(x856.f4) ="1252" && AX2012 acct# for Retail DMG
*!*								nAcctNum = 6324
*!*								m.acct_name = "MARC JACOBS RETAIL DMG"
*!*							Case Alltrim(x856.f4) ="1150" && AX2012 acct# for Wholesale DMG
*!*								nAcctNum = 6325
*!*								m.acct_name = "MARC JACOBS WHOLESALE DMG"
*!*							Case Alltrim(x856.f4) ="1400" && AX2012 acct# for Online sale
*!*								nAcctNum = 6364
*!*								m.acct_name = "MARC JACOBS ONLINE SALE"
*!*							Case Alltrim(x856.f4) ="1600" && AX2012 acct# for Press Samples
*!*								nAcctNum = 6543
*!*								m.acct_name = "MARC JACOBS PRESS SAMPLE"
*!*							Otherwise
*!*								Set Step On
*!*								cxErrMsg = "INVALID ACCOUNT NUMBER"
*!*								Throw
*!*							Endcase
*!*						Endif

					Set Step On

					If coffice="J"
						Do Case
						Case Alltrim(x856.f4) ="10100" && AX2012 acct# for Wholsale
							nacctnum = 6303
							m.acct_name = "MARC JACOBS WHOLESALE"
						Case Alltrim(x856.f4) ="10180" && AX2012 acct# for Wholesale DMG
							nacctnum = 6325
							m.acct_name = "MARC JACOBS WHOLESALE DMG"
						Case Alltrim(x856.f4) ="10190" && AX2012 acct# for Press Samples
							nacctnum = 6543
							m.acct_name = "MARC JACOBS PRESS SAMPLE"
						Otherwise
							cxerrmsg = "INVALID ACCOUNT NUMBER"
							Throw
						Endcase
					Endif

					If coffice="L"
						Do Case
						Case Alltrim(x856.f4) ="11100" && AX2012 acct# for Wholsale
							nacctnum = 6303
							m.acct_name = "MARC JACOBS WHOLESALE"
						Case Alltrim(x856.f4) ="11180" && AX2012 acct# for Wholesale DMG
							nacctnum = 6325
							m.acct_name = "MARC JACOBS WHOLESALE DMG"
						Case Alltrim(x856.f4) ="11190" && AX2012 acct# for Press Samples
							nacctnum = 6543
							m.acct_name = "MARC JACOBS PRESS SAMPLE"
						Otherwise
							cxerrmsg = "INVALID ACCOUNT NUMBER"
							Throw
						Endcase
					Endif

					m.accountid = nacctnum
					If Seek(nacctnum,'account','accountid')
						Scatter Fields account.acctname Memvar
					Endif
					Loop

				Case x856.segment = "N9" And x856.f1 = "ABS"
					m.reference = Alltrim(x856.f2)
					Loop
				Case x856.segment = "N9" And x856.f1 = "BM"
					m.brokerref =""
					m.container =""
					m.reference =""
					cbm = Alltrim(x856.f2)

					If "/"$cbm
						cbol = Left(cbm,At("/",cbm)-1)
						m.printcomments = "BOL*"+cbol
						m.brokerref = cbol

						cctr = Substr(cbm,At("/",cbm)+1)
						If Isalpha(cctr)
							Store cctr To m.container
							Store cbol To m.reference
						Else
							Store "" To m.container
							Store cctr To m.reference
						Endif
					Else
						Store cbm To m.brokerref
					Endif
					Loop
				Case x856.segment = "N9" And x856.f1 = "AW"
					caw = Substr(Alltrim(x856.f2),3)
					If  m.brokerref = "BOL"
						m.reference = caw
					Endif

				Case x856.segment = "N9" And x856.f1 = "VN"  &&Added to capture Vendor name in SEAL field  5/22/14 TM
					cvn = Substr(Alltrim(x856.f2),1,15)
					If  m.brokerref != "BOL"
						m.seal = cvn
					Endif

				Case x856.segment = "W27"
					If Empty(m.container)    && added 5/5/15 container data was getting wiped out by next command  TM
						m.container = Alltrim(x856.f6)+Alltrim(x856.f7)
					Endif

					Store m.container To ccontainer
					Insert Into xinwolog From Memvar
					Loop

				Case x856.segment = "W04"
					lluploaderrorflag = .F.
					m.totqty = Int(Val(x856.f1))
					Store m.totqty To ntotqty
					m.echo = "TOTQTY*"+Trans(ntotqty)
					lpick = Iif(Inlist(Alltrim(x856.f2),"PG","PK","EA"),.T.,.F.)
					m.units = lpick
					Store "1" To m.pack
					m.casepack = Int(Val(m.pack))
					m.echo = m.echo+Chr(13)+"PACKTYPE*"+Iif(lpick,"PIK","PRE")
					m.upc = Iif(!Empty(Alltrim(x856.f6)),Alltrim(x856.f7),"")
					If !Empty(m.upc)
						m.echo = m.echo+Chr(13)+"UPC*"+m.upc
					Endif
					m.style = Iif(!Empty(Alltrim(x856.f4)),Alltrim(x856.f5),"")

					llflip = .F.

********added 10/28/15 to capture all missing upc's  TM
					Select f7 From x856 Where segment='W04' Into Cursor tempupccheck Readwrite
					Select f5 From x856 Where segment ='W06' And f1='N' Into Cursor xshpid Readwrite

					If llflip=.F.  && not really a Flip but the 2 above conditions were not met so this is the fall through
						If !upcmastsql(6303,,,,m.upc)
							lluploaderrorflag = .T.
							lcupcerror = Alltrim(x856.f7)
							lcstyleerror = Alltrim(x856.f5)
							If File(cfilename)
								Copy File [&cFilename] To [&cArchivefile]
								If File(carchivefile) And !ltesting
									Delete File [&cFilename]
								Endif
							Endif
							cxerrmsg = "Missing Stylemaster Data for account:7 "+Str(nacctnum,4)+" for UPC: "+m.upc+" SID: "+upcmast.sid
&& insert upc complete check here
							missingupcmail()
							lholdmsg = .F.

							Select ftpsetup
							Update ftpsetup Set chkbusy=.F. Where transfer = '943-MARCJACOBSW'
							normalexit = .T.
							Throw
						Else
							m.echo = m.echo+Chr(13)+"ITEMSID*"+upcmast.sid
							m.echo = m.echo+Chr(13)+"STDUPC*"+upcmast.upc
						Endif
						inboundtype ="WHOLESALE"
					Endif

					Select x856
					cpo = Iif(!Empty(Alltrim(x856.f14)),Alltrim(x856.f15),"")
					m.container = ""
					If !Empty(cpo)
						Store cpo To m.po
						m.echo = m.echo+Chr(13)+"PO*"+cpo
					Endif

				Case x856.segment = "G69"
					m.echo = m.echo+Chr(13)+"DESC*"+Alltrim(x856.f1)
					Loop

				Case x856.segment = "N9"
					Do Case
					Case x856.f1 = "LI"  && Linenum
						cli = Alltrim(x856.f2)
						Store cli To m.linenum
						m.echo = m.echo+Chr(13)+"LINEID*"+cli

					Case x856.f1 = "D5"  && Linenum
						lccolorerror = Alltrim(x856.f2)
						If !llflip
							m.color =  Alltrim(x856.f2)
						Endif
					Case x856.f1 = "SZ"  && Inner cartons/polybags
						lciderror = Alltrim(x856.f2)
						If !llflip
							m.id      =  Alltrim(x856.f2)
						Endif

						If lluploaderrorflag = .T.
							Insert Into mj_943_errors (accountid,acct_ref,upc,Style,Color,Id,adddt,filename) Values ;
								(nacctnum,xinwolog.acct_ref,lcupcerror,lcstyleerror,lccolorerror,lciderror,Datetime(),cfilename)
						Endif

						Select mj943hist
						Locate For mj943hist.ship_num = m.ship_num And mj943hist.po = m.po ;
							and mj943hist.upc = m.upc And mj943hist.Style = m.style And mj943hist.Color = m.color And  mj943hist.Size = m.id ;
							and mj943hist.linenum = m.linenum
						If !Found()
							Insert Into mj943hist From Memvar
						Endif

						If ax2012
							m.echo = m.echo+Chr(13)+"SIZEBUCKET*"+Alltrim(x856.f2)+Chr(13)+Replicate("=",20)

							Insert Into xpl From Memvar

							If xpl.units
								Replace xinwolog.plunitsinqty With xinwolog.plunitsinqty+xpl.totqty In xinwolog
							Else
								Replace xinwolog.plinqty With xinwolog.plinqty+xpl.totqty In xinwolog
							Endif
						Endif


						Select x856

					Case x856.f1 = "SB"  && this is for the current 943 upload
						m.echo = m.echo+Chr(13)+"SIZEBUCKET*"+Alltrim(x856.f2)+Chr(13)+Replicate("=",20)
						Insert Into xpl From Memvar
						If xpl.units
							Replace xinwolog.plunitsinqty With xinwolog.plunitsinqty+xpl.totqty In xinwolog
						Else
							Replace xinwolog.plinqty With xinwolog.plinqty+xpl.totqty In xinwolog
						Endif

					Endcase

				Case x856.segment = "GE" Or (ltesting And Eof())
					If !Empty(cmastercomments)
						Replace xinwolog.printcomments With xinwolog.comments+Chr(13)+cmastercomments In xinwolog
					Endif
					Exit
				Endcase
			Endscan

			Select tempupcmast
			Locate
			If !Eof()
				Scan
					Scatter Memvar
					m.style = Alltrim(m.style)
					m.upc = Alltrim(m.upc)
					cupcmasthdr = cupcmasthdr+Padr(m.style,22)+m.upc+Chr(13)
				Endscan
				badupcmail(cupcmasthdr)
				normalexit = .T.
				Throw
			Endif

			If lbrowfiles
				Select xinwolog
				Locate
				Browse
				Select xpl
				Locate
				Browse
			Endif

			Select xinwolog
			Delete For errorflag = .T.
			Select * From xinwolog Into Cursor testwos

			nuploadcount = 0

			Select testwos
			Locate

			Scan
				niwrec = testwos.inwologid
				Wait Window "AT TESTWOS RECORD "+Alltrim(Str(niwrec)) Nowait
				xacct_num =  testwos.accountid
				xacct_ref = Alltrim(testwos.acct_ref)
				Select inwolog
				Locate
				Locate For inwolog.accountid = xacct_num And Alltrim(inwolog.acct_ref) = Alltrim(xacct_ref)
				If Found()
					cwo = Transform(inwolog.wo_num)
					Select xinwolog
					Locate
					Locate For Alltrim(xinwolog.acct_ref) = Alltrim(xacct_ref)
					If Found()
						Wait Window "This Acct. Ref., "+xacct_ref+", already exists in the INWOLOG table...skipping" Timeout 1
						ninwologid = xinwolog.inwologid
						Delete Next 1 In xinwolog
						Select xpl
						Delete For xpl.inwologid = ninwologid
						dupemail(cwo,xacct_ref)
					Else
						Wait Window "Acct. Ref "+xacct_ref+" not found in INWOLOG!" Timeout 20
					Endif
				Endif
				Select testwos
			Endscan

			Select xpl
			Locate
			Scan For !Deleted()
				nrec = Recno()
				Scatter Fields inwologid,Style,Color,Id Memvar Blank
				Scatter Fields inwologid,Style,Color,Id Memvar
				Locate For xpl.inwologid = m.inwologid And xpl.Style = m.style And xpl.Color = m.color And xpl.Id = m.id And Recno()#nrec
				If Found()
					Scatter Fields totqty,Echo Memo Memvar
					Delete Next 1 In xpl
					Go nrec
					Replace xpl.totqty With xpl.totqty+m.totqty In xpl
					Replace xpl.Echo With "ROLLED-UP LINES"+Chr(13)+Replicate("+",20)+Chr(13)+xpl.Echo+Chr(13)+m.echo In xpl
				Else
					Go nrec
				Endif
			Endscan

			Wait Window "Currently in "+Upper(Iif(ltesting,"test","production"))+" mode, base folder: "+cusefolder Timeout 2
			Select xinwolog
			Locate
*      BROWSE
			Select xpl
			Locate
*      BROWSE
			Set Step On
			Select xinwolog
			Count To nn For !Deleted()
			If nn > 0
				Scan  For !Deleted() && Scanning xinwolog here to create wolog records
					Scatter Memvar Memo
					ninwologid = m.inwologid
					Store m.acct_ref To cacct_ref
					cplctns = Alltrim(Str(m.plinqty))
					cplunits = Alltrim(Str(m.plunitsinqty))
*					Select whgenpk
*      ASSERT .F. MESSAGE "At WHGENPK point...debug"
*!*						Locate For gpk_pk="WONUM"
*!*						Replace gpk_currentnumber With gpk_currentnumber + 1 In whgenpk
*!*						nWO_Num = gpk_currentnumber
					nwo_num = dygenpk("WONUM",cwhseused)
					cwo_num = Alltrim(Str(nwo_num))

					If Empty(csubheader)
						csubheader = Padr(cacct_ref,17)+Padr(cwo_num,12)+Padr(cplctns,7)+cplunits
					Else
						csubheader = csubheader+Chr(13)+Padr(cacct_ref,17)+Padr(cwo_num,12)+Padr(cplctns,7)+cplunits
					Endif

					Wait Window "WO# is "+cwo_num Timeout 2
*!*						Locate For gpk_pk="INWOLOG"
*!*						Replace gpk_currentnumber With gpk_currentnumber + 1
*!*						m.inwologid = gpk_currentnumbe

*!*						m.accountid = nAcctNum   11/22/16 TMARG changed to next line to stop taking account from 2nd i/b wo
					m.accountid =xinwolog.accountid
					m.inwologid = dygenpk("INWOLOG",cwhseused)
					m.wo_num = nwo_num
					m.addby='FMI-PROC'
					m.addproc= 'MJW943'
					Set Step On   &&&&&todd
					If coffice='L'
						m.office='L'
						m.mod='L'
					Else
						m.office='N'
						m.mod='J'
					ENDIF

					insertinto("inwolog","wh",.T.)   

					nuploadcount = nuploadcount+1

					Select xpl && Scanning xplhere to create pl records
					Scan For inwologid=xinwolog.inwologid
						Scatter Fields Except inwologid Memvar Memo
						m.wo_num = nwo_num
						m.date_rcvd = Date()
*!*						m.accountid = nAcctNum   11/22/16 TMARG changed to next line to stop taking account from 2nd i/b wo
						m.accountid =inwolog.accountid
						m.inwologid = inwolog.inwologid
						If coffice='L'
							m.office='L'
							m.mod='L'
						Else
							m.office='N'
							m.mod='J'
						ENDIF
						insertinto("pl","wh",.T.)
					Endscan

					Select mj943hist
					Replace wo_num With inwolog.wo_num For Alltrim(mj943hist.ship_num) = Alltrim(inwolog.acct_ref) In mj943hist
				Endscan

				goodmail()
			Endif

			If nuploadcount = 0

				If File(cfilename)
					Copy File [&cFilename] To [&cArchivefile]
					If File(carchivefile) And !ltesting
						Delete File [&cFilename]
					Endif
				Endif
				Exit
			Endif
*			SET STEP ON
			Select inwolog
			tu("pl")
			tu('inwolog')

*			Select whgenpk
			cuploadcount = Alltrim(Str(nuploadcount))
			Wait Window "Marc Jacobs 943 file "+cfilename+" processed."+Chr(13)+"Header records loaded: "+cuploadcount Timeout 1
			If File(cfilename)
				Copy File [&cFilename] To [&cArchivefile]
				If File(carchivefile) And !ltesting
					Delete File [&cFilename]
				Endif
			Endif

			If ltesting
				Select inwolog
				Go Bottom
				Browse Timeout 5
				Select pl
				Go Bottom
				Browse Timeout 5
			Endif
		Endfor

	Endfor
	Wait Window "All Marc Jacobs 943s processed...exiting" Timeout 2

	Select 0
	Use F:\edirouting\ftpsetup
	Locate For ftpsetup.transfer = ctransfer
	Replace chkbusy With .F. For transfer = ctransfer
	Use In ftpsetup

	normalexit = .T.

Catch To oerr
	If normalexit = .F.

		Assert .F. Message "AT CATCH SECTION"




*!* PLEASE DO NOT COMMENT OUT THIS SNIPPET (Joe)
		lholdmsg = .F.
		If (cxerrmsg = "Missing UPC Stylemaster Data for8  " Or cxerrmsg = "INVALID ACCOUNT NUMBER")
			lholdmsg = .T.
			choldfile = (choldfilepath+cfilename)
			Copy File [&cFilename] To [&cHoldfile]
			If File(carchivefile) And !ltesting
				Delete File [&cFilename]
			Endif
		Endif

*!*	    Select mm
*!*	    Do Case
*!*	    Case InboundType = "RETAIL"
*!*	      Locate For Acctname = "MJ943 RETAIL ERRORS"
*!*	    Case InboundType = "WHOLESALE"
*!*	      Locate For Acctname = "MJ943 WHOLESALE ERRORS"
*!*	    Otherwise
*!*	    Endcase

*!*	    If Found()
*!*	      tsendto = mm.sendto
*!*	      tcc     = mm.cc
*!*	    Else
*!*	      Wait Window At 10,10 "Need to setup Mailmaster"
*!*	    Endif


		Select mm
		Locate For taskname = "TODDMAIL" And office = "X"
		tsubject = ccustname+" 943 Upload Error ("+Transform(oerr.ErrorNo)+") at "+Ttoc(Datetime())
		tsendto = mm.sendto
		tcc     = mm.cc
		tattach  = ""
*!*	    tsendto  = tsendtoerr
*!*	    tcc      = tccerr
*!*	    tmessage = cCustName+" 943 Upload Error..... Please fix me........!"
		lcsourcemachine = Sys(0)
		lcsourceprogram = Sys(16)

		If lholdmsg
			tmessage = tmessage+"File has been moved to the HOLD folder"
		Else
			tmessage =tmessage+ "Try/CATCH Exeception Message:"+Chr(13)+;
				[  Error: ] + Str(oerr.ErrorNo) +Chr(13)+;
				[  Procedure: ] + oerr.Procedure +Chr(13)+;
				[  LineNo: ] + Str(oerr.Lineno) +Chr(13)+;
				[  Message: ] + oerr.Message +Chr(13)+;
				[  Details: ] + oerr.Details +Chr(13)+;
				[  StackLevel: ] + Str(oerr.StackLevel) +Chr(13)+;
				[  LineContents: ] + oerr.LineContents+Chr(13)+;
				[  UserValue: ] + oerr.UserValue+Chr(13)+;
				[  Computer:  ] +lcsourcemachine+Chr(13)+;
				[  940 file:  ] +cfilename+Chr(13)+;
				[  Program:   ] +lcsourceprogram
		Endif

		tmessage = tmessage + Chr(13) + Chr(13) + cxerrmsg
		tattach  = ""
		tfrom    ="TGF EDI Processing Center <toll-edi-ops@fmiint.com>"
		Do Form m:\dev\frm\dartmail2 With tsendto,tfrom,tsubject,tcc,tattach,tmessage,"A"
	Endif
Finally
	Delete File (cinfolder+'*.txt')
	Delete File (cinfolder+'foxuser.*')
	Close Databases All
	On Error
Endtry


****************************
Procedure goodmail
****************************
*	SET STEP ON
tsubject = cmailname+" EDI FILE (943) Processed, Inv. WO"+cwo_num
tmessage = Replicate("-",90)+Chr(13)
tmessage = tmessage+"Packinglist upload run from machine: "+Sys(0)+Chr(13)
tmessage = tmessage+Replicate("-",90)+Chr(13)
tmessage = tmessage+"Inbound Workorders created for "+m.acctname+":"+Chr(13)
tmessage = tmessage+"Data from File : "+cfilename+Chr(13)
tmessage = tmessage+Replicate("-",90)+Chr(13)
tmessage = tmessage+cheaderline+Chr(13)
tmessage = tmessage+csubheader

If ltesting

Endif

If ldomail
	Do Form m:\dev\frm\dartmail2 With tsendto,tfrom,tsubject,tcc,tattach,tmessage,"A"
Endif
Endproc

****************************
Procedure dupemail
****************************
Parameters cwo,xacct_ref
Set Step On
tsubject = cmailname+" EDI FILE (943): Duplicate"
tmessage = Replicate("-",90)+Chr(13)
tmessage = tmessage+"Data from File : "+cfilename+Chr(13)
tmessage = tmessage+ "For Marc Jacobs Acct. Reference/Ship No. "+xacct_ref+Chr(13)
tmessage = tmessage+"already exists in the Toll Inbound WO table, in Work Order "+cwo
tmessage = tmessage+Chr(13)+Replicate("-",90)+Chr(13)

If ltesting

Endif

If ldomail
	Do Form m:\dev\frm\dartmail2 With tsendto,tfrom,tsubject,tcc,tattach,tmessage,"A"
Endif
Endproc

****************************
Procedure errormail
****************************
tsubject = cmailname+" EDI ERROR (943)"
tsendto = tsendtoerr
tcc = tccerr
tmessage = "File: "+cfilename
tmessage = tmessage+Chr(13)+cxerrmsg

Do Form m:\dev\frm\dartmail2 With tsendto,tfrom,tsubject,tcc,tattach,tmessage,"A"
normalexit = .T.

Throw
Endproc

****************************
Procedure badupcmail
****************************
Set Step On

upcmastsql(6303)

Select * From tempupccheck a Left Join upcmast b On a.f7=b.upc And b.accountid=6303 Into Cursor t1 Readwrite
Select f7 As upc From t1 Where Isnull(upc) Into Cursor t2 Readwrite
Export To "S:\MarcJacobsData\TEMP\missing_upc1"  Type Xls
Select f5 As shipmentid From xshpid Into Cursor s1 Readwrite
Export To "S:\MarcJacobsData\TEMP\SHIPID"  Type Xls
tsubject3 = cmailname+" Style Master Error (943)9  "
tsendto3 = Iif(ltesting,tsendtotest,tsendtoerr)
tcc3 = Iif(ltesting,tcctest,tccstyle)
*	tmessage3 = "The following Style/UPC combos do not exist in Toll's Style Master for Marc Jacobs Wholesale:"+CHR(13)+CHR(13)+cUPCMast
tmessage3 = "Error as follows: "+cxerrmsg
tmessage3 = tmessage3+Chr(13)+Chr(13)+"File: "+cfilename
tattach3 = "S:\MarcJacobsData\TEMP\missing_upc1.xls,S:\MarcJacobsData\TEMP\SHIPID.xls"
Do Form m:\dev\frm\dartmail2 With tsendto3,tfrom,tsubject3,tcc3,tattach3,tmessage3,"A"
cupcmast = ""
Delete Next 1 In xinwolog
Delete Next 1 In inwolog
*	SET STEP ON

*!*	SELECT * FROM XSHPID INTO CURSOR S1 READWRITE
*!*	export TO "S:\MarcJacobsData\TEMP\SHIPID"  TYPE xls
*!*	tsubject6 = cMailName+" Shipmentid_hung_up  "
*!*	tsendto6 = Iif(lTesting,tsendtotest,tsendtoerr)
*!*	tcc6 = Iif(lTesting,tcctest,tccstyle)
*!*	tmessage6 = cxErrMsg
*!*	tmessage6 = tmessage6+Chr(13)+"File: "+cFilename
*!*	tattach6 = "S:\MarcJacobsData\TEMP\SHIPID.xls"
*!*	Do Form m:\dev\frm\dartmail2 With tsendto6,tfrom,tsubject6,tcc6,tattach6,tmessage6,"A"

Copy File [&cArchiveFile] To [&cMissUPCfile]
Delete File [&cFilename]
Use F:\edirouting\ftpsetup
Replace chkbusy With .F. For  transfer ='943-MARCJACOBSW-ALL'
Endproc

****************************
Procedure missingupcmail
****************************
Set Step On

upcmastsql(6303)
**tsendtoerr=mm.sendto+mm.cc
Select * From tempupccheck a Left Join upcmast b On a.f7=b.upc And b.accountid=6303 Into Cursor t1 Readwrite
Select  f7 As upc From t1 Where Isnull(upc) Into Cursor t2 Readwrite
Export To "S:\MarcJacobsData\TEMP\missing_upc1"  Type Xls
Select f5 As shipmentid  From xshpid Into Cursor s1 Readwrite
Export To "S:\MarcJacobsData\TEMP\SHIPID"  Type Xls
tsubject4 = cmailname+" Miss UPC (943)11  "
tsendto4 = Iif(ltesting,tsendtotest,tsendtoerr)
tcc4 = Iif(ltesting,tcctest,tccstyle)
tmessage4 = cxerrmsg
tmessage4 = tmessage4+Chr(13)+"File: "+cfilename
tmessage4 = tmessage4+Chr(13)+"File has been moved to folder F:\FTPUSERS\MJ_Wholesale\WMS_Inbound\943IN\missingupc"
tattach4 = "S:\MarcJacobsData\TEMP\missing_upc1.xls,S:\MarcJacobsData\TEMP\SHIPID.xls"
Set Step On
Do Form m:\dev\frm\dartmail2 With tsendto4,tfrom,tsubject4,tcc4,tattach4,tmessage4,"A"
cxerrmsg = ""
Delete Next 1 In xinwolog
Delete Next 1 In inwolog
Set Step On



Copy File [&cArchiveFile] To [&cMissUPCfile]
Delete File [&cFilename]
Use F:\edirouting\ftpsetup
Replace chkbusy With .F. For  transfer ='943-MARCJACOBSW-ALL'
Endproc

****************************
Procedure setuprcvmail
****************************
*  ASSERT .f. MESSAGE "In Mail/Folder selection...debug"
Select mm

Locate For mm.edi_type = "943" And mm.Group = "MARCJACOBS" And mm.office = coffice
If !Found()
	cxerrmsg = "Office/Loc/Acct not in Mailmaster!"
	errormail()
Endif
Store Trim(mm.archpath) To carchivepath
lusealt = mm.use_alt
Store Iif(lusealt,Trim(mm.sendtoalt),Trim(mm.sendto)) To tsendto
Store Iif(lusealt,Trim(mm.ccalt),Trim(mm.cc)) To tcc
Locate
Locate For mm.edi_type = "MISC" And accountid = 6303 And mm.taskname = "STYLEMASTER"
lusealt = mm.use_alt
Store Iif(lusealt,Trim(mm.sendtoalt),Trim(mm.sendto)) To tsendtostyle
Store Iif(lusealt,Trim(mm.ccalt),Trim(mm.cc)) To tccstyle
Locate
Locate For mm.edi_type = "MISC" And mm.taskname = "TODDMAIL"
lusealt = mm.use_alt
Store Iif(lusealt,Trim(mm.sendtoalt),Trim(mm.sendto)) To tsendtotest
Store Iif(lusealt,Trim(mm.ccalt),Trim(mm.cc)) To tcctest
Store Iif(lusealt,Trim(mm.sendtoalt),Trim(mm.sendto)) To tsendtoerr
Store Iif(lusealt,Trim(mm.ccalt),Trim(mm.cc)) To tccerr
If ltesting Or ltestmail
	tsendto = tsendtotest
	tcc = tcctest
Endif
*Use In mm
carchivefile  = (carchivepath+cfilename)
Endproc
