parameters nacctnum,ccustname,cppname,lucc,coffice,lsql3,lmoret,ldocartons,ldoscanpack
if vartype(coffice) # "C"
	coffice = "C"
endif
csql = "tgfnjsql01"
csqlpass = ""

select 0
use f:\wh\sqlpassword
csqlpass = alltrim(sqlpassword.password)
use in sqlpassword

set escape on
on escape cancel

cfileoutname = "V"+trim(cppname)+"pp"
nacct = alltrim(str(nacctnum))
if used("temp1mj")
	use in temp1mj
endif

if datetime()<datetime(2018,03,28,21,30,00)
	set step on
endif

lcdsnless="driver=SQL Server;server=tgfnjsql01;uid=SA;pwd=&cSQLPass;DATABASE=PICKPACK"

*lcDSNLess="driver=SQL Server;server=sql5;uid=SA;pwd=&cSQLPass;DATABASE=PICKPACK"

nhandle=sqlstringconnect(m.lcdsnless,.t.)
sqlsetprop(nhandle,"DispLogin",3)
wait window "Now processing "+ccustname+" SQL view...please wait" nowait
if nhandle<1 && bailout
	wait window "Could not make SQL connection" timeout 3
	cretmsg = "NO SQL CONNECT"
	return cretmsg
	throw
endif

lappend = .f.

select sqlwomj
*!* Scans through all WOs within the OUTSHIP BOL#
scan
	nwo_num = sqlwomj.wo_num
	nwo   = alltrim(str(nwo_num))

*!* Changed select to drop accountid requirement, 12.30.2005
	if ltestinput
		wait window "SQL Records will be selected from LABELS" timeout 2
		lcq1=[SELECT * FROM dbo.labels Labels]
		lcq2 = [ WHERE Labels.wo_num = ]
		lcq3 = " &nWo "
		if lucc
			lcq6 = [order by Labels.ship_ref,Labels.outshipid,Labels.ucc]
		else
			lcq6 = [order by Labels.ship_ref,Labels.outshipid,Labels.CartonNum]
		endif
		lcsql = lcq1+lcq2+lcq3+lcq6

	else
		if usesql()
			if lucc
				xorderby="order by ship_ref, outshipid, ucc"
			else
				xorderby="order by ship_ref, outshipid, cartonnum"
			endif
			
			xsqlexec("select * from cartons where wo_num="+nwo+" and ucc<>'CUTS' and !empty(cartonnum) "+xorderby,cFileOutName,,"pickpack")

		else
			lcq1 = [SELECT * FROM dbo.cartons Cartons]
			lcq2 = [ WHERE Cartons.wo_num = ]
			lcq3 = " &nWo "
			lcq4 = [ AND  Cartons.ucc <> ]
			lcq5 = " 'CUTS' "
			lcq6 = [ and cartons.CARTONNUM <>' ' ] && added to eliminate blank cartonum data from being added to 945  TM 12/8/14
			if lucc
				lcq7 = [order by Cartons.ship_ref,Cartons.outshipid,Cartons.ucc]
			else
				lcq7 =[order by Cartons.ship_ref,Cartons.outshipid,Cartons.CartonNum]
			endif

			lcsql = lcq1+lcq2+lcq3+lcq4+lcq5+lcq6+lcq7
		endif
	endif

	if usesql()
	else
		llsuccess=sqlexec(nhandle,lcsql,cfileoutname)

		if llsuccess=0  && no records 0 indicates no records and 1 indicates records found
			assert .f. message "At NO RECORDS statement in SQLConnect"
			wait window "No "+ccustname+" records found" timeout 2
			cretmsg = "NO SQL CONNECT"
			return cretmsg
		endif
		sqlcancel(nhandle)
	endif
	
	if lappend = .f.
		lappend = .t.
		select &cfileoutname
		if ltesting
*!*			ASSERT .f. MESSAGE "In SQLConnect, browsing 'V' file"
*!*			BROWSE
		endif
		copy to ("F:\3pl\DATA\temp1mj")
		use ("F:\3pl\DATA\temp1mj") in 0 alias temp1mj
	else
		select &cfileoutname
		scan
			scatter memvar
			insert into temp1mj from memvar
		endscan
	endif
endscan
if used(cfileoutname)
	use in &cfileoutname
endif

select temp1mj
locate

if lucc
	select * from temp1mj ;
	order by ship_ref,outshipid,ucc ;
	into cursor &cfileoutname readwrite
else
	select * from temp1mj ;
	order by ship_ref,outshipid,cartonnum ;
	into cursor &cfileoutname readwrite
endif
if ltesting
	select &cfileoutname
*BROWSE
endif

use in temp1mj
use in sqlwomj
go bott
wait clear
*WAIT "" TIMEOUT 3
sqldisconnect(nhandle)

cretmsg = "OK"
return cretmsg
