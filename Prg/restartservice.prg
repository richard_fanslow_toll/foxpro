


* Detect if a service has stopped
* if stopped, try and restart it.
* Fo use with malvern services.
* Build EXE in F:\UTIL\RESTARTSERVICE\

LPARAMETERS tcServiceName

LOCAL loRESTARTSERVICE, llTestMode

llTestMode = .F.

*!*	IF NOT llTestMode THEN
*!*		utilsetup("RESTARTSERVICE")
*!*	ENDIF


loRESTARTSERVICE = CREATEOBJECT('RESTARTSERVICE')

loRESTARTSERVICE.MAIN( llTestMode, tcServiceName )


*!*	IF NOT llTestMode THEN
*!*		schedupdate()
*!*	ENDIF

RETURN

#DEFINE LOGIT 1
#DEFINE WAITIT 2
#DEFINE NOWAITIT 4
#DEFINE SENDIT 8
#DEFINE CR CHR(13)
#DEFINE LF CHR(10)
#DEFINE CRLF CHR(13) + CHR(10)

#define SC_MANAGER_CONNECT             0x0001
#define SC_MANAGER_CREATE_SERVICE      0x0002
#define SC_MANAGER_ENUMERATE_SERVICE   0x0004
#define SC_MANAGER_LOCK                0x0008
#define SC_MANAGER_QUERY_LOCK_STATUS   0x0010
#define SC_MANAGER_MODIFY_BOOT_CONFIG  0x0020

* Service object specific access type
#define SERVICE_QUERY_CONFIG           0x0001
#define SERVICE_CHANGE_CONFIG          0x0002
#define SERVICE_QUERY_STATUS           0x0004
#define SERVICE_ENUMERATE_DEPENDENTS   0x0008
#define SERVICE_START                  0x0010
#define SERVICE_STOP                   0x0020
#define SERVICE_PAUSE_CONTINUE         0x0040
#define SERVICE_INTERROGATE            0x0080
#define SERVICE_USER_DEFINED_CONTROL   0x0100

* Service State -- for CurrentState
#define SERVICE_STOPPED                0x00000001
#define SERVICE_START_PENDING          0x00000002
#define SERVICE_STOP_PENDING           0x00000003
#define SERVICE_RUNNING                0x00000004
#define SERVICE_CONTINUE_PENDING       0x00000005
#define SERVICE_PAUSE_PENDING          0x00000006
#define SERVICE_PAUSED                 0x00000007

DEFINE CLASS RESTARTSERVICE AS CUSTOM

	cProcessName = 'RESTARTSERVICE'

	lTestMode = .F.  && .F. FOR NORMAL OPERATION, .T. TO SUPPRESS ALL BUT DEVELOPER EMAILS AND CHANGE INPUT TABLE, ETC.

	lAutoYield = .F.

	* date time props
	dToday = DATE()
	*dToday = {^2011-11-11}

	cStartTime = TTOC(DATETIME())

	* file/folder properties

	* data properties
	
	* processing properties
	cServiceName = ''

	* wait window properties
	nWaitWindowTimeout = 2
	lWaitWindowIsOn = .T.

	* logfile properties
	lLoggingIsOn = .T.
	cLogFile = ''

	nSQLHandle = 0

	* INTERNAL email properties
	lSendInternalEmailIsOn = .T.
	cFrom = ''
	cSendTo = 'mbennett@fmiint.com'
	cCC = ''
	cSubject = 'Restart Service Process Results for ' + TTOC(DATETIME())
	cAttach = ''
	cBodyText = ''
	cTopBodyText = ''

	cCOMPUTERNAME = ''
	cUSERNAME = ''


	FUNCTION INIT
		IF NOT DODEFAULT()
			RETURN .F.
		ENDIF
		WITH THIS
			*SET RESOURCE OFF
			SET CENTURY ON
			SET DATE MDY
			SET HOURS TO 24
			CLOSE DATA
			SET DECIMAL TO 0
			SET TALK OFF
			SET DELETED ON
			SET CONSOLE OFF
			SET EXCLUSIVE OFF
			SET SAFETY OFF
			SET EXACT OFF
			SET STATUS BAR ON
			SET SYSMENU OFF
			SET ENGINEBEHAVIOR 70
			_VFP.AUTOYIELD = .lAutoYield
			.cCOMPUTERNAME = GETENV("COMPUTERNAME")
			.cUSERNAME = GETENV("USERNAME")
		ENDWITH
	ENDFUNC


	FUNCTION DESTROY
		WITH THIS
			IF .lLoggingIsOn  THEN
				SET ALTERNATE OFF
				SET ALTERNATE TO
			ENDIF
		ENDWITH
		DODEFAULT()
	ENDFUNC


	FUNCTION MAIN
		LPARAMETERS tlTestMode, tcServiceName
		WITH THIS

			.lTestMode = tlTestMode

			TRY
				LOCAL lnNumberOfErrors				
				LOCAL lhSCManager, lcServiceName, lhSChandle, lcQueryBuffer, lnRetVal, lcCmd

				lnNumberOfErrors = 0

				IF .lTestMode THEN
					.cLogFile = 'F:\UTIL\RESTARTSERVICE\LOGFILES\RESTARTSERVICE_LOG_TESTMODE_' + ALLTRIM(.cCOMPUTERNAME) + '.TXT'
					.cFrom = 'fmicorporate@fmiint.com'
					.cSendTo = 'mbennett@fmiint.com'
					.cCC = ''
				ELSE
					.cLogFile = 'F:\UTIL\RESTARTSERVICE\LOGFILES\RESTARTSERVICE_LOG_' + ALLTRIM(.cCOMPUTERNAME) + '.TXT'
					.cFrom = 'fmicorporate@fmiint.com'
					.cSendTo = 'mbennett@fmiint.com'
					.cCC = ''
				ENDIF
				.lLoggingIsOn = .lLoggingIsOn AND NOT EMPTY(.cLogFile)

				IF .lLoggingIsOn THEN
					SET ALTERNATE TO (.cLogFile) ADDITIVE
					SET ALTERNATE ON
				ENDIF
				
				.TrackProgress('Restart Service Process started....', LOGIT+SENDIT)
				IF .lTestMode THEN
					.TrackProgress('TEST MODE', LOGIT+SENDIT)
				ENDIF
				.TrackProgress('COMPUTERNAME = ' + .cCOMPUTERNAME, LOGIT+SENDIT)
				.TrackProgress('USERNAME = ' + .cUSERNAME, LOGIT+SENDIT)
				.TrackProgress('PROJECT = RESTARTSERVICE', LOGIT+SENDIT)
				
				IF TYPE('tcServiceName') = 'C' THEN
					.cServiceName = ALLTRIM(tcServiceName)
					.TrackProgress('Service Name = ' + .cServiceName, LOGIT+SENDIT)
				ELSE
					.TrackProgress('ERROR: missing or invalid Service Name parameter!', LOGIT+SENDIT)
					THROW
				ENDIF

				
				DECLARE Long OpenSCManager IN Advapi32 STRING lpMachineName, STRING lpDatabaseName, Long dwDesiredAccess
				DECLARE Long OpenService IN Advapi32 Long hSCManager, String lpServiceName, Long dwDesiredAccess
				DECLARE Long QueryServiceStatus IN Advapi32 Long hService, String @ lpServiceStatus
				DECLARE Long CloseServiceHandle  IN Advapi32 Long hSCObject
				
				lhSCManager = OpenSCManager(0, 0, SC_MANAGER_CONNECT + SC_MANAGER_ENUMERATE_SERVICE)
				
				IF lhSCManager = 0 THEN
					* Error
					.TrackProgress('ERROR: unable to get a handle to Service Control Manager!', LOGIT+SENDIT+WAITIT)
					THROW
				ENDIF
				
				*lcServiceName = "MSSQLSERVER"
				lcServiceName = .cServiceName
				
				lhSChandle = OpenService(lhSCManager, lcServiceName, SERVICE_QUERY_STATUS)
				
				IF lhSChandle = 0 THEN
					* Error
					.TrackProgress('ERROR: unable to open Service ' + .cServiceName, LOGIT+SENDIT+WAITIT)
					THROW
				ENDIF
				
				lcQueryBuffer = REPLICATE(CHR(0), 4*7 )
				lnRetVal = QueryServiceStatus(lhSChandle, @lcQueryBuffer )
				
				IF lnRetVal = 0
					* Error
					.TrackProgress('ERROR: lnRetVal = 0 calling QueryServiceStatus()!', LOGIT+SENDIT+WAITIT)
					THROW
				ENDIF
				
				* Close Handles
				CloseServiceHandle(lhSChandle)  
				CloseServiceHandle(lhSCManager) 
				 
				lnServiceStatus = ASC(SUBSTR(lcQueryBuffer,5,1))
				
				.TrackProgress('lcQueryBuffer = ' + lcQueryBuffer, LOGIT+SENDIT+WAITIT)
				.TrackProgress('lnServiceStatus = ' + TRANSFORM(lnServiceStatus), LOGIT+SENDIT+WAITIT)
				
				IF lnServiceStatus = SERVICE_RUNNING
					* Service is running
					.TrackProgress(.cServiceName + ' service is running :)', LOGIT+SENDIT+WAITIT)
					* set email off
					.lSendInternalEmailIsOn = .F.
				ELSE
					LOCAL lcMsg1, lcMsg2
				
					* Service isn't running
					lcMsg1 = .cServiceName + ' service is not running :('
					.TrackProgress(lcMsg1, LOGIT+SENDIT+WAITIT)
					.cSubject = 'Attempted to Restart Service [' + .cServiceName + '] on ' + .cCOMPUTERNAME + ' at ' + TTOC(DATETIME())	
									
					* try to restart it
					lcCmd = "net start " + .cServiceName
					lcMsg2 = 'Attempting to restart service: ' + .cServiceName + ', command = [' + lcCmd + ']'					
					.TrackProgress(lcMsg2, LOGIT+SENDIT+WAITIT)
					
					.cTopBodyText = lcMsg1 + CRLF + CRLF + lcMsg2 + CRLF + CRLF + "Refresh the Services Window to see current service status!"
					
					RUN &lcCMD
					
				ENDIF				
				

				.TrackProgress('Restart Service Process ended normally!',LOGIT+SENDIT)

			CATCH TO loError

				.lSendInternalEmailIsOn = .T.
				.TrackProgress('There was an error.',LOGIT+SENDIT)
				.TrackProgress('The Error # is: ' + TRANSFORM(loError.ERRORNO), LOGIT+SENDIT)
				.TrackProgress('The Error Message is: ' + TRANSFORM(loError.MESSAGE), LOGIT+SENDIT)
				.TrackProgress('The Error occurred in Program: ' + TRANSFORM(loError.PROCEDURE), LOGIT+SENDIT)
				.TrackProgress('The Error occurred at line #: ' + TRANSFORM(loError.LINENO), LOGIT+SENDIT)
				lnNumberOfErrors = lnNumberOfErrors + 1
				CLOSE DATA

			ENDTRY

			CLOSE DATABASES ALL

			WAIT CLEAR
			***************** INTERNAL email results ******************************
			.TrackProgress('About to send status email.',LOGIT)
			.TrackProgress('The logfile is: ' + .cLogFile, SENDIT)
			.TrackProgress('Restart Service Process started: ' + .cStartTime, LOGIT+SENDIT)
			.TrackProgress('Restart Service Process finished: ' + TTOC(DATETIME()), LOGIT+SENDIT)
			
			IF NOT EMPTY(.cTopBodyText) THEN
				.cBodyText = .cTopBodyText + CRLF + CRLF + "<< processing log follows >>" + CRLF + CRLF + .cBodyText
			ENDIF

			IF .lSendInternalEmailIsOn THEN
				DO FORM dartmail2 WITH .cSendTo,.cFrom,.cSubject,.cCC,.cAttach,.cBodyText,"A"
				.TrackProgress('Sent status email.',LOGIT)
			ELSE
				.TrackProgress('Did not send status email: THIS.lSendInternalEmailIsOn = FALSE.',LOGIT)
			ENDIF

		ENDWITH
		RETURN
	ENDFUNC && main


	PROCEDURE TrackProgress
		* do any combination of Wait Window, writing to logfile, and adding to body of email,
		* based on nFlags parameter.
		LPARAMETERS tcExpression, tnFlags
		WITH THIS
			IF BITAND(tnFlags,LOGIT) = LOGIT THEN
				IF .lLoggingIsOn THEN
					?
					? .cProcessName + "  " + TTOC(DATETIME()) + ": " + tcExpression
				ENDIF
			ENDIF
			IF .lWaitWindowIsOn THEN
				IF BITAND(tnFlags,NOWAITIT) = NOWAITIT THEN
					WAIT WINDOW tcExpression NOWAIT
				ENDIF
				IF BITAND(tnFlags,WAITIT) = WAITIT THEN
					WAIT WINDOW tcExpression TIMEOUT .nWaitWindowTimeout
				ENDIF
			ENDIF
			IF BITAND(tnFlags,SENDIT) = SENDIT THEN
				.cBodyText = .cBodyText + tcExpression + CRLF + CRLF
			ENDIF
		ENDWITH
	ENDPROC  &&  TrackProgress

ENDDEFINE


*!*	*** ***********
*!*	* from scm1.prg
*!*	* Service Control Manager object specific access types
*!*	#define SC_MANAGER_CONNECT             0x0001
*!*	#define SC_MANAGER_CREATE_SERVICE      0x0002
*!*	#define SC_MANAGER_ENUMERATE_SERVICE   0x0004
*!*	#define SC_MANAGER_LOCK                0x0008
*!*	#define SC_MANAGER_QUERY_LOCK_STATUS   0x0010
*!*	#define SC_MANAGER_MODIFY_BOOT_CONFIG  0x0020

*!*	* Service object specific access type
*!*	#define SERVICE_QUERY_CONFIG           0x0001
*!*	#define SERVICE_CHANGE_CONFIG          0x0002
*!*	#define SERVICE_QUERY_STATUS           0x0004
*!*	#define SERVICE_ENUMERATE_DEPENDENTS   0x0008
*!*	#define SERVICE_START                  0x0010
*!*	#define SERVICE_STOP                   0x0020
*!*	#define SERVICE_PAUSE_CONTINUE         0x0040
*!*	#define SERVICE_INTERROGATE            0x0080
*!*	#define SERVICE_USER_DEFINED_CONTROL   0x0100

*!*	* Service State -- for CurrentState
*!*	#define SERVICE_STOPPED                0x00000001
*!*	#define SERVICE_START_PENDING          0x00000002
*!*	#define SERVICE_STOP_PENDING           0x00000003
*!*	#define SERVICE_RUNNING                0x00000004
*!*	#define SERVICE_CONTINUE_PENDING       0x00000005
*!*	#define SERVICE_PAUSE_PENDING          0x00000006
*!*	#define SERVICE_PAUSED                 0x00000007

*!*	DECLARE Long OpenSCManager IN Advapi32 ;
*!*		  STRING lpMachineName, STRING lpDatabaseName, Long dwDesiredAccess
*!*	DECLARE Long OpenService IN Advapi32 ;
*!*		  Long hSCManager, String lpServiceName, Long dwDesiredAccess
*!*	DECLARE Long QueryServiceStatus IN Advapi32 ;
*!*		  Long hService, String @ lpServiceStatus
*!*	DECLARE Long CloseServiceHandle  IN Advapi32 ;
*!*		  Long hSCObject

*!*	lhSCManager = OpenSCManager(0, 0, SC_MANAGER_CONNECT + SC_MANAGER_ENUMERATE_SERVICE)
*!*	IF lhSCManager = 0
*!*		* Error
*!*	ENDIF
*!*	lcServiceName = "MSSQLSERVER"
*!*	lhSChandle = OpenService(lhSCManager, lcServiceName, SERVICE_QUERY_STATUS)
*!*	IF lhSCManager = 0
*!*		* Error
*!*	ENDIF
*!*	lcQueryBuffer = REPLICATE(CHR(0), 4*7 )
*!*	lnRetVal = QueryServiceStatus(lhSChandle, @lcQueryBuffer )
*!*	IF lnRetVal = 0
*!*		* Error
*!*	ENDIF
*!*	* Close Handles
*!*	CloseServiceHandle(lhSChandle)  
*!*	CloseServiceHandle(lhSCManager)  
*!*	lnServiceStatus = ASC(SUBSTR(lcQueryBuffer,5,1))
*!*	IF lnServiceStatus <> SERVICE_RUNNING
*!*		* Service isn't running
*!*		? "Service isn't running"
*!*	ENDIF	
*!*	*****************************************************************
*!*	* from scm2.prg
*!*	  
*!*	View message   
*!*	Message view setup 
*!*	 
*!*	    
*!*	 
*!*	  From 04/02/2002 12:38:05 
*!*	Jens Erik Pettersen   #030213 
*!*	Lexi As
*!*	Sarpsborg, Norway   
*!*	 
*!*	 
*!*	   To 04/02/2002 07:17:51 
*!*	Will Jones   #030936 
*!*	Vevey, Switzerland   
*!*	 
*!*	 
*!*	 
*!*	 

*!*	Forum:  Visual FoxPro  
*!*	  Thread ID:   639852  
*!*	Category:  COM/DCOM and OLE Automation  
*!*	  Message ID:   639994  
*!*	Title:  Re: Running .exe as a service  
*!*	  Viewed:   4    
*!*	  

*!*	Will,

*!*	I also had some problems trying to run a VFP exe as a service, until I found the Service control included in Server tools from www.dart.com.

*!*	The code below is only for testing and all it does is write a line to a text file every five seconds.

*!*	The nice thing with this control is that it also handles installing and uninstalling of the service. If you include the code below in a project and compile it into an exe, run the exe with "Install" as a parameter the first time. Then you'll find the exe as a service in the Service Manager where you can start and stop it.

*!*	#DEFINE CR_LF CHR(13) + CHR(10)
*!*	#DEFINE SERVICE_STOPPED 0
*!*	#DEFINE SERVICE_PAUSED 1
*!*	#DEFINE SERVICE_RUNNING 2
*!*	#DEFINE SERVICE_RESUMED 3

*!*	LPARAMETERS;
*!*	 lcStart

*!*	LOCAL;
*!*	 loService,;
*!*	 loEvents,;
*!*	 lnNtService,;
*!*	 lnLogTime

*!*	_VFP.AutoYield = .F.
*!*	SET TALK OFF
*!*	SET NOTIFY OFF
*!*	SET EXCLUSIVE OFF

*!*	ON ERROR DO ShowError WITH LINENO(), ERROR(), MESSAGE()
*!*	ON SHUTDOWN DO ShutDown

*!*	IF EMPTY(lcStart)
*!*		lcStart = ""
*!*	ENDIF

*!*	lnLogTime = SECONDS()

*!*	STORE 1 TO lnNtService
*!*	=SYS(2340,lnNtService)

*!*	lcStart = ALLTRIM(UPPER(lcStart))

*!*	loService	= CREATEOBJECT("Dart.Service.1")
*!*	loEvents	= CREATEOBJECT("Dartservice")
*!*	loEvents.oService = loService

*!*	llConnect = EVENTHANDLER(loService, loEvents)

*!*	DO CASE
*!*	CASE lcStart == "INSTALL"
*!*		loService.Install("VFP Service", 1)

*!*	CASE lcStart == "UNINSTALL"
*!*		loService.UnInstall("VFP Service")

*!*	OTHERWISE

*!*		loService.Connect()

*!*		READ EVENTS

*!*		ON SHUTDOWN

*!*	ENDCASE

*!*	loService.Abort()

*!*	loEvents	= .NULL.
*!*	loService	= .NULL.

*!*	RETURN


*!*	PROCEDURE ShowError
*!*	LPARAMETERS;
*!*	 lnLineno,;
*!*	 lnError,;
*!*	 lcMessage

*!*	WAIT WINDOW "Error line " +;
*!*	 TRANSFORM(lnLineno, "999999") + CHR(13) +;
*!*	 "Error: " +;
*!*	 TRANSFORM(lnError, "999999") + CHR(13) +;
*!*	 lcMessage

*!*	RETURN


*!*	PROCEDURE ShutDown

*!*	ON SHUTDOWN
*!*	QUIT

*!*	RETURN



*!*	DEFINE CLASS DartService AS session OLEPUBLIC

*!*		oService	= .NULL.
*!*		oLogger		= .NULL.
*!*		oTimer		= .NULL.

*!*		IMPLEMENTS _IServiceEvents IN {13F4DED1-D19F-11D2-BA94-0040053687FE}#1.0

*!*		PROCEDURE _IServiceEvents_State() AS VOID;
*!*	 				HELPSTRING "Fires when the State Property changes."

*!*			IF VARTYPE(THIS.oService) = "O" AND !ISNULL(THIS.oService)

*!*				WITH THIS.oService
*!*					DO CASE
*!*					CASE .State = SERVICE_STOPPED
*!*						THIS.oLogger.StopLog()
*!*						CLEAR EVENTS

*!*					CASE .State = SERVICE_PAUSED
*!*						THIS.oLogger.StopLog()

*!*					CASE .State = SERVICE_RUNNING
*!*						IF ISNULL(THIS.oLogger)
*!*							THIS.oLogger = CREATEOBJECT("Logger", THIS.oService)
*!*						ENDIF
*!*						THIS.oLogger.StartLog()

*!*					CASE .State = SERVICE_RESUMED
*!*						THIS.oLogger.StartLog()

*!*					ENDCASE
*!*				ENDWITH
*!*			ENDIF

*!*		ENDPROC

*!*		PROCEDURE _IServiceEvents_Message(Number AS Number) AS VOID;
*!*	 				HELPSTRING "Fires when a application-specific message arrives."
*!*		* add user code here

*!*		ENDPROC

*!*		PROCEDURE _IServiceEvents_Error(Number AS VARIANT, Description AS STRING) AS VOID;
*!*	 				HELPSTRING "Fires when an error condition is experienced."

*!*			LOCAL;
*!*			 lcFile

*!*			lcFile = ADDBS(JUSTPATH(_VFP.Servername)) + "ServiceError.txt"

*!*			=STRTOFILE("Error: " + ALLTRIM(STR(Number)) + " " + Description, lcFile, 1)
*!*		ENDPROC

*!*		PROCEDURE Destroy

*!*			THIS.oService = .NULL.
*!*			THIS.oLogger = .NULL.
*!*		
*!*		ENDPROC

*!*	ENDDEFINE



*!*	DEFINE CLASS Logger AS Session

*!*		cFile		= ""
*!*		oService	= .NULL.
*!*		oTimer		= .NULL.

*!*		PROCEDURE Init

*!*			LPARAMETERS;
*!*			 loService
*!*			
*!*			WITH THIS
*!*				.cFile = ADDBS(JUSTPATH(_VFP.Servername)) + "Servicelog.txt"

*!*				.oService			= loService
*!*				.oTimer				= CREATEOBJECT("ServiceTimer")
*!*				.oTimer.oService	= .oService
*!*				.oTimer.cFile		= .cFile
*!*			ENDWITH

*!*		ENDPROC


*!*		PROCEDURE StartLog
*!*			THIS.oTimer.StartLog()

*!*		ENDPROC


*!*		PROCEDURE StopLog
*!*			THIS.oTimer.StopLog()


*!*		ENDPROC


*!*		PROCEDURE Destroy

*!*			THIS.oService = .NULL.
*!*			THIS.oTimer = .NULL.
*!*		
*!*		
*!*		ENDPROC

*!*	ENDDEFINE


*!*	DEFINE CLASS ServiceTimer AS Timer

*!*		oService	= .NULL.
*!*		cFile		= ""
*!*		Interval	= 5000


*!*		PROCEDURE Timer

*!*			IF !EMPTY(THIS.cFile
*!*				=STRTOFILE("Time: " + TTOC(DATETIME()) + CR_LF, THIS.cFile, 1)
*!*			ENDIF

*!*		ENDPROC


*!*		PROCEDURE StartLog

*!*			THIS.Enabled = .T.
*!*		
*!*		ENDPROC


*!*		PROCEDURE StopLog
*!*		
*!*			THIS.Enabled = .F.
*!*		
*!*		ENDPROC


*!*	ENDDEFINE
*!*	>Hi All,
*!*	>
*!*	>Does anyone have any direct experience in running a VFP exe as a service?
*!*	>I have been playing with this for a while now without any real success.
*!*	>
*!*	>below is some code.. when run directly as an exe it works fine.. when I try to start it as a service it appears to start up, the .txt file is created and gets 3 lines added to it then the service manager gives an error.
*!*	>
*!*	>Could not start the monitor service on \\server
*!*	>Error 2186: The service is not responding to the control function
*!*	>
*!*	>Apparently control has to be returned back to the service manager before the service can be considered "started".
*!*	>
*!*	>I'm stumped!, I have tried replacing this timer with a loop with a sleep API call.
*!*	>
*!*	>Any help would be appreciated
*!*	>
*!*	>Thanks
*!*	>Will
*!*	>
*!*	>
*!*	>*!*	=Sys(2335, 0)		&& Unattended Server mode
*!*	>=Sys(3050, 1,256)	&& Set foreground buffer to 256k
*!*	>=Sys(3050, 2,256)	&& Set Background buffer to 256k
*!*	>=Sys(2340, 1)		&& NT Service support
*!*	>
*!*	>Application.Visible = .F.  && There is a screen=off in config.fpw
*!*	>
*!*	>Set Safety Off
*!*	>On Shutdown Clear Events
*!*	>
*!*	>Public oTimer
*!*	>oTimer = CreateObject("MainLoop")
*!*	>*!*	Read Events
*!*	>
*!*	>Define Class MainLoop As Timer
*!*	>	Name="MainLoop"
*!*	>	Interval=20000
*!*	>	Enabled=.T.
*!*	>
*!*	>	Procedure Timer
*!*	>		Local lcstr
*!*	>		If !File("c:\monitor\monitor.txt")
*!*	>			=Strtofile("Started at " + Ttoc(Datetime()) + Chr(13) + Chr(10), "C:\monitor\monitor.txt")
*!*	>		Endif
*!*	>		lcstr = Filetostr("c:\monitor\monitor.txt")
*!*	>		lcstr = lcstr + "Last Run : " + Ttoc(Datetime()) + Chr(13) + Chr(10)
*!*	>
*!*	>		=Strtofile(lcstr, "C:\monitor\monitor.txt")
*!*	>	Endproc
*!*	>Enddefine
*!*	>
*!*	 

*!*	*****************************************************************
*!*	* from scm3.prg
*!*	LOCAL MSSQL As Win32Service
*!*	MSSQL = CREATEOBJECT("Win32Service", "MSSQLSERVER")
*!*	MSSQL = CREATEOBJECT("Win32Service", "poop")
*!*	set step on 
*!*	IF MSSQL.GetWmiObject() = NULL
*!*	* the service is not found
*!*		RETURN
*!*	ENDIF

*!*	? "The service is running:", MSSQL.IsStarted()
*!*	* end of main

*!*	DEFINE CLASS Win32Service As Session
*!*	PROTECTED wmiserver, servicename, service
*!*		servicename=""
*!*		service=NULL

*!*	PROCEDURE Init(cServiceName)
*!*		THIS.servicename = m.cServiceName
*!*		THIS.wmiserver = GetObject("winmgmts:\\" +;
*!*			"." + "\root\cimv2")
*!*		THIS.InterrogateService

*!*	PROCEDURE InterrogateService
*!*		LOCAL oCollection, oMember

*!*		oCollection = THIS.wmiserver.ExecQuery(;
*!*				"SELECT * FROM Win32_Service " +;
*!*				"WHERE name='" + THIS.servicename + "'")

*!*		FOR EACH oMember IN oCollection
*!*			THIS.service = oMember
*!*		NEXT

*!*	PROCEDURE GetWmiObject
*!*	RETURN THIS.service

*!*	PROCEDURE StartService
*!*	RETURN THIS.service.StartService()

*!*	PROCEDURE StopService
*!*	RETURN THIS.service.StopService()

*!*	PROCEDURE PauseService
*!*	RETURN THIS.service.PauseService()

*!*	PROCEDURE ResumeService
*!*	RETURN THIS.service.ResumeService()

*!*	PROCEDURE GetServiceName
*!*	RETURN THIS.servicename

*!*	PROCEDURE GetProperty(cProperty, oDefault)
*!*		LOCAL oValue
*!*		TRY
*!*			oValue = EVALUATE("THIS.service." + cProperty)
*!*		CATCH
*!*			oValue = m.oDefault
*!*		ENDTRY
*!*	RETURN NVL(m.oValue, m.oDefault)

*!*	PROCEDURE GetCaption
*!*	RETURN THIS.GetProperty("Caption","")

*!*	PROCEDURE GetDescription
*!*	RETURN THIS.GetProperty("Description","")

*!*	PROCEDURE GetServiceType
*!*	RETURN THIS.GetProperty("ServiceType","")

*!*	PROCEDURE IsStarted
*!*	RETURN THIS.GetProperty("Started",.F.)

*!*	PROCEDURE GetStartMode
*!*	RETURN THIS.GetProperty("StartMode","")

*!*	PROCEDURE GetStartName
*!*	RETURN THIS.GetProperty("StartName","")

*!*	PROCEDURE GetServiceState
*!*	RETURN THIS.GetProperty("State","")

*!*	PROCEDURE GetServiceStatus
*!*	RETURN THIS.GetProperty("Status","")

*!*	PROCEDURE GetPathName
*!*	RETURN THIS.GetProperty("PathName","")

*!*	ENDDEFINE

*!*	*****************************************************************
*!*	* from scm4.prg
*!*	#DEFINE SERVICE_WIN32 0x30
*!*	#DEFINE SERVICE_RUNNING 4
*!*	set step on 
*!*	LOCAL ws As winservices, srv As winservice
*!*	ws = CREATEOBJECT("winservices", Null, Null, SERVICE_WIN32)

*!*	srv = ws.GetService("MSSQLSERVER")
*!*	IF ISNULL(srv)
*!*	        ? "MSSQLSERVER service is not found on this computer"
*!*	ELSE
*!*	        ? "MSSQLSERVER service is " +;
*!*	                IIF(srv.currentstate=SERVICE_RUNNING,;
*!*	                "running", "stopped") + "."
*!*	ENDIF

*!*	srv.StartService()
*!*	*srv.PauseService()
*!*	*srv.StopService()

*!*	 
*!*	 
*!*	 
*!*	 
*!*	LOCAL MSSQL As Win32Service
*!*	MSSQL = CREATEOBJECT("Win32Service", "MSSQLSERVER")
*!*	 
*!*	IF MSSQL.GetWmiObject() = NULL
*!*	* the service is not found
*!*	    RETURN
*!*	ENDIF
*!*	 
*!*	? MSSQL.IsStarted()
*!*	* end of main
*!*	 
*!*	DEFINE CLASS Win32Service As Session
*!*	PROTECTED wmiserver, servicename, service
*!*	    servicename=""
*!*	    service=NULL
*!*	 
*!*	PROCEDURE Init(cServiceName)
*!*	    THIS.servicename = m.cServiceName
*!*	    THIS.wmiserver = GetObject("winmgmts:\\" +;
*!*	        "." + "\root\cimv2")
*!*	    THIS.InterrogateService
*!*	 
*!*	PROCEDURE InterrogateService
*!*	    LOCAL oCollection, oMember
*!*	 
*!*	    oCollection = THIS.wmiserver.ExecQuery([SELECT * FROM Win32_Service WHERE name="" + THIS.servicename + ""])
*!*	 
*!*	    FOR EACH oMember IN oCollection
*!*	        THIS.service = oMember
*!*	    NEXT
*!*	 
*!*	PROCEDURE GetWmiObject
*!*	RETURN THIS.service
*!*	 
*!*	PROCEDURE StartService
*!*	RETURN THIS.service.StartService()
*!*	 
*!*	PROCEDURE StopService
*!*	RETURN THIS.service.StopService()
*!*	 
*!*	PROCEDURE PauseService
*!*	RETURN THIS.service.PauseService()
*!*	 
*!*	PROCEDURE ResumeService
*!*	RETURN THIS.service.ResumeService()
*!*	 
*!*	PROCEDURE GetServiceName
*!*	RETURN THIS.servicename
*!*	 
*!*	PROCEDURE GetProperty(cProperty, oDefault)
*!*	    LOCAL oValue
*!*	    TRY
*!*	        oValue = EVALUATE("THIS.service." + cProperty)
*!*	    CATCH
*!*	        oValue = m.oDefault
*!*	    ENDTRY
*!*	RETURN NVL(m.oValue, m.oDefault)
*!*	 
*!*	PROCEDURE GetCaption
*!*	RETURN THIS.GetProperty("Caption","")
*!*	 
*!*	PROCEDURE GetDescription
*!*	RETURN THIS.GetProperty("Description","")
*!*	 
*!*	PROCEDURE GetServiceType
*!*	RETURN THIS.GetProperty("ServiceType","")
*!*	 
*!*	PROCEDURE IsStarted
*!*	RETURN THIS.GetProperty("Started",.F.)
*!*	 
*!*	PROCEDURE GetStartMode
*!*	RETURN THIS.GetProperty("StartMode","")
*!*	 
*!*	PROCEDURE GetStartName
*!*	RETURN THIS.GetProperty("StartName","")
*!*	 
*!*	PROCEDURE GetServiceState
*!*	RETURN THIS.GetProperty("State","")
*!*	 
*!*	PROCEDURE GetServiceStatus
*!*	RETURN THIS.GetProperty("Status","")
*!*	 
*!*	PROCEDURE GetPathName
*!*	RETURN THIS.GetProperty("PathName","")
*!*	 
*!*	ENDDEFINE

*!*	*****************************************************88 
