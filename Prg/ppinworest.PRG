* called from ppinwo
* creates inloc records and adjusts inventory for cartons & units

lparameters xfromwave, xrackonhold, xwavepartial, xholdtype

m.whseloc=xrackloc
m.locqty=m.totqty
if xwavepartial
	m.removeqty=m.totqty
	m.remainqty=0
else
	m.removeqty=0
	m.remainqty=m.totqty
endif

insertinto("inloc","wh",.t.)

* adjust inventory

if !xfromwave
	select indet
	scatter memvar

	if !seek(str(m.accountid,4)+trans(m.units)+m.style+m.color+m.id+m.pack,xinven,"match")
		m.date_rcvd=date()
		insertinto(xinven,"wh",.t.)
		replace totqty with 0 in (xinven)
	endif
	
	if !seek(str(m.accountid,4)+trans(m.units)+m.style+m.color+m.id+m.pack+xrackloc,xinvenloc,"matchwh")
		m.invenid=&xinven..invenid
		m.ppadj=.t.
		m.holdtype=""
		insertinto(xinvenloc,"wh",.t.)
		replace locqty with 0 in &xinvenloc
	endif
		
	if &xinvenloc..hold or xrackonhold
		xholdqty=m.totqty
	else
		xholdqty=0
	endif
	
	replace totqty with &xinven..totqty+m.totqty, ;
			holdqty with &xinven..holdqty+xholdqty, ;
			availqty with &xinven..totqty-&xinven..allocqty-&xinven..holdqty in &xinven

	try
		replace updatemark with .f. in &xinven
	catch
	endtry

	replace locqty with &xinvenloc..locqty+m.locqty in &xinvenloc
endif

