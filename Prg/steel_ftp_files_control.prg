**This scriptwill identify files not FTP'd successfully

utilsetup("SS_FTP_FILES_CONTROL")
Set Exclusive Off
Set Deleted On
Set Talk Off
Set Safety Off
With _Screen
	.AutoCenter = .T.
	.WindowState = 0
	.BorderStyle = 1
	.Width = 320
	.Height = 210
	.Top = 290
	.Left = 110
	.Closable = .T.
	.MaxButton = .T.
	.MinButton = .T.
	.ScrollBars = 0
	.Caption = "SS_FTP_FILES_CONTROL"
Endwith


If !Used("edi_trigger")
	Use F:\3pl\Data\edi_trigger Shared In 0
Endif

If !Used("ftplog")
	Use F:\wh\ftplog Shared In 0
ENDIF

Select * From ftplog Where dd>=Date()-20 Into Cursor ssftplog
Use In ftplog


SELECT file945, SPACE(30) as trig945 FROM edi_trigger where INLIST(accountid,6612) AND edi_type='945 ' AND WHEN_proc > DATE()-4;
and WHEN_proc<datetime()-4000  into cursor trig945_1 readwrite
SELECT trig945_1
scan
pos1=ATC('945s',file945)
replace trig945 with substr(file945,pos1,36) 
ENDSCAN
SELECT distinct(trig945) as trig945 FROM trig945_1 INTO CURSOR trig945_2 READWRITE


Select *, SPACE(40) as log945 From ssftplog Where 'Sent file' $logdata And 'steels' $logdata And '.txt successfully' $logdata Into Cursor log945_1 READWRITE 
scan
pos1=ATC('\945s',logdata)
replace log945 with substr(logdata,pos1+1,35) 
endscan
SELECT * FROM trig945_2 a LEFT JOIN  log945_1 b ON RTRIM(a.trig945)=RTRIM(b.log945) INTO CURSOR  compare945_1 READWRITE
Select trig945 From compare945_1 Where Isnull(log945)Into Cursor steel_missing945 Readwrite
*******copy missing file to be resent
*!*	Select  steel_missing945
*!*	Scan
*!*		xfilename=Alltrim(trig945)

*!*		Copy File F:\FTPUSERS\Merkury\945OUT\Archive\&xfilename   To  F:\FTPUSERS\Merkury\945OUT\hold\&xfilename
*!*	Endscan
**************

SET STEP ON 

Select  steel_missing945 
If Reccount() > 0
	Export To "S:\steelseries\TEMP\steelseries_MISSING_945_FTP"  Type Xls
	tsendto = "tmarg@fmiint.com"
	tsendto = "tmarg@fmiint.com,todd.margolin@tollgroup.com"
	tattach = "S:\steelseries\TEMP\steelseries_MISSING_945_FTP.xls"
	tcc =""
	tFrom ="Toll EDI System Operations <fmi-transload-ops@fmiint.com>"
	tmessage = "Steel Series missing 945s from FTPlog "+Ttoc(Datetime())+"    Todd check HOLD folder"
	tSubject = "Steel Series missing 945s from FTPlog"
	Do Form m:\dev\frm\dartmail2 With tsendto,tFrom,tSubject, tcc,tattach,tmessage  ,"A"
Else
	tsendto = "tmarg@fmiint.com"
	tattach = ""
	tcc =""
	tFrom ="Toll EDI System Operations <fmi-transload-ops@fmiint.com>"
	tmessage = "NO Steel Series missing 945s from FTPlog "+Ttoc(Datetime())
	tSubject = "NO Steel Series missing 945s from FTPlog exist"
	Do Form m:\dev\frm\dartmail2 With tsendto,tFrom,tSubject, tcc,tattach,tmessage  ,"A"
Endif



Close Data All
schedupdate()
_Screen.Caption=gscreencaption