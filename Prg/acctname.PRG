lparameter xaccountid 

xdyselect=select()

if !used("account")
	xsqlexec("select accountid, acctname from account",,,"qq")
	index on accountid tag accountid
	set order to
endif

select account
locate for accountid=xaccountid
if found()
* removed dy 1/11/18
*	if type("gsystemmodule")="C" and inlist(gsystemmodule,"WH","RC","SL") and (type("goffice")="C" and goffice="J")
*		xreturn=trim(strtran(account.acctname,"MARC JACOBS ",""))
*	else
		xreturn=trim(account.acctname)
*	endif
else
	xreturn=space(30)
endif

if !empty(xdyselect)
	select (xdyselect)
endif

return xreturn