CLOSE DATA ALL
*SET STEP ON
SET DATE LONG
PUBLIC ARRAY thisarray(1)
nAcctNum = 6561
cOffice = "C"
gOffice = "2"
lTesting = .F.
lEmail = .T.
tfrom = "TGF EDI Operations <toll-edi-ops@tollgroup.com>"
tmessage=""
tcc = ""

xReturn = "XXX"
DO m:\dev\prg\wf_alt WITH cOffice,nAcctNum
cUseFolder = UPPER(xReturn)
cRetMsg = "X"

SELECT 0
USE F:\3pl\DATA\mailmaster ALIAS mm
LOCATE FOR mm.accountid = nAcctNum AND mm.office = cOffice AND taskname = "DAILYSHIPRPT"
tsendto = IIF(mm.use_alt,sendtoalt,sendto)
tcc = IIF(mm.use_alt,ccalt,cc)
LOCATE
LOCATE FOR edi_type = "MISC" AND taskname = "JOETEST"
tsendtotest = IIF(mm.use_alt,sendtoalt,sendto)
tcctest = IIF(mm.use_alt,ccalt,cc)
USE IN mm

tsendto = IIF(lTesting,tsendtotest,tsendto)
tcc = IIF(lTesting,tcctest,tcc)

IF USED('OUTSHIP')
	USE IN outship
ENDIF

xsqlexec("select * from outship where accountid =  6561 AND del_date = {"+dDate1+"}",,,"wh")

IF USED('SHIPMENT')
	USE IN shipment
ENDIF

goffice="2"

csq1 = [select * from shipment where accountid = ]+TRANSFORM(nAcctNum)
xsqlexec(csq1,,,"wh")
INDEX ON shipmentid TAG shipmentid
INDEX ON pickticket TAG pickticket

IF USED('PACKAGE')
	USE IN package
ENDIF

csq1 = [select * from package where accountid = ]+TRANSFORM(nAcctNum)
xsqlexec(csq1,,,"wh")
INDEX ON shipmentid TAG shipmentid
SET ORDER TO TAG shipmentid

CREATE CURSOR tempfinal (consignee c(35),bol_no c(22),pickticket c(20),ponum c(25),qty i,rfnumber c(10),trknumber c(30))
*SET STEP ON
WAIT WINDOW "Now selecting Outship records for "+DTOC(dDate1) NOWAIT NOCLEAR
SELECT consignee,bol_no,ship_ref AS pickticket,cnee_ref AS ponum,qty,shipins ;
	FROM outship ;
	WHERE outship.accountid =  6561 ;
	AND outship.del_date = dDate1 ;
	AND (INLIST(outship.scac,"MIMK","FSP") OR INLIST(outship.cacctnum,"GROUPDS","LIVSO10","QUIB","SUPPORT","STAPL")) ;
	INTO CURSOR tempout

WAIT CLEAR
WAIT WINDOW "Now scanning selection results and compiling final cursor" NOWAIT NOCLEAR
SELECT tempout
SCAN
	SCATTER FIELDS consignee,bol_no,pickticket,ponum,qty MEMVAR
	alength = ALINES(apt,tempout.shipins,.T.,CHR(13))
	m.rfnumber = segmentget(@apt,"RF",alength)
	cShip_ref = ALLTRIM(tempout.pickticket)
	SELECT shipment
	=SEEK(cShip_ref,"shipment","pickticket")
	IF !FOUND()
		WAIT WINDOW "No find in Shipment, PT# "+cShip_ref TIMEOUT 2
		LOOP
	ENDIF
	SELECT package
	SCAN FOR package.shipmentid = shipment.shipmentid
		m.trknumber = ALLTRIM(package.trknumber)
		INSERT INTO tempfinal FROM MEMVAR
	ENDSCAN
ENDSCAN

WAIT CLEAR
WAIT WINDOW "Now creating Excel output file for mailing" TIMEOUT 2
SELECT tempfinal
LOCATE
IF lTesting
	BROWSE
ENDIF
cXfilename = ("c:\tempfox\dsrpt"+DTOS(dDate1)+".xls")
COPY TO [&cXfilename] TYPE XL5

set step on

tsubject = "Merkury Drop Ship Report from TGF for "+DTOC(dDate1)
tattach = cXfilename
tmessage = "The Merkury DS report from TGF for today is attached"

tsendto = IIF(lTesting,tsendtotest,tsendto)
tcc = IIF(lTesting,tcctest,tcc)

IF lTesting
	tmessage = tmessage +CHR(13)+CHR(13)+"This is a TEST"
ELSE
	tmessage = tmessage+CHR(13)+CHR(13)+"If you have any questions, please eMail us or call Joe or Paul"
ENDIF

IF lEmail
	IF lTesting
		ASSERT .F. MESSAGE "At do mail stage...DEBUG"
	ENDIF
	DO FORM m:\dev\frm\dartmail2 WITH tsendto,tfrom,tsubject,tcc,tattach,tmessage,"A"
ENDIF
DELETE FILE &cXfilename
SET DATE AMERICAN
CLOSE DATA ALL

****************************
PROCEDURE segmentget
****************************
	PARAMETER thisarray,lcKey,nLength

	FOR i = 1 TO nLength
		IF i > nLength
			EXIT
		ENDIF
		lnEnd= AT("*",thisarray[i])
		IF lnEnd > 0
			lcThisKey =TRIM(SUBSTR(thisarray[i],1,lnEnd-1))
			IF OCCURS(lcKey,lcThisKey)>0
				RETURN SUBSTR(thisarray[i],lnEnd+1)
				i = 1
			ENDIF
		ENDIF
	ENDFOR

	RETURN ""
