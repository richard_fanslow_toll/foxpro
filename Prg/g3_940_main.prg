*!* m:\dev\prg\bernardo940_main.prg

PARAMETERS cOfficeIn

PUBLIC nAcctNum,cDelimiter,nRun_num,cTranslateOption,cWhichFile,cOffice,cCustname,cProperName,cStorenum,lOK,cMailLoc
PUBLIC cFilter,nRunID,lComplete,lTesting,m.UCCNUMBER,lEmail,lTestImport,lTestRun,emailcommentstr,cUseFolder,nFileSize
PUBLIC ptid,ptctr,ptqty,xfile,cfilename,nUploadCount,pickticket_num_start,pickticket_num_end,nFileCount,units,cUCCNumber,cAcctCode
PUBLIC lcPath,lcArchivepath,tsendto,tcc,NormalExit,lTestmail,LogCommentStr,lPick,lPrepack,tsendtoerr,tccerr,cErrMsg,cISA_Num,Archivefile
PUBLIC m.addby,m.adddt,m.addproc,cTransfer,lHighline,tfrom,tattach,cISA_Num,thisfile,tsendtotest,tcctest,lBrowfiles,lLoadSQL,cMod
PUBLIC ARRAY thisarray(1)
PUBLIC ARRAY a856(1)

CLOSE DATABASES ALL
CLEAR
ON ERROR THROW

xfile="No file yet"
cProperName = "G-III"

SET ASSERTS ON
cErrMsg = ""

TRY
	lTesting = .f.
	DO m:\dev\prg\_setvars WITH lTesting
	lookups()

	lTestImport = lTesting
	lTestRun = lTesting
	lTestmail = lTesting
	lOverridebusy = lTesting
	lBrowfiles = lTesting

*	lOverridebusy = .t.
	NormalExit = .F.
	nAcctNum  = 6614  && Starting account ID
	lEmail = .t.

	IF VARTYPE(cOfficeIn)="L"
		cOfficeIn = "C"
	ENDIF
	cCustname = "G-III"
*ASSERT .F. MESSAGE "At start of G-III 940 processing"

	IF !lTesting
		SELECT 0
		USE F:\edirouting\ftpsetup SHARED IN 0
		cTransfer = "940-G-III"
		LOCATE FOR ftpsetup.transfer = cTransfer AND ftpsetup.TYPE = "GET"
		IF ftpsetup.chkbusy AND !lOverridebusy
			WAIT WINDOW "Process is flagged busy...returning" TIMEOUT 3
			NormalExit = .T.
			THROW
		ELSE
			REPLACE chkbusy WITH .T.,trig_time WITH DATETIME()  ;
				FOR ftpsetup.transfer = cTransfer ;
				IN ftpsetup
			USE IN ftpsetup
		ENDIF
	ENDIF

	LogCommentStr = ""

	STORE cOfficeIn TO cOffice
	cMod = IIF(cOffice = "C","6",cOffice)
	gOffice = cMod
	lLoadSQL = .T.

	cMailLoc = IIF(INLIST(cOffice,"I","N"),"New Jersey","San Pedro")

	tfrom = "Toll/FMI EDI Operations <toll-edi-ops@tollgroup.com>"
	STORE " " TO tattach,tsendto,tcc

	_SCREEN.WINDOWSTATE= IIF(lTesting OR lOverridebusy or DATETIME() <DATETIME(2017,9,19,15,00,00),2,1)

	SELECT 0
	USE F:\3pl\DATA\mailmaster ALIAS mm SHARED
	LOCATE FOR mm.edi_type = "940" AND mm.accountid = 6614 AND mm.office = cOffice
	IF FOUND()
		STORE TRIM(mm.acctname) TO cCustname
		STORE TRIM(mm.basepath) TO lcPath
		STORE TRIM(mm.archpath) TO lcArchivepath
		_SCREEN.CAPTION = ALLT(mm.scaption)
*!*			STORE mm.testflag 	   TO lTest
		STORE TRIM(IIF(mm.use_alt,mm.sendtoalt,mm.sendto)) TO tsendto
		STORE TRIM(IIF(mm.use_alt,mm.ccalt,mm.cc)) TO tcc
		LOCATE
		LOCATE FOR mm.edi_type = "MISC" AND mm.taskname = "GENERAL"
		STORE TRIM(IIF(mm.use_alt,mm.sendtoalt,mm.sendto)) TO tsendtotest
		STORE TRIM(IIF(mm.use_alt,mm.ccalt,mm.cc)) TO tcctest
		tsendtoerr = tsendtotest
		tccerr = tcctest
		USE IN mm
	ELSE
		USE IN mm
		WAIT WINDOW AT 10,10  "No parameters set for this acct# "+ALLTRIM(STR(nAcctNum))+"  ---> Office "+cOffice TIMEOUT 2
		RELEASE ALL
		NormalExit = .F.
		THROW
	ENDIF

	IF lTesting
		STORE "f:\ftpusers\G-III\940INTest\"         TO lcPath
		STORE "f:\ftpusers\G-III\940INTest\archive\" TO lcArchivepath
	ENDIF

	cUseName = "G-III"
	cProperName = PROPER(cUseName)

	_SCREEN.CAPTION = cProperName+" 940 Process"
	_SCREEN.WINDOWSTATE = IIF(lTesting or lOverridebusy,2,1)
	CLEAR
	WAIT WINDOW "Now setting up "+cProperName+" 940 process..." TIMEOUT 2

	nFileCount = 0
	nRun_num = 999
*	SET STEP ON 
	lnRunID  =dygenpk("runid","whall")

	*SET STEP ON 
	IF lTesting
		cUseFolder= "F:\WHP\WHDATA\"
	ELSE
		xReturn="XXX"
		DO m:\dev\prg\wf_alt WITH cOffice,IIF(cOffice = "I",6623,nAcctNum)
		cUseFolder=xReturn
	ENDIF

&& uncomnet these out to reupload ppdata from the 940test folder.

IF lTesting
  STORE "f:\ftpusers\G-III\940INTest\"         TO lcPath
  STORE "f:\ftpusers\G-III\940INTest\archive\" TO lcArchivepath
  cUseFolder= "F:\WHP\WHDATA\"
endif


	WAIT WINDOW "Folder used is "+cUseFolder TIMEOUT 2

	IF lTesting
		SELECT 0
		USE F:\wh\ppdatatemplate EXCLUSIVE
		ZAP
		USE
	ENDIF

	USE F:\wh\ppdatatemplate IN 0 ALIAS xppdata
	SELECT * FROM xppdata WHERE .F. INTO CURSOR tempppdata READWRITE

	DO ("m:\dev\PRG\g3_940_PROCESS")

	IF !lTesting
		cTransfer = "940-G-III-"
		SELECT 0
		USE F:\edirouting\ftpsetup SHARED
		REPLACE chkbusy WITH .F. ;
			FOR ftpsetup.transfer = cTransfer ;
			IN ftpsetup
	ENDIF

	NormalExit = .T.

CATCH TO oErr
	IF NormalExit = .F.
		ASSERT .F. MESSAGE "In Catch section..."
		SET STEP ON
		tsubject = cProperName+" 940 Upload Error ("+TRANSFORM(oErr.ERRORNO)+") at "+TTOC(DATETIME())
		tsendto  = tsendtoerr
		tcc = IIF("N1SF"$cErrMsg,"maria.estrella@tollgroup.com",tccerr)
		tmessage = cProperName+" 940 Upload Error..... Please fix me........!"
		lcSourceMachine = SYS(0)
		lcSourceProgram = "from G-III  "

		IF !EMPTY(ALLTRIM(cErrMsg))
			tmessage = cErrMsg
		ELSE
			tmessage =tmessage+ "Try/CATCH Exeception Message:"+CHR(13)+;
				[  Error: ] + STR(oErr.ERRORNO) +CHR(13)+;
				[  LineNo: ] + STR(oErr.LINENO) +CHR(13)+;
				[  Message: ] + oErr.MESSAGE +CHR(13)+;
				[  Procedure: ] + oErr.PROCEDURE +CHR(13)+;
				[  Details: ] + oErr.DETAILS +CHR(13)+;
				[  StackLevel: ] + STR(oErr.STACKLEVEL) +CHR(13)+;
				[  LineContents: ] + oErr.LINECONTENTS+CHR(13)+;
				[  UserValue: ] + oErr.USERVALUE+CHR(13)+;
				[  Computer:  ] +lcSourceMachine+CHR(13)+;
				[  940 file:  ] +xfile+CHR(13)+;
				[  Program:   ] +lcSourceProgram
		ENDIF

		DO FORM m:\dev\frm\dartmail2 WITH tsendto,tfrom,tsubject,tcc,tattach,tmessage,"A"
	ELSE
		WAIT WINDOW AT 10,10 "Normal Exit " TIMEOUT 2
	ENDIF
FINALLY
	ON ERROR
	SET STATUS BAR ON
	CLOSE DATABASES ALL
ENDTRY
