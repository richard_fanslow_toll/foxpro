*!* m:\dev\prg\tjx317_in.prg
*!* Derived from Steel Series process

PUBLIC nAcct_num,cDelimiter,nRun_num,cTranslateOption,cWhichFile,cOffice,cCustname,cPropername,cStorenum,lOK,tsendtopt,tccpt
PUBLIC cFilter,nRunID,lComplete,lTesting,m.UCCNUMBER,lEmail,lTestImport,emailcommentstr,cUseFolder,lBrowfiles,cMailName
PUBLIC ptid,ptctr,ptqty,xfile,cfilename,nUploadCount,pickticket_num_start,pickticket_num_end,nFileCount,units,cUCCNumber
PUBLIC lcPath,lcArchivepath,tsendto,tcc,NormalExit,lTestmail,LogCommentStr,lPick,lPrepack,tsendtoerr,tccerr,lImported
PUBLIC m.addby,m.adddt,m.addproc,cTransfer,tfrom,tattach,cisa_num,thisfile,tsendtotest,tcctest,m.ofice
PUBLIC cErrMsg

PUBLIC ARRAY a856(1)
CLOSE DATABASES ALL
NormalExit = .F.

cErrMsg="Non-specific: In program body"

TRY
	lTesting = .f.
	lTestImport = lTesting
	lTestmail = lTesting
	lOverridebusy = lTesting
	lEmail = .T.

	lBrowfiles = lTesting
	lOverridebusy = lTesting
*	lOverridebusy = .t.
*	lBrowfiles = .f.

	DO m:\dev\prg\_setvars WITH lTesting

	cCustname = "TJMAX"
	cMailName = "TJMAXX"
	nAcct_num  = 6565
	cOfficein ="C"
	m.office = cOfficein
	cTransfer = "317-TJX"

	_SCREEN.CAPTION = cCustname+" 317 Process"
	_SCREEN.WINDOWSTATE = IIF(lTesting OR lOverridebusy,2,1)
	CLEAR
	WAIT WINDOW "Now setting up "+cCustname+" 317 process..." TIMEOUT 2

*ASSERT .F. MESSAGE "At start of "+cMailName+" 317 processing"
	SELECT 0
	IF !lTesting
		USE F:\edirouting\ftpsetup SHARED
		LOCATE FOR ftpsetup.transfer = cTransfer AND ftpsetup.TYPE = "GET"
		IF ftpsetup.chkbusy AND !lOverridebusy
			WAIT WINDOW "Process is flagged busy...returning" TIMEOUT 1
			NormalExit = .T.
			THROW
		ENDIF
	ELSE
		CLOSE DATA ALL
	ENDIF

	LogCommentStr = ""

	STORE cOfficein TO cOffice
	STORE cOffice TO m.office
	tfrom = "Toll EDI Operations <toll-edi-ops@tollgroup.com>"
	STORE " " TO tattach,tsendto,tcc

	SELECT 0
	USE F:\3pl\DATA\mailmaster ALIAS mm SHARED
	LOCATE FOR mm.edi_type = "317" AND mm.accountid = nAcct_num AND mm.office = cOffice
	IF FOUND()
		STORE TRIM(mm.acctname) TO cCustname
		STORE TRIM(mm.basepath) TO lcPath
		STORE TRIM(mm.archpath) TO lcArchivepath
		_SCREEN.CAPTION = ALLT(mm.scaption)
*!*			STORE mm.testflag 	   TO lTest
		STORE TRIM(IIF(mm.use_alt,mm.sendtoalt,mm.sendto)) TO tsendto
		STORE TRIM(IIF(mm.use_alt,mm.ccalt,mm.cc)) TO tcc
		LOCATE
		LOCATE FOR mm.edi_type = "MISC" AND mm.taskname = "JOETEST"
		STORE TRIM(IIF(mm.use_alt,mm.sendtoalt,mm.sendto)) TO tsendtotest
		STORE TRIM(IIF(mm.use_alt,mm.ccalt,mm.cc)) TO tcctest
		LOCATE
		LOCATE FOR mm.edi_type = "MISC" AND mm.taskname = "GENERAL"
		STORE TRIM(IIF(mm.use_alt,mm.sendtoalt,mm.sendto)) TO tsendtoerr
		STORE TRIM(IIF(mm.use_alt,mm.ccalt,mm.cc)) TO tccerr
		IF lTesting
			tsendto = tsendtotest
			tcc = tcctest
		ENDIF
		USE IN mm
	ELSE
		USE IN mm
		WAIT WINDOW AT 10,10  "No parameters set for this acct# "+ALLTRIM(STR(nAcct_num))+"  ---> Office "+cOffice TIMEOUT 2
		RELEASE ALL
		NormalExit = .F.
		THROW
	ENDIF

	cUseName = "TJMAX"
	cPropername = PROPER(cUseName)

	DO m:\dev\prg\createx856a
	SELECT x856

	cDelimiter = "*"
	cTranslateOption = "TILDE"
	nFileCount = 0

	IF lTesting
		cUseFolder = "F:\WHP\WHDATA\"
	ELSE
		xReturn="XXX"
		DO m:\dev\prg\wf_alt WITH cOffice,nAcct_num
		cUseFolder=xReturn
	ENDIF

*!* Set up temp structure for XWOLOG
	cUseFolder = IIF(lTesting,"F:\WHP\WHDATA\","F:\WO\WODATA\")

	USE (cUseFolder+"wogenpk") IN 0 ALIAS wogenpk
	useca("select * from sizes",,,"wo")

	SELECT 0
	USE (cUseFolder+"wolog") SHARED && NOUPDATE
	SCATTER FIELDS EXCEPT wologid,wo_num BLANK MEMVAR
	SELECT * FROM wolog WHERE .F. INTO CURSOR xwolog READWRITE
	SELECT xwolog
	INDEX ON wo_num TAG wo_num
	SET ORDER TO TAG wo_num

	SELECT xwolog

*!* BEGIN
**************
	IF !USED("ediinlog")
		USE F:\edipoller\ediinlog IN 0 ALIAS ediinlog SHARED
	ENDIF

	LogCommentStr = ""

	delimchar = cDelimiter
	lcTranOpt= cTranslateOption

	SET DEFAULT TO &lcPath
	lnNum = ADIR(tarray,"*.out")

	m.wo_date = DATE()
	m.acctname = 'TJX COMPANIES INC.'
	m.accountid = nAcct_num
	m.wotype = "OCEAN FULL"
	m.type = "O"
	m.billtoid = nAcct_num
	m.qty_type = "CARTONS"
	m.addby = "TOLLPROC"
	m.addproc = "TJX317"

	CREATE CURSOR refstring (brokref c(15),CONTAINER c(11))

	FOR thisfile = 1  TO lnNum
		xfile = lcPath+tarray[thisfile,1] && +"."
		cfilename = ALLTRIM(LOWER(tarray[thisfile,1]))
		m.comments = cfilename
		archivefile  = (lcArchivepath+cfilename)
		out997file = ("F:\FTPUSERS\TJMAX\997-317trans\")+cfilename
		IF !lTesting
			COPY FILE [&xfile] TO [&out997file] && to create 997
		ENDIF
		WAIT WINDOW "Importing file: "+cfilename NOWAIT
		DO m:\dev\prg\createx856a
		DO m:\dev\prg\loadedifile WITH xfile,delimchar,lcTranOpt,cCustname
		m.pdf2 = "EDI317*"+FILETOSTR(xfile)
		IF lTesting
*			BROWSE TIMEOUT 2
		ENDIF

		SELECT xwolog
		DELETE ALL

		SELECT x856
		SET FILTER TO
		LOCATE

		WAIT WINDOW "NOW IN BREAKDOWN" TIMEOUT 1
		WAIT WINDOW "NOW IN BREAKDOWN" NOWAIT
*ASSERT .F. MESSAGE "At header bkdn...debug"
		m.adddt = DATETIME()
		m.wo_num = IIF(lTesting,9999999,0)
		m.remarks = "EDI FILE: "+cfilename
		m.cust_ref = "DIRECT"

*!*			IF !USED("isa_num")
*!*				IF lTesting
*!*					USE F:\3pl\test\isa_num IN 0 ALIAS isa_num
*!*				ELSE
*!*					USE F:\3pl\DATA\isa_num IN 0 ALIAS isa_num
*!*				ENDIF
*!*			ENDIF
		lDoBrokref = .T.

		DO WHILE !EOF("x856")
			cSegment = TRIM(x856.segment)
			cCode = TRIM(x856.f1)
			IF TRIM(x856.segment) = "GE"
				EXIT
			ENDIF

			IF EMPTY(cSegment)
				IF lTesting
					SELECT x856
					SKIP
					LOOP
				ELSE
					cErrMsg = "Incorrect file format: Blank segment"
					THROW
				ENDIF
			ENDIF

			IF TRIM(x856.segment) = "ISA"
				cisa_num = ALLTRIM(x856.f13)
				CurrentISANum = ALLTRIM(x856.f13)
				WAIT WINDOW "This is a "+cMailName+" 317 upload" TIMEOUT 2
				ptctr = 0
			ENDIF

			IF TRIM(x856.segment) = "GS"
				cgs_num = ALLTRIM(x856.f6)
				lcCurrentGroupNum = ALLTRIM(x856.f6)
*!*					SELECT isa_num
*!*					LOCATE FOR isa_num.isa_num = cisa_num ;
*!*						AND isa_num.acct_num = nAcct_num
*!*					IF !FOUND()
*!*						APPEND BLANK
*!*						REPLACE isa_num.isa_num WITH cisa_num IN isa_num
*!*						REPLACE isa_num.acct_num WITH nAcct_num IN isa_num
*!*						REPLACE isa_num.office WITH cOffice IN isa_num
*!*						REPLACE isa_num.TYPE WITH "317" IN isa_num
*!*						REPLACE isa_num.ack WITH .F. IN isa_num
*!*						REPLACE isa_num.dateloaded WITH DATETIME() IN isa_num
*!*						REPLACE isa_num.filename WITH UPPER(TRIM(xfile)) IN isa_num
*!*					ENDIF
			ENDIF

			IF TRIM(x856.segment) = "ST"  && new shipment
* m.wologid = m.wologid+1
			ENDIF

			IF TRIM(x856.segment) = "G62"  && new shipment
				dInsDate = CTOD(SUBSTR(ALLTRIM(x856.f2),5,2)+"/"+SUBSTR(ALLTRIM(x856.f2),7,2)+"/"+LEFT(ALLTRIM(x856.f2),4))
				DO CASE
					CASE TRIM(x856.f1) = "17"  && Est Del. Date
						m.eta = dInsDate
					CASE TRIM(x856.f1) = "80"  && Del. Req. Date
						m.deliverondate = dInsDate
				ENDCASE
			ENDIF

			IF TRIM(x856.segment) = "N1" AND TRIM(x856.f1) = "T3"
				m.puloc = ALLTRIM(x856.f2)
			ENDIF

			IF TRIM(x856.segment) = "N9" AND TRIM(x856.f1) = "OC"
				m.container = ALLTRIM(x856.f2)

				cSize = ALLTRIM(x856.f3)
				cSize = LEFT(cSize,2)+"'"+SUBSTR(cSize,3)
				IF !SEEK(cSize,'sizes','ctrsize')
					IF !FILE(archivefile)
						COPY FILE [&Xfile] TO [&archivefile]
					ENDIF
					IF FILE(xfile)
						DELETE FILE [&xfile]
					ENDIF
					cErrMsg = "Trailer Size not found in SIZES table"
					THROW
				ENDIF
			ENDIF

			IF TRIM(x856.segment) = "N9" AND TRIM(x856.f1) = "ABS"
				m.vessel = UPPER(ALLTRIM(x856.f2))
			ENDIF

			IF TRIM(x856.segment) = "N1" AND TRIM(x856.f1) = "SH"
				IF "LOS ANGELES"$UPPER(ALLTRIM(x856.f2))
					m.office="C"
				ELSE
					m.office="N"
				ENDIF
			ENDIF

			IF TRIM(x856.segment) = "N9" AND TRIM(x856.f1) = "3Z"
				IF lDoBrokref
					lDoBrokref = .F.
					m.brokref = UPPER(ALLTRIM(x856.f2))
				ENDIF
			ENDIF

			IF TRIM(x856.segment) = "N9" AND TRIM(x856.f1) = "MB"
				m.mblnum = ALLTRIM(x856.f2)
			ENDIF

			IF TRIM(x856.segment) = "TD5"
				m.ssco = UPPER(ALLTRIM(x856.f3))
				m.suppdata = "CARRIER*"+ALLTRIM(x856.f5)
				SKIP 1 IN x856

				IF TRIM(x856.segment) = "L0"
					m.weight = CEILING(INT(VAL(TRIM(x856.f4))))
					m.quantity = INT(VAL(TRIM(x856.f8)))
				ENDIF
			ENDIF

			IF TRIM(x856.segment) = "SE"
				INSERT INTO xwolog FROM MEMVAR
				INSERT INTO refstring FROM MEMVAR
				IF lTesting
					SELECT xwolog
					LOCATE
					BROWSE
					SELECT x856
				ENDIF
			ENDIF

			SELECT x856
			SKIP
		ENDDO

		SELECT xwolog
		LOCATE
		lUpload = .F.
		SCAN
			SELECT wolog
			IF SEEK(xwolog.CONTAINER,"WOLOG","CONTAINER")
				SET STEP ON
				IF wolog.accountid = nAcct_num AND wolog.wo_date > (xwolog.wo_date - 30)
					WAIT WINDOW "Container exists within the past month...skipping" TIMEOUT 3
					IF FILE(xfile)
						DELETE FILE [&xfile]
					ENDIF
					EXIT
				ENDIF
			ENDIF

*!*  			SELECT wogenpk
*!*  *!*				LOCATE FOR gpk_pk="WOLOG"
*!*  *!*				REPLACE gpk_curren WITH gpk_curren + 1
*!*  *!*				REPLACE gpk_currentnumber WITH gpk_currentnumber + 1
*!*  *!*				m.wologid = gpk_curren
*!*  *!*				m.wologid = gpk_currentnumber
*!*  			LOCATE
*!*  *			Set Step On
*!*        LOCATE FOR gpk_pk="WONUM"
*!*  			REPLACE gpk_currentnumber WITH gpk_currentnumber + 1
*!*  *			REPLACE gpk_curren WITH gpk_curren + 1
*!*  *			m.wo_num = gpk_curren
*!*
*!*
*!*         m.wo_num = gpk_currentnumber
*!*			m.wo_num = x3genpk("wonum","wo",,,.T.,,.T.)
			m.wo_num = dygenpk("wonum","wo")
			SELECT wolog
			SET ORDER TO
			INSERT INTO wolog FROM MEMVAR
			lUpload = .T.
		ENDSCAN

		IF !lTesting
			IF !FILE(archivefile)
				COPY FILE [&Xfile] TO [&archivefile]
			ENDIF
			IF FILE(xfile)
				DELETE FILE [&xfile]
			ENDIF

			IF lUpload
				LogCommentStr = "TJX 317 data uploaded"
				SELECT ediinlog
				APPEND BLANK
				REPLACE ediinlog.acct_name  WITH cCustname
				REPLACE ediinlog.office     WITH cOffice
				REPLACE ediinlog.filename   WITH tarray(thisfile,1)
				REPLACE ediinlog.SIZE       WITH tarray(thisfile,2)
				REPLACE ediinlog.FDATE      WITH tarray(thisfile,3)
				REPLACE ediinlog.FTIME      WITH tarray(thisfile,4)
				REPLACE ediinlog.TYPE       WITH "317"
				REPLACE ediinlog.uploadtime WITH DATETIME()
*				REPLACE ediinlog.qty        WITH ptqty
				REPLACE ediinlog.comments   WITH LogCommentStr
			ELSE
				NormalExit = .T.
				THROW
			ENDIF
		ENDIF
	ENDFOR  &&   FOR thisfile = 1  TO lnNum
*********************** End of the Main Loop ***************************************8

	WAIT CLEAR
	IF !lTesting
		SELECT 0
		IF USED('ftpsetup')
			USE IN ftpsetup
		ENDIF
		USE F:\edirouting\ftpsetup SHARED
		REPLACE chkbusy WITH .F. ;
			FOR ftpsetup.TYPE = "GET" ;
			AND ftpsetup.transfer = cTransfer ;
			IN ftpsetup
	ENDIF

	WAIT "Entire "+cMailName+" 317 Process Now Complete" WINDOW TIMEOUT 1
	IF lTesting
		SET STEP ON
	ENDIF
	NormalExit = .T.

	tsubject = cMailName+" 317 EDI File uploaded, as of "+TTOC(DATETIME())
	tattach = " "
	tmessage = "317 EDI Info containing these refs/containers:"+CHR(13)+CHR(13)
	SELECT refstring
	SCAN
		tmessage = tmessage + PADR("REFERENCE",18)+"CONTAINER"
		tmessage = tmessage + CHR(13)+PADR(ALLTRIM(refstring.brokref),18)+ALLTRIM(refstring.CONTAINER)+CHR(13)+CHR(13)
	ENDSCAN
	tmessage = tmessage +CHR(13)+"has been uploaded into the WO Log."+CHR(13)+CHR(13)
	IF lTesting
		tmessage = tmessage + "This is a TEST 317 upload"
	ELSE
		tmessage = tmessage + "If you have any questions, please eMail or call 732-750-9000 x217."
	ENDIF

	IF lEmail
*ASSERT .F. MESSAGE "At do mail stage...DEBUG"
		DO FORM m:\dev\frm\dartmail2 WITH tsendto,tfrom,tsubject,tcc,tattach,tmessage,"A"
	ENDIF

**************
*!* END

CATCH TO oErr
	IF NormalExit = .F.
		ASSERT .F. MESSAGE "In Catch section..."
		tsubject = cMailName+" 317 Upload Error ("+TRANSFORM(oErr.ERRORNO)+") at "+TTOC(DATETIME())
		tsendto  = tsendtoerr
		tcc = tccerr
		tmessage = cMailName+" 317 Upload Error..... Please fix me........!"
		lcSourceMachine = SYS(0)
		lcSourceProgram = "from TJX317_in"

		tmessage =tmessage+ "Try/CATCH Exeception Message:"+CHR(13)+;
			[  Error: ] + STR(oErr.ERRORNO) +CHR(13)+;
			[  LineNo: ] + STR(oErr.LINENO) +CHR(13)+;
			[  Message: ] + oErr.MESSAGE +CHR(13)+;
			[  Procedure: ] + oErr.PROCEDURE +CHR(13)

		IF !EMPTY(cErrMsg)
			tmessage =tmessage+CHR(13)+"Error found: "+cErrMsg
		ELSE
			tmessage =tmessage+[  Details: ] + oErr.DETAILS +CHR(13)+;
				[  StackLevel: ] + STR(oErr.STACKLEVEL) +CHR(13)+;
				[  LineContents: ] + oErr.LINECONTENTS+CHR(13)+;
				[  UserValue: ] + oErr.USERVALUE+CHR(13)+;
				[  Computer:  ] +lcSourceMachine+CHR(13)+;
				[  317 file:  ] +xfile+CHR(13)+;
				[  Program:   ] +lcSourceProgram
		ENDIF

		DO FORM m:\dev\frm\dartmail2 WITH tsendto,tfrom,tsubject,tcc,tattach,tmessage,"A"
	ELSE
		WAIT WINDOW AT 10,10 "Normal Exit " TIMEOUT 2
	ENDIF
FINALLY
	ON ERROR
	SET STATUS BAR ON
	CLOSE DATABASES ALL
ENDTRY
