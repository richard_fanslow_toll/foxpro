* This program extractsO/B shipping OS&D's for pre-invoiced MJ customers from previous day
utilsetup("MJ_CABAZON_SHPMNT")
set exclusive off
set deleted on
set talk off
set safety off   &&Allows for overwrite of existing files

close databases all
use f:\wh\sqlpassword in 0
gsqlpassword = alltrim(sqlpassword.password)
use in sqlpassword
cserver = "tgfnjsql01"
sqlsetprop(0,'DispLogin',3)
sqlsetprop(0,"dispwarnings",.f.)
lcdsnless="driver=SQL Server;server=&cServer;uid=SA;pwd=&gsqlpassword;DATABASE=PICKPACK"
nhandle=sqlstringconnect(m.lcdsnless,.t.)
if nhandle<=0 && bailout
	wait window at 6,2 "No Data Connection.."+chr(13)+" Call MIS...."
	lcquery = [Could not make connection to sp3 SQL server]+chr(13)+"   "+lcdsnless
	throw
	return
endif

if usesql()
	xsqlexec("select * from cartons where inlist(accountid,6303,6325,6543) and name='MJI' and ucc#'CUTS'","tempdata",,"pickpack")
else
	lcquery  = [select * from cartons WHERE cartons.accountid in (6303,6325,6543) and cartons.name like 'MJI%'  and insdttm>'01/01/2016' and ucc !='CUTS']
	if sqlexec(nhandle,lcquery,"tempdata")#1
		wait window at 6,2 "Error Cartons SQL query.........." timeout 1
		sqldisconnect(nhandle)
	endif
endif

upcmastsql (6303)
select style,color,id,space (3) as div,info from upcmast where accountid=6303 into cursor vupcmast readwrite
replace all div with getmemodata("info","DIV")

**************************************MJI CABAZON  A
************NJ

xsqlexec("select * from outship where inlist(accountid,6303,6325,6543) " + ;
	"and mod='J' and consignee='MJI CABAZON ' and del_date>{"+dtoc(date()-10)+"} and dylog=0",,,"wh")

select 'NJ' as whse, a.wo_num, a.accountid, a.ship_ref as pt, a.del_date as ship_date, a.consignee, b.style, b.color, b.id, b.totqty, b.ucc as carton ;
	from outship a, tempdata b where a.outshipid=b.outshipid into cursor aj1 readwrite

select distinct accountid, wo_num from aj1 into cursor aj2 readwrite

select outship
scan
	select aj2
	locate for aj2.accountid=outship.accountid and aj2.wo_num=outship.wo_num
	if found("AJ2")
		xsqlexec("update outship set dylog=1 where outshipid="+transform(outship.outshipid),,,"wh")
	endif   
endscan

**************************************MJI CABAZON  A
************ML

xsqlexec("select * from outship where inlist(accountid,6303,6325,6543) " + ;
	"and mod='L' and consignee='MJI CABAZON ' and del_date>{"+dtoc(date()-10)+"} and dylog=0",,,"wh")

select 'CA' as whse, a.wo_num, a.accountid, a.ship_ref as pt, a.del_date as ship_date, a.consignee, b.style, b.color, b.id, b.totqty, b.ucc as carton ;
	from outship a, tempdata b where a.outshipid=b.outshipid into cursor al1 readwrite

select distinct accountid, wo_num from al1 into cursor al2 readwrite

select outship
scan
	select al2
	locate for accountid=outship.accountid and wo_num=outship.wo_num
	if found()
		xsqlexec("update outship set dylog=1 where outshipid="+transform(outship.outshipid),,,"wh")
	endif   
endscan

select * from aj1 union all select * from al1 into cursor a3 order by pt, carton, style,color,id readwrite

select a.*, b.div from a3 a left join vupcmast b on a.style=b.style and a.color=b.color and a.id=b.id into cursor a4
select a4
if reccount() > 0
	export to "S:\MarcJacobsData\TEMP\mj_cabazon_shpmnt"   type xl5
*!*	tsendto = "tmarg@fmiint.com,l.lopez@marcjacobs.com,c.clark@marcjacobs.com,j.alba@marcjacobs,Store_MBM_Deserthills@marcjacobs.com,j.ortiz@marcjacobs.com,j.leone@marcjacobs.com"
	tsendto = "tmarg@fmiint.com,DesertHills@marcjacobs.com,marcjacobs-us-retailshipmentnotification@marcjacobs.com"
	tattach = "S:\MarcJacobsData\TEMP\mj_cabazon_shpmnt.xls"
	tcc =""
	tfrom ="Toll EDI System Operations <fmi-transload-ops@fmiint.com>"
	tmessage = "MJ CABAZON SHPMNT FOR:  "+dtoc(date()-1)
	tsubject = "MJ CABAZON SHPMNT"
	do form m:\dev\frm\dartmail2 with tsendto,tfrom,tsubject, tcc,tattach,tmessage  ,"A"

else
endif


**************************************MJI LAS VEGAS OUTLET  B
************NJ

xsqlexec("select wo_num, accountid, ship_ref, del_date, consignee, outshipid from outship where inlist(accountid,6303,6325,6543) " + ;
	"and mod='J' and consignee='MJI LAS VEGAS OUTLET' and del_date>{"+dtoc(date()-10)+"} and dylog=0",,,"wh")

select 'NJ' as whse, a.wo_num, a.accountid, a.ship_ref as pt, a.del_date as ship_date, a.consignee, b.style, b.color, b.id, b.totqty, b.ucc as carton ;
	from outship a, tempdata b where a.outshipid=b.outshipid into cursor bj1 readwrite

select distinct accountid, wo_num from bj1 into cursor bj2 readwrite

select outship
scan
	select bj2
	locate for bj2.accountid=outship.accountid and bj2.wo_num=outship.wo_num
	if found("BJ2")
		xsqlexec("update outship set dylog=1 where outshipid="+transform(outship.outshipid),,,"wh")
	endif   
endscan

**************************************MJI LAS VEGAS OUTLET  B
************ML

xsqlexec("select wo_num, accountid, ship_ref, del_date, consignee, outshipid from outship where inlist(accountid,6303,6325,6543) " + ;
	"and mod='L' and consignee='MJI LAS VEGAS OUTLET' and del_date>{"+dtoc(date()-10)+"} and dylog=0",,,"wh")

select 'CA' as whse, a.wo_num, a.accountid, a.ship_ref as pt, a.del_date as ship_date, a.consignee, b.style, b.color, b.id, b.totqty, b.ucc as carton ;
	from outship a, tempdata b where a.outshipid=b.outshipid into cursor bl1 readwrite

select distinct accountid, wo_num from bl1 into cursor bl2 readwrite

select outship
scan
	select bl2
	locate for bl2.accountid=outship.accountid and bl2.wo_num=outship.wo_num
	if found("BL2")
		xsqlexec("update outship set dylog=1 where outshipid="+transform(outship.outshipid),,,"wh")
	endif   
endscan

select * from bj1 union all select * from bl1 into cursor b3 order by pt, carton, style,color,id readwrite

select a.*, b.div from b3 a left join vupcmast b on a.style=b.style and a.color=b.color and a.id=b.id into cursor b4
select b4
if reccount() > 0
	export to "S:\MarcJacobsData\TEMP\mj_las_vegas_shpmnt"   type xl5
*!*	tsendto = "tmarg@fmiint.com,l.lopez@marcjacobs.com,m.castillo@marcjacobs.com,s.tarver@marcjacobs.com,l.morato@marcjacobs.com,w.price@marcjaobs.com,MarcJacobsLasVegasOutlet@marcjacobs.com,j.ortiz@marcjacobs.com,j.leone@marcjacobs.com"
	tsendto = "tmarg@fmiint.com,LasVegasPremium@marcjacobs.com,marcjacobs-us-retailshipmentnotification@marcjacobs.com"
	tattach = "S:\MarcJacobsData\TEMP\mj_las_vegas_shpmnt.xls"
	tcc =""
	tfrom ="Toll EDI System Operations <fmi-transload-ops@fmiint.com>"
	tmessage = "MJ LAS VEGAS OUTLET SHPMNT FOR:  "+dtoc(date()-1)
	tsubject = "MJ LAS VEGAS OUTLET SHPMNT"
	do form m:\dev\frm\dartmail2 with tsendto,tfrom,tsubject, tcc,tattach,tmessage  ,"A"
else
endif


**************************************MJI BEAUTY C
************NJ

xsqlexec("select wo_num, accountid, ship_ref, del_date, consignee, outshipid from outship where inlist(accountid,6303,6325,6543) " + ;
	"and mod='J' and consignee='MJI BEAUTY' and del_date>{"+dtoc(date()-10)+"} and dylog=0",,,"wh")

select 'NJ' as whse, a.wo_num, a.accountid, a.ship_ref as pt, a.del_date as ship_date, a.consignee, b.style, b.color, b.id, b.totqty, b.ucc as carton ;
	from outship a, tempdata b where a.outshipid=b.outshipid into cursor cj1 readwrite

select distinct accountid, wo_num from cj1 into cursor cj2 readwrite

select outship
scan
	select cj2
	locate for cj2.accountid=outship.accountid and cj2.wo_num=outship.wo_num
	if found("CJ2")
		xsqlexec("update outship set dylog=1 where outshipid="+transform(outship.outshipid),,,"wh")
	endif   
endscan

**************************************MJI BEAUTY C
************ML

xsqlexec("select wo_num, accountid, ship_ref, del_date, consignee, outshipid from outship where inlist(accountid,6303,6325,6543) " + ;
	"and mod='L' and consignee='MJI BEAUTY' and del_date>{"+dtoc(date()-10)+"} and dylog=0",,,"wh")

select 'CA' as whse, a.wo_num, a.accountid, a.ship_ref as pt, a.del_date as ship_date, a.consignee, b.style, b.color, b.id, b.totqty, b.ucc as carton ;
	from outship a, tempdata b where a.outshipid=b.outshipid into cursor cl1 readwrite

select distinct accountid, wo_num from cl1 into cursor cl2 readwrite

select outship
scan
	select cl2
	locate for cl2.accountid=outship.accountid and cl2.wo_num=outship.wo_num
	if found("CJ2")
		xsqlexec("update outship set dylog=1 where outshipid="+transform(outship.outshipid),,,"wh")
	endif   
endscan

select * from cj1 union all select * from cl1 into cursor c3 order by pt, carton, style,color,id readwrite
select a.*, b.div from c3 a left join vupcmast b on a.style=b.style and a.color=b.color and a.id=b.id into cursor c4
select c4
if reccount() > 0
	export to "S:\MarcJacobsData\TEMP\MJI_BEAUTY"   type xl5
	tsendto = "tmarg@fmiint.com,l.lopez@marcjacobs.com,E.SKRZYNIARZ@marcjacobs.com,Beauty@marcjacobs.com,j.ortiz@marcjacobs.com,j.leone@marcjacobs.com"
	tattach = "S:\MarcJacobsData\TEMP\MJI_BEAUTY.xls"
	tcc =""
	tfrom ="Toll EDI System Operations <fmi-transload-ops@fmiint.com>"
	tmessage = "MJI BEAUTY SHPMNT FOR:  "+dtoc(date()-1)
	tsubject = "MJI BEAUTY SHPMNT"
	do form m:\dev\frm\dartmail2 with tsendto,tfrom,tsubject, tcc,tattach,tmessage  ,"A"
else
endif



**************************************MJI BOOKMARC LOS ANGELES  D
************NJ

xsqlexec("select wo_num, accountid, ship_ref, del_date, consignee, outshipid from outship where inlist(accountid,6303,6325,6543) " + ;
	"and mod='J' and consignee='MJI BOOKMARC LOS ANGELES' and del_date>{"+dtoc(date()-10)+"} and dylog=0",,,"wh")

select 'NJ' as whse, a.wo_num, a.accountid, a.ship_ref as pt, a.del_date as ship_date, a.consignee, b.style, b.color, b.id, b.totqty, b.ucc as carton ;
	from outship a, tempdata b where a.outshipid=b.outshipid into cursor dj1 readwrite

select distinct accountid, wo_num from dj1 into cursor dj2 readwrite

select outship
scan
	select dj2
	locate for dj2.accountid=outship.accountid and dj2.wo_num=outship.wo_num
	if found("DJ2")
		xsqlexec("update outship set dylog=1 where outshipid="+transform(outship.outshipid),,,"wh")
	endif   
endscan

**************************************MJI BOOKMARC LOS ANGELES  D
************ML

xsqlexec("select wo_num, accountid, ship_ref, del_date, consignee, outshipid from outship where inlist(accountid,6303,6325,6543) " + ;
	"and mod='L' and consignee='MJI BOOKMARC LOS ANGELES' and del_date>{"+dtoc(date()-10)+"} and dylog=0",,,"wh")

select 'CA' as whse, a.wo_num, a.accountid, a.ship_ref as pt, a.del_date as ship_date, a.consignee, b.style, b.color, b.id, b.totqty, b.ucc as carton ;
	from outship a, tempdata b where a.outshipid=b.outshipid into cursor dl1 readwrite

select distinct accountid, wo_num from dl1 into cursor dl2 readwrite

select outship
scan
	select dl2
	locate for accountid=outship.accountid and wo_num=outship.wo_num
	if found()
		xsqlexec("update outship set dylog=1 where outshipid="+transform(outship.outshipid),,,"wh")
	endif   
endscan

select * from dj1 union all select * from dl1 into cursor d3 order by pt, carton, style,color,id readwrite
select a.*, b.div from d3 a left join vupcmast b on a.style=b.style and a.color=b.color and a.id=b.id into cursor d4
select d4
if reccount() > 0
	export to "S:\MarcJacobsData\TEMP\MJI_BOOKMARC_LOS_ANGELES"   type xl5
	tsendto = "tmarg@fmiint.com,l.lopez@marcjacobs.com,E.SKRZYNIARZ@marcjacobs.com,BookMarc_LA@marcjacobs.com,j.ortiz@marcjacobs.com,j.leone@marcjacobs.com"
	tattach = "S:\MarcJacobsData\TEMP\MJI_BOOKMARC_LOS_ANGELES.xls"
	tcc =""
	tfrom ="Toll EDI System Operations <fmi-transload-ops@fmiint.com>"
	tmessage = "MJI BOOKMARC LOS ANGELES SHPMNT FOR:  "+dtoc(date()-1)
	tsubject = "MJI BOOKMARC LOS ANGELES SHPMNT"
	do form m:\dev\frm\dartmail2 with tsendto,tfrom,tsubject, tcc,tattach,tmessage  ,"A"
else
endif



**************************************MJI BOOKMARC NEW YORK  E
************NJ

xsqlexec("select wo_num, accountid, ship_ref, del_date, consignee, outshipid from outship where inlist(accountid,6303,6325,6543) " + ;
	"and mod='J' and consignee='MJI BOOKMARC NEW YORK' and del_date>{"+dtoc(date()-10)+"} and dylog=0",,,"wh")

select 'NJ' as whse, a.wo_num, a.accountid, a.ship_ref as pt, a.del_date as ship_date, a.consignee, b.style, b.color, b.id, b.totqty, b.ucc as carton ;
	from outship a, tempdata b where a.outshipid=b.outshipid into cursor ej1 readwrite

select distinct accountid, wo_num from ej1 into cursor ej2 readwrite

select outship
scan
	select ej2
	locate for ej2.accountid=outship.accountid and ej2.wo_num=outship.wo_num
	if found("EJ2")
		xsqlexec("update outship set dylog=1 where outshipid="+transform(outship.outshipid),,,"wh")
	endif   
endscan

**************************************MJI BOOKMARC NEW YORK  E
************ML

xsqlexec("select wo_num, accountid, ship_ref, del_date, consignee, outshipid from outship where inlist(accountid,6303,6325,6543) " + ;
	"and mod='L' and consignee='MJI BOOKMARC NEW YORK' and del_date>{"+dtoc(date()-10)+"} and dylog=0",,,"wh")

select 'CA' as whse, a.wo_num, a.accountid, a.ship_ref as pt, a.del_date as ship_date, a.consignee, b.style, b.color, b.id, b.totqty, b.ucc as carton ;
	from outship a, tempdata b where a.outshipid=b.outshipid into cursor el1 readwrite

select distinct accountid, wo_num from el1 into cursor el2 readwrite

select outship
scan
	select el2
	locate for accountid=outship.accountid and wo_num=outship.wo_num
	if found()
		xsqlexec("update outship set dylog=1 where outshipid="+transform(outship.outshipid),,,"wh")
	endif   
endscan

select * from ej1 union all select * from el1 into cursor e3 order by pt, carton, style,color,id readwrite
select a.*, b.div from e3 a left join vupcmast b on a.style=b.style and a.color=b.color and a.id=b.id into cursor e4
select e4
if reccount() > 0
	export to "S:\MarcJacobsData\TEMP\MJI_BOOKMARC_NEW_YORK"   type xl5
*!*	tsendto = "tmarg@fmiint.com,l.lopez@marcjacobs.com,E.SKRZYNIARZ@marcjacobs.com,BookMarc@marcjacobs.com,j.ortiz@marcjacobs.com,j.leone@marcjacobs.com"
	tsendto = "tmarg@fmiint.com,Bookmarc@marcjacobs.com,marcjacobs-us-retailshipmentnotification@marcjacobs.com"	
	tattach = "S:\MarcJacobsData\TEMP\MJI_BOOKMARC_NEW_YORK.xls"
	tcc =""
	tfrom ="Toll EDI System Operations <fmi-transload-ops@fmiint.com>"
	tmessage = "MJI BOOKMARC NEW YORK SHPMNT FOR:  "+dtoc(date()-1)
	tsubject = "MJI BOOKMARC NEW YORK SHPMNT"
	do form m:\dev\frm\dartmail2 with tsendto,tfrom,tsubject, tcc,tattach,tmessage  ,"A"
else
endif




**************************************MJI LITTLE MARC JACOBS NEW YORK  F
************NJ

xsqlexec("select wo_num, accountid, ship_ref, del_date, consignee, outshipid from outship where inlist(accountid,6303,6325,6543) " + ;
	"and mod='J' and consignee='MJI LITTLE MARC JACOBS NEW YORK' and del_date>{"+dtoc(date()-10)+"} and dylog=0",,,"wh")

select 'NJ' as whse, a.wo_num, a.accountid, a.ship_ref as pt, a.del_date as ship_date, a.consignee, b.style, b.color, b.id, b.totqty, b.ucc as carton ;
	from outship a, tempdata b where a.outshipid=b.outshipid into cursor fj1 readwrite

select distinct accountid, wo_num from fj1 into cursor fj2 readwrite

select outship
scan
	select fj2
	locate for fj2.accountid=outship.accountid and fj2.wo_num=outship.wo_num
	if found("FJ2")
		xsqlexec("update outship set dylog=1 where outshipid="+transform(outship.outshipid),,,"wh")
	endif   
endscan

**************************************MJI LITTLE MARC JACOBS NEW YORK  F
************ML

xsqlexec("select wo_num, accountid, ship_ref, del_date, consignee, outshipid from outship where inlist(accountid,6303,6325,6543) " + ;
	"and mod='L' and consignee='MJI LITTLE MARC JACOBS NEW YORK' and del_date>{"+dtoc(date()-10)+"} and dylog=0",,,"wh")

select 'CA' as whse, a.wo_num, a.accountid, a.ship_ref as pt, a.del_date as ship_date, a.consignee, b.style, b.color, b.id, b.totqty, b.ucc as carton ;
	from outship a, tempdata b where a.outshipid=b.outshipid into cursor fl1 readwrite

select distinct accountid, wo_num from fl1 into cursor fl2 readwrite

select outship
scan
	select fl2
	locate for accountid=outship.accountid and wo_num=outship.wo_num
	if found()
		xsqlexec("update outship set dylog=1 where outshipid="+transform(outship.outshipid),,,"wh")
	endif   
endscan

select * from fj1 union all select * from fl1 into cursor f3 order by pt, carton, style,color,id readwrite
select a.*, b.div from f3 a left join vupcmast b on a.style=b.style and a.color=b.color and a.id=b.id into cursor f4
select f4
if reccount() > 0
	export to "S:\MarcJacobsData\TEMP\MJI_LITTLE_MARC_NEW_YORK"   type xl5
	tsendto = "tmarg@fmiint.com,l.lopez@marcjacobs.com,E.SKRZYNIARZ@marcjacobs.com,LittleMarcJacobs@marcjacobs.com,j.ortiz@marcjacobs.com,j.leone@marcjacobs.com"
	tattach = "S:\MarcJacobsData\TEMP\MJI_LITTLE_MARC_NEW_YORK.xls"
	tcc =""
	tfrom ="Toll EDI System Operations <fmi-transload-ops@fmiint.com>"
	tmessage = "MJI LITTLE MARC JACOBS NEW YORK SHPMNT FOR:  "+dtoc(date()-1)
	tsubject = "MJI LITTLE MARC JACOBS NEW YORK SHPMNT"
	do form m:\dev\frm\dartmail2 with tsendto,tfrom,tsubject, tcc,tattach,tmessage  ,"A"
else
endif



**************************************MJI MJC CHICAGO  G
************NJ

xsqlexec("select wo_num, accountid, ship_ref, del_date, consignee, outshipid from outship where inlist(accountid,6303,6325,6543) " + ;
	"and mod='J' and consignee='MJI MJC CHICAGO' and del_date>{"+dtoc(date()-10)+"} and dylog=0",,,"wh")

select 'NJ' as whse, a.wo_num, a.accountid, a.ship_ref as pt, a.del_date as ship_date, a.consignee, b.style, b.color, b.id, b.totqty, b.ucc as carton ;
	from outship a, tempdata b where a.outshipid=b.outshipid into cursor gj1 readwrite

select distinct accountid, wo_num from gj1 into cursor gj2 readwrite

select outship
scan
	select gj2
	locate for gj2.accountid=outship.accountid and gj2.wo_num=outship.wo_num
	if found("GJ2")
		xsqlexec("update outship set dylog=1 where outshipid="+transform(outship.outshipid),,,"wh")
	endif   
endscan

**************************************MJI MJC CHICAGO  G
************ML

xsqlexec("select wo_num, accountid, ship_ref, del_date, consignee, outshipid from outship where inlist(accountid,6303,6325,6543) " + ;
	"and mod='L' and consignee='MJI MJC CHICAGO' and del_date>{"+dtoc(date()-10)+"} and dylog=0",,,"wh")

select 'CA' as whse, a.wo_num, a.accountid, a.ship_ref as pt, a.del_date as ship_date, a.consignee, b.style, b.color, b.id, b.totqty, b.ucc as carton ;
	from outship a, tempdata b where a.outshipid=b.outshipid into cursor gl1 readwrite

select distinct accountid, wo_num from gl1 into cursor gl2 readwrite

select outship
scan
	select gl2
	locate for accountid=outship.accountid and wo_num=outship.wo_num
	if found()
		xsqlexec("update outship set dylog=1 where outshipid="+transform(outship.outshipid),,,"wh")
	endif   
endscan

select * from gj1 union all select * from gl1 into cursor g3 order by pt, carton, style,color,id readwrite
select a.*, b.div from g3 a left join vupcmast b on a.style=b.style and a.color=b.color and a.id=b.id into cursor g4
select g4
if reccount() > 0
	export to "S:\MarcJacobsData\TEMP\MJI_MJC_CHICAGO"   type xl5
*!*	tsendto = "tmarg@fmiint.com,l.lopez@marcjacobs.com,E.SKRZYNIARZ@marcjacobs.com,Store_Chicago_Collection@marcjacobs.com,j.ortiz@marcjacobs.com,j.leone@marcjacobs.com"
	tsendto = "tmarg@fmiint.com,Chicago@marcjacobs.com,marcjacobs-us-retailshipmentnotification@marcjacobs.com"
	tattach = "S:\MarcJacobsData\TEMP\MJI_MJC_CHICAGO.xls"
	tcc =""
	tfrom ="Toll EDI System Operations <fmi-transload-ops@fmiint.com>"
	tmessage = "MJI MJC CHICAGO SHPMNT FOR:  "+dtoc(date()-1)
	tsubject = "MJI MJC CHICAGO SHPMNT"
	do form m:\dev\frm\dartmail2 with tsendto,tfrom,tsubject, tcc,tattach,tmessage  ,"A"
else
endif




**************************************MJI MJC LOS ANGELES  H
************NJ

xsqlexec("select wo_num, accountid, ship_ref, del_date, consignee, outshipid from outship where inlist(accountid,6303,6325,6543) " + ;
	"and mod='J' and consignee='MJI MJC LOS ANGELES' and del_date>{"+dtoc(date()-10)+"} and dylog=0",,,"wh")

select 'NJ' as whse, a.wo_num, a.accountid, a.ship_ref as pt, a.del_date as ship_date, a.consignee, b.style, b.color, b.id, b.totqty, b.ucc as carton ;
	from outship a, tempdata b where a.outshipid=b.outshipid into cursor hj1 readwrite

select distinct accountid, wo_num from hj1 into cursor hj2 readwrite

select outship
scan
	select hj2
	locate for accountid=outship.accountid and wo_num=outship.wo_num
	if found()
		xsqlexec("update outship set dylog=1 where outshipid="+transform(outship.outshipid),,,"wh")
	endif   
endscan

**************************************MJI MJC LOS ANGELES  H
************ML

xsqlexec("select wo_num, accountid, ship_ref, del_date, consignee, outshipid from outship where inlist(accountid,6303,6325,6543) " + ;
	"and mod='L' and consignee='MJI MJC LOS ANGELES' and del_date>{"+dtoc(date()-10)+"} and dylog=0",,,"wh")

select 'CA' as whse, a.wo_num, a.accountid, a.ship_ref as pt, a.del_date as ship_date, a.consignee, b.style, b.color, b.id, b.totqty, b.ucc as carton ;
	from outship a, tempdata b where a.outshipid=b.outshipid into cursor hl1 readwrite

select distinct accountid, wo_num from hl1 into cursor hl2 readwrite

select outship
scan
	select hl2
	locate for accountid=outship.accountid and wo_num=outship.wo_num
	if found()
		xsqlexec("update outship set dylog=1 where outshipid="+transform(outship.outshipid),,,"wh")
	endif   
endscan

select * from hj1 union all select * from hl1 into cursor h3 order by pt, carton, style,color,id readwrite
select a.*, b.div from h3 a left join vupcmast b on a.style=b.style and a.color=b.color and a.id=b.id into cursor h4
select h4
if reccount() > 0
	export to "S:\MarcJacobsData\TEMP\MJI_MJC_LOS_ANGELES"  type xl5
*!*	tsendto = "tmarg@fmiint.com,l.lopez@marcjacobs.com,E.SKRZYNIARZ@marcjacobs.com,Store_Los_Angeles_Marc_Jacobs_Collection@marcjacobs.com,j.ortiz@marcjacobs.com,j.leone@marcjacobs.com"
	tsendto = "tmarg@fmiint.com,LosAngeles@marcjacobs.com,marcjacobs-us-retailshipmentnotification@marcjacobs.com"
	tattach = "S:\MarcJacobsData\TEMP\MJI_MJC_LOS_ANGELES.xls"
	tcc =""
	tfrom ="Toll EDI System Operations <fmi-transload-ops@fmiint.com>"
	tmessage = "MJI MJC LOS ANGELES SHPMNT FOR:  "+dtoc(date()-1)
	tsubject = "MJI MJC LOS ANGELES SHPMNT"
	do form m:\dev\frm\dartmail2 with tsendto,tfrom,tsubject, tcc,tattach,tmessage  ,"A"
else
endif



**************************************MJI MJC NEW YORK  J
************NJ

xsqlexec("select wo_num, accountid, ship_ref, del_date, consignee, outshipid from outship where inlist(accountid,6303,6325,6543) " + ;
	"and mod='J' and consignee='MJI MJC NEW YORK ' and del_date>{"+dtoc(date()-10)+"} and dylog=0",,,"wh")

select 'NJ' as whse, a.wo_num, a.accountid, a.ship_ref as pt, a.del_date as ship_date, a.consignee, b.style, b.color, b.id, b.totqty, b.ucc as carton ;
	from outship a, tempdata b where a.outshipid=b.outshipid into cursor jj1 readwrite

select distinct accountid, wo_num from jj1 into cursor jj2 readwrite

select outship
scan
	select jj2
	locate for accountid=outship.accountid and wo_num=outship.wo_num
	if found()
		xsqlexec("update outship set dylog=1 where outshipid="+transform(outship.outshipid),,,"wh")
	endif   
endscan

**************************************MJI MJC NEW YORK  J
************ML

xsqlexec("select wo_num, accountid, ship_ref, del_date, consignee, outshipid from outship where inlist(accountid,6303,6325,6543) " + ;
	"and mod='L' and consignee='MJI MJC NEW YORK ' and del_date>{"+dtoc(date()-10)+"} and dylog=0",,,"wh")

select 'CA' as whse, a.wo_num, a.accountid, a.ship_ref as pt, a.del_date as ship_date, a.consignee, b.style, b.color, b.id, b.totqty, b.ucc as carton ;
	from outship a, tempdata b where a.outshipid=b.outshipid into cursor jl1 readwrite

select distinct accountid, wo_num from jl1 into cursor jl2 readwrite

select outship
scan
	select jl2
	locate for accountid=outship.accountid and wo_num=outship.wo_num
	if found()
		xsqlexec("update outship set dylog=1 where outshipid="+transform(outship.outshipid),,,"wh")
	endif   
endscan

select * from jj1 union all select * from jl1 into cursor j3 order by pt, carton, style,color,id readwrite
select a.*, b.div from j3 a left join vupcmast b on a.style=b.style and a.color=b.color and a.id=b.id into cursor j4
select j4
if reccount() > 0
	export to "S:\MarcJacobsData\TEMP\MJI_MJC_NEW_YORK"  type xl5
	tsendto = "tmarg@fmiint.com,Prince@marcjacobs.com,marcjacobs-us-retailshipmentnotification@marcjacobs.com"
*!*	tsendto = "tmarg@fmiint.com,l.lopez@marcjacobs.com,E.SKRZYNIARZ@marcjacobs.com,Store_Mercer_Marc_Jacobs_Collection@marcjacobs.com,j.ortiz@marcjacobs.com,j.leone@marcjacobs.com"
	tattach = "S:\MarcJacobsData\TEMP\MJI_MJC_NEW_YORK.xls"
	tcc =""
	tfrom ="Toll EDI System Operations <fmi-transload-ops@fmiint.com>"
	tmessage = "MJI MJC NEW YORK SHPMNT FOR:  "+dtoc(date()-1)
	tsubject = "MJI MJC NEW YORK SHPMNT"
	do form m:\dev\frm\dartmail2 with tsendto,tfrom,tsubject, tcc,tattach,tmessage  ,"A"
else
endif


**************************************MJI MJC NEW YORK ACCESSORIES  K
************NJ

xsqlexec("select wo_num, accountid, ship_ref, del_date, consignee, outshipid from outship where inlist(accountid,6303,6325,6543) " + ;
	"and mod='J' and consignee='MJI MJC NEW YORK ACCESSORIES' and del_date>{"+dtoc(date()-10)+"} and dylog=0",,,"wh")

select 'NJ' as whse, a.wo_num, a.accountid, a.ship_ref as pt, a.del_date as ship_date, a.consignee, b.style, b.color, b.id, b.totqty, b.ucc as carton ;
	from outship a, tempdata b where a.outshipid=b.outshipid into cursor kj1 readwrite

select distinct accountid, wo_num from kj1 into cursor kj2 readwrite

select outship
scan
	select kj2
	locate for accountid=outship.accountid and wo_num=outship.wo_num
	if found()
		xsqlexec("update outship set dylog=1 where outshipid="+transform(outship.outshipid),,,"wh")
	endif   
endscan

**************************************MJI MJC NEW YORK ACCESSORIES  K
************ML

xsqlexec("select wo_num, accountid, ship_ref, del_date, consignee, outshipid from outship where inlist(accountid,6303,6325,6543) " + ;
	"and mod='L' and consignee='MJI MJC NEW YORK ACCESSORIES' and del_date>{"+dtoc(date()-10)+"} and dylog=0",,,"wh")

select 'CA' as whse, a.wo_num, a.accountid, a.ship_ref as pt, a.del_date as ship_date, a.consignee, b.style, b.color, b.id, b.totqty, b.ucc as carton ;
	from outship a, tempdata b where a.outshipid=b.outshipid into cursor kl1 readwrite

select distinct accountid, wo_num from al1 into cursor kl2 readwrite

select outship
scan
	select kl2
	locate for accountid=outship.accountid and wo_num=outship.wo_num
	if found()
		xsqlexec("update outship set dylog=1 where outshipid="+transform(outship.outshipid),,,"wh")
	endif   
endscan

select * from kj1 union all select * from kl1 into cursor k3 order by pt, carton, style,color,id readwrite
select a.*, b.div from k3 a left join vupcmast b on a.style=b.style and a.color=b.color and a.id=b.id into cursor k4
select k4
if reccount() > 0
	export to "S:\MarcJacobsData\TEMP\MJI_MJC_NEW_YORK_ACCESSORIES"  type xl5
	tsendto = "tmarg@fmiint.com,l.lopez@marcjacobs.com,E.SKRZYNIARZ@marcjacobs.com,j.ortiz@marcjacobs.com,j.leone@marcjacobs.com"
	tattach = "S:\MarcJacobsData\TEMP\MJI_MJC_NEW_YORK_ACCESSORIES.xls"
	tcc =""
	tfrom ="Toll EDI System Operations <fmi-transload-ops@fmiint.com>"
	tmessage = "MJI MJC NEW YORK ACCESSORIES SHPMNT FOR:  "+dtoc(date()-1)
	tsubject = "MJI MJC NEW YORK ACCESSORIES SHPMNT"
	do form m:\dev\frm\dartmail2 with tsendto,tfrom,tsubject, tcc,tattach,tmessage  ,"A"
else
endif




**************************************MJI MMJ BOSTON  L
************NJ

xsqlexec("select wo_num, accountid, ship_ref, del_date, consignee, outshipid from outship where inlist(accountid,6303,6325,6543) " + ;
	"and mod='J' and consignee='MJI MMJ BOSTON' and del_date>{"+dtoc(date()-10)+"} and dylog=0",,,"wh")

select 'NJ' as whse, a.wo_num, a.accountid, a.ship_ref as pt, a.del_date as ship_date, a.consignee, b.style, b.color, b.id, b.totqty, b.ucc as carton ;
	from outship a, tempdata b where a.outshipid=b.outshipid into cursor lj1 readwrite

select distinct accountid, wo_num from lj1 into cursor lj2 readwrite

select outship
scan
	select lj2
	locate for accountid=outship.accountid and wo_num=outship.wo_num
	if found()
		xsqlexec("update outship set dylog=1 where outshipid="+transform(outship.outshipid),,,"wh")
	endif   
endscan

**************************************MJI MMJ BOSTON  L
************ML

xsqlexec("select wo_num, accountid, ship_ref, del_date, consignee, outshipid from outship where inlist(accountid,6303,6325,6543) " + ;
	"and mod='L' and consignee='MJI MMJ BOSTON' and del_date>{"+dtoc(date()-10)+"} and dylog=0",,,"wh")

select 'CA' as whse, a.wo_num, a.accountid, a.ship_ref as pt, a.del_date as ship_date, a.consignee, b.style, b.color, b.id, b.totqty, b.ucc as carton ;
	from outship a, tempdata b where a.outshipid=b.outshipid into cursor ll1 readwrite

select distinct accountid, wo_num from ll1 into cursor ll2 readwrite

select outship
scan
	select ll2
	locate for accountid=outship.accountid and wo_num=outship.wo_num
	if found()
		xsqlexec("update outship set dylog=1 where outshipid="+transform(outship.outshipid),,,"wh")
	endif   
endscan

select * from lj1 union all select * from ll1 into cursor l3 order by pt, carton, style,color,id readwrite
select a.*, b.div from l3 a left join vupcmast b on a.style=b.style and a.color=b.color and a.id=b.id into cursor l4
select l4
if reccount() > 0
	export to "S:\MarcJacobsData\TEMP\MJI_MMJ_BOSTON"  type xl5
*!*	tsendto = "tmarg@fmiint.com,l.lopez@marcjacobs.com,E.SKRZYNIARZ@marcjacobs.com,Store_Boston_Marc_by_Marc_Jacobs@marcjacobs.com,j.ortiz@marcjacobs.com,j.leone@marcjacobs.com"
	tsendto = "tmarg@fmiint.com,Boston@marcjacobs.com,marcjacobs-us-retailshipmentnotification@marcjacobs.com"
	tattach = "S:\MarcJacobsData\TEMP\MJI_MMJ_BOSTON.xls"
	tcc =""
	tfrom ="Toll EDI System Operations <fmi-transload-ops@fmiint.com>"
	tmessage = "MJI MMJ BOSTON SHPMNT FOR:  "+dtoc(date()-1)
	tsubject = "MJI MMJ BOSTON SHPMNT"
	do form m:\dev\frm\dartmail2 with tsendto,tfrom,tsubject, tcc,tattach,tmessage  ,"A"
else
endif





**************************************MJI MMJ LAS VEGAS  M
************NJ

xsqlexec("select wo_num, accountid, ship_ref, del_date, consignee, outshipid from outship where inlist(accountid,6303,6325,6543) " + ;
	"and mod='J' and consignee='MJI MMJ LAS VEGAS' and del_date>{"+dtoc(date()-10)+"} and dylog=0",,,"wh")

select 'NJ' as whse, a.wo_num, a.accountid, a.ship_ref as pt, a.del_date as ship_date, a.consignee, b.style, b.color, b.id, b.totqty, b.ucc as carton ;
	from outship a, tempdata b where a.outshipid=b.outshipid into cursor mj1 readwrite

select distinct accountid, wo_num from mj1 into cursor mj2 readwrite

select outship
scan
	select mj2
	locate for accountid=outship.accountid and wo_num=outship.wo_num
	if found()
		xsqlexec("update outship set dylog=1 where outshipid="+transform(outship.outshipid),,,"wh")
	endif   
endscan

**************************************MJI MMJ LAS VEGAS  M
************ML

xsqlexec("select wo_num, accountid, ship_ref, del_date, consignee, outshipid from outship where inlist(accountid,6303,6325,6543) " + ;
	"and mod='L' and consignee='MJI MMJ LAS VEGAS' and del_date>{"+dtoc(date()-10)+"} and dylog=0",,,"wh")

select 'CA' as whse, a.wo_num, a.accountid, a.ship_ref as pt, a.del_date as ship_date, a.consignee, b.style, b.color, b.id, b.totqty, b.ucc as carton ;
	from outship a, tempdata b where a.outshipid=b.outshipid into cursor ml1 readwrite

select distinct accountid, wo_num from ml1 into cursor ml2 readwrite

select outship
scan
	select al2
	locate for accountid=outship.accountid and wo_num=outship.wo_num
	if found()
		xsqlexec("update outship set dylog=1 where outshipid="+transform(outship.outshipid),,,"wh")
	endif   
endscan

select * from mj1 union all select * from ml1 into cursor m3 order by pt, carton, style,color,id readwrite
select a.*, b.div from m3 a left join vupcmast b on a.style=b.style and a.color=b.color and a.id=b.id into cursor m4
select m4
if reccount() > 0
	export to "S:\MarcJacobsData\TEMP\MJI_MMJ_LAS_VEGAS"  type xl5
	tsendto = "tmarg@fmiint.com,l.lopez@marcjacobs.com,E.SKRZYNIARZ@marcjacobs.com,MarcByMarcJacobsLasVegas@marcjacobs.com,j.ortiz@marcjacobs.com,j.leone@marcjacobs.com"
	tattach = "S:\MarcJacobsData\TEMP\MJI_MMJ_LAS_VEGAS.xls"
	tcc =""
	tfrom ="Toll EDI System Operations <fmi-transload-ops@fmiint.com>"
	tmessage = "MJI MMJ LAS VEGAS SHPMNT FOR:  "+dtoc(date()-1)
	tsubject = "MJI MMJ LAS VEGAS SHPMNT"
	do form m:\dev\frm\dartmail2 with tsendto,tfrom,tsubject, tcc,tattach,tmessage  ,"A"
else
endif



**************************************MJI MMJ MIAM  N
************NJ

xsqlexec("select wo_num, accountid, ship_ref, del_date, consignee, outshipid from outship where inlist(accountid,6303,6325,6543) " + ;
	"and mod='J' and consignee='MJI MMJ MIAMI' and del_date>{"+dtoc(date()-10)+"} and dylog=0",,,"wh")

select 'NJ' as whse, a.wo_num, a.accountid, a.ship_ref as pt, a.del_date as ship_date, a.consignee, b.style, b.color, b.id, b.totqty, b.ucc as carton ;
	from outship a, tempdata b where a.outshipid=b.outshipid into cursor nj1 readwrite

select distinct accountid, wo_num from nj1 into cursor nj2 readwrite

select outship
scan
	select nj2
	locate for accountid=outship.accountid and wo_num=outship.wo_num
	if found()
		xsqlexec("update outship set dylog=1 where outshipid="+transform(outship.outshipid),,,"wh")
	endif   
endscan

**************************************MJI MMJ MIAM  N
************ML

xsqlexec("select wo_num, accountid, ship_ref, del_date, consignee, outshipid from outship where inlist(accountid,6303,6325,6543) " + ;
	"and mod='L' and consignee='MJI MMJ MIAMI' and del_date>{"+dtoc(date()-10)+"} and dylog=0",,,"wh")

select 'CA' as whse, a.wo_num, a.accountid, a.ship_ref as pt, a.del_date as ship_date, a.consignee, b.style, b.color, b.id, b.totqty, b.ucc as carton ;
	from outship a, tempdata b where a.outshipid=b.outshipid into cursor nl1 readwrite

select distinct accountid, wo_num from nl1 into cursor nl2 readwrite

select outship
scan
	select nl2
	locate for accountid=outship.accountid and wo_num=outship.wo_num
	if found()
		xsqlexec("update outship set dylog=1 where outshipid="+transform(outship.outshipid),,,"wh")
	endif   
endscan

select * from nj1 union all select * from nl1 into cursor n3 order by pt, carton, style,color,id readwrite
select a.*, b.div from n3 a left join vupcmast b on a.style=b.style and a.color=b.color and a.id=b.id into cursor n4
select n4
if reccount() > 0
	export to "S:\MarcJacobsData\TEMP\MJI_MMJ_MIAMI"  type xl5
	tsendto = "tmarg@fmiint.com,l.lopez@marcjacobs.com,E.SKRZYNIARZ@marcjacobs.com,Store_MBM_Miami@marcjacobs.com,j.ortiz@marcjacobs.com,j.leone@marcjacobs.com"
	tattach = "S:\MarcJacobsData\TEMP\MJI_MMJ_MIAMI.xls"
	tcc =""
	tfrom ="Toll EDI System Operations <fmi-transload-ops@fmiint.com>"
	tmessage = "MJI MMJ MIAMI SHPMNT FOR:  "+dtoc(date()-1)
	tsubject = "MJI MMJ MIAMI SHPMNT"
	do form m:\dev\frm\dartmail2 with tsendto,tfrom,tsubject, tcc,tattach,tmessage  ,"A"
else
endif



**************************************MJI MMJ NEW YORK WOMEN  P
************NJ

xsqlexec("select wo_num, accountid, ship_ref, del_date, consignee, outshipid from outship where inlist(accountid,6303,6325,6543) " + ;
	"and mod='J' and consignee='MJI MMJ NEW YORK WOMEN' and del_date>{"+dtoc(date()-10)+"} and dylog=0",,,"wh")

select 'NJ' as whse, a.wo_num, a.accountid, a.ship_ref as pt, a.del_date as ship_date, a.consignee, b.style, b.color, b.id, b.totqty, b.ucc as carton ;
	from outship a, tempdata b where a.outshipid=b.outshipid into cursor pj1 readwrite

select distinct accountid, wo_num from pj1 into cursor pj2 readwrite

select outship
scan
	select pj2
	locate for accountid=outship.accountid and wo_num=outship.wo_num
	if found()
		xsqlexec("update outship set dylog=1 where outshipid="+transform(outship.outshipid),,,"wh")
	endif   
endscan

**************************************MJI MMJ NEW YORK WOMEN  P
************ML

xsqlexec("select wo_num, accountid, ship_ref, del_date, consignee, outshipid from outship where inlist(accountid,6303,6325,6543) " + ;
	"and mod='L' and consignee='MJI MMJ NEW YORK WOMEN' and del_date>{"+dtoc(date()-10)+"} and dylog=0",,,"wh")

select 'CA' as whse, a.wo_num, a.accountid, a.ship_ref as pt, a.del_date as ship_date, a.consignee, b.style, b.color, b.id, b.totqty, b.ucc as carton ;
	from outship a, tempdata b where a.outshipid=b.outshipid into cursor pl1 readwrite

select distinct accountid, wo_num from pl1 into cursor pl2 readwrite

select outship
scan
	select pl2
	locate for accountid=outship.accountid and wo_num=outship.wo_num
	if found()
		xsqlexec("update outship set dylog=1 where outshipid="+transform(outship.outshipid),,,"wh")
	endif   
endscan

select * from pj1 union all select * from pl1 into cursor p3 order by pt, carton, style,color,id readwrite
select a.*, b.div from p3 a left join vupcmast b on a.style=b.style and a.color=b.color and a.id=b.id into cursor p4
select p4
if reccount() > 0
	export to "S:\MarcJacobsData\TEMP\MJI_MMJ_NEW_YORK_WOMEN"  type xl5
	tsendto = "tmarg@fmiint.com,l.lopez@marcjacobs.com,E.SKRZYNIARZ@marcjacobs.com,Store_Bleecker@marcjacobs.com,j.ortiz@marcjacobs.com,j.leone@marcjacobs.com"
	tattach = "S:\MarcJacobsData\TEMP\MJI_MMJ_NEW_YORK_WOMEN.xls"
	tcc =""
	tfrom ="Toll EDI System Operations <fmi-transload-ops@fmiint.com>"
	tmessage = "MJI MMJ NEW YORK WOMEN SHPMNT FOR:  "+dtoc(date()-1)
	tsubject = "MJI MMJ NEW YORK WOMEN SHPMNT"
	do form m:\dev\frm\dartmail2 with tsendto,tfrom,tsubject, tcc,tattach,tmessage  ,"A"
else
endif


**************************************MJI MMJ SAN FRANCISCO  R
************NJ

xsqlexec("select wo_num, accountid, ship_ref, del_date, consignee, outshipid from outship where inlist(accountid,6303,6325,6543) " + ;
	"and mod='J' and consignee='MJI MMJ SAN FRANCISCO' and del_date>{"+dtoc(date()-10)+"} and dylog=0",,,"wh")

select 'NJ' as whse, a.wo_num, a.accountid, a.ship_ref as pt, a.del_date as ship_date, a.consignee, b.style, b.color, b.id, b.totqty, b.ucc as carton ;
	from outship a, tempdata b where a.outshipid=b.outshipid into cursor rj1 readwrite

select distinct accountid, wo_num from rj1 into cursor rj2 readwrite

select outship
scan
	select rj2
	locate for accountid=outship.accountid and wo_num=outship.wo_num
	if found()
		xsqlexec("update outship set dylog=1 where outshipid="+transform(outship.outshipid),,,"wh")
	endif   
endscan

**************************************MJI MMJ SAN FRANCISCO  R
************ML

xsqlexec("select wo_num, accountid, ship_ref, del_date, consignee, outshipid from outship where inlist(accountid,6303,6325,6543) " + ;
	"and mod='L' and consignee='MJI MMJ SAN FRANCISCO' and del_date>{"+dtoc(date()-10)+"} and dylog=0",,,"wh")

select 'CA' as whse, a.wo_num, a.accountid, a.ship_ref as pt, a.del_date as ship_date, a.consignee, b.style, b.color, b.id, b.totqty, b.ucc as carton ;
	from outship a, tempdata b where a.outshipid=b.outshipid into cursor rl1 readwrite

select distinct accountid, wo_num from rl1 into cursor rl2 readwrite

select outship
scan
	select rl2
	locate for accountid=outship.accountid and wo_num=outship.wo_num
	if found()
		xsqlexec("update outship set dylog=1 where outshipid="+transform(outship.outshipid),,,"wh")
	endif   
endscan

select * from rj1 union all select * from rl1 into cursor r3 order by pt, carton, style,color,id readwrite
select a.*, b.div from r3 a left join vupcmast b on a.style=b.style and a.color=b.color and a.id=b.id into cursor r4
select r4
if reccount() > 0
	export to "S:\MarcJacobsData\TEMP\MJI_MMJ_SAN_FRANCISCO"  type xl5
	tsendto = "tmarg@fmiint.com,l.lopez@marcjacobs.com,E.SKRZYNIARZ@marcjacobs.com,Store_San_Francisco@marcjacobs.com,j.ortiz@marcjacobs.com,j.leone@marcjacobs.com"
	tattach = "S:\MarcJacobsData\TEMP\MJI_MMJ_SAN_FRANCISCO.xls"
	tcc =""
	tfrom ="Toll EDI System Operations <fmi-transload-ops@fmiint.com>"
	tmessage = "MJI MMJ SAN FRANCISCO SHPMNT FOR:  "+dtoc(date()-1)
	tsubject = "MJI MMJ SAN FRANCISCO SHPMNT"
	do form m:\dev\frm\dartmail2 with tsendto,tfrom,tsubject, tcc,tattach,tmessage  ,"A"
else
endif




**************************************MJI SAWGRASS OUTLET  S
************NJ
xsqlexec("select wo_num, accountid, ship_ref, del_date, consignee, outshipid from outship where inlist(accountid,6303,6325,6543) " + ;
	"and mod='J' and consignee='MJI SAWGRASS OUTLET' and del_date>{"+dtoc(date()-10)+"} and dylog=0",,,"wh")

select 'NJ' as whse, a.wo_num, a.accountid, a.ship_ref as pt, a.del_date as ship_date, a.consignee, b.style, b.color, b.id, b.totqty, b.ucc as carton ;
	from outship a, tempdata b where a.outshipid=b.outshipid into cursor sj1 readwrite

select distinct accountid, wo_num from sj1 into cursor sj2 readwrite

select outship
scan
	select sj2
	locate for accountid=outship.accountid and wo_num=outship.wo_num
	if found()
		xsqlexec("update outship set dylog=1 where outshipid="+transform(outship.outshipid),,,"wh")
	endif   
endscan

**************************************MJI SAWGRASS OUTLET  S
************ML

xsqlexec("select wo_num, accountid, ship_ref, del_date, consignee, outshipid from outship where inlist(accountid,6303,6325,6543) " + ;
	"and mod='L' and consignee='MJI SAWGRASS OUTLET' and del_date>{"+dtoc(date()-10)+"} and dylog=0",,,"wh")

select 'CA' as whse, a.wo_num, a.accountid, a.ship_ref as pt, a.del_date as ship_date, a.consignee, b.style, b.color, b.id, b.totqty, b.ucc as carton ;
	from outship a, tempdata b where a.outshipid=b.outshipid into cursor sl1 readwrite

select distinct accountid, wo_num from sl1 into cursor sl2 readwrite

select outship
scan
	select sl2
	locate for accountid=outship.accountid and wo_num=outship.wo_num
	if found()
		xsqlexec("update outship set dylog=1 where outshipid="+transform(outship.outshipid),,,"wh")
	endif   
endscan

select * from sj1 union all select * from sl1 into cursor s3 order by pt, carton, style,color,id readwrite
select a.*, b.div from s3 a left join vupcmast b on a.style=b.style and a.color=b.color and a.id=b.id into cursor s4
select s4
if reccount() > 0
	export to "S:\MarcJacobsData\TEMP\MJI_SAWGRASS"  type xl5
	tsendto = "tmarg@fmiint.com,SawgrassMills@marcjacobs.com,marcjacobs-us-retailshipmentnotification@marcjacobs.com"
*!*		tsendto = "tmarg@fmiint.com,l.lopez@marcjacobs.com,E.SKRZYNIARZ@marcjacobs.com,MarcJacobsSawgrassOutlet@marcjacobs.com,j.ortiz@marcjacobs.com,j.leone@marcjacobs.com"
	tattach = "S:\MarcJacobsData\TEMP\MJI_SAWGRASS.xls"
	tcc =""
	tfrom ="Toll EDI System Operations <fmi-transload-ops@fmiint.com>"
	tmessage = "MJI SAWGRASS OUTLET SHPMNT FOR:  "+dtoc(date()-1)
	tsubject = "MJI SAWGRASS OUTLET SHPMNT"
	do form m:\dev\frm\dartmail2 with tsendto,tfrom,tsubject, tcc,tattach,tmessage  ,"A"
else
endif


**************************************MJI WOODBURY  T
************NJ
xsqlexec("select wo_num, accountid, ship_ref, del_date, consignee, outshipid from outship where inlist(accountid,6303,6325,6543) " + ;
	"and mod='J' and consignee='MJI WOODBURY' and del_date>{"+dtoc(date()-10)+"} and dylog=0",,,"wh")

select 'NJ' as whse, a.wo_num, a.accountid, a.ship_ref as pt, a.del_date as ship_date, a.consignee, b.style, b.color, b.id, b.totqty, b.ucc as carton ;
	from outship a, tempdata b where a.outshipid=b.outshipid into cursor tj1 readwrite

select distinct accountid, wo_num from tj1 into cursor tj2 readwrite

select outship
scan
	select tj2
	locate for accountid=outship.accountid and wo_num=outship.wo_num
	if found()
		xsqlexec("update outship set dylog=1 where outshipid="+transform(outship.outshipid),,,"wh")
	endif   
endscan

**************************************MJI WOODBURY  T
************ML

xsqlexec("select wo_num, accountid, ship_ref, del_date, consignee, outshipid from outship where inlist(accountid,6303,6325,6543) " + ;
	"and mod='L' and consignee='MJI WOODBURY' and del_date>{"+dtoc(date()-10)+"} and dylog=0",,,"wh")

select 'CA' as whse, a.wo_num, a.accountid, a.ship_ref as pt, a.del_date as ship_date, a.consignee, b.style, b.color, b.id, b.totqty, b.ucc as carton ;
	from outship a, tempdata b where a.outshipid=b.outshipid into cursor tl1 readwrite

select distinct accountid, wo_num from tl1 into cursor tl2 readwrite

select outship
scan
	select tl2
	locate for accountid=outship.accountid and wo_num=outship.wo_num
	if found()
		xsqlexec("update outship set dylog=1 where outshipid="+transform(outship.outshipid),,,"wh")
	endif   
endscan

select * from tj1 union all select * from tl1 into cursor t3 order by pt, carton, style,color,id readwrite
select a.*, b.div from t3 a left join vupcmast b on a.style=b.style and a.color=b.color and a.id=b.id into cursor t4
select t4
if reccount() > 0
	export to "S:\MarcJacobsData\TEMP\MJI WOODBURY"  type xl5
*!*	tsendto = "tmarg@fmiint.com,l.lopez@marcjacobs.com,E.SKRZYNIARZ@marcjacobs.com,j.leone@marcjacobs.com,c.daley@marcjacobs.com,woodburycommonstock@marcjacobs.com,s.pilenza@marcjacobs.com"
	tsendto = "tmarg@fmiint.com,WoodburyCommon@marcjacobs.com,marcjacobs-us-retailshipmentnotification@marcjacobs.com"
	tattach = "S:\MarcJacobsData\TEMP\MJI WOODBURY.xls"
	tcc =""
	tfrom ="Toll EDI System Operations <fmi-transload-ops@fmiint.com>"
	tmessage = "MJI WOODBURY SHPMNT FOR:  "+dtoc(date()-1)
	tsubject = "MJI WOODBURY SHPMNT"
	do form m:\dev\frm\dartmail2 with tsendto,tfrom,tsubject, tcc,tattach,tmessage  ,"A"
else
endif

**************************************MJI BLEECKER STREET  U
************NJ

xsqlexec("select wo_num, accountid, ship_ref, del_date, consignee, outshipid from outship where inlist(accountid,6303,6325,6543) " + ;
	"and mod='J' and consignee='MJI BLEECKER STREET' and del_date>{"+dtoc(date()-10)+"} and dylog=0",,,"wh")

select 'NJ' as whse, a.wo_num, a.accountid, a.ship_ref as pt, a.del_date as ship_date, a.consignee, b.style, b.color, b.id, b.totqty, b.ucc as carton ;
	from outship a, tempdata b where a.outshipid=b.outshipid into cursor uj1 readwrite

select distinct accountid, wo_num from uj1 into cursor uj2 readwrite

select outship
scan
	select uj2
	locate for accountid=outship.accountid and wo_num=outship.wo_num
	if found()
		xsqlexec("update outship set dylog=1 where outshipid="+transform(outship.outshipid),,,"wh")
	endif   
endscan

**************************************MJI BLEECKER STREET  U
************ML
xsqlexec("select wo_num, accountid, ship_ref, del_date, consignee, outshipid from outship where inlist(accountid,6303,6325,6543) " + ;
	"and mod='L' and consignee='MJI BLEECKER STREET' and del_date>{"+dtoc(date()-10)+"} and dylog=0",,,"wh")

select 'CA' as whse, a.wo_num, a.accountid, a.ship_ref as pt, a.del_date as ship_date, a.consignee, b.style, b.color, b.id, b.totqty, b.ucc as carton ;
	from outship a, tempdata b where a.outshipid=b.outshipid into cursor ul1 readwrite

select distinct accountid, wo_num from ul1 into cursor ul2 readwrite

select outship
scan
	select ul2
	locate for accountid=outship.accountid and wo_num=outship.wo_num
	if found()
		xsqlexec("update outship set dylog=1 where outshipid="+transform(outship.outshipid),,,"wh")
	endif   
endscan

select * from Uj1 union all select * from Ul1 into cursor U3 order by pt, carton, style,color,id readwrite
select a.*, b.div from U3 a left join vupcmast b on a.style=b.style and a.color=b.color and a.id=b.id into cursor U4
select U4
if reccount() > 0
	export to "S:\MarcJacobsData\TEMP\MJI BLEECKER STREET"  type xl5
	tsendto = "tmarg@fmiint.com,l.lopez@marcjacobs.com,E.SKRZYNIARZ@marcjacobs.com,Store_Bleecker@marcjacobs.com,j.ortiz@marcjacobs.com,j.leone@marcjacobs.com"
	tattach = "S:\MarcJacobsData\TEMP\MJI BLEECKER STREET.xls"
	tcc =""
	tfrom ="Toll EDI System Operations <fmi-transload-ops@fmiint.com>"
	tmessage = "MJI BLEECKER STREET SHPMNT FOR:  "+dtoc(date()-1)
	tsubject = "MJI BLEECKER STREET SHPMNT"
	do form m:\dev\frm\dartmail2 with tsendto,tfrom,tsubject, tcc,tattach,tmessage  ,"A"
else
endif

wait window at 10,10 "all done...." timeout 2


schedupdate()
_screen.caption=gscreencaption
on error

