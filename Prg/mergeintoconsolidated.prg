* utility to merge a pdf file into the consolidated bill file for today.
*
LOCAL lcPDFFile
lcPDFFile = "C:\TEMPFOX\54021310.PDF"

SET PROCEDURE TO F:\FOOTSTAR\PRG\FOOTCLASS
loFootstarProc = CREATEOBJECT('FOOTSTAR')

loFootstarProc.OpenAcrobat()
loFootstarProc.MergePDFFiles(lcPDFFile)

MESSAGEBOX("Merge Complete.",0+64,"Footstar Bill PDF Merge")

CLOSE DATABASES ALL

RETURN
