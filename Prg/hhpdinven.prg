parameters xaccountid, lphysinven, lallaccts, lprepack, xoffice, xoutlook

* the only account left using this is Intradeco in Mod C   dy 1/18/17
goffice="5"
* end dy

**lprepack - added to allow for files for non-pnp accounts (ie modern shoe) - mvw 12/08/08

**false generally
xtest=.f.
*xnewerplist="5451,5453,6262,6261"	&& add accounts here as they move into the new ERP system for Moret

xoutlook=iif(type("xoutlook")="L",xoutlook,iif(lower(xoutlook)=".t." or xoutlook="1",.t.,.f.))

utilsetup("HPDINVEN")

**remove scanty as it is combined with sbh   removed dy 11/14/16
*gMoretAcctList=strtran(gMoretAcctList,"5449,","")

xsqlexec("select * from account where inactive=0","account",,"qq")
index on accountid tag accountid
set order to

xoffice=iif(empty(xoffice),"L",upper(xoffice))
xofficestate=iif(xoffice="C","CA",iif(xoffice="L","ML",iif(xoffice="R","Rialto",iif(xoffice="M","FL","NJ"))))

xaccountid=iif(xaccountid="0","",xaccountid)
gaccountid=xaccountid &&needed for later as xaccountid is changed along the way
lallaccts=iif(lower(lallaccts)=".t." or lallaccts="1",.t.,.f.)

lprepack=iif(type("lprepack")="L",transform(lprepack),lprepack)
lprepack=iif(lower(lprepack)=".t." or lprepack="1",.t.,.f.)

do case
case empty(xaccountid) and !lallaccts
	select str(accountid,5) as acctid, * from account where inlist(accountid,&gmoretacctlist) or inlist(accountid,&gmoretacctlist2) into cursor xtemp
	do form inplist with "xtemp", "xtemp.acctid,acctname",2,"35,150",1,"Select an account:",.f.,"C",.f. to cacct
	if lower(cacct)="abort"
		schedupdate()
		close databases all
		return
	endif

	xaccountid = val(cacct)
	xmoretacctno=xaccountid
	lphysinven=iif(lower(lphysinven)=".t." or lphysinven="1",.t.,.f.)

*!*		if xaccountid = 5449
*!*			messagebox("Scanty's inventory is included with the sbh inventory, no need to sent it separately.",16,"Process aborted")
*!*			return
*!*		endif

	cfilter="accountid = "+transform(xaccountid)
case !empty(xaccountid)
	xmoretacctno=xaccountid
	xaccountid=iif(empty(xaccountid),5453,val(xaccountid))
	lphysinven=iif(lower(lphysinven)=".t." or lphysinven="1",.t.,.f.)
	cfilter="accountid = "+transform(xaccountid)

case empty(xaccountid) and xoffice="N" 	&&moret nj
	xmoretacctno=0
	**only HPD exists in old erp - mvw 01/09/11
	cfilter="(accountid=5452 and njinven)"
*	cfilter="((inlist(accountid,&gmoretacctlist) or inlist(accountid,&gmoretacctlist2)) and njinven and !inlist(accountid,&xnewerplist))"
*	cfilter="inlist(accountid,5447,5451,5742)"
	lphysinven=iif(lower(lphysinven)=".t." or lphysinven="1",.t.,.f.)

case empty(xaccountid) and xoffice="C" 	&&moret sp
	xmoretacctno=5453
	**only HPD exists in old erp - mvw 01/09/11
	cfilter="(accountid=5452 and cainven)"
*	cfilter="((inlist(accountid,&gmoretacctlist) or inlist(accountid,&gmoretacctlist2)) and njinven and !inlist(accountid,&xnewerplist))"
*	cfilter="inlist(accountid,5453,5742)"
	lphysinven=iif(lower(lphysinven)=".t." or lphysinven="1",.t.,.f.)

case empty(xaccountid) and xoffice="M" 	&&moret fl
	xmoretacctno=5742
	**only HPD exists in old erp - mvw 01/09/11
	cfilter="(accountid=5452 and flinven)"
*	cfilter="((inlist(accountid,&gmoretacctlist) or inlist(accountid,&gmoretacctlist2)) and njinven and !inlist(accountid,&xnewerplist))"
*	cfilter="inlist(accountid,5742)"
	lphysinven=iif(lower(lphysinven)=".t." or lphysinven="1",.t.,.f.)

endcase

if date()#{11/5/12}
	use (wf(xoffice,iif(!empty(xaccountid),xaccountid,xmoretacctno))+"outship") in 0
	use (wf(xoffice,iif(!empty(xaccountid),xaccountid,xmoretacctno))+"outdet") in 0
	
	xsqlexec("select * from inven where mod='"+goffice+"' and accountid="+transform(xaccountid),,,"wh")
		
	xsqlexec("select * from pt where office='"+xoffice+"'",,,"wh")
	index on ptid tag ptid
	set order to
	xsqlexec("select * from ptdet where office='"+xoffice+"'",,,"wh")
	index on accountid tag accountid
	index on style tag style
	set order to
	xsqlexec("select * from physppu where office='"+xoffice+"'",,,"wh")
	index on physid tag physid

else
	do case
	case xoffice="C"
		vvv="6"
	case xoffice="N"
		vvv="I"
	otherwise
		vvv=xoffice
	endcase
		
	use ("m:\bak\whbak\whdata-"+vvv+"\outship") in 0
	use ("m:\bak\whbak\whdata-"+vvv+"\outdet") in 0
	use ("m:\bak\whbak\whdata-"+vvv+"\inven") in 0
	use ("m:\bak\whbak\whdata-"+vvv+"\indet") in 0
	use ("m:\bak\whbak\whdata-"+vvv+"\pt") in 0
	use ("m:\bak\whbak\whdata-"+vvv+"\ptdet") in 0
endif

select account
scan for &cfilter
	xaccountid=account.accountid
	xfile="h:\fox\inven"+transform(xaccountid)+lower(xoffice)
	xpnp=.t.
	do case
	case !inlist(xaccountid,&gMoretAcctList) and !inlist(xaccountid,&gMoretAcctList2)
		xofficecode=xoffice
	case xoffice="N"
		xofficecode="4"
		xpnp=pnpn
	case xoffice="C"
		xofficecode="3"
		xpnp=pnpc
	case xoffice="M"
		xofficecode="1"
		xpnp=pnpm
	otherwise
		xofficecode="5"
		xpnp=pnpl
	endcase

	if xaccountid=5451
		xacctfilter="inlist(accountid,5451,5449)"
		xosacctfilter="inlist(s.accountid,5451,5449)"
	else
		xacctfilter="accountid="+transform(xaccountid)
		xosacctfilter="s.accountid=xaccountid"
	endif

	Wait window at 10,10 "Gathering Inventory Data........... Please wait " timeout 2

	do case
	case lphysinven and (inlist(accountid,&gmoretacctlist) or inlist(accountid,&gmoretacctlist2))
		xpnpfilter=iif(xpnp,"units","!units")

		**changed from substr(style,1,12) as style to grab entire style for new AMT system, only for HBEST 5453- mvw 05/09/11
		* fixed SQL problem dy 2/7/16
		xsqlexec("select * from phys where office='"+xoffice+"' and "+xacctfilter+" and !empty(style) and totqty>0",,,"wh")

		select xofficecode as whse, iif(xaccountid=5453,style,substr(style,1,12)) as style, space(6), padl(alltrim(str(sum(iif(units,totqty,totqty*val(pack))))),7," ") as qty,;
			padl(alltrim(str(0)),7," ") as alloc, padl(alltrim(str(0)),7," ") as wipqty, space(30) ;
			from phys where &xacctfilter and !empty(style) and totqty>0 group by style into cursor temp readwrite

		xqtyfld=iif(xpnp,"d.totqty","(d.totqty*val(d.pack))")
		**changed from substr(style,1,12) as style to grab entire style for new AMT system, only for HBEST 5453- mvw 05/09/11
		select xofficecode as whse, iif(xaccountid=5453,style,substr(style,1,12)) as style, space(6), padl(alltrim(str(sum(&xqtyfld))),7," ") as qty,;
			padl(alltrim(str(0)),7," ") as alloc, padl(alltrim(str(0)),7," ") as wipqty, space(30) ;
			from outship s left outer join outdet d on d.outshipid = s.outshipid where emptynul(del_date) and ;
			&xosacctfilter and s.pulled and &xpnpfilter and !notonwip group by style ;
		  into cursor wip

		copy to h:\fox\wipxls.xls for val(qty)>0 type xls
		copy to h:\fox\wiptxt.txt for val(qty)>0 type sdf

		tattach = xfile+".xls,"+xfile+".txt,h:\fox\wipxls.xls,h:\fox\wiptxt.txt"

	case lphysinven &&not moret
		select physid, style,color,id,pack,sum(iif(units,totqty,totqty*val(pack))) as qty,;
				0000000 as alloc, 0000000 as wipqty, unexpected ;
			from phys where &xacctfilter and !empty(style) and totqty>0 group by style,color,id,pack into cursor temp readwrite

		*break out musicals
		select temp
		scan for pack#"1 "
			xrecno=recno()

			xfound = xsqlexec("select * from indet where accountid="+transform(xaccountid)+" " + ;
						"and style='"+xtemp.style+"' and color='"+xtemp.color+"' " + ;
						"and id='"+xtemp.id+"' and pack='"+xtemp.pack+"'",,,"wh") # 0
			
			do case
			case !unexpected and xfound
				tempqty=temp.qty
				xwonum=indet.wo_num
				xpo=indet.po
				xpack=indet.pack
				xindetctnqty=indet.totqty
				tempctnqty=temp.qty

				select indet
				count for wo_num=xwonum and po=xpo and units
				if _tally>1
					select indet
					scan for wo_num=xwonum and po=xpo and units
						scatter memvar fields style, color, id, pack
						m.qty=tempqty/(xindetctnqty*val(xpack))*indet.totqty
						insert into temp from memvar
					endscan

					go (xrecno) in temp
					delete in temp
				endif

			case unexpected and seek(physid,"physppu","physid")
				select physppu
				count for physid=temp.physid
				if _tally>1
					select physppu
					scan for physid=temp.physid
						scatter memvar
						m.units=.t.
						m.unexpected=.t.
						insert into temp from memvar
					endscan
				
					go (xrecno) in temp
					delete in temp
				endif
			endcase
		endscan

		select 0 as physid, style, color, id, pack, 0 as qty, 0 as alloc, sum(d.totqty) as wipqty, .f. as unexpected ;
			from outship s left outer join outdet d on d.outshipid = s.outshipid where emptynul(del_date) and ;
			&xosacctfilter and s.pulled = .t. and units and !notonwip group by style,color,id ;
		  into cursor wip

		insert into temp select * from wip 

		select style,color,id,space(6),padl(alltrim(str(sum(qty))),7," ") as qty,;
				padl(alltrim(str(0)),7," ") as alloc, padl(alltrim(str(sum(wipqty))),7," ") as wipqty, space(30) ;
			from temp group by style,color,id into cursor temp readwrite

		tattach = xfile+".xls,"+xfile+".txt"

	case !lprepack and (inlist(accountid,&gmoretacctlist) or inlist(accountid,&gmoretacctlist2))
		xpnpfilter=iif(xpnp,"units","!units")

		xqtyfld=iif(xpnp,"totqty","(totqty*val(pack))")
		**changed from substr(style,1,12) as style to grab entire style for new AMT system, only for HBEST 5453- mvw 05/09/11
		select xofficecode as whse,iif(xaccountid=5453,style,substr(style,1,12)) as style,space(6),padl(alltrim(str(&xqtyfld)),7," ") as qty,;
			padl(alltrim(str(0)),7," ") as alloc, padl(alltrim(str(0)),7," ") as wipqty,space(30) ;
			from inven where &xacctfilter and &xpnpfilter into cursor temp readwrite

		xqtyfld=iif(xpnp,"totqty","(d.totqty*val(d.pack))")
		**changed from substr(style,1,12) as style to grab entire style for new AMT system, only for HBEST 5453- mvw 05/09/11
		select s.wo_num,s.notonwip,s.sp,iif(xaccountid=5453,style,substr(style,1,12)) as style,sum(&xqtyfld) as wipqty ;
			from outship s left join outdet d on d.outshipid = s.outshipid ;
			where emptynul(del_date) and &xosacctfilter and s.pulled and &xpnpfilter and ;
				(!notonwip or (notonwip and s.sp)) group by s.wo_num, style ;
		  into cursor wip readwrite

		select * from wip where notonwip and sp group by wo_num into cursor xtemp
		scan
			xsqlexec("select wo_num from project where wo_num="+transform(xtemp.wo_num)+" and completeddt={}",,,"wh")
			if reccount()=0
				delete for wo_num=xtemp.wo_num in wip
			endif
		endscan

		select style, sum(wipqty) as wipqty from wip group by style into cursor wip

		select temp
		scan
		  select wip
		  locate for wip.style = temp.style
		  if found()
		    replace qty with padl(transform(val(qty)+wip.wipqty),7," ") in temp
		*    replace wipqty with padl(transform(wip.wipqty),7," ") in temp
		  endif
		endscan

		delete for val(qty)=0 and val(wipqty)=0 in temp

		tattach = xfile+".xls,"+xfile+".txt"

	case !lprepack &&not moret
		select xofficecode as whse,style,color,id,space(6),padl(alltrim(str(totqty)),7," ") as qty,;
			padl(alltrim(str(0)),7," ") as alloc, padl(alltrim(str(0)),7," ") as wipqty,space(30) ;
			from inven where &xacctfilter and units into cursor temp readwrite

		select s.wo_num,s.notonwip,s.sp,d.style,d.color,d.id,sum(d.totqty) as wipqty ;
			from outship s left join outdet d on d.outshipid = s.outshipid ;
			where emptynul(del_date) and &xosacctfilter and s.pulled and units and ;
				(!notonwip or (notonwip and s.sp)) group by s.wo_num, style, color, id ;
		  into cursor wip readwrite

		select * from wip where notonwip and sp group by wo_num into cursor xtemp
		scan
			xsqlexec("select wo_num from project where wo_num="+transform(xtemp.wo_num)+" and completeddt={}",,,"wh")
			if reccount()=0
				delete for wo_num=xtemp.wo_num in wip
			endif
		endscan

		select style, color, id, sum(wipqty) as wipqty from wip group by style, color, id into cursor wip

		select temp
		scan
		  select wip
		  locate for wip.style = temp.style and wip.color = temp.color and wip.id = temp.id
		  if found()
			**changed to show wip in wipqty column for everyone but moret - 03/11/09 mvw
		    *replace qty with padl(transform(val(qty)+wip.wipqty),7," ") in temp
			replace wipqty with padl(transform(wip.wipqty),7," ") in temp
		  endif
		endscan

		delete for val(qty)=0 and val(wipqty)=0 in temp

		tattach = xfile+".xls,"+xfile+".txt"

	case lprepack and xaccountid=5687 &&UA
		select xofficecode as whse,date_rcvd,style,color,id,pack,padl(alltrim(str(totqty)),7," ") as qty,;
			{} as start, {} as cancel, space(30) as consignee ;
			from inven where &xacctfilter and !units into cursor temp readwrite

		select s.wo_num,s.notonwip,s.sp,d.style,d.color,d.id,d.pack,sum(d.totqty) as wipqty, start, cancel, consignee ;
			from outship s left join outdet d on d.outshipid = s.outshipid ;
			where emptynul(del_date) and &xosacctfilter and s.pulled and !units and ;
				(!notonwip or (notonwip and s.sp)) group by s.wo_num, style, color, id, pack ;
		  into cursor wip readwrite

		select * from wip where notonwip and sp group by wo_num into cursor xtemp
		scan
			xsqlexec("select wo_num from project where wo_num="+transform(xtemp.wo_num)+" and completeddt={}",,,"wh")
			if reccount()=0
				delete for wo_num=xtemp.wo_num in wip
			endif
		endscan

		select style, color, id, pack, sum(wipqty) as wipqty, start, cancel, consignee from wip group by style, color, id, pack into cursor wip

		select temp
		scan
		  select wip
		  **changed to ignore id - mvw 07/28/10
*		  locate for wip.style = temp.style and wip.color = temp.color and wip.id = temp.id and wip.pack = temp.pack
		  locate for wip.style = temp.style and wip.color = temp.color and wip.pack = temp.pack
		  if found()
			replace qty with padl(transform(val(qty)+wip.wipqty),7," "), start with wip.start, cancel with wip.cancel, consignee with wip.consignee in temp
		  endif
		endscan

		delete for val(qty)=0 in temp

		select whse, date_rcvd, style as uapo, color as custpo, id, pack, qty, start, cancel, consignee from temp into cursor temp readwrite

		scan for emptynul(start)
			select ptdet
			**changed to ignore id - mvw 07/28/10
*			locate for accountid=xaccountid and style=temp.uapo and color=temp.custpo and id=temp.id and pack=temp.pack
			locate for accountid=xaccountid and style=temp.uapo and color=temp.custpo and pack=temp.pack
			if found() and seek(ptdet.ptid,"pt","ptid")
				replace start with pt.start, cancel with pt.cancel, consignee with pt.consignee in temp
			else
				select outdet
				**changed to ignore id - mvw 07/28/10
*				locate for accountid=xaccountid and style=temp.uapo and color=temp.custpo and id=temp.id and pack=temp.pack
				locate for accountid=xaccountid and style=temp.uapo and color=temp.custpo and pack=temp.pack
				if found() and seek(outdet.outshipid,"outship","outshipid")
					replace start with outship.start, cancel with outship.cancel, consignee with outship.consignee in temp
				endif
			endif
		endscan

		tattach = xfile+".xls"

	otherwise &&lprepack=.t.
		select xofficecode as whse,style,color,id,pack,space(6),padl(alltrim(str(totqty)),7," ") as qty,;
			padl(alltrim(str(0)),7," ") as alloc, padl(alltrim(str(0)),7," ") as wipqty,space(30) ;
			from inven where &xacctfilter and !units into cursor temp readwrite

		select s.wo_num,s.notonwip,s.sp,d.style,d.color,d.id,d.pack,sum(d.totqty) as wipqty ;
			from outship s left join outdet d on d.outshipid = s.outshipid ;
			where emptynul(del_date) and &xosacctfilter and s.pulled and !units and ;
				(!notonwip or (notonwip and s.sp)) group by s.wo_num, style, color, id, pack ;
		  into cursor wip readwrite

		select * from wip where notonwip and sp group by wo_num into cursor xtemp
		scan
			xsqlexec("select wo_num from project where wo_num="+transform(xtemp.wo_num)+" and completeddt={}",,,"wh")
			if reccount()=0
				delete for wo_num=xtemp.wo_num in wip
			endif
		endscan

		select style, color, id, pack, sum(wipqty) as wipqty from wip group by style, color, id, pack into cursor wip

		select temp
		scan
		  select wip
		  locate for wip.style = temp.style and wip.color = temp.color and wip.id = temp.id and wip.pack = temp.pack
		  if found()
			**changed to show wip in wipqty column for everyone but moret - 12/7/09 mvw
		    *replace qty with padl(transform(val(qty)+wip.wipqty),7," ") in temp
			replace wipqty with padl(transform(wip.wipqty),7," ") in temp
		  endif
		endscan

		delete for val(qty)=0 and val(wipqty)=0 in temp

		tattach = xfile+".xls,"+xfile+".txt"
	endcase

	select temp
	locate
	if !eof() &&do not send if empty file - mvw 11/15/10
		copy to &xfile..xls type xls
		copy to &xfile..txt type sdf

		tcc =""
		tFrom ="TGF WMS Operations Center <transload-ops@fmiint.com>"

		tmessage = "See attached "+alltrim(account.acctname)+" Inventory files................"
		tsubject = alltrim(account.acctname)+" ("+xofficestate+") "+iif(!lphysinven,"","Physical ")+"Inventory Sync Files"

		do case
		case xtest
			tsendto = "dyoung@fmiint.com"
		case lphysinven and xoffice="N"
			tsendto = "mwinter@fmiint.com,cmalcolm@fmiint.com"
		case lphysinven and xoffice="M"
			tsendto = "mwinter@fmiint.com,BRodriguez@fmiint.com"
		case lphysinven
			tsendto = "mwinter@fmiint.com,ed.kurowski@tollgroup.com"

		case inlist(xaccountid,5687) and inlist(xoffice,"C","R") &&UA
			if day(date())#1 &&send test internally 3 days b4 end of the month, send to UA on the 1st
				tsendto = "mwinter@fmiint.com,juan@fmiint.com,mGoldberg@fmiint.com"
				tmessage = "See attached "+alltrim(account.acctname)+" inventory files... "+chr(13)+chr(13)+;
					"PLEASE VERIFY THIS DATA AND CORRECT ASAP IF NECESSARY AS A LIVE COPY WILL BE SENT DIRECTLY "+;
					"TO UA ON THE FIRST OF THE MONTH!!!"
			else
				tsendto = "dzuskin@underarmour.com,mwinter@fmiint.com,juan@fmiint.com"
			endif
		case inlist(xaccountid,5687) and xoffice="N" &&UA
			if day(date())#1 &&send test internally 3 days b4 end of the month, send to UA on the 1st
				tsendto = "mwinter@fmiint.com,juan@fmiint.com"
				tmessage = "See attached "+alltrim(account.acctname)+" inventory files... "+chr(13)+chr(13)+;
					"PLEASE VERIFY THIS DATA AND CORRECT ASAP IF NECESSARY AS A LIVE COPY WILL BE SENT DIRECTLY "+;
					"TO UA ON THE FIRST OF THE MONTH!!!"
			else
				tsendto = "dzuskin@underarmour.com,juan@fmiint.com,mwinter@fmiint.com"
			endif
		case inlist(xaccountid,5687) and xoffice="M" &&UA
			if day(date())#1 &&send test internally 3 days b4 end of the month, send to UA on the 1st
				tsendto = "mwinter@fmiint.com,juan@fmiint.com,BRodriguez@fmiint.com"
				tmessage = "See attached "+alltrim(account.acctname)+" inventory files... "+chr(13)+chr(13)+;
					"PLEASE VERIFY THIS DATA AND CORRECT ASAP IF NECESSARY AS A LIVE COPY WILL BE SENT DIRECTLY "+;
					"TO UA ON THE FIRST OF THE MONTH!!!"
			else
				tsendto = "dzuskin@underarmour.com,BRodriguez@fmiint.com,juan@fmiint.com,mwinter@fmiint.com"
			endif
		case inlist(xaccountid,6111,6112) &&Pumpkin Patch, Vision Racer
			tsendto = "Anthony.villa@tollgroup.com,Daniel.obrien@tollgroup.com,ask@simroom.com"
			
*		case inlist(xaccountid,6034) &&lifefactory	removed dy 12/7/6 per Chris M
*			tsendto = "stephanie@lifefactory.com"
		case xaccountid=5102 &&bugaboo
			tsendto = "David.Yamasaki@bugaboo.com,jesse.chan@bugaboo.com,hak.ong@tollgroup.com"
*			tsendto = "mwinter@fmiint.com"
		case inlist(xaccountid,4677) &&Courtaulds
			tsendto = "mwinter@fmiint.com,ed.kurowski@tollgroup.com"
		case inlist(xaccountid,4650,5023) &&perry ellis & supreme
			tsendto = "michelle.mosquera@pery.com,ed.kurowski@tollgroup.com"
		case xaccountid=5383
			tsendto = "mruiz@heelys.com,ed.kurowski@tollgroup.com,mwinter@fmiint.com,pgaidis@fmiint.com"
		case xaccountid=6416 &&creative recreation
			tsendto = "Melanie@cr8rec.com,anthony@cr8rec.com,mwinter@fmiint.com"
		case xaccountid=6438 &&bernardo
			tsendto = "syed@bernardo-usa.com,mwinter@fmiint.com"
		case xaccountid=5154 &&intradeco
			tsendto = "Johanna_Perez@Intradeco.com,Jesus_Castillo@intradeco.com,OrderProcessingES@intradesa.com,lizeth_turcios@Intradeco.com,natalia_siman@intradeco.com,hak.ong@tollgroup.com"
		case inlist(xaccountid,&gtkohighlineaccounts) &&modern shoe & highline united
			tsendto = "byron@ModernShoe.Net,roger@modernshoe.net,ed.kurowski@tollgroup.com,mwinter@fmiint.com"
		case empty(gaccountid) and xoffice="N" 	&&moret nj
*			tsendto = "mwinter@fmiint.com"
			tsendto = "lupe@moret.com,mwinter@fmiint.com,pgaidis@fmiint.com"
		case empty(gaccountid) and xoffice="C" 	&&moret sp
			tsendto = "lupe@moret.com,ed.kurowski@tollgroup.com,mwinter@fmiint.com,pgaidis@fmiint.com"
*			tsendto = "mwinter@fmiint.com"
		case empty(gaccountid) and xoffice="M" 	&&moret fl
			tsendto = "lupe@moret.com,mwinter@fmiint.com,pgaidis@fmiint.com"
		otherwise 								&&moret ml
*			tsendto = "mwinter@fmiint.com"
			tsendto = "lupe@moret.com,ed.kurowski@tollgroup.com,mwinter@fmiint.com,pgaidis@fmiint.com"
		endcase

		if xoutlook
			tsendto=strtran(tsendto,",",";")
			email(tsendto,tsubject,tmessage,tattach,.f.,tcc,.t.,,,,,.t.,tfrom)
		else
			do form dartmail2 with tsendto,tfrom,tsubject,tcc,tattach,tmessage,"A"
		endif
	endif
endscan

schedupdate()