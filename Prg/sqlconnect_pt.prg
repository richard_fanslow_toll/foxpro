PARAMETERS nAcctNum,cCustName,cPPName,lUCC,cOffice,lSQL3,lMoret,lDoCartons,lDoScanpack,sqlover_ride

*xgetsql

IF VARTYPE(cOffice) # "C"
	cOffice = "C"
ENDIF
csql = "tgfnjsql01"
cSQLPass = ""

SELECT 0
USE F:\wh\sqlpassword
cSQLPass = ALLTRIM(sqlpassword.PASSWORD)
USE IN sqlpassword

SET ESCAPE ON
ON ESCAPE CANCEL
cFileOutName = "v"+cCustName+"pp"
nAcct = ALLTRIM(STR(nAcctNum))
IF USED("temp1sqlpt")
	USE IN temp1sqlpt
ENDIF

lookups()

lAppend = .F.

*ASSERT .f. MESSAGE "In SQL Connect"
*SET STEP ON 

IF lTestinput
lcDSNLess="driver=SQL Server;server=njsql1;uid=SA;pwd=&cSQLPass;DATABASE=PICKPACK"
else
lcDSNLess="driver=SQL Server;server=tgfnjsql01;uid=SA;pwd=&cSQLPass;DATABASE=PICKPACK"
endif

*SET STEP ON 
SELECT sqlwopt
LOCATE
SCAN
	IF " OV"$sqlwopt.ship_ref
	LOOP
	ENDIF
	
	nHandle=SQLSTRINGCONNECT(m.lcDSNLess,.T.)
	SQLSETPROP(0,'DispLogin',3)
	SQLSETPROP(0,"dispwarnings",.F.)
	WAIT WINDOW "Now processing "+cCustName+" SQL view...please wait" NOWAIT
	IF nHandle<1 && bailout
		SET STEP ON
		WAIT WINDOW "Could not make SQL connection" TIMEOUT 3
		cRetMsg = "NO SQL CONNECT"
		RETURN cRetMsg
		THROW
	ENDIF



	nPT = "'"+ALLTRIM(sqlwopt.ship_ref)+"'"


	lUseLabels = .f.
	IF DATETIME()<DATETIME(2018,03,30,20,30,00) OR lTestinput
		lUseLabels = .t.
		WAIT WINDOW "SQL Records will be selected from LABELS" TIMEOUT 2
		lcQ1=[SELECT * FROM dbo.labels Labels]
		lcQ2 = [ WHERE Labels.ship_ref = ]
		lcQ3 = " &nPT "
		IF lUCC
			lcQ6 = [order by Labels.ship_ref,Labels.outshipid,Labels.ucc]
		ELSE
			lcQ6 = [order by Labels.ship_ref,Labels.outshipid,Labels.CartonNum]
		ENDIF
		lcsql = lcQ1+lcQ2+lcQ3+lcQ6

	ELSE
		WAIT WINDOW "SQL Records will be selected from CARTONS for PT: "+sqlwopt.ship_ref nowait
		
		if usesql() && and DATETIME()>DATETIME(2018,04,05,00,45,00)
			if lucc
				xorderby="order by ship_ref, outshipid, ucc"
			else
				xorderby="order by ship_ref, outshipid, cartonnum"
			endif
			
			if xsqlexec("select * from cartons where ship_ref='"+alltrim(sqlwopt.ship_ref)+"' and totqty>0 and ucc#'CUTS' "+xorderby,cFileOutName,,"pickpack") = 0
				ASSERT .F. MESSAGE "At NO RECORDS statement in SQLConnect"
				WAIT WINDOW "No "+cCustName+" records found" TIMEOUT 2
				cRetMsg = "NO SQL CONNECT"
				RETURN cRetMsg
			endif

		else
			lcQ1 = [SELECT * FROM dbo.cartons Cartons]
			lcQ2 = [ WHERE Cartons.ship_ref = ]
			lcQ3 = " &nPT "
			lcQ4 = [ AND  Cartons.totqty > ]
			lcQ5 = [ 0 ]
			IF lUCC
				lcQ6 = [order by Cartons.ship_ref,Cartons.outshipid,Cartons.ucc]
			ELSE
				lcQ6 = [order by Cartons.ship_ref,Cartons.outshipid,Cartons.CartonNum]
			ENDIF
			lcsql = lcQ1+lcQ2+lcQ3+lcQ4+lcQ5+lcQ6
		endif
	ENDIF

	if usesql() AND lUseLabels = .f.

	else
		llSuccess=SQLEXEC(nHandle,lcsql,cFileOutName)

		IF llSuccess < 1  && no records 0 indicates no records and 1 indicates records found
			ASSERT .F. MESSAGE "At NO RECORDS statement in SQLConnect"
			WAIT WINDOW "No "+cCustName+" records found" TIMEOUT 2
			cRetMsg = "NO SQL CONNECT"
			RETURN cRetMsg
		ENDIF
		SQLCANCEL(nHandle)
	endif
	
	IF lAppend = .F.
		lAppend = .T.
		SELECT &cFileOutName
		COPY TO ("F:\3pl\DATA\temp1sqlpt")
		USE ("F:\3pl\DATA\temp1sqlpt") IN 0 ALIAS temp1sqlpt
	ELSE
		SELECT &cFileOutName
		SCAN
			SCATTER MEMVAR
			INSERT INTO temp1sqlpt FROM MEMVAR
		ENDSCAN
	ENDIF
ENDSCAN

IF USED(cFileOutName)
	USE IN &cFileOutName
ENDIF

SELECT temp1sqlpt
LOCATE

IF lUCC
	SELECT * FROM temp1sqlpt ;
		ORDER BY ship_ref,outshipid,ucc ;
		INTO CURSOR &cFileOutName READWRITE
ELSE
	SELECT * FROM temp1sqlpt ;
		ORDER BY ship_ref,outshipid,cartonnum ;
		INTO CURSOR &cFileOutName READWRITE
ENDIF

USE IN temp1sqlpt
USE IN sqlwopt
GO BOTT
WAIT CLEAR

SQLDISCONNECT(nHandle)
cRetMsg = "OK"
RETURN cRetMsg
