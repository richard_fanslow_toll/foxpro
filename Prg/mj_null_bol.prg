* This program identifies shipped shipments in which the BOL is null in OUTSHIP
utilsetup("MJ_NULL_BOL")
SET EXCLUSIVE OFF 
SET DELETED ON 
SET TALK OFF  
SET SAFETY OFF  
*****************************NJ

Wait window at 10,10 "Performing the first query...." nowait

xsqlexec("select wo_num, ship_ref, bol_no, del_date from outship where mod='J' and inlist(accountid,6303,6325,6305,6543) " + ;
	"and del_date>{"+dtoc(date()-40)+"} and empty(bol_no)","temp1",,"wh")

*****************************CA

Wait window at 10,10 "Performing the second query...." nowait

xsqlexec("select wo_num, ship_ref, bol_no, del_date from outship where mod='L' and inlist(accountid,6303,6325,6305,6543) " + ;
	"and del_date>{"+dtoc(date()-60)+"} and empty(bol_no)","temp1",,"wh")

SELECT * FROM temp1 UNION select * FROM temp2 INTO CURSOR temp3 READWRITE 

	If Reccount() > 0 
		export TO S:\MarcJacobsData\INVALID_BOLS\mj_null_bol.xls  TYPE xls
	Wait window at 10,10 "Performing 1st email...." NOWAIT 		
		tsendto = "tmarg@fmiint.com,pgaidis@fmiint.com,todd.margolin@tollgroup.com"
		tattach = "S:\MarcJacobsData\INVALID_BOLS\mj_null_bol.xls" 
		tcc =""                                                                                                                               
		tFrom ="Toll EDI System Operations <fmi-transload-ops@fmiint.com>"                                
		tmessage = "Null BOL's for shipped product exist_ "+Ttoc(Datetime())        
		tSubject = "Null BOL's for shipped product exist"                                                           
		Do Form m:\dev\frm\dartmail2 With tsendto,tFrom,tSubject, tcc,tattach,tmessage  ,"A"  
			
		ELSE 
			Wait window at 10,10 "No  data to report.........." timeout 2
			tsendto = "tmarg@fmiint.com"
			tattach = "" 
			tcc =""                                                                                                                               
			tFrom ="Toll EDI System Operations <fmi-transload-ops@fmiint.com>"                                
			tmessage = "NO Null BOL's for shipped product exist_  "+Ttoc(Datetime())        
			tSubject = "NO Null BOL's for shipped product exist"                                                           
			Do Form m:\dev\frm\dartmail2 With tsendto,tFrom,tSubject, tcc,tattach,tmessage  ,"A"
					
	ENDIF		


schedupdate()
_screen.Caption=gscreencaption
on error
