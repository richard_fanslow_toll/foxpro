
************************************************************************
*!*	Chris Lind needs a report on receiving data which is all inbounds.
*!*	Inbound WO#, Container# or AWB#, acct_ref, brokerref, etc
*!*	Table: Inwolog
*!*	Moret account lists in lookups.prg
*!*	Sample query
*!*	Select wo_num,wo_date,acctname,container,reference,acct_ref,brokerref,confirmdt from inwolog where InList(accountid,&gmoretacctlist) and (Empty(confirmdt) or confirmdt>=Date()-5) and !sp
*!*	Need to capture all moret accounts, all warehouse locations
************************************************************************
*
* Build exe as F:\UTIL\MORET\MORETINBOUNDREPORT.EXE
*
************************************************************************

LOCAL loMoretInboundRptProc, lnError, lcProcessName

utilsetup("MORETINBOUNDREPORT")

DECLARE INTEGER CreateMutex IN Win32API ;
	STRING @lpMutexAttributes, ;
	STRING bInitialOwner, ;
	STRING @lpName

DECLARE INTEGER GetLastError IN Win32API

* if running in EXE check to see if already running
IF _VFP.STARTMODE > 0 THEN
	lcProcessName = "MoretInboundRptProc"
	CreateMutex(0,CHR(1),@lcProcessName)
	lnError = GetLastError()
	IF lnError = 183
		WAIT WINDOW TIMEOUT 5 "This program is already running..."
		schedupdate()
		RETURN .F.
	ENDIF
ENDIF

loMoretInboundRptProc = CREATEOBJECT('MoretInboundRptProc')
loMoretInboundRptProc.MAIN()

schedupdate()

CLOSE DATABASES ALL

RETURN

#DEFINE LOGIT 1
#DEFINE WAITIT 2
#DEFINE NOWAITIT 4
#DEFINE SENDIT 8
#DEFINE CR CHR(13)
#DEFINE LF CHR(10)
#DEFINE CRLF CHR(13) + CHR(10)
#DEFINE RETURN_DATA_MANDATORY .F.
#DEFINE RETURN_DATA_NOT_MANDATORY .T.
#DEFINE CSVFORMAT 6
#DEFINE WRITE_ONLY_UNBUFFERED 11
#DEFINE COMMA ","

DEFINE CLASS MoretInboundRptProc AS CUSTOM

	cProcessName = 'MoretInboundRptProc'

	lTestMode = .F.  && .F. FOR NORMAL OPERATION, .T. TO SUPPRESS ALL BUT DEVELOPER EMAILS AND CHANGE INPUT TABLE, ETC.

	lAutoYield = .T.

	* date time props
	dToday = DATE()
	*dToday = {^2010-07-24}

	cStartTime = TTOC(DATETIME())

	* file/folder properties
	cOutputFolder = 'F:\UTIL\MORET\REPORTS\'   
	cWHDataFolder = ''

	* WH office Table properties
	cWHOfficeTable = 'F:\WH\WHOFFICE'

	* processing properties
	cErrors = ''
	cSummaryInfo = ''
	nNumberOfErrors = 0

	* wait window properties
	nWaitWindowTimeout = 2
	lWaitWindowIsOn = .T.

	* logfile properties
	lLoggingIsOn = .T.
	cLogFile = 'F:\UTIL\MORET\LOGFILES\MoretInboundRptProc_log.txt'

	* INTERNAL email properties
	lSendInternalEmailIsOn = .T.
	cFrom = 'FMI Corporate <fmi-transload-ops@fmiint.com>'
	cSendTo = 'clind@fmiint.com'
	cCC = 'mbennett@fmiint.com'
	cSubject = 'Moret Inbound Report for ' + TTOC(DATETIME())
	cAttach = ''
	cBodyText = ''

	cCOMPUTERNAME = ''
	cUSERNAME = ''


	FUNCTION INIT
		IF NOT DODEFAULT()
			RETURN .F.
		ENDIF
		WITH THIS
			*SET RESOURCE OFF
			CLOSE DATA
			DO setenvi
			SET CENTURY ON
			SET DATE TO AMERICAN
			SET DECIMAL TO 0
			SET TALK OFF
			SET DELETED ON
			SET CONSOLE OFF
			SET EXCLUSIVE OFF
			SET SAFETY OFF
			SET EXACT OFF
			SET STATUS BAR ON
			SET SYSMENU OFF
			SET ENGINEBEHAVIOR 70
			_VFP.AUTOYIELD = .lAutoYield
			.cCOMPUTERNAME = GETENV("COMPUTERNAME")
			.cUSERNAME = GETENV("USERNAME")
			IF .lTestMode THEN
				.cLogFile = 'F:\UTIL\MORET\LOGFILES\MoretInboundRptProc_LOG_TESTMODE.txt'
				.cSendTo = 'mbennett@fmiint.com'
				.cCC = ''
				*.cOutputFolder = 'F:\UTIL\LF\TESTOUT\'
			ENDIF
			.lLoggingIsOn = .lLoggingIsOn AND NOT EMPTY(.cLogFile)
		ENDWITH
	ENDFUNC


	FUNCTION DESTROY
		WITH THIS
			IF .lLoggingIsOn  THEN
				SET ALTERNATE OFF
				SET ALTERNATE TO
			ENDIF
		ENDWITH
		DODEFAULT()
	ENDFUNC


	FUNCTION MAIN
		WITH THIS

			LOCAL loError, ldToday, lcFileDateTime, lcCSVFile, lcFileRoot, lcInWoLog
			LOCAL oExcel, oWorkbook, oWorksheet, lnRow, lcRow, ld7DaysAgo, ldToday
			LOCAL lcDescrip, lcFileDateTime, lcOutputfile

			TRY

				.nNumberOfErrors = 0

				IF .lLoggingIsOn THEN
					SET ALTERNATE TO (.cLogFile) ADDITIVE
					SET ALTERNATE ON
				ENDIF
				
				.cWHDataFolder = WF("N",5447)

				.TrackProgress('Moret Inbound Report Process started....', LOGIT+SENDIT+NOWAITIT)
				.TrackProgress('COMPUTERNAME = ' + .cCOMPUTERNAME, LOGIT+SENDIT)
				.TrackProgress('USERNAME = ' + .cUSERNAME, LOGIT+SENDIT)
				.TrackProgress('WHData Folder = ' + .cWHDataFolder, LOGIT+SENDIT)
				.TrackProgress('PROJECT = MORETINBOUNDREPORT', LOGIT+SENDIT)				
				IF .lTestMode THEN
					.TrackProgress('TEST MODE', LOGIT+SENDIT)
				ENDIF

				ldToday = .dToday
				ld7DaysAgo = ldToday - 7

				DO LOOKUPS

				* CREATE MASTER CURSOR
				SELECT ;
					SPACE(20) AS DESCRIP, WO_NUM, WO_DATE, ACCTNAME, CONTAINER, REFERENCE, ACCT_REF, BROKERREF, CONFIRMDT ;
					FROM (.cWHDataFolder + 'INWOLOG') ;
					INTO CURSOR CURMASTER ;
					WHERE .F. ;
					READWRITE

				IF USED('CURWHOFFICE') THEN
					USE IN CURWHOFFICE
				ENDIF

				SELECT * ;
					FROM (.cWHOfficeTable) ;
					INTO CURSOR CURWHOFFICE ;
					WHERE (NOT TEST) ;
					ORDER BY OFFICE

				SELECT CURWHOFFICE
				SCAN
					lcDescrip = ALLTRIM(CURWHOFFICE.DESCRIP)
					.TrackProgress('Processing ' + lcDescrip, LOGIT+SENDIT+NOWAITIT)
					
					lcInWoLog = UPPER(ALLTRIM(CURWHOFFICE.FOLDER)) + '\WHDATA\INWOLOG'
					
					SELECT ;
						lcDescrip AS DESCRIP, WO_NUM, WO_DATE, ACCTNAME, CONTAINER, REFERENCE, ACCT_REF, BROKERREF, CONFIRMDT ;
						FROM (lcInWoLog) ;
						INTO CURSOR CURMORET ;
						WHERE (INLIST(ACCOUNTID,&GMORETACCTLIST) or INLIST(ACCOUNTID,&GMORETACCTLIST2)) AND (EMPTYnul(CONFIRMDT) OR CONFIRMDT >= ld7DaysAgo) AND !SP

					IF USED('CURMORET') AND NOT EOF('CURMORET') THEN
						SELECT CURMORET
						SCAN
							SCATTER MEMVAR
							INSERT INTO CURMASTER FROM MEMVAR
						ENDSCAN
						USE IN CURMORET
					ENDIF

				ENDSCAN
				
				IF USED('CURMASTER') AND NOT EOF('CURMASTER') THEN
				
					lcFileDateTime = PADL(YEAR(ldToday),4,"0") + PADL(MONTH(ldToday),2,"0") + PADL(DAY(ldToday),2,"0") + STRTRAN(SUBSTR(TIME(),1,5),":","")
					lcOutputfile = .cOutputFolder + "MORETINBOUND_" + lcFileDateTime 
					
					WAIT WINDOW NOWAIT "Opening Excel..."
					oExcel = CREATEOBJECT("excel.application")
					oExcel.VISIBLE = .F.
					oExcel.DisplayAlerts = .F.
					oWorkbook = oExcel.workbooks.OPEN("F:\UTIL\MORET\TEMPLATES\MORETINBOUND.XLS")
					oWorkbook.SAVEAS(lcOutputfile)
					oWorksheet = oWorkbook.Worksheets[1]

					lnRow = 1
					SELECT CURMASTER
					SCAN
						lnRow = lnRow + 1
						lcRow = LTRIM(STR(lnRow))
						oWorksheet.RANGE("A" + lcRow).VALUE = CURMASTER.DESCRIP
						oWorksheet.RANGE("B" + lcRow).VALUE = CURMASTER.WO_NUM
						oWorksheet.RANGE("C" + lcRow).VALUE = CURMASTER.WO_DATE
						oWorksheet.RANGE("D" + lcRow).VALUE = CURMASTER.ACCTNAME
						oWorksheet.RANGE("E" + lcRow).VALUE = CURMASTER.CONTAINER
						oWorksheet.RANGE("F" + lcRow).VALUE = CURMASTER.REFERENCE
						oWorksheet.RANGE("G" + lcRow).VALUE = CURMASTER.ACCT_REF
						oWorksheet.RANGE("H" + lcRow).VALUE = CURMASTER.BROKERREF
						oWorksheet.RANGE("I" + lcRow).VALUE = DTOC(TTOD(CURMASTER.CONFIRMDT))
					ENDSCAN
					
					oWorkbook.SAVE()
					oWorkbook.CLOSE()
					oExcel.QUIT()
					RELEASE ALL LIKE oWorkbook
					RELEASE ALL LIKE oExcel

					.cAttach = lcOutputfile + ".XLS"
				ELSE
					.TrackProgress('No Data to Report!', LOGIT+SENDIT+NOWAITIT)
				ENDIF

				.TrackProgress('Moret Inbound Report Process ended normally!',LOGIT+SENDIT)
				
				.cBodyText = "See attached Moret Inbound Report." + CRLF + CRLF + "<process log follows>" + ;
					CRLF + CRLF + .cBodyText

			CATCH TO loError

				IF TYPE('oExcel') = "O" AND NOT oExcel = NULL THEN
					oExcel.QUIT()
					RELEASE ALL LIKE oExcel
				ENDIF

				.TrackProgress('There was an error.',LOGIT+SENDIT)
				.TrackProgress('The Error # is: ' + TRANSFORM(loError.ERRORNO), LOGIT+SENDIT)
				.TrackProgress('The Error Message is: ' + TRANSFORM(loError.MESSAGE), LOGIT+SENDIT)
				.TrackProgress('The Error occurred in Program: ' + TRANSFORM(loError.PROCEDURE), LOGIT+SENDIT)
				.TrackProgress('The Error occurred at line #: ' + TRANSFORM(loError.LINENO), LOGIT+SENDIT)
				.nNumberOfErrors = .nNumberOfErrors + 1
				CLOSE DATA

			ENDTRY

			CLOSE DATA
			*CLOSE ALL
			WAIT CLEAR
			***************** INTERNAL email results ******************************
			.TrackProgress('About to send status email.',LOGIT)
			.TrackProgress('The logfile is: ' + .cLogFile, SENDIT)
			.TrackProgress('Moret Inbound Report Process started: ' + .cStartTime, LOGIT+SENDIT)
			.TrackProgress('Moret Inbound Report Process finished: ' + TTOC(DATETIME()), LOGIT+SENDIT)

			IF .lSendInternalEmailIsOn THEN
				DO FORM dartmail2 WITH .cSendTo,.cFrom,.cSubject,.cCC,.cAttach,.cBodyText,"A"
				.TrackProgress('Sent status email.',LOGIT)
			ELSE
				.TrackProgress('Did not send status email: THIS.lSendInternalEmailIsOn = FALSE.',LOGIT)
			ENDIF

		ENDWITH
		RETURN
	ENDFUNC && main


	PROCEDURE LOGERROR
		LPARAMETERS tcError
		WITH THIS
			.TrackProgress(tcError,LOGIT+SENDIT)
			.cErrors = .cErrors + ALLTRIM(tcError) + CRLF
		ENDWITH
	ENDPROC


	PROCEDURE TrackProgress
		* do any combination of Wait Window, writing to logfile, and adding to body of email,
		* based on nFlags parameter.
		LPARAMETERS tcExpression, tnFlags
		WITH THIS
			IF BITAND(tnFlags,LOGIT) = LOGIT THEN
				IF .lLoggingIsOn THEN
					?
					? .cProcessName + "  " + TTOC(DATETIME()) + ": " + tcExpression
				ENDIF
			ENDIF
			IF .lWaitWindowIsOn THEN
				IF BITAND(tnFlags,NOWAITIT) = NOWAITIT THEN
					WAIT WINDOW tcExpression NOWAIT
				ENDIF
				IF BITAND(tnFlags,WAITIT) = WAITIT THEN
					WAIT WINDOW tcExpression TIMEOUT .nWaitWindowTimeout
				ENDIF
			ENDIF
			IF BITAND(tnFlags,SENDIT) = SENDIT THEN
				IF .lSendInternalEmailIsOn THEN
					.cBodyText = .cBodyText + tcExpression + CRLF + CRLF
				ENDIF
			ENDIF
		ENDWITH
	ENDPROC  &&  TrackProgress


	PROCEDURE SortArrayByDateTime
		* expects taInArray[] to be the result of an ADIR()
		LPARAMETERS taInArray, taOutArray
		EXTERNAL ARRAY taInArray, taOutArray
		LOCAL lnRows, lnCols, i, j
		* extending taInArray[] to process the files in date/time order
		lnRows = ALEN(taInArray,1)
		lnCols = ALEN(taInArray,2)
		* create taOutArray with one more column
		DIMENSION taOutArray[lnRows,lnCols + 1]
		* and fill it with taInArray columns AND (filedate + filetime) in new column
		* ( ACOPY() doesn't preserve elements properly because of different # of columns )
		FOR i = 1 TO lnRows
			FOR j = 1 TO lnCols
				taOutArray[i,j] = taInArray[i,j]
			ENDFOR
			taOutArray[i,lnCols + 1] = DTOS(taInArray[i,3]) + "_" + taInArray[i,4]
		ENDFOR
		* sort taOutArray on date/time column
		=ASORT(taOutArray,6)
	ENDPROC


ENDDEFINE
