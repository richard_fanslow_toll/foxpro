PARAMETERS cFilename

lLX = .F.
lnEaches = 0
nRepQty = 0
lPick = .F.
lPrepack = .T.
cBillTo = ""
lPackQty = 0
m.isa_num = ""
cVendNum = ""

SET ASSERTS ON
SELECT x856
SET FILTER TO
LOCATE

COUNT FOR TRIM(segment) = "W05" TO nSegCnt
cSegCnt = ALLTRIM(STR(nSegCnt))
WAIT "There are "+cSegCnt+" P/Ts in this file" WINDOW TIMEOUT 2
nPTqty = nSegCnt
SELECT x856
LOCATE

*!*	IF !USED("uploaddet")
*!*		IF lTesting OR lTestUploaddet
*!*			USE F:\3pl\test\uploaddet IN 0 ALIAS uploaddet
*!*		ELSE
*!*			USE F:\ediwhse\DATA\uploaddet IN 0 SHARED ALIAS uploaddet
*!*		ENDIF
*!*	ENDIF

SELECT xpt
DELETE ALL
SELECT xptdet
DELETE ALL

STORE 0 TO pt_total_cartons
STORE 0 TO nCtnCount
STORE 0 TO ptctr
STORE 0 TO nTotWt,nTotCube
cStoreNum = ""

SELECT x856
LOCATE
IF lTesting
	BROWSE
ENDIF

IF lTesting
	ASSERT .F. MESSAGE "At x856 Scan"
ENDIF
m.PTDETID=0
m.adddt = DATETIME()
m.addby = "FMI-PROC"
m.addproc = "INTRA940"
nXPTQty = 0

DO WHILE !EOF("x856")
	cSegment = TRIM(x856.segment)
	cF1 = TRIM(x856.f1)

	IF INLIST(TRIM(x856.segment),"GS","ST")
		SELECT x856
		IF !EOF()
			SKIP 1 IN x856
			LOOP
		ENDIF
	ENDIF

	IF TRIM(x856.segment) = "ISA"
		cISA_Num = ALLTRIM(x856.f13)
		SKIP 1 IN x856
		LOOP
	ENDIF

	IF TRIM(x856.segment) = "W05"  && new shipment
		SELECT xpt
		APPEND BLANK
		STORE 0 TO pt_total_cartons
		nXPTQty = 0
		ptctr = ptctr +1
		GATHER MEMVAR FIELDS addby,adddt,addproc
		REPLACE xpt.ptid       WITH ptctr IN xpt
		REPLACE xpt.ptdate     WITH DATE() IN xpt
		REPLACE xpt.accountid WITH nAcctNum IN xpt
		cShip_ref = ALLTRIM(x856.f2)
		cCNEE_ref = UPPER(ALLTRIM(x856.f3))
		REPLACE xpt.ship_ref WITH cShip_ref IN xpt && Pickticket
		REPLACE xpt.cnee_ref WITH cCNEE_ref IN xpt  && Customer PO
		WAIT CLEAR
		waitstr = "Now processing PT # "+cShip_ref+CHR(13)+"FILE: "+cFilename
		WAIT WINDOW AT 10,10  waitstr NOWAIT
	ENDIF

	IF TRIM(x856.segment) = "GE"
		CLEAR
	ENDIF

	IF TRIM(x856.segment) = "N1" AND INLIST(TRIM(x856.f1),"ST","CN")
		cDCNum = UPPER(ALLTRIM(x856.f4))
		nStoreNum = INT(VAL(RIGHT(cDCNum,5)))
		cConsignee = UPPER(ALLT(x856.f2))
		IF "BBB"$cConsignee OR "BB&B"$cConsignee OR ("BED"$cConsignee AND "BATH"$cConsignee)  && Bed Bath & Beyond only
			ASSERT .F. MESSAGE "At store number load...debug"
		ENDIF
		REPLACE xpt.dcnum WITH cDCNum IN xpt
		REPLACE xpt.storenum WITH nStoreNum IN xpt
		IF EMPTY(ALLTRIM(xpt.shipins))
			REPLACE xpt.shipins WITH "STORENUM*"+cDCNum IN xpt  && DC or Store Number
		ELSE
			IF !"STORENUM*"$xpt.shipins
				REPLACE xpt.shipins WITH xpt.shipins+CHR(13)+"STORENUM*"+cDCNum IN xpt  && DC or Store Number
			ENDIF
		ENDIF
		REPLACE xpt.consignee WITH cConsignee IN xpt NEXT 1
		REPLACE xpt.NAME WITH xpt.consignee IN xpt NEXT 1
		SKIP 1 IN x856
		DO CASE
			CASE TRIM(x856.segment) = "N3"  && address info
*				ASSERT .F. MESSAGE "In N3 Shipto"
				REPLACE xpt.address WITH UPPER(TRIM(x856.f1)) IN xpt
				REPLACE xpt.address2  WITH UPPER(TRIM(x856.f2)) IN xpt
			OTHERWISE
				cMessage = "Missing Ship-To Address Info: PT "+cShip_ref
				SET STEP ON
				WAIT WINDOW cMessage TIMEOUT 2
				NormalExit = .F.
				THROW
		ENDCASE
		SKIP 1 IN x856
		IF TRIM(x856.segment) = "N4"  && address info
			cCSZ = TRIM(x856.f1)+", "+TRIM(x856.f2)+" "+TRIM(x856.f3)
			REPLACE xpt.csz WITH UPPER(cCSZ) IN xpt
		ELSE
			cMessage = "Missing Ship-To CSZ Info"
			WAIT WINDOW cMessage TIMEOUT 2
			NormalExit = .F.
			THROW
		ENDIF
	ENDIF

	IF TRIM(x856.segment) = "N9"
		DO CASE
			CASE TRIM(x856.f1) = "IA" && Vendor Num
				cVendNum = TRIM(x856.f2)
				REPLACE xpt.vendor_num WITH cVendNum IN xpt

			CASE TRIM(x856.f1) = "DP" && Department
				cDept = TRIM(x856.f2)
				REPLACE xpt.dept WITH cDept IN xpt

			CASE TRIM(x856.f1) = "14"
				IF EMPTY(ALLTRIM(xpt.shipins))
					REPLACE xpt.shipins WITH "MASTACCT*"+TRIM(x856.f2)
				ELSE
					REPLACE xpt.shipins WITH xpt.shipins+CHR(13)+"MASTACCT*"+TRIM(x856.f2)
				ENDIF
				REPLACE xpt.cacctnum WITH ALLTRIM(x856.f2) IN xpt

			CASE TRIM(x856.f1) = "ST"
				cStoreNum = TRIM(x856.f2)
				IF EMPTY(ALLTRIM(xpt.shipins))
					REPLACE xpt.shipins WITH "STORENUM*"+cStoreNum
				ELSE
					REPLACE xpt.shipins WITH xpt.shipins+CHR(13)+"DEPTNAME*"+cStoreNum
				ENDIF
		ENDCASE
	ENDIF

	IF TRIM(x856.segment) = "G62" AND TRIM(x856.f1) == "37" && PO Date
		cXdate1 = TRIM(x856.f2)
		datestrconversion()
		REPLACE xpt.START WITH dxdate2 IN xpt
	ENDIF

	IF TRIM(x856.segment) = "G62" AND TRIM(x856.f1) == "38" && Cancel Date
		cXdate1 = TRIM(x856.f2)
		datestrconversion()
		REPLACE xpt.CANCEL WITH dxdate2 IN xpt
	ENDIF

	IF TRIM(x856.segment) = "W66" && Ship Instr.
		cShip_via = UPPER(TRIM(x856.f5))
		cSCAC = UPPER(TRIM(x856.f10))
		REPLACE xpt.ship_via WITH cShip_via,xpt.scac WITH cSCAC IN xpt
	ENDIF


*********************************************
*!* XPTDET Update Section
*********************************************

	IF TRIM(x856.segment) = "LX" && start of PT detail, stay here until this PT is complete
		lLX = .T.


		DO WHILE TRIM(x856.segment) != "SE"
			cxSegment = TRIM(x856.segment)
			cxField01 = TRIM(x856.f1)

			IF TRIM(x856.segment) = "LX"
*				REPLACE xptdet.printstuff WITH xptdet.printstuff+CHR(13)+"LINENUM*"+TRIM(x856.f1) IN xptdet
				m.linenum = TRIM(x856.f1)
				SKIP 1 IN x856
				LOOP
			ENDIF

			m.units = .F.
			IF TRIM(x856.segment) = "W01"
				m.totqty = INT(VAL(x856.f1))
*				nXPTQty = nXPTQty + m.totqty
				m.origqty = m.totqty
				IF TRIM(x856.f4) = "IN"
					m.style = ALLT(x856.f5)
					lPick = .F.
					lPrepack = !lPick
					REPLACE xpt.goh WITH IIF(lPick,.T.,.F.) IN xpt
				ENDIF
				IF TRIM(x856.f6) = "UP"
					m.upc = ALLT(x856.f7)
				ENDIF
				IF TRIM(x856.f15) = "VN"
					m.custsku = ALLT(x856.f16)
				ENDIF

				SELECT xptdet
				APPEND BLANK
				m.PTDETID=m.PTDETID+1
				GATHER MEMVAR
				REPLACE xptdet.totqty WITH 0, xptdet.origqty WITH 0 IN xptdet
				REPLACE xptdet.addby WITH "FMI PROC" IN xptdet
				REPLACE xptdet.adddt WITH DATETIME() IN xptdet
				REPLACE xptdet.ptid      WITH xpt.ptid IN xptdet
				REPLACE xptdet.accountid WITH nAcctNum IN xptdet

				nPack = 1
				cPack = ALLT(STR(nPack))
				nAddQty = (INT(VAL(x856.f1)))
				REPLACE xptdet.totqty WITH nAddQty IN xptdet
				REPLACE xptdet.origqty WITH xptdet.totqty IN xptdet
				REPLACE xptdet.PACK WITH cPack IN xptdet
				REPLACE xptdet.casepack WITH nPack IN xptdet
				REPLACE xptdet.units WITH IIF(lPick,.T.,.F.) IN xptdet
				nPack = 0
				nXPTQty = nXPTQty + nAddQty
				lnEaches = 0
			ENDIF
			SELECT x856

			IF TRIM(x856.segment) = "G69"
				REPLACE xptdet.printstuff WITH xptdet.printstuff+CHR(13)+"DESC*"+TRIM(x856.f1) IN xptdet
			ENDIF

			IF TRIM(x856.segment) = "W76"
				REPLACE xpt.qty WITH INT(VAL(x856.f1)) IN xpt
				REPLACE xpt.origqty WITH xpt.qty IN xpt
				nXPTQty = 0
				lLX = .F.
				REPLACE xpt.EACHES WITH INT(VAL(TRIM(x856.f1))) IN xpt
				STORE 0 TO nTotWt,nTotCube,pt_total_cartons
			ENDIF

			SELECT xptdet
			IF xptdet.ptid = 0
				DELETE IN xptdet
			ENDIF

			SELECT x856
			IF !EOF()
				SKIP 1 IN x856
			ENDIF
		ENDDO  && SEGMENT NOW = 'SE'
	ENDIF
	IF !EOF()
		SKIP 1 IN x856
	ENDIF
ENDDO

DO m:\dev\prg\setuppercase940

WAIT CLEAR
IF lBrowfiles
	WAIT WINDOW "Now displaying XPT/XPTDET cursors..."+CHR(13)+"CANCEL at XPTDET if desired." TIMEOUT 2
	SELECT xpt
	SET ORDER TO
	LOCATE
	BROWSE
	SELECT xptdet
	SET ORDER TO
	LOCATE
	BROWSE
	IF MESSAGEBOX("Do you wish to continue?",4+32+256,"Browse Files Box")<>6
		CANCEL
	ENDIF
ENDIF

WAIT CLEAR
WAIT cMailName+" BKDN process complete for file "+xfile WINDOW TIMEOUT 2
lLX = .F.
lnEaches = 0
lPick = .F.
nRepQty = 0
m.isa_num = ""
SELECT x856
lNewPT = .T.
RETURN

******************************
PROCEDURE datestrconversion
******************************
	cXdate1 = TRIM(cXdate1)
	dxdate2 = CTOD(SUBSTR(cXdate1,5,2)+"/"+RIGHT(cXdate1,2)+"/"+LEFT(cXdate1,4))
	RETURN
