* cleans WMS data; runs whmatch & whcheck to validate WMS data

if day(date())=1
	return
endif

utilsetup("VAL1")

store "" to guserid, gprocess
gdevelopment=.f.
xsystem="VAL1"

xpnpaccount="inlist(accountid,1"
xsqlexec("select * from account where inactive=0","account",,"qq")
index on accountid tag accountid
set order to
scan for pnpn or pnpc or pnpm or pnpl
	xpnpaccount=xpnpaccount+","+transform(accountid)
endscan
xpnpaccount=xpnpaccount+"))"

* Pack PTBAK & PTDETBAK; compress INVENLOC; remove pullid=0 in PULLDET, purge & pack PHYS, clean up rawdata

wait window ttoc(datetime())+":  cleaning warehousing data" nowait
? "cleaning warehousing data" 

guserid="AUTOPACK"
gprocess=guserid

xsqlexec("select * from whoffice",,,"wh")

select whoffice
scan for !test and !novalid
	if date()={4/8/15}
		loop
	endif

	try
		delete file (whoffice.folder+"\*.tmp")
	catch
	endtry

	xoffice=whoffice.office
	goffice=xoffice
	? xoffice

	if dow(date())=1
		try
			use (wf(xoffice)+"ptdel") exclusive in 0
			select ptdel
			delete for ptdate<date()-14
			pack
		catch
		endtry
	endif
	
	xsqlexec("delete from wosemi",,,"wh")
	xsqlexec("delete from ufsemi",,,"wh")
endscan

**delete and pack wavelabels - mvw 12/23/11

if usesqlwave()
	if dow(date())=1
		xsqlexec("delete from wavelabels where processdt<{"+dtoc(date()-90)+"} and processdt#{}",,,"wh")
	endif
else
	if dow(date())=1
		try
			use f:\wh\wavelabels exclusive in 0
			delete for processdt<date()-150 and processdt#{} in wavelabels
			pack in wavelabels
			use in wavelabels
		catch
		endtry
	endif
endif

* check warehousing data

wait window ttoc(datetime())+":  checking warehousing data" nowait
? "checking warehousing data" 
 
xsqlexec("select * from whoffice",,,"wh")

copy to array aofflist fields office for !test and !novalid
copy to array amasterofflist fields rateoffice for !test and !novalid

for xqq=1 to alen(aofflist,1)
	xoffice=aofflist[xqq]
	goffice=xoffice
	gmasteroffice=amasterofflist[xqq]
	gmodfilter="mod='"+goffice+"'"

	if !inlist(goffice,"M","Z","6","8","L","I","1","5","Y")		&& split into val1a   dy 7/13/16
		loop
	endif


	if date()={1/16/17} and goffice#"M"
		loop
	endif

	val1do()
next

email("dyoung@fmiint.com","VAL1",gsystemmodule,,,,.t.,,,,,.t.)

*close databases all

*

schedupdate()

_screen.Caption=gscreencaption
on error
