PARAMETERS nWO_Num,nAcctNum,cOffice
PUBLIC c_CntrlNum,c_GrpCntrlNum,cWO_Num,cSF_CSZ,tccmissloc
*Close Databases ALL
DO m:\dev\prg\_setvars
on error throw
cWO_Num = ""
lTesting = .F.
lEmail = .T.
lDoError = .F.
lTestmail = .F.
lCopyFiles = .T.
cSF_CSZ = ""
tccmissloc = ""
IF VARTYPE(nWO_Num)= "L"
	WAIT WINDOW "No params specified" TIMEOUT 2
	RELEASE ALL
	RETURN
*	lTesting = .T.
ENDIF

IF lTesting
	CLOSE DATABASES ALL
	nWO_Num = 1062264
	nAcctNum = 4700
	cOffice = "C"
ENDIF
xReturn = "XXX"
DO m:\dev\prg\wf_alt WITH cOffice,nAcctNum
IF xReturn = "XXX"
	DO errormail WITH "MISSACCT"
	closedata()
	RETURN
ENDIF

cUseFolder = UPPER(TRIM(xReturn))
cWO_Num = ALLTRIM(STR(nWO_Num))

DIMENSION thisarray(1)

SELECT 0
USE F:\3pl\DATA\mailmaster ALIAS mm SHARED
LOCATE FOR mm.accountid = 4700 ;
	AND mm.office = cOffice ;
	AND mm.edi_type = "214"
IF !FOUND()
	ASSERT .F. MESSAGE "At missing Office/Acct"
	WAIT WINDOW "Office/Loc/Acct not found!" TIMEOUT 2
	DO ediupdate WITH .T.,"ACCT/LOC !FOUND"
	NormalExit = .F.
	THROW
ENDIF
STORE TRIM(mm.acctname) TO cCustName
STORE TRIM(mm.basepath) TO lcOutPath
STORE TRIM(mm.holdpath) TO lcHoldPath
STORE TRIM(mm.archpath) TO lcArchivePath
tsendto = IIF(mm.USE_ALT,mm.sendtoalt,mm.sendto)
tcc = IIF(mm.USE_ALT,mm.ccalt,mm.cc)
LOCATE
LOCATE FOR mm.edi_type = "MISC" AND mm.taskname = "GENERAL"
tsendtoerr = IIF(mm.USE_ALT,mm.sendtoalt,mm.sendto)
tccerr = IIF(mm.USE_ALT,mm.ccalt,mm.cc)
tsendtotest = IIF(mm.USE_ALT,mm.sendtoalt,mm.sendto)
tcctest = IIF(mm.USE_ALT,mm.ccalt,mm.cc)
LOCATE
LOCATE FOR mm.edi_type = "MISC" AND accountid = 4700 AND mm.taskname = "MISSLOC"
tccmissloc = IIF(mm.USE_ALT,mm.ccalt,mm.cc)
USE IN mm


tfrom = "Toll EDI Operations <toll-edi-ops@tollgroup.com>"
tcc = ""
tattach = ""
cGroupName = ""

*!* Open Tables
closedata()
USE F:\3pl\DATA\vf214_wo IN 0 ORDER TAG wo_num ALIAS vf214_wo

xsqlexec("select * from account where inactive=0","account",,"qq")
index on accountid tag accountid

IF SEEK(nAcctNum,"account","accountid")
	cGroupName = ALLTRIM(account.acctname)
ELSE
	DO errormail WITH "MISSACCT"
	closedata()
	RETURN
ENDIF

USE F:\wo\wodata\DETAIL IN 0 ALIAS DETAIL
USE F:\3pl\DATA\vf_unloccodes IN 0 ALIAS unloccodes

xsqlexec("select * from dellocs",,,"wo")

IF !SEEK(nWO_Num,'DETAIL','WO_NUM')
	SELECT vf214_wo
	IF !SEEK(nWO_Num)
		INSERT INTO vf214_wo (wo_num,processed) VALUES (nWO_Num,.F.)
	ELSE
		REPLACE processed WITH .F. NEXT 1
	ENDIF
	SELECT DETAIL
	DO errormail WITH "MISSWO"
	closedata()
	RETURN
ELSE
	SELECT dellocs
	LOCATE
	LOCATE FOR dellocs.accountid = DETAIL.accountid AND dellocs.div = DETAIL.div
	IF FOUND()
		cDellocCity = UPPER(ALLTRIM(dellocs.city))
		cDellocState = UPPER(ALLTRIM(dellocs.state))
		cDellocZip = ALLTRIM(dellocs.zip)
		IF cDellocCity = "JFK"
			cDelCS = "JFK"
		ELSE
			cDelCS = cDellocCity+", "+cDellocState
		ENDIF
		cDellocUNCode = "US*UN*XYZ"
		IF !SEEK(cDelCS,'unloccodes','city')
			DO errormail WITH "MISSLOC"
			closedata()
			RETURN
		ELSE
			cDellocUNCode = ALLTRIM(unloccodes.CODE)
		ENDIF
		cSF_CSZ = cDellocCity+"*"+cDellocState+"*"+cDellocZip+"*"+cDellocUNCode
	ENDIF
ENDIF

cCustName = "VF"
cCustFolder = "VF"
cMailName = PROPER(STRTRAN(cGroupName,"VF","VANITY FAIR"))
cCustPrefix = cCustName+"214"+LOWER(cOffice)
cDivision = IIF(cOffice="C","California",IIF(cOffice="M","Miami","New Jersey"))

*!* Ship-from Information
IF EMPTY(cSF_CSZ)
	DO CASE
		CASE cOffice = "N"
			cSF_Name = "PORT OF NEWARK/ELIZABETH"
			cSF_Addr = ("C/O "+cGroupName+"*ELIZABETH")
			cSF_CSZ = "ELIZABETH*NJ*07206*US*UN*ELZ"
		CASE cOffice = "M"
			cSF_Name = "PORT OF MIAMI"
			cSF_Addr = ("C/O "+cGroupName+"*MIAMI")
			cSF_CSZ = "MIAMI*FL*33132*US*UN*MIA"
		OTHERWISE
			cSF_Name = "LOS ANGELES PORTS"
			cSF_Addr = ("C/O "+cGroupName+"*LOS ANGELES")
			cSF_CSZ = "LOS ANGELES*CA*90731*US*UN*LAX"
	ENDCASE
ENDIF

*!* Ship-to Information
cST_Name = "TGF INC."
DO CASE
	CASE cOffice = "C"
		cST_Addr =  ("C/O "+cGroupName+"*450 WESTMONT DRIVE")
		cFMICity = "SAN PEDRO"
		cFMIState = "CA"
		cFMIZip = "90731"
		cST_CSZ    = "SAN PEDRO*CA*90731*US*UN*SPQ"
	CASE cOffice = "N"
		cFMICity = "CARTERET"
		cFMIState = "NJ"
		cFMIZip = "07008"
		cST_Addr =  ("C/O "+cGroupName+"*800 FEDERAL BLVD")
		cST_CSZ    = "CARTERET*NJ*07095*US*UN*CSF"
	CASE cOffice = "M"
		cFMICity = "MIAMI"
		cFMIState = "FL"
		cFMIZip = "33178"
		cST_Addr =  ("C/O "+cGroupName+"*2100 NW 129th AVE SUITE 100")
		cST_CSZ    = "MIAMI*FL*33172*US*UN*MDY"
ENDCASE

cfd = "*"  && Field delimiter
csegd = "~"  && Segment delimiter
cterminator = ">"  && Used at end of ISA segment

csendqual = "ZZ"  && Sender qualifier
csendid = "FMIT"  && Sender ID code
crecqual = "ZZ"  && Recip qualifier
crecid = "APLUNET"   && Recip ID Code
cDate = DTOS(DATE())
cTruncDate = RIGHT(cDate,6)
cTruncTime = SUBSTR(TTOC(DATETIME(),1),9,4)
cfiledate = cDate+cTruncTime
csendidlong = PADR(csendid,15," ")
crecidlong = PADR(crecid,15," ")
dt1 = TTOC(DATETIME(),1)
dtmail = TTOC(DATETIME())
dt2 = DATETIME()
cString = ""

cDirUsed = ("F:\"+cCustName+"\")
SET DEFAULT TO &cDirUsed

cFilenameShort = (cCustPrefix+dt1+".txt")
cFilename = (lcHoldPath+cFilenameShort)
cFilename2 = (lcArchivePath+cFilenameShort)
cFilename3 = (lcOutPath+cFilenameShort)
nFilenum = FCREATE(cFilename)
nSTCount = 0
nSegCtr = 0
nLXNum = 1
STORE "" TO cEquipNum,cHAWB

IF lTesting
	cISACode = "T"
ELSE
	cISACode = "P"
ENDIF

*SET STEP ON
DO num_incr_isa
cISA_Num = PADL(c_CntrlNum,9,"0")
STORE "ISA"+cfd+"00"+cfd+"          "+cfd+"00"+cfd+"          "+cfd+csendqual+cfd+csendidlong+cfd+;
	crecqual+cfd+crecidlong+cfd+cTruncDate+cfd+cTruncTime+cfd+"^"+cfd+"00501"+cfd+;
	cISA_Num+cfd+"0"+cfd+cISACode+cfd+cterminator+csegd TO cString
DO cstringbreak

STORE "GS"+cfd+"QM"+cfd+csendid+cfd+crecid+cfd+cDate+cfd+cTruncTime+cfd+c_CntrlNum+;
	cfd+"X"+cfd+"005010"+csegd TO cString
DO cstringbreak

DO num_incr_st
STORE "ST"+cfd+"214"+cfd+PADL(c_GrpCntrlNum,9,"0")+csegd TO cString
DO cstringbreak
nSTCount = nSTCount + 1
nSegCtr = nSegCtr + 1

SELECT DETAIL
SET ORDER TO wo_num   && WO_NUM
cHAWB = ALLTRIM(DETAIL.hawb)
SET ORDER TO

STORE "B10"+cfd+cWO_Num+cfd+cHAWB+cfd+csendid+csegd TO cString
DO cstringbreak
nSegCtr = nSegCtr + 1

STORE "MS3"+cfd+csendid+cfd+"1"+cfd+cFMICity+cfd+"J"+cfd+cFMIState+csegd TO cString
DO cstringbreak
nSegCtr = nSegCtr + 1


WAIT CLEAR
WAIT WINDOW "Now creating Trucking WO#-based 214 information..." NOWAIT

*!* Now in line loop
SCAN FOR DETAIL.wo_num = nWO_Num

	STORE "LX"+cfd+ALLTRIM(STR(nLXNum))+csegd TO cString  && Size
	DO cstringbreak
	nSegCtr = nSegCtr + 1
	nLXNum = nLXNum + 1

	STORE "L11"+cfd+"001"+cfd+"8X"+csegd TO cString
	DO cstringbreak
	nSegCtr = nSegCtr + 1

	STORE "L11"+cfd+cGroupName+cfd+"IC"+csegd TO cString  && added 9/27/06
	DO cstringbreak
	nSegCtr = nSegCtr + 1

	cMBOL = ALLTRIM(DETAIL.hawb)
	STORE "L11"+cfd+cMBOL+cfd+"MB"+cfd+"BILL OF LADING"+csegd TO cString
	DO cstringbreak
	nSegCtr = nSegCtr + 1

	cEquipNum = ALLTRIM(DETAIL.CONTAINER)
	cEquipQual = "EQ"
	cEquipDesc = "EQUIPMENT NUMBER"
	IF EMPTY(cEquipNum)
		cEquipNum = ALLTRIM(DETAIL.awb)
		IF EMPTY(cEquipNum)
			DO errormail WITH "MISSEQUIP"
			=FCLOSE(nFilenum)
			DELETE FILE &cFilename
			closedata()
			RETURN
		ENDIF
		cEquipQual = "AW"
		cEquipDesc = "AIRBILL NUMBER"
	ENDIF
	STORE "L11"+cfd+cEquipNum+cfd+cEquipQual+cfd+cEquipDesc+csegd TO cString
	DO cstringbreak
	nSegCtr = nSegCtr + 1

	cPO_Num = ALLTRIM(DETAIL.po_num)
	STORE "L11"+cfd+cPO_Num+cfd+"PO"+cfd+"PURCHASE ORDER NUMBER"+csegd TO cString
	DO cstringbreak
	nSegCtr = nSegCtr + 1

	cArrival = ALLTRIM(DETAIL.arrival)
	IF !EMPTY(cArrival)
		STORE "L11"+cfd+cArrival+cfd+"CR"+cfd+"ASN NUMBER"+csegd TO cString
		DO cstringbreak
		nSegCtr = nSegCtr + 1
	ENDIF

	cLoadWeight = ALLTRIM(STR(DETAIL.weight))
	cCartonsExp = ALLTRIM(STR(DETAIL.pl_qty))
	IF !EMPTY(DETAIL.rcv_qty)
		cCartonsRec = ALLTRIM(STR(DETAIL.rcv_qty))
	ELSE
		cCartonsRec = cCartonsExp
	ENDIF
	STORE "AT8"+cfd+"G"+cfd+"L"+cfd+cLoadWeight+cfd+cCartonsExp+cfd+cCartonsRec+csegd TO cString
	DO cstringbreak
	nSegCtr = nSegCtr + 1

	STORE "AT7"+cfd+"X1"+REPLICATE(cfd,4)+cDate+cfd+cTruncTime+cfd+"LT"+csegd TO cString
	DO cstringbreak
	nSegCtr = nSegCtr + 1

	cImportSeal = "XXX"
	STORE "M7"+cfd+cImportSeal+REPLICATE(cfd,4)+"SH"+csegd TO cString
	DO cstringbreak
	nSegCtr = nSegCtr + 1

*!*		STORE "N1"+cfd+cSF_Name+csegd TO cString
	N101 = "XG"
	cEventLoc = IIF(N101="XG","EVENT LOCATION",cSF_Name)
	STORE "N1"+cfd+N101+cfd+cEventLoc+csegd TO cString
	DO cstringbreak
	nSegCtr = nSegCtr + 1

*!*		STORE "N3"+cfd+cSF_Addr+csegd TO cString
*!*		DO cstringbreak
*!*		nSegCtr = nSegCtr + 1

	STORE "N4"+cfd+cSF_CSZ+csegd TO cString

	DO cstringbreak
	nSegCtr = nSegCtr + 1

	STORE "G62"+cfd+"69"+cfd+cDate+csegd TO cString
	DO cstringbreak
	nSegCtr = nSegCtr + 1

	STORE "N1"+cfd+"SH"+cfd+cST_Name+csegd TO cString
	DO cstringbreak
	nSegCtr = nSegCtr + 1

	STORE "N3"+cfd+cST_Addr+csegd TO cString
	DO cstringbreak
	nSegCtr = nSegCtr + 1

	STORE "N4"+cfd+cST_CSZ+csegd TO cString
	DO cstringbreak
	nSegCtr = nSegCtr + 1

	STORE "G62"+cfd+"70"+cfd+cDate+csegd TO cString
	DO cstringbreak
	nSegCtr = nSegCtr + 1

ENDSCAN

*!* Finish processing
DO close214
=FCLOSE(nFilenum)

IF lCopyFiles
	COPY FILE &cFilename TO &cFilename2
	COPY FILE &cFilename TO &cFilename3
	DELETE FILE &cFilename
ENDIF

IF !lTesting
	tcc = " "
	IF !USED("ftpedilog")
		SELECT 0
		USE F:\edirouting\ftpedilog ALIAS ftpedilog
		INSERT INTO ftpedilog (TRANSFER,ftpdate,filename,acct_name,TYPE) VALUES ("214-"+cCustName,dt2,cFilename,UPPER(cCustName),"214")
		USE IN ftpedilog
	ENDIF
ENDIF

IF lTesting
	tsubject = "VF 214 *TEST* EDI from TGF as of "+dtmail
ELSE
	tsubject = "VF 214 EDI from TGF as of "+dtmail
ENDIF

tmessage = "214 EDI Info from TGF-CA, Truck WO# "+cWO_Num+CHR(10)
tmessage = tmessage + "for coalition "+cMailName+CHR(10)
tmessage = tmessage +"has been created."
*!*	+CHR(10)+CHR(10)
*!*	tmessage = tmessage + "If you have any questions, please eMail or call 732-750-9000 x217."
IF lTesting
	tmessage = tmessage + CHR(10)+CHR(10)+"This is a TEST RUN..."
ENDIF

IF lTestmail
	tsendto = "joe.bianchi@tollgroup.com"
	tcc = " "
	tattach = cFilename
ENDIF
IF lEmail
	DO FORM m:\dev\frm\dartmail2 WITH tsendto,tfrom,tsubject,tcc,tattach,tmessage,"A"
ENDIF

IF !lTesting

	SELECT edi_trigger
	REPLACE edi_trigger.processed WITH .T.,edi_trigger.created WITH .T.,proc214 WITH .T.;
		edi_trigger.when_proc WITH DATETIME(),edi_trigger.fin_status WITH "214 CREATED";
		edi_trigger.errorflag WITH .F.,file214 WITH cFilename ;
		FOR edi_trigger.wo_num = nWO_Num AND edi_type = "214"
ENDIF

IF SEEK(nWO_Num,"vf214_wo","wo_num")
	REPLACE processed WITH .T.
ELSE
	INSERT INTO vf214_wo (wo_num,processed) VALUES (nWO_Num,.T.)
ENDIF

RELEASE ALL LIKE c_CntrlNum,c_GrpCntrlNum
RELEASE ALL LIKE nOrigSeq,nOrigGrpSeq
WAIT CLEAR
WAIT WINDOW cMailName+" 214 EDI File output complete" AT 20,60 TIMEOUT 2

IF USED('vf214_wo')
	USE IN vf214_wo
ENDIF

IF USED('detail')
	USE IN DETAIL
ENDIF

IF USED('serfile')
	USE IN serfile
endif
RELEASE ALL LIKE t*
on error
_SCREEN.CAPTION = "INBOUND POLLER - EDI"
&& END OF MAIN CODE SECTION



****************************
PROCEDURE num_incr_isa
****************************

	IF !USED("serfile")
		USE ("F:\3pl\data\serial\"+cCustName+"214_serial") IN 0 ALIAS serfile
	ENDIF
	SELECT serfile
	IF lTesting
		nOrigSeq = serfile.seqnum
		nOrigGrpSeq = serfile.grpseqnum
		lISAFlag = .F.
	ENDIF
	nISA_Num = serfile.seqnum
	c_CntrlNum = ALLTRIM(STR(serfile.seqnum))
	REPLACE serfile.seqnum WITH serfile.seqnum + 1 IN serfile
	SELECT DETAIL
	RETURN

****************************
PROCEDURE num_incr_st
****************************

	IF !USED("serfile")
		USE ("F:\3pl\data\serial\"+cCustName+"214_serial") IN 0 ALIAS serfile
	ENDIF
	SELECT serfile
	IF lTesting
		nOrigSeq = serfile.seqnum
		nOrigGrpSeq = serfile.grpseqnum
		lSTFlag = .F.
	ENDIF
	c_GrpCntrlNum = ALLTRIM(STR(serfile.grpseqnum))
	REPLACE serfile.grpseqnum WITH serfile.grpseqnum + 1 IN serfile
	SELECT DETAIL
	RETURN

****************************
PROCEDURE segmentget
****************************

	PARAMETER thisarray,lcKey,nLength

	FOR i = 1 TO nLength
		IF i > nLength
			EXIT
		ENDIF
		lnEnd= AT("*",thisarray[i])
		IF lnEnd > 0
			lcThisKey =TRIM(SUBSTR(thisarray[i],1,lnEnd-1))
			IF OCCURS(lcKey,lcThisKey)>0
				RETURN SUBSTR(thisarray[i],lnEnd+1)
				i = 1
			ENDIF
		ENDIF
	ENDFOR

	RETURN ""

****************************
PROCEDURE close214
****************************
	STORE  "SE"+cfd+ALLTRIM(STR(nSegCtr+1))+cfd+PADL(c_GrpCntrlNum,9,"0")+csegd TO cString
	DO cstringbreak

	STORE  "GE"+cfd+ALLTRIM(STR(nSTCount))+cfd+c_CntrlNum+csegd TO cString
	DO cstringbreak

	STORE  "IEA"+cfd+"1"+cfd+PADL(c_CntrlNum,9,"0")+csegd TO cString
	DO cstringbreak

	RETURN

****************************
PROCEDURE cstringbreak
****************************
	cLen = LEN(ALLTRIM(cString))

	FWRITE(nFilenum,cString)
	RETURN


****************************
PROCEDURE errormail
****************************
	PARAMETERS msgtype
	ASSERT .F. MESSAGE "At Errormail...Debug"
	tsubject = cGroupName+" 214 EDI File Error"
	tmessage = "214 EDI Info from TGF-CA, Trk WO# "+cWO_Num+CHR(10)
	tcc = ""
	DO CASE
		CASE msgtype = "MISSACCT"
			tmessage = tmessage + "Account ID "+nAcctNum+" does not exist in ACCOUNT."+CHR(10)
			tmessage = tmessage + "Check account information submitted from PICKUP trigger."
			lEmail = .F.
		CASE msgtype = "MISSWO"
			tmessage = tmessage + "WO# "+cWO_Num+" does not exist in F:\WO\WODATA\DETAIL."+CHR(10)
			tmessage = tmessage + "Breakdown/scanning of freight has not been completed. Check, reset the trigger."
			lEmail = .F.
		CASE msgtype = "MISSEQUIP"
			tmessage = tmessage + "Equipment not specified for WO# "+cWO_Num+CHR(10)
			tmessage = tmessage + "Correct the data in WO\WODATA\DETAIL."
			tcc = " "
			lEmail = .F.
		CASE msgtype = "MISSLOC"
			tmessage = tmessage + "UN Location City/State missing for WO# "+cWO_Num+CHR(10)
			tcc = tccmissloc
			lEmail = .T.
	ENDCASE

	IF lTesting
		tmessage = tmessage + CHR(10)+CHR(10)+"This is TEST DATA only..."
	ENDIF
	IF lEmail
		DO FORM m:\dev\frm\dartmail2 WITH tsendto,tfrom,tsubject,tcc,tattach,tmessage,"A"
	ENDIF

	IF !USED('edi_trigger')
		cEDIFolder = IIF(DATE()<{^2010-01-24},"F:\JAG3PL\DATA\","F:\3PL\DATA\")
		USE (cEDIFolder+"edi_trigger") IN 0 ALIAS edi_trigger
	ENDIF

	SELECT edi_trigger
	REPLACE edi_trigger.processed WITH .T.,edi_trigger.created WITH .F.,when_proc WITH DATETIME();
		edi_trigger.fin_status WITH "WO MISS BKDN",edi_trigger.errorflag WITH .F.;
		FOR edi_trigger.wo_num = nWO_Num AND edi_type = "214"
	RETURN

**********************
PROCEDURE closedata
**********************

	IF USED('account')
		USE IN account
	ENDIF

	IF USED('vf214_wo')
		USE IN vf214_wo
	ENDIF

	IF USED('DETAIL')
		USE IN DETAIL
	ENDIF

	IF USED('unloccodes')
		USE IN unloccodes
	ENDIF

	IF USED('dellocs')
		USE IN dellocs
	ENDIF
ENDPROC
