*!*	CLOSE DATABASES ALL
*!*	CLEAR ALL
*!*	RELEASE ALL
SET STEP ON
lTesting = .F.  && Allows cursor browsing

_setvars(lTesting)
SET STATUS BAR ON
SET HOURS TO 24

WAIT WINDOW "" TIMEOUT 3

FOR wz = 1 TO 2

	DO CASE
		CASE wz=1
			cWhse = "MOD D"
			cUseMod = "2"

		CASE wz=2
			cWhse = "MOD C"
			cUseMod = "5"

    CASE wz=3
			cWhse = "MOD L"
			cUseMod = "L"
	ENDCASE

	WAIT WINDOW "Now running Early Triggers program, post-parcel triggering for Mod "+cUseMod NOWAIT NOCLEAR

	IF USED("edi_trigger")
		USE IN edi_trigger
	ENDIF

	IF USED("outship")
		USE IN outship
	ENDIF

	IF USED("shipment")
		USE IN shipment
	ENDIF

	IF USED("package")
		USE IN package
	ENDIF

	USE F:\3pl\DATA\edi_trigger IN 0

	xsqlexec("select * from outship where mod = '"+cUseMod+"' and del_date >= {"+DTOC(DATE()-10)+"}",,,"wh")
	INDEX ON (STR(outship.accountid,4)+outship.ship_ref) TAG acctpt


* sentoffenterdt criterion, below, will only pull shipment records greater than 1 hour old.
* changed sentoffenterdt to updatedt
* early trigger criteria should be sent = .t. and updatedt less than current time by one hour, PG 11/18/2016+
	DO CASE
		CASE wz = 1
			&& Running BCNY, Synclaire, Merkury and S3 here
			csq = [select * from shipment where accountid in (6221,6521,6561,6803) and shipdate > {]+DTOC(DATE()-3)+[} and sent = 1]
			xsqlexec(csq,,,"wh")


			IF RECCOUNT("shipment") > 0
				xjfilter="shipmentid in ("
				SCAN
					nShipmentId = shipment.shipmentid
					xsqlexec("select * from package where shipmentid="+TRANSFORM(nShipmentId),,,"wh")

					IF RECCOUNT("package")>0
						xjfilter=xjfilter+TRANSFORM(shipmentid)+","
					ELSE
						xjfilter="1=0"
					ENDIF
				ENDSCAN
				xjfilter=LEFT(xjfilter,LEN(xjfilter)-1)+")"

				xsqlexec("select * from package where "+xjfilter,,,"wh")
			ELSE
				xsqlexec("select * from package where .f.",,,"wh")
			ENDIF

		CASE wz = 2
			&& Row One
			csq = [select * from shipment where accountid in (6468) and shipdate >= {]+DTOC(DATE()-3)+[} and sent = 1]
			xsqlexec(csq,,,"wh")

			IF RECCOUNT("shipment") > 0
				xjfilter="shipmentid in ("
				SCAN
					nShipmentId = shipment.shipmentid
					xsqlexec("select * from package where shipmentid="+TRANSFORM(nShipmentId),,,"wh")

					IF RECCOUNT("package")>0
						xjfilter=xjfilter+TRANSFORM(shipmentid)+","
					ELSE
						xjfilter="1=0"
					ENDIF
				ENDSCAN
				xjfilter=LEFT(xjfilter,LEN(xjfilter)-1)+")"

				xsqlexec("select * from package where "+xjfilter,,,"wh")
			ELSE
				xsqlexec("select * from package where .f.",,,"wh")
			ENDIF

		CASE wz = 3
			&& BBC
			csq = [select * from shipment where accountid in (6757,6759,6763) and shipdate >= {]+DTOC(DATE()-3)+[} and sent = 1]  && BBC Accounts
			xsqlexec(csq,,,"wh")

			IF RECCOUNT("shipment") > 0
				xjfilter="shipmentid in ("
				SCAN
					nShipmentId = shipment.shipmentid
					xsqlexec("select * from package where shipmentid="+TRANSFORM(nShipmentId),,,"wh")

					IF RECCOUNT("package")>0
						xjfilter=xjfilter+TRANSFORM(shipmentid)+","
					ELSE
						xjfilter="1=0"
					ENDIF
				ENDSCAN
				xjfilter=LEFT(xjfilter,LEN(xjfilter)-1)+")"

				xsqlexec("select * from package where "+xjfilter,,,"wh")
			ELSE
				xsqlexec("select * from package where .f.",,,"wh")
			ENDIF

	ENDCASE

	SELECT package
	INDEX ON shipmentid TAG shipmentid

	SELECT shipmentid,INT(VAL(RIGHT(TRANSFORM(accountid),4))) AS accountid, ;
		pickticket AS ship_ref,SPACE(22) AS bol_no ;
		FROM shipment ;
		WHERE updatedt < DATETIME()-3600 ;
		INTO CURSOR tempship READWRITE
	LOCATE

	IF lTesting
		BROWSE
	ENDIF

	SCAN
		IF SEEK(tempship.shipmentid,"package","shipmentid")
			m.bol = ALLTRIM(package.trknumber)
			IF EMPTY(ALLTRIM(m.bol))
				WAIT WINDOW "No tracking number found..." NOWAIT && TIMEOUT 2
				DELETE NEXT 1 IN tempship
				LOOP
			ELSE
				WAIT WINDOW "Track#/BOL: "+m.bol NOWAIT && TIMEOUT 2
				REPLACE tempship.bol_no WITH m.bol NEXT 1
			ENDIF
		ENDIF
	ENDSCAN

	LOCATE
	IF lTesting
		BROWSE
	ENDIF

	SCAN
		cAcctPT = TRIM(ALLTRIM(STR(tempship.accountid))+tempship.ship_ref)
		IF SEEK(cAcctPT,"edi_trigger","acct_pt")
			WAIT WINDOW "Deleting PT# "+tempship.ship_ref+", already in triggers" NOWAIT
			DELETE NEXT 1 IN tempship
		ENDIF
	ENDSCAN

	LOCATE
	IF EOF()
		WAIT WINDOW "No trigger records were added" TIMEOUT 2
		LOOP
	ENDIF


	LOCATE
	IF lTesting
		WAIT CLEAR
		WAIT WINDOW "CANCEL after browsing" TIMEOUT 1
		BROWSE
	ENDIF

	nCnt1 = 0
	WAIT CLEAR
	WAIT WINDOW "Now adding BOL/Del.Date to outship..." NOWAIT NOCLEAR
	SCAN
		cAcctPT = STR(tempship.accountid,4)+tempship.ship_ref
		WAIT WINDOW "Processing Acct. "+TRANSFORM(tempship.accountid)+", PT# "+ALLTRIM(tempship.ship_ref) NOWAIT
		IF SEEK(cAcctPT,"outship","acctpt")

* lets trigger USPS early for Synclaire, removed by PG 12/16/2017
*!*  			IF INLIST(outship.scac,"PRRM","SPAR","SPRI","SPAD","SFIR") OR LEFT(ALLTRIM(outship.ship_via),4) = "USPS"&& ALL USPS needs to close with EOD parcels
*!*  				LOOP
*!*  			ENDIF
			IF EMPTY(outship.del_date) OR EMPTY(outship.bol_no)
				IF LEN(ALLTRIM(tempship.bol_no))>20
					REPLACE outship.shipins WITH outship.shipins+CHR(13)+"LONGTRKNUM*"+ALLTRIM(tempship.bol_no) IN outship NEXT 1
				ENDIF
				REPLACE outship.del_date WITH DATE() IN outship NEXT 1
				REPLACE outship.bol_no WITH tempship.bol_no IN outship NEXT 1
				nCnt1 = nCnt1+1
			ENDIF
		ENDIF
	ENDSCAN

	WAIT CLEAR
	WAIT WINDOW TRANSFORM(nCnt1)+" new parcel records were marked delivered for "+cWhse TIMEOUT 2
ENDFOR
CLOSE DATA ALL
SET HOURS TO 12
replaceltn()
RETURN
