Parameters tfile


Set Date mdy

Create Cursor csrbbbin (;
trailer    Char(12),;
closedt    date,;
arrivedt   date,;
carrier    Char(4),;
bol        Char(30),;
pro        Char(30),;
ucc        Char(28),;
weight     Char(12),;
cube       Char(12),;
class      Char(7),;
origshipid Char(11),;
shiptype   Char(1),;
shipid     Char(8),;
shipname   Char(30),;
addr1      Char(30),;
addr2      Char(30),;
addr3      Char(30),;
city       Char(20),;
state      Char(2),;
zip        Char(10),;
filename   char(50),;
isa        Char(12),;
isadt      date,;
whse       char(6),;
invnum     Char(12),;
invdt      date)


Create Cursor csrbbbtn (;
pcsman     Char(6),;
closedt    date,;
bbbman     Char(30),;
ucc        Char(28),;
weight     Char(12),;
cube       Char(12),;
class      Char(7),;
origshipid Char(11),;
shiptype   Char(1),;
shipid     Char(8),;
shipname   Char(30),;
addr1      Char(30),;
addr2      Char(30),;
addr3      Char(30),;
city       Char(20),;
state      Char(2),;
zip        Char(10),;
filename   char(50),;
isa        Char(12),;
isadt      date,;
whse       char(6),;
invnum     Char(12),;
invdt      date)


Cd "f:\ftpusers\bedbath\datain\"

*tfile = Getfile()
fhand = Fopen(tfile)

If !fhand >0
  Wait Window At 10,10 "Low level file cannot open " timeout 2
  Return .f.
endif
ictr = 1

If "TN"$Upper(tfile)
  Do While !Feof(fhand)
    lcS = Fgets(fhand,500)
    Select csrbbbtn
    Scatter Memvar Blank
    m.pcsman   =  Substr(lcS,1,6)
    lnDTStart = 7
    m.closedt    =Ctod(Substr(lcS,lnDTStart+4,2)+"/"+Substr(lcS,lnDTStart+6,2)+"/"+Substr(lcS,lnDTStart,4))
    m.bbbman   =  Substr(lcS,15,30)
    m.ucc      =  Substr(lcS,45,28)
    m.weight   =  Substr(lcS,73,12)
    m.cube     =  Substr(lcS,85,12)
    m.class    =  Substr(lcS,97,7)
    m.origshipid =Substr(lcS,114,11)
    m.shiptype =  Substr(lcS,115,1)
    m.shipid   =  Substr(lcS,116,7)
    m.shipname =  Substr(lcS,124,30)
    m.addr1    =  Substr(lcS,154,30)
    m.addr2    =  Substr(lcS,184,30)
    m.addr3    =  Substr(lcS,214,30)
    m.city     =  Substr(lcS,244,20)
    m.state    =  Substr(lcS,264,2)
    m.zip      =  Substr(lcS,266,10)
    m.filename = Justfname(tfile)
    Insert Into csrbbbtn From Memvar
  Enddo
  Fclose(fhand)
  Select csrbbbtn
*!*    Scan
*!*      Scatter memvar
*!*      Insert Into bbbtn From memvar
*!*      replace uploaddt With date() In bbbtn
*!*      replace uploadtm With datetime() In bbbtn
*!*    endscan
  Return .t.
Endif

If "IN"$Upper(tfile)
  Do While !Feof(fhand)
    lcS = Fgets(fhand,500)
    Select csrbbbin
    Scatter Memvar Blank
    m.trailer    =Substr(lcS,1,12)
    lnDTStart = 13
    m.closedt    =Ctod(Substr(lcS,lnDTStart+4,2)+"/"+Substr(lcS,lnDTStart+6,2)+"/"+Substr(lcS,lnDTStart,4))
    lnDTStart = 21
    m.arrivedt   =Ctod(Substr(lcS,lnDTStart+4,2)+"/"+Substr(lcS,lnDTStart+6,2)+"/"+Substr(lcS,lnDTStart,4))
    m.carrier    =Substr(lcS,29,4)
    m.bol        =Substr(lcS,33,30)
    m.pro        =Substr(lcS,63,30)
    m.ucc        =Substr(lcS,93,28)
    m.weight     =Substr(lcS,121,12)
    m.cube       =Substr(lcS,132,12)
    m.class      =Substr(lcS,145,7)
    m.origshipid =Substr(lcS,152,11)
    m.shiptype   =Substr(lcS,163,1)
    m.shipid     =Substr(lcS,164,8)
    m.shipname   =Substr(lcS,172,30)
    m.addr1      =Substr(lcS,202,30)
    m.addr2      =Substr(lcS,232,30)
    m.addr3      =Substr(lcS,262,306)
    m.city       =Substr(lcS,292,20)
    m.state      =Substr(lcS,312,2)
    m.zip        =Substr(lcS,314,10)
    m.filename = Justfname(tfile)
    Insert Into CSRbbbin From Memvar
  Enddo
  Fclose(fhand)

  Select csrbbbIN
*!*    Scan
*!*      Scatter memvar
*!*      Insert Into bbbin From memvar
*!*      replace uploaddt With date() In bbbin
*!*      replace uploadtm With datetime() In bbbin
*!*    endscan

  Return .t.
Endif
