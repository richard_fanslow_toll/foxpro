* Create a plethora of Labor/time reports for Temp associates for the temp agencies that are in Kronos.
* author: Mark Bennett
* Modified 12/2011 to also report on Hourly San Pedro employees for Jaime Barba.
* EXEs go in F:\UTIL\KRONOS\
* modified 4/2014 to add 2nd tab which is an invoice summary and sign off sheet for self-invoicing.
* Modified 8/10/15 to use the 6th Kronos labor level field (formerly SHIFT) as a Client identifier. Because ML is trying to break out non-Sears clients.
* Modified 9/16/15 to support the BONUS paycode.
* Modified 9/28/15 to add support for Staffing Solutions Group - Carson
* Modified 6/16/2016 to add support for Staffing Solutions Group - Carson 2
* Modified 6/23/2016 to use a TempAgency class instead of several of its own functions.
* modified 06/26/2017 to add Allied Security - Louisville as Temp Agency W
* modified 7/26/2017 to add Chartwell SP as temp agency X
* modified 7/26/2017 to add Simplfied ML as temp agency Y
* modified 12/2017 to add Chartwell NJ as temp agency G
* modified 1/3/2018 to change agencies U & V from SSG to Simplified Labor SS
* modified 1/30/2018 to add agency F for Chartwell KY - transitioning from Cor-Tech KY.
* modified 1/31/2018 to add Giovanni Andriani and remove Terry Ironside from the CA temp time report emails.
* modified 2/1/2018 to add Chris Malcolm to the Chartwell NJ Invoice report email.
* modified 2/2/2018 to add Marty Cuniff to the Chartwell NJ and Quality Invoice report emails.
* modified 2/12/2018 to remove Maria Estrella and Giovanni Andriani from all temp time reports, at their respective requests.
* modified 2/28/2018 to add Melissa Gomez and Bryon Summersville to the Mira Loma reports.
* modified 3/14/2018 to exclude GAP/Payless data from the Div 57 BBB shift summary report which goes daily to Steve Sykes.
* modified 3/13/201 MB for Simplified Staffing Carson 1 & 2 email changes.
* 3/23/2018 MB: began modifying labor by div/dept summary report as specified by Adelene Teo.
* 4/2/2018 MB: removed william schiele from emails.
* 4/23/201 MB: added Carlos Carballeda to the ML emails.
* 4/24/201 MB: changed mbennett@fmiint.com references to my toll email.

LPARAMETERS tcTempCo, tcMode

IF TYPE('tcTempCo') = "L" THEN
	tcTempCo = "?"
ENDIF
IF TYPE('tcMode') = "L" THEN
	tcMode = "?"
ENDIF

ON ERROR DO errHandlerKTTR WITH ERROR( ), MESSAGE( ), MESSAGE(1), PROGRAM( ), LINENO( ), tcTempCo, tcMode

runack("KRONOSTEMPTIMEREPORT")

LOCAL lnError, lcProcessName

DECLARE INTEGER CreateMutex IN Win32API ;
	STRING @lpMutexAttributes, ;
	STRING bInitialOwner, ;
	STRING @lpName

DECLARE INTEGER GetLastError IN Win32API

* if running in EXE check to see if already running
IF _VFP.STARTMODE > 0 THEN
	lcProcessName = "KRONOSTEMPTIMEREPORT"
	CreateMutex(0,CHR(1),@lcProcessName)
	lnError = GetLastError()
	IF lnError = 183
		WAIT WINDOW TIMEOUT 10 "KRONOS TIME REPORT process is already running..."
		RETURN .F.
	ENDIF
ENDIF

SET PROCEDURE TO M:\DEV\PRG\KRONOSCLASSLIBRARY ADDITIVE


_SCREEN.CAPTION = "KRONOSTEMPTIMEREPORT"

utilsetup("KRONOSTEMPTIMEREPORT")

LOCAL loKronosTempTimeReport, lcTempCo, lcMode

loKronosTempTimeReport = CREATEOBJECT('KronosTempTimeReport')
*!*	IF TYPE('tcTempCo') = "L" THEN
*!*		tcTempCo = "?"
*!*	ENDIF
*!*	IF TYPE('tcMode') = "L" THEN
*!*		tcMode = "?"
*!*	ENDIF
lcTempCo = UPPER(ALLTRIM(tcTempCo))
lcMode = UPPER(ALLTRIM(tcMode))

loKronosTempTimeReport.MAIN( lcTempCo, lcMode )

*ON ERROR DO errHandlerKTTR WITH ERROR( ), MESSAGE( ), MESSAGE(1), PROGRAM( ), LINENO( ), tcTempCo, tcMode

schedupdate()

CLOSE DATABASES ALL

WAIT WINDOW TIMEOUT 30  && TO FORCE THIS PROCESS TO RUN FOR > 1 MINUTE, I THINK IF IT RUNS WITHIN 1 MINUTE STM RUNS IT TWICE

RETURN

#DEFINE LOGIT 1
#DEFINE WAITIT 2
#DEFINE NOWAITIT 4
#DEFINE SENDIT 8
#DEFINE NOCRLF 16
#DEFINE CRLF CHR(13) + CHR(10)
#DEFINE LF CHR(10)
#DEFINE RETURN_DATA_MANDATORY .F.
#DEFINE RETURN_DATA_NOT_MANDATORY .T.
#DEFINE DAILY_OT_MULTIPLIER 1.5
#DEFINE DAILY_OT20_MULTIPLIER 2.0
#DEFINE xlEdgeBottom 9
#DEFINE xlContinuous 1
#DEFINE xlMedium -4138
#DEFINE TAB CHR(9) 

DEFINE CLASS KronosTempTimeReport AS CUSTOM

	cProcessName = 'KronosTempTimeReport' 

	* which payroll server, and associated connections, to hit 
	lUsePAYROLL2Server = .T.

	lAutoYield = .T.	

	cExtraWhere = ""
	cExtraWhere2 = ""
	cExtraWhere3 = ""
	cExtraWhere4 = ""
	cExcludedList = ""
	
	******************************** 
	********************************
	
	lForceAllHoursToOverTime = .F.  && .T. to force OT for weekends/holidays worked 
	
	lForceAllHoursToDoubleTime = .F.  && .T. to force DT for weekends/holidays worked 
	
	lTestMode = .F.  && .F. FOR NORMAL OPERATION, .T. TO SUPPRESS ALL BUT DEVELOPER EMAILS, ETC.  

	* date properties
	dtNow = DATETIME()   

	dToday = DATE()
	*dToday = {^2018-04-09}
		
	* ===> for date-range filtering (search for "DATERANGE-INVOICE")  
	dSetStartDate = {}
	dSetEndDate = {}	

	*dSetStartDate = {^2018-03-01}      
	*dSetEndDate = {^2018-03-18}

	* report on some, not all, associates
*!*	cExtraWhere  = " AND (C.PERSONNUM = '507987') " 

*!*	cExtraWhere  = " AND (C.PERSONNUM in ('506457','507870')) "

*!*	cExtraWhere  = " AND (C.PERSONNUM in ('311137','311150','311047')) "

*!*	cExtraWhere  = " AND (C.PERSONNUM in ('507159','507207','507256','507266')) "

*!*	cExtraWhere  = " AND (C.PERSONNUM in ('506713','504833','506784','506782','506185')) "

*!*	cExtraWhere  = " AND (C.PERSONNUM in ('507262','507239','507263','507258','507259','507261')) "

*!*	cExtraWhere  = " AND (C.PERSONNUM in ('311413','311412','311414','311417','311418','311419','311420')) "

*!*	cExtraWhere2 = " AND (PERSONNUM in ('27306','20165','22088','23098','27135','27341','26032')) "

*!*	cExtraWhere  = " AND (C.PERSONNUM in ('270259','240101','230016','270255','270245','223876','223932','223453','223150')) "

*!*	* special invoice for SSG for associates that worked at ML from 9/21 to 9/23 - see Jennifer Cano for details on that.
*!*	cExtraWhere  = " AND ( (C.PERSONNUM >= '100') AND (C.PERSONNUM <= '129') ) " 
	
*****************************************************************************

* EXCLUDE some associates from the report - maybe because they were already paid....
	
* ===>
* ===> NOTE: if reporting terminations in current Un-invoiced week - add them to Excluded table with Enabled = .f., change to .t. after the report
* ===>
	
	
*!*		cExtraWhere  = " AND (NOT (C.PERSONNUM = '504839')) " 

*!*		cExtraWhere  = " AND (NOT (C.PERSONNUM in ('504781','504682','503309'))) " 
	
*****************************************************************************
	
	* Temp Agency object properties
	oTempAgency = NULL

	cStartTime = TTOC(DATETIME())

	* mode properties
	cMode = ""

	* connection properties
	nSQLHandle = 0

	* file properties
	nFileHandle = 0
	nCSVHandle = 0
	cWeeklyTemplate = ''

	lNewDivDeptReport = .F. && .T. to change the std div/dept summary report to the customized office-specific reports from Adelene Teo.
	 
	* table properties
	*cFoxProTimeReviewerTable = "F:\UTIL\KRONOS\DATA\TIMERVWR"

	* sql/report filter properties
	cTempCoWhere = ""
	cTempCoWhere2 = ""
	cTempCo = ""
	cTempCoName = ""
	lShowDollars = .T.

	* hours discrepancy testing props
	nMinHoursTest = 2
	nMaxHoursTest = 10

	* WAGE discrepancy testing props
	nMinWageTest = 8.00
	nMaxWageTest = 40.00

	dEndDate = {}
	dStartDate = {}

	cToday = DTOC(DATE())

	* wait window properties
	nWaitWindowTimeout = 2
	lWaitWindowIsOn = .T.

	* logfile properties
	lLoggingIsOn = .T.
	cLogFile = 'F:\UTIL\KRONOS\Logfiles\KronosTempTimeReport_log.txt'

	* processing properties
	lCheckBio = .T.
	lHourlyNotTemps = .F.
	nTotSummaryPay = 0.00
	nTotalsRow = 0
	nGrandTotalSheetDollar = 0.00
	nHeadCount = 0

	nPeriodEndDayNum = 1   && if 7, weekly pay period ends on a Saturday (Sunday - Saturday), if 1, it ends on a Sunday (Monday - Sunday)

	*lRoundWageAtEachStep = .T.  && .F. would only apply rounding after all wage*markups have been accumulated; If .T. it rounds to nearest penny at each step before accumulating (which seems to be the way TriState is doing it).
	nRoundingType = 1
	* 1 = standard rounding - round effective wage to nearest penny after multiplying the agency markup times base wage; also round daily $ to nearest penny
	* 2 = no rounding
	* 3 = SELECT ROUNDING = round product of base wage and agency markup and ot multiplier to nearest cent

	* INTERNAL email properties
	lSendInternalEmailIsOn = .T.
	*cFrom = 'FMI Corporate <fmicorporate@fmiint.com>'
	cFrom = 'mark.bennett@tollgroup.com'
	*cSendTo = 'Mark Bennett <mark.bennett@tollgroup.com>'
	cSendTo = 'mark.bennett@tollgroup.com'
	cCC = ''
	cSubject = 'Time Report'
	cAttach = ''
	cBodyText = ''

	cCOMPUTERNAME = ''
	cUSERNAME = ''

	FUNCTION INIT
		IF NOT DODEFAULT()
			RETURN .F.
		ENDIF
		WITH THIS
			CLEAR
			*SET RESOURCE OFF
			CLOSE DATA
			SET CENTURY ON
			SET DATE YMD
			SET HOURS TO 24
			SET ANSI ON
			SET TALK OFF
			SET DELETED ON
			SET CONSOLE OFF
			SET EXCLUSIVE OFF
			SET SAFETY OFF
			SET EXACT OFF
			SET STATUS BAR ON
			SET SYSMENU OFF
			SET ENGINEBEHAVIOR 70
			SET DECIMALS TO 4

			SET FIXED ON

			_VFP.AUTOYIELD = .lAutoYield
			.cCOMPUTERNAME = GETENV("COMPUTERNAME")
			.cUSERNAME = GETENV("USERNAME")
		ENDWITH
	ENDFUNC


	FUNCTION DESTROY
		WITH THIS
			IF .lLoggingIsOn  THEN
				SET ALTERNATE OFF
				SET ALTERNATE TO
			ENDIF
		ENDWITH
		DODEFAULT()
	ENDFUNC


	FUNCTION MAIN
		LPARAMETERS tcTempCo, tcMode
		WITH THIS
			LOCAL lnNumberOfErrors, lcSQL, lcSQL2, lcSQL3, lcSQL4, lcSQL5, lcSQL6, lcSQL7, lcSQL8, loError, lcTopBodyText, llValid
			LOCAL lcXLFileName, ldStartDate, ldEndDate, lcStartDate, lcEndDate, ldDate
			LOCAL lcSQLStartDate, lcSQLEndDate, lcXLFileName, ldYesterday
			LOCAL oExcel, oWorkbook, oWorksheet, lnRow, lcRow, lnStartRow, lcStartRow, lnEndRow, lnPP, lcPP, lnNN, lcNN, lcJJ
			LOCAL lcColsToProcess, lnColumn, lcBoldColumn, i, j, lcBoldRow, luCellValue, lcTargetDirectory, ldPayDate, lcFiletoSaveAs
			LOCAL lcFileDate, lcDivision, lcTitle, llSaveAgain, lcPayCode, lcSpecialWhere
			LOCAL lcSQLToday, lcSQLTomorrow, ldToday, ldTomorrow, lcExcluded9999s, lcFoxProTimeReviewerTable, lcMissingApprovals
			LOCAL lcAuditSubject, oWorkbookAUDIT, oWorksheetAUDIT, lcSendTo, lcCC, lcBodyText, lcXLFileNameAUDIT, lcTotalRow
			LOCAL lcTRWhere, lnStraightHours, lnStraightDollars, lnOTDollars, lcFilePrefix
			LOCAL ldPass2Date, lcPrevFileNum, lnPrevFileNumStartRow, lnPrevFileNumEndRow, lcPrevFileNumStartRow, lcPrevFileNumEndRow
			LOCAL lcOrderBy, lcPrevDept, lnPrevDeptStartRow, lnPrevDeptEndRow, lcPrevDeptStartRow, lcPrevDeptEndRow, lnTempMarkupREG
			LOCAL lcPrevDivision, lnPrevDivisionStartRow, lcPrevDivisionStartRow, lnPrevDivisionEndRow, lcPrevDivisionEndRow
			LOCAL lnShiftDiff, lcTopBodyText, lcXLSummaryFileName, lnWage, lnPay, lcRootPDFFilename, lcChar
			LOCAL ltInpunch, lnTotHours, lcPRIMACCTCHANGED, lcNoBioFileNum, lcUnscannable, lcInvalidRegHours, lnTempMarkupOT
			LOCAL lnDiff, lnDiffRow, lcDiffRow, lnLastRow, lcLastRow, lcTotalsRow, lnMinWageTest, lnMaxWageTest, lcBADWAGES, lcDeptname
			LOCAL lcXLSummaryQuality, lcCorTechRecruitsFile, lcYTDSummaryFile, ldYTDSummaryEndDate, ldYTDSummaryStartDate, lcYTDSummaryEndDate, lcYTDSummaryStartDate
			LOCAL lcSSAll, lcSSNum, lcFileNum, lcXLSummarySSG, lcCSVSummarySSG, lcNewDivision, lcDivisionName, lcClientName, lnTotAllHours
			LOCAL lcSendToR, lcCCR, lcAttachR, lcBodyTextR, lcType, lcXLSummarySSGKiandra, lcXLSUMMARYDIV57, lcXLSummaryBBB, lcBBBSubject
			LOCAL lcCMPRecruitsFile, lcExact


			ON ERROR

			TRY
				lnNumberOfErrors = 0
				lcTopBodyText = ''

				*.TrackProgress('==================================================================================================================', LOGIT+SENDIT)
				.TrackProgress('KRONOS Time process started....', LOGIT+SENDIT)
				.TrackProgress('COMPUTERNAME = ' + .cCOMPUTERNAME, LOGIT+SENDIT)
				.TrackProgress('USERNAME = ' + .cUSERNAME, LOGIT+SENDIT)
				.TrackProgress('PROJECT = KRONOSTEMPTIMEREPORT', LOGIT+SENDIT)
				.TrackProgress('Temp Co = ' + tcTempCo, LOGIT+SENDIT)

				.TrackProgress('Mode = ' + tcMode, LOGIT+SENDIT)
				IF .lTestMode THEN
					.TrackProgress('                        TEST MODE                        ', LOGIT+SENDIT)
				ENDIF
		
				* set mode from parameter
				SET EXACT ON
				IF INLIST(tcMode,"DAILY","ZDAILY","WEEKLY","ZWEEKLY","WEEKLY-AGENCIES","INVOICE","SUMMARYONLY","DATERANGE","DIV56","DIV59WEEKLY","DIV59DAILY","DATERANGE-INVOICE") ;
						OR INLIST(tcMode,"K+0630","K-0630","DIV58DAILY","DIV52DAILY","DIV57MONTHLY") THEN 
					* valid mode
					.cMode = tcMode
				ELSE
					* invalid mode
					.TrackProgress('Missing or Invalid Mode Parameter!', LOGIT+SENDIT)
					THROW
				ENDIF
				SET EXACT OFF
				
				*cMode = "DAILY"
				*cMode = "WEEKLY"
				*cMode = "WEEKLY-AGENCIES"
				*cMode = "INVOICE"
				*cMode = "SUMMARYONLY"
				*cMode = "DATERANGE"
				*cMode = "DIV56"

*!*					* set temp agency from parameter
*!*					.SetTempCo( tcTempCo )
				
				.oTempAgency = CREATEOBJECT('TempAgency',tcTempCo)				
				lcType = TYPE('.oTempAgency')
				
				IF lcType == 'O' THEN
					.cTempCo = tcTempCo
					.cTempCoName = .oTempAgency.GetTempCoName()
					.lCheckBio = .oTempAgency.lCheckBio
					.nMinHoursTest = .oTempAgency.nMinHoursTest
					.nMaxHoursTest = .oTempAgency.nMaxHoursTest
					.nPeriodEndDayNum = .oTempAgency.nPeriodEndDayNum
					.cWeeklyTemplate = .oTempAgency.cWeeklyTemplate
					IF NOT EMPTY(.cWeeklyTemplate) THEN
						.TrackProgress('Weekly Template = ' + .cWeeklyTemplate, LOGIT+SENDIT)
						.cWeeklyTemplate = 'F:\UTIL\Kronos\TEMPLATES\' + .cWeeklyTemplate
					ENDIF
				ELSE
					.TrackProgress('====> ERROR instantiating Temp Agency class!', LOGIT+SENDIT)
					THROW
				ENDIF


				.cExtraWhere2 = STRTRAN(.cExtraWhere,'C.','')  && 4/6/2018 MB
				
*!*					* make logfiles company-specific so that report run times can overlap 
*!*					IF .lTestMode THEN
*!*						.cLogFile = 'F:\UTIL\KRONOS\Logfiles\KronosTempTimeReport_' + .cTempCoName + "_" + .cMode + "_" + .cCOMPUTERNAME + '_log_TESTMODE.txt'
*!*					ELSE
*!*						.cLogFile = 'F:\UTIL\KRONOS\Logfiles\KronosTempTimeReport_' + + .cTempCoName + "_" + .cMode + "_" + .cCOMPUTERNAME + '_log.txt'
*!*					ENDIF
*!*					.lLoggingIsOn = .lLoggingIsOn AND NOT EMPTY(.cLogFile)
*!*					IF .lLoggingIsOn THEN
*!*						SET ALTERNATE TO (.cLogFile) ADDITIVE
*!*						SET ALTERNATE ON
*!*					ENDIF

*!*					IF EMPTY(.cTempCoName) THEN
*!*						.TrackProgress('Missing or Invalid Temp CO Parameter!', LOGIT+SENDIT)
*!*						THROW
*!*					ENDIF

				*IF .lTestMode THEN
*!*					lcChar = UPPER(LEFT(.cTempCo,1))

*!*					IF INLIST(lcChar,"A","B","D","J","K","S","R","T","U","V") THEN
*!*						* we are doing TriState/CRSCO and Select and LaborReady Temps reports...added Kamran ML and Staffing Solutions Group SP & ml 3/18/2015
*!*						* "L" not included because it is SUN-SAT, even tho it is CRSCO...
*!*						* set last day of pay period to Sunday, effecting Monday-Sunday Pay Period when mode is weekly
*!*						.nPeriodEndDayNum = 1
*!*						.TrackProgress('** Pay Period = Monday thru Sunday **', LOGIT+SENDIT)
*!*					ENDIF
				
				DO CASE
					CASE .nPeriodEndDayNum = 1
						.TrackProgress('** Pay Period = Monday thru Sunday **', LOGIT+SENDIT)
					CASE .nPeriodEndDayNum = 7
						.TrackProgress('** Pay Period = Sunday thru Saturday **', LOGIT+SENDIT)
					OTHERWISE
						.TrackProgress('===> Invalid nPeriodEndDayNum = ' + TRANSFORM(nPeriodEndDayNum), LOGIT+SENDIT)
				ENDCASE
				*ENDIF

				.TrackProgress('.lCheckBio = ' + TRANSFORM(.lCheckBio), LOGIT+SENDIT)
				.TrackProgress('.nMinHoursTest = ' + TRANSFORM(.nMinHoursTest), LOGIT+SENDIT)
				.TrackProgress('.nMaxHoursTest = ' + TRANSFORM(.nMaxHoursTest), LOGIT+SENDIT)
				.TrackProgress('.nPeriodEndDayNum = ' + TRANSFORM(.nPeriodEndDayNum), LOGIT+SENDIT)
				.TrackProgress('.nRoundingType = ' + TRANSFORM(.nRoundingType), LOGIT+SENDIT)

				IF INLIST(tcMode,"ZDAILY","ZWEEKLY") THEN
					* zero out $ on spreadsheet
					.lShowDollars = .F.
				ENDIF

				lcInvalidRegHours = ''
				ldToday = .dToday
				ldTomorrow = ldToday + 1
				ldYesterday = ldToday - 1

				.cToday = DTOC(.dToday)

				oExcel = NULL

				lcExact = SET('EXACT')
				SET EXACT ON
				* setup emails
				IF NOT .lTestMode THEN
					* assign Send To based on TempCo (which indicates temp agency)
					DO CASE
						CASE INLIST(.cTempCo,'A','AJ','A!J')
							.cSendTo = 'mark.bennett@tollgroup.com'
							.cCC = ''
						CASE INLIST(.cTempCo,'D')&& Quality NJ Temp Agencies
							DO CASE
								CASE .cMode == "DAILY"
									.cSendTo = 'Bill.Marsh@tollgroup.com, Jim.Lake@tollgroup.com, Scott.Schreck@tollgroup.com, sueoqualitytemps@outlook.com'
									*.cCC = 'quality25@comcast.net, mark.bennett@tollgroup.com, josephscohen@verizon.net'
									.cCC = 'mark.bennett@tollgroup.com, toll@chartwellstaff.com'
									*.cCC = 'mark.bennett@tollgroup.com'
								CASE INLIST(.cMode,"INVOICE","DATERANGE-INVOICE")
									.cSendTo = 'Marty.Cuniff@tollgroup.com, Bill.Marsh@tollgroup.com, Jim.Lake@tollgroup.com, ryan.brenan@tollgroup.com'
									.cCC = 'mark.bennett@tollgroup.com, tgf.usap@tollgroup.com, Neil.Devine@Tollgroup.com, george.gereis@tollgroup.com, Todd.Wilen@Tollgroup.com, Debbie.Brinker@tollgroup.com, Scott.Schreck@tollgroup.com, sueoqualitytemps@outlook.com'
								CASE .cMode == "SUMMARYONLY"
									.cSendTo = ''
									.cCC = 'mark.bennett@tollgroup.com'
								CASE .cMode == "WEEKLY"
									.cSendTo = 'Bill.Marsh@tollgroup.com, Jim.Lake@tollgroup.com, Scott.Schreck@tollgroup.com, sueoqualitytemps@outlook.com'
									.cCC = 'mark.bennett@tollgroup.com'
									*.cCC = 'mark.bennett@tollgroup.com'
								CASE .cMode == "DATERANGE"
									.cSendTo = 'Bill.Marsh@tollgroup.com, Jim.Lake@tollgroup.com, Scott.Schreck@tollgroup.com, sueoqualitytemps@outlook.com'
									.cCC = 'mark.bennett@tollgroup.com'
								OTHERWISE
									.cSendTo = 'mark.bennett@tollgroup.com'
							ENDCASE
						CASE INLIST(.cTempCo,'F') && F = Chartwell Louisville KY Temps
							DO CASE
								CASE .cMode == "DAILY"
									.cSendTo = 'cheri.foster@tollgroup.com, robin.ragg@tollgroup.com'
									.cCC = 'mark.bennett@tollgroup.com, toll@chartwellstaff.com'
								CASE .cMode == "INVOICE"
									.cSendTo = 'cheri.foster@tollgroup.com, robin.ragg@tollgroup.com'
									.cCC = 'mark.bennett@tollgroup.com, toll@chartwellstaff.com, Todd.Wilen@tollgroup.com, tgf.usap@tollgroup.com, ryan.brenan@tollgroup.com'
								CASE .cMode == "WEEKLY"
									.cSendTo = 'cheri.foster@tollgroup.com, robin.ragg@tollgroup.com'
									.cCC = 'mark.bennett@tollgroup.com, toll@chartwellstaff.com, ryan.brenan@tollgroup.com'
								CASE .cMode == "DATERANGE"
									.cSendTo = 'mark.bennett@tollgroup.com'
									.cCC = ''
								OTHERWISE
									.cSendTo = 'mark.bennett@tollgroup.com'
							ENDCASE
						CASE INLIST(.cTempCo,'G')&& Chartwell NJ
							DO CASE
								CASE INLIST(.cMode,"DAILY","WEEKLY")
									.cSendTo = 'Bill.Marsh@tollgroup.com, Jim.Lake@tollgroup.com, Scott.Schreck@tollgroup.com' 
									.cCC = 'mark.bennett@tollgroup.com, toll@chartwellstaff.com'
								CASE INLIST(.cMode,"INVOICE","DATERANGE-INVOICE")
									.cSendTo = 'Marty.Cuniff@tollgroup.com, Bill.Marsh@tollgroup.com, Jim.Lake@tollgroup.com, toll@chartwellstaff.com, Chris.Malcolm@tollgroup.com'
									.cCC = 'mark.bennett@tollgroup.com, tgf.usap@tollgroup.com, Neil.Devine@Tollgroup.com, george.gereis@tollgroup.com, Todd.Wilen@Tollgroup.com, ryan.brenan@tollgroup.com, Debbie.Brinker@tollgroup.com, Scott.Schreck@tollgroup.com'
								CASE .cMode == "DATERANGE"
									.cSendTo = 'Bill.Marsh@tollgroup.com, Jim.Lake@tollgroup.com, Scott.Schreck@tollgroup.com'
									.cCC = 'mark.bennett@tollgroup.com, toll@chartwellstaff.com'
								OTHERWISE
									.cSendTo = 'mark.bennett@tollgroup.com'
							ENDCASE
						CASE INLIST(.cTempCo,'E')&& Wear Staff NJ Temp Agencies   
							.cSendTo = 'mark.bennett@tollgroup.com'
							.cCC = ''
						CASE INLIST(.cTempCo,'J')&& Diamond Staffing NJ Temp Agency
							.cSendTo = 'mark.bennett@tollgroup.com'
							.cCC = ''
						CASE INLIST(.cTempCo,'L') && L = CRSCO Louisville Temps
							.cSendTo = 'mark.bennett@tollgroup.com'
							.cCC = ''
						CASE INLIST(.cTempCo,'N') && N = LaborReady Louisville Temps - Josh Eckhardt
							.cSendTo = 'mark.bennett@tollgroup.com'
							.cCC = ''
						CASE INLIST(.cTempCo,'O') && O = Express Staffing Louisville Temps - Sarah Davis
							.cSendTo = 'mark.bennett@tollgroup.com'
							.cCC = ''
						CASE INLIST(.cTempCo,'P') && Packaging & More - Louisville
							.cSendTo = 'mark.bennett@tollgroup.com'
							.cCC = ''
						CASE INLIST(.cTempCo,'Q') && Q = COR-TECH Louisville Temps
							DO CASE
								CASE .cMode == "DAILY"
									.cSendTo = 'cheri.foster@tollgroup.com, robin.ragg@tollgroup.com, mtorres@cor-tech.net, ealvarez@cor-tech.net, rlayton@cor-tech.net, amagil@cor-tech.net'
									.cCC = 'mark.bennett@tollgroup.com'
								CASE .cMode == "INVOICE"
									.cSendTo = 'cheri.foster@tollgroup.com, robin.ragg@tollgroup.com, mtorres@cor-tech.net, ealvarez@cor-tech.net, rlayton@cor-tech.net, amagil@cor-tech.net'
									.cCC = 'mark.bennett@tollgroup.com, Todd.Wilen@tollgroup.com, tgf.usap@tollgroup.com, ryan.brenan@tollgroup.com'
								CASE .cMode == "SUMMARYONLY"
									.cSendTo = 'cheri.foster@tollgroup.com, mtorres@cor-tech.net, ealvarez@cor-tech.net, rlayton@cor-tech.net, amagil@cor-tech.net'
									.cCC = 'mark.bennett@tollgroup.com'
								CASE .cMode == "WEEKLY"
									.cSendTo = 'cheri.foster@tollgroup.com, robin.ragg@tollgroup.com, mtorres@cor-tech.net, ealvarez@cor-tech.net, rlayton@cor-tech.net, amagil@cor-tech.net'
									.cCC = 'mark.bennett@tollgroup.com, ryan.brenan@tollgroup.com'
								CASE .cMode == "WEEKLY-AGENCIES"
									.cSendTo = 'mtorres@cor-tech.net, ealvarez@cor-tech.net, rlayton@cor-tech.net, amagil@cor-tech.net'
									.cCC = 'mark.bennett@tollgroup.com'
								CASE .cMode == "DATERANGE"
									.cSendTo = 'mark.bennett@tollgroup.com'
									.cCC = ''
								OTHERWISE
									.cSendTo = 'mark.bennett@tollgroup.com'
							ENDCASE
						CASE INLIST(.cTempCo,'R')  && Staffing Solutions Group - SP
							.cSendTo = 'mark.bennett@tollgroup.com'
							.cCC = ''
						CASE INLIST(.cTempCo,'S') && S = Kamran Staffing Mira Loma
							.cSendTo = 'mark.bennett@tollgroup.com'
							.cCC = ''
						CASE INLIST(.cTempCo,'T') && T = Staffing Solutions Group - Mira Loma
							.cSendTo = 'mark.bennett@tollgroup.com'
							.cCC = ''
						*!*	CASE INLIST(.cTempCo,'U')  && Staffing Solutions Group - Carson
						*!*		DO CASE
						*!*			CASE .cMode == "ZDAILY"
						*!*				.cSendTo = 'mark.bennett@tollgroup.com'
						*!*				.cCC = ''
						*!*			CASE .cMode == "ZWEEKLY"
						*!*				.cSendTo = 'mark.bennett@tollgroup.com'
						*!*				.cCC = ''

						*!*			CASE .cMode == "WEEKLY-AGENCIES"
						*!*				.cSendTo = 'malvarez@thessg.com, ajimenez@thessg.com, mavidano@coastjobs.com, jpreciado@thessg.com, operea@thessg.com, jcano@thessg.com, rsandoval@thessg.com, achavez@thessg.com'
						*!*				.cCC = 'mark.bennett@tollgroup.com'

						*!*			CASE INLIST(.cMode,"DAILY","WEEKLY")
						*!*				.cSendTo = 'steven.balbas@tollgroup.com, roy.soriano@tollgroup.com, will.keller@tollgroup.com, ' + ;
						*!*					'malvarez@thessg.com, jpreciado@thessg.com, operea@thessg.com, jcano@thessg.com, rsandoval@thessg.com, achavez@thessg.com'
						*!*				.cCC = 'ajimenez@thessg.com, mavidano@coastjobs.com, mark.bennett@tollgroup.com, maria.rodriguez@Tollgroup.com'
						*!*			CASE INLIST(.cMode,"SUMMARYONLY")
						*!*				.cSendTo = 'steven.balbas@tollgroup.com, roy.soriano@tollgroup.com, will.keller@tollgroup.com, ' + ;
						*!*					'malvarez@thessg.com, jpreciado@thessg.com, operea@thessg.com, jcano@thessg.com'
						*!*				.cCC = 'ajimenez@thessg.com, mavidano@coastjobs.com, mark.bennett@tollgroup.com'
						*!*			CASE INLIST(.cMode,"INVOICE")
						*!*				.cSendTo = 'steven.balbas@tollgroup.com, roy.soriano@tollgroup.com, will.keller@tollgroup.com, ' + ;
						*!*					'malvarez@thessg.com, tgf.usap@tollgroup.com, jpreciado@thessg.com, operea@thessg.com, rsandoval@thessg.com, achavez@thessg.com'
						*!*				.cCC = 'ajimenez@thessg.com, mavidano@coastjobs.com, mark.bennett@tollgroup.com, maria.rodriguez@Tollgroup.com, jcano@thessg.com, george.gereis@tollgroup.com, Todd.Wilen@Tollgroup.com, ryan.brenan@tollgroup.com'
						*!*			OTHERWISE
						*!*				.cSendTo = 'mark.bennett@tollgroup.com'
						*!*		ENDCASE
						CASE INLIST(.cTempCo,'U')  && Simplified Labor Staffing - Carson  -- changed from SSG 1/1/2018
							DO CASE
								CASE INLIST(.cMode,"DAILY","WEEKLY")
									.cSendTo = 'carina.contreras@yourstaffingfirm.com, juan.santisteban@yourstaffingfirm.com, martha.diaz@yourstaffingfirm.com, will.keller@tollgroup.com, ' + ;
										'Sandra.Senbol@yourstaffingfirm.com, maria.sanchez@yourstaffingfirm.com, Cynthia.Stopani@yourstaffingfirm.com, crystal.hernandez@yourstaffingfirm.com'
									.cCC = 'mark.bennett@tollgroup.com, maria.rodriguez@Tollgroup.com'
								CASE INLIST(.cMode,"INVOICE")
									.cSendTo = 'carina.contreras@yourstaffingfirm.com, juan.santisteban@yourstaffingfirm.com, will.keller@tollgroup.com, tgf.usap@tollgroup.com, ' + ;
										'Sandra.Senbol@yourstaffingfirm.com, maria.sanchez@yourstaffingfirm.com, Cynthia.Stopani@yourstaffingfirm.com, crystal.hernandez@yourstaffingfirm.com'
									.cCC = 'mark.bennett@tollgroup.com, maria.rodriguez@Tollgroup.com, george.gereis@tollgroup.com, Todd.Wilen@Tollgroup.com, ryan.brenan@tollgroup.com'
								OTHERWISE
									.cSendTo = 'mark.bennett@tollgroup.com'
							ENDCASE
						*!*	CASE INLIST(.cTempCo,'V')  && Staffing Solutions Group - Carson 2
						*!*		DO CASE
						*!*			CASE .cMode == "ZDAILY"
						*!*				.cSendTo = 'mark.bennett@tollgroup.com'
						*!*				.cCC = ''
						*!*			CASE .cMode == "ZWEEKLY"
						*!*				.cSendTo = 'mark.bennett@tollgroup.com'
						*!*				.cCC = ''

						*!*			CASE .cMode == "WEEKLY-AGENCIES"
						*!*				.cSendTo = 'malvarez@thessg.com, ajimenez@thessg.com, mavidano@coastjobs.com, jpreciado@thessg.com, operea@thessg.com, jcano@thessg.com, rsandoval@thessg.com, achavez@thessg.com'
						*!*				.cCC = 'mark.bennett@tollgroup.com'

						*!*			CASE INLIST(.cMode,"DAILY","WEEKLY")
						*!*				.cSendTo = 'steven.balbas@tollgroup.com, roy.soriano@tollgroup.com, ' + ;
						*!*					'malvarez@thessg.com, jpreciado@thessg.com, operea@thessg.com, flerida.donado@tollgroup.com, isidoro.martinez@tollgroup.com, jcano@thessg.com, rsandoval@thessg.com, achavez@thessg.com'
						*!*				.cCC = 'ajimenez@thessg.com, mavidano@coastjobs.com, mark.bennett@tollgroup.com, maria.rodriguez@Tollgroup.com'
						*!*			CASE INLIST(.cMode,"SUMMARYONLY")
						*!*				.cSendTo = 'steven.balbas@tollgroup.com, roy.soriano@tollgroup.com, ' + ;
						*!*					'malvarez@thessg.com, jpreciado@thessg.com, operea@thessg.com, flerida.donado@tollgroup.com, isidoro.martinez@tollgroup.com, jcano@thessg.com'
						*!*				.cCC = 'ajimenez@thessg.com, mavidano@coastjobs.com, mark.bennett@tollgroup.com'
						*!*			CASE INLIST(.cMode,"INVOICE")
						*!*				.cSendTo = 'steven.balbas@tollgroup.com, roy.soriano@tollgroup.com, george.gereis@tollgroup.com, Todd.Wilen@Tollgroup.com, ' + ;
						*!*					'malvarez@thessg.com, tgf.usap@tollgroup.com, jpreciado@thessg.com, operea@thessg.com, flerida.donado@tollgroup.com, rsandoval@thessg.com, achavez@thessg.com'
						*!*				.cCC = 'ajimenez@thessg.com, mavidano@coastjobs.com, mark.bennett@tollgroup.com, maria.rodriguez@Tollgroup.com, isidoro.martinez@tollgroup.com, jcano@thessg.com, ryan.brenan@tollgroup.com'
						*!*			OTHERWISE
						*!*				.cSendTo = 'mark.bennett@tollgroup.com'
						*!*		ENDCASE
						CASE INLIST(.cTempCo,'V')  && && Simplified Labor Staffing - Carson 2  -- changed from SSG 1/1/2018
							DO CASE
								CASE INLIST(.cMode,"DAILY","WEEKLY")
									.cSendTo = 'carina.contreras@yourstaffingfirm.com, juan.santisteban@yourstaffingfirm.com, martha.diaz@yourstaffingfirm.com, maria.estrella@tollgroup.com, flerida.donado@tollgroup.com, isidoro.martinez@tollgroup.com, ' + ;
										'Sandra.Senbol@yourstaffingfirm.com, maria.sanchez@yourstaffingfirm.com, Cynthia.Stopani@yourstaffingfirm.com, crystal.hernandez@yourstaffingfirm.com'
									.cCC = 'mark.bennett@tollgroup.com, maria.rodriguez@Tollgroup.com'
								CASE INLIST(.cMode,"INVOICE")
									.cSendTo = 'carina.contreras@yourstaffingfirm.com, juan.santisteban@yourstaffingfirm.com, maria.estrella@tollgroup.com, tgf.usap@tollgroup.com, ' + ;
										'Sandra.Senbol@yourstaffingfirm.com, maria.sanchez@yourstaffingfirm.com, Cynthia.Stopani@yourstaffingfirm.com, crystal.hernandez@yourstaffingfirm.com'
									.cCC = 'mark.bennett@tollgroup.com, maria.rodriguez@Tollgroup.com, george.gereis@tollgroup.com, Todd.Wilen@Tollgroup.com, ryan.brenan@tollgroup.com'
								OTHERWISE
									.cSendTo = 'mark.bennett@tollgroup.com'
							ENDCASE
						CASE INLIST(.cTempCo,'W') && Allied Security - Louisville
							DO CASE
								CASE .cMode == "DAILY"
									.cSendTo = 'robin.ragg@tollgroup.com'
									.cCC = 'mark.bennett@tollgroup.com'
								CASE .cMode == "INVOICE"
									.cSendTo = 'robin.ragg@tollgroup.com'
									.cCC = 'mark.bennett@tollgroup.com, Todd.Wilen@tollgroup.com, tgf.usap@tollgroup.com, ryan.brenan@tollgroup.com'
								CASE .cMode == "SUMMARYONLY"
									.cSendTo = 'robin.ragg@tollgroup.com'
									.cCC = 'mark.bennett@tollgroup.com'
								CASE .cMode == "WEEKLY"
									.cSendTo = 'robin.ragg@tollgroup.com'
									.cCC = 'mark.bennett@tollgroup.com, ryan.brenan@tollgroup.com'
								CASE .cMode == "WEEKLY-AGENCIES"
									.cSendTo = ''
									.cCC = 'mark.bennett@tollgroup.com'
								CASE .cMode == "DATERANGE"
									.cSendTo = 'mark.bennett@tollgroup.com' 
									.cCC = ''
								OTHERWISE
									.cSendTo = 'mark.bennett@tollgroup.com'
							ENDCASE
						CASE INLIST(.cTempCo,'X')  && Chartwell - SP
							DO CASE
								CASE INLIST(.cMode,"INVOICE","DATERANGE-INVOICE")
									.cSendTo = 'louie.ybarra@tollgroup.com, gene.roebuck@tollgroup.com, ' + ;
										'mark.bennett@tollgroup.com, Alma.Navarro@tollgroup.com, Santiago.Castro@tollgroup.com, Guadalupe.Herrejon@tollgroup.com, ' + ;
										'toll@chartwellstaff.com, Neil.Devine@Tollgroup.com, george.gereis@tollgroup.com, malvarez@thessg.com, ryan.brenan@tollgroup.com, jeff.smith@tollgroup.com'
									.cCC = 'tgf.usap@tollgroup.com, maria.rodriguez@Tollgroup.com, george.gereis@tollgroup.com, Todd.Wilen@Tollgroup.com, yesenia.valencia@tollgroup.com'
								CASE INLIST(.cMode,"DAILY","WEEKLY","DATERANGE")
									.cSendTo = 'oscar.valdivia@tollgroup.com, gene.roebuck@tollgroup.com, ' + ;
										'mark.bennett@tollgroup.com, juan.flores@tollgroup.com, louie.ybarra@tollgroup.com, toll@chartwellstaff.com, Guadalupe.Herrejon@tollgroup.com, ' + ;
										'jeff.smith@tollgroup.com'
									.cCC = 'maria.rodriguez@Tollgroup.com, yesenia.valencia@tollgroup.com, Alma.Navarro@tollgroup.com, Santiago.Castro@tollgroup.com'
								CASE .cMode == "DATERANGE"
									.cSendTo = 'mark.bennett@tollgroup.com' 
									.cCC = ''
								CASE .cMode == "DIV52DAILY"
									.cSendTo = 'mark.bennett@tollgroup.com'
									.cCC = 'mark.bennett@tollgroup.com'
								CASE .cMode == "DIV56"
									.cSendTo = ''
									.cCC = 'mark.bennett@tollgroup.com'
								CASE .cMode == "DIV58DAILY"
									.cSendTo = 'mark.bennett@tollgroup.com'
									.cCC = 'mark.bennett@tollgroup.com'
								OTHERWISE
									.cSendTo = 'mark.bennett@tollgroup.com'
							ENDCASE
						CASE INLIST(.cTempCo,'Y') && Y = Simplified Labor Staffing Solutions Inc. - Mira Loma
							DO CASE
								CASE INLIST(.cMode,"DAILY","WEEKLY")
									.cSendTo = 'maria.rodriguez@tollgroup.com, annamarie.decastro@tollgroup.com, ' + ;
										'Sandra.Senbol@yourstaffingfirm.com, Seandy.Reyes@yourstaffingfirm.com, maria.sanchez@yourstaffingfirm.com, Cynthia.Stopani@yourstaffingfirm.com, Wendy.Hernandez@yourstaffingfirm.com, ' +;
										'armando.rivera@tollgroup.com, joana.burke@tollgroup.com, juan.hernandez@tollgroup.com, juan.perez@tollgroup.com, omar.razo@tollgroup.com, ' + ;
										'alma.molina@tollgroup.com, ericka.nicholas@tollgroup.com, gabe.madrid@tollgroup.com, claudia.zavala@tollgroup.com, melissa.gomez@tollgroup.com, bryon.summersville@tollgroup.com'
										.cCC = 'mark.bennett@tollgroup.com, joni.golding@tollgroup.com, carlos.carballeda@tollgroup.com'
								CASE .cMode == "INVOICE"
									.cSendTo = 'tgf.usap@tollgroup.com, Neil.Devine@Tollgroup.com, Debbie.Brinker@tollgroup.com, ryan.brenan@tollgroup.com, ' + ;
										'Sandra.Senbol@yourstaffingfirm.com, Seandy.Reyes@yourstaffingfirm.com, maria.sanchez@yourstaffingfirm.com, Cynthia.Stopani@yourstaffingfirm.com, Wendy.Hernandez@yourstaffingfirm.com, ' +;
										'armando.rivera@tollgroup.com, joana.burke@tollgroup.com, juan.hernandez@tollgroup.com, juan.perez@tollgroup.com, omar.razo@tollgroup.com, ' + ;
										'alma.molina@tollgroup.com, ericka.nicholas@tollgroup.com, gabe.madrid@tollgroup.com, claudia.zavala@tollgroup.com, melissa.gomez@tollgroup.com, bryon.summersville@tollgroup.com'
									.cCC = 'mark.bennett@tollgroup.com, joni.golding@tollgroup.com, george.gereis@tollgroup.com, Todd.Wilen@Tollgroup.com, maria.rodriguez@tollgroup.com, annamarie.decastro@tollgroup.com, ' + ;
										'carlos.carballeda@tollgroup.com'
								CASE .cMode == "DIV57MONTHLY"
									.cSendTo = 'maria.rodriguez@tollgroup.com'
									.cCC = 'mark.bennett@tollgroup.com'
								CASE .cMode == "DATERANGE"
									.cSendTo = 'mark.bennett@tollgroup.com'
									.cCC = ''
								OTHERWISE
									.cSendTo = 'mark.bennett@tollgroup.com'
							ENDCASE
						CASE INLIST(.cTempCo,'Z') && The Job Store - Louisville
							DO CASE
								CASE .cMode == "DAILY"
									.cSendTo = 'robin.ragg@tollgroup.com, lmcdowell@thejobstorellc.com, cdestefano@thejobstorellc.com, thawkins@thejobstorellc.com, cheri.foster@tollgroup.com, kimberly.sallee@tollgroup.com, mrubi@thejobstorellc.com'
									.cCC = 'mark.bennett@tollgroup.com'
								CASE .cMode == "INVOICE"
									.cSendTo = 'robin.ragg@tollgroup.com, cheri.foster@tollgroup.com, kimberly.sallee@tollgroup.com, cdestefano@thejobstorellc.com, lmcdowell@thejobstorellc.com, mrubi@thejobstorellc.com'
									.cCC = 'mark.bennett@tollgroup.com, Todd.Wilen@tollgroup.com, tgf.usap@tollgroup.com, ryan.brenan@tollgroup.com, thawkins@thejobstorellc.com'
								CASE .cMode == "SUMMARYONLY"
									.cSendTo = 'robin.ragg@tollgroup.com'
									.cCC = 'mark.bennett@tollgroup.com'
								CASE .cMode == "WEEKLY"
									.cSendTo = 'robin.ragg@tollgroup.com, lmcdowell@thejobstorellc.com, cdestefano@thejobstorellc.com, thawkins@thejobstorellc.com, cheri.foster@tollgroup.com, kimberly.sallee@tollgroup.com, mrubi@thejobstorellc.com'
									.cCC = 'mark.bennett@tollgroup.com, ryan.brenan@tollgroup.com'
								CASE .cMode == "WEEKLY-AGENCIES"
									.cSendTo = ''
									.cCC = 'mark.bennett@tollgroup.com'
								CASE .cMode == "DATERANGE"
									.cSendTo = 'mark.bennett@tollgroup.com'
									.cCC = ''
								OTHERWISE
									.cSendTo = 'mark.bennett@tollgroup.com'
							ENDCASE
						OTHERWISE
							.cSendTo = 'mark.bennett@tollgroup.com'
							.cCC = ""
					ENDCASE
				ENDIF
				SET EXACT &lcExact

				*.TrackProgress('.cSendTo = ' + .cSendTo, LOGIT+SENDIT)


				* now change this mode since it has served its purpose
				IF .cMode == "WEEKLY-AGENCIES" THEN
					.cMode = "WEEKLY"
				ENDIF

				DO CASE
						*CASE .cMode = "DAILY"  
					CASE INLIST(.cMode, "DIV57MONTHLY")
						* calculate prior month date range
						ldEndDate = .dToday - DAY(.dToday)
						ldStartDate = GOMONTH(ldEndDate + 1, -1)
						
					CASE INLIST(.cMode, "DAILY", "ZDAILY", "DIV52DAILY", "DIV58DAILY", "DIV59DAILY")
						* date range = yesterday
						ldEndDate = .dToday - 1
						ldStartDate = ldEndDate
					CASE INLIST(.cMode, "WEEKLY", "ZWEEKLY", "INVOICE", "SUMMARYONLY", "DIV56", "DIV59WEEKLY")
						* calculate prior week date range
						* We go back from today until we hit a Saturday (DOW = 7) , which is the end day of the previous Sunday-Saturday pay period.
						* then we subtract 6 days from that to get the prior Sunday (DOW = 1), which is the start day of the previous pay period.
						* or
						* we can go back until we hit a Sunday (DOW = 1) for a Monday-Sunday Pay Period for Temps,
						* based on whether .nPeriodEndDayNum = 7 or 1

						ldDate = .dToday
						*!*	DO WHILE DOW(ldDate,1) <> 7 
						*!*		ldDate = ldDate - 1
						*!*	ENDDO
						DO WHILE DOW(ldDate,1) <> .nPeriodEndDayNum
							ldDate = ldDate - 1
						ENDDO
						ldEndDate = ldDate
						ldStartDate = ldDate - 6

******************* to force non standard invoice periods
*!*	ldStartDate = {^2015-09-07}
*!*	ldEndDate = {^2015-09-11}

*!*	ldStartDate = {^2015-09-12}
*!*	ldEndDate = {^2015-09-12}

*!*	ldStartDate = {^2015-09-28}
*!*	ldEndDate = {^2015-09-30}

**********************************************************

						.dEndDate = ldEndDate
						.dStartDate = ldStartDate
					CASE INLIST(.cMode,"DATERANGE","DATERANGE-INVOICE")
						* must manually define start/end dates
						.dStartDate = {^2017-07-01}
						.dEndDate = {^2017-07-31}

						IF (NOT EMPTY(.dSetStartDate)) AND (NOT EMPTY(.dSetEndDate)) THEN
							.dStartDate = .dSetStartDate
							.dEndDate = .dSetEndDate						
						ENDIF

						ldStartDate = .dStartDate
						ldEndDate = .dEndDate
					OTHERWISE
						* nothing
						.TrackProgress('Unexpected .cMode setting!!', LOGIT+SENDIT)
						THROW
				ENDCASE


				*******************************************************************************************************************************
				*******************************************************************************************************************************
				*IF .lTestMode THEN
				
				.cExtraWhere3 = ""
				.cExtraWhere4 = ""
				.cExcludedList = ""
				IF INLIST(.cMode,"WEEKLY","INVOICE")THEN
					* BUILD filter to prevent folks from getting paid who already have been paid. Those folks need to have been entered in 
					* EXCLUDED table.
					
					.cExtraWhere3 = " AND (C.PERSONNUM NOT IN ("
					.cExtraWhere4 = " AND (PERSONNUM NOT IN ("
									
					USE F:\UTIL\KRONOS\DATA\EXCLUDED.DBF IN 0 ALIAS EXCLUDED
					SELECT EXCLUDED
					SCAN FOR (AGENCYCODE = .cTempCo) AND (STARTDATE = .dStartDate) AND (ENDDATE = .dEndDate) AND ENABLED
						.cExcludedList = .cExcludedList + EXCLUDED.ASSOCIATE + " #" + EXCLUDED.PERSONNUM + CRLF
						.cExtraWhere3 = .cExtraWhere3 + "'" + ALLTRIM(EXCLUDED.PERSONNUM) + "',"
						.cExtraWhere4 = .cExtraWhere4 + "'" + ALLTRIM(EXCLUDED.PERSONNUM) + "',"
					ENDSCAN

					*!*		cExtraWhere3 = " AND (C.PERSONNUM NOT IN ('504781','504682','503309')) "
					*!*		cExtraWhere4 = " AND (PERSONNUM NOT IN ('504781','504682','503309')) "
					
					USE IN EXCLUDED
					
					IF EMPTY(.cExcludedList) THEN
						* reset these back to blank
						.cExtraWhere3 = ""
						.cExtraWhere4 = ""
					
					ELSE
						
						.cExtraWhere3 = SUBSTR(.cExtraWhere3,1,(LEN(.cExtraWhere3)-1))  && Strip extra comma at end
						.cExtraWhere3 = .cExtraWhere3 + ")) "
						
						.cExtraWhere4 = SUBSTR(.cExtraWhere4,1,(LEN(.cExtraWhere4)-1))  && Strip extra comma at end
						.cExtraWhere4 = .cExtraWhere4 + ")) "
						
						.cExcludedList = "===> The following have been excluded from this report because they were already paid for this pay period." + CRLF + ;
							"===========================================================================================" + CRLF + ; 
							.cExcludedList

						.TrackProgress('cExtraWhere3 = ' + .cExtraWhere3, LOGIT+SENDIT)
						.TrackProgress('cExtraWhere4 = ' + .cExtraWhere4, LOGIT+SENDIT)
						.TrackProgress(.cExcludedList, LOGIT+SENDIT)
					ENDIF

				ENDIF
					
				*ENDIF && .lTestMode
				*******************************************************************************************************************************88
				*******************************************************************************************************************************88


				lcSpecialWhere = ""


				SET DATE AMERICAN

				lcStartDate = STRTRAN(DTOC(ldStartDate),"/","-")
				lcEndDate = STRTRAN(DTOC(ldEndDate),"/","-")

				SET DATE YMD

				*	 create "YYYYMMDD" safe date format for use in SQL query
				* NOTE: for SQL we are going to use < EndDate ( not <= ), so we are going to add 1 to that date here for querying purposes.
				lcSQLStartDate = STRTRAN(DTOC(ldStartDate),"/","")
				lcSQLEndDate = STRTRAN(DTOC(ldEndDate + 1),"/","")
				lcSQLToday = STRTRAN(DTOC(ldToday),"/","")
				lcSQLTomorrow = STRTRAN(DTOC(ldTomorrow),"/","")

				************************************************************
				* set start dates for YTD summary hours select later on
				ldYTDSummaryStartDate = CTOD(STR(INT(YEAR(.dToday)),4,0) + "-01-01")      && {^2015-01-01}
				ldYTDSummaryEndDate = .dToday
				
				*!*	DO CASE
				*!*		CASE .cTempCo == 'D'  && Quality NJ
				*!*			ldYTDSummaryStartDate = {^2015-01-01}
				*!*			ldYTDSummaryEndDate = .dToday
				*!*		CASE .cTempCo == 'Q'  && Core-Tech KY
				*!*			ldYTDSummaryStartDate = {^2015-01-01}
				*!*			ldYTDSummaryEndDate = .dToday
				*!*		CASE .cTempCo == 'R'  && Staffing Solutions SP
				*!*			ldYTDSummaryStartDate = {^2015-01-01}
				*!*			ldYTDSummaryEndDate = .dToday
				*!*		CASE .cTempCo == 'T'  && Staffing Solutions ML
				*!*			ldYTDSummaryStartDate = {^2015-01-01}
				*!*			ldYTDSummaryEndDate = .dToday
				*!*		OTHERWISE
				*!*			ldYTDSummaryStartDate = {^2015-01-01}
				*!*			ldYTDSummaryEndDate = .dToday
				*!*	ENDCASE


				lcYTDSummaryStartDate = "'" + STRTRAN(DTOC(ldYTDSummaryStartDate),"/","") + "'"
				lcYTDSummaryEndDate = "'" + STRTRAN(DTOC(ldYTDSummaryEndDate),"/","") + "'"
				
				.TrackProgress('lcYTDSummaryStartDate = ' + lcYTDSummaryStartDate, LOGIT+SENDIT)
				.TrackProgress('lcYTDSummaryEndDate = ' + lcYTDSummaryEndDate, LOGIT+SENDIT)

				SET DATE AMERICAN

				* create subject line of email
				DO CASE
					CASE INLIST(.cTempCo,'HJB','HMG')
						* hourly
						DO CASE
							CASE .cMode = "DAILY"
								.cSubject = 'Time for ' + .cTempCoName + ' for: ' + lcStartDate
							CASE .cMode = "WEEKLY"
								.cSubject = 'Time for ' + .cTempCoName + ' for Week: ' + lcStartDate + " to " + lcEndDate
							CASE .cMode = "DATERANGE"
								.cSubject = 'Time for ' + .cTempCoName + ' for Date Range: ' + lcStartDate + " to " + lcEndDate
							CASE .cMode = "SUMMARYONLY"
								.cSubject = 'Summary for ' + .cTempCoName + ' for Week: ' + lcStartDate + " to " + lcEndDate
						ENDCASE
					CASE INLIST(.cTempCo,'K+0630') AND (.cMode = "INVOICE")
						* K - special invoicing
						.cSubject = 'Invoice for ' + .cTempCoName + ' Only Dept 0630 for Week: ' + lcStartDate + " to " + lcEndDate
					CASE INLIST(.cTempCo,'K-0630') AND (.cMode = "INVOICE")
						* K - special invoicing
						.cSubject = 'Invoice for ' + .cTempCoName + ' All but Dept 0630 for Week: ' + lcStartDate + " to " + lcEndDate
					OTHERWISE
						DO CASE
								* temps
								*CASE .cMode = "DAILY"
							CASE INLIST(.cMode, "DAILY", "ZDAILY")
								.cSubject = 'Time for ' + .cTempCoName + ' for: ' + lcStartDate
								*CASE .cMode = "WEEKLY"
							CASE INLIST(.cMode, "WEEKLY", "ZWEEKLY")
								.cSubject = 'Time for ' + .cTempCoName + ' for Week: ' + lcStartDate + " to " + lcEndDate
							CASE INLIST(.cMode,"INVOICE","DATERANGE-INVOICE")
								.cSubject = 'Invoice for ' + .cTempCoName + ' for Week: ' + lcStartDate + " to " + lcEndDate
							CASE .cMode = "DATERANGE"
								.cSubject = 'Time for ' + .cTempCoName + ' for Date Range: ' + lcStartDate + " to " + lcEndDate
							CASE .cMode = "SUMMARYONLY"
								.cSubject = 'Summary for ' + .cTempCoName + ' for Week: ' + lcStartDate + " to " + lcEndDate
							CASE .cMode = "DIV52DAILY"
								.cSubject = 'Div 52 Time for ' + .cTempCoName + ' for: ' + lcStartDate
							CASE .cMode = "DIV56"
								.cSubject = 'Div 56 Time for ' + .cTempCoName + ' for Week: ' + lcStartDate + " to " + lcEndDate
							CASE .cMode = "DIV59WEEKLY"
								.cSubject = 'Div 59 Time for ' + .cTempCoName + ' for Week: ' + lcStartDate + " to " + lcEndDate
							CASE .cMode = "DIV59DAILY"
								.cSubject = 'Div 59 Time for ' + .cTempCoName + ' for: ' + lcStartDate
							CASE .cMode = "DIV58DAILY"
								.cSubject = 'Div 58 Time for ' + .cTempCoName + ' for: ' + lcStartDate
							CASE .cMode = "DIV57MONTHLY"
								.cSubject = 'Div 57 Time for ' + .cTempCoName + ' for Month: ' + lcStartDate + " to " + lcEndDate
						ENDCASE
				ENDCASE

				IF USED('CURWTKHOURS') THEN
					USE IN CURWTKHOURS
				ENDIF
				IF USED('CURWTKHOURSPRE') THEN
					USE IN CURWTKHOURSPRE
				ENDIF
				IF USED('CURDIVISIONS') THEN
					USE IN CURDIVISIONS
				ENDIF
				IF USED('CURCLIENTS') THEN
					USE IN CURCLIENTS
				ENDIF
				IF USED('CURDEPARTMENTS') THEN
					USE IN CURDEPARTMENTS
				ENDIF
				IF USED('CURNOBIOMETRICPRE') THEN
					USE IN CURNOBIOMETRICPRE
				ENDIF
				IF USED('CURNOBIOMETRIC') THEN
					USE IN CURNOBIOMETRIC
				ENDIF
				IF USED('CURTIMESHEET') THEN
					USE IN CURTIMESHEET
				ENDIF
				IF USED('CURTIMESHEET2') THEN
					USE IN CURTIMESHEET2
				ENDIF
				IF USED('CURLASTFIRST') THEN
					USE IN CURLASTFIRST
				ENDIF
				IF USED('CURYTDSUMMARY') THEN
					USE IN CURYTDSUMMARY
				ENDIF

				*** NOTE:
				*** .cTempCo = 'A' means report on all TriState (SP) temps
				*** .cTempCo = 'AJ' means report on TriState (SP) temps - Jacque Moret only; DIV 57
				*** .cTempCo = 'A!J' means report on TriState (SP) temps - NON-Jacque Moret; NON-DIV 57
				*** .cTempCo = 'A2' means report on TriState (SP) temps - 2WIRE only; DIV 54
				*** .cTempCo = 'A!2' means report on TriState (SP) temps - NON-2WIRE; NON-DIV 54
				*** .cTempCo = 'B' means report on all Select (ML) temps
				*** .cTempCo = 'BJ' means report on Select (ML) temps - Jacque Moret only; DIV 51
				*** .cTempCo = 'BS' means report on Select (ML) temps - Sears-SHC only; DIV 50
				*** .cTempCo = 'HJB' means report on SP2 hourly for Jaime Barba; worksite = SP2
				*** .cTempCo = 'HMG' means report on RIALTO hourly for Jaime Barba; DIV = 59
				*** .cTempCo = 'D' means report on all Quality (NJ) temps


				* define filters by passed tempco parameter
				*STORE '' TO .cExtraWhere, .cExtraWhere2
				IF NOT EMPTY(.cTempCo) THEN
					DO CASE
						CASE .cTempCo == 'A'
							.cTempCoWhere =  " AND (D.LABORLEV1NM = 'TMP') AND (D.LABORLEV5NM = '" + .cTempCo + "') "
							.cTempCoWhere2 = " AND (LABORLEVELNAME1 = 'TMP') AND (LABORLEVELNAME5 = '" + .cTempCo + "') "
						CASE .cTempCo == 'AJ'
							.cTempCoWhere =  " AND (D.LABORLEV1NM = 'TMP') AND (D.LABORLEV2NM = '57') AND (D.LABORLEV5NM = 'A') "
							.cTempCoWhere2 = " AND (LABORLEVELNAME1 = 'TMP') AND (LABORLEVELNAME2 = '57') AND (LABORLEVELNAME5 = 'A') "
						CASE .cTempCo == 'A!J'
							.cTempCoWhere =  " AND (D.LABORLEV1NM = 'TMP') AND (D.LABORLEV2NM <> '57') AND (D.LABORLEV5NM = 'A') "
							.cTempCoWhere2 = " AND (LABORLEVELNAME1 = 'TMP') AND (LABORLEVELNAME2 <> '57') AND (LABORLEVELNAME5 = 'A') "
						CASE .cTempCo == 'A2'
							.cTempCoWhere =  " AND (D.LABORLEV1NM = 'TMP') AND (D.LABORLEV2NM = '54') AND (D.LABORLEV5NM = 'A') "
							.cTempCoWhere2 = " AND (LABORLEVELNAME1 = 'TMP') AND (LABORLEVELNAME2 = '54') AND (LABORLEVELNAME5 = 'A') "
						CASE .cTempCo == 'A!2'
							.cTempCoWhere =  " AND (D.LABORLEV1NM = 'TMP') AND (D.LABORLEV2NM <> '54') AND (D.LABORLEV5NM = 'A') "
							.cTempCoWhere2 = " AND (LABORLEVELNAME1 = 'TMP') AND (LABORLEVELNAME2 <> '54') AND (LABORLEVELNAME5 = 'A') "
						CASE .cTempCo == 'B'
							.cTempCoWhere =  " AND (D.LABORLEV1NM = 'TMP') AND (D.LABORLEV5NM = '" + .cTempCo + "') "
							.cTempCoWhere2 = " AND (LABORLEVELNAME1 = 'TMP') AND (LABORLEVELNAME5 = '" + .cTempCo + "') "
						CASE .cTempCo == 'BJ'
							.cTempCoWhere =  " AND (D.LABORLEV1NM = 'TMP') AND (D.LABORLEV2NM = '51') AND (D.LABORLEV5NM = 'B') "
							.cTempCoWhere2 = " AND (LABORLEVELNAME1 = 'TMP') AND (LABORLEVELNAME2 = '51') AND (LABORLEVELNAME5 = 'B') "
						CASE .cTempCo == 'BM'
							.cTempCoWhere =  " AND (D.LABORLEV1NM = 'TMP') AND (D.LABORLEV2NM = '60') AND (D.LABORLEV5NM = 'B') "
							.cTempCoWhere2 = " AND (LABORLEVELNAME1 = 'TMP') AND (LABORLEVELNAME2 = '60') AND (LABORLEVELNAME5 = 'B') "
						CASE .cTempCo == 'BS'
							.cTempCoWhere =  " AND (D.LABORLEV1NM = 'TMP') AND (D.LABORLEV2NM = '50') AND (D.LABORLEV5NM = 'B') "
							.cTempCoWhere2 = " AND (LABORLEVELNAME1 = 'TMP') AND (LABORLEVELNAME2 = '50') AND (LABORLEVELNAME5 = 'B') "
						CASE .cTempCo == 'HJB'
							* report on SP2 hourly for Jaime Barba
							.cTempCoWhere =  " AND (D.LABORLEV1NM = 'SXI') AND (D.LABORLEV4NM='SP2') "
							.cTempCoWhere2 = " AND (LABORLEVELNAME1 = 'SXI') AND (LABORLEVELNAME4 = 'SP2') "
						CASE .cTempCo == 'D'
							.cTempCoWhere =  " AND (D.LABORLEV1NM = 'TMP') AND (D.LABORLEV5NM = '" + .cTempCo + "') "
							.cTempCoWhere2 = " AND (LABORLEVELNAME1 = 'TMP') AND (LABORLEVELNAME5 = '" + .cTempCo + "') "
						CASE .cTempCo == 'E'
							.cTempCoWhere =  " AND (D.LABORLEV1NM = 'TMP') AND (D.LABORLEV5NM = '" + .cTempCo + "') "
							.cTempCoWhere2 = " AND (LABORLEVELNAME1 = 'TMP') AND (LABORLEVELNAME5 = '" + .cTempCo + "') "
						CASE .cTempCo == 'HMG'
							* report on RIALTO hourly for Mark Goldberg
							.cTempCoWhere =  " AND (D.LABORLEV1NM = 'SXI') AND (D.LABORLEV2NM = '59') "
							.cTempCoWhere2 = " AND (LABORLEVELNAME1 = 'SXI') AND (LABORLEVELNAME2 = '59') "
						CASE .cTempCo == 'J'
							.cTempCoWhere =  " AND (D.LABORLEV1NM = 'TMP') AND (D.LABORLEV5NM = '" + .cTempCo + "') "
							.cTempCoWhere2 = " AND (LABORLEVELNAME1 = 'TMP') AND (LABORLEVELNAME5 = '" + .cTempCo + "') "
						CASE .cTempCo == 'K'
							.cTempCoWhere =  " AND (D.LABORLEV1NM = 'TMP') AND (D.LABORLEV5NM = '" + .cTempCo + "') "
							.cTempCoWhere2 = " AND (LABORLEVELNAME1 = 'TMP') AND (LABORLEVELNAME5 = '" + .cTempCo + "') "
						CASE INLIST(.cTempCo,'K+0630')
							* K - special invoicing
							.cTempCoWhere =  " AND (D.LABORLEV1NM = 'TMP') AND (D.LABORLEV3NM = '0630') AND (D.LABORLEV5NM = 'K') "
							.cTempCoWhere2 = " AND (LABORLEVELNAME1 = 'TMP') AND (LABORLEVELNAME3 = '0630') AND (LABORLEVELNAME5 = 'K') "
						CASE INLIST(.cTempCo,'K-0630')
							* K - special invoicing
							.cTempCoWhere =  " AND (D.LABORLEV1NM = 'TMP') AND (D.LABORLEV3NM <> '0630') AND (D.LABORLEV5NM = 'K') "
							.cTempCoWhere2 = " AND (LABORLEVELNAME1 = 'TMP') AND (LABORLEVELNAME3 <> '0630') AND (LABORLEVELNAME5 = 'K') "
						CASE .cTempCo == 'L'
							.cTempCoWhere =  " AND (D.LABORLEV1NM = 'TMP') AND (D.LABORLEV5NM = '" + .cTempCo + "') "
							.cTempCoWhere2 = " AND (LABORLEVELNAME1 = 'TMP') AND (LABORLEVELNAME5 = '" + .cTempCo + "') "
						CASE .cTempCo == 'M'
							.cTempCoWhere =  " AND (D.LABORLEV1NM = 'TMP') AND (D.LABORLEV5NM = '" + .cTempCo + "') "
							.cTempCoWhere2 = " AND (LABORLEVELNAME1 = 'TMP') AND (LABORLEVELNAME5 = '" + .cTempCo + "') "
						CASE .cTempCo == 'N'
							.cTempCoWhere =  " AND (D.LABORLEV1NM = 'TMP') AND (D.LABORLEV5NM = '" + .cTempCo + "') "
							.cTempCoWhere2 = " AND (LABORLEVELNAME1 = 'TMP') AND (LABORLEVELNAME5 = '" + .cTempCo + "') "
						CASE .cTempCo == 'O'
							.cTempCoWhere =  " AND (D.LABORLEV1NM = 'TMP') AND (D.LABORLEV5NM = '" + .cTempCo + "') "
							.cTempCoWhere2 = " AND (LABORLEVELNAME1 = 'TMP') AND (LABORLEVELNAME5 = '" + .cTempCo + "') "
						CASE .cTempCo == 'P'
							.cTempCoWhere =  " AND (D.LABORLEV1NM = 'TMP') AND (D.LABORLEV5NM = '" + .cTempCo + "') "
							.cTempCoWhere2 = " AND (LABORLEVELNAME1 = 'TMP') AND (LABORLEVELNAME5 = '" + .cTempCo + "') "
						CASE .cTempCo == 'Q'
							.cTempCoWhere =  " AND (D.LABORLEV1NM = 'TMP') AND (D.LABORLEV5NM = '" + .cTempCo + "') "
							.cTempCoWhere2 = " AND (LABORLEVELNAME1 = 'TMP') AND (LABORLEVELNAME5 = '" + .cTempCo + "') "
						CASE .cTempCo == 'R'
							.cTempCoWhere =  " AND (D.LABORLEV1NM = 'TMP') AND (D.LABORLEV5NM = '" + .cTempCo + "') "
							.cTempCoWhere2 = " AND (LABORLEVELNAME1 = 'TMP') AND (LABORLEVELNAME5 = '" + .cTempCo + "') "
						CASE .cTempCo == 'S'
							.cTempCoWhere =  " AND (D.LABORLEV1NM = 'TMP') AND (D.LABORLEV5NM = '" + .cTempCo + "') "
							.cTempCoWhere2 = " AND (LABORLEVELNAME1 = 'TMP') AND (LABORLEVELNAME5 = '" + .cTempCo + "') "
						CASE .cTempCo == 'T'
							.cTempCoWhere =  " AND (D.LABORLEV1NM = 'TMP') AND (D.LABORLEV5NM = '" + .cTempCo + "') "
							.cTempCoWhere2 = " AND (LABORLEVELNAME1 = 'TMP') AND (LABORLEVELNAME5 = '" + .cTempCo + "') "
						CASE .cTempCo == 'U'
							.cTempCoWhere =  " AND (D.LABORLEV1NM = 'TMP') AND (D.LABORLEV5NM = '" + .cTempCo + "') "
							.cTempCoWhere2 = " AND (LABORLEVELNAME1 = 'TMP') AND (LABORLEVELNAME5 = '" + .cTempCo + "') "
						CASE .cTempCo == 'V'
							.cTempCoWhere =  " AND (D.LABORLEV1NM = 'TMP') AND (D.LABORLEV5NM = '" + .cTempCo + "') "
							.cTempCoWhere2 = " AND (LABORLEVELNAME1 = 'TMP') AND (LABORLEVELNAME5 = '" + .cTempCo + "') "
						CASE .cTempCo == 'W'
							.cTempCoWhere =  " AND (D.LABORLEV1NM = 'TMP') AND (D.LABORLEV5NM = '" + .cTempCo + "') "
							.cTempCoWhere2 = " AND (LABORLEVELNAME1 = 'TMP') AND (LABORLEVELNAME5 = '" + .cTempCo + "') "
						CASE .cTempCo == 'X'
							.cTempCoWhere =  " AND (D.LABORLEV1NM = 'TMP') AND (D.LABORLEV5NM = '" + .cTempCo + "') "
							.cTempCoWhere2 = " AND (LABORLEVELNAME1 = 'TMP') AND (LABORLEVELNAME5 = '" + .cTempCo + "') "
						CASE .cTempCo == 'Y'
							.cTempCoWhere =  " AND (D.LABORLEV1NM = 'TMP') AND (D.LABORLEV5NM = '" + .cTempCo + "') "
							.cTempCoWhere2 = " AND (LABORLEVELNAME1 = 'TMP') AND (LABORLEVELNAME5 = '" + .cTempCo + "') "
						CASE .cTempCo == 'Z'
							.cTempCoWhere =  " AND (D.LABORLEV1NM = 'TMP') AND (D.LABORLEV5NM = '" + .cTempCo + "') "
							.cTempCoWhere2 = " AND (LABORLEVELNAME1 = 'TMP') AND (LABORLEVELNAME5 = '" + .cTempCo + "') "
						OTHERWISE
							.cTempCoWhere =  " AND (D.LABORLEV1NM = 'TMP') AND (D.LABORLEV5NM = '" + .cTempCo + "') "
							.cTempCoWhere2 = " AND (LABORLEVELNAME1 = 'TMP') AND (LABORLEVELNAME5 = '" + .cTempCo + "') "
					ENDCASE
				ELSE
					.cTempCoWhere = ""
					.cTempCoWhere2 = ""
				ENDIF

				*!*					* exclude Div 59 for all reports except the Div 59 ones 7/5/13 MB
				*!*					* BECAUSE Rialto on and after 7/1/13 belongs to UnderArmour, not us. Per Rich Nazzaro 7/3/13.
				*!*					IF NOT INLIST(.cMode,"DIV59WEEKLY","DIV59DAILY") THEN
				*!*						.cTempCoWhere =.cTempCoWhere + " AND (D.LABORLEV2NM <> '59') "
				*!*						.cTempCoWhere2 =.cTempCoWhere2 + " AND (LABORLEVELNAME2 <> '59') "
				*!*						.TrackProgress('*************** NOTE: Div 59 Data has been excluded from this report!', LOGIT+SENDIT)
				*!*					ENDIF

				DO CASE
					CASE INLIST(.cMode,"DIV57MONTHLY")
						* MODIFY SQL to report on specific division
						.cExtraWhere =  " AND (D.LABORLEV2NM = '57') "
						.cExtraWhere2 = " AND (LABORLEVELNAME2 = '57') "
					CASE INLIST(.cMode,"DIV52WEEKLY","DIV52DAILY")
						* MODIFY SQL to report on specific division
						.cExtraWhere =  " AND (D.LABORLEV2NM = '52') "
						.cExtraWhere2 = " AND (LABORLEVELNAME2 = '52') "
					CASE .cMode = "DIV56"
						* MODIFY SQL to report on specific division
						.cExtraWhere =  " AND (D.LABORLEV2NM = '56') "
						.cExtraWhere2 = " AND (LABORLEVELNAME2 = '56') "
					CASE INLIST(.cMode,"DIV58WEEKLY","DIV58DAILY")
						* MODIFY SQL to report on specific division
						.cExtraWhere =  " AND (D.LABORLEV2NM = '58') "
						.cExtraWhere2 = " AND (LABORLEVELNAME2 = '58') "
					CASE INLIST(.cMode,"DIV59WEEKLY","DIV59DAILY")
						* MODIFY SQL to report on specific division
						.cExtraWhere =  " AND (D.LABORLEV2NM = '59') "
						.cExtraWhere2 = " AND (LABORLEVELNAME2 = '59') "
					CASE .cMode = "DATERANGE"
						* MODIFY SQL IF necessary
						*.cExtraWhere =  " AND (D.LABORLEV2NM IN ('51','59')) "
						*.cExtraWhere =  " AND (D.LABORLEV2NM = '68') "
						*.cExtraWhere2 = " AND (LABORLEVELNAME2 = '68') "
						*.cExtraWhere =  " AND (D.LABORLEV2NM = '58') "
						*.cExtraWhere2 = " AND (LABORLEVELNAME2 = '58') "
						*.cExtraWhere =  " AND (D.LABORLEV2NM = '56') "
						*.cExtraWhere2 = " AND (LABORLEVELNAME2 = '56') "
						*.cExtraWhere =  " AND (D.LABORLEV2NM = '59') "
						*.cExtraWhere2 = " AND (LABORLEVELNAME2 = '59') "
						*.cExtraWhere =  " AND (D.LABORLEV2NM IN ('53','59')) "
						*.cExtraWhere2 = " AND (LABORLEVELNAME2 IN ('53','59')) "
						*.cExtraWhere =  " AND (D.LABORLEV2NM = '57') "
						*.cExtraWhere2 = " AND (LABORLEVELNAME2 = '57') "
						*.cExtraWhere =  " AND (D.LABORLEV2NM = '50') "
						*.cExtraWhere2 = " AND (LABORLEVELNAME2 = '50') "
						*.cExtraWhere =  " AND (D.LABORLEV3NM IN ('0620','0625','0635')) "
						*.cExtraWhere2 = " AND (LABORLEVELNAME3 IN ('0620','0625','0635')) "
						* filter on multiple associates
						*.cExtraWhere  = " AND (C.PERSONNUM IN ('503884','503238','503664','503954')) "
						*.cExtraWhere2 = " AND (PERSONNUM IN ('503884','503238','503664','503954')) "
				ENDCASE



				*!*	IF .lTestMode THEN
				*!*		.cExtraWhere =  " AND (D.LABORLEV2NM = '58') "
				*!*	ENDIF

				* main SQL
				lcSQL = ;
					" SELECT " + ;
					" '1' AS CPASS, " + ;
					" SPACE(3) AS DIVISION, " + ;
					" D.LABORLEV5NM AS AGENCYNUM, " + ;
					" D.LABORLEV5DSC AS AGENCY, " + ;
					" D.LABORLEV2NM AS DIVISION2, " + ;
					" D.LABORLEV3NM AS DEPT, " + ;
					" D.LABORLEV6NM AS CLIENTCODE, " + ;
					" D.LABORLEV6DSC AS CLIENTDESC, " + ;
					" D.LABORLEV7NM AS TIMERVWR, " + ;
					" C.FULLNM AS EMPLOYEE, " + ;
					" C.SHORTNM AS SHORTNAME, " + ;
					" C.PERSONNUM AS FILE_NUM, " + ;
					" C.PERSONID, " + ;
					" A.EMPLOYEEID, " + ;
					" E.ABBREVIATIONCHAR AS PAYCODE, " + ;
					" E.NAME AS PAYCODEDESC, " + ;
					" (SUM(A.DURATIONSECSQTY) / 3600.00) AS TOTHOURS, " + ;
					" SUM(A.MONEYAMT) AS TOTPAY, " + ;
					" 000000.0000 AS WAGE " + ;
					" FROM WFCTOTAL A " + ;
					" JOIN WTKEMPLOYEE B " + ;
					" ON B.EMPLOYEEID = A.EMPLOYEEID " + ;
					" JOIN PERSON C " + ;
					" ON C.PERSONID = B.PERSONID " + ;
					" JOIN LABORACCT D " + ;
					" ON D.LABORACCTID = A.LABORACCTID " + ;
					" JOIN PAYCODE E " + ;
					" ON E.PAYCODEID = A.PAYCODEID " + ;
					" WHERE (A.APPLYDTM >= '" + lcSQLStartDate + "')" + ;
					" AND (A.APPLYDTM < '" + lcSQLEndDate + "')" + ;
					" AND E.NAME IN ('Regular Hours','Overtime Hours','Doubltime Hours','Bonus Pay') " + ;
					.cTempCoWhere + .cExtraWhere+ .cExtraWhere3 + ;
					" GROUP BY D.LABORLEV5NM, D.LABORLEV5DSC, D.LABORLEV2NM, D.LABORLEV3NM, D.LABORLEV6NM, D.LABORLEV6DSC, D.LABORLEV7NM, C.FULLNM, C.SHORTNM, C.PERSONNUM, C.PERSONID, A.EMPLOYEEID, E.ABBREVIATIONCHAR, E.NAME " + ;
					" ORDER BY 1, 2, 3, 4, 7, 8, 9 "

				*!*	" AND (D.LABORLEV1NM = 'TMP') " + ;


				lcSQL2 = ;
					" SELECT DISTINCT " + ;
					" NAME, DESCRIPTION " + ;
					" FROM LABORLEVELENTRY " + ;
					" WHERE LABORLEVELDEFID = 2 " + ;
					" AND INACTIVE <> 1 " + ;
					" ORDER BY NAME "

				lcSQL3 = ;
					" SELECT DISTINCT " + ;
					" NAME, DESCRIPTION " + ;
					" FROM LABORLEVELENTRY " + ;
					" WHERE LABORLEVELDEFID = 3 " + ;
					" AND INACTIVE <> 1 " + ;
					" ORDER BY NAME "

				lcSQL4 = ;
					" SELECT " + ;
					" PERSONNUM AS FILE_NUM " + ;
					" FROM VP_EMPLOYEEV42 " + ;
					" WHERE HOMELABORLEVELNM1 = 'TMP' " + ;
					" AND PERSONID NOT IN (SELECT PERSONID FROM BIOFINGERSCAN) " + ;
					" ORDER BY PERSONNUM "

				lcSQL5 = ;
					" SELECT " + ;
					" PERSONNUM AS FILE_NUM, " + ;
					" EVENTDATE, " + ;
					" INPUNCHDTM " + ;
					" FROM VP_TIMESHTPUNCHV42 " + ;
					" WHERE (EVENTDATE >= '" + lcSQLStartDate + "')" + ;
					" AND (EVENTDATE < '" + lcSQLEndDate + "')" + ;
					.cTempCoWhere2 + .cExtraWhere2 + .cExtraWhere4 + ;
					" ORDER BY PERSONNUM, EVENTDATE, INPUNCHDTM "

				*!*	" AND (LABORLEVELNAME1 = 'TMP') " + ;

				lcSQL6 = " SELECT PERSONNUM AS FILE_NUM, LASTNM, FIRSTNM FROM VP_EMPLOYEEV42 WHERE HOMELABORLEVELNM1 = 'TMP' ORDER BY PERSONNUM "


				SET TEXTMERGE ON

				TEXT TO	lcSQL7 NOSHOW


SELECT
C.FULLNM AS EMPLOYEE,
C.SHORTNM AS FILE_NUM,
(SUM(CASE E.NAME WHEN 'Regular Hours' THEN A.DURATIONSECSQTY ELSE 0.000 END) / 3600) AS REGHOURS,
(SUM(CASE E.NAME WHEN 'Overtime Hours' THEN A.DURATIONSECSQTY ELSE 0.000 END) / 3600) AS OTHOURS,
(SUM(CASE E.NAME WHEN 'Doubltime Hours' THEN A.DURATIONSECSQTY ELSE 0.000 END) / 3600) AS DBLHOURS 
FROM WFCTOTAL A
JOIN WTKEMPLOYEE B
ON B.EMPLOYEEID = A.EMPLOYEEID
JOIN PERSON C
ON C.PERSONID = B.PERSONID
JOIN LABORACCT D
ON D.LABORACCTID = A.LABORACCTID
JOIN PAYCODE E
ON E.PAYCODEID = A.PAYCODEID
WHERE (A.APPLYDTM >= <<lcYTDSummaryStartDate>>)
AND (A.APPLYDTM <= <<lcYTDSummaryEndDate>>)
<<.cTempCoWhere>>
AND E.NAME IN ('Regular Hours','Overtime Hours','Doubltime Hours')
AND A.DURATIONSECSQTY > 0.00
GROUP BY C.FULLNM, C.SHORTNM
ORDER BY C.FULLNM

				ENDTEXT

				SET TEXTMERGE OFF


				lcSQL8 = ;
					" SELECT DISTINCT " + ;
					" NAME, DESCRIPTION " + ;
					" FROM LABORLEVELENTRY " + ;
					" WHERE LABORLEVELDEFID = 6 " + ;
					" AND INACTIVE <> 1 " + ;
					" ORDER BY NAME "


				IF .lTestMode THEN
					.TrackProgress('', LOGIT+SENDIT)
					.TrackProgress('lcSQL =' + lcSQL, LOGIT+SENDIT)
					.TrackProgress('', LOGIT+SENDIT)
					.TrackProgress('lcSQL2 =' + lcSQL2, LOGIT+SENDIT)
					.TrackProgress('', LOGIT+SENDIT)
					.TrackProgress('lcSQL3 =' + lcSQL3, LOGIT+SENDIT)
					.TrackProgress('', LOGIT+SENDIT)
					.TrackProgress('lcSQL4 =' + lcSQL4, LOGIT+SENDIT)
					.TrackProgress('', LOGIT+SENDIT)
					.TrackProgress('lcSQL5 =' + lcSQL5, LOGIT+SENDIT)
					.TrackProgress('', LOGIT+SENDIT)
					.TrackProgress('lcSQL6 =' + lcSQL6, LOGIT+SENDIT)
					.TrackProgress('', LOGIT+SENDIT)
					.TrackProgress('lcSQL7 =' + lcSQL7, LOGIT+SENDIT)
					.TrackProgress('', LOGIT+SENDIT)
					.TrackProgress('lcSQL8 =' + lcSQL8, LOGIT+SENDIT)
				ENDIF

				*.TrackProgress('Connecting to WTK System....', LOGIT+SENDIT)

				* access kronos
				*OPEN DATABASE F:\UTIL\KRONOS\KRONOSEDI

				*.nSQLHandle = SQLCONNECT("WTK_ON_PAYROLL2","sa","kr0n1tes")
				.nSQLHandle = SQLCONNECT("WTK2","sa","kr0n1tes")

				IF .nSQLHandle > 0 THEN

					IF .ExecSQL(lcSQL, 'CURWTKHOURSPRE', RETURN_DATA_MANDATORY) AND ;
							.ExecSQL(lcSQL2, 'CURDIVISIONS', RETURN_DATA_NOT_MANDATORY) AND ;
							.ExecSQL(lcSQL8, 'CURCLIENTS', RETURN_DATA_NOT_MANDATORY) AND ;
							.ExecSQL(lcSQL3, 'CURDEPARTMENTS', RETURN_DATA_NOT_MANDATORY) AND ;
							.ExecSQL(lcSQL4, 'CURNOBIOMETRICPRE', RETURN_DATA_NOT_MANDATORY) AND ;
							.ExecSQL(lcSQL5, 'CURTIMESHEET2', RETURN_DATA_NOT_MANDATORY) AND ;
							.ExecSQL(lcSQL6, 'CURLASTFIRST', RETURN_DATA_MANDATORY) AND ;
							.ExecSQL(lcSQL7, 'CURYTDSUMMARY', RETURN_DATA_NOT_MANDATORY) THEN


						*******************************************************************************
						* YTD SUMMARY OF HOURS FOR AGENCIES TO CALCULATE SICK TIME EARNED
						lcFilePrefix = "YTD Hours Summary " + .cTempCoName
						lcFileDate = STRTRAN((lcYTDSummaryStartDate + " - " + lcYTDSummaryEndDate),"'")
						lcTargetDirectory = "F:\UTIL\KRONOS\TEMPTIMEREPORTS\"
						lcFiletoSaveAs = lcTargetDirectory + lcFilePrefix + " " + lcFileDate
						lcYTDSummaryFile = lcFiletoSaveAs + ".XLS"
						IF USED('CURYTDSUMMARY') AND NOT EOF('CURYTDSUMMARY') THEN
							SELECT CURYTDSUMMARY
							*COPY TO (lcYTDSummaryFile) XLS
							IF INLIST(.cTempCo,"R","T","U") THEN
								* copy all fields for ssg sites - they have simple file_nums
								COPY FIELDS EMPLOYEE, FILE_NUM, REGHOURS, OTHOURS, DBLHOURS TO (lcYTDSummaryFile) XLS
							ELSE
								* don't copy file_num for non ssg - they will require special parsing of shortnmm to do correctly
								COPY FIELDS EMPLOYEE, REGHOURS, OTHOURS, DBLHOURS TO (lcYTDSummaryFile) XLS
							ENDIF

						ENDIF
						*******************************************************************************
						

						SELECT ;
							FILE_NUM, ;
							TTOD(EVENTDATE) AS EVENTDATE,  ;
							INPUNCHDTM ;
							FROM CURTIMESHEET2 ;
							INTO CURSOR CURTIMESHEET ;
							ORDER BY FILE_NUM, EVENTDATE, INPUNCHDTM ;
							WHERE NOT ISNULL( INPUNCHDTM )

						IF USED('CURTIMESHEET2') THEN
							USE IN CURTIMESHEET2
						ENDIF

						*!*	IF .lTestMode THEN
						*!*		SELECT CURTIMESHEET
						*!*		BROWSE
						*!*	ENDIF

						*!*	SELECT CURDEPARTMENTS   CURTIMESHEET
						*!*	BROWSE

						* there was data in entire date range, marked as Pass = 1, which will be deleted later

						* add a foxpro date field to the cursor
						SELECT CTOD('') AS PAYDATE, * ;
							FROM CURWTKHOURSPRE ;
							INTO CURSOR CURWTKHOURS ;
							READWRITE

						* now, loop thru date range and create cursor by each individual date



						DO CASE
								*CASE .cMode = "DAILY"
							CASE INLIST(.cMode,"DAILY","ZDAILY","DIV52DAILY","DIV58DAILY","DIV59DAILY")
								.BuildDailyCursor( ldStartDate )
							CASE INLIST(.cMode, "WEEKLY", "ZWEEKLY", "INVOICE", "SUMMARYONLY", "DATERANGE", "DATERANGE-INVOICE", "DIV56", "DIV59WEEKLY","DIV57MONTHLY")
								ldPass2Date = ldStartDate
								DO WHILE ldPass2Date <= ldEndDate
									.BuildDailyCursor( ldPass2Date )
									ldPass2Date =ldPass2Date + 1
								ENDDO
						ENDCASE
						
						* at this point all the detail data is in CURWTKHOURS.
						* we need to examine field CLIENTCODE. If it is a 1 or 2, that means there is no special sub-division client - just copy DIVISION2 into DIVISION.
						* Otherwise there is a sub-division client, so append the clientcode to DIVISION2 and save in DIVISION.
						
						WAIT WINDOW NOWAIT "Looking for clients within the Divisions..."
						
						SELECT CURWTKHOURS
						SCAN
						
							lcClientCode = ALLTRIM(CURWTKHOURS.CLIENTCODE)
							
							DO CASE
								CASE INLIST(lcClientCode,"0","1","2","3")
									* old shift code # - NO SPECIAL CLIENT
									lcNewDivision = ALLTRIM(CURWTKHOURS.DIVISION2)
								OTHERWISE	

									lcNewDivision = ALLTRIM(CURWTKHOURS.DIVISION2) + lcClientCode
							ENDCASE
							
							REPLACE CURWTKHOURS.DIVISION WITH lcNewDivision IN CURWTKHOURS
							
						ENDSCAN
						
*!*	SELECT CURWTKHOURS
*!*	BROWSE


						WAIT WINDOW NOWAIT "Preparing data..."
						*!*	IF .lTestMode THEN
						*!*		SELECT CURWAGES
						*!*		BROWSE FOR WAGE = 10.25
						*!*		THROW
						*!*	ENDIF

						* at this point both pass 1 & 2 data are combined in CURWTKHOURS
						*!*	IF .lTestMode THEN
						*!*		SELECT CURWTKHOURS
						*!*		BROW
						*!*	ENDIF

*!*	IF .lTestMode THEN
*!*		SELECT CURWTKHOURS
*!*		BROWSE
*!*	ENDIF


						SELECT CURWTKHOURS
						INDEX ON CPASS TAG TCPASS
						INDEX ON PAYDATE TAG TPAYDATE
						INDEX ON DIVISION TAG TDIV
						INDEX ON AGENCYNUM TAG AGENCYNUM
						INDEX ON DEPT TAG TDEPT
						INDEX ON EMPLOYEE TAG TEMPLOYEE
						INDEX ON FILE_NUM TAG TFILE_NUM

						****** horizontalize cursor so paycodes are in separate columns

						IF USED('CURTEMPS') THEN
							USE IN CURTEMPS
						ENDIF

						* create cursor with one row per employee which will drive spreadsheet,
						* filter out pass 1, etc.
						lcOrderBy = "ORDER BY A.AGENCY, A.DIVISION, A.DEPT, A.EMPLOYEE, A.FILE_NUM, A.PAYDATE"

						SELECT ;
							A.PAYDATE, ;
							A.DIVISION, ;
							A.AGENCY, ;
							A.AGENCYNUM, ;
							A.DEPT, ;
							A.EMPLOYEE, ;
							A.SHORTNAME, ;
							A.FILE_NUM, ;
							A.WAGE, ;
							A.CLIENTCODE, ;
							A.CLIENTDESC, ;
							' ' AS CALCDSHIFT, ;
							CTOT('') AS INPUNCHDTM, ;
							0000.00 AS REG_HRS, ;
							0000.00 AS OT_HRS, ;
							0000.00 AS OT20_HRS, ;
							0000.00 AS UNPD_HRS, ;
							000000000.0000 AS PAY, ;
							000000000.0000 AS BONUS_PAY ;
							FROM CURWTKHOURS A ;
							INTO CURSOR CURTEMPS ;
							WHERE CPASS = '2' ;
							GROUP BY A.PAYDATE, A.DIVISION, A.AGENCY, A.AGENCYNUM, A.DEPT, A.EMPLOYEE, A.SHORTNAME, A.FILE_NUM, A.CLIENTCODE, A.CLIENTDESC ;
							&lcOrderBy. ;
							READWRITE


						*!*	SELECT CURTEMPS
						*!*	BROWSE
						*!*	THROW
						
		*SET STEP ON 

						* populate hours/code fields in main cursor
						SELECT CURTEMPS
						SCAN
							SCATTER MEMVAR
							STORE 0000.00 TO ;
								M.REG_HRS, ;
								M.OT_HRS, ;
								M.OT20_HRS, ;
								M.BONUS_PAY, ;
								M.UNPD_HRS

							SELECT CURWTKHOURS
							SCAN FOR ;
									CPASS = '2' AND ;
									PAYDATE = CURTEMPS.PAYDATE AND ;
									DIVISION = CURTEMPS.DIVISION AND ;
									DEPT = CURTEMPS.DEPT AND ;
									AGENCY = CURTEMPS.AGENCY AND ;
									AGENCYNUM = CURTEMPS.AGENCYNUM AND ;
									EMPLOYEE = CURTEMPS.EMPLOYEE AND ;
									FILE_NUM = CURTEMPS.FILE_NUM AND ;
									CLIENTCODE = CURTEMPS.CLIENTCODE

								lcPayCode = UPPER(ALLTRIM(CURWTKHOURS.PAYCODE))

								DO CASE
									CASE lcPayCode == "BON"
										m.BONUS_PAY = m.BONUS_PAY + CURWTKHOURS.TOTPAY
									CASE lcPayCode == "REG"
										m.REG_HRS = m.REG_HRS + CURWTKHOURS.TOTHOURS
									CASE lcPayCode == "OTM"
										m.OT_HRS = m.OT_HRS + CURWTKHOURS.TOTHOURS
									CASE lcPayCode == "DTM"
										m.OT20_HRS = m.OT20_HRS + CURWTKHOURS.TOTHOURS
									CASE INLIST(lcPayCode,"???")
										m.UNPD_HRS = m.UNPD_HRS + CURWTKHOURS.TOTHOURS
									OTHERWISE
										* NOTHING
								ENDCASE

							ENDSCAN
							* accumulate totals
							SELECT CURTEMPS
							GATHER MEMVAR
						ENDSCAN



*!*	IF .lTestMode THEN
*!*		SELECT CURTEMPS
*!*		BROWSE
*!*	ENDIF

						*!*	IF .lTestMode THEN
						*!*		SELECT CURTEMPS
						*!*		COPY TO C:\A\KTTR_OT20.XLS XL5 FOR OT20_HRS > 0
						*!*	ENDIF

						* populate inpunchdtm and calcdshift in main cursor
						SELECT CURTEMPS
						SCAN
							SELECT CURTIMESHEET
							GOTO TOP
							LOCATE FOR (FILE_NUM = CURTEMPS.FILE_NUM) AND (EVENTDATE = CURTEMPS.PAYDATE)
							IF FOUND() THEN
								ltInpunch = CURTIMESHEET.INPUNCHDTM
								REPLACE CURTEMPS.INPUNCHDTM WITH ltInpunch, ;
									CURTEMPS.CALCDSHIFT WITH .oTempAgency.GetCalculatedShift( ltInpunch )
							ENDIF
						ENDSCAN

						*!*	IF .lTestMode THEN
						*!*		SELECT CURTEMPS
						*!*		BROW
						*!*	ENDIF

						SELECT CURTEMPS
						GOTO TOP
						IF EOF() THEN
							.TrackProgress("There was no data to export!", LOGIT+SENDIT)
						ELSE



							***********************************************************************************
							* if a temp report (not hourly), identify possible wage discrepancies in the period
							*lcChar = UPPER(LEFT(.cTempCo,1))
							*IF INLIST(lcChar,"A","B") THEN

								IF USED('CURBADWAGES') THEN
									USE IN CURBADWAGES
								ENDIF

								lnMinWageTest =	.nMinWageTest
								lnMaxWageTest = .nMaxWageTest

								SELECT EMPLOYEE, FILE_NUM, PAYDATE, WAGE ;
									FROM CURTEMPS ;
									INTO CURSOR CURBADWAGES ;
									WHERE ( WAGE < lnMinWageTest ) OR ( WAGE > lnMaxWageTest ) ;
									ORDER BY 1, 3

								IF USED("CURBADWAGES") AND NOT EOF("CURBADWAGES") THEN
									lcBADWAGES = "Warning: these Temps may have had invalid Base Wages during the period." + CRLF + CRLF
									SELECT CURBADWAGES
									SCAN
										lcBADWAGES = lcBADWAGES + ;
											LEFT(CURBADWAGES.EMPLOYEE,30) + TAB + TAB + ;
											LEFT(CURBADWAGES.FILE_NUM,8) + TAB + TAB + ;
											TRANSFORM(CURBADWAGES.PAYDATE) + TAB + ;
											TRANSFORM(CURBADWAGES.WAGE,'999999.99') + CRLF
									ENDSCAN

									.TrackProgress(lcBADWAGES, LOGIT+SENDIT+NOCRLF)

								ENDIF

								IF USED('CURBADWAGES') THEN
									USE IN CURBADWAGES
								ENDIF

							*ENDIF



							*********************************************************************************
							* identify temps who had more than one primary account during the period.
							* this could be legitimate for a week, but it should never happen for a single day

							IF USED('CURPRIMACCTCHANGEDPRE') THEN
								USE IN CURPRIMACCTCHANGEDPRE
							ENDIF
							IF USED('CURPRIMACCTCHANGED') THEN
								USE IN CURPRIMACCTCHANGED
							ENDIF

							SELECT DISTINCT EMPLOYEE, DIVISION, DEPT ;
								FROM CURTEMPS ;
								INTO CURSOR CURPRIMACCTCHANGEDPRE ;
								READWRITE

							SELECT DISTINCT A.EMPLOYEE, A.FILE_NUM, A.DIVISION, A.DEPT, A.PAYDATE, ;
								( SELECT COUNT(*) FROM CURPRIMACCTCHANGEDPRE WHERE EMPLOYEE = A.EMPLOYEE )AS COUNT ;
								FROM CURTEMPS A ;
								HAVING COUNT > 1 ;
								INTO CURSOR CURPRIMACCTCHANGED ;
								ORDER BY 1, 5

							IF USED('CURPRIMACCTCHANGED') AND NOT EOF('CURPRIMACCTCHANGED') THEN

								DO CASE
										*CASE .cMode = "DAILY"
									CASE INLIST(.cMode, "DAILY", "ZDAILY")
										* the temp worked 1/2 day in one dept, 1/2 day in another, and the time reviewer split
										* the date to indicate this with labor transfers.
										lcPRIMACCTCHANGED = "WARNING: these Temps had more than one Primary Account in a single Day!" + CRLF + CRLF
									OTHERWISE
										lcPRIMACCTCHANGED = "Warning: these Temps had more than one Primary Account during the period." + CRLF + CRLF
								ENDCASE

								SELECT CURPRIMACCTCHANGED
								SCAN
									lcPRIMACCTCHANGED = lcPRIMACCTCHANGED + ;
										LEFT(CURPRIMACCTCHANGED.EMPLOYEE,30) + ;
										LEFT(CURPRIMACCTCHANGED.FILE_NUM,8) + ;
										LEFT(CURPRIMACCTCHANGED.DIVISION,4) + ;
										LEFT(CURPRIMACCTCHANGED.DEPT,6) + ;
										TRANSFORM(CURPRIMACCTCHANGED.PAYDATE) + CRLF
								ENDSCAN

								.TrackProgress(lcPRIMACCTCHANGED, LOGIT+SENDIT+NOCRLF)

							ENDIF && USED('CURPRIMACCTCHANGED') AND NOT EOF('CURPRIMACCTCHANGED') THEN

							************************************************************************

							* Create a list of those temps in this spreadsheet who are not biometrically enrolled
							IF .lCheckBio THEN
								IF USED('CURNOBIOMETRICPRE') AND NOT EOF('CURNOBIOMETRICPRE') THEN

									SELECT DISTINCT EMPLOYEE, FILE_NUM ;
										FROM CURTEMPS ;
										INTO CURSOR CURNOBIOMETRIC ;
										WHERE FILE_NUM IN (SELECT FILE_NUM FROM CURNOBIOMETRICPRE) ;
										ORDER BY EMPLOYEE

									IF USED('CURNOBIOMETRIC') AND NOT EOF('CURNOBIOMETRIC') THEN
										*.TrackProgress('', LOGIT+SENDIT)
										.TrackProgress("!!! WARNING: the following temps have not been biometrically enrolled:", LOGIT+SENDIT)
										SELECT CURNOBIOMETRIC
										SCAN
											lcNoBioFileNum = ALLTRIM(CURNOBIOMETRIC.FILE_NUM)
											lcUnscannable = IIF(.IsTempUnscannable( lcNoBioFileNum ), " (fingers unscannable)","")
											.TrackProgress(LEFT(CURNOBIOMETRIC.EMPLOYEE,30) + " " + lcNoBioFileNum + lcUnscannable, LOGIT+SENDIT+NOCRLF)
										ENDSCAN
										.TrackProgress('', LOGIT+SENDIT)
									ENDIF

								ENDIF 	&& USED('CURNOBIOMETRICPRE') AND NOT EOF('CURNOBIOMETRICPRE') THEN
							ENDIF && .lCheckBio

							DO CASE
									*CASE .cMode = "DAILY"
								CASE INLIST(.cMode, "DAILY", "ZDAILY")
									lcFilePrefix = "Daily " + .cTempCoName
									lcFileDate = PADL(MONTH(ldStartDate),2,"0") + "-"  + PADL(DAY(ldStartDate),2,"0") + "-" + PADL(YEAR(ldStartDate),4,"0")
									*CASE .cMode = "WEEKLY"
								CASE INLIST(.cMode, "WEEKLY", "ZWEEKLY")
									lcFilePrefix = "Weekly Time " + .cTempCoName
									IF .nPeriodEndDayNum = 1 THEN
										ldPayDate = ldEndDate + 5
									ELSE
										ldPayDate = ldEndDate + 6
									ENDIF
									lcFileDate = PADL(MONTH(ldPayDate),2,"0") + "-"  + PADL(DAY(ldPayDate),2,"0") + "-" + PADL(YEAR(ldPayDate),4,"0")
								CASE .cMode == "DATERANGE"
									lcFilePrefix = "Date Range Time " + .cTempCoName
									ldPayDate = ldEndDate + 6
									lcFileDate = PADL(MONTH(ldPayDate),2,"0") + "-"  + PADL(DAY(ldPayDate),2,"0") + "-" + PADL(YEAR(ldPayDate),4,"0")
								CASE .cMode == "DATERANGE-INVOICE"
									lcFilePrefix = "Date Range Invoice " + .cTempCoName
									ldPayDate = ldEndDate && + 6
									lcFileDate = PADL(MONTH(ldPayDate),2,"0") + "-"  + PADL(DAY(ldPayDate),2,"0") + "-" + PADL(YEAR(ldPayDate),4,"0")
								CASE INLIST(.cMode, "INVOICE", "SUMMARYONLY")

									DO CASE
										CASE .cTempCo == 'K+0630'
											lcFilePrefix = "Weekly Invoice " + .cTempCoName + " only Dept 0630 "
										CASE .cTempCo == 'K-0630'
											lcFilePrefix = "Weekly Invoice " + .cTempCoName + " all but Dept 0630 "
										OTHERWISE
											lcFilePrefix = "Weekly Invoice " + .cTempCoName
									ENDCASE

									IF .nPeriodEndDayNum = 1 THEN
										ldPayDate = ldEndDate + 5
									ELSE
										ldPayDate = ldEndDate + 6
									ENDIF
									lcFileDate = PADL(MONTH(ldPayDate),2,"0") + "-"  + PADL(DAY(ldPayDate),2,"0") + "-" + PADL(YEAR(ldPayDate),4,"0")
								CASE .cMode = "DIV56"
									lcFilePrefix = "DIV 56 Weekly Time " + .cTempCoName
									IF .nPeriodEndDayNum = 1 THEN
										ldPayDate = ldEndDate + 5
									ELSE
										ldPayDate = ldEndDate + 6
									ENDIF
									lcFileDate = PADL(MONTH(ldPayDate),2,"0") + "-"  + PADL(DAY(ldPayDate),2,"0") + "-" + PADL(YEAR(ldPayDate),4,"0")
								CASE .cMode = "DIV52DAILY"
									lcFilePrefix = "DIV 52 Daily " + .cTempCoName
									lcFileDate = PADL(MONTH(ldStartDate),2,"0") + "-"  + PADL(DAY(ldStartDate),2,"0") + "-" + PADL(YEAR(ldStartDate),4,"0")
								CASE .cMode = "DIV58DAILY"
									lcFilePrefix = "DIV 58 Daily " + .cTempCoName
									lcFileDate = PADL(MONTH(ldStartDate),2,"0") + "-"  + PADL(DAY(ldStartDate),2,"0") + "-" + PADL(YEAR(ldStartDate),4,"0")
								CASE .cMode = "DIV59WEEKLY"
									lcFilePrefix = "DIV 59 Weekly Time " + .cTempCoName
									IF .nPeriodEndDayNum = 1 THEN
										ldPayDate = ldEndDate + 5
									ELSE
										ldPayDate = ldEndDate + 6
									ENDIF
									lcFileDate = PADL(MONTH(ldPayDate),2,"0") + "-"  + PADL(DAY(ldPayDate),2,"0") + "-" + PADL(YEAR(ldPayDate),4,"0")
								CASE .cMode = "DIV59DAILY"
									lcFilePrefix = "DIV 59 Daily " + .cTempCoName
									lcFileDate = PADL(MONTH(ldStartDate),2,"0") + "-"  + PADL(DAY(ldStartDate),2,"0") + "-" + PADL(YEAR(ldStartDate),4,"0")
								CASE .cMode = "DIV57MONTHLY"
									lcFilePrefix = "DIV 57 Monthly " + .cTempCoName
									lcFileDate = PADL(MONTH(ldStartDate),2,"0") + "-"  + PADL(DAY(ldStartDate),2,"0") + "-" + PADL(YEAR(ldStartDate),4,"0")
							ENDCASE

							lcTargetDirectory = "F:\UTIL\KRONOS\TEMPTIMEREPORTS\" && + lcFileDate + "\"

							lcFiletoSaveAs = lcTargetDirectory + lcFilePrefix + " " + lcFileDate
							lcXLFileName = lcFiletoSaveAs + ".XLS"

							lcCorTechRecruitsFile = lcTargetDirectory + "Cor-Tech Recruits" + " " + lcFileDate + ".XLS"
							lcCMPRecruitsFile = lcTargetDirectory + "Comand Staffing Recruits" + " " + lcFileDate + ".XLS"


							* POPULATE PAY FIELD IN CURTEMPS -- NEEDED IF WE PRODUCE A SUMMARY FILE
							* also identify Reg Hours errors (i.e., > 8.0)

							IF .lHourlyNotTemps THEN
								* Replace Kronos Wage with the hourly wage from ADP
								SELECT CURTEMPS
								SCAN
									SELECT CURADP
									LOCATE FOR FILE_NUM = VAL(CURTEMPS.FILE_NUM)
									IF FOUND() THEN
										REPLACE CURTEMPS.WAGE WITH CURADP.RATE
										WAIT WINDOW NOWAIT 'GETTING ADP WAGES FOR: ' + CURTEMPS.FILE_NUM
									ENDIF
								ENDSCAN
								WAIT CLEAR
							ENDIF && .lHourlyNotTemps
							
							
							*******************************************************************************************************
							*******************************************************************************************************
							*******************************************************************************************************
							*******************************************************************************************************
							
							* special processing for when associates work weekends or holidays and we want to force OT or DT
							DO CASE
								CASE .lForceAllHoursToOverTime
									* change any regular hours to OT; but don't change DT hours
									.TrackProgress('==============================================================', LOGIT+SENDIT)
									.TrackProgress('========== FORCING ALL HOURS TO OVERTIME =====================', LOGIT+SENDIT)
									.TrackProgress('==============================================================', LOGIT+SENDIT)
									SELECT CURTEMPS
									SCAN
										lnTotAllHours = CURTEMPS.REG_HRS + CURTEMPS.OT_HRS
										REPLACE CURTEMPS.REG_HRS WITH 0.00, CURTEMPS.OT_HRS WITH lnTotAllHours IN CURTEMPS
									ENDSCAN
								CASE .lForceAllHoursToDoubleTime
									* change all hours to DT
									.TrackProgress('==============================================================', LOGIT+SENDIT)
									.TrackProgress('========== FORCING ALL HOURS TO DOUBLETIME ===================', LOGIT+SENDIT)
									.TrackProgress('==============================================================', LOGIT+SENDIT)
									SELECT CURTEMPS
									SCAN
										lnTotAllHours = CURTEMPS.REG_HRS + CURTEMPS.OT_HRS + CURTEMPS.OT20_HRS
										REPLACE CURTEMPS.REG_HRS WITH 0.00, CURTEMPS.OT_HRS WITH 0.00, CURTEMPS.OT20_HRS WITH lnTotAllHours IN CURTEMPS
									ENDSCAN
								OTHERWISE
									* nothing - continue with normal processing							
							ENDCASE
							
							*******************************************************************************************************
							*******************************************************************************************************
							*******************************************************************************************************
							*******************************************************************************************************
							*******************************************************************************************************
							
							
							
							

							*!*	SELECT CURTEMPS
							*!*	BROWSE
							*!*	THROW
							*SET STEP ON
							lcInvalidRegHours = ''
							SELECT CURTEMPS
							SCAN
								*lnTempMarkupREG = .getTempAgencyMarkupREG(CURTEMPS.AGENCYNUM, CURTEMPS.SHORTNAME, CURTEMPS.PAYDATE)
								lnTempMarkupREG = .oTempAgency.getTempAgencyMarkupREG(CURTEMPS.SHORTNAME, CURTEMPS.PAYDATE, CURTEMPS.FILE_NUM)
								
								*lnTempMarkupOT = .getTempAgencyMarkupOT(CURTEMPS.AGENCYNUM, CURTEMPS.SHORTNAME, CURTEMPS.PAYDATE)
								lnTempMarkupOT = .oTempAgency.getTempAgencyMarkupOT(CURTEMPS.SHORTNAME, CURTEMPS.PAYDATE, CURTEMPS.FILE_NUM)
								
								*lnShiftDiff = .getShiftDiff(CURTEMPS.AGENCYNUM, CURTEMPS.CALCDSHIFT, CURTEMPS.DEPT, CURTEMPS.WAGE)
								lnShiftDiff = .oTempAgency.getShiftDiff(CURTEMPS.CALCDSHIFT)


								*!*	IF .lRoundWageAtEachStep THEN

								*!*		lnWage = (CURTEMPS.WAGE + lnShiftDiff)

								*!*		lnPay = (CURTEMPS.REG_HRS * ROUND(lnWage * lnTempMarkupREG,2)) + ;
								*!*			(CURTEMPS.OT_HRS * ROUND(lnWage * DAILY_OT_MULTIPLIER * lnTempMarkupOT,2)) + ;
								*!*			(CURTEMPS.OT20_HRS * ROUND(lnWage * DAILY_OT20_MULTIPLIER * lnTempMarkupOT,2))
								*!*
								*!*		lnPay = ROUND(lnPay,2)

								*!*	ELSE

								*!*		lnWage = (CURTEMPS.WAGE + lnShiftDiff)

								*!*		lnPay = (CURTEMPS.REG_HRS * lnWage * lnTempMarkupREG) + ;
								*!*			(CURTEMPS.OT_HRS * lnWage * DAILY_OT_MULTIPLIER * lnTempMarkupOT) + ;
								*!*			(CURTEMPS.OT20_HRS * lnWage * DAILY_OT20_MULTIPLIER * lnTempMarkupOT)
								*!*
								*!*	ENDIF  &&  .lRoundWageAtEachStep

								DO CASE
									CASE .nRoundingType = 1  && Standard Rounding

										lnWage = (CURTEMPS.WAGE + lnShiftDiff)

										lnPay = (CURTEMPS.REG_HRS * ROUND(lnWage * lnTempMarkupREG,2)) + ;
											(CURTEMPS.OT_HRS * ROUND(lnWage * DAILY_OT_MULTIPLIER * lnTempMarkupOT,2)) + ;
											(CURTEMPS.OT20_HRS * ROUND(lnWage * DAILY_OT20_MULTIPLIER * lnTempMarkupOT,2))
										
										* add new $ fields
										lnPay = lnPay + CURTEMPS.BONUS_PAY

										lnPay = ROUND(lnPay,2)

									CASE INLIST(.nRoundingType,2)
										* 2 = No rounding
										* DONT round lnpay, which is the daily pay

										lnWage = (CURTEMPS.WAGE + lnShiftDiff)

										lnPay = (CURTEMPS.REG_HRS * lnWage * lnTempMarkupREG) + ;
											(CURTEMPS.OT_HRS * lnWage * DAILY_OT_MULTIPLIER * lnTempMarkupOT) + ;
											(CURTEMPS.OT20_HRS * lnWage * DAILY_OT20_MULTIPLIER * lnTempMarkupOT)
										
										* add new $ fields
										lnPay = lnPay + CURTEMPS.BONUS_PAY

									CASE INLIST(.nRoundingType,3)
										* 3 = SELECT rounding - round product of base wage and agency markup and ot multiplier to nearest cent
										* DONT round lnpay, which is the daily pay

										lnWage = (CURTEMPS.WAGE + lnShiftDiff)

										lnPay = (CURTEMPS.REG_HRS * ROUND(lnWage * lnTempMarkupREG,2)) + ;
											(CURTEMPS.OT_HRS * ROUND(lnWage * DAILY_OT_MULTIPLIER * lnTempMarkupOT,2)) + ;
											(CURTEMPS.OT20_HRS * ROUND(lnWage * DAILY_OT20_MULTIPLIER * lnTempMarkupOT,2))
										
										* add new $ fields
										lnPay = lnPay + CURTEMPS.BONUS_PAY

									OTHERWISE
										* NOTHING
								ENDCASE


								REPLACE CURTEMPS.PAY WITH lnPay IN CURTEMPS

								* IF A CA TEMP AGENCY, FLAG REG HRS OVER 8 BECAUSE THAT SHOULD NOT BE POSSIBLE
								* OTHERWISE FLAG REG HRS OVER 24...
								IF NOT .lHourlyNotTemps THEN
									DO CASE
										CASE INLIST(.cTempCo,'R','T','U','V')  && CA TEMP AGENCIES - SSG SP, ML & CARSONS
											IF (CURTEMPS.REG_HRS > 8.0) THEN
												* Kronos is confused, probably by a recycled temp with invalid hire date/active date
												lcInvalidRegHours = lcInvalidRegHours +  LEFT(CURTEMPS.EMPLOYEE,25) + " #: " + CURTEMPS.FILE_NUM + ;
													"     Invalid Reg. Hrs. = " + TRANSFORM(CURTEMPS.REG_HRS) + CRLF
											ENDIF
										OTHERWISE
											IF (CURTEMPS.REG_HRS > 24.0) THEN
												* Kronos is confused, probably by a recycled temp with invalid hire date/active date
												lcInvalidRegHours = lcInvalidRegHours +  LEFT(CURTEMPS.EMPLOYEE,25) + " #: " + CURTEMPS.FILE_NUM + ;
													"     Invalid Reg. Hrs. = " + TRANSFORM(CURTEMPS.REG_HRS) + CRLF
											ENDIF
									ENDCASE
								ENDIF

								* identify Reg Hours errors (i.e., > 8.0)
								*IF CURTEMPS.REG_HRS > 8.0 THEN
								*IF (CURTEMPS.REG_HRS > 8.0) AND (NOT .lHourlyNotTemps) THEN
								* MUST EXCLUDE TEMP CO 'D' = QUALITY NJ FROM THIS CHECK BECAUSE IN NJ YOU CAN GET REG HRS > 8 EACH DAY
								*!*	IF (CURTEMPS.REG_HRS > 8.0) AND (NOT .lHourlyNotTemps) AND (NOT INLIST(.cTempCo,'D','J')) THEN
								*!*		* Kronos is confused, probably by a recycled temp with invalid hire date/active date
								*!*		lcInvalidRegHours = lcInvalidRegHours +  LEFT(CURTEMPS.EMPLOYEE,25) + " #: " + CURTEMPS.FILE_NUM + ;
								*!*			"     Invalid Reg. Hrs. = " + TRANSFORM(CURTEMPS.REG_HRS) + CRLF
								*!*	ENDIF

							ENDSCAN
							IF NOT EMPTY(lcInvalidRegHours) THEN
								lcInvalidRegHours = "WARNING: The following Temps had an invalid # of Reg. Hrs. in the time period. " + CRLF + ;
									"This is usually caused by an invalid Hire Date, or having an Active Date not " + CRLF + ;
									"far back enough in time. " + CRLF + ;
									"Contact Mark Bennett for assistance in correcting the problem." + CRLF + ;
									"------------------------------------------------------------------------------" + CRLF + ;
									lcInvalidRegHours
							ENDIF

							*!*	IF .lTestMode THEN
							*!*		SELECT CURTEMPS
							*!*		COPY TO C:\A\TEMPDETAIL.XLS XL5
							*!*		THROW
							*!*	ENDIF



							IF .cMode <> "SUMMARYONLY" THEN
								* create primary, detail spreadsheet

								WAIT WINDOW NOWAIT "Opening Excel..."
								oExcel = CREATEOBJECT("excel.application")
								oExcel.VISIBLE = .F.
								oWorkbook = oExcel.workbooks.OPEN("F:\UTIL\KRONOS\TEMPLATES\KronosTempTimeReportBYDEPTBYDAY5.XLS")

								* if target directory exists, save there
								llSaveAgain = .F.
								IF DIRECTORY(lcTargetDirectory) THEN
									IF FILE(lcXLFileName) THEN
										DELETE FILE (lcXLFileName)
									ENDIF
									oWorkbook.SAVEAS(lcFiletoSaveAs)
									* set flag to save again after sheet is populated
									llSaveAgain = .T.
								ENDIF
								***********************************************************************************************************************
								***********************************************************************************************************************
								WAIT WINDOW NOWAIT "Building Time spreadsheet..."

								lnRow = 3
								lnStartRow = lnRow + 1
								lcStartRow = ALLTRIM(STR(lnStartRow))

								oWorksheet = oWorkbook.Worksheets[1]
								oWorksheet.RANGE("A" + lcStartRow,"S10000").clearcontents()

								oWorksheet.RANGE("A" + lcStartRow,"S10000").FONT.SIZE = 10
								oWorksheet.RANGE("A" + lcStartRow,"S10000").FONT.NAME = "Arial"
								oWorksheet.RANGE("A" + lcStartRow,"S10000").FONT.bold = .F.

								DO CASE
										*CASE .cMode = "DAILY"
									CASE INLIST(.cMode, "DAILY", "ZDAILY")
										lcTitle = "Time for " + .cTempCoName + LF + " for Date: " + lcStartDate
										*CASE .cMode = "WEEKLY"
									CASE INLIST(.cMode, "WEEKLY", "ZWEEKLY")
										lcTitle = "Time for " + .cTempCoName + LF + ;
											" for Pay Date: " + DTOC(ldPayDate) + LF + ;
											" Week of: " + lcStartDate + " - " + lcEndDate
									CASE .cMode = "DATERANGE"
										lcTitle = "Time for " + .cTempCoName + LF + ;
											" For Date Range " + lcStartDate + " - " + lcEndDate
									CASE .cMode = "INVOICE"
										DO CASE
											CASE .cTempCo == 'K+0630'
												lcTitle = "Invoice for " + .cTempCoName + LF + ;
													" Only Dept 0630, for Pay Date: " + DTOC(ldPayDate) + LF + ;
													" Week of: " + lcStartDate + " - " + lcEndDate
											CASE .cTempCo == 'K-0630'
												lcTitle = "Invoice for " + .cTempCoName + LF + ;
													" All but Dept 0630, for Pay Date: " + DTOC(ldPayDate) + LF + ;
													" Week of: " + lcStartDate + " - " + lcEndDate
											OTHERWISE
												lcTitle = "Invoice for " + .cTempCoName + LF + ;
													" for Pay Date: " + DTOC(ldPayDate) + LF + ;
													" Week of: " + lcStartDate + " - " + lcEndDate
										ENDCASE
									CASE .cMode = "DIV56"
										lcTitle = "DIV 56 Time for " + .cTempCoName + LF + ;
											" for Pay Date: " + DTOC(ldPayDate) + LF + ;
											" Week of: " + lcStartDate + " - " + lcEndDate
									CASE .cMode = "DIV52DAILY"
										lcTitle = "DIV 52 Time for " + .cTempCoName + LF + " for Date: " + lcStartDate
									CASE .cMode = "DIV58DAILY"
										lcTitle = "DIV 58 Time for " + .cTempCoName + LF + " for Date: " + lcStartDate
									CASE .cMode = "DIV59WEEKLY"
										lcTitle = "DIV 59 Time for " + .cTempCoName + LF + ;
											" for Pay Date: " + DTOC(ldPayDate) + LF + ;
											" Week of: " + lcStartDate + " - " + lcEndDate
									CASE .cMode = "DIV59DAILY"
										lcTitle = "DIV 59 Time for " + .cTempCoName + LF + " for Date: " + lcStartDate
									CASE .cMode = "DIV57MONTHLY"
										lcTitle = "DIV 57 Time for " + .cTempCoName + LF + " for Month of: " + lcStartDate + " - " + lcEndDate
								ENDCASE

								IF USED('CURSIGNOFF') THEN
									USE IN CURSIGNOFF
								ENDIF

								CREATE CURSOR CURSIGNOFF (DIVISION C(2), CLIENT C(20), DEPT C(4), DEPTNAME C(30), AMOUNT Y)

								oWorksheet.RANGE("A1").VALUE = lcTitle

								* main scan/processing
								SELECT CURTEMPS
								LOCATE

								* init for break logic
								lcPrevFileNum = CURTEMPS.FILE_NUM
								lcPrevDept = CURTEMPS.DEPT
								lcPrevDivision = CURTEMPS.DIVISION
								lnPrevFileNumStartRow = lnRow + 1
								lcPrevFileNumStartRow = LTRIM(STR(lnPrevFileNumStartRow))
								lnPrevFileNumEndRow = lnPrevFileNumStartRow
								lcPrevFileNumEndRow = LTRIM(STR(lnPrevFileNumEndRow))
								lnPrevDeptStartRow = lnRow + 1
								lcPrevDeptStartRow = LTRIM(STR(lnPrevDeptStartRow))
								lnPrevDeptEndRow = lnPrevDeptStartRow
								lcPrevDeptEndRow = LTRIM(STR(lnPrevDeptEndRow))
								lnPrevDivisionStartRow = lnRow + 1
								lcPrevDivisionStartRow = LTRIM(STR(lnPrevDivisionStartRow))
								lnPrevDivisionEndRow = lnPrevDivisionStartRow
								lcPrevDivisionEndRow = LTRIM(STR(lnPrevDivisionEndRow))

								STORE 0 TO num_salary
								SCAN
									DO CASE
										CASE (CURTEMPS.DIVISION != lcPrevDivision) OR (CURTEMPS.DEPT != lcPrevDept) OR (CURTEMPS.FILE_NUM != lcPrevFileNum)
											DO CASE
												CASE (CURTEMPS.DIVISION != lcPrevDivision)
													* do all 3 breaks
													lnPrevDivisionEndRow = lnRow
													lcPrevDivisionEndRow = LTRIM(STR(lnPrevDivisionEndRow))
													lnPrevDeptEndRow = lnRow
													lcPrevDeptEndRow = LTRIM(STR(lnPrevDeptEndRow))
													lnPrevFileNumEndRow = lnRow
													lcPrevFileNumEndRow = LTRIM(STR(lnPrevFileNumEndRow))
													oWorksheet.RANGE("Q" + lcRow).formula = "=sum(P" + lcPrevFileNumStartRow + ":P" + lcPrevFileNumEndRow + ")"

													lnRow = lnRow + 1
													lcRow = LTRIM(STR(lnRow))
													oWorksheet.RANGE("A" + lcRow,"R" + lcRow).Interior.ColorIndex = 15
													lnRow = lnRow + 1
													lcRow = LTRIM(STR(lnRow))

													*oWorksheet.RANGE("A" + lcRow).VALUE = "Totals for Division + + " Dept:  " + ALLTRIM(lcPrevDept) + " " + .GetDepartmentName(lcPrevDept)
													oWorksheet.RANGE("A" + lcRow).VALUE = "Totals for Division: " + ALLTRIM(lcPrevDivision) + " Dept:  " + ALLTRIM(lcPrevDept) + " " + .GetDepartmentName(lcPrevDept)

													oWorksheet.RANGE("R" + lcRow).formula = "=sum(P" + lcPrevDeptStartRow + ":P" + lcPrevDeptEndRow + ")"
													oWorksheet.RANGE("A" + lcRow,"R" + lcRow).FONT.bold = .T.
													
													************************************************************************************
													* new division/client stuff
													lcDivisionName = .GetDivisionName(LEFT(lcPrevDivision,2))
													lcClientName = ""
													IF NOT EMPTY(RIGHT(lcPrevDivision,1)) THEN
														lcClientName = .GetClientName(RIGHT(lcPrevDivision,1))
													ENDIF
													************************************************************************************

													INSERT INTO CURSIGNOFF (DIVISION, CLIENT, DEPT, DEPTNAME, AMOUNT) VALUES (lcPrevDivision, lcClientName, lcPrevDept, .GetDepartmentName(lcPrevDept), oWorksheet.RANGE("R" + lcRow).VALUE)

													lnRow = lnRow + 1
													lcRow = LTRIM(STR(lnRow))
													oWorksheet.RANGE("A" + lcRow,"S" + lcRow).Interior.ColorIndex = 15
													lnRow = lnRow + 1
													lcRow = LTRIM(STR(lnRow))
													
													*oWorksheet.RANGE("A" + lcRow).VALUE = "Totals for Division:  " + ALLTRIM(lcPrevDivision) + " " + .GetDivisionName(lcPrevDivision)
													oWorksheet.RANGE("A" + lcRow).VALUE = "Totals for Division: " + ALLTRIM(lcPrevDivision) + " " + .GetDivisionName(LEFT(lcPrevDivision,2)) + IIF(EMPTY(lcClientName),"",", Client = " + lcClientName)
													oWorksheet.RANGE("S" + lcRow).formula = "=sum(P" + lcPrevDivisionStartRow + ":P" + lcPrevDivisionEndRow + ")"
													oWorksheet.RANGE("A" + lcRow,"S" + lcRow).FONT.bold = .T.

													lnRow = lnRow + 2
													lnPrevDivisionStartRow = lnRow
													lcPrevDivisionStartRow = LTRIM(STR(lnPrevDivisionStartRow))
													lnPrevDeptStartRow = lnRow
													lcPrevDeptStartRow = LTRIM(STR(lnPrevDeptStartRow))
													lnPrevFileNumStartRow = lnRow
													lcPrevFileNumStartRow = LTRIM(STR(lnPrevDivisionStartRow))
												OTHERWISE
													DO CASE
														CASE (CURTEMPS.DEPT != lcPrevDept)
															* do dept & filenum breaks
															lnPrevDeptEndRow = lnRow
															lcPrevDeptEndRow = LTRIM(STR(lnPrevDeptEndRow))
															lnPrevFileNumEndRow = lnRow
															lcPrevFileNumEndRow = LTRIM(STR(lnPrevFileNumEndRow))
															oWorksheet.RANGE("Q" + lcRow).formula = "=sum(P" + lcPrevFileNumStartRow + ":P" + lcPrevFileNumEndRow + ")"

															lnRow = lnRow + 1
															lcRow = LTRIM(STR(lnRow))
															oWorksheet.RANGE("A" + lcRow,"R" + lcRow).Interior.ColorIndex = 15
															lnRow = lnRow + 1
															lcRow = LTRIM(STR(lnRow))

															*oWorksheet.RANGE("A" + lcRow).VALUE = "Totals for Dept: " + ALLTRIM(lcPrevDept) + " " + .GetDepartmentName(lcPrevDept)
															oWorksheet.RANGE("A" + lcRow).VALUE = "Totals for Division: " + ALLTRIM(lcPrevDivision) + " Dept:  " + ALLTRIM(lcPrevDept) + " " + .GetDepartmentName(lcPrevDept)

															oWorksheet.RANGE("R" + lcRow).formula = "=sum(P" + lcPrevDeptStartRow + ":P" + lcPrevDeptEndRow + ")"
															oWorksheet.RANGE("A" + lcRow,"R" + lcRow).FONT.bold = .T.
													
															************************************************************************************
															* new division/client stuff
															lcDivisionName = .GetDivisionName(LEFT(lcPrevDivision,2))
															lcClientName = ""
															IF NOT EMPTY(RIGHT(lcPrevDivision,1)) THEN
																lcClientName = .GetClientName(RIGHT(lcPrevDivision,1))
															ENDIF
															************************************************************************************

															INSERT INTO CURSIGNOFF (DIVISION, CLIENT, DEPT, DEPTNAME, AMOUNT) VALUES (lcPrevDivision, lcClientName, lcPrevDept, .GetDepartmentName(lcPrevDept), oWorksheet.RANGE("R" + lcRow).VALUE)

															*INSERT INTO CURSIGNOFF (DIVISION, DEPT, DEPTNAME, AMOUNT) VALUES (lcPrevDivision, lcPrevDept, .GetDepartmentName(lcPrevDept), oWorksheet.RANGE("O" + lcRow).VALUE)

															lnRow = lnRow + 2
															lnPrevDeptStartRow = lnRow
															lcPrevDeptStartRow = LTRIM(STR(lnPrevDeptStartRow))
															lnPrevFileNumStartRow = lnRow
															lcPrevFileNumStartRow = LTRIM(STR(lnPrevFileNumStartRow))
														OTHERWISE
															* do filenum break
															lnPrevFileNumEndRow = lnRow
															lcPrevFileNumEndRow = LTRIM(STR(lnPrevFileNumEndRow))
															oWorksheet.RANGE("Q" + lcRow).formula = "=sum(P" + lcPrevFileNumStartRow + ":P" + lcPrevFileNumEndRow + ")"

															lnRow = lnRow + 2
															lnPrevFileNumStartRow = lnRow
															lcPrevFileNumStartRow = LTRIM(STR(lnPrevFileNumStartRow))
													ENDCASE
											ENDCASE
										OTHERWISE
											* NO CHANGES; just advance a line
											lnRow = lnRow + 1
									ENDCASE

									* calculate temp agency % markup
									*lnTempMarkupREG = .getTempAgencyMarkupREG(CURTEMPS.AGENCYNUM, CURTEMPS.SHORTNAME, CURTEMPS.PAYDATE)
									lnTempMarkupREG = .oTempAgency.getTempAgencyMarkupREG(CURTEMPS.SHORTNAME, CURTEMPS.PAYDATE, CURTEMPS.FILE_NUM)
									
									*lnTempMarkupOT = .getTempAgencyMarkupOT(CURTEMPS.AGENCYNUM, CURTEMPS.SHORTNAME, CURTEMPS.PAYDATE)
									lnTempMarkupOT = .oTempAgency.getTempAgencyMarkupOT(CURTEMPS.SHORTNAME, CURTEMPS.PAYDATE, CURTEMPS.FILE_NUM)

									*lnShiftDiff = .getShiftDiff(CURTEMPS.AGENCYNUM, CURTEMPS.CALCDSHIFT, CURTEMPS.DEPT, CURTEMPS.WAGE)
									lnShiftDiff = .oTempAgency.getShiftDiff(CURTEMPS.CALCDSHIFT)
									
									lcRow = LTRIM(STR(lnRow))
									IF .lHourlyNotTemps THEN
										oWorksheet.RANGE("A" + lcRow).VALUE = 'N/A'
									ELSE
										oWorksheet.RANGE("A" + lcRow).VALUE = CURTEMPS.AGENCY
									ENDIF
									oWorksheet.RANGE("B" + lcRow).VALUE = "'" + ALLTRIM(CURTEMPS.DIVISION)

									oWorksheet.RANGE("C" + lcRow).VALUE = "'" + ALLTRIM(CURTEMPS.DEPT)
									oWorksheet.RANGE("D" + lcRow).VALUE = CURTEMPS.EMPLOYEE
									oWorksheet.RANGE("E" + lcRow).VALUE = CURTEMPS.FILE_NUM
									oWorksheet.RANGE("F" + lcRow).VALUE = CURTEMPS.PAYDATE
									*oWorksheet.RANGE("G" + lcRow).VALUE = "'" + ALLTRIM(CURTEMPS.SHIFT)
									oWorksheet.RANGE("G" + lcRow).VALUE = "'" + ALLTRIM(CURTEMPS.CALCDSHIFT)
									oWorksheet.RANGE("H" + lcRow).VALUE = CURTEMPS.REG_HRS
									oWorksheet.RANGE("I" + lcRow).VALUE = CURTEMPS.OT_HRS
									oWorksheet.RANGE("J" + lcRow).VALUE = CURTEMPS.OT20_HRS
									
									*oWorksheet.RANGE("K" + lcRow).VALUE = CURTEMPS.?    SICK HOURS
									oWorksheet.RANGE("L" + lcRow).VALUE = CURTEMPS.BONUS_PAY
									oWorksheet.RANGE("M" + lcRow).VALUE = 0.00   && CURTEMPS.RETRO PAY

									IF .lShowDollars THEN
										oWorksheet.RANGE("N" + lcRow).VALUE = CURTEMPS.WAGE
									ELSE
										oWorksheet.RANGE("N" + lcRow).VALUE = 0.00
									ENDIF

									IF .lShowDollars THEN
										oWorksheet.RANGE("O" + lcRow).formula = ;
											"=(" + ALLTRIM(TRANSFORM(CURTEMPS.WAGE,'999.99')) + "+" + ;
											ALLTRIM(TRANSFORM(lnShiftDiff,'9.99')) + ")"
									ELSE
										oWorksheet.RANGE("O" + lcRow).VALUE = 0.00
									ENDIF

									*!*	IF .lRoundWageAtEachStep THEN

									*!*		* note final rounding so there are no fractions of cents in result
									*!*		oWorksheet.RANGE("M" + lcRow).formula = ;
									*!*			"=ROUND((H" + lcRow + "*ROUND(L" + lcRow  + "*" + ;
									*!*			ALLTRIM(TRANSFORM(lnTempMarkupREG,'@R 9.9999')) + ",2))+(I" + ;
									*!*			lcRow + "*ROUND(L" + lcRow + "*" + ALLTRIM(TRANSFORM(DAILY_OT_MULTIPLIER,'@R 9.99')) + "*" + ;
									*!*			ALLTRIM(TRANSFORM(lnTempMarkupOT,'@R 9.9999')) + ",2))+(J" + ;
									*!*			lcRow + "*ROUND(L" + lcRow + "*" + ALLTRIM(TRANSFORM(DAILY_OT20_MULTIPLIER,'@R 9.99')) + "*" + ;
									*!*			ALLTRIM(TRANSFORM(lnTempMarkupOT,'@R 9.9999')) + ",2)),2)"
									*!*
									*!*	ELSE

									*!*		oWorksheet.RANGE("M" + lcRow).formula = ;
									*!*			"=ROUND((H" + lcRow + "*L" + lcRow  + "*" + ;
									*!*			ALLTRIM(TRANSFORM(lnTempMarkupREG,'@R 9.9999')) + ")+(I" + ;
									*!*			lcRow + "*L" + lcRow + "*" + ALLTRIM(TRANSFORM(DAILY_OT_MULTIPLIER,'@R 9.99')) + "*" + ;
									*!*			ALLTRIM(TRANSFORM(lnTempMarkupOT,'@R 9.9999')) + ")+(J" + ;
									*!*			lcRow + "*L" + lcRow + "*" + ALLTRIM(TRANSFORM(DAILY_OT20_MULTIPLIER,'@R 9.99')) + "*" + ;
									*!*			ALLTRIM(TRANSFORM(lnTempMarkupOT,'@R 9.9999')) + "),2)"
									*!*
									*!*	ENDIF  && .lRoundWageAtEachStep

									DO CASE
										CASE .nRoundingType = 1  && Standard Rounding

											* note final rounding so there are no fractions of cents in result
											oWorksheet.RANGE("P" + lcRow).formula = ;
												"=ROUND((L" + lcRow + "+M" + lcRow + ") + (H" + lcRow + "*ROUND(O" + lcRow  + "*" + ;
												ALLTRIM(TRANSFORM(lnTempMarkupREG,'@R 9.99999')) + ",2))+(I" + ;
												lcRow + "*ROUND(O" + lcRow + "*" + ALLTRIM(TRANSFORM(DAILY_OT_MULTIPLIER,'@R 9.99')) + "*" + ;
												ALLTRIM(TRANSFORM(lnTempMarkupOT,'@R 9.99999')) + ",2))+(J" + ;
												lcRow + "*ROUND(O" + lcRow + "*" + ALLTRIM(TRANSFORM(DAILY_OT20_MULTIPLIER,'@R 9.99')) + "*" + ;
												ALLTRIM(TRANSFORM(lnTempMarkupOT,'@R 9.99999')) + ",2)),2)"

										CASE INLIST(.nRoundingType,2)
											* 2 = No rounding;

											oWorksheet.RANGE("P" + lcRow).formula = ;
												"=(L" + lcRow + "+M" + lcRow + ") + (H" + lcRow + "*O" + lcRow  + "*" + ;
												ALLTRIM(TRANSFORM(lnTempMarkupREG,'@R 9.99999')) + ")+(I" + ;
												lcRow + "*O" + lcRow + "*" + ALLTRIM(TRANSFORM(DAILY_OT_MULTIPLIER,'@R 9.99')) + "*" + ;
												ALLTRIM(TRANSFORM(lnTempMarkupOT,'@R 9.99999')) + ")+(J" + ;
												lcRow + "*O" + lcRow + "*" + ALLTRIM(TRANSFORM(DAILY_OT20_MULTIPLIER,'@R 9.99')) + "*" + ;
												ALLTRIM(TRANSFORM(lnTempMarkupOT,'@R 9.99999')) + ")"

										CASE INLIST(.nRoundingType,3)
											* 3 = SELECT rounding - round product of base wage and agency markup and ot multiplier to nearest cent
											* do not round the result of the entire formula

											oWorksheet.RANGE("P" + lcRow).formula = ;
												"=(L" + lcRow + "+M" + lcRow + ") + (H" + lcRow + "*ROUND(O" + lcRow  + "*" + ;
												ALLTRIM(TRANSFORM(lnTempMarkupREG,'@R 9.99999')) + ",2))+(I" + ;
												lcRow + "*ROUND(O" + lcRow + "*" + ALLTRIM(TRANSFORM(DAILY_OT_MULTIPLIER,'@R 9.99')) + "*" + ;
												ALLTRIM(TRANSFORM(lnTempMarkupOT,'@R 9.99999')) + ",2))+(J" + ;
												lcRow + "*ROUND(O" + lcRow + "*" + ALLTRIM(TRANSFORM(DAILY_OT20_MULTIPLIER,'@R 9.99')) + "*" + ;
												ALLTRIM(TRANSFORM(lnTempMarkupOT,'@R 9.99999')) + ",2))"

										OTHERWISE
											* NOTHING
									ENDCASE



									*oWorksheet.RANGE("A" + lcRow,"K" + lcRow).Interior.ColorIndex = IIF(((lnRow % 2) = 1),15,0)

									* highlight invalid division
									IF NOT .oTempAgency.IsDivisionValid( ALLTRIM(CURTEMPS.DIVISION) ) THEN
										oWorksheet.RANGE("B" + lcRow).FONT.ColorIndex = 3
										oWorksheet.RANGE("B" + lcRow).FONT.bold = .T.
									ENDIF

									* highlight invalid department
									IF (ALLTRIM(CURTEMPS.DEPT) == '0000') THEN
										oWorksheet.RANGE("C" + lcRow).FONT.ColorIndex = 3
										oWorksheet.RANGE("C" + lcRow).FONT.bold = .T.
									ENDIF
									
									* HIGHLIGHT BONUS $
									IF CURTEMPS.BONUS_PAY > 0 THEN
										oWorksheet.RANGE("L" + lcRow).FONT.ColorIndex = 5  && DARK BLUE
										oWorksheet.RANGE("L" + lcRow).FONT.bold = .T.
									ENDIF

									* highlight hours if hours for shift were outside expected bounds (per Temp agency)
									lnTotHours = CURTEMPS.REG_HRS + CURTEMPS.OT_HRS + CURTEMPS.OT20_HRS
									IF (lnTotHours < .nMinHoursTest) OR (lnTotHours > .nMaxHoursTest) THEN
										oWorksheet.RANGE("H" + lcRow,"J" + lcRow).FONT.ColorIndex = 3
										oWorksheet.RANGE("H" + lcRow,"J" + lcRow).FONT.bold = .T.
									ENDIF

									* highlight adjusted wage if it is zero
									IF (CURTEMPS.WAGE = 0.00) THEN
										oWorksheet.RANGE("K" + lcRow).FONT.ColorIndex = 3
										oWorksheet.RANGE("K" + lcRow).FONT.bold = .T.
									ENDIF

									lcPrevFileNum = CURTEMPS.FILE_NUM
									lcPrevDept = CURTEMPS.DEPT
									lcPrevDivision = CURTEMPS.DIVISION

								ENDSCAN

								* final break processing
								lnPrevDivisionEndRow = lnRow
								lcPrevDivisionEndRow = LTRIM(STR(lnPrevDivisionEndRow))
								lnPrevDeptEndRow = lnRow
								lcPrevDeptEndRow = LTRIM(STR(lnPrevDeptEndRow))
								lnPrevFileNumEndRow = lnRow
								lcPrevFileNumEndRow = LTRIM(STR(lnPrevFileNumEndRow))
								oWorksheet.RANGE("Q" + lcRow).formula = "=sum(P" + lcPrevFileNumStartRow + ":P" + lcPrevFileNumEndRow + ")"

								lnRow = lnRow + 1
								lcRow = LTRIM(STR(lnRow))
								oWorksheet.RANGE("A" + lcRow,"R" + lcRow).Interior.ColorIndex = 15
								lnRow = lnRow + 1
								lcRow = LTRIM(STR(lnRow))

								*oWorksheet.RANGE("A" + lcRow).VALUE = "Totals for Dept:  " + ALLTRIM(lcPrevDept) + " " + .GetDepartmentName(lcPrevDept)
								oWorksheet.RANGE("A" + lcRow).VALUE = "Totals for Division: " + ALLTRIM(lcPrevDivision) + " Dept:  " + ALLTRIM(lcPrevDept) + " " + .GetDepartmentName(lcPrevDept)

								oWorksheet.RANGE("R" + lcRow).formula = "=sum(P" + lcPrevDeptStartRow + ":P" + lcPrevDeptEndRow + ")"
								oWorksheet.RANGE("A" + lcRow,"R" + lcRow).FONT.bold = .T.
								
								************************************************************************************
								* new division/client stuff
								lcDivisionName = .GetDivisionName(LEFT(lcPrevDivision,2))
								lcClientName = ""
								IF NOT EMPTY(RIGHT(lcPrevDivision,1)) THEN
									lcClientName = .GetClientName(RIGHT(lcPrevDivision,1))
								ENDIF
								************************************************************************************

								INSERT INTO CURSIGNOFF (DIVISION, CLIENT, DEPT, DEPTNAME, AMOUNT) VALUES (lcPrevDivision, lcClientName, lcPrevDept, .GetDepartmentName(lcPrevDept), oWorksheet.RANGE("R" + lcRow).VALUE)

								*INSERT INTO CURSIGNOFF (DIVISION, DEPT, DEPTNAME, AMOUNT) VALUES (lcPrevDivision, lcPrevDept, .GetDepartmentName(lcPrevDept), oWorksheet.RANGE("O" + lcRow).VALUE)

								lnRow = lnRow + 1
								lcRow = LTRIM(STR(lnRow))
								oWorksheet.RANGE("A" + lcRow,"S" + lcRow).Interior.ColorIndex = 15
								lnRow = lnRow + 1
								lcRow = LTRIM(STR(lnRow))
								*oWorksheet.RANGE("A" + lcRow).VALUE = "Totals for Division:  " + ALLTRIM(lcPrevDivision) + " " + .GetDivisionName(lcPrevDivision)
								oWorksheet.RANGE("A" + lcRow).VALUE = "Totals for Division:  " + ALLTRIM(lcPrevDivision) + " " + .GetDivisionName(LEFT(lcPrevDivision,2)) + IIF(EMPTY(lcClientName),"",", Client = " + lcClientName)
								oWorksheet.RANGE("S" + lcRow).formula = "=sum(P" + lcPrevDivisionStartRow + ":P" + lcPrevDivisionEndRow + ")"
								oWorksheet.RANGE("A" + lcRow,"S" + lcRow).FONT.bold = .T.

								*!*								lnPrevFileNumStartRow = lnRow + 1
								*!*								lcPrevFileNumStartRow = LTRIM(STR(lnPrevFileNumStartRow))
								*!*								lnPrevDeptStartRow = lnRow + 1
								*!*								lcPrevDeptStartRow = LTRIM(STR(lnPrevDeptStartRow))
								*!*								lnPrevDivisionStartRow = lnRow + 1
								*!*								lcPrevDivisionStartRow = LTRIM(STR(lnPrevDivisionStartRow))

								lnEndRow = lnRow
								lcEndRow = ALLTRIM(STR(lnEndRow))

								lnTotalsRow = lnRow + 2
								lcRow = LTRIM(STR(lnTotalsRow))

								*oWorksheet.RANGE("A" + lcRow,"Y" + lcRow).FONT.SIZE = 9
								oWorksheet.RANGE("A" + lcRow).VALUE = "Totals:"

								oWorksheet.RANGE("H" + lcRow).formula = "=sum(H" + lcStartRow + ":H" + lcEndRow + ")"
								oWorksheet.RANGE("I" + lcRow).formula = "=sum(I" + lcStartRow + ":I" + lcEndRow + ")"
								oWorksheet.RANGE("J" + lcRow).formula = "=sum(J" + lcStartRow + ":J" + lcEndRow + ")"

								*oWorksheet.RANGE("K" + lcRow).formula = "=sum(K" + lcStartRow + ":K" + lcEndRow + ")"
								oWorksheet.RANGE("L" + lcRow).formula = "=sum(L" + lcStartRow + ":L" + lcEndRow + ")"
								*oWorksheet.RANGE("M" + lcRow).formula = "=sum(M" + lcStartRow + ":M" + lcEndRow + ")"
								
								oWorksheet.RANGE("P" + lcRow).formula = "=sum(P" + lcStartRow + ":P" + lcEndRow + ")"
								oWorksheet.RANGE("Q" + lcRow).formula = "=sum(Q" + lcStartRow + ":Q" + lcEndRow + ")"
								oWorksheet.RANGE("R" + lcRow).formula = "=sum(R" + lcStartRow + ":R" + lcEndRow + ")"
								oWorksheet.RANGE("S" + lcRow).formula = "=sum(S" + lcStartRow + ":S" + lcEndRow + ")"

								oWorksheet.RANGE("A" + lcRow,"S" + lcRow).FONT.bold = .T.

								* Save row #S
								.nTotalsRow = lnTotalsRow

								* save Grand Total $ value
								.nGrandTotalSheetDollar = ROUND(oWorksheet.RANGE("S" + lcRow).VALUE,2)

*!*									* we are seeing Excel ROUND() not working exactly as expected, not matching foxPro round(). For that reason we will use FoxPro's and
*!*									* put the ROUNDED value back into the cell, replacing the formula.
*!*									IF INLIST(.cTempCo,'B') AND INLIST(.cMode,"INVOICE") THEN
*!*										* ROUND THE GRANT TOTAL
*!*										oWorksheet.RANGE("P" + lcRow).formula = ""
*!*										oWorksheet.RANGE("P" + lcRow).VALUE = .nGrandTotalSheetDollar
*!*									ENDIF


								lnRow = lnRow + 3
								lcRow = LTRIM(STR(lnRow))


								oWorksheet.PageSetup.PrintArea = "$A$"  + lcStartRow + ":$S$" + lcRow
								
*!*									* ADD A FORMULA TO DISPLAY BONUS $ FROM BONUS TAB
*!*									IF INLIST(.cTempCo,'R') AND INLIST(.cMode,"INVOICE") THEN
*!*										oWorksheet.RANGE("P" + lcRow).formula = ""
*!*										oWorksheet.RANGE("P" + lcRow).VALUE = .nGrandTotalSheetDollar
*!*									ENDIF

*!*	* 								* ADD DATE RANGE TO BONUS TAB
*!*									oWorksheet = oWorkbook.Worksheets[3]
*!*									oWorksheet.RANGE("E1").VALUE = lcStartDate + " - " + lcEndDate
								

								*******************************************************************************************
								*******************************************************************************************



								IF INLIST(.cTempCo,'D','G') AND INLIST(.cMode,"INVOICE","DATERANGE","DATERANGE-INVOICE") THEN
									** create a summary sign-off sheet on tab 2
									*!*	SELECT CURSIGNOFF
									*!*	BROWSE
									lnStartRow = 9
									lcStartRow = LTRIM(STR(lnStartRow))

									oWorksheet = oWorkbook.Worksheets[2]

									oWorksheet.RANGE("D5").VALUE = lcStartDate + " - " + lcEndDate

									DO CASE
										*CASE .cTempCo = 'D'
										CASE INLIST(.cTempCo,'D','G')
											oWorksheet.RANGE("A1").VALUE = 'Sign Off Sheet for ' + .cTempCoName
											oWorksheet.RANGE("A2").VALUE = 'Carteret Invoice'
											oWorksheet.RANGE("G5").VALUE = ' '
											oWorksheet.RANGE("G10").VALUE = 'Jim Lake'
											oWorksheet.RANGE("G15").VALUE = 'Bill Marsh'
											oWorksheet.RANGE("G20").VALUE = .cTempCoName + ' On-Site'
										OTHERWISE
											* NOTHING
									ENDCASE


									oWorksheet.RANGE("A" + lcStartRow,"E100").clearcontents()
									oWorksheet.RANGE("A" + lcStartRow,"E100").FONT.SIZE = 10
									oWorksheet.RANGE("A" + lcStartRow,"E100").FONT.NAME = "Arial"
									oWorksheet.RANGE("A" + lcStartRow,"E100").FONT.bold = .F.

									lnRow = lnStartRow
									SELECT CURSIGNOFF
									SCAN
										lcRow = LTRIM(STR(lnRow))
										oWorksheet.RANGE("A" + lcRow).VALUE = CURSIGNOFF.DIVISION
										oWorksheet.RANGE("B" + lcRow).VALUE = CURSIGNOFF.DEPT
										oWorksheet.RANGE("C" + lcRow).VALUE = CURSIGNOFF.DEPT
										oWorksheet.RANGE("D" + lcRow).VALUE = CURSIGNOFF.DEPTNAME
										oWorksheet.RANGE("E" + lcRow).VALUE = CURSIGNOFF.AMOUNT
										lnEndRow =  lnRow
										lnRow = lnRow + 1
									ENDSCAN

									lcRow = LTRIM(STR(lnRow))
									lcEndRow = LTRIM(STR(lnEndRow))

									* UNDERLINE cell to be totaled...
									oWorksheet.RANGE("E" + lcEndRow).BORDERS(xlEdgeBottom).LineStyle = xlContinuous
									oWorksheet.RANGE("E" + lcEndRow).BORDERS(xlEdgeBottom).Weight = xlMedium

									* TOTAL AMOUNT COLUMN
									oWorksheet.RANGE("E" + lcRow).formula = "=sum(E" + lcStartRow + ":E" + lcEndRow + ")"

								ENDIF  &&  INLIST(.cTempCo,'J') AND INLIST(.cMode,"INVOICE")

								*******************************************************************************************
								*******************************************************************************************


							ENDIF && .cMode <> "SUMMARYONLY"

							DO CASE
								CASE .cTempCo == 'K+0630'
									lcFilePrefix = "Weekly Summary " + .cTempCoName + " only Dept 0630 "
								CASE .cTempCo == 'K-0630'
									lcFilePrefix = "Weekly Summary " + .cTempCoName + " all but Dept 0630 "
								CASE .cTempCo == 'DIV57MONTHLY'
									lcFilePrefix = "Monthly Div 57 Summary " + .cTempCoName
								OTHERWISE
									lcFilePrefix = "Weekly Summary " + .cTempCoName
							ENDCASE

							*lcFilePrefix = "Weekly Summary " + .cTempCoName

							* create generic summary info

							ldPayDate = ldEndDate + 6
							*lcFileDate = PADL(MONTH(ldPayDate),2,"0") + "-"  + PADL(DAY(ldPayDate),2,"0") + "-" + PADL(YEAR(ldPayDate),4,"0")
							lcXLSummaryFileName = lcTargetDirectory + lcFilePrefix + " " + lcFileDate + ".XLS"

							IF USED('CURSUMMARY') THEN
								USE IN CURSUMMARY
							ENDIF

							SELECT ;
								ldStartDate AS STARTDATE, ;
								ldEndDate AS ENDDATE, ;
								DIVISION, ;
								DEPT, ;
								EMPLOYEE, ;
								FILE_NUM, ;
								SUM(REG_HRS) AS REG_HRS, ;
								SUM(OT_HRS) AS OT_HRS, ;
								SUM(OT20_HRS) AS OT20_HRS, ;
								SUM(PAY) AS PAY ;
								FROM CURTEMPS ;
								INTO CURSOR CURSUMMARY ;
								GROUP BY DIVISION, DEPT, EMPLOYEE, FILE_NUM ;
								ORDER BY DIVISION, DEPT, EMPLOYEE, FILE_NUM ;
								READWRITE

							*IF .cTempCo = 'A' THEN
							*IF INLIST(.cTempCo,'A','R','T','U') THEN
								* GET HEADCOUNT FOR CARLINA
								IF USED('CURHEADCOUNT') THEN
									USE IN CURHEADCOUNT
								ENDIF

								SELECT COUNT(DISTINCT FILE_NUM) AS HEADCNT ;
									FROM CURTEMPS ;
									INTO CURSOR CURHEADCOUNT

								SELECT CURHEADCOUNT
								.nHeadCount = INT(CURHEADCOUNT.HEADCNT)
							*ENDIF

							*!*										SHORTNAME, ;

							*!*	SELECT CURSUMMARY
							*!*	BROWSE

							* right now, the PAY field in CURSUMMARY is not explicitly rounded to nearest penny
							* But we may need to round it now, depending on rounding mode

							DO CASE
								CASE .nRoundingType = 1  && Standard Rounding

									* daily pays were already rounded to the nearest penny, so no need to round the totals fo this field

								CASE .nRoundingType = 2  && No rounding

									* no rounding anywhere, do nothing

								CASE .nRoundingType = 3  && SELECT rounding

									* must round PAY field in CURSUMMARY to support this mode.
									* round per week associate tot $ to nearest penny, data will be shown on foxpro report
									SELECT CURSUMMARY
									SCAN
										REPLACE CURSUMMARY.PAY WITH ROUND(CURSUMMARY.PAY,2) IN CURSUMMARY
									ENDSCAN

								OTHERWISE
									* NOTHING
							ENDCASE

							* for Select bills, adjust main report total to match the summary spreadsheet, which has individual totals rounded to nearest cent
							IF INLIST(.cTempCo,'B') AND INLIST(.cMode,"INVOICE") THEN
								SELECT CURSUMMARY
								SUM CURSUMMARY.PAY TO .nTotSummaryPay
								*lnDiff = .nTotSummaryPay - ROUND(.nGrandTotalSheetDollar,2)
								lnDiff = .nTotSummaryPay - .nGrandTotalSheetDollar
								IF lnDiff > 0.00 THEN

									lnDiffRow = .nTotalsRow + 1
									lcDiffRow = LTRIM(STR(lnDiffRow))
									oWorksheet.RANGE("P" + lcDiffRow).VALUE = lnDiff

									lnLastRow = .nTotalsRow + 2
									lcLastRow = LTRIM(STR(lnLastRow))
									lcTotalsRow = LTRIM(STR(.nTotalsRow))

									oWorksheet.RANGE("P" + lcLastRow).formula = "=sum(P" + lcTotalsRow + ":P" + lcDiffRow + ")"

									oWorksheet.RANGE("Q" + lcDiffRow).VALUE = "Rounding Adjustment"
									oWorksheet.RANGE("O" + lcLastRow).VALUE = "Grand Total:"

									oWorksheet.RANGE("O" + lcLastRow,"P" + lcLastRow).FONT.bold = .T.

									* UNDERLINE diff cell to be totaled...
									oWorksheet.RANGE("P" + lcDiffRow).BORDERS(xlEdgeBottom).LineStyle = xlContinuous
									oWorksheet.RANGE("P" + lcDiffRow).BORDERS(xlEdgeBottom).Weight = xlMedium


								ENDIF
							ENDIF
							

							IF .cMode <> "SUMMARYONLY" THEN
								* save again
								IF llSaveAgain THEN
									oWorkbook.SAVE()
								ENDIF
								oWorkbook.CLOSE()

								*oExcel.QUIT()

								IF FILE(lcXLFileName) THEN
									.cAttach = lcXLFileName
									.TrackProgress('Attached Time File : ' + lcXLFileName, LOGIT+SENDIT)
									.TrackProgress('KRONOS Time process ended normally.', LOGIT+SENDIT+NOWAITIT)
								ELSE
									.TrackProgress('!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!', LOGIT+SENDIT)
									.TrackProgress("ERROR: There was a problem attaching Time File : " + lcXLFileName, LOGIT+SENDIT)
									.TrackProgress('!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!', LOGIT+SENDIT)
								ENDIF
							ENDIF && .cMode <> "SUMMARYONLY"

							*!*	SELECT CURSUMMARY
							*!*	BROWSE

							IF INLIST(.cMode,"INVOICE","SUMMARYONLY","WEEKLY","DATERANGE","DATERANGE-INVOICE","DIV59WEEKLY") THEN
								* create a summary for the period, will allow AGENCIES to import the invoice #s

								* run foxpro report
								PRIVATE pdStartDate, pdEndDate, pcTempCoName
								pdStartDate = ldStartDate
								pdEndDate = ldEndDate
								pcTempCoName = .cTempCoName


								SELECT CURSUMMARY
								*REPORT FORM M:\DEV\RPT\KRONOSTEMPTIMESUMMARY PREVIEW

								DO CASE
									CASE .cTempCo == 'K+0630'
										lcRootPDFFilename = "C:\TEMPFOX\KRONOSTEMPTIMESUMMARY_ONLY_0630_" + STRTRAN(DTOC(DATE()),"/")
									CASE .cTempCo == 'K-0630'
										lcRootPDFFilename = "C:\TEMPFOX\KRONOSTEMPTIMESUMMARY_NO_0630_" + STRTRAN(DTOC(DATE()),"/")
									OTHERWISE
										lcRootPDFFilename = "C:\TEMPFOX\KRONOSTEMPTIMESUMMARY_" + STRTRAN(DTOC(DATE()),"/")
								ENDCASE

								lcFullPDFFilename = lcRootPDFFilename + ".PDF"

								.CreatePDFReport(lcRootPDFFilename, "M:\DEV\RPT\KRONOSTEMPTIMESUMMARY")

								IF FILE(lcFullPDFFilename) THEN
									IF EMPTY(.cAttach) THEN
										.cAttach = lcFullPDFFilename
									ELSE
										.cAttach = .cAttach + "," + lcFullPDFFilename
									ENDIF
									.TrackProgress('Attached Summary PDF: ' + lcFullPDFFilename, LOGIT+SENDIT)
								ELSE
									.TrackProgress('!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!', LOGIT+SENDIT)
									.TrackProgress("ERROR: There was a problem attaching Summary PDF: " + lcFullPDFFilename, LOGIT+SENDIT)
									.TrackProgress('!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!', LOGIT+SENDIT)
								ENDIF


								SELECT CURSUMMARY
								COPY TO (lcXLSummaryFileName) XL5
								USE IN CURSUMMARY


								* MOST AGENCIES HAVE CUSTOM SUMMARY FILES - SO ONLY ATTACH THE GENERIC ONE IF IT'S NOT THEM
								IF NOT INLIST(.cTempCo,'D','Q','R','T','U','V','X','Y') THEN

									IF FILE(lcXLSummaryFileName) THEN
										IF EMPTY(.cAttach) THEN
											.cAttach = lcXLSummaryFileName
										ELSE
											.cAttach = .cAttach + "," + lcXLSummaryFileName
										ENDIF
										.TrackProgress('Attached Summary Spreadsheet: ' + lcXLSummaryFileName, LOGIT+SENDIT)
									ELSE
										.TrackProgress('!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!', LOGIT+SENDIT)
										.TrackProgress("ERROR: There was a problem attaching Summary Spreadsheet: " + lcXLSummaryFileName, LOGIT+SENDIT)
										.TrackProgress('!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!', LOGIT+SENDIT)
									ENDIF

								ENDIF  &&  NOT INLIST(.cTempCo,'J')

							ENDIF  &&  INLIST(.cMode,"INVOICE","SUMMARYONLY","WEEKLY","DATERANGE")


							IF (.cMode <> "SUMMARYONLY") THEN && AND (.cTempCo = "K") THEN
								* add an hours summary by department for Juan @ ML
								* let everyone have it...

								DO CASE
									CASE .cTempCo == 'K+0630'
										lcFilePrefix = "Hours by Dept Summary Only 0630 " + .cTempCoName
									CASE .cTempCo == 'K-0630'
										lcFilePrefix = "Hours by Dept Summary No 0630 " + .cTempCoName
									OTHERWISE
										lcFilePrefix = "Hours by Division-Dept Summary " + .cTempCoName
								ENDCASE

								*lcFilePrefix = "Hours by Dept Summary " + .cTempCoName

								lcXLSummaryFileName = lcTargetDirectory + lcFilePrefix + " " + lcFileDate + ".XLS"

								IF USED('CURSUMMARY') THEN
									USE IN CURSUMMARY
								ENDIF

								SELECT ;
									ldStartDate AS STARTDATE, ;
									ldEndDate AS ENDDATE, ;
									DIVISION, ;
									DEPT, ;
									SPACE(30) AS DEPTNAME, ;
									SUM(REG_HRS) AS REG_HRS, ;
									SUM(OT_HRS) AS OT_HRS, ;
									SUM(OT20_HRS) AS OT20_HRS, ;
									SUM(PAY) AS PAY ;
									FROM CURTEMPS ;
									INTO CURSOR CURSUMMARY ;
									GROUP BY DIVISION, DEPT ;
									ORDER BY DIVISION, DEPT ;
									READWRITE

								SELECT CURSUMMARY
								SCAN
									lcDeptname = .GetDepartmentName( CURSUMMARY.DEPT )
									REPLACE CURSUMMARY.DEPTNAME WITH lcDeptname IN CURSUMMARY
								ENDSCAN
								
								IF .lNewDivDeptReport AND (NOT EMPTY(.cWeeklyTemplate)) AND INLIST(.cMode,"INVOICE","DATERANGE-INVOICE")THEN 
									* create new div/dept reports from Adelene Teo.

									*lcXLSummaryFileName = lcTargetDirectory + 'Labor_Allocation_' + .cTempCoName + '_PE_' + DTOS(.dEndDate) + '.xlsx'
									lcXLSummaryFileName = lcTargetDirectory + lcFilePrefix + " " + DTOS(.dStartDate) + " - " + DTOS(.dEndDate) + ".xlsx"
									
									oWorkbook = oExcel.workbooks.OPEN(.cWeeklyTemplate + '.xlsx')

									IF FILE(lcXLSummaryFileName) THEN
										DELETE FILE (lcXLSummaryFileName)
									ENDIF

									oWorkbook.SAVEAS(lcXLSummaryFileName)
									
									WAIT WINDOW NOWAIT "Building Time spreadsheet..."									
									
									oWorksheet = oWorkbook.Worksheets[1]
									
									oWorksheet.RANGE("E3").VALUE = .dStartDate
									oWorksheet.RANGE("G3").VALUE = .dEndDate
									
									lnRow = 9
									SELECT CURSUMMARY
									SCAN
										lcRow = LTRIM(STR(lnRow))
										oWorksheet.RANGE("A" + lcRow).VALUE = .dStartDate
										oWorksheet.RANGE("B" + lcRow).VALUE = .dEndDate
										oWorksheet.RANGE("C" + lcRow).VALUE = "'" + cursummary.division
										oWorksheet.RANGE("D" + lcRow).VALUE = "'" + cursummary.dept
										oWorksheet.RANGE("E" + lcRow).VALUE = cursummary.deptname
										oWorksheet.RANGE("F" + lcRow).VALUE = cursummary.REG_HRS
										oWorksheet.RANGE("G" + lcRow).VALUE = cursummary.OT_HRS
										oWorksheet.RANGE("H" + lcRow).VALUE = cursummary.OT20_HRS
										oWorksheet.RANGE("I" + lcRow).VALUE = cursummary.PAY
										lnRow = lnRow + 1
									ENDSCAN

									* clear template rows which are not needed
									lcRow = LTRIM(STR(lnRow))
									oWorksheet.RANGE("A" + lcRow,"AP151").clearcontents()
									
									WAIT CLEAR
									
									oWorkbook.SAVE()
									oExcel.QUIT()
								
								ELSE
									* create standard div/dept report

									SELECT CURSUMMARY
									COPY TO (lcXLSummaryFileName) XL5
									
								ENDIF  &&  .lTestMode AND .lNewDivDeptReport 

								USE IN CURSUMMARY

								IF FILE(lcXLSummaryFileName) THEN
									.cAttach = .cAttach + "," + lcXLSummaryFileName
									lcAttachR = lcXLSummaryFileName
									.TrackProgress('Attached Time File : ' + lcXLSummaryFileName, LOGIT+SENDIT)									
									
									* file is now sent to Kiandra a bit further down 12/16/2016 MB
									*!*	********************************************************************************
									*!*	* Send this summary file to Aspen Vernon, per Ed K. 10/12/2015
									*!*	* changed to Kiandra Davis 3/18/2016 MB
									*!*	IF INLIST(.cTempCo,'R') THEN

									*!*		IF .lTestMode THEN											
									*!*			lcSendToR = 'mark.bennett@tollgroup.com'
									*!*			lcCCR = ''
									*!*		ELSE
									*!*			lcSendToR = 'Kiandra.Davis@Tollgroup.com'
									*!*			lcCCR = 'mark.bennett@tollgroup.com'
									*!*		ENDIF

									*!*		* send email...

									*!*		lcAttachR = lcXLSummaryFileName
									*!*		lcBodyTextR = 'See attached summary spreadsheet.'

									*!*		DO FORM dartmail2 WITH lcSendToR,.cFrom,.cSubject,lcCCR,lcAttachR,lcBodyTextR,"A"
									*!*		.TrackProgress('Sent SSG San Pedro summary file email to ' + lcSendToR + '.',LOGIT+SENDIT)
									*!*		
									*!*	ENDIF  &&  INLIST(.cTempCo,'R')
									********************************************************************************
								
								ELSE
									.TrackProgress('!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!', LOGIT+SENDIT)
									.TrackProgress("ERROR: There was a problem attaching Time File : " + lcXLSummaryFileName, LOGIT+SENDIT)
									.TrackProgress('!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!', LOGIT+SENDIT)
								ENDIF
								
										
								*IF INLIST(.cTempCo,'R') THEN
								*IF INLIST(.cTempCo,'R') AND (NOT INLIST(.cMode,'DIV52DAILY','DIV58DAILY')) THEN
								IF INLIST(.cTempCo,'X') AND (NOT INLIST(.cMode,'DIV52DAILY','DIV58DAILY')) THEN

									* do separate mailing to Kiandra, with a new spreadhseet just produced PLUS a new spreadsheet by employee 
									* (similar to summary by name report produced later, but without pay info), for Kiandra - per Maria Rodriguez 12/16/2016.

									IF USED('CURSUMMARYSSGPRE') THEN
										USE IN CURSUMMARYSSGPRE
									ENDIF

									IF USED('CURSUMMARYSSGKIANDRA') THEN
										USE IN CURSUMMARYSSGKIANDRA
									ENDIF
						
									SELECT ;
										ldStartDate AS STARTDATE, ;
										ldEndDate AS ENDDATE, ;
										A.DIVISION, ;
										A.DEPT, ;
										A.EMPLOYEE, ;
										A.FILE_NUM, ;
										A.SHORTNAME AS EMP_NUM, ;
										SUM(A.REG_HRS) AS REG_HRS, ;
										SUM(A.OT_HRS) AS OT_HRS, ;
										SUM(A.OT20_HRS) AS OT20_HRS ;
										FROM CURTEMPS A ;
										INTO CURSOR CURSUMMARYSSGPRE ;
										GROUP BY A.DIVISION, A.DEPT, A.EMPLOYEE, A.FILE_NUM, A.SHORTNAME ;
										ORDER BY A.DIVISION, A.DEPT, A.EMPLOYEE, A.FILE_NUM, A.SHORTNAME

									SELECT STARTDATE, ENDDATE, DIVISION, DEPT, EMPLOYEE, EMP_NUM, REG_HRS, OT_HRS, OT20_HRS, (REG_HRS + OT_HRS + OT20_HRS) AS TOT_HOURS ;
										FROM CURSUMMARYSSGPRE ;
										INTO CURSOR CURSUMMARYSSGKIANDRA ;
										ORDER BY DIVISION, DEPT, EMPLOYEE ;
										READWRITE
									
									lcXLSummarySSGKiandra = lcTargetDirectory + .cTempCoName + " Summary by Div-Dept-Associate " + lcFileDate + ".XLS"

									SELECT CURSUMMARYSSGKIANDRA
									COPY TO (lcXLSummarySSGKiandra) XL5

									IF FILE(lcXLSummarySSGKiandra) THEN
										lcAttachR = lcAttachR + "," + lcXLSummarySSGKiandra
										.TrackProgress('Attached Time File : ' + lcXLSummarySSGKiandra, LOGIT+SENDIT)									
									ELSE
										.TrackProgress('!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!', LOGIT+SENDIT)
										.TrackProgress("ERROR: There was a problem attaching Time File : " + lcXLSummarySSGKiandra, LOGIT+SENDIT)
										.TrackProgress('!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!', LOGIT+SENDIT)
									ENDIF										

									IF .lTestMode THEN											
										lcSendToR = 'mark.bennett@tollgroup.com'
										lcCCR = ''
									ELSE
										lcSendToR = 'Maria.Rodriguez@Tollgroup.com'
										lcCCR = 'mark.bennett@tollgroup.com'
									ENDIF
								
									lcBodyTextR = 'See attached informational spreadsheets.'

									DO FORM dartmail2 WITH lcSendToR,.cFrom,.cSubject,lcCCR,lcAttachR,lcBodyTextR,"A"
									.TrackProgress('Sent SSG San Pedro summary file email to ' + lcSendToR + '.',LOGIT+SENDIT)
								
								ENDIF  && INLIST(.cTempCo,'R') THEN
	
								********************************************************************************
								
								*IF INLIST(.cTempCo,'T') AND (INLIST(.cMode,'DAILY')) THEN
								IF INLIST(.cTempCo,'Y') AND (INLIST(.cMode,'DAILY')) THEN

									* do separate BBB div 57 summary by shift for prior day mailing to Steve Sykes.

									IF USED('CURSUMMARYBBB') THEN
										USE IN CURSUMMARYBBB
									ENDIF
									
									*!*	SELECT * FROM curtemps INTO CURSOR curtestbbb WHERE division = '57'
									*!*	SELECT curtestbbb
									*!*	COPY TO c:\a\curtestbbb.xls xl5
						
									* 3/14/2018 MB: added A.DEPT <> '6301' to the where below to exclude GAP/Payless from the data per Joana Burke.
									SELECT ;
										ldStartDate as DATE, ;
										A.DIVISION, ;
										A.CALCDSHIFT AS SHIFT, ;
										COUNT(*) AS HEADCNT, ;
										SUM(A.REG_HRS) AS REG_HRS, ;
										SUM(A.OT_HRS) AS OT_HRS, ;
										SUM(A.OT20_HRS) AS OT20_HRS ;
										FROM CURTEMPS A ;
										INTO CURSOR CURSUMMARYBBB ;
										WHERE A.DIVISION = '57' ;
										AND A.DEPT <> '6301' ;
										GROUP BY A.DIVISION, A.CALCDSHIFT ;
										ORDER BY A.DIVISION, A.CALCDSHIFT
									
									lcXLSummaryBBB = lcTargetDirectory + .cTempCoName + " BBB Summary by Shift " + DTOS(ldStartDate) + ".XLS"
									lcBBBSubject = "BBB Summary by Shift for " + DTOC(ldStartDate)

									SELECT CURSUMMARYBBB 
									COPY TO (lcXLSummaryBBB) XL5

									IF FILE(lcXLSummaryBBB) THEN
										lcAttachR = lcXLSummaryBBB
										.TrackProgress('Attached BBB Summary File : ' + lcXLSummaryBBB, LOGIT+SENDIT)									
									ELSE
										.TrackProgress('!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!', LOGIT+SENDIT)
										.TrackProgress("ERROR: There was a problem attaching BBB summary File : " + lcXLSummaryBBB, LOGIT+SENDIT)
										.TrackProgress('!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!', LOGIT+SENDIT)
									ENDIF										

									IF .lTestMode THEN											
										lcSendToR = 'mark.bennett@Tollgroup.com'
										lcCCR = 'mark.bennett@tollgroup.com'
									ELSE
										lcSendToR = 'Steven.Sykes@Tollgroup.com, Scott.Schreck@Tollgroup.com'
										lcCCR = 'mark.bennett@tollgroup.com'
									ENDIF
								
									lcBodyTextR = 'See attached informational spreadsheets.'

									DO FORM dartmail2 WITH lcSendToR,.cFrom,lcBBBSubject,lcCCR,lcAttachR,lcBodyTextR,"A"
									.TrackProgress('Sent BBB summary file email to ' + lcSendToR + '.',LOGIT+SENDIT)
								
								ENDIF  && INLIST(.cTempCo,'Y') AND (INLIST(.cMode,'DAILY'))
	
								********************************************************************************
							
								
							ENDIF && .cMode <> "SUMMARYONLY"


							IF .lTestMode AND (.cMode = "DATERANGE") THEN

								* create multi-month trending spreadsheet for people like Jon and Joe DeSaye
								IF USED('CURTRENDPRE') THEN
									USE IN
								ENDIF
								IF USED('CURTREND') THEN
									USE IN
								ENDIF

								SELECT *, 0000 AS YEAR, 00 AS MONTH ;
									FROM CURTEMPS ;
									INTO CURSOR CURTRENDPRE ;
									READWRITE

								SELECT CURTRENDPRE
								REPLACE ALL YEAR WITH YEAR(PAYDATE), MONTH WITH MONTH(PAYDATE) IN CURTRENDPRE

								SELECT ;
									DIVISION, ;
									YEAR, ;
									MONTH, ;
									SUM(REG_HRS) AS REG_HRS, ;
									SUM(OT_HRS) AS OT_HRS, ;
									SUM(OT20_HRS) AS OT20_HRS, ;
									SUM(REG_HRS + OT_HRS + OT20_HRS) AS TOT_HRS, ;
									SUM(PAY) AS PAY ;
									FROM CURTRENDPRE ;
									INTO CURSOR CURTREND ;
									GROUP BY DIVISION, YEAR, MONTH ;
									ORDER BY DIVISION, YEAR, MONTH

								SELECT CURTREND
								COPY TO ("C:\A\TIME_SUMMARY_" + lcStartDate + " - " + lcEndDate + ".XLS") XL5

							ENDIF  &&  .lTestMode AND (.cMode = "DATERANGE")

							IF USED('CURRECRUITS') THEN
								USE IN CURRECRUITS
							ENDIF

							IF INLIST(.cTempCo,'Q') THEN
							
								* ATTACH SUMMARY OF TEMPS RECRUITED by COR-TECH OR ManPower (MP)
								SELECT DISTINCT EMPLOYEE ;
									FROM CURTEMPS ;
									INTO CURSOR CURRECRUITS ;
									WHERE ("~REC" $ UPPER(SHORTNAME) OR "~MP" $ UPPER(SHORTNAME)) ;
									ORDER BY EMPLOYEE

								IF USED('CURRECRUITS') AND NOT EOF('CURRECRUITS') THEN
									SELECT CURRECRUITS
									COPY TO (lcCorTechRecruitsFile) XL5
									USE IN CURRECRUITS
									IF FILE(lcCorTechRecruitsFile) THEN
										IF EMPTY(.cAttach) THEN
											.cAttach = lcCorTechRecruitsFile
										ELSE
											.cAttach = .cAttach + "," + lcCorTechRecruitsFile
										ENDIF
										.TrackProgress('Attached Cor-Tech Recruits Spreadsheet: ' + lcCorTechRecruitsFile, LOGIT+SENDIT)
									ELSE
										.TrackProgress('!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!', LOGIT+SENDIT)
										.TrackProgress("ERROR: There was a problem attaching Cor-Tech Recruits Spreadsheet: " + lcCorTechRecruitsFile, LOGIT+SENDIT)
										.TrackProgress('!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!', LOGIT+SENDIT)
									ENDIF
								ENDIF
								
								** 05/11/2017: added support for additional company = 'Command Staffing' which is identified by ~CMP~ in the short name field.
								* ATTACH SUMMARY OF TEMPS RECRUITED by Command Staffing
								SELECT DISTINCT EMPLOYEE ;
									FROM CURTEMPS ;
									INTO CURSOR CURCMPRECRUITS ;
									WHERE ("~CMP" $ UPPER(SHORTNAME)) ;
									ORDER BY EMPLOYEE

								IF USED('CURCMPRECRUITS') AND NOT EOF('CURCMPRECRUITS') THEN
									SELECT CURCMPRECRUITS
									COPY TO (lcCMPRecruitsFile) XL5
									USE IN CURCMPRECRUITS
									IF FILE(lcCMPRecruitsFile) THEN
										IF EMPTY(.cAttach) THEN
											.cAttach = lcCMPRecruitsFile
										ELSE
											.cAttach = .cAttach + "," + lcCMPRecruitsFile
										ENDIF
										.TrackProgress('Attached Command Staffing Recruits Spreadsheet: ' + lcCMPRecruitsFile, LOGIT+SENDIT)
									ELSE
										.TrackProgress('!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!', LOGIT+SENDIT)
										.TrackProgress("ERROR: There was a problem attaching Command Staffing Recruits Spreadsheet: " + lcCMPRecruitsFile, LOGIT+SENDIT)
										.TrackProgress('!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!', LOGIT+SENDIT)
									ENDIF
								ENDIF
								
								
								
							ENDIF  &&  INLIST(.cTempCo,'Q')
							
							*** BEGIN CREATING CUSTOM SUMMARY REPORTS ********************************************************************************
							
							*IF INLIST(.cTempCo,'T') AND INLIST(.cMode,"DIV57MONTHLY") THEN
							IF INLIST(.cTempCo,'Y') AND INLIST(.cMode,"DIV57MONTHLY") THEN
							
								IF .lTestMode THEN 
									* create a BBB month summary by shift for Steve Sykes
									
									IF USED('CURSUMMARYBBB') THEN
										USE IN CURSUMMARYBBB
									ENDIF
									
									*!*	SELECT * FROM curtemps INTO CURSOR curtestbbb WHERE division = '57'
									*!*	SELECT curtestbbb
									*!*	COPY TO c:\a\curtestbbb.xls xl5
						
									SELECT ;
										ldStartDate as STARTDATE, ;
										ldEndDate as ENDDATE, ;
										A.DIVISION, ;
										A.CALCDSHIFT AS SHIFT, ;
										COUNT(*) AS HEADCNT, ;
										SUM(A.REG_HRS) AS REG_HRS, ;
										SUM(A.OT_HRS) AS OT_HRS, ;
										SUM(A.OT20_HRS) AS OT20_HRS ;
										FROM CURTEMPS A ;
										INTO CURSOR CURSUMMARYBBB ;
										WHERE A.DIVISION = '57' ;
										GROUP BY A.DIVISION, A.CALCDSHIFT ;
										ORDER BY A.DIVISION, A.CALCDSHIFT
									
									lcXLSummaryBBB = lcTargetDirectory + .cTempCoName + " BBB Summary by Shift " + DTOS(ldStartDate) + " - " + DTOS(ldEndDate) + ".XLS"
									*lcBBBSubject = "BBB Summary by Shift for " + DTOC(ldStartDate)

									SELECT CURSUMMARYBBB 
									COPY TO (lcXLSummaryBBB) XL5
									
								ELSE
									* CREATE A SUMMARY BY DAY FILE OF THE MONTHLY DETAIL for Maria R.
									
									IF USED('CURSUMMARYDIV57') THEN
										USE IN CURSUMMARYDIV57
									ENDIF
									
									SELECT ;
										PAYDATE, ;
										SUM(REG_HRS) AS REG_HRS, ;
										SUM(OT_HRS) AS OT_HRS, ;
										SUM(OT20_HRS) AS OT20_HRS ;
										FROM CURTEMPS ;
										INTO CURSOR CURSUMMARYDIV57 ;
										GROUP BY PAYDATE ;
										ORDER BY PAYDATE
										
	*!*									SELECT CURSUMMARYDIV57
	*!*									BROWSE

									lcFilePrefixSummary = "Div 57 Monthly Summary by Date " + .cTempCoName
									lcXLSUMMARYDIV57 = lcTargetDirectory + lcFilePrefixSummary + " " + lcFileDate + ".XLS"
									SELECT CURSUMMARYDIV57
									COPY TO (lcXLSUMMARYDIV57) XL5
						
									IF FILE(lcXLSUMMARYDIV57) THEN
										.cAttach = .cAttach + "," + lcXLSUMMARYDIV57
										.TrackProgress('Attached Time File : ' + lcXLSUMMARYDIV57, LOGIT+SENDIT)
									ELSE
										.TrackProgress('!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!', LOGIT+SENDIT)
										.TrackProgress("ERROR: There was a problem attaching Time File : " + lcXLSUMMARYDIV57, LOGIT+SENDIT)
										.TrackProgress('!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!', LOGIT+SENDIT)
									ENDIF
									
								ENDIF && .lTestMode 
							
							ENDIF  &&  INLIST(.cTempCo,'Y') AND INLIST(.cMode,"DIV57MONTHLY")
							

							IF INLIST(.cMode,"INVOICE","WEEKLY","DATERANGE","DATERANGE-INVOICE","DAILY","DIV52DAILY","DIV58DAILY") THEN
								lcFilePrefixSummary = "Summary by Associate " + .cTempCoName
								DO CASE
									CASE INLIST(.cTempCo,'D','F','G')
										* FOR CREATE A SUMMARY SPREADSHEET WITH SS# (IN THE SHORTNAME FIELD) and wage.

										IF USED('CURSUMMARYQUALITYPRE') THEN
											USE IN CURSUMMARYQUALITYPRE
										ENDIF

										IF USED('CURSUMMARYQUALITY') THEN
											USE IN CURSUMMARYQUALITY
										ENDIF

										SELECT ;
											ldStartDate AS STARTDATE, ;
											ldEndDate AS ENDDATE, ;
											A.DIVISION, ;
											A.DEPT, ;
											A.EMPLOYEE, ;
											B.LASTNM, ;
											B.FIRSTNM, ;
											A.FILE_NUM, ;
											A.SHORTNAME AS SS_NUM, ;
											A.WAGE, ;
											SUM(A.REG_HRS) AS REG_HRS, ;
											SUM(A.OT_HRS) AS OT_HRS, ;
											SUM(A.OT20_HRS) AS OT20_HRS, ;
											SUM(A.PAY) AS PAY ;
											FROM CURTEMPS A ;
											LEFT OUTER JOIN CURLASTFIRST B ;
											ON B.FILE_NUM = A.FILE_NUM ;
											INTO CURSOR CURSUMMARYQUALITYPRE ;
											GROUP BY A.DIVISION, A.DEPT, A.EMPLOYEE, B.LASTNM, B.FIRSTNM, A.FILE_NUM, A.SHORTNAME, A.WAGE ;
											ORDER BY A.DIVISION, A.DEPT, B.LASTNM, B.FIRSTNM, A.FILE_NUM, A.SHORTNAME, A.WAGE

										* SHORTNAME CAN HOLD SS# PLUS THEIR ADP FILE_NUM, SO TRY TO EXTRACT BOTH...

										SELECT STARTDATE, ENDDATE, DIVISION, DEPT, EMPLOYEE, LASTNM, FIRSTNM, SPACE(6) AS FILE_NUM, SS_NUM, WAGE AS BASEWAGE, REG_HRS, OT_HRS, OT20_HRS, PAY ;
											FROM CURSUMMARYQUALITYPRE ;
											INTO CURSOR CURSUMMARYQUALITY ;
											ORDER BY DIVISION, DEPT, LASTNM, FIRSTNM ;
											READWRITE


										SELECT CURSUMMARYQUALITY
										SCAN
											lcSSAll = ALLTRIM(CURSUMMARYQUALITY.SS_NUM)
											IF LEN(lcSSAll) > 11 THEN
												* ANYTHING PAST 11 IS FILE_NUM FOR RONALD
												lcSSNum = LEFT(lcSSAll,11)
												lcFileNum = STRTRAN(SUBSTR(lcSSAll,12),"-")
												REPLACE CURSUMMARYQUALITY.SS_NUM WITH lcSSNum, CURSUMMARYQUALITY.FILE_NUM WITH lcFileNum IN CURSUMMARYQUALITY
											ENDIF
										ENDSCAN


										lcXLSummaryQuality = lcTargetDirectory + lcFilePrefixSummary + " SSN " + lcFileDate + ".XLS"
										SELECT CURSUMMARYQUALITY
										COPY TO (lcXLSummaryQuality) XL5


										* ACTIVATED THIS BLOCK TO SEND SS_NUMS ONLY TO QUALITY, 4/16/2015 MB
										IF .cTempCo = 'D' THEN
											LOCAL lcSendToQ, lcCCQ, lcAttachQ, lcBodyTextQ
							
											IF FILE(lcXLSummaryQuality) THEN

												DO CASE
													CASE INLIST(.cMode,"INVOICE","DATERANGE-INVOICE","DAILY")
														lcSendToQ = 'sueoqualitytemps@outlook.com'
													CASE .cMode = "WEEKLY"
														lcSendToQ = 'sueoqualitytemps@outlook.com'
													OTHERWISE
														lcSendToQ = 'mark.bennett@tollgroup.com'
												ENDCASE
												
												IF .lTestMode THEN											
													lcSendToQ = 'mark.bennett@tollgroup.com'
													lcCCQ = ''
												ENDIF

												lcCCQ = 'mark.bennett@tollgroup.com'
												lcAttachQ = lcXLSummaryQuality
												lcBodyTextQ = 'See attached confidential summary spreadsheet.'

												DO FORM dartmail2 WITH lcSendToQ,.cFrom,.cSubject,lcCCQ,lcAttachQ,lcBodyTextQ,"A"
												.TrackProgress('Sent Quality custom summary file email.',LOGIT)
											ELSE
												.TrackProgress('!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!', LOGIT+SENDIT)
												.TrackProgress("ERROR: There was a problem attaching summary File : " + lcXLSummaryQuality, LOGIT+SENDIT)
												.TrackProgress('!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!', LOGIT+SENDIT)
											ENDIF
										ENDIF  &&  .cTempCo = 'D'

										* we have emailed the summary file to Quality; now blank out ss_num in the file that will get emailed to everyone
										SELECT CURSUMMARYQUALITY
										REPLACE ALL SS_NUM WITH "" IN CURSUMMARYQUALITY
										SELECT CURSUMMARYQUALITY
										COPY TO (lcXLSummaryQuality) XL5

										IF FILE(lcXLSummaryQuality) THEN
											.cAttach = .cAttach + "," + lcXLSummaryQuality
											.TrackProgress('Attached Time File : ' + lcXLSummaryQuality, LOGIT+SENDIT)
										ELSE
											.TrackProgress('!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!', LOGIT+SENDIT)
											.TrackProgress("ERROR: There was a problem attaching Time File : " + lcXLSummaryQuality, LOGIT+SENDIT)
											.TrackProgress('!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!', LOGIT+SENDIT)
										ENDIF

									CASE INLIST(.cTempCo,'Q')
										* FOR COR-TECH CREATE A SUMMARY SPREADSHEET WITH THEIR EMP #S (IN THE SHORTNAME FIELD) and wage.

										IF USED('CURSUMMARYCORTECHPRE') THEN
											USE IN CURSUMMARYCORTECHPRE
										ENDIF

										IF USED('CURSUMMARYCORTECH') THEN
											USE IN CURSUMMARYCORTECH
										ENDIF

										SELECT ;
											ldStartDate AS STARTDATE, ;
											ldEndDate AS ENDDATE, ;
											A.DIVISION, ;
											A.DEPT, ;
											A.EMPLOYEE, ;
											B.LASTNM, ;
											B.FIRSTNM, ;
											A.SHORTNAME AS FILE_NUM, ;
											A.WAGE, ;
											SUM(A.REG_HRS) AS REG_HRS, ;
											SUM(A.OT_HRS) AS OT_HRS, ;
											SUM(A.OT20_HRS) AS OT20_HRS, ;
											SUM(A.PAY) AS PAY ;
											FROM CURTEMPS A ;
											LEFT OUTER JOIN CURLASTFIRST B ;
											ON B.FILE_NUM = A.FILE_NUM ;
											INTO CURSOR CURSUMMARYCORTECHPRE ;
											GROUP BY A.DIVISION, A.DEPT, A.EMPLOYEE, B.LASTNM, B.FIRSTNM, A.SHORTNAME, A.WAGE ;
											ORDER BY A.DIVISION, A.DEPT, B.LASTNM, B.FIRSTNM, A.SHORTNAME, A.WAGE

										* SHORTNAME CAN HOLD "~REC~" PLUS THEIR ADP FILE_NUM, SO TRY TO EXTRACT BOTH...

										SELECT STARTDATE, ENDDATE, DIVISION, DEPT, EMPLOYEE, LASTNM, FIRSTNM, FILE_NUM, WAGE AS BASEWAGE, REG_HRS, OT_HRS, OT20_HRS, PAY ;
											FROM CURSUMMARYCORTECHPRE ;
											INTO CURSOR CURSUMMARYCORTECH ;
											ORDER BY DIVISION, DEPT, LASTNM, FIRSTNM ;
											READWRITE


										SELECT CURSUMMARYCORTECH
										SCAN
											* STRIP OUT "~REC~" FROM SHORTNAME FIELD IF IT IS THERE
											lcEmpNumber = STRTRAN(ALLTRIM(CURSUMMARYCORTECH.FILE_NUM),"~REC~","")
											lcEmpNumber = STRTRAN(lcEmpNumber,"~MP~","")
											lcEmpNumber = STRTRAN(lcEmpNumber,"~CMP~","")
											REPLACE CURSUMMARYCORTECH.FILE_NUM WITH lcEmpNumber IN CURSUMMARYCORTECH
										ENDSCAN

										lcXLSummaryCORTECH = lcTargetDirectory + lcFilePrefixSummary + " By FileNum " + lcFileDate + ".XLS"
										SELECT CURSUMMARYCORTECH
										COPY TO (lcXLSummaryCORTECH) XL5
										
										IF FILE(lcXLSummaryCORTECH) THEN
											.cAttach = .cAttach + "," + lcXLSummaryCORTECH
											.TrackProgress('Attached Time File : ' + lcXLSummaryCORTECH, LOGIT+SENDIT)
										ELSE
											.TrackProgress('!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!', LOGIT+SENDIT)
											.TrackProgress("ERROR: There was a problem attaching Time File : " + lcXLSummaryCORTECH, LOGIT+SENDIT)
											.TrackProgress('!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!', LOGIT+SENDIT)
										ENDIF

									CASE INLIST(.cTempCo,'R','T','U','V','X','Y')
										* FOR STAFFING SOLUTIONS - SP, ML & CARSONS - CREATE A SUMMARY SPREADSHEET WITH SSG UNIQUE EMPLOYEE# (IN THE SHORTNAME FIELD) and wage.

										IF USED('CURSUMMARYSSGPRE') THEN
											USE IN CURSUMMARYSSGPRE
										ENDIF

										IF USED('CURSUMMARYSSG') THEN
											USE IN CURSUMMARYSSG
										ENDIF

										IF USED('CURSUMMARYSSGKIANDRA') THEN
											USE IN CURSUMMARYSSGKIANDRA
										ENDIF

										IF USED('CURCSVSUMMARYSSGPRE') THEN
											USE IN CURCSVSUMMARYSSGPRE
										ENDIF

										IF USED('CURCSVSUMMARYSSG') THEN
											USE IN CURCSVSUMMARYSSG
										ENDIF

										*!*			SELECT ;
										*!*				ldStartDate AS STARTDATE, ;
										*!*				ldEndDate AS ENDDATE, ;
										*!*				A.DIVISION, ;
										*!*				A.DEPT, ;
										*!*				A.EMPLOYEE, ;
										*!*				B.LASTNM, ;
										*!*				B.FIRSTNM, ;
										*!*				A.FILE_NUM, ;
										*!*				A.SHORTNAME AS EMP_NUM, ;
										*!*				A.WAGE, ;
										*!*				SUM(A.REG_HRS) AS REG_HRS, ;
										*!*				SUM(A.OT_HRS) AS OT_HRS, ;
										*!*				SUM(A.OT20_HRS) AS OT20_HRS, ;
										*!*				SUM(A.PAY) AS PAY ;
										*!*				FROM CURTEMPS A ;
										*!*				LEFT OUTER JOIN CURLASTFIRST B ;
										*!*				ON B.FILE_NUM = A.FILE_NUM ;
										*!*				INTO CURSOR CURSUMMARYSSGPRE ;
										*!*				GROUP BY A.DIVISION, A.DEPT, A.EMPLOYEE, B.LASTNM, B.FIRSTNM, A.FILE_NUM, A.SHORTNAME, A.WAGE ;
										*!*				ORDER BY A.DIVISION, A.DEPT, B.LASTNM, B.FIRSTNM, A.FILE_NUM, A.SHORTNAME, A.WAGE

										*!*				SELECT STARTDATE, ENDDATE, DIVISION, DEPT, EMPLOYEE, LASTNM, FIRSTNM, EMP_NUM, WAGE AS BASEWAGE, REG_HRS, OT_HRS, OT20_HRS, PAY ;
										*!*				FROM CURSUMMARYSSGPRE ;
										*!*				INTO CURSOR CURSUMMARYSSG ;
										*!*				ORDER BY DIVISION, DEPT, LASTNM, FIRSTNM ;
										*!*				READWRITE

										SELECT ;
											ldStartDate AS STARTDATE, ;
											ldEndDate AS ENDDATE, ;
											A.DIVISION, ;
											A.DEPT, ;
											A.EMPLOYEE, ;
											A.FILE_NUM, ;
											A.SHORTNAME AS EMP_NUM, ;
											A.WAGE, ;
											SUM(A.REG_HRS) AS REG_HRS, ;
											SUM(A.OT_HRS) AS OT_HRS, ;
											SUM(A.OT20_HRS) AS OT20_HRS, ;
											SUM(A.PAY) AS PAY ;
											FROM CURTEMPS A ;
											INTO CURSOR CURSUMMARYSSGPRE ;
											GROUP BY A.DIVISION, A.DEPT, A.EMPLOYEE, A.FILE_NUM, A.SHORTNAME, A.WAGE ;
											ORDER BY A.DIVISION, A.DEPT, A.EMPLOYEE, A.FILE_NUM, A.SHORTNAME, A.WAGE

										SELECT STARTDATE, ENDDATE, DIVISION, DEPT, EMPLOYEE, EMP_NUM, WAGE AS BASEWAGE, REG_HRS, OT_HRS, OT20_HRS, PAY ;
											FROM CURSUMMARYSSGPRE ;
											INTO CURSOR CURSUMMARYSSG ;
											ORDER BY DIVISION, DEPT, EMPLOYEE ;
											READWRITE

										*lcXLSummarySSG = lcTargetDirectory + lcFilePrefixSummary + " EMP_NUM " + lcFileDate + ".XLS"
										lcXLSummarySSG = lcTargetDirectory + .cTempCoName + " Summary by Name " + lcFileDate + ".XLS"

										SELECT CURSUMMARYSSG
										COPY TO (lcXLSummarySSG) XL5

										IF FILE(lcXLSummarySSG) THEN
											.cAttach = .cAttach + "," + lcXLSummarySSG
											.TrackProgress('Attached Time File : ' + lcXLSummarySSG, LOGIT+SENDIT)
										ELSE
											.TrackProgress('!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!', LOGIT+SENDIT)
											.TrackProgress("ERROR: There was a problem attaching Time File : " + lcXLSummarySSG, LOGIT+SENDIT)
											.TrackProgress('!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!', LOGIT+SENDIT)
										ENDIF
										
										
										
										***************************************************************************************************************
										
										* create the .csv file SSG wants  -- .GetTempRefNumber()
										*lcCSVSummarySSG = lcTargetDirectory + lcFilePrefixSummary + " EMP_NUM " + lcFileDate + ".CSV"
										lcCSVSummarySSG = lcTargetDirectory + .cTempCoName + " Accounting Summary " + lcFileDate + ".CSV"

										SELECT ;
											SPACE(3) AS REF_NUM, ;
											ldEndDate AS ENDDATE, ;
											A.DIVISION, ;											
											A.SHORTNAME AS EMP_NUM, ;
											SUM(A.REG_HRS) AS REG_HRS, ;
											SUM(A.OT_HRS) AS OT_HRS, ;
											SUM(A.OT20_HRS) AS OT20_HRS ;
											FROM CURTEMPS A ;
											INTO CURSOR CURCSVSUMMARYSSGPRE ;
											GROUP BY A.DIVISION, A.SHORTNAME ;
											ORDER BY A.DIVISION, A.SHORTNAME ;
											READWRITE
											
										SELECT CURCSVSUMMARYSSGPRE 
										SCAN
											*REPLACE CURCSVSUMMARYSSGPRE.REF_NUM WITH .GetTempRefNumber( CURCSVSUMMARYSSGPRE.DIVISION ) IN CURCSVSUMMARYSSGPRE
											REPLACE CURCSVSUMMARYSSGPRE.REF_NUM WITH .GetTempRefNumber( LEFT(ALLTRIM(CURCSVSUMMARYSSGPRE.DIVISION),2) ) IN CURCSVSUMMARYSSGPRE
										ENDSCAN
										
										.CreateSSGCSVFile( lcCSVSummarySSG, 'CURCSVSUMMARYSSGPRE' )									
								
									OTHERWISE
										* NOTHING

								ENDCASE
								*** END CREATING CUSTOM SUMMARY REPORTS ********************************************************************************
								
								
								****************************************************************
								* 
								* add ytd hours file as attachment
								IF FILE(lcYTDSummaryFile) THEN
									.cAttach = .cAttach + "," + lcYTDSummaryFile
									.TrackProgress('Attached YTD Hours File : ' + lcYTDSummaryFile, LOGIT+SENDIT)
								ELSE
									.TrackProgress('!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!', LOGIT+SENDIT)
									.TrackProgress("ERROR: There was a problem attaching YTD Hours File : " + lcYTDSummaryFile, LOGIT+SENDIT)
									.TrackProgress('!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!', LOGIT+SENDIT)
								ENDIF
								****************************************************************
								
							ENDIF  &&  INLIST(.cMode,"INVOICE","WEEKLY")
							

						ENDIF  && EOF() CURTEMPS

					ENDIF  &&  .ExecSQL(lcSQL, 'CURWTKEMPLOYEE', RETURN_DATA_MANDATORY)

					.TrackProgress('KRONOS Time process ended normally.', LOGIT+SENDIT+NOWAITIT)

					CLOSE DATABASES ALL

					DO CASE
							*CASE .cMode = "DAILY"
						CASE INLIST(.cMode, "DAILY", "ZDAILY")
							lcTopBodyText = "Time for yesterday is attached." + CRLF + ;
								"Managers, please review those temps you are responsible for and " + ;
								"report any inaccuracies in Division, Department, or Hours Worked to the temp agency for correction." + CRLF + ;
								"Potential problem shifts ( < " + TRANSFORM(.nMinHoursTest,"9.9") + ;
								" or > " + TRANSFORM(.nMaxHoursTest,"99.9") + ;
								" Hours Worked ), and other bad data are bolded and highlighted in red."
							*CASE .cMode = "WEEKLY"
						CASE INLIST(.cMode, "WEEKLY", "ZWEEKLY")
							lcTopBodyText = "A provisional invoice for the previous pay period is attached."  + CRLF + ;
								"Managers, please review those temps you are responsible for and "  + ;
								"report any inaccuracies in Division, Department, or Hours worked to the temp agency for correction." + CRLF + ;
								"Potential problem shifts ( < " + TRANSFORM(.nMinHoursTest,"9.9") + ;
								" or > " + TRANSFORM(.nMaxHoursTest,"99.9") + ;
								" Hours Worked ), and other bad data are bolded and highlighted in red."
						CASE INLIST(.cMode,"INVOICE","DATERANGE-INVOICE")
							lcTopBodyText = "The final invoice for the previous pay period, and a matching summary file, are attached."
						CASE .cMode = "SUMMARYONLY"
							lcTopBodyText = "A summary of hours/dollars by temp for the previous pay period is attached." + CRLF + ;
								"Warning: these #s are provisional, and are subject to change via timecard review."
					ENDCASE

					IF NOT EMPTY(.cExcludedList) THEN
						lcTopBodyText = .cExcludedList + CRLF + CRLF + lcTopBodyText
					ENDIF

					lcTopBodyText = lcInvalidRegHours + CRLF + CRLF + lcTopBodyText

					*IF INLIST(.cTempCo,'A','R','T','U') THEN
						lcTopBodyText = "Headcount = " + TRANSFORM(.nHeadCount,'9999') + CRLF + lcTopBodyText
					*ENDIF

					IF .lTestMode THEN
						.cBodyText = lcTopBodyText + CRLF + CRLF + "<processing log follows>" + CRLF + CRLF + .cBodyText
					ELSE
						.cBodyText = lcTopBodyText + CRLF + CRLF + .cBodyText
					ENDIF
					
					DO CASE
						CASE .lForceAllHoursToOverTime 
							.cSubject = .cSubject + " (SPECIAL: Forced Overtime)"
						CASE .lForceAllHoursToDoubleTime
							.cSubject = .cSubject + " (SPECIAL: Forced Doubletime)"
						OTHERWISE
							* nothing
					ENDCASE

				ELSE
					* connection error
					.TrackProgress('Unable to connect to WTK System.', LOGIT+SENDIT)
				ENDIF   &&  .nSQLHandle > 0

				IF TYPE('oExcel') = 'O' AND NOT ISNULL(oExcel) THEN
					oExcel.QUIT()
				ENDIF

			CATCH TO loError

				.TrackProgress('There was an error.',LOGIT+SENDIT)
				.TrackProgress('The Error # is: ' + TRANSFORM(loError.ERRORNO), LOGIT+SENDIT)
				.TrackProgress('The Error Message is: ' + TRANSFORM(loError.MESSAGE), LOGIT+SENDIT)
				.TrackProgress('The Error occurred in Program: ' + TRANSFORM(loError.PROCEDURE), LOGIT+SENDIT)
				.TrackProgress('The Error occurred at line #: ' + TRANSFORM(loError.LINENO), LOGIT+SENDIT)
				lnNumberOfErrors = lnNumberOfErrors + 1
				IF TYPE('oExcel') = 'O' AND NOT ISNULL(oExcel) THEN
					oExcel.QUIT()
				ENDIF
				IF .nCSVHandle > -1 THEN
					=FCLOSE(.nCSVHandle)
				ENDIF
				CLOSE DATA
				*CLOSE ALL

			ENDTRY

			CLOSE DATA
			WAIT CLEAR
			***************** INTERNAL email results ******************************
			.TrackProgress('About to send status email.',LOGIT)
			.TrackProgress('==================================================================================================================', SENDIT)
			.TrackProgress('The logfile is: ' + .cLogFile, SENDIT)
			.TrackProgress('KRONOS Time process started: ' + .cStartTime, LOGIT+SENDIT)
			.TrackProgress('KRONOS Time process finished: ' + TTOC(DATETIME()), LOGIT+SENDIT)

			IF .lSendInternalEmailIsOn THEN
				* try to trap error from not having dartmail dll's registered on user's pc...
				TRY
					WAIT WINDOW TIMEOUT 10 "Delaying 10 seconds before sending main email...."
					DO FORM dartmail2 WITH .cSendTo,.cFrom,.cSubject,.cCC,.cAttach,.cBodyText,"A"
					.TrackProgress('Sent status email.',LOGIT)
				CATCH TO loError
					*=MESSAGEBOX('There was an error sending the status email. Please contact Mark Bennett.',0+64,"KRONOS Temp Time")
					.TrackProgress('There was an error sending the status email.',LOGIT)
					.TrackProgress('The Error # is: ' + TRANSFORM(loError.ERRORNO), LOGIT)
					.TrackProgress('The Error Message is: ' + TRANSFORM(loError.MESSAGE), LOGIT)
					.TrackProgress('The Error occurred in Program: ' + TRANSFORM(loError.PROCEDURE), LOGIT)
					.TrackProgress('The Error occurred at line #: ' + TRANSFORM(loError.LINENO), LOGIT)
					lnNumberOfErrors = lnNumberOfErrors + 1
					CLOSE DATA
				ENDTRY

			ELSE
				.TrackProgress('Did not send status email: THIS.lSendInternalEmailIsOn = FALSE.',LOGIT)
			ENDIF

		ENDWITH
		RETURN
	ENDFUNC && main


	PROCEDURE CreateSSGCSVFile
		LPARAMETERS tcCSVSummarySSG, tcAlias
		WITH THIS
			LOCAL lcCSVSummarySSG, lcAlias, lcLine, lnDecimals
			lcCSVSummarySSG = tcCSVSummarySSG
			lcAlias = tcAlias
			lnDecimals = SET('DECIMALS')
			SET DECIMALS TO 2
			* create a csv file based on passed filename and cursor alias, primarily for SSG
			* create without headers, per SSG
			DO CASE
				CASE lcAlias = 'CURCSVSUMMARYSSGPRE'
					IF FILE(lcCSVSummarySSG) THEN
						DELETE FILE (lcCSVSummarySSG)
					ENDIF
					.nCSVHandle = FCREATE(lcCSVSummarySSG)
					IF .nCSVHandle > -1 THEN
						SELECT (lcAlias)

						SCAN
							*!*	lcLine = ;
							*!*		.CleanUpField(ENDDATE) + "," + ;
							*!*		.CleanUpField(DIVISION) + "," + ;
							*!*		.CleanUpField(EMP_NUM) + "," + ;
							*!*		.CleanUpField(REG_HRS) + "," + ;
							*!*		.CleanUpField(OT_HRS) + "," + ;
							*!*		.CleanUpField(OT20_HRS)

							lcLine = ;
								.CleanUpField(ENDDATE) + "," + ;
								.CleanUpField(REF_NUM) + "," + ;
								.CleanUpField(EMP_NUM) + "," + ;
								.CleanUpField(REG_HRS) + "," + ;
								.CleanUpField(OT_HRS) + "," + ;
								.CleanUpField(OT20_HRS)

							=FPUTS(.nCSVHandle,lcLine)

						ENDSCAN
					ELSE
						.TrackProgress('Error opening file for FWRITE(): ' + lcCSVSummarySSG, LOGIT+SENDIT) 
					ENDIF
					=FCLOSE(.nCSVHandle)
					
					IF FILE(lcCSVSummarySSG) THEN
						.cAttach = .cAttach + "," + lcCSVSummarySSG
						.TrackProgress('Attached CSV Time File : ' + lcCSVSummarySSG, LOGIT+SENDIT)
					ELSE
						.TrackProgress('!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!', LOGIT+SENDIT)
						.TrackProgress("ERROR: There was a problem attaching CSV Time File : " + lcCSVSummarySSG, LOGIT+SENDIT)
						.TrackProgress('!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!', LOGIT+SENDIT)
					ENDIF


				OTHERWISE
					* nothing
			ENDCASE
			SET DECIMALS TO lnDecimals
		ENDWITH
		RETURN
	ENDPROC  &&  CreateSSGCSVFile


	FUNCTION CleanUpField
		* prepare field for inclusion in the string to be written with FPUTS().
		* especially critical to remove any commas, since that creates bogus column in CSV output file.
		LPARAMETERS tuValue
		LOCAL lcRetVal
		*lcRetVal = STRTRAN(ALLTRIM(TRANSFORM(tuValue)),",","")
		lcRetVal = TRANSFORM(tuValue)
		lcRetVal = STRTRAN(lcRetVal,",","")  && remove commas
		lcRetVal = ALLTRIM(lcRetVal)
		RETURN lcRetVal
	ENDFUNC


	FUNCTION IsTempUnscannable
		LPARAMETERS tcNoBioFileNum
		LOCAL llRetVal
		llRetVal = .F.
		* ADD file#s of unscannable temps to the Inlist statements below
		*llRetVal = INLIST(tcNoBioFileNum,'220385','220134')
		*llRetVal = llRetVal OR INLIST(tcNoBioFileNum,'220066','220187')
		RETURN llRetVal
	ENDFUNC


*!*		FUNCTION GetCalculatedShift
*!*			LPARAMETERS tInpunchDTM
*!*			LOCAL lcShift, lcTimePortion
*!*			lcShift = "?"
*!*			lcTimePortion = RIGHT(TTOC(tInpunchDTM),8)
*!*			WITH THIS
*!*				DO CASE
*!*					CASE .cTempCo = 'A'
*!*						* changed per Jaime 4/22/14 MB
*!*						*!*	DO CASE
*!*						*!*	CASE lcTimePortion < "13:00:00"
*!*						*!*		lcShift = "1"
*!*						*!*	CASE lcTimePortion < "20:00:00"
*!*						*!*		lcShift = "2"
*!*						*!*	OTHERWISE
*!*						*!*		lcShift = "3"
*!*						DO CASE
*!*							CASE lcTimePortion < "15:00:00"
*!*								lcShift = "1"
*!*							CASE lcTimePortion < "20:00:00"
*!*								lcShift = "2"
*!*							OTHERWISE
*!*								lcShift = "3"
*!*						ENDCASE
*!*					CASE INLIST(.cTempCo,'B','BJ','BS')
*!*						DO CASE
*!*							CASE lcTimePortion < "11:53:00"
*!*								lcShift = "1"
*!*							OTHERWISE
*!*								lcShift = "2"
*!*						ENDCASE
*!*					CASE INLIST(.cTempCo,'D')
*!*						DO CASE
*!*							CASE lcTimePortion < "16:23:00"
*!*								lcShift = "1"
*!*							OTHERWISE
*!*								lcShift = "2"
*!*						ENDCASE
*!*					CASE INLIST(.cTempCo,'E')
*!*						DO CASE
*!*							CASE lcTimePortion < "16:23:00"
*!*								lcShift = "1"
*!*							OTHERWISE
*!*								lcShift = "2"
*!*						ENDCASE
*!*					CASE INLIST(.cTempCo,'J')
*!*						DO CASE
*!*								*!*							CASE lcTimePortion < "16:23:00"
*!*							CASE lcTimePortion < "16:00:00"  && made this 4pm per ronald because some 3nd shifts are punching early
*!*								lcShift = "1"
*!*							OTHERWISE
*!*								lcShift = "2"
*!*						ENDCASE
*!*					CASE INLIST(.cTempCo,'K','K+0630','K-0630')
*!*						DO CASE
*!*							CASE lcTimePortion < "11:53:00"
*!*								lcShift = "1"
*!*							OTHERWISE
*!*								lcShift = "2"
*!*						ENDCASE
*!*					CASE INLIST(.cTempCo,'L')
*!*						DO CASE
*!*							CASE lcTimePortion < "16:23:00"
*!*								lcShift = "1"
*!*							OTHERWISE
*!*								lcShift = "2"
*!*						ENDCASE
*!*					CASE INLIST(.cTempCo,'M')
*!*						DO CASE
*!*							CASE lcTimePortion < "16:23:00"
*!*								lcShift = "1"
*!*							OTHERWISE
*!*								lcShift = "2"
*!*						ENDCASE
*!*					CASE INLIST(.cTempCo,'N')
*!*						DO CASE
*!*							CASE lcTimePortion < "16:23:00"
*!*								lcShift = "1"
*!*							OTHERWISE
*!*								lcShift = "2"
*!*						ENDCASE
*!*					CASE INLIST(.cTempCo,'O')
*!*						DO CASE
*!*							CASE lcTimePortion < "16:23:00"
*!*								lcShift = "1"
*!*							OTHERWISE
*!*								lcShift = "2"
*!*						ENDCASE
*!*					CASE INLIST(.cTempCo,'P')
*!*						DO CASE
*!*							CASE lcTimePortion < "16:23:00"
*!*								lcShift = "1"
*!*							OTHERWISE
*!*								lcShift = "2"
*!*						ENDCASE
*!*					CASE INLIST(.cTempCo,'Q')
*!*						DO CASE
*!*							CASE lcTimePortion < "16:23:00"
*!*								lcShift = "1"
*!*							OTHERWISE
*!*								lcShift = "2"
*!*						ENDCASE
*!*					CASE .cTempCo = 'R'  &&  Staffing Solutions Group - SP
*!*						DO CASE
*!*							CASE lcTimePortion < "15:00:00"
*!*								lcShift = "1"
*!*							CASE lcTimePortion < "20:00:00"
*!*								lcShift = "2"
*!*							OTHERWISE
*!*								lcShift = "3"
*!*						ENDCASE
*!*					CASE INLIST(.cTempCo,'S')  && Kamran Staffing - ML
*!*						DO CASE
*!*							CASE lcTimePortion < "11:53:00"
*!*								lcShift = "1"
*!*							OTHERWISE
*!*								lcShift = "2"
*!*						ENDCASE
*!*					CASE INLIST(.cTempCo,'T')  && Staffing Solutions Group - ML
*!*						DO CASE
*!*							CASE lcTimePortion < "11:53:00"
*!*								lcShift = "1"
*!*							OTHERWISE
*!*								lcShift = "2"
*!*						ENDCASE
*!*					CASE .cTempCo = 'U'  &&  Staffing Solutions Group - Carson
*!*						DO CASE
*!*							CASE lcTimePortion < "15:00:00"
*!*								lcShift = "1"
*!*							CASE lcTimePortion < "20:00:00"
*!*								lcShift = "2"
*!*							OTHERWISE
*!*								lcShift = "3"
*!*						ENDCASE
*!*					CASE .cTempCo = 'V'  &&  Staffing Solutions Group - Carson 2
*!*						DO CASE
*!*							CASE lcTimePortion < "15:00:00"
*!*								lcShift = "1"
*!*							CASE lcTimePortion < "20:00:00"
*!*								lcShift = "2"
*!*							OTHERWISE
*!*								lcShift = "3"
*!*						ENDCASE
*!*					OTHERWISE
*!*						* nothing
*!*				ENDCASE
*!*			ENDWITH
*!*			RETURN lcShift
*!*		ENDFUNC


	FUNCTION GetTempRefNumber
		LPARAMETERS tcDivision
		* FOR SSG, TRANSLATE DIVISION TO A TEMP REF #
		* From Michael Avidano <mavidano@tpsfoc.com>
		WITH THIS
			LOCAL lcRefNumber
			DO CASE
				CASE ALLTRIM(tcDivision) == '05'
					lcRefNumber = '282'
				CASE ALLTRIM(tcDivision) == '50'
					lcRefNumber = '285'
				CASE ALLTRIM(tcDivision) == '51'
					lcRefNumber = '284'
				CASE ALLTRIM(tcDivision) == '52'
					lcRefNumber = '241'
				CASE ALLTRIM(tcDivision) == '54'
					lcRefNumber = '270'
				CASE ALLTRIM(tcDivision) == '55'
					lcRefNumber = '395'
				CASE ALLTRIM(tcDivision) == '56'
					lcRefNumber = '271'
				CASE ALLTRIM(tcDivision) == '58'
					lcRefNumber = '269'
				CASE ALLTRIM(tcDivision) == '60'
					lcRefNumber = '273'
				CASE ALLTRIM(tcDivision) == '61'
					lcRefNumber = '268'
				CASE ALLTRIM(tcDivision) == '66'
					lcRefNumber = '378'
				CASE ALLTRIM(tcDivision) == '67'
					lcRefNumber = '379'
				CASE ALLTRIM(tcDivision) == '69'
					lcRefNumber = '459'
				OTHERWISE
					lcRefNumber = tcDivision
			ENDCASE
		ENDWITH
		RETURN lcRefNumber
	ENDFUNC


*!*		FUNCTION IsDivisionValid
*!*			LPARAMETERS tcDivision
*!*			WITH THIS
*!*				LOCAL llRetVal
*!*				llRetVal = .F.
*!*				DO CASE
*!*					CASE (LEFT(.cTempCo,1) == 'A') AND (INLIST(tcDivision,'05','52','53','54','55','56','57','58','59','60','61','62'))
*!*						llRetVal = .T.
*!*					CASE (.cTempCo == 'B') AND (INLIST(tcDivision,'50','51'))
*!*						llRetVal = .T.
*!*					CASE (.cTempCo == 'BJ') AND (INLIST(tcDivision,'51'))
*!*						llRetVal = .T.
*!*					CASE (.cTempCo == 'BS') AND (INLIST(tcDivision,'50'))
*!*						llRetVal = .T.
*!*					CASE (.cTempCo == 'D') AND (INLIST(tcDivision,'04','14','60'))
*!*						llRetVal = .T.
*!*					CASE (.cTempCo == 'E') AND (INLIST(tcDivision,'04','14'))
*!*						llRetVal = .T.
*!*					CASE (.cTempCo == 'J') AND (INLIST(tcDivision,'04','14'))
*!*						llRetVal = .T.
*!*					CASE INLIST(.cTempCo,'K','K+0630','K-0630') AND (INLIST(tcDivision,'50','51'))
*!*						llRetVal = .T.
*!*					CASE (.cTempCo == 'L') AND (INLIST(tcDivision,'15'))
*!*						llRetVal = .T.
*!*					CASE (.cTempCo == 'M') AND (INLIST(tcDivision,'62'))
*!*						llRetVal = .T.
*!*					CASE (.cTempCo == 'N') AND (INLIST(tcDivision,'15'))
*!*						llRetVal = .T.
*!*					CASE (.cTempCo == 'O') AND (INLIST(tcDivision,'15'))
*!*						llRetVal = .T.
*!*					CASE (.cTempCo == 'P') AND (INLIST(tcDivision,'15'))
*!*						llRetVal = .T.
*!*					CASE (.cTempCo == 'Q') AND (INLIST(tcDivision,'15'))
*!*						llRetVal = .T.
*!*					CASE (.cTempCo == 'R') AND (INLIST(tcDivision,'05','52','53','54','55','56','57','58','59','60','61','62'))
*!*						llRetVal = .T.
*!*					CASE (.cTempCo == 'S') AND (INLIST(tcDivision,'50','51'))
*!*						llRetVal = .T.
*!*					CASE (.cTempCo == 'T') AND (INLIST(tcDivision,'50','59'))
*!*						llRetVal = .T.
*!*					CASE (.cTempCo == 'U') AND (INLIST(tcDivision,'66','67'))
*!*						llRetVal = .T.
*!*					CASE (.cTempCo == 'V') AND (INLIST(tcDivision,'69'))
*!*						llRetVal = .T.
*!*					OTHERWISE
*!*						* nothing
*!*				ENDCASE
*!*			ENDWITH
*!*			RETURN llRetVal
*!*		ENDFUNC



*!*		FUNCTION getTempAgencyMarkupREG
*!*			LPARAMETERS tcTempCo, tcSHORTNAME, tdPAYDATE
*!*			* note: this is for Reg Hours only; separate function for OT markup
*!*			LOCAL lnMarkup
*!*			DO CASE
*!*				CASE tcTempCo = 'A'   && TriState San Pedro
*!*					DO CASE
*!*						CASE BETWEEN(tdPAYDATE,{^2001-01-01},{^2012-01-30})
*!*							lnMarkup = 1.28
*!*						CASE BETWEEN(tdPAYDATE,{^2012-01-31},{^2012-09-23})
*!*							lnMarkup = 1.275
*!*						CASE BETWEEN(tdPAYDATE,{^2012-09-24},{^2013-10-27})
*!*							lnMarkup = 1.2725
*!*						CASE BETWEEN(tdPAYDATE,{^2013-10-28},{^2015-01-04})
*!*							lnMarkup = 1.2725
*!*						CASE BETWEEN(tdPAYDATE,{^2015-01-05},{^2099-12-31})
*!*							lnMarkup = 1.28
*!*					ENDCASE

*!*					*!*				CASE INLIST(tcTempCo,'B','BJ','BS')  && Select Staffing Mira Loma
*!*					*!*					DO CASE
*!*					*!*						CASE BETWEEN(tdPAYDATE,{^2001-01-01},{^2012-12-31})
*!*					*!*							* Select indicated a flat 27.5% with no variations.
*!*					*!*							lnMarkup = 1.275
*!*					*!*						CASE BETWEEN(tdPAYDATE,{^2013-01-01},{^2099-12-31})
*!*					*!*							* Select indicated a flat 27.5% with no variations.
*!*					*!*							lnMarkup = 1.275
*!*					*!*					ENDCASE
*!*				CASE INLIST(tcTempCo,'D')  && QUALITY TEMPS - NJ
*!*					DO CASE
*!*						CASE BETWEEN(tdPAYDATE,{^2013-01-01},{^2099-12-31})
*!*							* flat 31% with no variations per Ronald 3/12/15.
*!*							lnMarkup = 1.310
*!*					ENDCASE
*!*				CASE INLIST(tcTempCo,'E')  && Wear Staff TEMPS - NJ
*!*					DO CASE
*!*						CASE BETWEEN(tdPAYDATE,{^2013-01-01},{^2099-12-31})
*!*							* ???
*!*							lnMarkup = 1.000
*!*					ENDCASE
*!*				CASE INLIST(tcTempCo,'J')  && Diamond Staffing - NJ (same markup as TriState ML per Rich Pacheco)
*!*					DO CASE
*!*						CASE BETWEEN(tdPAYDATE,{^2013-10-27},{^2015-01-04})
*!*							lnMarkup = 1.2650
*!*						CASE BETWEEN(tdPAYDATE,{^2015-01-05},{^2099-12-31})
*!*							lnMarkup = 1.275
*!*					ENDCASE
*!*					*CASE INLIST(tcTempCo,'K')
*!*				CASE INLIST(tcTempCo,'K','K+0630','K-0630')  && TriState Mira Loma
*!*					DO CASE
*!*						CASE BETWEEN(tdPAYDATE,{^2013-10-01},{^2015-01-04})
*!*							lnMarkup = 1.2650
*!*						CASE BETWEEN(tdPAYDATE,{^2015-01-05},{^2099-12-31})
*!*							lnMarkup = 1.28
*!*					ENDCASE
*!*				CASE INLIST(tcTempCo,'L')  && CRSCO Louisville
*!*					DO CASE
*!*						CASE BETWEEN(tdPAYDATE,{^2014-04-01},{^2015-01-04})
*!*							lnMarkup = 1.2650
*!*						CASE BETWEEN(tdPAYDATE,{^2015-01-05},{^2099-12-31})
*!*							lnMarkup = 1.275
*!*					ENDCASE
*!*					*!*				CASE INLIST(tcTempCo,'M')  && NJ Office Temps
*!*					*!*					DO CASE
*!*					*!*						CASE BETWEEN(tdPAYDATE,{^2015-04-27},{^2099-12-31})
*!*					*!*							lnMarkup = 1.0000
*!*					*!*					ENDCASE
*!*					*!*				CASE INLIST(tcTempCo,'N')  && LaborReady Louisville
*!*					*!*					DO CASE
*!*					*!*						CASE BETWEEN(tdPAYDATE,{^2014-04-01},{^2099-12-31})
*!*					*!*							lnMarkup = 1.3900
*!*					*!*					ENDCASE
*!*					*!*				CASE INLIST(tcTempCo,'O')  && Express Staffing - Louisville
*!*					*!*					DO CASE
*!*					*!*						CASE BETWEEN(tdPAYDATE,{^2014-04-01},{^2099-12-31})
*!*					*!*							lnMarkup = 1.3800
*!*					*!*					ENDCASE
*!*				CASE INLIST(tcTempCo,'P')  && Packaging & More - Louisville
*!*					DO CASE
*!*						CASE BETWEEN(tdPAYDATE,{^2014-04-01},{^2099-12-31})
*!*							lnMarkup = 1.4000
*!*					ENDCASE
*!*				CASE INLIST(tcTempCo,'Q')  && COR-TECH Louisville
*!*					DO CASE
*!*						CASE BETWEEN(tdPAYDATE,{^2015-03-08},{^2099-12-31})
*!*							IF "~REC" $ UPPER(tcSHORTNAME) THEN
*!*								* temp was recruited; gets higher rate - per Anthony Cox at cor-tech
*!*								lnMarkup = 1.400
*!*							ELSE
*!*								* temp was transitioned from CRSCO; regular rate
*!*								lnMarkup = 1.350
*!*							ENDIF
*!*					ENDCASE
*!*				CASE tcTempCo = 'R'   && Staffing Solutions Group - San Pedro
*!*					DO CASE
*!*						CASE BETWEEN(tdPAYDATE,{^2015-03-08},{^2015-12-31})
*!*							lnMarkup = 1.300
*!*						CASE BETWEEN(tdPAYDATE,{^2016-01-01},{^2099-12-31})
*!*							lnMarkup = 1.310
*!*					ENDCASE
*!*				CASE INLIST(tcTempCo,'S') && Kamran Staffing - Mira Loma
*!*					DO CASE
*!*						CASE BETWEEN(tdPAYDATE,{^2015-03-16},{^2099-12-31})
*!*							lnMarkup = 1.280
*!*					ENDCASE
*!*				CASE tcTempCo = 'T'   && Staffing Solutions Group - Mira Loma
*!*					DO CASE
*!*						CASE BETWEEN(tdPAYDATE,{^2015-03-16},{^2015-12-31})
*!*							lnMarkup = 1.300
*!*						CASE BETWEEN(tdPAYDATE,{^2016-01-01},{^2099-12-31})
*!*							lnMarkup = 1.310
*!*					ENDCASE
*!*				CASE tcTempCo = 'U'   && Staffing Solutions Group - Carson
*!*					DO CASE
*!*						CASE BETWEEN(tdPAYDATE,{^2015-10-01},{^2015-12-31})
*!*							lnMarkup = 1.300
*!*						CASE BETWEEN(tdPAYDATE,{^2016-01-01},{^2099-12-31})
*!*							lnMarkup = 1.310
*!*					ENDCASE
*!*				CASE tcTempCo = 'V'   && Staffing Solutions Group - Carson 2
*!*					DO CASE
*!*						CASE BETWEEN(tdPAYDATE,{^2015-10-01},{^2015-12-31})
*!*							lnMarkup = 1.300
*!*						CASE BETWEEN(tdPAYDATE,{^2016-01-01},{^2099-12-31})
*!*							lnMarkup = 1.310
*!*					ENDCASE
*!*				OTHERWISE
*!*					* unexpected temp co
*!*					.TrackProgress('****** ERROR: Reg Markup not defined for tcTempCo = ' + tcTempCo, LOGIT+SENDIT)
*!*					THROW
*!*			ENDCASE
*!*			RETURN lnMarkup
*!*		ENDFUNC	&&  getTempAgencyMarkupREG


*!*		FUNCTION getTempAgencyMarkupOT
*!*			LPARAMETERS tcTempCo, tcSHORTNAME, tdPAYDATE
*!*			* note: this is for Reg Hours only; separate function below for OT markup
*!*			LOCAL lnMarkup
*!*			DO CASE
*!*				CASE tcTempCo = 'A'  && TriState San Pedro
*!*					* Tri-State gets 28% markup unless temp is a rollover, which is 27.5% per Neftali 11/16/09
*!*					* Rollover will be indicated indicated by "ROLL" in shortname field.
*!*					DO CASE
*!*						CASE BETWEEN(tdPAYDATE,{^2001-01-01},{^2012-01-30})
*!*							lnMarkup = 1.28
*!*						CASE BETWEEN(tdPAYDATE,{^2012-01-31},{^2013-10-27})
*!*							lnMarkup = 1.24
*!*						CASE BETWEEN(tdPAYDATE,{^2013-10-28},{^2015-01-04})
*!*							lnMarkup = 1.24
*!*						CASE BETWEEN(tdPAYDATE,{^2015-01-05},{^2099-12-31})
*!*							lnMarkup = 1.255
*!*					ENDCASE

*!*					*!*				CASE INLIST(tcTempCo,'B','BJ','BS')  && Select Staffing Mira Loma
*!*					*!*					DO CASE
*!*					*!*						CASE BETWEEN(tdPAYDATE,{^2001-01-01},{^2012-12-31})
*!*					*!*							* Select indicated a flat 27.5% with no variations.
*!*					*!*							lnMarkup = 1.275
*!*					*!*						CASE BETWEEN(tdPAYDATE,{^2013-01-01},{^2099-12-31})
*!*					*!*							* Select indicated a flat 27.5% with no variations.
*!*					*!*							lnMarkup = 1.275
*!*					*!*					ENDCASE
*!*				CASE INLIST(tcTempCo,'D')  && QUALITY TEMPS - NJ
*!*					DO CASE
*!*						CASE BETWEEN(tdPAYDATE,{^2013-01-01},{^2099-12-31})
*!*							* flat 31% with no variations per Ronald 3/12/15.
*!*							lnMarkup = 1.310
*!*					ENDCASE
*!*				CASE INLIST(tcTempCo,'E')  && Wear Staff TEMPS - NJ
*!*					DO CASE
*!*						CASE BETWEEN(tdPAYDATE,{^2013-01-01},{^2099-12-31})
*!*							* ???
*!*							lnMarkup = 1.000
*!*					ENDCASE
*!*				CASE INLIST(tcTempCo,'J')  && Diamond Staffing NJ
*!*					DO CASE
*!*						CASE BETWEEN(tdPAYDATE,{^2013-10-27},{^2015-01-04})
*!*							lnMarkup = 1.2650
*!*						CASE BETWEEN(tdPAYDATE,{^2015-01-05},{^2099-12-31})
*!*							lnMarkup = 1.255
*!*					ENDCASE
*!*				CASE INLIST(tcTempCo,'K','K+0630','K-0630') && Tri-State Mira Loma
*!*					DO CASE
*!*						CASE BETWEEN(tdPAYDATE,{^2013-10-01},{^2015-01-04})
*!*							lnMarkup = 1.2650
*!*						CASE BETWEEN(tdPAYDATE,{^2015-01-05},{^2099-12-31})
*!*							lnMarkup = 1.255
*!*					ENDCASE
*!*				CASE INLIST(tcTempCo,'L')  && CRSCO KY
*!*					DO CASE
*!*						CASE BETWEEN(tdPAYDATE,{^2014-04-01},{^2015-01-04})
*!*							lnMarkup = 1.2650
*!*						CASE BETWEEN(tdPAYDATE,{^2015-01-05},{^2099-12-31})
*!*							lnMarkup = 1.255
*!*					ENDCASE
*!*					*!*				CASE INLIST(tcTempCo,'M')  && NJ Office Temps
*!*					*!*					DO CASE
*!*					*!*						CASE BETWEEN(tdPAYDATE,{^2014-04-27},{^2099-12-31})
*!*					*!*							lnMarkup = 1.0000
*!*					*!*					ENDCASE
*!*					*!*				CASE INLIST(tcTempCo,'N')  && LABORREADY KY
*!*					*!*					DO CASE
*!*					*!*						CASE BETWEEN(tdPAYDATE,{^2014-04-01},{^2099-12-31})
*!*					*!*							lnMarkup = 1.3900
*!*					*!*					ENDCASE
*!*					*!*				CASE INLIST(tcTempCo,'O')  && express staffing - Louisville
*!*					*!*					DO CASE
*!*					*!*						CASE BETWEEN(tdPAYDATE,{^2014-04-01},{^2099-12-31})
*!*					*!*							lnMarkup = 1.3800
*!*					*!*					ENDCASE
*!*				CASE INLIST(tcTempCo,'P')  && Packaging & More - Louisville
*!*					DO CASE
*!*						CASE BETWEEN(tdPAYDATE,{^2014-04-01},{^2099-12-31})
*!*							lnMarkup = 1.4000
*!*					ENDCASE
*!*				CASE INLIST(tcTempCo,'Q')  && COR-TECH - Louisville
*!*					DO CASE
*!*						CASE BETWEEN(tdPAYDATE,{^2015-03-08},{^2099-12-31})
*!*							lnMarkup = 1.350
*!*					ENDCASE
*!*				CASE tcTempCo = 'R'   && Staffing Solutions Group - San Pedro
*!*					DO CASE
*!*						CASE BETWEEN(tdPAYDATE,{^2015-03-08},{^2099-12-31})
*!*							lnMarkup = 1.290
*!*					ENDCASE
*!*				CASE INLIST(tcTempCo,'S') && Kamran Staffing - Mira Loma
*!*					DO CASE
*!*						CASE BETWEEN(tdPAYDATE,{^2015-03-16},{^2099-12-31})
*!*							lnMarkup = 1.250
*!*					ENDCASE
*!*				CASE tcTempCo = 'T'   && Staffing Solutions Group - Mira Loma
*!*					DO CASE
*!*						CASE BETWEEN(tdPAYDATE,{^2015-03-16},{^2099-12-31})
*!*							lnMarkup = 1.290
*!*					ENDCASE
*!*				CASE tcTempCo = 'U'   && Staffing Solutions Group - Carson
*!*					DO CASE
*!*						CASE BETWEEN(tdPAYDATE,{^2015-03-08},{^2099-12-31})
*!*							lnMarkup = 1.290
*!*					ENDCASE
*!*				CASE tcTempCo = 'V'   && Staffing Solutions Group - Carson 2
*!*					DO CASE
*!*						CASE BETWEEN(tdPAYDATE,{^2015-03-08},{^2099-12-31})
*!*							lnMarkup = 1.290
*!*					ENDCASE
*!*				OTHERWISE
*!*					* unexpected temp co
*!*					.TrackProgress('****** ERROR: OT Markup not defined for tcTempCo = ' + tcTempCo, LOGIT+SENDIT)
*!*					THROW
*!*			ENDCASE
*!*			RETURN lnMarkup
*!*		ENDFUNC	&& getTempAgencyMarkupOT


*!*		FUNCTION getShiftDiff
*!*			LPARAMETERS tcTempCo, tcCalcdShift, tcDept, tnBaseWage
*!*			LOCAL lnShiftDiff, lcDept, lcCalcdShift
*!*			lnShiftDiff = 0.00
*!*			lcDept = ALLTRIM( tcDept )
*!*			lcCalcdShift = ALLTRIM( tcCalcdShift )
*!*			DO CASE
*!*				CASE tcTempCo = 'A'
*!*					*!*	* Tri-State rules, updated per Neftali for base wage check 10/27/09 MB.
*!*					*!*	* basic rules for shift differential:
*!*					*!*	* if lcDept = '0622' (forklift drivers) or CalcdShift = '1' or base wage > 8.00 then no differential
*!*					*!*	* otherwise, give differential.
*!*					*!*	DO CASE
*!*					*!*		CASE (lcDept = '0622') OR (lcCalcdShift = '1') OR (tnBaseWage > 8.00)
*!*					*!*			lnShiftDiff = 0.00
*!*					*!*		OTHERWISE
*!*					*!*			lnShiftDiff = 0.20
*!*					*!*	ENDCASE

*!*					lnShiftDiff = 0.00

*!*				CASE INLIST(tcTempCo,'B','BJ','BS')
*!*					* Select rules, per Alex Figueroa and Juan Santisteban 2/10/10 MB.
*!*					* basic rules for shift differential:
*!*					* simply, if temp works 2nd shift, they get $0.20 extra

*!*					* PER JUAN S 2/12/13 - THEY ARE PUTTING SHIFT DIFF IN BASE WAGE - SHOULD BE ZERO HERE
*!*					lnShiftDiff = 0.00

*!*					*!*	DO CASE
*!*					*!*		CASE (lcCalcdShift = '1')
*!*					*!*			lnShiftDiff = 0.00
*!*					*!*		OTHERWISE
*!*					*!*			lnShiftDiff = 0.20
*!*					*!*	ENDCASE

*!*				CASE INLIST(tcTempCo,'D')
*!*					* Quality rules, per Jim Lake 2/6/13.
*!*					* basic rules for shift differential:
*!*					* simply, if temp works 2nd shift, they get $0.50 extra
*!*					DO CASE
*!*						CASE (lcCalcdShift = '1')
*!*							lnShiftDiff = 0.00
*!*						OTHERWISE
*!*							*lnShiftDiff = 0.50
*!*							lnShiftDiff = 0.00  && PER rONALD/jIM 3/23/2015 - they will put shift diff in base wage
*!*					ENDCASE
*!*				CASE INLIST(tcTempCo,'E')
*!*					* Wear Staff.
*!*					* basic rules for shift differential:
*!*					* simply, if temp works 2nd shift, they get $0.40 extra
*!*					DO CASE
*!*						CASE (lcCalcdShift = '1')
*!*							lnShiftDiff = 0.00
*!*						OTHERWISE
*!*							lnShiftDiff = 0.00
*!*					ENDCASE
*!*				CASE INLIST(tcTempCo,'J')
*!*					* Diamond Staffing NJ.
*!*					DO CASE
*!*						CASE (lcCalcdShift = '1')
*!*							lnShiftDiff = 0.00
*!*						OTHERWISE
*!*							lnShiftDiff = 0.00
*!*					ENDCASE
*!*					*CASE tcTempCo = 'K'
*!*				CASE INLIST(tcTempCo,'K','K+0630','K-0630')
*!*					* Tri-State Mira Loma
*!*					lnShiftDiff = 0.00
*!*				CASE INLIST(tcTempCo,'L')
*!*					* CRSCO ky.
*!*					DO CASE
*!*						CASE (lcCalcdShift = '1')
*!*							lnShiftDiff = 0.00
*!*						OTHERWISE
*!*							lnShiftDiff = 0.00
*!*					ENDCASE
*!*				CASE tcTempCo = 'M'
*!*					* NJ Office Temps
*!*					lnShiftDiff = 0.00
*!*				CASE INLIST(tcTempCo,'N')
*!*					* CRSCO ky.
*!*					DO CASE
*!*						CASE (lcCalcdShift = '1')
*!*							lnShiftDiff = 0.00
*!*						OTHERWISE
*!*							lnShiftDiff = 0.00
*!*					ENDCASE
*!*				CASE INLIST(tcTempCo,'O')
*!*					* express ky.
*!*					DO CASE
*!*						CASE (lcCalcdShift = '1')
*!*							lnShiftDiff = 0.00
*!*						OTHERWISE
*!*							lnShiftDiff = 0.00
*!*					ENDCASE
*!*				CASE INLIST(tcTempCo,'P')
*!*					* && Packaging & More - Louisville
*!*					DO CASE
*!*						CASE (lcCalcdShift = '1')
*!*							lnShiftDiff = 0.00
*!*						OTHERWISE
*!*							lnShiftDiff = 0.00
*!*					ENDCASE
*!*				CASE INLIST(tcTempCo,'Q')
*!*					* COR-TECH ky.
*!*					DO CASE
*!*						CASE (lcCalcdShift = '1')
*!*							lnShiftDiff = 0.00
*!*						OTHERWISE
*!*							lnShiftDiff = 0.00
*!*					ENDCASE
*!*				CASE tcTempCo = 'R'
*!*					lnShiftDiff = 0.00
*!*				CASE tcTempCo = 'S'
*!*					lnShiftDiff = 0.00
*!*				CASE tcTempCo = 'T'
*!*					lnShiftDiff = 0.00
*!*				CASE tcTempCo = 'U'
*!*					lnShiftDiff = 0.00
*!*				CASE tcTempCo = 'V'
*!*					lnShiftDiff = 0.00
*!*				OTHERWISE
*!*					lnShiftDiff = 0.00
*!*			ENDCASE
*!*			RETURN lnShiftDiff
*!*		ENDFUNC


	PROCEDURE BuildDailyCursor
		LPARAMETERS tdPass2Date
		WITH THIS
			LOCAL lcSQLStartDate, lcSQLEndDate, lcSQLPass2, lcSQLPass2A

			SET DATE YMD

			*	 create "YYYYMMDD" safe date format for use in SQL query
			* NOTE: for SQL we are going to use < EndDate ( not <= ), so we are going to add 1 to that date here for querying purposes.
			lcSQLStartDate = STRTRAN(DTOC(tdPass2Date),"/","")
			lcSQLEndDate = STRTRAN(DTOC(tdPass2Date + 1),"/","")

			SET DATE AMERICAN

			IF USED('CURWTKHOURSTEMPPRE') THEN
				USE IN CURWTKHOURSTEMPPRE
			ENDIF
			IF USED('CURWTKHOURSTEMP') THEN
				USE IN CURWTKHOURSTEMP
			ENDIF
			IF USED('CURWAGES') THEN
				USE IN CURWAGES
			ENDIF

			*!*				* filter by Temp Co # if there was a passed tempco parameter
			*!*				IF NOT EMPTY(.cTempCo) THEN
			*!*					.cTempCoWhere = " AND (D.LABORLEV5NM = '" + .cTempCo + "') "
			*!*				ELSE
			*!*					.cTempCoWhere = ""
			*!*				ENDIF

			* pass 2 SQL
			* the join to basewagehistory table was causing duplicate records; had to get wage from separate sql
			* 9/23/09 MB

			* pass 2 SQL
			lcSQLPass2 = ;
				" SELECT " + ;
				" '2' AS CPASS, " + ;
				" SPACE(3) AS DIVISION, " + ;
				" D.LABORLEV5NM AS AGENCYNUM, " + ;
				" D.LABORLEV5DSC AS AGENCY, " + ;
				" D.LABORLEV2NM AS DIVISION2, " + ;
				" D.LABORLEV3NM AS DEPT, " + ;
				" D.LABORLEV6NM AS CLIENTCODE, " + ;
				" D.LABORLEV6DSC AS CLIENTDESC, " + ;
				" D.LABORLEV7NM AS TIMERVWR, " + ;
				" C.FULLNM AS EMPLOYEE, " + ;
				" C.SHORTNM AS SHORTNAME, " + ;
				" C.PERSONNUM AS FILE_NUM, " + ;
				" C.PERSONID, " + ;
				" A.EMPLOYEEID, " + ;
				" E.ABBREVIATIONCHAR AS PAYCODE, " + ;
				" E.NAME AS PAYCODEDESC, " + ;
				" (SUM(A.DURATIONSECSQTY) / 3600.00) AS TOTHOURS, " + ;
				" SUM(A.MONEYAMT) AS TOTPAY, " + ;
				" 000000.0000 AS WAGE " + ;
				" FROM WFCTOTAL A " + ;
				" JOIN WTKEMPLOYEE B " + ;
				" ON B.EMPLOYEEID = A.EMPLOYEEID " + ;
				" JOIN PERSON C " + ;
				" ON C.PERSONID = B.PERSONID " + ;
				" JOIN LABORACCT D " + ;
				" ON D.LABORACCTID = A.LABORACCTID " + ;
				" JOIN PAYCODE E " + ;
				" ON E.PAYCODEID = A.PAYCODEID " + ;
				" WHERE (A.APPLYDTM >= '" + lcSQLStartDate + "')" + ;
				" AND (A.APPLYDTM < '" + lcSQLEndDate + "')" + ;
				" AND E.NAME IN ('Regular Hours','Overtime Hours','Doubltime Hours','Bonus Pay') " + ;
				.cTempCoWhere + .cExtraWhere + .cExtraWhere3 + ;
				" GROUP BY D.LABORLEV5NM, D.LABORLEV5DSC, D.LABORLEV2NM, D.LABORLEV3NM, D.LABORLEV6NM, D.LABORLEV6DSC, D.LABORLEV7NM, C.FULLNM, C.SHORTNM, C.PERSONNUM, C.PERSONID, A.EMPLOYEEID, E.ABBREVIATIONCHAR, E.NAME " + ;
				" ORDER BY 1, 2, 3, 4, 5, 6, 7, 8, 9 "

			*!*	" AND (D.LABORLEV1NM = 'TMP') " + ;


			lcSQLPass2A = ;
				" SELECT " + ;
				" EMPLOYEEID, " + ;
				" {fn ifnull(BASEWAGEHOURLYAMT,0.00)} AS WAGE, " + ;
				" EFFECTIVEDTM, " + ;
				" EXPIRATIONDTM " + ;
				" FROM BASEWAGERTHIST " + ;
				" WHERE '" + lcSQLStartDate + "' BETWEEN EFFECTIVEDTM AND EXPIRATIONDTM " + ;
				" ORDER BY EMPLOYEEID DESC, EXPIRATIONDTM DESC "

			IF .lTestMode THEN
				.TrackProgress('', LOGIT+SENDIT)
				.TrackProgress('lcSQLPass2 =' + lcSQLPass2, LOGIT+SENDIT)
				.TrackProgress('', LOGIT+SENDIT)
				.TrackProgress('lcSQLPass2A =' + lcSQLPass2A, LOGIT+SENDIT)
			ENDIF

			IF .ExecSQL(lcSQLPass2, 'CURWTKHOURSTEMPPRE', RETURN_DATA_NOT_MANDATORY) AND ;
					.ExecSQL(lcSQLPass2A, 'CURWAGES', RETURN_DATA_MANDATORY) THEN

				IF USED('CURWTKHOURSTEMPPRE') AND NOT EOF('CURWTKHOURSTEMPPRE') THEN

					* data was returned

					* add a foxpro date field to the cursor
					SELECT tdPass2Date AS PAYDATE, * ;
						FROM CURWTKHOURSTEMPPRE ;
						INTO CURSOR CURWTKHOURSTEMP ;
						ORDER BY EMPLOYEE ;
						READWRITE

					*!*	SELECT CURWTKHOURSTEMP
					*!*	BROWSE
*!*	SELECT CURWAGES
*!*	BROWSE

					* populate WAGE field in CURWTKHOURSTEMP
					SELECT CURWTKHOURSTEMP
					SCAN
						SELECT CURWAGES
						LOCATE FOR EMPLOYEEID = CURWTKHOURSTEMP.EMPLOYEEID
						IF FOUND() THEN
							REPLACE CURWTKHOURSTEMP.WAGE WITH CURWAGES.WAGE IN CURWTKHOURSTEMP
						ENDIF
					ENDSCAN

					*!*	SELECT CURWTKHOURSTEMP
					*!*	BROWSE


					* now append it to CURWTKHOURS
					SELECT CURWTKHOURS
					APPEND FROM DBF('CURWTKHOURSTEMP')


				ENDIF  &&  USED('CURWTKHOURSTEMPPRE') AND NOT EOF('CURWTKHOURSTEMPPRE')

			ENDIF  &&  .ExecSQL(lcSQLPass2, 'CURWTKHOURSTEMPPRE', RETURN_DATA_NOT_MANDATORY)

		ENDWITH
		RETURN
	ENDPROC && BuildDailyCursor



	FUNCTION ExecSQL
		LPARAMETERS tcSQL, tcCursorName, tlNoDataReturnedIsOkay
		LOCAL llRetVal, lnResult
		WITH THIS
			* close target cursor if it's open
			IF USED(tcCursorName)
				USE IN (tcCursorName)
			ENDIF
			WAIT WINDOW "Fetching remote data; please wait..." NOWAIT
			lnResult = SQLEXEC(.nSQLHandle, tcSQL, tcCursorName)
			llRetVal = ( lnResult > 0 )
			IF llRetVal THEN
				* see if any data came back
				IF NOT tlNoDataReturnedIsOkay THEN
					IF (NOT USED(tcCursorName)) OR EOF(tcCursorName) THEN
						llRetVal = .F.
						.TrackProgress('No data was returned by this query: ' +CRLF + tcSQL, LOGIT+SENDIT)
					ENDIF
				ENDIF
			ELSE
				.TrackProgress("Result Code [" + TRANSFORM(lnResult) + "] returned by this query: " + tcSQL, LOGIT+SENDIT)
			ENDIF
			WAIT CLEAR
			RETURN llRetVal
		ENDWITH
	ENDFUNC


	PROCEDURE TrackProgress
		* do any combination of Wait Window, writing to logfile, and adding to body of email,
		* based on nFlags parameter.
		LPARAMETERS tcExpression, tnFlags
		LOCAL lcCRLF
		WITH THIS
			IF BITAND(tnFlags,NOCRLF) = NOCRLF THEN
				lcCRLF = ""
			ELSE
				lcCRLF = CRLF
			ENDIF
			IF BITAND(tnFlags,LOGIT) = LOGIT THEN
				IF .lLoggingIsOn THEN
					?
					? .cProcessName + "  " + TTOC(DATETIME()) + ": " + tcExpression
				ENDIF
			ENDIF
			IF .lWaitWindowIsOn THEN
				IF BITAND(tnFlags,NOWAITIT) = NOWAITIT THEN
					WAIT WINDOW tcExpression NOWAIT
				ENDIF
				IF BITAND(tnFlags,WAITIT) = WAITIT THEN
					WAIT WINDOW tcExpression TIMEOUT .nWaitWindowTimeout
				ENDIF
			ENDIF
			IF BITAND(tnFlags,SENDIT) = SENDIT THEN
				IF .lSendInternalEmailIsOn THEN
					.cBodyText = .cBodyText + tcExpression + CRLF + lcCRLF
				ENDIF
			ENDIF
		ENDWITH
	ENDPROC  &&  TrackProgress


*!*		PROCEDURE SetTempCo
*!*			LPARAMETERS tcTempCo
*!*			WITH THIS
*!*				.nRoundingType = 1  && standard rounding
*!*				DO CASE
*!*					CASE tcTempCo == 'A'
*!*						.cTempCo = tcTempCo
*!*						.cTempCoName = 'Tri-State SP'
*!*						.lCheckBio = .T.
*!*						.nMinHoursTest = 2
*!*						.nMaxHoursTest = 10
*!*						IF .lTestMode THEN
*!*							.TrackProgress('Set TempCo = ' + tcTempCo,LOGIT+SENDIT)
*!*							.TrackProgress('Set TempCoName = ' + .cTempCoName,LOGIT+SENDIT)
*!*						ELSE
*!*							.TrackProgress('Set TempCo = ' + tcTempCo,LOGIT)
*!*							.TrackProgress('Set TempCoName = ' + .cTempCoName,LOGIT)
*!*						ENDIF
*!*					CASE tcTempCo == 'AJ'
*!*						.cTempCo = tcTempCo
*!*						.cTempCoName = 'Tri-State SP Jacques Moret'
*!*						.lCheckBio = .T.
*!*						.nMinHoursTest = 2
*!*						.nMaxHoursTest = 10
*!*						IF .lTestMode THEN
*!*							.TrackProgress('Set TempCo = ' + tcTempCo,LOGIT+SENDIT)
*!*							.TrackProgress('Set TempCoName = ' + .cTempCoName,LOGIT+SENDIT)
*!*						ELSE
*!*							.TrackProgress('Set TempCo = ' + tcTempCo,LOGIT)
*!*							.TrackProgress('Set TempCoName = ' + .cTempCoName,LOGIT)
*!*						ENDIF
*!*					CASE tcTempCo == 'A!J'
*!*						.cTempCo = tcTempCo
*!*						.cTempCoName = 'Tri-State SP Non-Jacques Moret'
*!*						.lCheckBio = .T.
*!*						.nMinHoursTest = 2
*!*						.nMaxHoursTest = 10
*!*						IF .lTestMode THEN
*!*							.TrackProgress('Set TempCo = ' + tcTempCo,LOGIT+SENDIT)
*!*							.TrackProgress('Set TempCoName = ' + .cTempCoName,LOGIT+SENDIT)
*!*						ELSE
*!*							.TrackProgress('Set TempCo = ' + tcTempCo,LOGIT)
*!*							.TrackProgress('Set TempCoName = ' + .cTempCoName,LOGIT)
*!*						ENDIF
*!*					CASE tcTempCo == 'A2'
*!*						.cTempCo = tcTempCo
*!*						.cTempCoName = 'Tri-State SP 2WIRE'
*!*						.lCheckBio = .T.
*!*						.nMinHoursTest = 2
*!*						.nMaxHoursTest = 10
*!*						IF .lTestMode THEN
*!*							.TrackProgress('Set TempCo = ' + tcTempCo,LOGIT+SENDIT)
*!*							.TrackProgress('Set TempCoName = ' + .cTempCoName,LOGIT+SENDIT)
*!*						ELSE
*!*							.TrackProgress('Set TempCo = ' + tcTempCo,LOGIT)
*!*							.TrackProgress('Set TempCoName = ' + .cTempCoName,LOGIT)
*!*						ENDIF
*!*					CASE tcTempCo == 'A!2'
*!*						.cTempCo = tcTempCo
*!*						.cTempCoName = 'Tri-State SP Non-2WIRE'
*!*						.lCheckBio = .T.
*!*						.nMinHoursTest = 2
*!*						.nMaxHoursTest = 10
*!*						IF .lTestMode THEN
*!*							.TrackProgress('Set TempCo = ' + tcTempCo,LOGIT+SENDIT)
*!*							.TrackProgress('Set TempCoName = ' + .cTempCoName,LOGIT+SENDIT)
*!*						ELSE
*!*							.TrackProgress('Set TempCo = ' + tcTempCo,LOGIT)
*!*							.TrackProgress('Set TempCoName = ' + .cTempCoName,LOGIT)
*!*						ENDIF
*!*					CASE tcTempCo == 'B'
*!*						.cTempCo = tcTempCo
*!*						.cTempCoName = 'Select ML '
*!*						.lCheckBio = .F.

*!*						*.nRoundingType = 1  && standard rounding
*!*						*.nRoundingType = 2  && no rounding
*!*						.nRoundingType = 3 &&  SELECT ROUNDING

*!*						.nMinHoursTest = 8
*!*						.nMaxHoursTest = 10
*!*						IF .lTestMode THEN
*!*							.TrackProgress('Set TempCo = ' + tcTempCo,LOGIT+SENDIT)
*!*							.TrackProgress('Set TempCoName = ' + .cTempCoName,LOGIT+SENDIT)
*!*						ELSE
*!*							.TrackProgress('Set TempCo = ' + tcTempCo,LOGIT)
*!*							.TrackProgress('Set TempCoName = ' + .cTempCoName,LOGIT)
*!*						ENDIF
*!*						*!*					CASE tcTempCo == 'BJ'
*!*						*!*						.cTempCo = tcTempCo
*!*						*!*						.cTempCoName = 'Select ML Jacques Moret '
*!*						*!*						.lCheckBio = .F.
*!*						*!*						.nMinHoursTest = 8
*!*						*!*						.nMaxHoursTest = 10
*!*						*!*						IF .lTestMode THEN
*!*						*!*							.TrackProgress('Set TempCo = ' + tcTempCo,LOGIT+SENDIT)
*!*						*!*							.TrackProgress('Set TempCoName = ' + .cTempCoName,LOGIT+SENDIT)
*!*						*!*						ELSE
*!*						*!*							.TrackProgress('Set TempCo = ' + tcTempCo,LOGIT)
*!*						*!*							.TrackProgress('Set TempCoName = ' + .cTempCoName,LOGIT)
*!*						*!*						ENDIF
*!*						*!*					CASE tcTempCo == 'BM'
*!*						*!*						.cTempCo = tcTempCo
*!*						*!*						.cTempCoName = 'Select ML Maintenance '
*!*						*!*						.lCheckBio = .F.
*!*						*!*						.nMinHoursTest = 8
*!*						*!*						.nMaxHoursTest = 10
*!*						*!*						IF .lTestMode THEN
*!*						*!*							.TrackProgress('Set TempCo = ' + tcTempCo,LOGIT+SENDIT)
*!*						*!*							.TrackProgress('Set TempCoName = ' + .cTempCoName,LOGIT+SENDIT)
*!*						*!*						ELSE
*!*						*!*							.TrackProgress('Set TempCo = ' + tcTempCo,LOGIT)
*!*						*!*							.TrackProgress('Set TempCoName = ' + .cTempCoName,LOGIT)
*!*						*!*						ENDIF
*!*						*!*					CASE tcTempCo == 'BS'
*!*						*!*						.cTempCo = tcTempCo
*!*						*!*						.cTempCoName = 'Select ML Sears-SHC '
*!*						*!*						.lCheckBio = .F.
*!*						*!*						.nMinHoursTest = 8
*!*						*!*						.nMaxHoursTest = 10
*!*						*!*						IF .lTestMode THEN
*!*						*!*							.TrackProgress('Set TempCo = ' + tcTempCo,LOGIT+SENDIT)
*!*						*!*							.TrackProgress('Set TempCoName = ' + .cTempCoName,LOGIT+SENDIT)
*!*						*!*						ELSE
*!*						*!*							.TrackProgress('Set TempCo = ' + tcTempCo,LOGIT)
*!*						*!*							.TrackProgress('Set TempCoName = ' + .cTempCoName,LOGIT)
*!*						*!*						ENDIF
*!*					CASE tcTempCo == 'D'

*!*						.cTempCo = tcTempCo
*!*						.cTempCoName = 'Quality Temps NJ'
*!*						.lCheckBio = .T.
*!*						.nMinHoursTest = 2
*!*						.nMaxHoursTest = 10
*!*						IF .lTestMode THEN
*!*							.TrackProgress('Set TempCo = ' + tcTempCo,LOGIT+SENDIT)
*!*							.TrackProgress('Set TempCoName = ' + .cTempCoName,LOGIT+SENDIT)
*!*						ELSE
*!*							.TrackProgress('Set TempCo = ' + tcTempCo,LOGIT)
*!*							.TrackProgress('Set TempCoName = ' + .cTempCoName,LOGIT)
*!*						ENDIF
*!*					CASE tcTempCo == 'E'
*!*						.cTempCo = tcTempCo
*!*						.cTempCoName = 'Wear Staff Temps NJ'
*!*						.lCheckBio = .T.
*!*						.nMinHoursTest = 2
*!*						.nMaxHoursTest = 10
*!*						IF .lTestMode THEN
*!*							.TrackProgress('Set TempCo = ' + tcTempCo,LOGIT+SENDIT)
*!*							.TrackProgress('Set TempCoName = ' + .cTempCoName,LOGIT+SENDIT)
*!*						ELSE
*!*							.TrackProgress('Set TempCo = ' + tcTempCo,LOGIT)
*!*							.TrackProgress('Set TempCoName = ' + .cTempCoName,LOGIT)
*!*						ENDIF
*!*					CASE tcTempCo == 'HJB'
*!*						* report on SP2 hourly for Jaime Barba
*!*						.cTempCo = tcTempCo
*!*						.cTempCoName = 'Hourly - San Pedro '
*!*						.lCheckBio = .T.
*!*						.nMinHoursTest = 8
*!*						.nMaxHoursTest = 10
*!*						IF .lTestMode THEN
*!*							.TrackProgress('Set TempCo = ' + tcTempCo,LOGIT+SENDIT)
*!*							.TrackProgress('Set TempCoName = ' + .cTempCoName,LOGIT+SENDIT)
*!*						ELSE
*!*							.TrackProgress('Set TempCo = ' + tcTempCo,LOGIT)
*!*							.TrackProgress('Set TempCoName = ' + .cTempCoName,LOGIT)
*!*						ENDIF
*!*					CASE tcTempCo == 'HMG'
*!*						* report on RIALTO hourly for Mark Goldberg
*!*						.cTempCo = tcTempCo
*!*						.cTempCoName = 'Hourly - Div 59 '
*!*						.lCheckBio = .T.
*!*						.nMinHoursTest = 8
*!*						.nMaxHoursTest = 10
*!*						IF .lTestMode THEN
*!*							.TrackProgress('Set TempCo = ' + tcTempCo,LOGIT+SENDIT)
*!*							.TrackProgress('Set TempCoName = ' + .cTempCoName,LOGIT+SENDIT)
*!*						ELSE
*!*							.TrackProgress('Set TempCo = ' + tcTempCo,LOGIT)
*!*							.TrackProgress('Set TempCoName = ' + .cTempCoName,LOGIT)
*!*						ENDIF
*!*					CASE tcTempCo == 'J'
*!*						.cTempCo = tcTempCo
*!*						.cTempCoName = 'Diamond Staffing NJ'
*!*						.lCheckBio = .T.
*!*						.nMinHoursTest = 2
*!*						.nMaxHoursTest = 10
*!*						IF .lTestMode THEN
*!*							.TrackProgress('Set TempCo = ' + tcTempCo,LOGIT+SENDIT)
*!*							.TrackProgress('Set TempCoName = ' + .cTempCoName,LOGIT+SENDIT)
*!*						ELSE
*!*							.TrackProgress('Set TempCo = ' + tcTempCo,LOGIT)
*!*							.TrackProgress('Set TempCoName = ' + .cTempCoName,LOGIT)
*!*						ENDIF
*!*						*CASE tcTempCo == 'K'
*!*					CASE INLIST(tcTempCo,'K','K+0630','K-0630')
*!*						.cTempCo = tcTempCo
*!*						.cTempCoName = 'Tri-State ML'
*!*						.lCheckBio = .T.
*!*						.nMinHoursTest = 2
*!*						.nMaxHoursTest = 10
*!*						IF .lTestMode THEN
*!*							.TrackProgress('Set TempCo = ' + tcTempCo,LOGIT+SENDIT)
*!*							.TrackProgress('Set TempCoName = ' + .cTempCoName,LOGIT+SENDIT)
*!*						ELSE
*!*							.TrackProgress('Set TempCo = ' + tcTempCo,LOGIT)
*!*							.TrackProgress('Set TempCoName = ' + .cTempCoName,LOGIT)
*!*						ENDIF
*!*					CASE tcTempCo == 'L'
*!*						.cTempCo = tcTempCo
*!*						.cTempCoName = 'CRSCO - Louisville'
*!*						.lCheckBio = .T.
*!*						.nMinHoursTest = 2
*!*						.nMaxHoursTest = 10
*!*						IF .lTestMode THEN
*!*							.TrackProgress('Set TempCo = ' + tcTempCo,LOGIT+SENDIT)
*!*							.TrackProgress('Set TempCoName = ' + .cTempCoName,LOGIT+SENDIT)
*!*						ELSE
*!*							.TrackProgress('Set TempCo = ' + tcTempCo,LOGIT)
*!*							.TrackProgress('Set TempCoName = ' + .cTempCoName,LOGIT)
*!*						ENDIF
*!*					CASE tcTempCo == 'M'
*!*						.cTempCo = tcTempCo
*!*						.cTempCoName = 'NJ Office Temps'
*!*						.lCheckBio = .T.
*!*						.nMinHoursTest = 2
*!*						.nMaxHoursTest = 10
*!*						IF .lTestMode THEN
*!*							.TrackProgress('Set TempCo = ' + tcTempCo,LOGIT+SENDIT)
*!*							.TrackProgress('Set TempCoName = ' + .cTempCoName,LOGIT+SENDIT)
*!*						ELSE
*!*							.TrackProgress('Set TempCo = ' + tcTempCo,LOGIT)
*!*							.TrackProgress('Set TempCoName = ' + .cTempCoName,LOGIT)
*!*						ENDIF
*!*					CASE tcTempCo == 'N'
*!*						.cTempCo = tcTempCo
*!*						.cTempCoName = 'LaborReady - Louisville'
*!*						.lCheckBio = .T.
*!*						.nMinHoursTest = 2
*!*						.nMaxHoursTest = 10
*!*						IF .lTestMode THEN
*!*							.TrackProgress('Set TempCo = ' + tcTempCo,LOGIT+SENDIT)
*!*							.TrackProgress('Set TempCoName = ' + .cTempCoName,LOGIT+SENDIT)
*!*						ELSE
*!*							.TrackProgress('Set TempCo = ' + tcTempCo,LOGIT)
*!*							.TrackProgress('Set TempCoName = ' + .cTempCoName,LOGIT)
*!*						ENDIF
*!*					CASE tcTempCo == 'O'
*!*						.cTempCo = tcTempCo
*!*						.cTempCoName = 'Express Staffing - Louisville'
*!*						.lCheckBio = .T.
*!*						.nMinHoursTest = 2
*!*						.nMaxHoursTest = 10
*!*						IF .lTestMode THEN
*!*							.TrackProgress('Set TempCo = ' + tcTempCo,LOGIT+SENDIT)
*!*							.TrackProgress('Set TempCoName = ' + .cTempCoName,LOGIT+SENDIT)
*!*						ELSE
*!*							.TrackProgress('Set TempCo = ' + tcTempCo,LOGIT)
*!*							.TrackProgress('Set TempCoName = ' + .cTempCoName,LOGIT)
*!*						ENDIF
*!*					CASE tcTempCo == 'P'
*!*						.cTempCo = tcTempCo
*!*						.cTempCoName = 'Packaging & More LLC - Louisville'
*!*						.lCheckBio = .T.
*!*						.nMinHoursTest = 2
*!*						.nMaxHoursTest = 10
*!*						IF .lTestMode THEN
*!*							.TrackProgress('Set TempCo = ' + tcTempCo,LOGIT+SENDIT)
*!*							.TrackProgress('Set TempCoName = ' + .cTempCoName,LOGIT+SENDIT)
*!*						ELSE
*!*							.TrackProgress('Set TempCo = ' + tcTempCo,LOGIT)
*!*							.TrackProgress('Set TempCoName = ' + .cTempCoName,LOGIT)
*!*						ENDIF
*!*					CASE tcTempCo == 'Q'
*!*						.cTempCo = tcTempCo
*!*						.cTempCoName = 'Cor-Tech - Louisville'
*!*						.lCheckBio = .T.
*!*						.nMinHoursTest = 2
*!*						.nMaxHoursTest = 10
*!*						IF .lTestMode THEN
*!*							.TrackProgress('Set TempCo = ' + tcTempCo,LOGIT+SENDIT)
*!*							.TrackProgress('Set TempCoName = ' + .cTempCoName,LOGIT+SENDIT)
*!*						ELSE
*!*							.TrackProgress('Set TempCo = ' + tcTempCo,LOGIT)
*!*							.TrackProgress('Set TempCoName = ' + .cTempCoName,LOGIT)
*!*						ENDIF
*!*					CASE tcTempCo == 'R'
*!*						.cTempCo = tcTempCo
*!*						.cTempCoName = 'Staffing Solutions - SP'
*!*						.lCheckBio = .T.
*!*						.nMinHoursTest = 2
*!*						.nMaxHoursTest = 10
*!*						IF .lTestMode THEN
*!*							.TrackProgress('Set TempCo = ' + tcTempCo,LOGIT+SENDIT)
*!*							.TrackProgress('Set TempCoName = ' + .cTempCoName,LOGIT+SENDIT)
*!*						ELSE
*!*							.TrackProgress('Set TempCo = ' + tcTempCo,LOGIT)
*!*							.TrackProgress('Set TempCoName = ' + .cTempCoName,LOGIT)
*!*						ENDIF
*!*					CASE INLIST(tcTempCo,'S')
*!*						.cTempCo = tcTempCo
*!*						.cTempCoName = 'Kamran Staffing - ML'
*!*						.lCheckBio = .T.
*!*						.nMinHoursTest = 2
*!*						.nMaxHoursTest = 10
*!*						IF .lTestMode THEN
*!*							.TrackProgress('Set TempCo = ' + tcTempCo,LOGIT+SENDIT)
*!*							.TrackProgress('Set TempCoName = ' + .cTempCoName,LOGIT+SENDIT)
*!*						ELSE
*!*							.TrackProgress('Set TempCo = ' + tcTempCo,LOGIT)
*!*							.TrackProgress('Set TempCoName = ' + .cTempCoName,LOGIT)
*!*						ENDIF
*!*					CASE tcTempCo == 'T'
*!*						.cTempCo = tcTempCo
*!*						.cTempCoName = 'Staffing Solutions - ML'
*!*						.lCheckBio = .T.
*!*						.nMinHoursTest = 2
*!*						.nMaxHoursTest = 10
*!*						IF .lTestMode THEN
*!*							.TrackProgress('Set TempCo = ' + tcTempCo,LOGIT+SENDIT)
*!*							.TrackProgress('Set TempCoName = ' + .cTempCoName,LOGIT+SENDIT)
*!*						ELSE
*!*							.TrackProgress('Set TempCo = ' + tcTempCo,LOGIT)
*!*							.TrackProgress('Set TempCoName = ' + .cTempCoName,LOGIT)
*!*						ENDIF
*!*					CASE tcTempCo == 'U'
*!*						.cTempCo = tcTempCo
*!*						.cTempCoName = 'Staffing Solutions - Carson'
*!*						.lCheckBio = .T.
*!*						.nMinHoursTest = 2
*!*						.nMaxHoursTest = 10
*!*						IF .lTestMode THEN
*!*							.TrackProgress('Set TempCo = ' + tcTempCo,LOGIT+SENDIT)
*!*							.TrackProgress('Set TempCoName = ' + .cTempCoName,LOGIT+SENDIT)
*!*						ELSE
*!*							.TrackProgress('Set TempCo = ' + tcTempCo,LOGIT)
*!*							.TrackProgress('Set TempCoName = ' + .cTempCoName,LOGIT)
*!*						ENDIF
*!*					CASE tcTempCo == 'V'
*!*						.cTempCo = tcTempCo
*!*						.cTempCoName = 'Staffing Solutions - Carson 2'
*!*						.lCheckBio = .T.
*!*						.nMinHoursTest = 2
*!*						.nMaxHoursTest = 10
*!*						IF .lTestMode THEN
*!*							.TrackProgress('Set TempCo = ' + tcTempCo,LOGIT+SENDIT)
*!*							.TrackProgress('Set TempCoName = ' + .cTempCoName,LOGIT+SENDIT)
*!*						ELSE
*!*							.TrackProgress('Set TempCo = ' + tcTempCo,LOGIT)
*!*							.TrackProgress('Set TempCoName = ' + .cTempCoName,LOGIT)
*!*						ENDIF
*!*					OTHERWISE
*!*						.TrackProgress('INVALID TempCo = ' + tcTempCo,LOGIT+SENDIT) 
*!*						.cTempCoName = ''
*!*				ENDCASE

*!*				* determine if we are processing Hourly, not Temps
*!*				IF INLIST(.cTempCo,'HJB','HMG') THEN
*!*					.lHourlyNotTemps = .T.
*!*					.GetHourlyRatesFromADP()
*!*				ENDIF

*!*				* make logfiles company-specific so that report run times can overlap 
*!*				IF .lTestMode THEN
*!*					.cLogFile = 'F:\UTIL\KRONOS\Logfiles\KronosTempTimeReport_' + .cTempCoName + "_" + .cMode + "_" + .cCOMPUTERNAME + '_log_TESTMODE.txt'
*!*				ELSE
*!*					.cLogFile = 'F:\UTIL\KRONOS\Logfiles\KronosTempTimeReport_' + + .cTempCoName + "_" + .cMode + "_" + .cCOMPUTERNAME + '_log.txt'
*!*				ENDIF
*!*				.lLoggingIsOn = .lLoggingIsOn AND NOT EMPTY(.cLogFile)
*!*				IF .lLoggingIsOn THEN
*!*					SET ALTERNATE TO (.cLogFile) ADDITIVE
*!*					SET ALTERNATE ON
*!*				ENDIF
*!*			ENDWITH
*!*		ENDPROC  &&  SetTempCo


	PROCEDURE GetHourlyRatesFromADP

		* get ADP hourly rates (for hourly employees)
		IF USED('CURADP') THEN
			USE IN CURADP
		ENDIF

		.nSQLHandle = SQLCONNECT("PCPAYWIN","MBENNETT","VP737A")

		IF .nSQLHandle > 0 THEN

			SET TEXTMERGE ON
			TEXT TO	lcSQLADP NOSHOW
SELECT
FILE# AS FILE_NUM,
{fn ifnull(MAX(rate1amt),0.00)} AS RATE
FROM REPORTS.V_EMPLOYEE
WHERE COMPANYCODE = 'E87'
GROUP BY FILE#
			ENDTEXT
			SET TEXTMERGE OFF

			.TrackProgress('lcSQLADP =' + lcSQLADP, LOGIT+SENDIT)

			IF .ExecSQL(lcSQLADP, 'CURADP', RETURN_DATA_MANDATORY) THEN

				*SELECT CURADP
				*BROWSE

				=SQLDISCONNECT(.nSQLHandle)

			ENDIF  &&  .ExecSQL(lcSQLADP, 'CURADP', RETURN_DATA_MANDATORY)

		ELSE
			* connection error
			.TrackProgress('Unable to connect to ADP Payroll System.', LOGIT+SENDIT)
			THROW
		ENDIF   &&  .nSQLHandle > 0
	ENDPROC


	FUNCTION GetClientName
		LPARAMETERS tcClient
		LOCAL lcClientName, lnSelect
		lnSelect = SELECT(0)
		lcClientName = "?"
		SELECT CURCLIENTS
		LOCATE FOR ALLTRIM(tcClient) == ALLTRIM(CURCLIENTS.NAME)
		IF FOUND() THEN
			lcClientName = ALLTRIM(CURCLIENTS.DESCRIPTION)
		ENDIF
		SELECT (lnSelect)
		RETURN lcClientName
	ENDFUNC


	FUNCTION GetDivisionName
		LPARAMETERS tcDivision
		LOCAL lcDivisionName, lnSelect
		lnSelect = SELECT(0)
		lcDivisionName = "?"
		SELECT CURDIVISIONS
		LOCATE FOR ALLTRIM(tcDivision) == ALLTRIM(CURDIVISIONS.NAME)
		IF FOUND() THEN
			lcDivisionName = ALLTRIM(CURDIVISIONS.DESCRIPTION)
		ENDIF
		SELECT (lnSelect)
		RETURN lcDivisionName
	ENDFUNC


	FUNCTION GetDepartmentName
		LPARAMETERS tcDepartment
		LOCAL lcDepartmentName, lnSelect
		lnSelect = SELECT(0)
		lcDepartmentName = "?"
		SELECT CURDEPARTMENTS
		LOCATE FOR ALLTRIM(tcDepartment) == ALLTRIM(CURDEPARTMENTS.NAME)
		IF FOUND() THEN
			lcDepartmentName = ALLTRIM(CURDEPARTMENTS.DESCRIPTION)
		ENDIF
		SELECT (lnSelect)
		RETURN lcDepartmentName
	ENDFUNC


	FUNCTION CreatePDFReport
		* create PDF report
		LPARAMETERS tcRootPDFFilename, tcPDFReportFRX

		WITH THIS

			.TrackProgress('in CreatePDFReport tcRootPDFFilename=' + tcRootPDFFilename, LOGIT)
			.TrackProgress('in CreatePDFReport tcPDFReportFRX=' + tcPDFReportFRX, LOGIT)

			LOCAL llPDFReportWasCreated, lcRootPDFFilename, lcFullPDFFilename, lcPDFReportFRX
			lcCentury = SET("CENTURY")

			SET CENTURY OFF  && FOR XX/XX/XX FORMAT DATES ON REPORT
			lcRootPDFFilename = tcRootPDFFilename
			lcPDFReportFRX = tcPDFReportFRX

			lcFullPDFFilename = lcRootPDFFilename + ".PDF"

			llPDFReportWasCreated = .F.

			DO M:\DEV\PRG\RPT2PDF WITH lcPDFReportFRX, lcRootPDFFilename, llPDFReportWasCreated

			IF NOT llPDFReportWasCreated THEN
				.TrackProgress('ERROR - PDF file [' + lcFullPDFFilename + '] was not created',LOGIT+WAITIT+SENDIT)
			ENDIF

			*!*				* keep track of pdf report files for emailing later
			*!*				.AddAttachment(lcFullPDFFilename)

			SET PRINTER TO DEFAULT
			SET CENTURY &lcCentury
		ENDWITH
	ENDFUNC  &&  CreatePDFReport


ENDDEFINE


PROCEDURE errHandlerKTTR
	PARAMETER merror, MESS, mess1, mprog, mlineno, tcTempCo, tcMode
	LOCAL lcSendTo,lcFrom,lcSubject,lcCC,lcAttach,lcBodyText
	CLEAR

	lcSendTo = 'mark.bennett@tollgroup.com'
	lcFrom = 'fmicorporate@fmiint.com'
	lcSubject = 'Early error in KronosTempTimeReport'
	lcCC = ''
	lcAttach = ''
	lcBodyText = 'Error number: ' + LTRIM(STR(merror)) + CRLF + ;
		'Error message: ' + MESS + CRLF + ;
		'Line of code with error: ' + mess1 + CRLF + ;
		'Line number of error: ' + LTRIM(STR(mlineno)) + CRLF + ;
		'Program with error: ' + mprog + CRLF + ;
		'tcTempCo = ' + tcTempCo + CRLF + ;
		'tcMode = ' + tcMode

	DO FORM dartmail2 WITH lcSendTo,lcFrom,lcSubject,lcCC,lcAttach,lcBodyText,"A"

	QUIT

ENDPROC
