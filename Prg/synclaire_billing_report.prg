parameters xacct

xacct = 6221
do m:\dev\prg\lookups

do _setvars

set safety off
public gsystemmodule

gsystemmodule ="wh"

runack("SYNCLAIRE_BILLING")

goffice="2"

wait window at 10,10 "Synclaire Billing Processing..........." nowait

* changed to sql dy 3/30/18
xsqlexec("select * from invoice where invdt>{"+dtoc(date()-30)+"} and inlist(accountid,6221,6521)",,,"ar")
xsqlexec("select * from invdet where xinvdt>{"+dtoc(date()-30)+"} and inlist(xaccountid,6221,6521)",,,"ar")

if xacct = 6521
	useca("miscwo","wh",,,"select * from miscwo where accountid=6521")
	useca("miscdet","wh",,,"select * from miscdet where accountid=6521")
	useca("project","wh",,,"select * from project where accountid=6521")
	useca("projdet","wh",,,"select * from projdet where accountid=6521")
else
	useca("miscwo","wh",,,"select * from miscwo where accountid=6221")
	useca("miscdet","wh",,,"select * from miscdet where accountid=6221")
	useca("project","wh",,,"select * from project where accountid=6221")
	useca("projdet","wh",,,"select * from projdet where accountid=6221")

endif

use f:\wh\sqlpassword in 0
gsqlpassword = alltrim(sqlpassword.password)
use in sqlpassword

cserver = "tgfnjsql01"
sqlsetprop(0,'DispLogin',3)
sqlsetprop(0,"dispwarnings",.f.)

lcdsnless="driver=SQL Server;server=&cServer;uid=SA;pwd=&gsqlpassword;DATABASE=PICKPACK"
nhandle=sqlstringconnect(m.lcdsnless,.t.)
if nhandle<=0 && bailout
	wait window at 6,2 "No Data Connection.."+chr(13)+" Call MIS...."
	lcquery = [Could not make connection to tgfnjsql01 SQL server]+chr(13)+"   "+lcdsnless
	throw
	return
endif

if xacct = 6521

	select invoice.invnum,invdt,invamt,xwo_num,space(20) as detdesc,0000.00 as rate, 0000.00 as rateqty,invoice.wo_num, ;
	invdet.wo_num as wo_numd,linenum,description,substr(description,4,8) as wo,substr(description,6,8) as wo2,0000000 as miscwo,;
	glcode,space(10) as type,invdetamt,invdetid,space(20) as ship_ref,space(25) as ponum,space(20) as ucc, ;
	space(20) as style,space(10) as color,space(10) as pack,space(13) as upc,;
	0000 as totqty, space(10)as billing, 0000.00 as pkgcharge, 0000000 as proj_wonum, space(60) as unitdesc, ;
	space(20) as bol_no, space(31) as ship_via, space(25) as cnee_ref, ;
	space(4) as carrier, space(3) as service, space(3) as billing, 6521 as acctnum  ;
	from invoice left outer join invdet on invoice.invoiceid = invdet.invoiceid ;
	where invdt >= date()-30 and accountid = 6521 into cursor temp readwrite
&&  Where invdt >= {05/01/2017} and invdt<={06/01/2017} And accountid = 6521 Into Cursor temp Readwrite

endif

if xacct = 6221

	select invoice.invnum,invdt,invamt,xwo_num,space(20) as detdesc,0000.00 as rate, 0000.00 as rateqty,invoice.wo_num, ;
	invdet.wo_num as wo_numd,linenum,description,substr(description,4,8) as wo,substr(description,6,8) as wo2,0000000 as miscwo,;
	glcode,space(10) as type,invdetamt,invdetid,space(20) as ship_ref,space(25) as ponum,space(20) as ucc, ;
	space(20) as style,space(10) as color,space(10) as pack,space(13) as upc,;
	0000 as totqty, space(10)as billing, 0000.00 as pkgcharge, 0000000 as proj_wonum, space(60) as unitdesc, ;
	space(20) as bol_no, space(31) as ship_via, space(25) as cnee_ref, ;
	space(4) as carrier, space(3) as service, space(3) as billing, 6221 as acctnum ;
	from invoice left outer join invdet on invoice.invoiceid = invdet.invoiceid ;
	where invdt >= date()-30 and accountid = 6221 into cursor temp readwrite
**  Where invdt >= {05/01/2017} And invdt<={08/01/2017} And accountid = 6221 Into Cursor temp Readwrite

endif


select temp
scan for xwo_num< 200
	do case
	case xwo_num = 2
		replace detdesc with "HANDLING"
	case xwo_num = 8
		replace detdesc with "SPECIAL PROJ"
	case xwo_num = 11
		replace detdesc with "OUTBOUND SHIPPING"
	case xwo_num = 18
		replace detdesc with "MISC WOS"
	case xwo_num = 21
		replace detdesc with "OUTBOUND P&P"
	case xwo_num = 28
		replace detdesc with "DEDICATED SPACE"
	case xwo_num = 31
		replace detdesc with "PIER DRAYAGE"
	case xwo_num = 91
		replace detdesc with "CANCELLED BTS"
	case xwo_num = 96
		replace detdesc with "RETURNED UNITS"

	endcase

endscan

select temp
goto top
scan
	select miscwo
	locate for wo_num=  temp.wo_numd
*  =Seek(wo_numD,"miscwo","wo_num")
	if found("miscwo")
		replace temp.miscwo_wonum with miscowo.wo_num in miscwo
		select miscdet
		locate for wo_num = temp.xwo_num and alltrim(miscdet.description)$alltrim(temp.description)
		if found()
			replace temp.rateqty  with miscdet.qty  in temp
			replace temp.rate     with miscdet.rate in temp
			replace temp.unitdesc with miscdet.description in temp
		endif


	endif
endscan

select temp
scan
	select project
	locate for wo_num = temp.wo_numd
*  =Seek(wo_numD,"project","wo_num")
	if found("project")
		replace temp.proj_wonum with project.wo_num in temp
		select projdet
		locate for alltrim(transform(temp.xwo_num))$ temp.description and alltrim(projdet.description)$ alltrim(temp.description)
		if found()
			replace temp.rateqty  with projdet.qty   in temp
			replace temp.rate     with projdet.rate  in temp
			replace temp.unitdesc with projdet.description in temp
		endif


	endif
endscan

select temp
goto top
scan
	select project
	locate for wo_num = temp.xwo_num
*  =Seek(xwo_num,"miscwo","wo_num")
	if found("miscwo")
		replace temp.misc_wonum with miscwo.wo_num in temp
		select miscdet
		locate for wo_num = temp.xwo_num and alltrim(miscdet.description)$ alltrim(temp.description)
		if found()
			replace temp.rateqty  with miscdet.qty  in temp
			replace temp.rate     with miscdet.rate in temp
			replace temp.unitdesc with miscdet.description in temp
		endif
	endif
endscan

select temp
scan
	select project
	locate for wo_num = temp.xwo_num
*  =Seek(xwo_num,"project","wo_num")
	if found("project")
		replace temp.proj_wonum with project.wo_num in temp
		select projdet
		locate for alltrim(transform(temp.xwo_num))$ temp.description and alltrim(projdet.description)$ alltrim(temp.description)
		if found()
			replace temp.rateqty  with projdet.qty   in temp
			replace temp.rate     with projdet.rate  in temp
			replace temp.unitdesc with projdet.description in temp
		endif


	endif
endscan

select temp
scan
	do case
	case "CARTONS @"$description
		replace type with "CARTONS"
	case "PICKS @"$description
		replace type with "PICKS"
	case "PICK  @"$description
		replace type with "PICKS"
	endcase
endscan


select temp
select * from temp into cursor temp2 readwrite where .f.

select temp
select * from temp into cursor temp3 readwrite where .f.

lldopicks = .t.

if lldopicks
	select temp
	scan for temp.type = "PICKS"
		if usesql()
			xsqlexec("select ship_ref, ucc, upc, style, color, pack, qty, totqty from cartons " + ;
				"where wo_num="+transform(temp.wo_numd),"tempdata",,"pickpack")
		else
			lcquery  = [select ship_ref,ucc,upc,style,color,pack,qty,totqty from cartons where wo_num=]+alltrim(transform(temp.wo_numd))
			if sqlexec(nhandle,lcquery,"tempdata")#1
				wait window at 6,2 "Error Cartons SQL query.........." timeout 1
				sqldisconnect(nhandle)
			endif
		endif

		select tempdata
		scan
			insert into temp2 (wo_num,invnum,invdt,invdetid,type,ship_ref,ucc,upc,style,color,pack,totqty) values;
			(temp.wo_num,temp.invnum,temp.invdt,temp.invdetid,"UNITPICKS",tempdata.ship_ref,tempdata.ucc,tempdata.upc,tempdata.style,tempdata.color,tempdata.pack,tempdata.totqty)

			if xacct = 6521
				if xsqlexec("select cnee_ref from outship where mod='"+goffice+"' and accountid=6521 and ship_ref='"+temp2.ship_ref+"'",,,"wh") # 0
					replace ponum with outship.cnee_ref in temp
				endif
			endif

			if xacct = 6221
				if xsqlexec("select cnee_ref from outship where mod='"+goffice+"' and accountid=6221 and ship_ref='"+temp2.ship_ref+"'",,,"wh") # 0
					replace ponum with outship.cnee_ref in temp
				endif
			endif

		endscan
	endscan
endif


lldocartons = .t.

if lldocartons
	select temp
	scan for temp.type = "CARTONS"
		if usesql()
			xsqlexec("select ship_ref, ucc from cartons where wo_num="+transform(temp.wo_num)+" group by ship_ref, ucc","tempdata",,"pickpack")
		else
			lcquery  = [select ship_ref,ucc from cartons where wo_num=]+alltrim(transform(temp.wo_num))+[ group by ship_ref,ucc]
			if sqlexec(nhandle,lcquery,"tempdata")#1
				wait window at 6,2 "Error Cartons SQL query.........." timeout 1
				sqldisconnect(nhandle)
			endif
		endif

		select tempdata
		scan
			insert into temp2 (wo_num,invnum,invdt,invdetid,type,ship_ref,ucc) values;
			(temp.wo_num,temp.invnum,temp.invdt,temp.invdetid,"CARTONS",tempdata.ship_ref,tempdata.ucc)

			if xacct = 6521
				if xsqlexec("select cnee_ref from outship where mod='"+goffice+"' and accountid=6521 and ship_ref='"+temp2.ship_ref+"'",,,"wh") # 0
					replace ponum with outship.cnee_ref in temp
				endif
			endif

			if xacct = 6221
				if xsqlexec("select cnee_ref from outship where mod='"+goffice+"' and accountid=6221 and ship_ref='"+temp2.ship_ref+"'",,,"wh") # 0
					replace ponum with outship.cnee_ref in temp
				endif
			endif

		endscan
	endscan
endif



select temp2
scan

	if xacct = 6521
		if xsqlexec("select bol_no, ship_via, ponum from outship where mod='"+goffice+"' and accountid=6521 and ship_ref='"+temp2.ship_ref+"'",,,"wh") # 0
			replace bol_no with outship.bol_no, ship_via with outship.ship_via, ponum with outship.cnee_ref in temp2
		endif
	endif

	if xacct = 6221
		if xsqlexec("select bol_no, ship_via, ponum from outship where mod='"+goffice+"' and accountid=6221 and ship_ref='"+temp2.ship_ref+"'",,,"wh") # 0
			replace bol_no with outship.bol_no, ship_via with outship.ship_via, ponum with outship.cnee_ref in temp2
		endif
	endif


**  =Seek(temp2.wo_num,"shipment","wo_num")
**  If Found("shipment")
	xsqlexec("select * from shipment where accountid="+transform(outship.accountid)+" and wo_num = "+transform(temp2.wo_num),,,"wh")
	if !eof("shipment")
		replace carrier with shipment.carrier in temp2
		replace service with shipment.service in temp2
		replace billing with shipment.billing in temp2

**    =Seek(temp2.ucc,"package","ucc")
**    If Found("package")
		xsqlexec("select * from package where accountid="+transform(outship.accountid)+" and ucc = "+transform(temp2.ucc),,,"wh")
		if !eof("package")
			replace pkgcharge with package.pkgcharge in temp2
		endif

		use in package
	endif

	use in shipment
endscan

select temp2



set step on

copy to h:\fox\temp2

select temp
append from h:\fox\temp2

select * from temp order by invdt,invdetid into cursor fdata
select fdata

if xacct = 6521
	copy to f:\ftpusers\synclaire\invoicedata\synclaire_billing.csv csv
else
	copy to f:\ftpusers\synclaire\invoicedata\bcny_billing.csv csv
endif

wait window at 10,10 "Synclaire Billing Process Complete.............." timeout 2

tsubject = "Synclaire Billing file created at: "+ttoc(datetime())
tsendto  = "pgaidis@fmiint.com,tmarg@fmiint.com"
tcc = ""
tattach=""
tfrom = "Toll/FMI EDI Operations <toll-edi-ops@tollgroup.com>"
tmessage = tsubject
do form m:\dev\frm\dartmail2 with tsendto,tfrom,tsubject,tcc,tattach,tmessage,"A"

schedupdate()

return
*******************************************8888
