**guessrpt
**04/15/08 mvw
**
**create a cursor from invoice/invDet with a SINGLE RECORD FOR each w/o... then create a cursor from
**that cursor join all the rest - mfst, wolog, aircans, etc.
dstart=ctod(transform(month(gomonth(date(),-1)))+"/01/"+transform(year(gomonth(date(),-1))))
dend=ctod(transform(month(date()))+"/01/"+transform(year(date())))-1

naccountid=5249

xsqlexec("select * from account where inactive=0","account",,"qq")
index on accountid tag accountid
set order to

use f:\watusi\ardata\invoice in 0
use f:\watusi\ardata\invdet in 0
use f:\wo\wodata\manifest in 0

xacctname=acctname(naccountid)

select i.accountid, i.invdt, i.invnum, i.wo_num, i.invamt, i.acctname, i.billname, i.billname2, i.address, ;
	i.city, i.state, i.zip, i.billparty, ;
	sum(iif(invdet.chgtype = "PICKUP",  invdetamt, 0.00)) as "invPuChg", ;
	sum(iif(invdet.chgtype = "DRAYAGE", invdetamt, 0.00)) as "invDrayage", ;
	sum(iif(invdet.chgtype = "TRANSLOAD", invdetamt, 0.00)) as "invTrans", ;
	sum(iif(invdet.chgtype = "CFS", invdetamt, 0.00)) as "invCfs", ;
	sum(iif(invdet.chgtype = "LINEHAUL", invdetamt, 0.00)) as "invLH", ;
	sum(iif(invdet.chgtype = "OTHER" and "PASSTHRU"$description, invdetamt, 0.00)) as "invPass", ;
	sum(iif(invdet.chgtype = "FUEL", invdetamt, 0.00)) as "invFuel", ;
	sum(iif(invdet.chgtype = "DOE", invdetamt, 0.00)) as "invDoe" ;
	from invoice i left join invdet on invdet.invoiceid = i.invoiceid ;
	group by invdet.invnum ;
	where i.accountid=naccountid and between(i.invdt,dstart,dend) and (invdet.main or (!invdet.main and !invdet.split)) and !empty(i.wo_num) ;
	into cursor xrpt

select x.*, m.orig_wo, m.hawb, m.po_num, m.pucharge, m.drayage, m.transload, m.other, m.linehaul, m.passthru, m.fuelchg, m.doechg ;
	from xrpt x left join manifest m on m.wo_num=x.wo_num into cursor xrpt

public oexcel
oexcel = createobject("Excel.Application")

cexcelfile = "h:\fox\"+sys(3)+".XLS"
=StrtoFile(FileToStr("guessrpt.xls"), cexcelfile)

oworkbook = oexcel.workbooks.open(cexcelfile)
oworksheet1 = oworkbook.worksheets[1]

**could use "activeSheet", but easier to refer to the sheets specifically**
oExcel.ActiveSheet.Range("A2").Value = 'Monthly Invoicing: '+dtoc(dstart)+" - "+dtoc(dend)
oExcel.ActiveSheet.Range("A3").Value = "Account: "+ALLTRIM(xacctName)
wait "Building Spreadheet..." window nowait noclear

nrownum = 6
do while !eof()
	chdrrow = alltrim(str(nrownum, 5, 0))
	wait window "Writing row "+chdrrow nowait

	with oworksheet1
		.range("A"+chdrrow+":"+"O"+chdrrow).interior.color = rgb(193,238,253)

		.range("A"+chdrrow).value = invdt
		.range("B"+chdrrow).value = invnum
		.range("C"+chdrrow).value = wo_num
		.range("G"+chdrrow).value = invpuchg
		.range("H"+chdrrow).value = invdrayage
		.range("I"+chdrrow).value = invcfs
		.range("J"+chdrrow).value = invtrans
		.range("K"+chdrrow).value = invlh
		.range("L"+chdrrow).value = invpass
		.range("M"+chdrrow).value = invfuel+invdoe
		.range("N"+chdrrow).value = round(invamt-(invpuchg+invdrayage+invcfs+invtrans+invlh+invpass+invfuel+invdoe),2)
		.range("O"+chdrrow).value = invamt

		nrownum=nrownum+1

		ntotchg=0
		nwonum=wo_num
		nstartrow=nrownum
		scan while wo_num=nwonum
			crow = alltrim(str(nrownum, 5, 0))

			.range("D"+crow).value = orig_wo
			.range("E"+crow).value = hawb
			.range("F"+crow).value = po_num
			.range("G"+crow).value = pucharge
			.range("H"+crow).value = drayage
			.range("I"+crow).value = other
			.range("J"+crow).value = transload
			.range("K"+crow).value = linehaul
			.range("L"+crow).value = passthru
			.range("M"+crow).value = fuelchg+doechg
			.range("O"+crow).value = pucharge+drayage+other+transload+linehaul+passthru+fuelchg+doechg

			ntotchg=ntotchg+pucharge+drayage+other+transload+linehaul+passthru+fuelchg+doechg
			nrownum=nrownum+1
		endscan

		if ntotchg#round(.range("O"+chdrrow).value,2)
			crow = alltrim(str(nrownum, 5, 0))
			.range("G"+crow).value = "=sum(G"+transform(nstartrow)+":G"+transform(nrownum-1)+")"
			.range("H"+crow).value = "=sum(H"+transform(nstartrow)+":H"+transform(nrownum-1)+")"
			.range("I"+crow).value = "=sum(I"+transform(nstartrow)+":I"+transform(nrownum-1)+")"
			.range("J"+crow).value = "=sum(J"+transform(nstartrow)+":J"+transform(nrownum-1)+")"
			.range("K"+crow).value = "=sum(K"+transform(nstartrow)+":K"+transform(nrownum-1)+")"
			.range("L"+crow).value = "=sum(L"+transform(nstartrow)+":L"+transform(nrownum-1)+")"
			.range("M"+crow).value = "=sum(M"+transform(nstartrow)+":M"+transform(nrownum-1)+")"
			.range("O"+crow).value = ntotchg
			nrownum=nrownum+1

			.range("O"+chdrrow).interior.color = rgb(255,0,0)
		endif

		nrownum=nrownum+1
	endwith
enddo

wait clear

*!*	crow = alltrim(str(nrownum+2, 5, 0))
*!*	oworksheet1.range("A"+crow+":"+"J"+crow).interior.color = rgb(227,215,172)
*!*	oworksheet1.range("C"+crow).font.bold = .t.
*!*	select sum(invamt),sum(invpuchg),sum(invdrayage),sum(invcfs),sum(invtrans),sum(invlh),sum(invfuel),sum(invdoe) ;
*!*		from xrpt into array atot

*!*	if type("atot")#"U"
*!*		with oworksheet1
*!*			.range("C"+crow).value = "Totals:"
*!*			.range("G"+crow).value = atot(2)
*!*			.range("H"+crow).value = atot(3)
*!*			.range("I"+crow).value = atot(4)
*!*			.range("J"+crow).value = atot(5)
*!*			.range("K"+crow).value = atot(6)
*!*			.range("L"+crow).value = atot(7)+atot(8)
*!*			.range("M"+crow).value = round(atot(1)-(atot(2)+atot(3)+atot(4)+atot(5)+atot(6)+atot(7)+atot(8)),2)

*!*			store "=SUM(G"+crow+":M"+crow+")" to ctotstr
*!*			.range("M"+crow).value = ctotstr
*!*		endwith
*!*		release atot
*!*	endif


oworkbook.save()
oExcel.quit &&release the file before attempting to send
release oworksheet1, oworkbook, oexcel

cto="mwinter@fmiint.com"
ccc="jrocio@fmiint.com"

cAttach=cexcelfile
cFrom="TGF WMS Operations <fmicorporate@fmiint.com>"
cSubject='Monthly Invoicing: '+dtoc(dstart)+" - "+dtoc(dend)
cMessage='Worksheet Attached'
do form dartmail2 with cto,cfrom,csubject,ccc,cattach,cmessage,"A"

use in account
use in invoice
use in invdet
use in manifest