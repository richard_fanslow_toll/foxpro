*!* Nautica 944 CREATOR

PARAMETERS nWO_Num,nAcctnum

PUBLIC lXfer944,cWhse,cFilename,cFilenameOut,tsendto,tcc,tsendtoerr,tccerr,tsendtotest,tcctest,lIsError,lsendmail,tsendtorcverr,tccrcverr
PUBLIC leMail,tfrom,lDoCatch,goffice

lTesting = .F. && If TRUE, disables certain functions
lUsePL = .F.
lIsError = .F.
lOverrideXcheck = .T. && If true, doesn't check ASN carton count against barcode scans.
lDoFilesOut = !lTesting && If true, keeps output file in 944HOLD folder, otherwise into 944OUT
lDoCatch  = .T.
leMail = .T.  && If .t., sends mail to WAREHOUSES
lCtnUCC = .F.

cCustName ="Nautica\Kipling"

IF lTesting
	CLOSE DATABASES ALL
ENDIF

ON ERROR THROW


cOffice = "M"
cMod = "M"
gMasterOffice = cOffice
gOffice = cOffice

cErrMsg = ""
cIntUsage = IIF(lTesting,"T","P")

DO m:\dev\prg\_setvars WITH lTesting

tfrom = "TGF Warehouse Operations <warehouse-ops@fmiint.com>"
*Set STEP ON
TRY

	IF VARTYPE(nWO_Num) = "L" OR lTesting
		nWO_Num =  8045690
		nAcctnum = 687
		lUsePL = .F.
	ENDIF
	cWO_Num = TRANS(nWO_Num)

	cContainer = ""

	xReturn = "XXX"
	DO m:\dev\prg\wf_alt WITH cOffice,nAcctnum
	IF xReturn = "XXX"
		WAIT WINDOW "No Bldg. # Provided...closing" TIMEOUT 3
		DO ediupdate WITH .T.,"NO BLDG #"
		RETURN
	ENDIF

	cUseFolder = UPPER(xReturn)

	SELECT 0
	USE F:\3pl\DATA\mailmaster ALIAS mm SHARED
	LOCATE FOR mm.accountid = 6356 ;
		AND mm.office = cOffice ;
		AND mm.edi_type = "944"
	IF !FOUND()
		ASSERT .F. MESSAGE "At missing Office/Acct"
		WAIT WINDOW "Office/Loc/Acct not found!" TIMEOUT 2
		DO ediupdate WITH .T.,"ACCT/LOC !FOUND"
		NormalExit = .F.
		THROW
	ENDIF
	STORE TRIM(mm.acctname) TO cCustName
	STORE TRIM(mm.basepath) TO lcOutPath
	STORE TRIM(mm.holdpath) TO lcHoldPath
	STORE TRIM(mm.archpath) TO lcArchivePath
	tsendto = IIF(mm.use_alt,mm.sendtoalt,mm.sendto)
	tcc = IIF(mm.use_alt,mm.ccalt,mm.cc)

	LOCATE
	LOCATE FOR mm.edi_type  = "MISC" AND mm.taskname = "GENERAL"
	tsendtoerr = IIF(mm.use_alt,mm.sendtoalt,mm.sendto)
	tccerr = IIF(mm.use_alt,mm.ccalt,mm.cc)
	tsendtotest = IIF(mm.use_alt,mm.sendtoalt,mm.sendto)
	tcctest = IIF(mm.use_alt,mm.ccalt,mm.cc)
	LOCATE
	LOCATE FOR mm.accountid = 6356 AND mm.edi_type  = "MISC" AND mm.taskname = "RCVERROR"
	tsendtorcverr = IIF(mm.use_alt,mm.sendtoalt,mm.sendto)
	tccrcverr = IIF(mm.use_alt,mm.ccalt,mm.cc)
	USE IN mm

	IF lTesting
		STORE tsendtotest TO tsendto
		STORE tcctest TO tcc
	ENDIF

	DIMENSION thisarray(1)
	cSuffix = ""
	cWhse = "MIAMI"

	IF TYPE("nWO_Num") <> "N"  OR EMPTY(nWO_Num) && If WO is not numeric or equals Zero
		WAIT WINDOW "No WO# Provided...closing" TIMEOUT 3
		DO ediupdate WITH .T.,"NO WO_NUM"
		RETURN
	ENDIF

	cBarMiss = ""
	cASN = ""
	cUPC = ""
	nSTSets = 0
	cSpecChar = .F.
	XFERUPDATE = .T.
	nFilenum = 0
	lBackup = .F.
	lOverage = .F.
	lsendmail = .T.
	cErrMsg = ""
	lIsError = .T.
	lXfer944 = .F.
	cTrk_WO = ""
	nCTNTotal = 0

	CREATE CURSOR temp1 (wo_num N(6), ;
		po_num c(5), ;
		STYLE c(20), ;
		expected i, ;
		actual i)

*!* SET CUSTOMER CONSTANTS
	cMailName = "NAUTICA"  && Proper name for eMail, etc.
	cZLAddress = "101 SPRING STREET"
	cZLCity = "NEW YORK"
	cZLState = "NY"
	cZLZip = "10002"

	cCustPrefix = "MS" && Customer Prefix (for output file names)
	cX12 = "005010"  && X12 Standards Set used
	crecid = "2122441111"  && Recipient EDI address

	dt1 = TTOC(DATETIME(),1)
	dt2 = DATETIME()
	dtmail = TTOC(DATETIME())

	cFilestem = cCustPrefix+dt1+".944"
	cFilename = (lcHoldPath+cFilestem) && Holds file here until complete and ready to send
	cFilenameOut = (lcOutPath+cFilestem)
	cFilenameArchive = (lcArchivePath+cFilestem)

	STORE "" TO c_CntrlNum,c_GrpCntrlNum
	nFilenum = FCREATE(cFilename)

*!* SET OTHER CONSTANTS
	nCtnNumber = 1
	cString = ""
	csendqual = "ZZ"
	csendid = "TGF"
	crecqual = "12"
	cfd = "*"  && Field/element delimiter
	csegd = "~" && Line/segment delimiter
	nSegCtr = 0
	cterminator = ">" && Used at end of ISA segment
	cdate = DTOS(DATE())
	ctruncdate = RIGHT(cdate,6)
	ctime = LEFT(TIME(DATE()),2)+SUBSTR(TIME(DATE()),4,2)
	cfiledate = cdate+ctime
	cOrig = "J"
	cdate = DTOS(DATE())
	ctruncdate = RIGHT(cdate,6)
	ctime = LEFT(TIME(DATE()),2)+SUBSTR(TIME(DATE()),4,2)
	cQtyRec = "1"
	cCTypeUsed = ""
	nQtyExpected = 0
	nCTNDamaged = 0
	nCTNShortage = 0
	nCTNExpected = 0  && From INWOLOG count
	nCTNActual = 0
	cMissingCtn = "Missing Cartons: "+CHR(13)
	cPO = ""
	cStyle = ""
	nPOExpected = 0
	nPOActual = 0

** PAD ID Codes
	csendidlong = PADR(csendid,15," ")
	crecidlong = PADR(crecid,15," ")

*!* Serial number incrementing table
	IF USED("serfile")
		USE IN serfile
	ENDIF

	USE ("F:\3pl\data\serial\nautica_944_serial") IN 0 ALIAS serfile

	IF !USED('EDI_TRIGGER')
		USE F:\3PL\DATA\EDI_TRIGGER IN 0
	ENDIF

*!* Search table (parent)
	IF !USED('inwolog') && and lTesting
		SET STEP ON 
		csqi = [select * from inwolog where accountid = ]+TRANSFORM(nAcctnum)+[ and wo_num = ]+TRANSFORM(nWO_Num)
		xsqlexec(csqi,,,"wh")
		RELEASE csqi
	ENDIF

*!* Search table (xref for PO's)
	IF !USED('INDET') &&and lTesting
		csqi = [select * from indet where accountid = ]+TRANSFORM(nAcctnum)+[ and wo_num = ]+TRANSFORM(nWO_Num)
		xsqlexec(csqi,,,"wh")
		RELEASE csqi
	ENDIF

	nRCnt = 0
	nISACount = 0
	nSTCount = 1
	nSECount = 1

	SELECT inwolog

	cAcctname  = TRIM(inwolog.acctname)
	cContainer = TRIM(inwolog.CONTAINER)
	cAcct_ref  = TRIM(inwolog.acct_ref)
	cBOL       = TRIM(inwolog.acct_ref)

	DO num_incr
	STORE "ISA"+cfd+"00"+cfd+"          "+cfd+"00"+cfd+"          "+cfd+csendqual+cfd+csendidlong+cfd+;
		crecqual+cfd+crecidlong+cfd+ctruncdate+cfd+ctime+cfd+"U"+cfd+"00403"+cfd+;
		PADL(c_CntrlNum,9,"0")+cfd+"0"+cfd+cIntUsage+cfd+cterminator+csegd TO cString
	DO cstringbreak

	STORE "GS"+cfd+"RE"+cfd+csendid+cfd+crecid+cfd+cdate+cfd+ctime+cfd+c_CntrlNum+;
		cfd+"X"+cfd+cX12+csegd TO cString
	DO cstringbreak

	nSegCtr = 0
	nISACount = 1

	IF nSECount = 1
		STORE "ST"+cfd+"944"+cfd+PADL(c_GrpCntrlNum,9,"0")+csegd TO cString
		DO cstringbreak
		nSegCtr = nSegCtr + 1

		IF lTesting AND EMPTY(cAcct_ref)
			cAcct_ref = "9999999"
		ENDIF
		STORE "W17"+cfd+cOrig+cfd+cdate+cfd+cWO_Num+cfd+IIF(EMPTY(cAcct_ref),cWO_Num,cAcct_ref)+csegd TO cString
		DO cstringbreak
		nSegCtr = nSegCtr + 1

		STORE "N1"+cfd+"RC"+cfd+"TOLL LOGISTICS"+cfd+"ZZ"+cfd+"FL"+csegd TO cString
		DO cstringbreak
		nSegCtr = nSegCtr + 1

		STORE "PER"+cfd+"CN"+cfd+"PAUL GAIDIS"+cfd+"TE"+cfd+"732-750-9000"+cfd+"EM"+cfd+"PGAIDIS@FMIINT.COM"+csegd TO cString
		DO cstringbreak
		nSegCtr = nSegCtr + 1

		STORE "N9"+cfd+"ER"+cfd+IIF(EMPTY(cContainer),"NA",cContainer)+csegd TO cString  && Used for either AWB or container type
		DO cstringbreak
		nSegCtr = nSegCtr + 1

		STORE "N9"+cfd+"RF"+cfd+cBOL+csegd TO cString
		DO cstringbreak
		nSegCtr = nSegCtr + 1

		STORE "G62"+cfd+"AB"+cfd+DTOS(DATE())+csegd TO cString
		DO cstringbreak
		nSegCtr = nSegCtr + 1
		nSECount = 0
	ENDIF
	nCTNExpected=0

	SELECT indet
	SELECT STYLE,COLOR,ID,PACK,po,totqty FROM indet WHERE indet.inwologid = inwolog.inwologid INTO CURSOR indet2

	SELECT indet2

	IF RECCOUNT("indet2") = 0
		SET STEP ON
		cErrMsg ="No matching INDET records for a Nautica\Kipling 944, WO : "+ ALLTRIM(TRANSFORM(nWO_Num))
		DO ediupdate WITH .T.,"No INDET Records"
		NormalExit = .F.
		THROW
	ENDIF

	LineCtr = 1

	SCAN &&FOR indet.inwologid = inwolog.inwologid AND units
		lnNumCtns = 0
		cStyle = ALLTRIM(indet2.STYLE)
		cColor = ALLTRIM(indet2.COLOR)
		cSize  = ALLTRIM(indet2.ID)
		cPack  = ALLTRIM(indet2.PACK)
		cPO    = ALLTRIM(indet2.po)
		cCtns  = ALLTRIM(STR(indet2.totqty))
		nCTNExpected = nCTNExpected + indet2.totqty

		SET STEP ON
		xsqlexec("select ucc, upc, ponum from ctnucc where mod='M' and style='"+cstyle+"' and color='"+ccolor+"' and id='"+csize+"' and pack='"+cpack+"' " + ;
			"and inwonum="+transform(nwo_num)+"and rcvdt#{}","uccs",,"wh")
		
		SELECT uccs
		LOCATE
		IF EOF()
			=FCLOSE(nFilenum)
			cErrMsg ="No CtnUCC Records for Nautica\Kipling 944 work order: "+ ALLTRIM(TRANSFORM(nWO_Num))
			cErrMsg = cErrMsg+CHR(13)+"Style/Color/Size/Pack "+cStyle+"/"+cColor+"/"+cSize+"/"+cPack
			lCtnUCC = .T.
			SET STEP ON
			DO ediupdate WITH .T.,"No CTNUCC Records"
			NormalExit = .F.
			THROW
		ENDIF
		SCAN
			STORE "LX"+cfd+ALLTRIM(TRANSFORM(LineCtr))+csegd TO cString
			DO cstringbreak
			nSegCtr = nSegCtr + 1

			STORE "MAN"+cfd+"GM"+cfd+uccs.ucc+csegd TO cString
			DO cstringbreak
			nSegCtr = nSegCtr + 1
			lnNumCtns = lnNumCtns +1

			STORE "W07"+cfd+cPack+cfd+"EA"+cfd+ALLTRIM(uccs.upc)+cfd+"VN"+cfd+cStyle+csegd TO cString
			DO cstringbreak
			nSegCtr = nSegCtr + 1

			STORE "N9*VA"+cfd+ALLTRIM(cStyle)+csegd TO cString
			DO cstringbreak
			nSegCtr = nSegCtr + 1

			STORE "N9*CL"+cfd+ALLTRIM(cColor)+csegd TO cString
			DO cstringbreak
			nSegCtr = nSegCtr + 1

			STORE "N9*SZ"+cfd+ALLTRIM(cSize)+csegd TO cString
			DO cstringbreak
			nSegCtr = nSegCtr + 1

			STORE "N9*PO"+cfd+ALLTRIM(uccs.ponum)+csegd TO cString
			DO cstringbreak
			nSegCtr = nSegCtr + 1
			LineCtr = LineCtr +1
		ENDSCAN

		IF lnNumCtns > 0
			STORE "W20"+cfd+ALLTRIM(TRANSFORM(lnNumCtns))+csegd TO cString
			DO cstringbreak
			nSegCtr = nSegCtr + 1
		ENDIF

	ENDSCAN

	WAIT WINDOW "END OF SCAN/CHECK PHASE ROUND..." NOWAIT

	nSTCount = 0
	DO close944
	lXfer944 = .T.
	=FCLOSE(nFilenum)

	IF !lTesting
		IF !USED("ftpedilog")
			SELECT 0
			USE F:\edirouting\ftpedilog ALIAS ftpedilog
			INSERT INTO ftpedilog (ftpdate,filename,acct_name,TYPE) VALUES (dt2,cFilename,"cCustname","944")
			USE IN ftpedilog
		ENDIF
		SELECT EDI_TRIGGER
	ENDIF

	WAIT "944 Creation process complete:"+CHR(13)+cFilename WINDOW AT 45,60 TIMEOUT 3
	cFin = "944 FILE "+cFilenameOut+" CREATED WITHOUT ERRORS"

	COPY FILE &cFilename TO &cFilenameArchive
	IF lDoFilesOut AND !lTesting
		COPY FILE &cFilename TO &cFilenameOut
		ERASE &cFilename
	ENDIF

	IF !lTesting
		SELECT EDI_TRIGGER
		LOCATE
		REPLACE EDI_TRIGGER.processed WITH .T.,EDI_TRIGGER.created WITH .T.,EDI_TRIGGER.when_proc WITH DATETIME(),;
			EDI_TRIGGER.fin_status WITH "944 CREATED",EDI_TRIGGER.errorflag WITH .F.,file944crt WITH cFilename ;
			FOR EDI_TRIGGER.wo_num = nWO_Num AND EDI_TRIGGER.accountid = nAcctnum AND EDI_TRIGGER.edi_type = "944"
		LOCATE
		lIsError = .F.
		IF leMail
			sendmail()
		ENDIF
	ENDIF
	RELEASE ALL LIKE T*
	lDoCatch = .F.

CATCH TO oErr
	IF lDoCatch AND EMPTY(ALLTRIM(cErrMsg))
		WAIT WINDOW "At error catch block" TIMEOUT 1
		SET STEP ON
		tsubject = "944 Error in "+cCustName+" WO "+TRIM(cWO_Num)
		tattach = " "
		tsendto = tsendtoerr
		tcc = tccerr

		tmessage = cCustName+" Error processing "+CHR(13)
		tmessage =tmessage+ "Try/CATCH Exeception Message:"+CHR(13)+;
			[  Error: ] + STR(oErr.ERRORNO) +CHR(13)+;
			[  Procedure: ] + oErr.PROCEDURE +CHR(13)+;
			[  LineNo: ] + STR(oErr.LINENO) +CHR(13)+;
			[  Message: ] + oErr.MESSAGE +CHR(13)+;
			[  Details: ] + oErr.DETAILS +CHR(13)+;
			[  StackLevel: ] + STR(oErr.STACKLEVEL) +CHR(13)+;
			[  LineContents: ] + oErr.LINECONTENTS+CHR(13)+;
			[  UserValue: ] + oErr.USERVALUE
	ENDIF
FINALLY
	ON ERROR
	closedata()
	IF !lTesting
		SELECT EDI_TRIGGER
		LOCATE
	ENDIF
ENDTRY

************************************************************************
*!* END OF MAIN PROGRAM
************************************************************************


****************************
PROCEDURE stclose
****************************
	nCTNShortage = (nCTNExpected - nCTNActual - nCTNDamaged)
	nCTNTotal = nCTNTotal + nCTNExpected
	IF nCTNShortage < 0
		WAIT CLEAR
		lOverage = .T.
		tsendto = tsendtoerr
		tcc = tccerr
		tsubject = cMailName+" 944 EDI File OVERAGE, WO "+cWO_Num
		tattach = " "
		tmessage = "PO "+cPONumOld+" OVERAGE (More ctns than expected for a PO)! Check EDI File immediately!"
		tmessage = tmessage + "Expected: "+TRANSFORM(nCTNExpected)+", Actual: "+TRANSFORM(nCTNActual)
		tfrom = "TGF Warehouse Operations <warehouse-ops@fmiint.com>"
		DO FORM m:\dev\frm\dartmail2 WITH tsendto,tfrom,tsubject,tcc,tattach,tmessage,"A"
	ENDIF

*!* For 944 process only!
	IF nSECount = 0
		STORE "W14"+cfd+ALLTRIM(STR(nCTNExpected))+csegd TO cString
		DO cstringbreak
		nSegCtr = nSegCtr + 1
*!* End 944 addition

		STORE  "SE"+cfd+ALLTRIM(STR(nSegCtr+1))+cfd+PADL(c_GrpCntrlNum,9,"0")+csegd TO cString
		FPUTS(nFilenum,cString)
		nSTCount = 1
		nSECount = 1
	ENDIF
	nSegCtr = 0
	nSTSets = nSTSets + 1

	SELECT serfile
	REPLACE serfile.grpseqnum WITH serfile.grpseqnum + 1
	c_GrpCntrlNum = ALLTRIM(STR(serfile.grpseqnum))
*? c_GrpCntrlNum
	SELECT indet1
	LXCount = 1
	STORE 0 TO nCTNExpected,nCTNActual,nCTNDamaged,nCTNShortage
	RETURN

****************************
PROCEDURE close944
****************************
** Footer Creation

	IF nSTCount = 0
*!* For 944 process only!
		nCTNShortage = (nCTNExpected - nCTNActual - nCTNDamaged)
*    STORE "W14"+cfd+ALLTRIM(STR(nCTNExpected))+cfd+ALLTRIM(STR(nCTNExpected-nCTNShortage))+cfd+ALLTRIM(STR(nCTNShortage))+csegd TO cString
		STORE "W14"+cfd+ALLTRIM(STR(nCTNExpected))+csegd TO cString
		DO cstringbreak
		nSegCtr = nSegCtr + 1
*!* End 944 addition

		STORE  "SE"+cfd+ALLTRIM(STR(nSegCtr+1))+cfd+PADL(c_GrpCntrlNum,9,"0")+csegd TO cString
		DO cstringbreak
		nSegCtr = 0
		nSTSets = nSTSets + 1
	ENDIF

	STORE  "GE"+cfd+ALLTRIM(STR(nSTSets))+cfd+c_CntrlNum+csegd TO cString
	DO cstringbreak

	STORE  "IEA"+cfd+"1"+cfd+PADL(c_CntrlNum,9,"0")+csegd TO cString
	DO cstringbreak

	RETURN

****************************
PROCEDURE cstringbreak
****************************
*  FPUTS(nFilenum,cString)
	FWRITE(nFilenum,cString)
	RETURN

****************************
PROCEDURE num_incr
****************************
	SELECT serfile
	REPLACE serfile.seqnum WITH serfile.seqnum + 1 IN serfile
	REPLACE serfile.grpseqnum WITH serfile.grpseqnum + 1 IN serfile
	c_CntrlNum = ALLTRIM(STR(serfile.seqnum))
	c_GrpCntrlNum = ALLTRIM(STR(serfile.grpseqnum))
	RETURN

****************************
PROCEDURE ediupdate
****************************
	PARAMETERS lIsError,cFin
	IF cFin = "NO PO DATA IN ECHO"
		cFin = "NO 944 POSSIBLE: 'Echo' field empty"
	ENDIF
*SET STEP ON
	IF !lTesting
		SELECT EDI_TRIGGER
		LOCATE
		REPLACE processed WITH .T.,created WITH .F.,fin_status WITH cFin,errorflag WITH .F. ;
			FOR EDI_TRIGGER.wo_num = nWO_Num AND EDI_TRIGGER.accountid = nAcctnum AND EDI_TRIGGER.edi_type = "944"
		LOCATE
		IF !lIsError
			REPLACE when_proc WITH DATETIME(),file944crt WITH cFilenameArchive ;
				FOR EDI_TRIGGER.wo_num = nWO_Num ;
				AND EDI_TRIGGER.accountid = nAcctnum AND EDI_TRIGGER.edi_type = "944"
		ELSE
			REPLACE processed WITH .T.,created WITH .F.,fin_status WITH cFin,errorflag WITH .T. ;
				EDI_TRIGGER.file944crt WITH "" ;
				FOR EDI_TRIGGER.wo_num = nWO_Num AND EDI_TRIGGER.accountid = nAcctnum ;
				AND EDI_TRIGGER.edi_type = "944"
		ENDIF
		IF lsendmail AND leMail
			sendmail()
		ENDIF
	ENDIF
	closeout()
ENDPROC

****************************
PROCEDURE closeout
****************************
	IF !EMPTY(nFilenum)
		=FCLOSE(nFilenum)
		ERASE &cFilename
	ENDIF
	closedata()
ENDPROC

****************************
PROCEDURE closedata
****************************
	IF USED('edi_xfer')
		USE IN edi_xfer
	ENDIF
	IF USED('ctnucc')
		USE IN ctnucc
	ENDIF
	IF USED('indet')
		USE IN indet
	ENDIF
	IF USED('inwolog')
		USE IN inwolog
	ENDIF
	RELEASE ALL LIKE cContainer
	ON ERROR
	WAIT CLEAR
	_SCREEN.CAPTION = "INBOUND POLLER - EDI"
ENDPROC

****************************
PROCEDURE sendmail
****************************
	IF lIsError OR lCtnUCC
		tsubject = cMailName+" EDI ERROR (944), Inv. WO"+cWO_Num
		IF lCtnUCC
			tsendto = tsendtorcverr
			tcc = tccrcverr
		ELSE
			IF cFin = "NO PO DATA IN ECHO"
				SELECT 0
				USE F:\3pl\DATA\mailmaster ALIAS mm
				LOCATE FOR mm.edi_type = "MISC" AND mm.taskname = "GENERAL"
				tsendto = IIF(mm.use_alt,sendtoalt,sendto)
				tcc = IIF(mm.use_alt,ccalt,cc)
				USE IN mm
			ENDIF
		ENDIF
	ELSE
		tsubject = cMailName+" EDI FILE (944) Created, Inv. WO"+cWO_Num
	ENDIF

	tattach = " "
	IF (cTrk_WO = "0" OR EMPTY(cTrk_WO))
		cThisTrk_WO = "MISSING"
	ELSE
		cThisTrk_WO = cTrk_WO
	ENDIF
	tmessage = "Inv. WO #: "+cWO_Num+CHR(10)
	IF EMPTY(cErrMsg)
		tmessage = tmessage + cFin
	ELSE
		tmessage = tmessage + cErrMsg
	ENDIF
	IF cFin = "NO PO DATA IN ECHO"
		tmessage = tmessage + CHR(13)+CHR(13)+"Container/Airbill: "+cContainer
	ENDIF

	DO FORM m:\dev\frm\dartmail2 WITH tsendto,tfrom,tsubject,tcc,tattach,tmessage,"A"
ENDPROC

****************************
PROCEDURE segmentget
****************************

	PARAMETER thisarray,lcKey,nLength

	FOR i = 1 TO nLength
		IF i > nLength
			EXIT
		ENDIF
		lnEnd= AT("*",thisarray[i])
		IF lnEnd > 0
			lcThisKey =TRIM(SUBSTR(thisarray[i],1,lnEnd-1))
			IF OCCURS(lcKey,lcThisKey)>0
				RETURN SUBSTR(thisarray[i],lnEnd+1)
				i = 1
			ENDIF
		ENDIF
	ENDFOR

	RETURN ""
ENDPROC
