* This program extracts cutu data from inven
utilsetup("MJ_PACKSTAGE")
SET EXCLUSIVE OFF 
SET DELETED ON 
SET TALK OFF  
SET SAFETY OFF   &&Allows for overwrite of existing files

close databases all

Use F:\wh\sqlpassword In 0
gsqlpassword = Alltrim(sqlpassword.Password)
Use In sqlpassword
cServer = "tgfnjsql01"
SQLSetprop(0,'DispLogin',3)
SQLSetprop(0,"dispwarnings",.F.)
lcDSNLess="driver=SQL Server;server=&cServer;uid=SA;pwd=&gsqlpassword;DATABASE=PICKPACK"
nHandle=Sqlstringconnect(m.lcDSNLess,.T.)
If nHandle<=0 && bailout
  Wait Window At 6,2 "No Data Connection.."+Chr(13)+" Call MIS...."
  lcQuery = [Could not make connection to sp3 SQL server]+Chr(13)+"   "+lcDSNLess
  Throw
  Return
Endif

if usesql()
	xsqlexec("select *, convert(char,insdttm,101) as pickdt  from cartons where cartons.accountid in (6303,6325,6543) and insdttm>dateadd(day,-60,getdate())","tempdata",,"pickpack")
else
	lcQuery  = [select *,  CONVERT(CHAR,INSDTTM,101) AS pickdt  from cartons WHERE cartons.accountid in (6303,6325,6543)  AND INSDTTM>DATEADD(DAY,-60,GETDATE()) ]
	If SQLExec(nHandle,lcQuery,"tempdata")#1
		Wait Window At 6,2 "Error Cartons SQL query.........." Timeout 1
		SQLDisconnect(nHandle)
	Endif
endif

*******packed qty
SELECT OFFICE,ACCOUNTID AS ACCOUNT,PiCKDT as packdt, SUM(TOTQTY) AS PACKQTY FROM TEMPDATA WHERE UCC!='CUT' GROUP BY 1,2,3 INTO CURSOR P1 READWRITE 

*******labeled carton
xsqlexec("select mod as office, accountid as account, labeled, ctnqty from outship where mod='J' and inlist(accountid,6303,6543,6325) and labeled>={"+dtoc(date()-60)+"}",,,"wh")
select office, account, dtoc(labeled) as labeled, sum(ctnqty)as labeled_cartons from outship group by 1,2,3 into cursor sc1nj readwrite
*******labeled wo
xsqlexec("select mod as office, accountid as account, wo_num, labeled from outship where mod='J' and inlist(accountid,6303,6543,6325) and labeled>={"+dtoc(date()-60)+"}",,,"wh")
select office, account, wo_num, dtoc(labeled) as labeled from outship group by 1,2,3,4 into cursor sw1nj readwrite
select office,account, labeled, cnt(1) as labeled_wo from sw1nj group by office, account, labeled into cursor sw2nj readwrite
*******labeled pt
xsqlexec("select mod as office, accountid as account, ship_ref, labeled from outship where mod='J' and inlist(accountid,6303,6543,6325) and labeled>={"+dtoc(date()-60)+"}",,,"wh")
select office, account, ship_ref, dtoc(labeled) as labeled from outship group by 1,2,3,4 into cursor spt1nj readwrite
select office, account, labeled, cnt(1) as labeled_pt from spt1nj group by office, account, labeled into cursor spt2nj readwrite

*******labeled carton
xsqlexec("select mod as office, accountid as account, labeled, ctnqty from outship where mod='L' and inlist(accountid,6303,6543,6325) and labeled>={"+dtoc(date()-60)+"}",,,"wh")
select office, account, dtoc(labeled) as labeled, sum(ctnqty)as labeled_cartons from outship group by 1,2,3 into cursor sc1ml readwrite
*******labeled wo
xsqlexec("select mod as office, accountid as account, wo_num, labeled from outship where mod='L' and inlist(accountid,6303,6543,6325) and labeled>={"+dtoc(date()-60)+"}",,,"wh")
select office, account, wo_num, dtoc(labeled) as labeled from outship group by 1,2,3,4 into cursor sw1ml readwrite
select office,account, labeled, cnt(1) as labeled_wo from sw1nj group by office, account, labeled into cursor sw2ml readwrite
*******labeled pt
xsqlexec("select mod as office, accountid as account, ship_ref, labeled from outship where mod='L' and inlist(accountid,6303,6543,6325) and labeled>={"+dtoc(date()-60)+"}",,,"wh")
select office, account, ship_ref, dtoc(labeled) as labeled from outship group by 1,2,3,4 into cursor spt1ml readwrite
select office, account, labeled, cnt(1) as labeled_pt from spt1nj group by office, account, labeled into cursor spt2ml readwrite

SELECT * FROM sc1nj UNION select * FROM sc1ml INTO CURSOR sc1 READWRITE
SELECT * FROM sw2nj UNION select * FROM sw2ml INTO CURSOR sw2 READWRITE
SELECT * FROM sPT2nj UNION select * FROM sPT2ml INTO CURSOR sPT2 READWRITE
SELECT * FROM p1 a FULL JOIN sc1 b ON a.office=b.office AND a.account=b.account AND a.packdt=b.labeled INTO CURSOR c1 READWRITE 
REPLACE office_a WITH office_b FOR ISNULL(office_a) IN c1
REPLACE account_a WITH account_b FOR ISNULL(account_a) IN c1
REPLACE packdt WITH labeled FOR ISNULL(packdt) IN c1
REPLACE packqty WITH 0 FOR ISNULL(packqty) IN c1
REPLACE labeled_cartons WITH 0 FOR ISNULL(labeled_cartons) IN c1
SELECT office_a as office , account_a as account, packdt, packqty, labeled_cartons FROM c1 INTO CURSOR c2 READWRITE
SELECT * FROM c2 a FULL JOIN sw2 b ON a.office=b.office AND a.account=b.account AND a.packdt=b.labeled INTO CURSOR c3 READWRITE
REPLACE labeled_wo WITH 0 FOR ISNULL(labeled_wo) IN c3
SELECT office_a as office, account_a as account, packdt as transaction_date, packqty,labeled_cartons, labeled_wo FROM c3 INTO CURSOR c4 READWRITE

SELECT * FROM C4 A FULL JOIN SPT2 B ON  a.office=b.office AND a.account=b.account AND a.TRANSACTION_DATE=b.labeled INTO CURSOR cPT4 READWRITE
REPLACE office_a WITH office_b FOR ISNULL(office_a) IN cPT4
REPLACE account_a WITH account_b FOR ISNULL(account_a) IN CPT4
REPLACE TRANSACTION_DATE WITH labeled FOR ISNULL(TRANSACTION_DATE) IN CPT4
REPLACE packqty WITH 0 FOR ISNULL(packqty) IN CPT4
REPLACE labeled_cartons WITH 0 FOR ISNULL(labeled_cartons) IN CPT4
REPLACE labeled_wo WITH 0 FOR ISNULL(labeled_wo) IN CPT4
replace labeled_pt WITH 0 FOR ISNULL(labeled_pt) IN cpt4
SELECT office_a as office, account_a as account,  transaction_date, packqty,labeled_cartons, labeled_wo, labeled_pt FROM cpt4 INTO CURSOR cpt5 READWRITE

SET STEP ON 
SELECT *, SPACE(5) as loc, SPACE(20) as acctname FROM cpt5 INTO CURSOR c5 READWRITE
REPLACE loc WITH 'NJ' FOR OFFICE='J'
REPLACE loc WITH 'ML' FOR OFFICE='L'
REPLACE acctname WITH 'WHOLESALE' FOR ACCOUNT=6303
REPLACE acctname WITH 'PRESS SAMPLES' FOR ACCOUNT=6543
REPLACE acctname WITH 'DAMAGES' FOR ACCOUNT=6325
SELECT LOC AS OFFICE, ACCOUNT,ACCTNAME, TRANSACTION_DATE ,PACKQTY, labeled_CARTONS, labeled_WO,labeled_pt FROM C5 INTO CURSOR C6 READWRITE
*COPY TO S:\MarcJacobsData\Reports\mjdashboard\packstage type csv
COPY TO F:\FTPUSERS\MJ_Wholesale\OUT\reports\packstage type csv


schedupdate()
_screen.Caption=gscreencaption
on error