PARAMETER lTestIndicator
CLOSE DATA ALL
PUBLIC lTesting,len1

utilsetup('g3_945-997_ack')
_SCREEN.CAPTION = "G-III 945-997 Ack. Checker"
_setvars()

lTesting = lTestIndicator
cInfolder = "F:\FTPUSERS\G-III\997IN\"
cArchiveFolder = "F:\FTPUSERS\G-III\997IN\ARCHIVE\"
USE F:\3pl\DATA\edi_trigger IN 0 ALIAS edi_trigger
DO m:\dev\prg\lookups

CD &cInfolder
lenary1 = ADIR(ary1,"*.*")

lDo997Loops = .T.
IF lenary1 = 0
	WAIT WINDOW "No 997 files to process...skipping to reports" TIMEOUT 1
	lDo997Loops = .F.
ELSE
	WAIT WINDOW "Will now scan through "+TRANSFORM(lenary1)+" 997 files" TIMEOUT 2
ENDIF

nFound = 0
nNotFound = 0

IF lDo997Loops
	FOR runthis = 1 TO lenary1
		cFilename = ALLTRIM(ary1[runthis,1])
		WAIT WINDOW "Processing file "+cFilename TIMEOUT 1
		xfile = (cInfolder+cFilename)
		cArchivefile = (cArchiveFolder+cFilename)

		dDate = ary1[runthis,3]
		IF dDate < DATE()-7
			WAIT WINDOW "File "+cFilename+" too old...looping" NOWAIT
			COPY FILE [&xfile] TO [&cArchiveFile]
			DELETE FILE [&xfile]
			LOOP
		ENDIF

		DO m:\dev\prg\createx856a
		DO m:\dev\prg\loadedifile WITH xfile,"*","G3-997ACK","G-III"

		SELECT x856
*	BROWSE TIMEOUT 4
		LOCATE

		SCAN FOR x856.segment = "AK1"
			cISA_Num = PADL(ALLTRIM(x856.f2),9,"0")
			WAIT WINDOW "Checking ISA# "+cISA_Num NOWAIT
			SELECT edi_trigger
			LOCATE FOR g3account(edi_trigger.accountid) AND edi_trigger.isa_num = cISA_Num
			IF FOUND()
				SCAN FOR g3account(edi_trigger.accountid) AND edi_trigger.isa_num = cISA_Num
					WAIT WINDOW "ISA# found : "+cISA_Num NOWAIT
					REPLACE ack945 WITH .T. IN edi_trigger
					nFound = nFound+1
				ENDSCAN
			ELSE
				WAIT WINDOW "ISA# NOT found in Triggers: "+cISA_Num NOWAIT
				nNotFound = nNotFound+1
			ENDIF
		ENDSCAN
		COPY FILE [&xfile] TO [&cArchiveFile]
		DELETE FILE [&xfile]
		USE IN x856
	ENDFOR

	WAIT CLEAR

	?"ISAs found: "+TRANSFORM(nFound)
	?"ISAs NOT found: "+TRANSFORM(nNotFound)
ENDIF

IF lTesting
	SET STEP ON
ENDIF

xsqlexec("select * from account",,,"qq")

SELECT a.accountid,b.acctname,a.isa_num,a.bol,a.ship_ref,a.scac,a.wo_num,a.trig_time,a.when_proc,a.fin_status ;
	FROM edi_trigger a,account b;
	WHERE g3account(a.accountid) ;
	AND a.ack945 # .T. ;
	AND a.trig_time > (DATETIME()-(7*24*3600)) ;
	AND a.fin_status # "NO 945" ;
	AND a.accountid = b.accountid ;
	GROUP BY a.accountid,a.bol,a.ship_ref ;
	ORDER BY a.accountid,a.bol,a.ship_ref ;
	INTO CURSOR temp1

LOCATE
IF lTesting
	BROWSE
ENDIF

IF !EOF()
	cMiss997File = ("h:\fox\miss_997s_"+TTOC(DATETIME(),1)+".xls")
	COPY TO &cMiss997File TYPE XL5
	LOCATE
	IF lTesting
*		BROWSE TIMEOUT 3
	ENDIF

	IF lTesting
		tsendto = "joe.bianchi@tollgroup.com"
		tcc = ""
	ELSE
		tsendto = "paul.gaidis@tollgroup.com,esther.ludwig@g-iii.com,imamut@g-iii.com,alex.mitnik@g-iii.com"
		tcc = "joe.bianchi@tollgroup.com,Erica.Corea@tollgroup.com"
	ENDIF
	tsubject= "TGF G-III Missing 997s as of " +TTOC(DATETIME())
	tattach = cMiss997File
	tmessage = "Attached is a list of G-III shipments over the past week, for which no 997 has been received."
	tFrom ="TGF EDI Operations <toll-edi-ops@tollgroup.com>"
	IF lTesting
		tmessage = tmessage+CHR(13)+"*TEST DATA* - LOADED INTO F:\WHP TABLES"
	ENDIF
	DO FORM m:\dev\frm\dartmail2 WITH tsendto,tFrom,tsubject,tcc,tattach,tmessage,"A"
	DELETE FILE &cMiss997File

ENDIF

CLOSE DATA ALL
schedupdate()

RETURN
