*!* m:\dev\prg\rowone940_process.prg
ASSERT .F.
cOffice = "C"
CD &lcPath

LogCommentStr = ""

delimchar = cDelimiter
lcTranOpt= cTranslateOption

lnNum = ADIR(tarray,"*.*")

IF lnNum = 0
	WAIT WINDOW AT 10,10 "      No "+cCustname+" 940s to import     " TIMEOUT 3
	NormalExit = .T.
	THROW
ENDIF

FOR thisfile = 1  TO lnNum
	Xfile = lcPath+tarray[thisfile,1] && +"."
	cfilename = LOWER(tarray[thisfile,1])
	nFileSize = VAL(TRANSFORM(tarray(thisfile,2)))
	COPY FILE [&xfile] TO ("F:\ftpusers\rowone\940xfer\"+cfilename) && to create 997
	WAIT WINDOW "Importing file: "+cfilename NOWAIT
	DO m:\dev\prg\loadedifile WITH Xfile,delimchar,lcTranOpt,cCustname

	SELECT x856
	LOCATE
	COUNT TO N FOR x856.segment = "ISA"
	IF N > 1  && Extra ISA/IEA loops
		cISAFiles = IIF(EMPTY(cISAFiles),cfilename,cISAFiles+CHR(13)+cfilename)
		SET STEP ON 
		archivefile  = (lcArchivePath+JUSTFNAME(Xfile))
		IF !FILE(archivefile)
		SET STEP ON 
			COPY FILE [&Xfile] TO [&archivefile]
		ENDIF
		IF FILE(Xfile)
			DELETE FILE [&xfile]
		ENDIF
		LOOP
	ENDIF

	DO create_pt_cursors WITH cUseFolder,lLoadSQL && Added to prep for SQL Server transition

	lOK = .T.
	DO ("m:\dev\prg\"+cusename+"940_bkdn")
	IF lOK && If no divide-by-zero error
	archivefile  = ("F:\ftpusers\"+cCustname+"\940in\archive\"+JUSTFNAME(xfile))
		DO "m:\dev\prg\all940_import"

*		release_ptvars()
	ELSE
		archivefile  = ("F:\ftpusers\"+cCustname+"\940in\archive\"+JUSTFNAME(Xfile))
		IF !lTesting
			IF !FILE(archivefile)
			SET STEP ON 
				COPY FILE [&Xfile] TO [&archivefile]
			ENDIF
			IF FILE(Xfile)
				DELETE FILE [&xfile]
			ENDIF
		ENDIF
		LOOP
	ENDIF

NEXT thisfile

&& now clean up the 940in archive files, delete for the upto the last 10 days
deletefile(lcarchivepath,IIF(DATE()<{^2016-05-01},20,10))

WAIT CLEAR
IF !EMPTY(cISAFiles)
	SET STEP ON
	ISAMail()
ENDIF

WAIT "Entire "+cCustname+" 940 Process Now Complete" WINDOW TIMEOUT 5
NormalExit = .T.

*************************************************************************************************
PROCEDURE ISAMail
*************************************************************************************************
*	SET STEP ON
	IF lTestmail
		tsendto = tsendtotest
		tcc = tcctest
	ENDIF
	cMailLoc = IIF(cOffice = "C","West",IIF(cOffice = "N","New Jersey","Florida"))
	tattach = ""
	tsubject= "TGF "+cMailLoc+" "+cCustname+" Multiple ISA Error"
	tmessage = "Multiple ISA Loops detected in the following file(s), which were not processed:"
	tmessage = tmessage+CHR(13)+CHR(13)+cISAFiles
	tFrom ="TGF EDI Operations <toll-edi-ops@fmiint.com>"
	DO FORM m:\dev\frm\dartmail2 WITH tsendto,tFrom,tsubject,tcc,tattach,tmessage,"A"
ENDPROC
