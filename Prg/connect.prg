
*****************************************************
*!*	** IMPORTANT!!
*!*	** Only create views that DO NOT show restricted info, such as individual salaries.
*!*	** Also restrict WHERE clauses so that individual info cannot be selected.
*!*	*****************************************************
*!*	SET SAFETY OFF
*SET STEP ON
*!*	LOCAL lcUserID, lcPassword
*!*	lcUserID = "MBENNETT"
*!*	lcPassword = "vp737a"
OPEN DATABASE F:\UTIL\ADPCONNECTION\CONNECT1
*!*	*CREATE CONNECTION remote_01 DATASOURCE "Oracle Payroll Data" userid "MBENNETT" PASSWORD "vp737a"
*!*	*CREATE SQL VIEW vTotSalary  CONNECTION remote_01 SHARE AS SELECT SUM(ANNUALSALARY) AS TOTSALARY FROM V_EMPLOYEE

*!*	*CREATE CONNECTION remote_03 DATASOURCE "Oracle Payroll Data" USERID MBENNETT PASSWORD vp737a
*!*	CREATE CONNECTION remote_03 DATASOURCE "Oracle Payroll Data" USERID &lcUserID PASSWORD &lcPassword
*!*	*CREATE CONNECTION remote_03 DATASOURCE "Oracle Payroll Data" USERID MBENNETT PASSWORD vp37a



*!*	CREATE SQL VIEW vTotSalary ;
*!*	CONNECTION remote_03 AS ;
*!*	SELECT sum(V_employee.ANNUALSALARY) ;
*!*	 FROM ;
*!*	     REPORTS.V_EMPLOYEE V_employee

*!*	CREATE SQL VIEW vtest ;
*!*		CONNECTION cnORACLE AS ;
*!*		SELECT A.REGHOURS, A.OTHOURS, A.FILE# AS FILENUM, B.HOMEDEPARTMENT ;
*!*		FROM REPORTS.V_PAYDATA_ALL A ;
*!*		LEFT OUTER JOIN REPORTS.V_EMPLOYEE B ;
*!*		ON B.FILE# = A.FILE#

lcSQL = ;
	"SELECT SUM(A.REGHOURS) AS REGHOURS, SUM(A.OTHOURS) AS OTHOURS, B.HOMEDEPARTMENT, " + ;
	" {fn substring(B.HOMEDEPARTMENT,1,2)} as division, {fn substring(B.HOMEDEPARTMENT,3,4)} as department" + ;
	" FROM { oj REPORTS.V_PAYDATA_ALL A RIGHT OUTER JOIN REPORTS.V_EMPLOYEE B " + ;
	" ON B.FILE# = A.FILE# }" + ;
	" WHERE B.COMPANYCODE = 'SXI' " + ;
	" GROUP BY HOMEDEPARTMENT " + ;
	" ORDER BY HOMEDEPARTMENT "

*!*	lcSQL = ;
*!*		"SELECT A.REGHOURS, A.OTHOURS, A.FILE# AS FILENUM " + ;
*!*		" FROM REPORTS.V_PAYDATA_ALL A " 

lnHandle = SQLCONNECT("Oracle Payroll Data","MBENNETT","vp737a")
IF lnHandle > 0 THEN
	lnResult = sqlexec(lnHandle, lcSQL, 'mycursor')
ENDIF

SELECT mycursor
BROWSE

*!*	USE vtest
*!*	BROWSE
*!*	USE
