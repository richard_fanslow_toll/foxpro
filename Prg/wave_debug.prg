* Find most current mm.exe

declare aexe[1]
adir("aexe","wave_debug*.exe")
asort(aexe,1)

exedt=aexe(1,3)
exetime=aexe(1,4)
exenum=aexe(1,1)
for i=2 to alen(aexe,1)
	if (aexe(i,3)>exedt or (aexe(i,3)=exedt and aexe(i,4)>exetime)) and left(upper(aexe(i,1)),5)#"WAVE_DEBUG"
		exedt=aexe(i,3)
		exetime=aexe(i,4)
		exenum=aexe(i,1)
	endif
next

wshshell=createobject("wscript.shell")
wshshell.run(exenum)


