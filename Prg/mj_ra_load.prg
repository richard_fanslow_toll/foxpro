utilsetup("MJ_RA_LOAD")
CLOSE DATABASES ALL
Set Exclusive Off
Set Deleted On
Set Talk Off
Set Safety Off
*!*  With _Screen
*!*  	.AutoCenter = .T.
*!*  	.WindowState = 0
*!*  	.BorderStyle = 1
*!*  	.Width = 320
*!*  	.Height = 210
*!*  	.Top = 290
*!*  	.Left = 110
*!*  	.Closable = .T.
*!*  	.MaxButton = .T.
*!*  	.MinButton = .T.
*!*  	.ScrollBars = 0
*!*  	.Caption = "MJ_RA_LOAD"
*!*  Endwith

set step on

If !Used("mj_ra_return")
**Use c:\\MJ_RA_RETURN2.Dbf ALIAS mj_ra_return In 0  && for testing
	Use F:\wh\mj_ra_return.Dbf Alias mj_ra_return In 0
Endif
Set Step On
Xpath='F:\FTPUSERS\MJ_Wholesale\In\RA\'
Xarchivepath='F:\FTPUSERS\MJ_Wholesale\In\RA\archive\'
len1 = Adir(ary1,Xpath+"*.*")
If len1 = 0
	Wait Window "No files found...exiting" Timeout 2
	Close Data All

	On Error
	Return
Endif

For xrow = 1 To len1
	Set Step On
	Create Cursor BC1 (RA C(16), b C(30), CUST_ACCT C(20), CONSIGNEE C(50), e C(20), CLAIM C(25), g C(7), h C(10), i C(10), j C(22), k C(22), qty n(8), loaded l(1))
	Tfile=Alltrim(ary1[xrow,1])
	tfilepath=Xpath+Tfile
	tfilearchivepath=Xarchivepath+Tfile
	Appe From "&tfilepath" Type Deli
	Delete For Empty(RA) In BC1
	Delete For RA='Order' In BC1
	Delete For RA='OTC' In BC1
	Replace loaded With .F. For !Empty(RA)
*	Copy File [&tfilepath] To [&tfilearchivepath]
*	Delete File [&tfilepath]
	Select BC1
	replace ALL  qty WITH -1*qty FOR qty<0 IN bc1
***********************outslip
	Select CLAIM As RA,RA As CLAIM, CONSIGNEE, CUST_ACCT, qty From BC1 Where !Empty(CLAIM) And Substr(CLAIM,1,1)='2' And Substr(CLAIM,7,1)=' 'And Substr(CLAIM,6,1)!=' '  Into Cursor bcSLIP Readwrite
	Replace loaded With .T. For !Empty(CLAIM) And Substr(CLAIM,1,1)='2' And Substr(CLAIM,7,1)=' 'And Substr(CLAIM,6,1)!=' '  In BC1

***********************not empty claim but not an outslip
	Select RA,CLAIM, CONSIGNEE, CUST_ACCT ,  qty From BC1 Where !Empty(CLAIM) And !(Substr(CLAIM,1,1)='2' And Substr(CLAIM,7,1)=' 'And Substr(CLAIM,6,1)!=' ')  Into Cursor bcNOTSLIP Readwrite
	Replace loaded With .T. For !Empty(CLAIM) And !(Substr(CLAIM,1,1)='2' And Substr(CLAIM,7,1)=' 'And Substr(CLAIM,6,1)!=' ') In BC1

	Select RA, CLAIM,CONSIGNEE,CUST_ACCT ,  qty  From BC1 Where Empty(CLAIM) And (RA='10-' Or Substr(RA,1,1)='6' Or Substr(RA,1,1)='7') Into Cursor bcnotslip2 Readwrite
	Replace loaded With .T. For Empty(CLAIM) And (RA='10-' Or Substr(RA,1,1)='6' Or Substr(RA,1,1)='7')   In BC1

	Replace RA With '10-'+RA For RA !='10-' And (Substr(RA,1,1)='6' Or Substr(RA,1,1)='7' ) In  bcnotslip2
	Replace RA With '10-'+RA For RA !='10-' And (Substr(RA,1,1)='6' Or Substr(RA,1,1)='7' ) In  bcNOTSLIP
	Replace CLAIM With '' For RA='10-' In bcNOTSLIP
	Replace CLAIM With '' For RA='10-' In bcnotslip2

	Select * From  bcSLIP Union All Select * From bcNOTSLIP Union All Select * From bcnotslip2 Into Cursor BC4 Readwrite
	Select Distinct(RA) As RA From BC4 Into Cursor bc5 Readwrite
	Delete  From mj_ra_return Where RA In(Select RA From bc5)
	Delete For Empty(RA)
	Select BC4
	Scan
		Scatter Memvar
		Select mj_ra_return
		Locate For (mj_ra_return.RA)=Alltrim(BC4.RA)
		If !Found("MJ_RA_RETURN")
			m.adddt=Datetime()
			m.updatedt=Datetime()
			Append Blank
			Gather Memvar
		Endif
	Endscan
ENDFOR

schedupdate()
*!*	SELECT ra FROM bc4 WHERE ra='2' INTO CURSOR voutslip READWRITE 
*!*	If Reccount() > 0
*!*		Export To "S:\MarcJacobsData\TEMP\ra_outslips_loaded"  Type Xls
*!*		tsendto = "tmarg@fmiint.com, todd.margolin@tollgroup.com, Yenny.Garcia@tollgroup.com, ana.hernandez@tollgroup.com" 
*!*		tattach = "S:\MarcJacobsData\TEMP\ra_outslips_loaded.xls"
*!*		tcc =""
*!*		tFrom ="Toll EDI System Operations <fmi-transload-ops@fmiint.com>"
*!*		tmessage = "MJ RA outslips loaded :  "+Ttoc(Datetime())
*!*		tSubject = "MJ RA outslips loaded "
*!*		Do Form m:\dev\frm\dartmail2 With tsendto,tFrom,tSubject, tcc,tattach,tmessage  ,"A"
*!*	Else
*!*	Endif
*!*	Set Step On
*!*	Select * From BC1 Where !loaded Into Cursor notload Readwrite



*!*	If Reccount() > 0
*!*		Export To "S:\MarcJacobsData\TEMP\ra_not_loaded"  Type Xls
*!*		tsendto = "todd.margolin@tollgroup.com"
*!*		tattach = "S:\MarcJacobsData\TEMP\ra_not_loaded.xls"
*!*		tcc =""
*!*		tFrom ="Toll EDI System Operations <fmi-transload-ops@fmiint.com>"
*!*		tmessage = "MJ RA not loaded :  "+Ttoc(Datetime())
*!*		tSubject = "MJ RA not loaded "
*!*		Do Form m:\dev\frm\dartmail2 With tsendto,tFrom,tSubject, tcc,tattach,tmessage  ,"A"
*!*	Else
*!*	Endif

*!*	If !Used("mj_ra_return")
*!*		Use F:\wh\mj_ra_return.Dbf In 0
*!*	Endif
*!*	Select RA, Cnt(1) As Count From mj_ra_return Group By 1 Into Cursor t1 Readwrite
*!*	Select * From t1 Where Count !=1 Into Cursor t2 Readwrite
*!*	If Reccount() > 0
*!*		Export To "S:\MarcJacobsData\TEMP\duplicate_ra"  Type Xls
*!*		tsendto = "todd.margolin@tollgroup.com"
*!*		tattach = "S:\MarcJacobsData\TEMP\duplicate_ra.xls"
*!*		tcc =""
*!*		tFrom ="Toll EDI System Operations <fmi-transload-ops@fmiint.com>"
*!*		tmessage = "MJ duplicate RA exists_:"+Ttoc(Datetime())
*!*		tSubject = "MJ duplicate RA exists"
*!*		Do Form m:\dev\frm\dartmail2 With tsendto,tFrom,tSubject, tcc,tattach,tmessage  ,"A"
*!*	Else
*!*	Endif

*!*	schedupdate()
*!*	_Screen.Caption=gscreencaption





*********************************************************************   SQL
utilsetup("MJ_RA_LOAD")
CLOSE DATABASES ALL
Set Exclusive Off
Set Deleted On
Set Talk Off
Set Safety Off
*!*  With _Screen
*!*  	.AutoCenter = .T.
*!*  	.WindowState = 0
*!*  	.BorderStyle = 1
*!*  	.Width = 320
*!*  	.Height = 210
*!*  	.Top = 290
*!*  	.Left = 110
*!*  	.Closable = .T.
*!*  	.MaxButton = .T.
*!*  	.MinButton = .T.
*!*  	.ScrollBars = 0
*!*  	.Caption = "MJ_RA_LOAD"
*!*  Endwith

useca('mjraret','wh',.t.)
Set Step On
Xpath='F:\FTPUSERS\MJ_Wholesale\In\RA\'
Xarchivepath='F:\FTPUSERS\MJ_Wholesale\In\RA\archive\'
len1 = Adir(ary1,Xpath+"*.*")
If len1 = 0
	Wait Window "No files found...exiting" Timeout 2
	Close Data All

	On Error
	Return
Endif

For xrow = 1 To len1
	Set Step On
	Create Cursor BC1 (RA C(16), b C(30), CUST_ACCT C(20), CONSIGNEE C(50), e C(20), CLAIM C(25), g C(7), h C(10), i C(10), j C(22), k C(22), qty n(8), loaded l(1))
	Tfile=Alltrim(ary1[xrow,1])
	tfilepath=Xpath+Tfile
	tfilearchivepath=Xarchivepath+Tfile
	Appe From "&tfilepath" Type Deli
	Delete For Empty(RA) In BC1
	Delete For RA='Order' In BC1
	Delete For RA='OTC' In BC1
	Replace loaded With .F. For !Empty(RA)
	Copy File [&tfilepath] To [&tfilearchivepath]
	Delete File [&tfilepath]
	Select BC1
	replace ALL  qty WITH -1*qty FOR qty<0 IN bc1
***********************outslip
	Select CLAIM As RA,RA As CLAIM, CONSIGNEE, CUST_ACCT, qty From BC1 Where !Empty(CLAIM) And Substr(CLAIM,1,1)='2' And Substr(CLAIM,7,1)=' 'And Substr(CLAIM,6,1)!=' '  Into Cursor bcSLIP Readwrite
	Replace loaded With .T. For !Empty(CLAIM) And Substr(CLAIM,1,1)='2' And Substr(CLAIM,7,1)=' 'And Substr(CLAIM,6,1)!=' '  In BC1

***********************not empty claim but not an outslip
	Select RA,CLAIM, CONSIGNEE, CUST_ACCT ,  qty From BC1 Where !Empty(CLAIM) And !(Substr(CLAIM,1,1)='2' And Substr(CLAIM,7,1)=' 'And Substr(CLAIM,6,1)!=' ')  Into Cursor bcNOTSLIP Readwrite
	Replace loaded With .T. For !Empty(CLAIM) And !(Substr(CLAIM,1,1)='2' And Substr(CLAIM,7,1)=' 'And Substr(CLAIM,6,1)!=' ') In BC1

	Select RA, CLAIM,CONSIGNEE,CUST_ACCT ,  qty  From BC1 Where Empty(CLAIM) And (RA='10-' Or Substr(RA,1,1)='6' Or Substr(RA,1,1)='7') Into Cursor bcnotslip2 Readwrite
	Replace loaded With .T. For Empty(CLAIM) And (RA='10-' Or Substr(RA,1,1)='6' Or Substr(RA,1,1)='7')   In BC1

	Replace RA With '10-'+RA For RA !='10-' And (Substr(RA,1,1)='6' Or Substr(RA,1,1)='7' ) In  bcnotslip2
	Replace RA With '10-'+RA For RA !='10-' And (Substr(RA,1,1)='6' Or Substr(RA,1,1)='7' ) In  bcNOTSLIP
	Replace CLAIM With '' For RA='10-' In bcNOTSLIP
	Replace CLAIM With '' For RA='10-' In bcnotslip2

	Select * From  bcSLIP Union All Select * From bcNOTSLIP Union All Select * From bcnotslip2 Into Cursor BC4 Readwrite
	Select Distinct(RA) As RA From BC4 Into Cursor bc5 Readwrite
	Delete  From mjraret Where RA In(Select RA From bc5)
	Delete For Empty(RA)
	Select BC4
	Scan
		Scatter Memvar
		Select mjraret
		Locate For (mjraret.RA)=Alltrim(BC4.RA)
		If !Found("mjraret")
		GMASTEROFFICE='N'
		GOFFICE='J'
			m.adddt=Datetime()
			m.addby='LOAD'
			m.addproc='LOAD'
			m.updatedt=Datetime()
			m.updateby='LOAD'
			m.updproc='LOAD'
			 Insertinto ("MJRARET","WH",.T.)
		Endif
	Endscan
ENDFOR
TU("mjraret")

SELECT ra FROM bc4 WHERE ra='2' INTO CURSOR voutslip READWRITE 
If Reccount() > 0
	Export To "S:\MarcJacobsData\TEMP\ra_outslips_loaded"  Type Xls
	tsendto = "tmarg@fmiint.com, todd.margolin@tollgroup.com, Yenny.Garcia@tollgroup.com, ana.hernandez@tollgroup.com" 
	tattach = "S:\MarcJacobsData\TEMP\ra_outslips_loaded.xls"
	tcc =""
	tFrom ="Toll EDI System Operations <fmi-transload-ops@fmiint.com>"
	tmessage = "MJ RA outslips loaded :  "+Ttoc(Datetime())
	tSubject = "MJ RA outslips loaded "
	Do Form m:\dev\frm\dartmail2 With tsendto,tFrom,tSubject, tcc,tattach,tmessage  ,"A"
Else
Endif
Set Step On
Select * From BC1 Where !loaded Into Cursor notload Readwrite



If Reccount() > 0
	Export To "S:\MarcJacobsData\TEMP\ra_not_loaded"  Type Xls
	tsendto = "todd.margolin@tollgroup.com"
	tattach = "S:\MarcJacobsData\TEMP\ra_not_loaded.xls"
	tcc =""
	tFrom ="Toll EDI System Operations <fmi-transload-ops@fmiint.com>"
	tmessage = "MJ RA not loaded :  "+Ttoc(Datetime())
	tSubject = "MJ RA not loaded "
	Do Form m:\dev\frm\dartmail2 With tsendto,tFrom,tSubject, tcc,tattach,tmessage  ,"A"
Else
Endif

Select RA, Cnt(1) As Count From mjraret Group By 1 Into Cursor t1 Readwrite
Select * From t1 Where Count !=1 Into Cursor t2 Readwrite
If Reccount() > 0
	Export To "S:\MarcJacobsData\TEMP\duplicate_ra"  Type Xls
	tsendto = "todd.margolin@tollgroup.com"
	tattach = "S:\MarcJacobsData\TEMP\duplicate_ra.xls"
	tcc =""
	tFrom ="Toll EDI System Operations <fmi-transload-ops@fmiint.com>"
	tmessage = "MJ duplicate RA exists_:"+Ttoc(Datetime())
	tSubject = "MJ duplicate RA exists"
	Do Form m:\dev\frm\dartmail2 With tsendto,tFrom,tSubject, tcc,tattach,tmessage  ,"A"
Else
Endif


_Screen.Caption=gscreencaption

