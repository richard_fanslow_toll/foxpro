*****
SET EXCLUSIVE OFF 
SET DELETED ON 
SET TALK OFF  
SET SAFETY OFF
_screen.WindowState=1

do m:\dev\prg\_setvars
Set Deleted off
Set Century on

if usesqloutwo("J")
	xsqlexec("select ship_ref, wo_num, bol_no, routedfor, wo_date, del_date, consignee, ship_via, cnee_ref, appt, " + ;
		"qty, ctnqty, called, apptremarks, apptstatus, truckloaddt, staged, labeled, picked, pulled " + ;
		"from outship where mod='J' and accountid=6303 and del_date>={"+dtoc(date()-60)+"} and notonwip=0","xdy",,"wh")

	select "MJ Wholesale" as acctname, ship_ref, wo_num, bol_no, routedfor, wo_date, del_date, consignee, ship_via, ;
		cnee_ref, appt, qty, ctnqty, called, apptremarks, apptstatus, "          " as procstatus, truckloaddt, staged, ;
		labeled, picked, pulled from xdy into cursor mjstats readwrite

else
	If !Used("outship")
	  use f:\whj\whdata\outship In 0
	ENDIF

	Select "MJ Wholesale" as acctname,ship_ref,wo_num,bol_no,routedfor,wo_date,del_date,consignee,ship_via,cnee_ref,appt,qty,ctnqty,;
	  called,apptremarks,apptstatus,"          " as procstatus,truckloaddt,staged,labeled,picked,pulled from outship where accountid = 6303;
	  and !Emptynul(del_date) and del_date >= Date()-60 and !notonwip into cursor mjstats readwrite
endif

Select mjstats
replace all procstatus with iif(!pulled,"NOT PULLED",iif(!emptynul(del_date),"DELIVERED",iif(!emptynul(truckloaddt),"LOADED",iif(!emptynul(staged),"STAGED",iif(!emptynul(labeled),"LABELED",iif(!emptynul(picked),"PICKED","PULLED"))))))

Export to s:\marcjacobsdata\reports\New_Wholesale_deliveries.xls type xls



*!*	Select "MJ Retail" as acctname,ship_ref,wo_num,bol_no,routedfor,wo_date,del_date,consignee,ship_via,cnee_ref,appt,qty,ctnqty,;
*!*	  called,apptremarks,apptstatus,"          " as procstatus,truckloaddt,staged,labeled,picked,pulled from outship where INLIST(accountid ,6304);
*!*	  and !Emptynul(del_date) and del_date >= Date()-60 and !notonwip into cursor mjstats readwrite
*!*	  
*!*	Select mjstats
*!*	replace all procstatus with iif(!pulled,"NOT PULLED",iif(!emptynul(del_date),"DELIVERED",iif(!emptynul(truckloaddt),"LOADED",iif(!emptynul(staged),"STAGED",iif(!emptynul(labeled),"LABELED",iif(!emptynul(picked),"PICKED","PULLED"))))))
*!*	Export to s:\marcjacobsdata\reports\New_Retail_deliveries.xls type xls

if usesqloutwo("J")
	xsqlexec("select ship_ref, wo_num, bol_no, routedfor, wo_date, del_date, consignee, ship_via, cnee_ref, appt, qty, " + ;
		"ctnqty, called, apptremarks, apptstatus, truckloaddt, staged, labeled, picked, pulled " + ;
		"from outship where mod='J' and consignee='GSI' and del_date>={"+dtoc(date()-60)+"} and notonwip=0",,,"wh")

	select "GSI" as acctname, ship_ref, wo_num, bol_no, routedfor, wo_date, del_date, consignee, ship_via, cnee_ref, appt, qty, ctnqty,;
	  called, apptremarks, apptstatus, "          " as procstatus, truckloaddt, staged, labeled, picked, pulled ;
	  from outship into cursor mjstats readwrite

else
	Select "GSI" as acctname,ship_ref,wo_num,bol_no,routedfor,wo_date,del_date,consignee,ship_via,cnee_ref,appt,qty,ctnqty,;
	  called,apptremarks,apptstatus,"          " as procstatus,truckloaddt,staged,labeled,picked,pulled from outship where consignee='GSI';
	  and !Emptynul(del_date) and del_date >= Date()-60 and !notonwip into cursor mjstats readwrite
endif

Select mjstats
replace all procstatus with iif(!pulled,"NOT PULLED",iif(!emptynul(del_date),"DELIVERED",iif(!emptynul(truckloaddt),"LOADED",iif(!emptynul(staged),"STAGED",iif(!emptynul(labeled),"LABELED",iif(!emptynul(picked),"PICKED","PULLED"))))))
set step on 
IF SUBSTR(TIME(),1,2)='08'
If Reccount() > 0
Export to s:\marcjacobsdata\reports\New_GSI_deliveries.xls type xls

  tsendto = "tmarg@fmiint.com,l.lee@marcjacobs.com,p.markovic@marcjacobs.com"
  tattach = "s:\marcjacobsdata\reports\New_GSI_deliveries.xls"
  tcc =""
  tFrom ="Toll/FMI EDI System Operations <fmi-transload-ops@fmiint.com>"
  tmessage = "GSI Delivery report for   "+Ttoc(Datetime())
  tSubject = "GSI Delivery report"
  Do Form m:\dev\frm\dartmail2 With tsendto,tFrom,tSubject, tcc,tattach,tmessage  ,"A"
ELSE 
  tsendto = "tmarg@fmiint.com,l.lee@marcjacobs.com,p.markovic@marcjacobs.com"
  tattach = ""
  tcc =""
  tFrom ="Toll/FMI EDI System Operations <fmi-transload-ops@fmiint.com>"
  tmessage = "NO GSI Delivery report"+Ttoc(Datetime())
  tSubject = "NO GSI Delivery report exist"
  Do Form m:\dev\frm\dartmail2 With tsendto,tFrom,tSubject, tcc,tattach,tmessage  ,"A"
ENDIF
ELSE
endif
*!*	Select "MJ UK" as acctname,ship_ref,wo_num,bol_no,routedfor,wo_date,del_date,consignee,ship_via,cnee_ref,appt,qty,ctnqty,;
*!*	  called,apptremarks,apptstatus,"          " as procstatus,truckloaddt,staged,labeled,picked,pulled from outship where accountid = 6321;
*!*	  and !Emptynul(del_date) and del_date >= Date()-60 and !notonwip into cursor mjstats readwrite
*!*	Select mjstats
*!*	replace all procstatus with iif(!pulled,"NOT PULLED",iif(!emptynul(del_date),"DELIVERED",iif(!emptynul(truckloaddt),"LOADED",iif(!emptynul(staged),"STAGED",iif(!emptynul(labeled),"LABELED",iif(!emptynul(picked),"PICKED","PULLED"))))))
*!*	Export to s:\marcjacobsdata\reports\New_UK_deliveries.xls type xls

*!*	Select "MJ France" as acctname,ship_ref,wo_num,bol_no,routedfor,wo_date,del_date,consignee,ship_via,cnee_ref,appt,qty,ctnqty,;
*!*	  called,apptremarks,apptstatus,"          " as procstatus,truckloaddt,staged,labeled,picked,pulled from outship where accountid = 6322;
*!*	  and !Emptynul(del_date) and del_date >= Date()-60 and !notonwip into cursor mjstats readwrite
*!*	Select mjstats
*!*	replace all procstatus with iif(!pulled,"NOT PULLED",iif(!emptynul(del_date),"DELIVERED",iif(!emptynul(truckloaddt),"LOADED",iif(!emptynul(staged),"STAGED",iif(!emptynul(labeled),"LABELED",iif(!emptynul(picked),"PICKED","PULLED"))))))
*!*	Export to s:\marcjacobsdata\reports\New_France_deliveries.xls type xls

*!*	Select "MJ Italy" as acctname,ship_ref,wo_num,bol_no,routedfor,wo_date,del_date,consignee,ship_via,cnee_ref,appt,qty,ctnqty,;
*!*	  called,apptremarks,apptstatus,"          " as procstatus,truckloaddt,staged,labeled,picked,pulled from outship where accountid = 6323;
*!*	  and !Emptynul(del_date) and del_date >= Date()-60 and !notonwip into cursor mjstats readwrite
*!*	Select mjstats
*!*	replace all procstatus with iif(!pulled,"NOT PULLED",iif(!emptynul(del_date),"DELIVERED",iif(!emptynul(truckloaddt),"LOADED",iif(!emptynul(staged),"STAGED",iif(!emptynul(labeled),"LABELED",iif(!emptynul(picked),"PICKED","PULLED"))))))
*!*	Export to s:\marcjacobsdata\reports\New_Italy_deliveries.xls type xls

*!*	Select "MJ Partner" as acctname,ship_ref,wo_num,bol_no,routedfor,wo_date,del_date,consignee,ship_via,cnee_ref,appt,qty,ctnqty,;
*!*	  called,apptremarks,apptstatus,"          " as procstatus,truckloaddt,staged,labeled,picked,pulled from outship where accountid = 6320;
*!*	  and !Emptynul(del_date) and del_date >= Date()-60 and !notonwip into cursor mjstats readwrite
*!*	Select mjstats
*!*	replace all procstatus with iif(!pulled,"NOT PULLED",iif(!emptynul(del_date),"DELIVERED",iif(!emptynul(truckloaddt),"LOADED",iif(!emptynul(staged),"STAGED",iif(!emptynul(labeled),"LABELED",iif(!emptynul(picked),"PICKED","PULLED"))))))
*!*	Export to s:\marcjacobsdata\reports\New_Partner_deliveries.xls type xls

USE IN outship

if usesqloutwo("L")
	xsqlexec("select ship_ref, wo_num, bol_no, routedfor, wo_date, del_date, consignee, ship_via, cnee_ref, appt, " + ;
		"qty, ctnqty, called, apptremarks, apptstatus, truckloaddt, staged, labeled, picked, pulled " + ;
		"from outship where mod='L' and accountid=6303 and del_date>={"+dtoc(date()-60)+"} and notonwip=0",,,"wh")

	select "MJ Wholesale" as acctname, ship_ref, wo_num, bol_no, routedfor, wo_date, del_date, consignee, ship_via, ;
		cnee_ref, appt, qty, ctnqty, called, apptremarks, apptstatus, "          " as procstatus, truckloaddt, staged, ;
		labeled, picked, pulled from xdy into cursor mjstatsml readwrite

else
	If !Used("outship")
	  use f:\whl\whdata\outship In 0
	ENDIF

	Select "MJ Wholesale" as acctname,ship_ref,wo_num,bol_no,routedfor,wo_date,del_date,consignee,ship_via,cnee_ref,appt,qty,ctnqty,;
	  called,apptremarks,apptstatus,"          " as procstatus,truckloaddt,staged,labeled,picked,pulled from outship where accountid = 6303;
	  and !Emptynul(del_date) and del_date >= Date()-60 and !notonwip into cursor mjstatsml readwrite
endif

Select mjstatsml
replace all procstatus with iif(!pulled,"NOT PULLED",iif(!emptynul(del_date),"DELIVERED",iif(!emptynul(truckloaddt),"LOADED",iif(!emptynul(staged),"STAGED",iif(!emptynul(labeled),"LABELED",iif(!emptynul(picked),"PICKED","PULLED"))))))

Export to s:\marcjacobsdata\reports\ML_New_Wholesale_deliveries.xls type xls

USE IN mjstats
USE IN outship
USE IN mjstatsml

