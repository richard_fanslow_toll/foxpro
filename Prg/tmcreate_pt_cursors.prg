PARAMETERS cUseFolder,lLoadSQL

llSqlTesting = .t.  

IF USED("pt")
USE IN pt
endif
IF USED("ptdet")
USE IN ptdet
endif

IF lTesting
	xsqlexec("select * from pt where .f.","xpt",,"wh")
	SELECT xpt
	INDEX ON ship_ref TAG ship_ref
	INDEX ON ptid TAG ptid
	SET ORDER TO TAG ship_ref
	xsqlexec("select * from ptdet where .f.","xptdet",,"wh")
	SELECT xptdet
ELSE
*!* Set up temp structure for XPTDET
	USE (cUseFolder+"PTDET") IN 0 SHARED NOUPDATE
	SELECT ptdet
	SELECT * FROM ptdet WHERE .F. INTO CURSOR xptdet READWRITE
	USE IN ptdet

*!* Set up temp structure for XPT
	USE (cUseFolder+"PT") IN 0 SHARED NOUPDATE
	SELECT pt
	SELECT * FROM pt WHERE .F. INTO CURSOR xpt READWRITE
	SELECT xpt
	INDEX ON ship_ref TAG ship_ref
	INDEX ON ptid TAG ptid
	SET ORDER TO TAG ship_ref
	USE IN pt

ENDIF

SELECT xpt