*runack("MORET945CONFIRM")

SET ESCAPE ON
ON ESCAPE CANCEL

CLEAR
CLEAR ALL
CLOSE DATABASES ALL
PUBLIC nAccountid,cFilename,dconfirm_dt,lTesting,dDaysBack,cOfficeUsed,cState,tsendto,tcc,lUnconfirmed,nfor,lTesting,lStartDigit,lEmail

DO m:\dev\prg\_setvars WITH .T.
DO m:\dev\prg\lookups

lTesting = .F.
lEmail = .T.
lDoScreen = .t.

_SCREEN.CAPTION = "MORET 945 CONFIRMATION PROCESS"
IF lDoScreen OR lTesting
	_SCREEN.WINDOWSTATE =IIF(lTesting,2,1)
ENDIF

IF !lTesting
	utilsetup("MORET945CONFIRM")
ENDIF

SELECT 0
USE F:\edirouting\FTPSETUP SHARED
REPLACE chkbusy WITH .F. FOR FTPSETUP.TRANSFER = "997-MORET-IN-SP"
USE IN FTPSETUP

IF USED('edi_trigger')
	USE IN edi_trigger
ENDIF

USE F:\3pl\DATA\edi_trigger

lStartDigit = IIF(lTesting,2,1)
lStartDigit = 1
FOR j = lStartDigit TO 3
	CLEAR
	cExcelFile=""
	lExit = .F.
	DO CASE
		CASE j = 1
			cState = "FL"
			cOfficeUsed = "M"
		CASE j = 2
			cState = "ML"
			cOfficeUsed = "L"
		OTHERWISE
			cState = "SP"
			cOfficeUsed = "C"
	ENDCASE

	WAIT WINDOW "Now Checking Received Confirmations in EDI_TRIGGER for State "+cState TIMEOUT 2
	check_trigger_confirm()
ENDFOR

IF lTesting
	SELECT edi_trigger
	BROWSE FOR edi_trigger.moret AND !ack945 AND when_proc>DATETIME(2013,10,01,00,00,01)
ENDIF

CLOSE DATA ALL
WAIT WINDOW "Process complete" TIMEOUT 2

*******************************
PROCEDURE check_trigger_confirm
*******************************
*SET STEP ON
	IF lTesting
		ASSERT .F. MESSAGE "In Trigger Confirm Mailing"
	ENDIF
	WAIT WINDOW "Now scanning EDI_TRIGGER ("+cState+") for overdue confirmations" NOWAIT
	SELECT isa_num,accountid,ICASE(cState="FL","Miami",cState="ML","Mira Loma","San Pedro") AS warehouse,bol,ship_ref,trig_time ;
		FROM edi_trigger ;
		WHERE (INLIST(accountid,&gmoretacctlist) OR INLIST(accountid,&gMoretAcctList2)) ;
		AND BETWEEN(TTOD(trig_time),DATE()-15,DATE()) ;
		AND edi_type = "945" ;
		AND !EMPTY(when_proc) ;
		AND !errorflag ;
		AND fin_status # "NO 945" ;
		AND EMPTY(confirm_dt) ;
		AND office = cOfficeUsed ;
		AND when_proc < (DATETIME()-3600) ;
		INTO CURSOR tempconf

	SELECT tempconf
	LOCATE
	IF EOF()
		WAIT WINDOW "No unconfirmed 945s for "+cState TIMEOUT 2
		lUnconfirmed = .F.
	ELSE
		COPY TO ("c:\tempfox\moret_noconfirm"+cState)
		LOCATE
		lUnconfirmed = .T.
		IF lTesting
			BROWSE TIMEOUT 20
		ENDIF

		WAIT WINDOW "Errors found...creating Excel error file and mailing" TIMEOUT 3
		cExcelFile = ("F:\3pl\DATA\missconfirms_"+cState+".xls")
		COPY TO [&cExcelFile] TYPE XL5
	ENDIF

	IF lUnconfirmed
		tsubject= "Unconfirmed Moret 945s: Office "+cOfficeUsed
		tattach = cExcelFile
		tmessage = "The attached Excel file contains PTs for which we have not received Moret 997s."
	ELSE
		tsubject= "No unconfirmed Moret 945s: Office "+cOfficeUsed+" ("+cState+")"
		tattach = ""
		tmessage = "We have received Moret 997s for all sent 945s."
	ENDIF
	tFrom ="Toll EDI Operations <toll-edi-ops@tollgroup.com>"
	SELECT 0
	USE F:\3pl\DATA\mailmaster ALIAS mm SHARED
	IF lTesting
		LOCATE FOR edi_type = "MISC" AND taskname = "JOETEST"
	ELSE
		LOCATE FOR edi_type = "MISC" AND taskname = "MORET945CFM"
	ENDIF
	tsendto = IIF(mm.use_alt,sendtoalt,sendto)
	tcc = IIF(mm.use_alt,ccalt,cc)
	USE IN mm

	IF lEmail
		DO FORM m:\dev\frm\dartmail2 WITH tsendto,tFrom,tsubject,tcc,tattach,tmessage,"A"
	ENDIF
	IF FILE(cExcelFile)
		DELETE FILE [&cExcelFile]
	ENDIF
	USE IN tempconf
ENDPROC
