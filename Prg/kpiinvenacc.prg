**Inventory Accuracy KPI

**Difference between the inventory and physical inventory at the time of last ;
**  physical divided by avg monthly inventory for the last year
**  - Based on the adj.dbf counts (records marked with "Physical Adjustment" comment) ;
**	  divided by the avg inventory in eomInven for the last 12 months
**
Wait window "Gathering Inventory Accuracy data..." nowait noclear

xsqlexec("select zdate from info where accountid="+cacctnum+" and mod='"+xoffice+"' and message='Physical inventory updated' order by zdate descending","csrdetail",,"wh")

dAdjDt = zdate	 &&completion date of last physical - changed to look into info instead of adj  dy 12/16/15

If Empty(dAdjDt)
	strMsg = "There has been no recent physical done for this account therefore no pertinent data exists."
	=MessageBox(strMsg, 48, "Inventory Accuracy")
Else
	Wait window "Creating Inventory Accuracy Spreadsheet..." nowait noclear

	xquery="select adjdt,style,color,id,pack,totqty,iif(units=1,'Yes','No ') as units,whseLoc,comment from adj "+;
		"where accountid="+cAcctNum+" and mod='"+xoffice+"' and adjDt={"+transform(dAdjDt)+"} and comment='PHYSICAL ADJUSTMENT'"
	xsqlexec(xquery,"csrdetail",,"wh")

	**because we need to limit the avg monthly inventory to either units or cartons in calculation below, if the 
	**	physical contains both units and cartons, the calculations won't work. After speaking with Darren, a 
	**	physical should only be units OR cartons, not both. Depening on acct type - prepack vs pnp - mvw 01/20/14
	xunits=iif(units="Y",.t.,.f.)

	xerror=.f.
	locate for units="Y"
	if found()
		locate for units="N"
		if found()
			strMsg = "There are both unit and acrton records for this physical inventory... This report cannot "+;
				"be run. Please contact Mike Winter x178."
			=MessageBox(strMsg, 48, "Inventory Accuracy")
			use in csrdetail
			xerror=.t.
		endif
	endif

	if !xerror
		Sum totQty to nTotAdj

		STORE tempfox+SUBSTR(SYS(2015), 3, 10)+".xls" TO cFile
		**create spreadsheet this way b/c cannot create new spreadsheet in excel with a specified file name
		Store Iif(lDetail, "", "For .f.") to cFilter
		Copy To &cFile &cFilter type xl5

		oWorkbook = oExcel.Workbooks.Open(cFile)

		If lDetail
			oworkbook.worksheets[1].name = "Detail"
			oworkbook.worksheets[1].Range("A1:I"+Alltrim(Str(Reccount()+1))).Columns.autoFit()
			oworkbook.worksheets[1].Range("A1:I1").HorizontalAlignment = -4108
			oworkbook.worksheets[1].Range("A1:I1").Font.Bold = .t.

			oWorkSheet = oworkbook.worksheets.add(oworkbook.worksheets[1])
			oWorkSheet.activate()
		Else
			oworkbook.worksheets[1].Range("A1:I1").value = ""
		EndIf

		oworkbook.worksheets[1].name = "Summary"
		oworkbook.worksheets[1].range("A1:L1").merge()
		oworkbook.worksheets[1].range("A1") = "Key Performance Indicators (KPIs)"
		oworkbook.worksheets[1].Range("A1:L1").Font.Bold = .t.
		oworkbook.worksheets[1].Range("A1:L1").Font.Size = 14
		oworkbook.worksheets[1].Range("A1:L1").HorizontalAlignment = -4108
		oworkbook.worksheets[1].range("A2:L2").merge()
		oworkbook.worksheets[1].range("A2") = "Inventory Accuracy: "+cAcctName+;
			" ("+Iif(cOffice="C","SP",Iif(cOffice="L","ML",Iif(cOffice="R","RI",Iif(cOffice="M", "FL", "NJ"))))+")"
		oworkbook.worksheets[1].Range("A2:L2").Font.Bold = .t.
		oworkbook.worksheets[1].Range("A2:L2").Font.Size = 12
		oworkbook.worksheets[1].Range("A2:L2").HorizontalAlignment = -4108

		**need to create a filter for eomInven.zMonth which is a 4 digit char "mmyy" of month ;
		**  end inventory for 12 months prior to physical
		cFilter = ""

		For i = 1 to 12
			xmon=padl(transform(month(gomonth(dAdjDt,-i))),2,"0")+right(transform(year(gomonth(dAdjDt,-i))),2)
			cFilter = cFilter+"'"+xmon+"'"+Iif(i=12, "", ", ")
		EndFor
		*****

		xsqlexec("select * from eomInven where accountid="+transform(cAcctNum)+" and inlist(zMonth,"+cFilter+") and half=.f. and units="+transform(xunits)+" and mod='"+xoffice+"'","eominven",,"wh")
set step on 
		Select sum(totQty) as totQty from eomInven into cursor csrDetail

		nAvgQty = totQty/12

		oworkbook.worksheets[1].range("A5") = "Total Discrepancy On Physical ("+Dtoc(dAdjDt)+"): "+Alltrim(Str(nTotAdj))
		oworkbook.worksheets[1].range("A6") = "Average Monthly Inventory for the Year: "+Alltrim(Str(nAvgQty))
		oworkbook.worksheets[1].range("A8") = "Inventory Inaccuracy: "+Alltrim(Str((nTotAdj/nAvgQty)*100,6,2))+"%"
		oworkbook.worksheets[1].range("A8").Font.Bold = .t.

		oWorkbook.Save()
	endif
EndIf
