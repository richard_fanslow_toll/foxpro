* return a labor rate based on passed in WO.CRDATE
RETURN

*!*	LPARAMETERS tdCRDATE
*!*	LOCAL lnLaborRate
*!*	DO CASE
*!*		CASE tdCRDATE < {^2004-01-01}
*!*			lnLaborRate = 37.5
*!*		OTHERWISE
*!*			lnLaborRate = 60.0
*!*	ENDCASE
*!*	RETURN lnLaborRate
