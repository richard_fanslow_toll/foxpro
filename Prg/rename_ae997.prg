USE f:\edirouting\ftpsetup
replace chkbusy WITH .t. FOR transfer = 'AEO-850-997'

SELECT 0
CD f:\ftpusers\AmericanEagle\997Out
len1 = ADIR(ary1,'*.dat')
IF len1 = 1
cFilename = ary1[1]
RENAME &cFilename TO LOWER(cFilename)
ENDIF

SELECT ftpsetup
replace chkbusy WITH .f. FOR transfer = 'AEO-850-997'
return
