*!*	Work in progress...please don't change anything. Joe, 07.12.16

PARAMETERS cOffice,nAcctNum

PUBLIC lTesting,lTestimport,lDoSQL,lDoInwolog,lDoImport,cMod,gOffice,gMasterOffice,lAGMail,cWhseMod,m.acctname

CLOSE DATA ALL
utilsetup("AGEGROUP943")

lTesting = .F.
lTestimport = lTesting
lDoImport=.T.
lSQLInwolog = .F.
lBrowfiles = lTesting
lBrowfiles = .F.
lOverridebusy = lTesting
*lOverridebusy = .T.

lAGMail = .T.
lTestmail = .F.

DO m:\dev\prg\_setvars WITH lTesting
SET STATUS BAR ON

USE F:\3pl\DATA\agegroup943csvtemplate.DBF IN 0 ALIAS ag943
SELECT * FROM ag943 WHERE .F. INTO CURSOR intake READWRITE

USE IN ag943

*AG nAcctNum = 1285, EB nAcctNum = 6769
IF VARTYPE(cOffice) # "C"
	cOffice = "Y"
	nAcctNum = 1285
ENDIF
_screen.Caption="AGE GROUP 943 PROCESS FOR OFFICE "+cOffice

nacctnum=val(transform(nacctnum))	&& dy 3/31/18

gMasterOffice = IIF(cOffice="I","N",cOffice)

DO CASE
	CASE cOffice = "N" && AG/EB-NJ
		cMod = "I"
	CASE cOffice = "C"  && EB-SP
		cMod = "1"
	OTHERWISE  &&AG-C2
		cMod = "Y"
ENDCASE
gOffice = cMod
cWhseMod = "wh"+LOWER(cMod)

m.accountid = nAcctNum

IF !lTesting
	SELECT 0
	USE F:\edirouting\FTPSETUP SHARED
	LOCATE FOR FTPSETUP.TRANSFER = "943-AGEGROUP"
	IF FOUND()
		IF chkbusy AND !lOverridebusy
			WAIT WINDOW "943 Process is busy...exiting" TIMEOUT 2
			NormalExit = .T.
			THROW
		ENDIF
	ENDIF
	REPLACE chkbusy WITH .T.,trig_time WITH DATETIME() FOR FTPSETUP.TRANSFER = "943-AGEGROUP"
	USE IN FTPSETUP
ENDIF

SELECT 0
USE F:\3pl\DATA\mailmaster ALIAS mm

LOCATE FOR mm.edi_type = "943" AND mm.accountid = val(transform(nAcctNum)) AND mm.Office = cOffice


IF FOUND()
	STORE TRIM(mm.acctname) TO cCustName
	STORE TRIM(mm.basepath) TO lcPath
	STORE TRIM(mm.archpath) TO lcArchivePath
	STORE TRIM(mm.sendto) TO tsendto
	STORE TRIM(mm.cc)   TO tcc
	IF lTesting OR lTestmail
		LOCATE FOR mm.edi_type = "MISC" AND mm.taskname = "JOETEST"
	ELSE
		LOCATE FOR mm.edi_type = "MISC" AND mm.taskname = "GENERAL"
	ENDIF
	STORE TRIM(mm.sendto) TO tsendtotest
	STORE TRIM(mm.cc)   TO tcctest
	STORE TRIM(mm.sendto) TO tsendtoerr
	STORE TRIM(mm.cc)   TO tccerr
	USE IN mm
ELSE
	WAIT WINDOW AT 10,10  "No parameters set for this acct# "+ALLTRIM(STR(nAcctNum))+"  ---> Office "+cOffice TIMEOUT 2
	USE IN mm
	RELEASE ALL
	RETURN
ENDIF


IF lTesting
	cOffice = "P"
	cMod = "P"
	gOffice = cOffice
	gMasterOffice = cOffice
	cUseFolder = "F:\whp\whdata\"
ENDIF

IF lTestimport
	USE (cUseFolder+"inwolog") IN 0 ALIAS inwolog
	useca("pl","wh")
	SELECT * FROM inwolog WHERE .F. INTO CURSOR xinwolog READWRITE
	xsqlexec("select * from pl where .f.","xpl",,"wh")
ELSE
	useca("pl","wh")
	xsqlexec("select * from pl where .f.","xpl",,"wh")

	useca("inwolog","wh")
	xsqlexec("select * from inwolog where .f.","xinwolog",,"wh")
ENDIF
SELECT xinwolog
*SCATTER MEMVAR memo blank

lcPath = IIF(lTesting,STRTRAN(lcPath,"943in","943intest"),lcPath)

CD [&lcPath]

len1 = ADIR(ary1,"*.csv")
STORE 0 TO nIWID,nPLID,nWO_Num

FOR k = 1 TO len1
	cFilename = ALLTRIM(ary1[k,1])
	InFile = (lcPath+cFilename)
	ArchiveFile = (lcArchivePath+cFilename)

	SELECT intake
	APPEND FROM &cFilename TYPE DELIM
	LOCATE

	SELECT DISTINCT CONTAINER,acct_ref FROM intake INTO CURSOR ctr1 ORDER BY 1,2
	SELECT ctr1
	LOCATE
	cOrigStr = PADR("CONTAINER",20)+"ACCT.REF."
	nPO = 0
	SCAN
		cContainerIn = ALLTRIM(ctr1.CONTAINER)
		cAcct_refIn = ALLTRIM(ctr1.acct_ref)
		lDoInwolog = .T.
		SELECT intake
		SUM(intake.totqty) TO m.plinqty FOR intake.CONTAINER = ctr1.CONTAINER AND intake.acct_ref = ctr1.acct_ref
*		SUM(intake.totqty) TO m.plopenqty FOR intake.CONTAINER = ctr1.CONTAINER AND intake.acct_ref = ctr1.acct_ref
		LOCATE

		SCAN FOR intake.CONTAINER = ctr1.CONTAINER AND intake.acct_ref = ctr1.acct_ref
			cContainer = ALLTRIM(intake.CONTAINER)
			cAcct_ref = ALLTRIM(intake.acct_ref)
			IF !lTesting AND lDoImport
				lcQuery = [select * from inwolog where container = ']+cContainerIn+[' and acct_ref = ']+cAcct_refIn+[']
				xsqlexec(lcQuery,"p1",,"wh")
				IF RECCOUNT()>0
					WAIT WINDOW "Container/Acct Ref combo already exists in INWOLOG" NOWAIT
					SELECT intake
					LOCATE
					DELETE FOR CONTAINER = cContainer AND acct_ref = cAcct_ref
					EXIT
				ENDIF
			ENDIF

			IF cContainer$cOrigStr AND cAcct_ref$cOrigStr
*
			ELSE
				cOrigStr = cOrigStr+CHR(13)+PADR(cContainer,20)+cAcct_ref
			ENDIF


			SELECT intake
			SCATTER MEMVAR
			m.ADDBY = "TOLLPROC"
			m.ADDDT = DATETIME()
			m.ADDPROC = "AG943"
			m.Office = gMasterOffice
			m.mod = cMod

			IF lTesting

			ENDIF
			m.wo_date = DATE()
			m.comments = "VENDOR*"+m.vendor
			m.comments = m.comments+CHR(13)+"PONUM*"+m.po_num
			m.comments = m.comments+CHR(13)+"WHSENUM*"+ALLTRIM(STR(m.whsenum))
			IF lDoInwolog
				m.acctname = IIF(val(transform(nAcctNum)) = 6769,"ESSENTIAL BRANDS","AGE GROUP INC.")
				nIWID = nIWID+1
				m.inwologid = nIWID
				nWO_Num = nWO_Num + 1
				m.wo_num = nWO_Num
				m.confirmdt = {  /  /    }
				INSERT INTO xinwolog FROM MEMVAR
				lDoInwolog = .F.
			ENDIF


*!*			SECTIONS COMMENTED BELOW ARE TO PROCESS CARSON II AS NJ-TYPE PNP, Joe, 06.23.2017
			m.ADDBY = "TOLLPROC"
			m.ADDDT = DATETIME()
			m.ADDPROC = "AG943"
			m.Office = gMasterOffice
			m.mod = cOffice
			m.pack = ALLTRIM(STR(m.unitsqty/m.totqty))
			nPLID = nPLID + 1
			m.plid = nPLID
			m.date_rcvd = DATE()
			m.style = ALLTRIM(m.style)+ALLTRIM(m.cus_os)
			m.echo = "SEASON*"+m.season
			m.echo = m.echo+CHR(13)+"DESC*"+ALLTRIM(m.desc)
			m.echo = m.echo+CHR(13)+"CUS_OS*"+ALLTRIM(m.cus_os)
			m.echo = m.echo+CHR(13)+"PACKDESC*"+ALLTRIM(m.packdesc)
			m.echo = m.echo+CHR(13)+"PACKTYPE*"+ALLTRIM(m.packtype)
			m.echo = m.echo+CHR(13)+"UPC*"+ALLTRIM(m.upc)
			m.acctname = "AGE GROUP INC."
*!*				IF cOffice # "Y"
			nPO = nPO+1
			m.po = TRANSFORM(nPO)
			m.units = .F.
*!*				ENDIF
			INSERT INTO xpl FROM MEMVAR

*!*				IF cOffice # "Y"
			nPLID = nPLID + 1
			m.plid = nPLID
			m.units = .T.
			m.totqty = INT(VAL(m.pack))*m.totqty
			m.pack = '1'
			INSERT INTO xpl FROM MEMVAR
*!*				ENDIF

		ENDSCAN
	ENDSCAN
************************************************************************************************8
Select xpl
Sum(totqty) to lnUnitsIN for xpl.units = .t.
Sum(totqty) to lnCtnsIN for xpl.units = .t.
Select xinwolog 
replace plunitsinqty With lnUnitsIN In xinwolog
replace plinqty      With lnCtnsIN  In xinwolog
************************************************************************************************8
	IF lBrowfiles
		SELECT xinwolog
		LOCATE
		IF EOF()
			lDoImport = .F.
		ELSE
			BROWSE
		ENDIF
		SELECT xpl
		LOCATE
		BROWSE
	ENDIF

	IF lDoImport
		IF lTesting
			SET STEP ON
		ENDIF
		SELECT xinwolog
		LOCATE
		nUpload = 0
		SCAN
			SCATTER MEMVAR MEMO
			WAIT WINDOW "Processing Container/AcctRef "+ALLTRIM(m.container)+"/"+ALLTRIM(m.acct_ref) NOWAIT
*SELECT * from xinwolog INTO CURSOR xctr READWRITE
			xsqlexec("select * from inwolog where container = '"+ALLTRIM(m.container)+"' and acct_ref = '"+ALLTRIM(m.acct_ref)+"'","xctr",,"wh")
			SELECT xctr
			IF RECCOUNT()>0
				WAIT WINDOW "Container record found in Inwolog...looping"  TIMEOUT 2
				USE IN xctr
				LOOP
			ENDIF

			nUpload =  nUpload+1
			SELECT xinwolog
			m.Office = gMasterOffice
			m.mod = cMod
			nWO_Num = dygenpk("wonum",cWhseMod)
			m.wo_num = nWO_Num
			m.accountid = nAcctNum
			m.acctname = "AGE GROUP INC."
			m.ADDBY = "TOLLPROC"
			m.ADDDT = DATETIME()
			m.ADDPROC = "AG943"

			insertinto("inwolog","wh",.T.)

			SELECT xpl
			SCAN FOR xpl.inwologid = xinwolog.inwologid
				SCATTER MEMVAR MEMO
				m.inwologid = inwolog.inwologid
				m.ADDBY = "TOLLPROC"
				m.ADDDT = DATETIME()
				m.ADDPROC = "AG943"
				m.accountid = nAcctNum
				m.wo_num = nWO_Num
				m.Office = gMasterOffice
				m.mod = cMod
				insertinto("pl","wh",.T.)
			ENDSCAN
		ENDSCAN

	ENDIF
************************************************************************************************
*SET STEP ON
	COPY FILE [&Infile] TO [&ArchiveFile]
	IF FILE(ArchiveFile)
		DELETE FILE [&InFile]
	ENDIF

	IF lAGMail
		IF nUpload > 0
			goodmail()
			tu("inwolog")
			tu("pl")
		ELSE
			WAIT WINDOW "No new inbound records uploaded" TIMEOUT 3
		ENDIF
	ENDIF
ENDFOR
************************************************************************************************8
WAIT WINDOW "Entire 943 Upload process complete" TIMEOUT 2
SET STATUS BAR ON
CLOSE DATA ALL

IF !lTesting
	SELECT 0
	USE F:\edirouting\FTPSETUP SHARED
	REPLACE chkbusy WITH .F. FOR FTPSETUP.TRANSFER = "943-AGEGROUP"
	USE IN FTPSETUP
ENDIF

********************
PROCEDURE goodmail()
********************
	ASSERT .F. MESSAGE "At email"
	tfrom = "TGF EDI Operations <toll-edi-ops@tollgroup.com>"
	cMailLoc = ICASE(INLIST(cOffice,"L","Z"),"Mira Loma",INLIST(cOffice,"K","S"),"Louisville",INLIST(cOffice,"Y"),"Carson 2",cOffice = "M","Miami",INLIST(cOffice,"I","N","J"),"New Jersey","San Pedro")
	tsubject= "TGF "+cMailLoc+" "+cCustName+" 943 PL Upload: " +TTOC(DATETIME())
	tattach = ""
	tmessage = "Uploaded the following containers/acct.refs for "+cCustName+CHR(13)+"From File: "+cFilename+CHR(13)
	tmessage = tmessage+CHR(13)+cOrigStr
	IF lTesting OR lTestimport
		tmessage = tmessage+CHR(13)+"*TEST DATA* - LOADED INTO F:\WHP TABLES"
	ENDIF
	DO FORM m:\dev\frm\dartmail2 WITH tsendto,tfrom,tsubject,tcc,tattach,tmessage,"A"
ENDPROC
