xacctname=acctname(xaccountid)

use f:\wo\wodata\manifest in 0
use f:\wo\wodata\wolog alias mainlog in 0

xsqlexec("select * from pudlocs",,,"wo")
index on accountid tag accountid
set order to

xstartdt=ctod(transform(nmonth)+"/1/"+transform(nyear))
xenddt=gomonth(xstartdt,1)-1

xquery="select wo_num, origin, destination from fxwolog " + ;
	"where accountid="+transform(xaccountid)+" and between(wo_date,{"+dtoc(xstartdt)+"},{"+dtoc(xenddt)+"})"
	
xsqlexec(xquery,"fxlog",,"fx")

select mfst.*, iif(!isnull(f.origin), f.origin, iif(empty(m.fromloc), ;
	padr("OCEAN",30), fromloc.acct_name)) as "Orig", ;
	iif(!isnull(f.destination), f.destination, iif(empty(m.toloc), ;
	padr(alltrim(upper(o.city))+", "+o.state, 30), toloc.acct_name)) as "Destination", ;
	sum(iif(mfst.qty_type = "GOH", mfst.quantity, 0)) as "TotGoh", ;
	sum(iif(mfst.qty_type # "GOH", mfst.quantity, 0)) as "TotCtn", ;
	sum(mfst.weight) as totwt, sum(mfst.cbm) as totcbm ;
	from manifest mfst ;
	left join fxlog f         on f.wo_num = mfst.wo_num ;
	left join mainlog m       on m.wo_num = mfst.wo_num ;
	left join pudlocs toloc   on str(toloc.accountid,4)+str(toloc.dash,4) = str(xaccountid,4)+str(m.toloc,4) ;
	left join pudlocs fromloc on str(fromloc.accountid,4)+str(fromloc.dash,4) = str(xaccountid,4)+str(m.fromloc,4) ;
	left join offices o       on o.locale = m.office ;
	where mfst.accountid = xaccountid and between(mfst.wo_date,xstartdt,xenddt) ;
	group by mfst.wo_num ;
	into cursor csrmanifest

insert into pdf_file (unique_id) values (reports.unique_id)

if eof() &&need something to distinguish from "poller down"
	select pdf_file
	replace memo with "NO DATA"
else
	public oexcel
	oexcel = createobject("Excel.Application")
	oexcel.displayalerts = .f.

	store tempfox+alltrim(str(reports.unique_id)) to cfile
	copy file f:\webrpt\trailersum.xls to &cfile

	oworkbook = oexcel.workbooks.open(cfile)
	oexcel.activesheet.range("A2").value = "Monthly Trailer Shipping For: "+padl(ltrim(str(nmonth,2,0)),2,"0")+"/"+str(nyear,4,0)
	oexcel.activesheet.range("A3").value = "Account: "+alltrim(xacctname)

	lshaded = .t.
	nrownum = 7
	select csrmanifest
	scan
		crownum = alltrim(str(nrownum, 5, 0))

		with oexcel.activesheet
			.range("A"+crownum).value = wo_num
			.range("B"+crownum).value = orig
			.range("C"+crownum).value = destination
			.range("D"+crownum).value = trailer
			.range("E"+crownum).value = dtoc(wo_date)
			.range("F"+crownum).value = dtoc(delivered)
			.range("G"+crownum).value = totgoh
			.range("H"+crownum).value = totctn
			.range("I"+crownum).value = totwt
			.range("J"+crownum).value = totcbm
		endwith

		if lshaded
			oexcel.activesheet.range("A"+crownum+":"+"J"+crownum).interior.color = rgb(179,219,234)
		endif

		lshaded = !lshaded
		nrownum = nrownum+1
	endscan

	oworkbook.save()
	oworkbook.close()
	oexcel.quit()
	release oworkbook, oexcel

	select pdf_file
	**in order to avoid "VFPOLEDB" "provider ran out of memory" errors for large files (>2mb) no longer move file 
	**  thru linked server, just copy file directly to \\iis2\apps\CargoTrackingWeb\reports\ - mvw 03/08/13
	*append memo memo from &cfile overwrite
	copy file "&cfile" to \\iis2\apps\CargoTrackingWeb\reports\*.*
	replace memo with "FILE COPIED TO REPORT FOLDER" in pdf_file
endif

use in manifest
use in csrmanifest
use in fxlog
use in mainlog
use in pudlocs

set database to fx
close databases
