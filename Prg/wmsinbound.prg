PARAMETERS cOffice,Acctnum
* call this like this: do m:\DEV\prg\wmsinbound with "C",5453

*runack("WMSINBOUND")

IF VARTYPE(Acctnum) = "C"
	Acctnum = INT(VAL(Acctnum))
ENDIF

PUBLIC goffice,tsendto,tcc,lcAccountname,lCartonAcct,lDoGeneric,lCreateErr,PNAME,nAcctNum,lHoldFile,lDoMix,nLineStart,cMod
PUBLIC m.style,m.color,m.id,m.size,m.pack,cTransfer,lTest,lAllPicks,lcWHPath,lnormalexit,lcErrorMessage,newAcctNum,lDoPL,cWhseMod
PUBLIC cAcctname,lAcctName,xacctnum,lPrepack,nPrepacks,nPickpacks,tFrom,lProcError,thisfile,lcpath,lDoSLError,inrec
PUBLIC cSLError,lPik,lPre,cMessage,cFilename,lTestmail,lModErr,xfile,cMyFile,lXLSX,archivefile,lDelDBF,plqty1,plqty2,lcErrorMessage

STORE "" TO m.style,m.color,m.id,m.size,m.pack,lcBrokerref,cMessage,cFilename,xfile

lDoSql = .T.

CLOSE DATABASES ALL
SET PATH TO
SET PATH TO "M:\DEV\PRG\"
DO m:\dev\prg\_setvars WITH .T.

SET STATUS BAR ON
ON ESCAPE CANCEL

DO m:\dev\prg\lookups

lcErrorMessage=""
lTest = .F.  && This directs output to F:\WHP\WHDATA test files (default = .f.)
lTestmail = .F. &&lTest && Causes mail to go to default test recipient(s)
lHoldFile = .F. && Causes error CATCH message; on completion with no errors, changed to .t. (default = .f.)
lBrowse = lHoldFile  && If this is set with lHoldFile, browsing will occur at various points (default = .f.)
lOverrideBusy = IIF(lHoldFile OR lTest,.T.,.F.) && If this is set, CHKBUSY flag in FTPSETUP will be ignored (default = .f.)

lOverrideBusy = .T.

lAllPicks = .F.  && If this is set, all inbounding will be done as units...should not be used (default = .f.)
lDoSLError = .F.  && This flag will be set if there is missing info in the Courtaulds inbound sheet
lXLSX = .F.
lDelDBF = .T.

TRY
	useca("inrcv","wh")
	xfile = " "
	IF !lTest AND !lHoldFile
		ON ERROR THROW
	ENDIF

	_SCREEN.CAPTION = "WMS Inbound - Work Order Builder"

*	_SCREEN.WINDOWSTATE=IIF(lHoldFile OR lTest,2,1)
	IF TYPE("acctnum") = "C"
		Acctnum = VAL(Acctnum)
	ENDIF

	STORE Acctnum TO nAcctNum

	tFrom = "TGF WMS Inbound EDI System Operations <inbound-ops@fmiint.com>"
	lDoGeneric = .F.
	lCreateErr = .F.
	lMainMail = .T.
	tsendto = ""
	tcc = ""
	inrec = 0
	cSLError = ""
	lCartonAcct = .F.
	SELECT 0
	USE F:\edirouting\ftpsetup
*	ASSERT .f. MESSAGE "In FTPSETUP list"
	m.office = cOffice
	DO CASE
	CASE cOffice = "C" AND Acctnum = 1285
		cTransfer = "PL-AGEGROUP-CA"
		cMod = "5"
	CASE INLIST(cOffice,"I","N") AND Acctnum = 1285
		cTransfer = "PL-AGEGROUP-NJ"
		cMod = "I"
	CASE INLIST(Acctnum,4610,4694)
		cTransfer = "PL-NANJING"
		cMod = cOffice
	CASE cOffice = "M" AND Acctnum = 4677
		cTransfer = "PL-COURTAULDS"
		cMod = "M"
	CASE cOffice = "C" AND Acctnum = 5836
		cTransfer = "PL-TKO-CA"
		cMod = "2"
	CASE cOffice = "C" AND Acctnum = 5865
		cTransfer = "PL-TKO-CAPP"
		cMod = "2"
	CASE INLIST(cOffice,"I","N") AND Acctnum = 5836
		cTransfer = "PL-TKO-NJ"
		cMod = "I"
	CASE INLIST(cOffice,"I","N") AND Acctnum = 5865
		cTransfer = "PL-TKO-NJPP"
		cMod = "I"
	CASE cOffice = "M" AND Acctnum = 5836
		cTransfer = "PL-TKO-FL"
		cMod = "M"
	CASE cOffice = "M" AND Acctnum = 5865
		cTransfer = "PL-TKO-FLPP"
		cMod = "M"
	CASE INLIST(cOffice,"I","N") AND Acctnum = 6209
		cTransfer = "PL-YELLSTEEL-NJ"
		cMod = "I"
	CASE cOffice = "I" AND Acctnum = 6237
		cTransfer = "PL-IVORY-NJ"
		cMod = "I"
	CASE cOffice = "J" AND INLIST(Acctnum,&gmjacctlist)
		cTransfer = "PL-MARCJACOBS"
		cMod = "J"
	OTHERWISE
		cTransfer = "XXX"  && For Modern Shoe, etc.
	ENDCASE

	goffice = cMod
	gMasteroffice = cOffice
	lDoPL = .T.

	cWhseMod = LOWER("wh"+cMod)

	ASSERT .F. MESSAGE "At ftpsetup stage"
	LOCATE FOR UPPER(ftpsetup.transfer) = UPPER(cTransfer)
	IF FOUND()
		IF !lHoldFile AND !lBrowse AND !lOverrideBusy
			IF ftpsetup.CHKBUSY=.T.
				SET STEP ON
				WAIT WINDOW AT 10,10  "This transfer is busy...will try again later."+CHR(13)+cTransfer TIMEOUT 1
				lnormalexit = .T.
				THROW
			ELSE
				REPLACE ftpsetup.CHKBUSY WITH .T. IN ftpsetup NEXT 1
			ENDIF
		ENDIF
	ENDIF

	IF lTest
		WAIT WINDOW "This is a test inbound upload into WHP tables" TIMEOUT 2
	ENDIF

	IF USED('mm')
		USE IN mm
	ENDIF
	SELECT 0
	USE F:\3pl\DATA\mailmaster ALIAS mm
*	ASSERT .F. MESSAGE "At MAIL MASTER variable population"
*	LOCATE FOR (mm.accountid = Acctnum) AND (mm.office = "C") AND edi_type = "PL"
	LOCATE FOR (mm.accountid = Acctnum) AND (mm.office = cOffice) AND edi_type = "PL"

	IF FOUND()

		STORE TRIM(mm.acctname) TO lcAccountname
		STORE TRIM(mm.basepath) TO lcpath
		STORE TRIM(mm.archpath) TO lcArchivePath
		tsendto = ALLTRIM(IIF(mm.use_alt,mm.sendtoalt,mm.sendto))
		tcc = ALLTRIM(IIF(mm.use_alt,mm.ccalt,mm.cc))
*		WAIT WINDOW "SendTo: "+tsendto+CHR(13)+"CC: "+tcc TIMEOUT 2
		STORE TRIM(mm.fmask)    TO filemask
		STORE TRIM(mm.progname) TO PNAME
		STORE TRIM(mm.scaption) TO _SCREEN.CAPTION
		STORE mm.CtnAcct  	TO lCartonAcct
*		STORE mm.testflag 	TO lTest
		STORE mm.dogeneric TO lDoGeneric
		LOCATE FOR edi_type = "MISC" AND taskname = "GENERAL"
		tsendtoerr = ALLTRIM(IIF(mm.use_alt,mm.sendtoalt,mm.sendto))
		tccerr = ALLTRIM(IIF(mm.use_alt,mm.ccalt,mm.cc))
		tsendtotest = ALLTRIM(IIF(mm.use_alt,mm.sendtoalt,mm.sendto))
		tcctest = ALLTRIM(IIF(mm.use_alt,mm.ccalt,mm.cc))
		USE IN mm
	ELSE
		lnormalexit=.T.
		WAIT WINDOW AT 10,10  "No parameters set for this acct# "+ALLTRIM(STR(Acctnum))+"  ---> Office "+cOffice TIMEOUT 2
		THROW
	ENDIF
	IF INLIST(nAcctNum,4677,6035,6097)
		DELETE FILE lcpath+"*.csv"
	ENDIF
	IF INLIST(nAcctNum,1285,6097,6209,6137)  && Age Group, JQueen
		IF lDelDBF
			lDelDBF = .F.
			DELETE FILE (lcpath+"*.dbf")
		ENDIF
	ENDIF

	IF nAcctNum = 4677 AND lTest
		lcpath = lcpath+"test\"
		WAIT WINDOW "PATH is "+lcpath TIMEOUT 10
	ENDIF
	CD &lcpath

	IF lTest OR lHoldFile
		tsendto = tsendtotest
		tcc = tcctest
	ENDIF

	IF lDoGeneric AND EMPTY(PNAME)
		PNAME = "GenericInbound"
	ENDIF


	WAIT WINDOW AT 10,10  "Now setting up a WMS Inbound for "+lcAccountname+"  ---> Office "+cOffice TIMEOUT 1

	SET tablevalidate TO 0
	SET TALK OFF
	SET EngineBehavior 70
	SET ESCAPE ON
	SET SAFETY OFF
	SET MULTILOCKS ON
	SET DELETED ON
	SET EXCLUSIVE OFF

	SELECT 0

	xsqlexec("select * from account where inactive=0","account",,"qq")
	INDEX ON accountid TAG accountid

	SELECT account

	IF SEEK(Acctnum,"account","accountid")
		cAcctname= account.acctname
	ELSE
		lnormalexit=.T.
		WAIT WINDOW AT 10,10 "No account name on file.........." TIMEOUT 2
		CLOSE DATABASES ALL
		THROW
	ENDIF

	xreturn = "X"
	DO m:\dev\prg\wf_ALT WITH cOffice,Acctnum
	lcWHPath = UPPER(xreturn)
	IF xreturn = "X"
		WAIT WINDOW "No such account for this office" TIMEOUT 2
		THROW
	ENDIF

	IF INLIST(Acctnum,4610,4694)
*lcWHPath = "f:\why\whdata\"
	ENDIF

	SET PATH TO '&lcWHPath' ADDITIVE
	lnNum = ADIR(tarray,filemask)

	IF lnNum = 0
		lnormalexit=.T.
		WAIT WINDOW AT 10,10 "    No Inbound files to process for "+lcAccountname+"   office--> "+cOffice  TIMEOUT 2
		THROW
	ENDIF

	IF lTest
		lcWHPath = "f:\whp\whdata\"
		SELECT 0
		USE (lcWHPath+"inwolog")
		DELETE FOR accountid = Acctnum
	ENDIF

	FOR thisfile = 1  TO lnNum
		IF USED('wolog')
			USE IN wolog
		ENDIF
		IF USED('inwolog')
			USE IN inwolog
		ENDIF
		IF USED('pl')
			USE IN pl
		ENDIF

		USE F:\wo\wodata\wolog IN 0 ALIAS wolog
		useca("inwolog","wh")

		IF lDoPL
			useca("pl","wh")
		ELSE
			USE &lcWHPath.pl IN 0 ALIAS pl
		ENDIF

		IF USED("generatepk")	&& dy 11/18/09
			USE IN generatepk
		ENDIF

		IF USED('upcmast')
			USE IN upcmast
		ENDIF
*		USE F:\wh\upcmast IN 0 ALIAS upcmast SHARED

		CREATE CURSOR wos ( ;
			filename   c(100),;
			wo_num     N(15),;
			CONTAINER  c(18), ;
			sku_units  N(6),;
			cartons    N(6),;
			quantity   N(10), ;
			sorts      N(6),;
			season     c(25),;
			computer   c(1))

		CREATE CURSOR oldwos ( ;
			filename   c(100),;
			wo_num     N(15),;
			wo_date    d,;
			CONTAINER  c(18), ;
			sku_units  N(6),;
			cartons    N(6),;
			quantity   N(10), ;
			sorts      N(6),;
			computer   c(1))

		IF INLIST(ALLTRIM(PNAME),"NanjingInbound","GenericInbound")
*			ASSERT .F. MESSAGE "In Read-only fix and Excel>CSV section"
			llContinue = .T.
			TRY
*				ASSERT .F. MESSAGE "DEBUG HERE!"
				xfile = lcpath+ALLTRIM(tarray[thisfile,1])
				IF INLIST(UPPER(JUSTEXT(xfile)),"TMP","DBF")
					LOOP
				ENDIF

				attrfile = '"'+lcpath+ALLTRIM(tarray[thisfile,1])+'"'
				cFilename = JUSTFNAME(xfile)
				!ATTRIB -R &attrfile  && Removes read-only flag from file to allow deletion
				WAIT WINDOW "Filename = "+xfile TIMEOUT 1
				IF INLIST(nAcctNum,6035,4610,4694)
					CREATE CURSOR tempmain (a c(50), ;
						b c(30), ;
						c c(30), ;
						d c(50), ;
						e c(30), ;
						F c(30), ;
						g c(30), ;
						h c(30), ;
						i c(30), ;
						j c(30), ;
						k c(30))
					xfilecsv = JUSTSTEM(JUSTFNAME(xfile))+".csv"
					DO m:\dev\prg\excel_to_csv WITH cFilename
					SELECT tempmain
					APPEND FROM [&xfilecsv] TYPE CSV
					LOCATE
					IF nAcctNum = 4610 AND DATETIME()<DATETIME(2010,08,16,16,30,00)
						BROWSE TIMEOUT 10
					ENDIF
				ENDIF
			CATCH TO oErr
				SET STEP ON
				ASSERT .F. MESSAGE "In internal Catch Section (Not main loop)...debug"
				IF lnormalexit = .F.
					tmessage = "Account: "+ALLTRIM(STR(Acctnum))+" (Internal loop)"
					tmessage = tmessage+CHR(13)+"TRY/CATCH Exeception Message:"+CHR(13)+;
						[  Error: ] + STR(oErr.ERRORNO) +CHR(13)+;
						[  LineNo: ] + STR(oErr.LINENO) +CHR(13)+;
						[  Message: ] + oErr.MESSAGE +CHR(13)+;
						[  Procedure: ] + oErr.PROCEDURE +CHR(13)+;
						[  Details: ] + oErr.DETAILS +CHR(13)+;
						[  StackLevel: ] + STR(oErr.STACKLEVEL) +CHR(13)+;
						[  LineContents: ] + LEFT(oErr.LINECONTENTS,50)+CHR(13)+;
						[  UserValue: ] + oErr.USERVALUE+CHR(13)

					tsubject = "Error Loading Inbound File "+xfile
					tsendto = tsendtoerr
					DO CASE
					CASE INLIST(cOffice,"C","Y") AND INLIST(nAcctNum,4610,4694)  && Nanjing
*						CASE cOffice = "Y" AND INLIST(nAcctNum,4610,4694)  && Nanjing
						tcc = ""
					CASE cOffice = "C" AND nAcctNum = 1285  && AgeGroup
						tcc = "yvalencia@fmiint.com"
					OTHERWISE
						tcc = tccerr
					ENDCASE
					tattach = ""
					DO FORM m:\dev\FRM\dartmail2 WITH tsendto,tFrom,tsubject,tcc,tattach,tmessage,"A"

					MESSAGEBOX(tmessage,0,"EDI Error",5000)

					WAIT CLEAR
					llContinue = .F.
				ELSE
					WAIT WINDOW "Normal Exit..." TIMEOUT 2
				ENDIF
				SET STATUS BAR ON
			ENDTRY

			IF llContinue = .F.
				NormalExit = .T.
				CLOSE DATABASES ALL
				THROW
			ELSE
				USE IN 201
			ENDIF
		ENDIF


*		ASSERT .F. MESSAGE "At main process ATTRIB change point...DEBUG"
		xfile = lcpath+TRIM(tarray[thisfile,1])
		attrfile = '"'+lcpath+TRIM(tarray[thisfile,1])+'"'
		archivefile = lcArchivePath+TRIM(tarray[thisfile,1])
		IF !INLIST(nAcctNum,6035,4610,4694)
			!ATTRIB -R &attrfile  && Removes read-only flag from file to allow deletion
		ENDIF

		WAIT WINDOW "Importing file: "+xfile TIMEOUT 2

		IF FILE(xfile)
			IF UPPER(JUSTEXT(xfile)) = "XLSM"
				WAIT WINDOW "Invalid Excel Format TYPE" TIMEOUT 2
				DELETE FILE [&xfile]
				THROW
			ENDIF

			IF INLIST(Acctnum,1285,5432,6097,6209,6242)
				IF UPPER(JUSTEXT(xfile)) = "XLSX"
					lXLSX = .T.
					DO m:\dev\prg\xlsx_conversion WITH xfile
				ELSE
					WAIT WINDOW "No file conversion for account "+TRANSFORM(Acctnum) TIMEOUT 2
				ENDIF
			ENDIF

*			useca("upcmast","wh",,,,,"upcmastsql")

			IF INLIST(Acctnum,6035)
				DO &PNAME WITH xfilecsv,Acctnum,cAcctname,cOffice
			ELSE
				DO &PNAME WITH xfile,Acctnum,cAcctname,cOffice
			ENDIF

			SELECT pl
			COUNT TO xnumskus FOR !units AND wo_num=inwolog.wo_num

			xsqlexec("select * from inrcv where container='"+inwolog.CONTAINER+"' and status#'CONFIRMED'","xinrcv",,"wh")
			SELECT xinrcv
			IF RECCOUNT() > 0
				xsqlexec("update inrcv set inwonum="+TRANSFORM(inwolog.wo_num)+" where container='"+inwolog.CONTAINER+"' and status#'CONFIRMED'",,,"wh")
			ELSE
				SELECT inrcv
				SCATTER MEMVAR MEMO BLANK
				m.mod = cMod
				m.office = cOffice
				m.inrcvid=dygenpk("inrcv","whall")  && new get the ID value
				m.inwonum=inwolog.wo_num
				m.accountid=inwolog.accountid
				m.acctname=inwolog.acctname
				m.container=inwolog.CONTAINER
				m.status="NOT AVAILABLE"
				m.skus=xnumskus
				m.cartons=inwolog.plinqty
				m.newdt=DATE()
				m.addfrom="IN"
				m.confirmdt=empty2nul(m.confirmdt)
				m.newdt=empty2nul(m.newdt)
				m.dydt=empty2nul(m.dydt)
				m.firstavaildt=empty2nul(m.firstavaildt)
				m.apptdt=empty2nul(m.apptdt)
				m.pickedup=empty2nul(m.pickedup)

				INSERT INTO inrcv FROM MEMVAR
				tu("inrcv")
			ENDIF

			SELECT wos
			nWOSRecs = RECCOUNT()
			cfilenamein = wos.filename

			LOCATE

			IF lMainMail AND (wos.cartons>0 OR wos.quantity>0)

				IF INLIST(Acctnum,4610,4694) && AND nWOSRecs > 1
					cAcctname = "NANJING"
				ENDIF

				IF INLIST(Acctnum,&gtkohighlineaccounts)
					cAcctname = "MODERN SHOE/HIGHLINE"
				ENDIF


				EmailcommentStr = REPLICATE("-",80)+CHR(13)
				EmailcommentStr = EmailcommentStr+"Packinglist upload run from machine: "+SYS(0)+CHR(13)
				EmailcommentStr = EmailcommentStr+REPLICATE("-",80)+CHR(13)
				EmailcommentStr = EmailcommentStr+"Inbound Workorders created for "+cAcctname+":"+CHR(13)
				EmailcommentStr = EmailcommentStr+"Data from File : "+cfilenamein+CHR(13)
				EmailcommentStr = EmailcommentStr+REPLICATE("-",80)+CHR(13)

				SELECT wos
				SCAN
					DO CASE
					CASE (cOffice = "C" AND Acctnum =4654)
						EmailcommentStr = EmailcommentStr+"Container: "+ALLTRIM(wos.CONTAINER)+"  WO# "+ ALLTRIM(STR(wos.wo_num))+;
							"  Ctns: "+ALLTRIM(STR(wos.quantity))+ "  Sorts: "+ALLTRIM(STR(wos.sorts))+"  Season: "+ALLTRIM(wos.season)+CHR(13)
					CASE INLIST(Acctnum,&gtkohighlineaccounts)  && Modern Shoe
						EmailcommentStr = EmailcommentStr+"("+ALLT(wos.acctcode)+") WO#: "+ALLTRIM(TRANSFORM(wos.wo_num))+;
							", Cust BOL: "+ALLTRIM(wos.REFNUM)+",  Ctns: "+PADL(ALLTRIM(STR(wos.cartons)),5)+;
							", Pairs: "+ALLTRIM(STR(wos.sku_units))+"  Ctr/AWB: "+ALLTRIM(wos.CONTAINER)+CHR(13)+IIF(wos.accrue," (*ACCRUED*)","")+CHR(13)
					CASE INLIST(Acctnum,4610,4694)
						EmailcommentStr = EmailcommentStr+"Container: "+ALLTRIM(wos.CONTAINER)+"  WO# "+ ALLTRIM(STR(wos.wo_num))+;
							"  Ctns: "+PADR(ALLTRIM(STR(wos.quantity)),5)+"  Packtype: "+ALLTRIM(wos.packtype)+CHR(13)
					CASE Acctnum = 4677
						SET STEP ON
						EmailcommentStr = EmailcommentStr+"Container: "+ALLTRIM(wos.CONTAINER)+"  WO# "+ ALLTRIM(STR(wos.wo_num))+;
							"  Ctns: "+ALLTRIM(STR(plqty1))+CHR(13)
					OTHERWISE
						EmailcommentStr = EmailcommentStr+"Container: "+ALLTRIM(wos.CONTAINER)+"  WO# "+ ALLTRIM(STR(wos.wo_num))+;
							"  Ctns: "+ALLTRIM(STR(wos.quantity))+CHR(13)
					ENDCASE
				ENDSCAN
				WAIT CLEAR
				tsubject= "Inbound WO(s) created for "+ALLTRIM(cAcctname)+" at "+TTOC(DATETIME())
				tattach = ""
				IF USED('mm')
					USE IN mm
				ENDIF
				SELECT 0
				USE F:\3pl\DATA\mailmaster ALIAS mm
				IF lTestmail OR lTest
					LOCATE FOR edi_type = "MISC" AND taskname = "GENERAL"
					tsendto = ALLTRIM(IIF(mm.use_alt,mm.sendtoalt,mm.sendto))
					tcc = ALLTRIM(IIF(mm.use_alt,mm.ccalt,mm.cc))
				ENDIF
				USE IN mm


				tmessage = EmailcommentStr

*				ASSERT .F. MESSAGE "At email process...debug"
				IF !lTest
					SELECT wos
					nWOSCtns = wos.cartons
				ENDIF
				DO FORM m:\dev\frm\dartmail2 WITH tsendto,tFrom,tsubject,tcc,tattach,tmessage,"A"
			ENDIF
		ENDIF

*		SET STEP ON
		IF !lHoldFile && and lTest
			IF FILE(xfile)
				IF !lXLSX  && If file hadn't already been converted
					COPY FILE [&xfile] TO [&archivefile]
				ENDIF
				DELETE FILE [&xfile]
			ENDIF
			IF INLIST(nAcctNum,4677,6035,4610,4694)
				DELETE FILE lcpath+"*.csv"
			ENDIF
		ENDIF
	ENDFOR

	lnormalexit = .T.
	IF lnormalexit = .T.
		WAIT WINDOW AT 10,10 "Normal Exit " TIMEOUT 2
		SELECT ftpsetup
		REPLACE ftpsetup.CHKBUSY WITH .F. FOR UPPER(ftpsetup.transfer) = UPPER(cTransfer) IN ftpsetup
	ENDIF

CATCH TO oErr
	IF lnormalexit = .F.
		ASSERT .F. MESSAGE "AT MAIN CATCH SECTION"
		SET STEP ON
		tmessage = "WMS Inbound Error..... Please fix me........!"
		lcSourceMachine = SYS(0)
		lcSourceProgram = SYS(16)

		tmessage =tmessage+ "Try/CATCH Exeception Message:"+CHR(13)+;
			[  Error: ] + STR(oErr.ERRORNO) +CHR(13)+;
			[  LineNo: ] + STR(oErr.LINENO) +CHR(13)+;
			[  Message: ] + oErr.MESSAGE +CHR(13)+;
			[  Procedure: ] + oErr.PROCEDURE +CHR(13)+;
			[  Details: ] + oErr.DETAILS +CHR(13)+;
			[  StackLevel: ] + STR(oErr.STACKLEVEL) +CHR(13)+;
			[  LineContents: ] + oErr.LINECONTENTS+CHR(13)+;
			[  UserValue: ] + oErr.USERVALUE+CHR(13)+;
			[  Computer:  ] +lcSourceMachine+CHR(13)+;
			[  Program:   ] +lcSourceProgram

		tsubject = "WMS Inbound Error ("+TRANSFORM(oErr.ERRORNO)+") at "+TTOC(DATETIME())
		tattach  = " "
		IF USED('mm')
			USE IN mm
		ENDIF
		SELECT 0
		USE F:\3pl\DATA\mailmaster ALIAS mm
		LOCATE FOR edi_type = "MISC" AND taskname = "GENERAL"
		lUseAlt = mm.use_alt
		tsendto = ALLTRIM(IIF(lUseAlt,mm.sendtoalt,mm.sendto))
		tcc = ALLTRIM(IIF(lUseAlt,mm.ccalt,mm.cc))
		USE IN mm
		WAIT WINDOW "SEND TO: "+tsendto+CHR(13)+"CC: "+tcc TIMEOUT 2
		DO FORM m:\dev\FRM\dartmail2 WITH tsendto,tFrom,tsubject,tcc,tattach,tmessage,"A"
	ELSE
		WAIT WINDOW AT 10,10 "Normal Exit " TIMEOUT 2
	ENDIF
FINALLY
	SELECT 0
	CLOSE DATA ALL
	SET STATUS BAR ON
	ON ERROR
	closedata()
ENDTRY

*!* Removed Modern Shoe inbound process to its own project. Joe, 10.17.2016

*************************************************************************************************
PROCEDURE CourtauldsInbound
*************************************************************************************************
PARAMETERS xfile,xacctnum,xacctname,cOffice
*On Error Debug

m.confirmdt = DATETIME()
goffice = cOffice
TRY

	SET DATE TO american
	lcErrorMessage = ""
	ssend = 0

	xsqlexec("select * from inwolog where .f.","inwolog1",,"wh")

	SELECT inwolog1
	SCATTER MEMVAR MEMO BLANK
	SELE * FROM pl WHERE .F. INTO CURSOR pl_in READWRITE
	WAIT WINDOW AT 10,10 "Courtaulds Inbound.... Processing............" NOWAIT

	CREATE CURSOR wos ( ;
		filename c(100),;
		wo_num N(10),;
		CONTAINER  c(18), ;
		cartons N(6),;
		quantity N(10))
	SELECT wos
	APPEND BLANK
	REPLACE wos.filename WITH [&xfile] IN wos

*!* Creates a dump cursor for Excel data
	CREATE CURSOR IN1 ( ;
		a  c(20),;
		b  c(20),;
		c  c(20),;
		d  c(30),;
		e  c(20),;
		F  c(50),;
		g  c(20),;
		h  c(20),;
		i  c(20),;
		j  c(20),;
		k  c(20),;
		l  c(20),;
		m  c(20),;
		N  c(20),;
		o  c(20),;
		p  c(20),;
		q  c(20),;
		R  c(20),;
		s  c(20),;
		T  c(20),;
		u  c(20),;
		v  c(20),;
		w  c(20),;
		x  c(20),;
		Y  c(20),;
		z  c(20),;
		aa  c(20),;
		ab  c(20),;
		ac  c(20),;
		ad  c(20),;
		ae  c(20),;
		af  c(20),;
		ag  c(20),;
		ah  c(20),;
		ai  c(20),;
		aj  c(20),;
		ak  c(20),;
		al  c(20))

*!* Creates a holding cursor for style grouping
	CREATE CURSOR holdstyle (STYLE c(20), packn i, PACK c(10), ctnqty i, units i,LENGTH N(5,1),WIDTH N(5,1),depth N(5,1),ctnwt N(5,1))
	IF lTest
		SET STEP ON
	ENDIF

	cFilename = JUSTFNAME(xfile)
	xfilecsv = JUSTSTEM(JUSTFNAME(xfile))+".csv"
	DO m:\dev\prg\excel_to_csv WITH cFilename
	SELECT IN1
	APPEND FROM [&xfilecsv] TYPE CSV
	IF lHoldFile OR lBrowse
		LOCATE
		BROWSE
	ENDIF

	LOCATE FOR UPPER(d) = "BPCS STYLE"
	SKIP
	SCAN REST
		IF EMPTY(c) AND EMPTY(d) AND !EMPTY(a)
			DELETE
		ENDIF
	ENDSCAN
	IF lHoldFile OR lBrowse
		LOCATE
		BROWSE
	ENDIF

	STORE 0 TO ww
	STORE "" TO ww1
	STORE "" TO m.reference,m.acct_ref,m.container,m.comments,m.Seal,m.InvNum,cPack
	STORE 0 TO m.quantity,m.unitsinqty,m.UnitsOpenQty,m.plinqty,m.plopenqty,nWidth,nLength,nDepth,nUnits,nCtnQty,nCtnWt,m.po

	SELECT IN1
	LOCATE
	LOCATE
	SCAN
		IF "VESSEL"$UPPER(F)
			m.reference = UPPER(ALLTRIM(g))  && JCPenney
			IF EMPTY(m.reference)
				m.reference = UPPER(ALLTRIM(h))  && Knightcrown
			ENDIF
		ENDIF

		IF "PORT OF ARRIVAL"$UPPER(F)
			cPort = UPPER(ALLTRIM(g))  && JCPenney
			IF EMPTY(cPort)
				cPort = UPPER(ALLTRIM(h))  && Knightcrown
			ENDIF
			cPortStr = "PORT*"+cPort
			IF EMPTY(m.comments)
				m.comments= cPortStr
			ELSE
				IF !cPortStr$m.comments
					m.comments = m.comments+CHR(13)+cPortStr
				ENDIF
			ENDIF
		ENDIF

		IF "CONTAINER NO"$UPPER(F)
			m.container = UPPER(ALLTRIM(g))
			IF EMPTY(m.container)
				SKIP
				m.container = UPPER(ALLTRIM(g))
				SKIP -1
			ENDIF
			IF EMPTY(m.container)
				m.container = UPPER(ALLTRIM(h))
			ENDIF
			IF EMPTY(m.container)
				SKIP
				m.container = UPPER(ALLTRIM(h))
				SKIP -1
			ENDIF

			m.container = STRTRAN(STRTRAN(m.container,"-","")," ","")
			m.container = STRTRAN(m.container," ","")
			REPLACE wos.CONTAINER WITH UPPER(m.container) IN wos

			IF SUBSTR(ALLTRIM(m.container),4,1)<>"U"
				m.container = ""
				m.reference = UPPER(ALLTRIM(g))
				REPLACE wos.CONTAINER WITH m.reference IN wos
			ENDIF
		ENDIF

		IF "AWB NO."$UPPER(F)
			m.reference = UPPER(ALLTRIM(g))
			IF EMPTY(m.reference)
				m.reference = UPPER(ALLTRIM(h))
			ENDIF
			IF EMPTY(m.container) AND EMPTY(m.reference)
				lcErrorMsg = "BOTH CONTAINER AND AWB MISSING"
				lnormalexit = .F.
				THROW
			ENDIF
		ENDIF

		IF "SEAL NO"$UPPER(F)
			m.Seal = UPPER(ALLTRIM(g))
			IF EMPTY(m.Seal)
				SKIP
				m.Seal = UPPER(ALLTRIM(g))
				SKIP -1
			ENDIF
			IF EMPTY(m.Seal)
				m.Seal = UPPER(ALLTRIM(h))
				IF EMPTY(m.Seal)
					SKIP
					m.Seal = UPPER(ALLTRIM(h))
					SKIP -1
				ENDIF
			ENDIF
		ENDIF

		IF "BILL OF LADING"$UPPER(F)
			cRefBill = UPPER(ALLTRIM(g))
			IF EMPTY(cRefBill)
				cRefBill = UPPER(ALLTRIM(h))
			ENDIF
			IF EMPTY(cRefBill)
				lDoSLError = .T.
				cSLErrMsg = "REF. BOL"
				cSLError = IIF(EMPTY(cSLError),cSLErrMsg,cSLError+CHR(13)+cSLErrMsg)
			ENDIF
			cRefStr = "BOL*"+cRefBill
			IF EMPTY(m.comments)
				m.comments= cRefStr
			ELSE
				IF !cRefStr$m.comments
					m.comments = m.comments+CHR(13)+cRefStr
				ENDIF
			ENDIF
		ENDIF

		IF "TOTAL CARTONS"$UPPER(q)
			ASSERT .F. MESSAGE "At total cartons check...debug"
			m.quantity = INT(VAL(ALLTRIM(s)))
			STORE m.quantity TO m.plinqty,m.plopenqty
		ENDIF

		IF ("TRANSIT WHSE"$UPPER(k) OR "TRANSIT WHSE"$UPPER(l))
			cTW = UPPER(ALLTRIM(N))
			IF EMPTY(cTW)
				lDoSLError = .T.
				cSLErrMsg = "TRANSIT WHSE"
				cSLError = IIF(EMPTY(cSLError),cSLErrMsg,cSLError+CHR(13)+cSLErrMsg)
			ENDIF

			cTWStr = "TW*"+cTW
			IF EMPTY(m.comments)
				m.comments= cTWStr
			ELSE
				IF !cTWStr$m.comments
					m.comments = m.comments+CHR(13)+cTWStr
				ENDIF
			ENDIF
		ENDIF

		IF ("TRANSIT LOCATION"$UPPER(k) OR "TRANSIT LOCATION"$UPPER(l))
			m.acct_ref = UPPER(ALLTRIM(N))
			IF EMPTY(m.acct_ref)
				lDoSLError = .T.
				cSLErrMsg = "TRANSIT LOCATION"
				cSLError = IIF(EMPTY(cSLError),cSLErrMsg,cSLError+CHR(13)+cSLErrMsg)
			ENDIF
		ENDIF

		IF ("FG WHSE"$UPPER(k) OR "FG WHSE"$UPPER(l))
			cFGW = UPPER(ALLTRIM(N))
			IF EMPTY(cFGW)
				lDoSLError = .T.
				cSLErrMsg = "FG WHSE"
				cSLError = IIF(EMPTY(cSLError),cSLErrMsg,cSLError+CHR(13)+cSLErrMsg)
			ENDIF
			cFWStr = "FW*"+cFGW
			IF EMPTY(m.comments)
				m.comments= cFWStr
			ELSE
				IF !cFWStr$m.comments
					m.comments = m.comments+CHR(13)+cFWStr
				ENDIF
			ENDIF
		ENDIF

		IF ("FG LOCATION"$UPPER(k)OR "FG LOCATION"$UPPER(l))
			cFGL = UPPER(ALLTRIM(N))
			IF EMPTY(cFGW)
				lDoSLError = .T.
				cSLErrMsg = "FG LOCATION"
				cSLError = IIF(EMPTY(cSLError),cSLErrMsg,cSLError+CHR(13)+cSLErrMsg)
			ENDIF
			cFLStr = "FL*"+cFGL
			IF EMPTY(m.comments)
				m.comments= cFLStr
			ELSE
				IF !cFLStr$m.comments
					m.comments = m.comments+CHR(13)+cFLStr
				ENDIF
			ENDIF
		ENDIF

		IF "TOTAL PACKS"$UPPER(q)
			m.unitsinqty = 0
			STORE m.unitsinqty TO m.UnitsOpenQty
		ENDIF
	ENDSCAN
	IF lDoSLError = .T.
		SLErrormail()
		lnormalexit = .F.
		THROW
	ENDIF

	LOCATE
	DO WHILE !("BPCS STYLE NO"$UPPER(IN1.d)) AND !EOF()
		SKIP
	ENDDO
	WAIT WINDOW "BPCS Style line is in record "+ALLTRIM(STR(RECNO())) TIMEOUT 2
	IF lTest
		BROWSE
	ENDIF

*		ASSERT .F. MESSAGE "At DIM and weight determination"
	STORE "" TO cLenCol,cWidCol,cHiCol,cCWtCol
	lKG = .T.
	cPackCol = ""

	IF lTest
		SET STEP ON
	ENDIF
	FOR xz = 108 TO 97 STEP -1
		cCol = "a"+CHR(xz)  && Col AL to AA
		DO CASE
		CASE UPPER(ALLTRIM(IN1.&cCol)) == "H"
			cHiCol = cCol
		CASE UPPER(ALLTRIM(IN1.&cCol)) == "W"
			cWidCol = cCol
		CASE UPPER(ALLTRIM(IN1.&cCol)) == "L"
			cLenCol = cCol
		CASE UPPER(ALLTRIM(IN1.&cCol)) = "N.W KGS/CTN"
			lKG = .T.
			cCWtCol = cCol
		CASE UPPER(ALLTRIM(IN1.&cCol)) = "G.W KGS/CTN"
			lKG = .T.
			cCWtCol = cCol
		CASE UPPER(ALLTRIM(IN1.&cCol)) = "N.W LBS/CTN"
			lKG = .F.
			cCWtCol = cCol
		CASE UPPER(ALLTRIM(IN1.&cCol)) = "G.W LBS/CTN"
			lKG = .F.
			cCWtCol = cCol
		CASE UPPER(ALLTRIM(IN1.&cCol)) = "PKGS PER CARTON"
			cPackCol = cCol
		ENDCASE
	ENDFOR

	FOR xz = 122 TO 114 STEP -1 && Cols Z to R
		cCol = CHR(xz)
		DO CASE
		CASE UPPER(ALLTRIM(IN1.&cCol)) == "H"
			cHiCol = cCol
		CASE UPPER(ALLTRIM(IN1.&cCol)) == "W"
			cWidCol = cCol
		CASE UPPER(ALLTRIM(IN1.&cCol)) == "L"
			cLenCol = cCol
		CASE UPPER(ALLTRIM(IN1.&cCol)) = "N.W KGS/CTN"
			lKG = .T.
			cCWtCol = cCol
		CASE UPPER(ALLTRIM(IN1.&cCol)) = "G.W KGS/CTN"
			lKG = .T.
			cCWtCol = cCol
		CASE UPPER(ALLTRIM(IN1.&cCol)) = "N.W LBS/CTN"
			lKG = .F.
			cCWtCol = cCol
		CASE UPPER(ALLTRIM(IN1.&cCol)) = "G.W LBS/CTN"
			lKG = .F.
			cCWtCol = cCol
		CASE UPPER(ALLTRIM(IN1.&cCol)) = "PKGS PER CARTON"
			cPackCol = cCol
		ENDCASE
	ENDFOR
	ASSERT .F. MESSAGE "At Weight/Measures...debug"
	IF EMPTY(cLenCol) OR EMPTY(cWidCol) OR EMPTY(cHiCol) OR EMPTY(cCWtCol)
		WAIT WINDOW "Empty Wts & Meas. Value" TIMEOUT 5
	ENDIF

	FOR sq = 104 TO 126  && h to z
		sq1 = CHR(sq)
		IF (EMPTY(ALLTRIM(IN1.&sq1)) OR ("/"$IN1.&sq1) OR (IN1.&sq1="0"))
			ASSERT .F. MESSAGE "At SSEND location...debug"
			ssend = sq-1
			EXIT
		ENDIF
	ENDFOR

	IF !EOF()
		SKIP 1 IN IN1
		IF EMPTY(IN1.d)
			SKIP 1 IN IN1
		ENDIF
	ENDIF

	ASSERT .F. MESSAGE "In initial Pack header determination"  && At "BPCS Style" line
	FOR ss = ssend TO 104 STEP -1
		ss1 = CHR(ss)
		IF lHoldFile
			WAIT WINDOW "Contents of IN1."+ss1+": "+(IN1.&ss1) NOWAIT
		ENDIF
		IF VAL(ALLTRIM(IN1.&ss1)) = 0
			LOOP
		ENDIF
		IF EMPTY(ALLTRIM(IN1.&ss1))
			LOOP
		ENDIF
		IF !ISDIGIT(ALLTRIM(IN1.&ss1))
			LOOP
		ENDIF
		STORE ss TO nSS
		EXIT
	ENDFOR

	lThong = IIF(EMPTY(IN1.h) AND EMPTY(IN1.i) AND EMPTY(IN1.j) ;
		AND EMPTY(IN1.k) AND EMPTY(IN1.l),.F.,.T.)
	IF lThong
		WAIT WINDOW "This is a Thong packing list" TIMEOUT 2
	ENDIF

	IF lHoldFile
		ASSERT .F. MESSAGE "Browsing IN1 file...debug"
*			BROWSE
	ENDIF

	IF lBrowse
*			BROWSE
	ENDIF

	DO WHILE !EOF() AND !EMPTY(TRIM(IN1.d))
		FOR ss = ssend TO 104 STEP -1
			ss1 = CHR(ss)
			IF lHoldFile
				WAIT WINDOW "Contents of IN1."+ss1+": "+(IN1.&ss1) NOWAIT
			ENDIF
			IF VAL(ALLTRIM(IN1.&ss1)) = 0
				LOOP
			ENDIF
			IF EMPTY(ALLTRIM(IN1.&ss1))
				LOOP
			ENDIF
			IF !ISDIGIT(ALLTRIM(IN1.&ss1))
				LOOP
			ENDIF
			STORE ss TO nSS
			EXIT
		ENDFOR

		ASSERT .F. MESSAGE "At thong determination"
		nCtnQty = INT(VAL(ALLTRIM(IN1.c)))
		cStyle = UPPER(ALLTRIM(IN1.d))
		IF cStyle = "CTNS"
			SKIP 1 IN IN1
			LOOP
		ENDIF
		IF !lThong
			cPack = IN1.&cPackCol
		ELSE
			FOR ss = nSS TO 104 STEP -1
				cSS = CHR(ss)
				IF VAL(ALLTRIM(IN1.&cSS))>0
					cPack = ALLTRIM(IN1.&cSS)
					EXIT
				ENDIF
			ENDFOR
		ENDIF

		nPack = INT(VAL(cPack))
		nUnits = 0
		IF lThong
			STORE nPack TO nUnits
		ELSE
			nUnits = INT(VAL(IN1.u))
		ENDIF
		nLength = ROUND(VAL(IN1.&cLenCol)/2.54,0)
		nWidth = ROUND(VAL(IN1.&cWidCol)/2.54,0)
		nDepth = ROUND(VAL(IN1.&cHiCol)/2.54,0)
		IF lKG
			nCtnWt = ROUND(VAL(IN1.&cCWtCol)*2.202,2)/nCtnQty
		ELSE
			nCtnWt = ROUND(VAL(IN1.&cCWtCol),2)/nCtnQty
		ENDIF
		SELECT holdstyle
		LOCATE
		LOCATE FOR holdstyle.STYLE = cStyle AND holdstyle.PACK = PADR(ALLTRIM(cPack),10)
		IF FOUND()
			REPLACE holdstyle.ctnqty WITH holdstyle.ctnqty + nCtnQty
			REPLACE holdstyle.units WITH holdstyle.units + nUnits
			STORE 0 TO nCtnQty,nUnits
		ELSE
			INSERT INTO holdstyle (STYLE,packn,PACK,ctnqty,units,LENGTH,WIDTH,depth,ctnwt) ;
				VALUES (cStyle,nPack,cPack,nCtnQty,nUnits,nLength,nWidth,nDepth,nCtnWt)
			STORE 0 TO nCtnQty,nUnits,nWidth,nLength,nDepth
		ENDIF


		SELECT IN1
		SKIP
	ENDDO

	IF lHoldFile
		BROWSE
	ENDIF

	SELECT holdstyle
	SUM ((holdstyle.LENGTH*holdstyle.WIDTH*holdstyle.depth)/1728) TO m.cuft
	SUM holdstyle.ctnqty TO m.cartons
	SUM holdstyle.ctnwt TO m.weight
	SUM holdstyle.units TO m.unitsinqty
	STORE m.unitsinqty TO m.UnitsOpenQty
	REPLACE wos.cartons WITH m.cartons,wos.quantity WITH m.unitsinqty IN wos
	STORE 0  TO m.unitsinqty,m.quantity,m.remainqty,m.cartons
	LOCATE

	m.adddt = {}
	m.adddt = DATETIME()
	m.accountid = xacctnum
	m.acctname=xacctname
	m.office = cOffice
	m.inwologid  = 1
	m.plid = 0
	m.npo = 0
*m.confirmdt = IIF(EMPTY(m.confirmdt),empty2nul(m.confirmdt),m.confirmdt)

	INSERT INTO inwolog1 FROM MEMVAR
	SELECT inwolog1

	SELECT holdstyle
	LOCATE
	IF DATE() = {^2012-04-18}
		BROWSE
	ENDIF

	SCAN
* Carton detail line for Picking
		m.plid = m.plid + 1
		m.npo = m.npo + 1
		m.po = ALLTRIM(STR(m.npo))
		m.ctncube = ((holdstyle.LENGTH*holdstyle.WIDTH*holdstyle.depth)/1728)
		m.date_rcvd = DATE()
		m.units = .F.
		m.style = UPPER(holdstyle.STYLE)
		m.pack = holdstyle.PACK
		m.totqty = holdstyle.ctnqty
		m.length = holdstyle.LENGTH
		m.width = holdstyle.WIDTH
		m.depth = holdstyle.depth
		m.ctnwt = holdstyle.ctnwt
		INSERT INTO pl_in FROM MEMVAR

		?"PLID: "+TYPE('m.plid')
		?"PO: "+TYPE('m.po')
		?"Rec Date: "+TYPE('m.date_rcvd')
		?"Units: "+TYPE('m.units')
		?"Style: "+TYPE('m.style')
		?"TotQty: "+TYPE('m.totqty')
		?"Length: "+TYPE('m.length')
		?"Width: "+TYPE('m.width')
		?"Depth: "+TYPE('m.depth')
		?"CtnWt: "+TYPE('m.ctnwt')
		?"Pack: "+TYPE('m.pack')
		?
		?
* Units detail line for Picking
		m.units = .T.
		m.plid = m.plid + 1
		m.totqty = holdstyle.ctnqty*INT(VAL(m.pack))
		STORE 0 TO m.length,m.width,m.depth,m.ctnwt
		m.pack = "1"

*!* Determination of data types
*!*				?"PLID: "+TYPE('m.plid')
*!*				?"PO: "+TYPE('m.po')
*!*				?"TotQty: "+TYPE('m.totqty')
*!*				?"Pack: "+TYPE('m.pack')
		INSERT INTO pl_in FROM MEMVAR
		CLEAR
	ENDSCAN
	IF lBrowse OR lHoldFile
		SELECT inwolog1
		LOCATE
		BROWSE
		SELECT pl_in
		LOCATE
		BROWSE
		ASSERT .F. MESSAGE "Just before insertion of data...debug or cancel here"
	ENDIF

	m.date_rcvd=DATE()
	m.plinqty=0

*	now create the work order

	WAIT WINDOW AT 10,10 "Courtaulds Inbound.... Creating the work order........." NOWAIT
	SELECT inwolog1
	SCATTER MEMVAR MEMO
	m.addby='WMSIN'
	m.adddt={}
	m.adddt=DATETIME()
	m.inwologid1 = m.inwologid
	m.wo_num1=dygenpk("wonum",cWhseMod)
	m.refer_num=whrefnum(m.accountid)
	m.wo_date=DATE()
	m.office=cOffice
	STORE m.wo_num1 TO m.wo_num
	m.mod = cMod
	m.office = cOffice
	insertinto("inwolog","wh",.T.)
	nInwologid = inwolog.inwologid

	REPLACE wos.wo_num WITH m.wo_num IN wos
	SELECT pl_in
	GOTO TOP
	SCAN
		SCATTER MEMVAR MEMO
		m.addby='WMSIN'
		m.adddt={}
		m.adddt=DATETIME()
		m.wo_num = m.wo_num1
		m.inwologid = nInwologid
		m.po      = UPPER(ALLTRIM(pl_in.po))
		m.style   = UPPER(ALLTRIM(pl_in.STYLE))
		m.pack    = ALLTRIM(pl_in.PACK)
		m.totqty  = pl_in.totqty
		m.plinqty = pl_in.totqty
		m.plopenqty = m.plinqty
		m.office = cOffice
		m.mod = cMod
		insertinto("pl","wh",.T.)
	ENDSCAN

	SELECT pl
*		SET STEP ON
	STORE 0 TO plqty1,plqty2
	SUM totqty TO plqty1 FOR !pl.units AND pl.wo_num = m.wo_num1
	SUM totqty TO plqty2 FOR pl.units AND pl.wo_num = m.wo_num1
	REPLACE inwolog.plinqty WITH plqty1 FOR inwolog.wo_num = m.wo_num1 IN inwolog
	REPLACE inwolog.plunitsinqty WITH plqty1 FOR inwolog.wo_num = m.wo_num1 IN inwolog

	IF lBrowse OR DATETIME()<DATETIME(2016,08,30,16,15,00)
		SELECT inwolog
		BROWSE FOR wo_num = m.wo_num1
		SELECT pl
		BROWSE FOR wo_num = m.wo_num1
	ENDIF

	tu("inwolog")
	IF lDoPL
		tu("pl")
	ENDIF

CATCH TO oErr
	IF lnormalexit = .F.
		ptError = .T.
		tsubject = "Inbound Upload Error ("+TRANSFORM(oErr.ERRORNO)+") for Courtaulds at "+TTOC(DATETIME())+"  on file "+xfile
		tattach  = ""
		tmessage = "Error processing Inbound file for Courtaulds.... Filename: "+xfile+CHR(13)
		SELECT 0
		USE F:\3pl\DATA\mailmaster ALIAS mm
		LOCATE FOR edi_type = "MISC" AND taskname = "GENERAL"
		tsendto  = IIF(mm.use_alt,mm.sendtoalt,mm.sendto)
		tcc = IIF(mm.use_alt,mm.ccalt,mm.cc)
		USE IN mm
		SELECT pl_in
		lnRec= RECNO("pl_in")

		tmessage = tmessage+CHR(13)+" Error at record "+TRANSFORM(lnRec)+CHR(13)+CHR(13)
		tmessage = tmessage+STYLE
		tmessage = tmessage+PACK


		tmessage =tmessage+ "Try/CATCH Exeception Message:"+CHR(13)+;
			[  Error: ] + STR(oErr.ERRORNO) +CHR(13)+;
			[  LineNo: ] + STR(oErr.LINENO) +CHR(13)+;
			[  Message: ] + oErr.MESSAGE +CHR(13)+;
			[  Procedure: ] + oErr.PROCEDURE +CHR(13)+;
			[  Details: ] + oErr.DETAILS +CHR(13)+;
			[  StackLevel: ] + STR(oErr.STACKLEVEL) +CHR(13)+;
			[  LineContents: ] + oErr.LINECONTENTS+CHR(13)+;
			[  UserValue: ] + oErr.USERVALUE+CHR(13)+;
			[  Machine Name: ] + SYS(0)+CHR(13)+;
			[  Error Message :]+lcErrorMessage+CHR(13)

		tsubject = "Inbound WO Error for Courtaulds at "+TTOC(DATETIME())+"  on file "+xfile
		tattach  = ""
		IF USED('mm')
			USE IN mm
		ENDIF
		USE F:\3pl\DATA\mailmaster ALIAS mm
		LOCATE FOR edi_type = "MISC" AND taskname = "GENERAL"
		lUseAlt = mm.use_alt
		tsendto = ALLTRIM(IIF(lUseAlt,mm.sendtoalt,mm.sendto))
		tcc = ALLTRIM(IIF(lUseAlt,mm.ccalt,mm.cc))
		USE IN mm
		DO FORM m:\dev\frm\dartmail2 WITH tsendto,tFrom,tsubject,tcc,tattach,tmessage,"A"
		lCreateErr = .T.
	ELSE
		WAIT WINDOW "Normal exit..." TIMEOUT 2
	ENDIF
ENDTRY
ENDPROC


**************************
PROCEDURE GenericInbound
**************************

PARAMETERS xfile,xacctnum,xacctname,cOffice
lcAcctRef = ""

ASSERT .F. MESSAGE "At start of GenericInbound Process"
TRY
	lCayset = .F.
	lcErrorMessage = ""
	goffice= cOffice

	CREATE CURSOR GEN856_in (ctnrange c(20),;
		grouping c(3), ;
		units  l, ;
		STYLE c(20), ;
		COLOR c(10), ;
		ID    c(30), ;
		SIZE  c(10), ;
		PACK  c(10), ;
		LENGTH N(4), ;
		WIDTH  N(4), ;
		acct_ref c(20), ;
		REFERENCE c(20), ;
		depth  N(4), ;
		ctnwt i, ;
		skuqty i, ;
		ctnqty i)
	STORE "" TO m.style,m.color,m.id,m.size,m.pack

	CREATE CURSOR INX ( ;
		a  c(20),;
		b  c(20),;
		c  c(20),;
		d  c(30),;
		e  c(20),;
		F  c(20),;
		g  c(20),;
		h  i)

	IF xacctnum = 6035
		SELECT tempmain
	ELSE
		SELECT 200
		IMPORT FROM [&xfile] TYPE XL8
		SELECT 200
	ENDIF
	talias=ALIAS()
	SELECT &talias
	REPLACE &talias..a WITH ALLT(&talias..a) ALL
	REPLACE &talias..b WITH ALLT(&talias..b) ALL
	REPLACE &talias..c WITH ALLT(&talias..c) ALL
	REPLACE &talias..d WITH ALLT(&talias..d) ALL
	REPLACE &talias..e WITH ALLT(&talias..e) ALL

	SELECT INX
	APPEND FROM DBF(talias)

	LOCATE FOR ("# OF CARTONS"$UPPER(INX.a) OR "CARTON RANGE" $UPPER(INX.a))
	IF FOUND()
		nDetRec1 = RECNO()
	ELSE
		WAIT WINDOW "Generic Inbd CARTON RANGE line missing" TIMEOUT 4
		lnormalexit = .F.
		THROW
	ENDIF

	LOCATE
	IF lTest
*			BROW
	ENDIF
	DELETE FOR EMPTY(INX.a) AND EMPTY(INX.b) AND EMPTY(INX.c) AND EMPTY(INX.d) AND RECNO()>nDetRec1
	LOCATE
	SCAN FOR !DELETED() AND RECNO()>nDetRec1
		IF RECNO()>nDetRec1
			lcRange = ALLTRIM(a)
			IF "-"$lcRange
				f1 = INT(VAL(F))
				g1 = INT(VAL(g))
				REPLACE h WITH g1-f1+1 IN INX
			ELSE
				REPLACE F WITH "1" IN INX
				REPLACE g WITH lcRange IN INX
				f1 = INT(VAL(F))
				g1 = INT(VAL(lcRange))
				REPLACE h WITH g1-f1+1 IN INX
			ENDIF
		ENDIF
	ENDSCAN

	lcReference = ""
	lcAcctRef = ""

	SELECT INX
	LOCATE
	IF lHoldFile
		WAIT WINDOW "Browsing INX after A,F & G replacement" TIMEOUT 2
		BROWSE
	ENDIF

*!* New intake format: line correction
	IF UPPER(ALLTRIM(a)) = "FA" AND UPPER(ALLTRIM(b)) = "FB"
		ASSERT .F. MESSAGE "In INX line correction"
		COPY TO "F:\3pl\tempxyz.dbf" FOR RECNO()>1
		CREATE CURSOR INX ( ;
			a  c(20),;
			b  c(20),;
			c  c(20),;
			d  c(30),;
			e  c(20),;
			F  c(20),;
			g  c(20),;
			h  i)
		APPEND FROM "F:\3pl\tempxyz.dbf"
		DELETE FILE "F:\3pl\tempxyz.dbf"
		LOCATE
		IF lHoldFile
			BROWSE
		ENDIF
	ENDIF
*!* End intake format change

*!* Added this to correct carton ranges.
	SELECT INX
	SCAN FOR RECNO()>nDetRec1
		IF RECNO()= nDetRec1+1
			REPLACE F WITH "1" IN INX
			REPLACE g WITH ALLTRIM(STR(INX.h)) IN INX
			nStart = h+1
		ELSE
			REPLACE F WITH ALLTRIM(STR(nStart)) IN INX
			REPLACE g WITH ALLTRIM(STR(nStart+h-1)) IN INX
			nStart = INT(VAL(g))+1
		ENDIF
		REPLACE a WITH ALLTRIM(F)+"-"+ALLTRIM(g) IN INX
	ENDSCAN

	IF lHoldFile
		WAIT WINDOW "Browsing INX after ranges are reset" TIMEOUT 2
		BROWSE
		ASSERT .F. MESSAGE "Proceeding through...debug"
	ENDIF

	lnCtr=1

	lcReference = ""
	lcComments = ""
	lcAcctRef = ""
	lcSeal = ""
	lcBrokerref = ""
	lcPOnum = ""
	ShipDate = ""
	lcContainer = ""
	lcArrivePort = ""
	lcPrintComments = ""

	SELECT INX
	LOCATE
	IF lHoldFile
		ASSERT .F. MESSAGE "At Rec. 11 and under scan---DEBUG here"
	ENDIF

	DO WHILE RECNO() < nDetRec1
		IF "PREPARED BY"$UPPER(a) AND xacctnum = 1285 && For AgeGroup Trailer clearance (32 trailers)
			lCayset = IIF(UPPER(ALLTRIM(b))="XFER",.T.,.F.)
		ENDIF

		IF "ACCOUNT REF"$UPPER(a)
			lcAcctRef = UPPER(ALLTRIM(b))
		ENDIF

		IF "BROKER REF"$UPPER(a)
			lcBrokerref = UPPER(ALLTRIM(b))
		ENDIF

		IF ("VESSEL/AWB"$UPPER(a) OR "AWB NO."$UPPER(a))
			lcReference = UPPER(ALLTRIM(b))
		ENDIF

		IF "CONTAINER"$UPPER(a)
			lcContainer = UPPER(ALLTRIM(b))
		ENDIF

		IF UPPER(ALLTRIM(a))="COMMENTS"
			lcComments = UPPER(ALLTRIM(b))
		ENDIF

		IF "SEAL"$UPPER(a)
			lcSeal = UPPER(ALLTRIM(b))
		ENDIF

		IF "PRINT COMMENTS"$UPPER(a)
			lcPrintComments = UPPER(ALLTRIM(b))
		ENDIF

		SKIP 1 IN INX
	ENDDO

	DO WHILE " "$lcContainer
		lcContainer = STRTRAN(lcContainer," ","")
	ENDDO
	lcContainer = STRTRAN(lcContainer,"-","")


	WAIT WINDOW AT 10,10 "Generic Inbound.... Processing............" NOWAIT

	SELECT INX
	LOCATE
	IF lHoldFile
		BROWSE
	ENDIF
	DELETE FOR EMPTY(ALLTRIM(a))
	LOCATE
	GOTO 12

	SCAN FOR RECNO()> nDetRec1 AND !EMPTY(ALLTRIM(a))
		lcRange = ALLTRIM(a)
		IF EMPTY(F) OR EMPTY(g)
			lcAA = ALLTRIM(LEFT(lcRange,AT("-",lcRange)-1))
			lcBB = ALLTRIM(SUBSTR(lcRange,AT("-",lcRange)+1))
			REPLACE F WITH lcAA
			REPLACE g WITH lcBB
		ENDIF
	ENDSCAN
	LOCATE

	IF lHoldFile
		SET STEP ON
	ENDIF
	SELECT INX
	talias=ALIAS()

	SELECT &talias
	LOCATE
	IF lCartonAcct
		lnGroup = 1
	ELSE
		lnGroup = 0
	ENDIF
	IF talias="IN2"
		IF lHoldFile
*!*		ASSERT .f. message "At IN2 line locator...browse"
*!*		BROWSE
		ENDIF
	ENDIF
	LOCATE
	LOCATE FOR "# OF CARTONS"$UPPER(&talias..a)
	IF FOUND()
		SKIP
	ELSE
		LOCATE
	ENDIF
*!*		ASSERT .F. MESSAGE "At GEN856_in population...debug"

	DO WHILE !EOF(talias) AND !EMPTY(&talias..a)
		IF !lCartonAcct
			lnGroup = lnGroup+1
		ENDIF
		lMixRec = .F.

		SELECT GEN856_in
		APPEND BLANK
		cAField = ALLTRIM(&talias..a)
		REPLACE ctnrange WITH ALLTRIM(cAField)

		IF "-"$cAField
			STORE cAField TO lcCartonRange
			REPLACE ID WITH UPPER(ALLTRIM(&talias..d))
			lDoMix = IIF("MIX"$&talias..d,.T.,.F.)
		ELSE
			SELECT INX
			nRec=RECNO()
			cA1 = ALLT(&talias..a)
			cB1 = ALLT(&talias..b)
			cC1 = ALLT(&talias..c)
			cE1 = ALLT(&talias..e)
			COUNT TO nCount1 FOR (&talias..a) = cA1 AND (&talias..b) = cB1 ;
				AND (&talias..c) = cC1 AND (&talias..e) = cE1
			IF nCount1>1
				SUM VAL(&talias..e) TO nTotq FOR (&talias..a) = cA1 AND (&talias..b) = cB1 ;
					AND (&talias..c) = cC1 AND (&talias..e) = cE1
				cTotq = ALLTRIM(STR(INT(nTotq)))
				GO nRec
				SELECT GEN856_in
				lcCartonRange = cAField+"-"+cAField
				REPLACE ID WITH "MIXED-"+cAField
				lMixRec = .T.
			ELSE
				GO nRec
				SELECT GEN856_in
				lcCartonRange = cAField+"-"+cAField
				REPLACE ID WITH ALLTRIM(&talias..d)
			ENDIF
		ENDIF

		REPLACE units WITH .F.
		REPLACE grouping WITH ALLTRIM(STR(lnGroup))
		IF lDoMix
			cGroup = GEN856_in.grouping
		ENDIF
		lcStyle = STRTRAN(UPPER(ALLTRIM(&talias..b)),CHR(160)," ")

		SELECT GEN856_in

		lcColor = STRTRAN(UPPER(ALLTRIM(&talias..c)),CHR(160)," ")
		REPLACE STYLE WITH lcStyle
		REPLACE COLOR WITH lcColor
		IF (lMixRec AND !lCartonAcct)
			REPLACE PACK WITH cTotq
		ELSE
			REPLACE PACK WITH ALLTRIM(&talias..e)
		ENDIF

		lnPackVal = VAL(ALLTRIM(PACK))
		lnCtnQty = (VAL(ALLTRIM(&talias..g)) - VAL(ALLTRIM(&talias..F)))+1
		REPLACE ctnqty WITH (lnCtnQty) IN GEN856_in

*		ASSERT .f. message "At GenericInbound Carton Acct selector"
		IF !lCartonAcct && If account is Pick/Pack (std.) and not musical
			SELECT GEN856_in  && pfg added this 3/27/2007
			APPEND BLANK
			REPLACE units WITH .T.
			REPLACE ctnrange WITH ALLTRIM(cAField)
			REPLACE grouping WITH ALLTRIM(STR(lnGroup))
			REPLACE STYLE WITH STRTRAN(UPPER(ALLTRIM(&talias..b)),CHR(160)," ")
			REPLACE COLOR WITH STRTRAN(UPPER(ALLTRIM(&talias..c)),CHR(160)," ")
			REPLACE ID    WITH STRTRAN(UPPER(ALLTRIM(&talias..d)),CHR(160)," ")

			lnUnits = INT(VAL(ALLTRIM(&talias..e)))
			lnCtnQty = (VAL(ALLTRIM(&talias..g)) - VAL(ALLTRIM(&talias..F)))+1
			REPLACE ctnqty WITH (lnUnits*lnCtnQty)
			REPLACE PACK WITH "1" && pack always 1 on units for pick/pack accounts  (&talias..e)
			IF !EOF(talias)
				SKIP 1 IN &talias
			ELSE
				LOOP
			ENDIF

			DO WHILE ALLTRIM(&talias..F)+"-"+ALLTRIM(&talias..g) = lcCartonRange ;
					AND ALLTRIM(&talias..b)= lcStyle AND !EOF("INX")
				SELECT GEN856_in  && pfg added this 3/27/2007
				APPEND BLANK
				REPLACE units WITH .T.
				REPLACE grouping WITH ALLTRIM(STR(lnGroup))
				REPLACE STYLE WITH STRTRAN(UPPER(ALLTRIM(&talias..b)),CHR(160)," ")
				REPLACE COLOR WITH STRTRAN(UPPER(ALLTRIM(&talias..c)),CHR(160)," ")
				REPLACE ID    WITH STRTRAN(UPPER(ALLTRIM(&talias..d)),CHR(160)," ")
				lnUnits = INT(VAL(ALLTRIM(&talias..e)))
				lnCtnQty = (VAL(ALLTRIM(&talias..g)) - VAL(ALLTRIM(&talias..F)))+1
				REPLACE ctnqty WITH (lnUnits*lnCtnQty)
				REPLACE PACK WITH "1" && pack always 1 on units for pick/pack accounts  (&talias..e)

				IF !EOF(talias)
					SKIP 1 IN &talias
				ELSE
					LOOP
				ENDIF
				SELECT &talias
			ENDDO
		ELSE
		ENDIF

		IF lCartonAcct
			SKIP 1 IN &talias
		ENDIF
	ENDDO


	lnTotSorts = lnGroup
	lnTotPairs = 0
	lnTotCtns = 0
	lnTotWt = 0
	lnTotCube= 0

	SELECT GEN856_in
	DELETE FOR EMPTY(GEN856_in.grouping)
	LOCATE
	IF lHoldFile
		BROW
		COPY TO c:\tempfox\GEN856_in TYPE XL5
	ENDIF

*	ASSERT .F. MESSAGE "At GEN856_IN replacement"
	SCAN
		IF "MIX"$GEN856_in.ID
			SCATTER MEMVAR FIELDS ctnrange,STYLE,PACK
			nStartCtn = INT(VAL(LEFT(ctnrange,AT("-",ctnrange,1)-1)))
			nEndCtn = INT(VAL(SUBSTR(ctnrange,AT("-",ctnrange,1)+1)))
			nCtnCount = (nEndCtn - nStartCtn)+1
			nPackCtn = INT(VAL(m.pack))
			REPLACE GEN856_in.ctnqty WITH (nCtnCount)
			SKIP 1 IN GEN856_in
			DELETE
			SKIP 1 IN GEN856_in
			DO WHILE GEN856_in.ctnrange = m.ctnrange && AND GEN856_in.STYLE = m.style
				IF units = .F.
					DELETE
				ELSE
					REPLACE PACK WITH "1"
					REPLACE GEN856_in.ctnqty WITH (GEN856_in.ctnqty*nCtnCount)
				ENDIF
				SKIP 1 IN GEN856_in
			ENDDO
			SKIP - 1
		ENDIF
	ENDSCAN
	SELECT GEN856_in
	IF xacctnum = 4999 AND DATE()={^2009-04-24}
		BROWSE FIELDS ctnrange,grouping,units,STYLE,ID,PACK,ctnqty TIMEOUT 15
	ENDIF


	SELECT 0
	SELECT *,SPACE(10) AS packnum FROM GEN856_in INTO CURSOR gen856_out READWRITE
	SELECT gen856_out
	LOCATE
	IF EOF()
		WAIT WINDOW "EMPTY GEN856_OUT cursor...aborting" TIMEOUT 4
		lnormalexit = .F.
		THROW
	ENDIF

	nRecs = RECCOUNT()
	SCAN
		STORE PACK TO m.packnum
		SELECT gen856_out
		GATHER MEMVAR FIELDS packnum
		IF !EOF() AND nRecs>1 AND !lCartonAcct
			SKIP
			GATHER MEMVAR FIELDS packnum
		ENDIF
	ENDSCAN
	USE IN GEN856_in


	SELECT ctnrange,STYLE,COLOR,ID,PACK,units,IIF(LEN(TRIM(ID))<3,PADL(TRIM(ID),3,"0"),ID) AS id_b,packnum, ;
		SUM(ctnqty) AS ctnqty ;
		FROM gen856_out ;
		GROUP BY ctnrange,STYLE,COLOR,ID,packnum,units ;
		ORDER BY ctnrange,STYLE,COLOR,id_b,packnum,units	;
		INTO CURSOR gen856_grp READWRITE

*	ASSERT .F. MESSAGE "At Pack Line..."
	SELECT gen856_grp
	DELETE FOR EMPTY(ALLTRIM(gen856_grp.ctnrange))
	LOCATE
	PUBLIC m.ctnqty
	SCAN
		SCATTER MEMVAR
		SELECT gen856_out
		LOCATE
		LOCATE FOR gen856_out.ctnrange = UPPER(m.ctnrange) ;
			AND UPPER(gen856_out.STYLE) = UPPER(m.style) ;
			AND UPPER(gen856_out.COLOR) = UPPER(m.color) ;
			AND UPPER(gen856_out.ID) == UPPER(m.id) ;
			AND gen856_out.PACK = m.pack ;
			AND gen856_out.units = m.units ;
			AND !DELETED()

		IF FOUND()
			GATHER MEMVAR FIELD ctnqty
			m.ctnqty = 0
			WAIT WINDOW TRIM(STYLE)+" "+TRIM(COLOR)+" "+TRIM(ID)+" "+TRIM(PACK) NOWAIT NOCLEAR
			IF !EOF() AND nRecs>1 AND !lCartonAcct
				SELECT gen856_grp
				SKIP
				SCATTER MEMVAR FIELD ctnqty
				SELECT gen856_out
				nRecs=RECCOUNT()
				IF !EOF()
					SKIP
					GATHER MEMVAR FIELD ctnqty
				ENDIF
			ENDIF

			m.ctnqty = 0
			IF !EOF()
				SKIP
			ELSE
				EXIT
			ENDIF
			lContinue = .T.
			DO WHILE lContinue
				CONTINUE
				IF FOUND()
					IF lCartonAcct
						DELETE NEXT 1
					ELSE
						DELETE NEXT 2
					ENDIF
				ELSE
					lContinue = .F.
				ENDIF
			ENDDO
			WAIT CLEAR
		ENDIF
	ENDSCAN
	SELECT gen856_out
*		ALTER TABLE gen856_out DROP COLUMN packnum
	LOCATE
	nCount = 1
	SCAN
		REPLACE grouping WITH ALLTRIM(STR(nCount))
		IF !lCartonAcct
			SKIP
			REPLACE grouping WITH ALLTRIM(STR(nCount))
			nCount = nCount + 1
		ENDIF
	ENDSCAN
	REPLACE ALL ID WITH STRTRAN(ID,"00","") FOR "MIX"$ID
	LOCATE
*	ASSERT .F. MESSAGE "At GEN856_OUT Final browse...debug"
*	BROWSE
	LOCATE
	SUM ctnqty TO lnWOCtns FOR units = .F.
	LOCATE
	SUM ctnqty TO lnWOUnits FOR units = .T.

	m.date_rcvd=DATE()
	m.plinqty=0

*	now create the work order
	WAIT WINDOW AT 10,10 "Generic Inbound.... Creating the work order........." NOWAIT
	IF USED('gwos')
		USE IN gwos
	ENDIF
	xsq1 = [select * from inwolog where accountid = ]+TRANSFORM(xacctnum)
	xsqlexec(xsq1,"gwos",,"wh")
	SELECT gwos
*	ASSERT .f. message "At GWOS insertion...debug"
	SCAN
		IF gwos.CONTAINER = lcContainer
			SELECT oldwos
			APPEND BLANK
			REPLACE oldwos.wo_num    WITH gwos.wo_num  IN oldwos
			REPLACE oldwos.wo_date   WITH gwos.wo_date IN oldwos
			REPLACE oldwos.filename  WITH xfile        IN oldwos
			REPLACE oldwos.CONTAINER WITH gwos.CONTAINER IN oldwos
		ENDIF
	ENDSCAN


	SELECT inwolog
	SCATTER MEMVAR BLANK
	m.addby='WMSIN'
	m.adddt = {}
	m.adddt=DATETIME()
	m.accountid=xacctnum
	m.acctname=TRIM(xacctname)
	m.wo_num = dygenpk("wonum",cWhseMod)
	m.refer_num = whrefnum(m.accountid)
	m.wo_date=DATE()
	m.office=cOffice
	m.acct_ref= lcAcctRef
	m.reference = lcReference
	m.container = lcContainer

	IF lHoldFile
		ASSERT .F. MESSAGE "debug here, too."
	ENDIF
	m.mod = cMod
	insertinto("inwolog","wh",.T.)
	m.quantity = 0
	SELECT gen856_out
	LOCATE
	IF lHoldFile OR (lTest AND xacctnum = 5865)
		BROWSE
		COPY TO c:\tempfox\gen856_out
		ASSERT .F. MESSAGE "At ROLLUP determination"
		IF xacctnum = 5865
			DO rollup_prepack_tko
		ENDIF
	ENDIF


	CREATE CURSOR sm_err (STYLE c(20),COLOR c(10),ID c(10))
	SELECT 0

	useca("upcmast","wh",,,,,"upcmastsql")
	lcQuery = [select * from upcmast where accountid = ]+TRANSFORM(nAcctNum)
	xsqlexec(lcQuery,"upcmast",,"wh")

	SELECT gen856_out
	SCAN
		m.style   = UPPER(ALLTRIM(gen856_out.STYLE))
		m.color   = UPPER(ALLTRIM(gen856_out.COLOR))
		m.id      = UPPER(ALLTRIM(gen856_out.ID))
		IF nAcctNum = 6137
			IF LEN(m.id)=1 AND ISDIGIT(m.id)
				ASSERT .F. MESSAGE "At single-digit size code for LP"
				m.id = PADL(m.id,2,"0")
			ENDIF
		ENDIF
		cSeekStr = STR(nAcctNum,4)+PADR(m.style,20)+PADR(m.color,10)+PADR(m.id,10)
		IF !SEEK(cSeekStr,'upcmast','stylecolid')
			INSERT INTO sm_err FROM MEMVAR
		ENDIF
	ENDSCAN
	SELECT sm_err
	LOCATE
	IF !EOF()
		COPY FILE [&xfile] TO [&archivefile]
		DELETE FILE [&xfile]
		smerrmail()
		CLOSE DATA ALL
		CANCEL
		RETURN
	ENDIF

	SELECT gen856_out
	SCAN
		m.totqty  = gen856_out.ctnqty
		m.units   = gen856_out.units
		m.po      = ALLTRIM(gen856_out.grouping)
		IF nAcctNum = 6035
			m.po = ""
		ENDIF
		m.style   = UPPER(ALLTRIM(gen856_out.STYLE))
		m.color   = UPPER(ALLTRIM(gen856_out.COLOR))
		m.id      = UPPER(ALLTRIM(gen856_out.ID))

		m.pack    = ALLTRIM(gen856_out.PACK)
		m.inwologid = inwolog.inwologid
		IF lDoPL
			m.mod = cMod
			m.office = cOffice
			insertinto("pl","wh",.T.)
		ELSE
			m.plid = dygenpk("pl",cWhseMod)
			INSERT INTO pl FROM MEMVAR
		ENDIF

		IF lCayset AND !lTest
			REPLACE pl.cayset WITH UPPER(pl.ID) NEXT 1 IN pl
*			BLANK FIELDS pl.ID NEXT 1 IN pl
		ENDIF
	ENDSCAN
	USE IN gen856_out

	SELECT inwolog
	REPLACE plinqty  WITH lnWOCtns IN inwolog
	REPLACE plunitsinqty WITH lnWOUnits IN inwolog
	REPLACE unitsinqty WITH 0 IN inwolog
	REPLACE REFERENCE WITH UPPER(lcReference) IN inwolog
	REPLACE acct_ref  WITH UPPER(lcAcctRef) IN inwolog
	REPLACE brokerref WITH UPPER(lcBrokerref) IN inwolog
	REPLACE comments  WITH UPPER(lcComments) IN inwolog
	REPLACE CONTAINER WITH UPPER(lcContainer) IN inwolog
	REPLACE Seal WITH UPPER(lcSeal) IN inwolog
	REPLACE printcomments WITH UPPER(lcPrintComments) IN inwolog

	IF lHoldFile
		WAIT WINDOW "Browsing INWOLOG for Container info (test mode)"
		BROWSE
	ENDIF

	SCATTER MEMVAR

	m.filename  = xfile
	m.container = UPPER(lcContainer)
	m.quantity  = lnWOCtns
	m.sorts     = lnTotSorts

	INSERT INTO wos FROM MEMVAR
	IF lBrowse
		SELECT inwolog
		GO BOTT
		BROWSE
		SELECT pl
		BROWSE FOR accountid = xacctnum AND date_rcvd = DATE()
	ENDIF

	WAIT WINDOW AT 10,10 "Generic Inbound.... work order created......." TIMEOUT 2
	WAIT CLEAR

	tu("inwolog")
	IF lDoPL
		tu("pl")
	ENDIF

CATCH TO oErr
	ptError = .T.
	tsubject = "Inbd Upload Error ("+TRANSFORM(oErr.ERRORNO)+") for Generic at "+TTOC(DATETIME())+"  on file "+xfile
	tattach  = ""
	SELECT 0
	USE F:\3pl\DATA\mailmaster ALIAS mm
	LOCATE FOR edi_type = "MISC" AND taskname = "GENERAL"
	lUseAlt = mm.use_alt
	tsendto = ALLTRIM(IIF(lUseAlt,mm.sendtoalt,mm.sendto))
	tcc = ALLTRIM(IIF(lUseAlt,mm.ccalt,mm.cc))
	USE IN mm
	tmessage = "Error processing Inbound file for Generic.... Filename: "+xfile+CHR(13)
	IF FILE("gen856_out")
		SELECT gen856_out
		lnRec= RECNO("GEN856_out")
		tmessage = tmessage+CHR(13)+" Error at record "+TRANSFORM(lnRec)+CHR(13)+CHR(13)
	ENDIF
	tmessage = tmessage+STYLE
	tmessage = tmessage+COLOR
	tmessage = tmessage+ID
	tmessage = tmessage+SIZE
	tmessage = tmessage+PACK


	tmessage =tmessage+ "Try/CATCH Exeception Message:"+CHR(13)+;
		[  Error: ] + STR(oErr.ERRORNO) +CHR(13)+;
		[  LineNo: ] + STR(oErr.LINENO) +CHR(13)+;
		[  Message: ] + oErr.MESSAGE +CHR(13)+;
		[  Procedure: ] + oErr.PROCEDURE +CHR(13)+;
		[  Details: ] + oErr.DETAILS +CHR(13)+;
		[  StackLevel: ] + STR(oErr.STACKLEVEL) +CHR(13)+;
		[  LineContents: ] + oErr.LINECONTENTS+CHR(13)+;
		[  UserValue: ] + oErr.USERVALUE+CHR(13)+;
		[  Machine Name: ] + SYS(0)+CHR(13)+;
		[  Error Message :]+lcErrorMessage+CHR(13)

	tsubject = "Inbound WO Error for Generic Inbound at "+TTOC(DATETIME())+"  on file "+xfile
	tattach  = ""
	tsendto  = IIF(lHoldFile OR lTest,tcctest,"edihelpdesk@fmiint.com")
	DO FORM m:\dev\frm\dartmail2 WITH tsendto,tFrom,tsubject,tcc,tattach,tmessage,"A"
	lMainMail = .F.
ENDTRY

IF USED('GEN856_IN')
	USE IN GEN856_in
ENDIF
ENDPROC

***************************
PROCEDURE NanjingInbound
***************************
PARAMETERS xfile,xacctnum,xacctname,cOffice
cMsg = "At start of NanjingInbound procedure"
WAIT WINDOW cMsg TIMEOUT 2
cMsg = cMsg + "...debug here"
*	ASSERT .F. MESSAGE cMsg


TRY
	IF USED("generatepk")	&& dy 11/18/09
		USE IN generatepk
	ENDIF
	IF USED('wolog')
		USE IN wolog
	ENDIF

	COPY FILE [&xfile] TO [&archivefile]

	USE F:\wo\wodata\wolog IN 0 ALIAS wolog

	IF EMPTY(goffice) AND lTest OR lHoldFile
		goffice = "1"
	ELSE
		goffice = cOffice
	ENDIF
	STORE "" TO m.style,m.color,m.id,m.size,m.pack

	IF USED('wos')
		USE IN wos
	ENDIF
	CREATE CURSOR wos ( ;
		filename   c(100),;
		wo_num     N(15),;
		CONTAINER  c(18), ;
		sku_units  N(6),;
		cartons    N(6),;
		quantity   N(10), ;
		sorts      N(6),;
		season     c(25),;
		computer   c(1),;
		packtype   c(3))

	CREATE CURSOR INX ( ;
		a  c(20),;
		b  c(20),;
		c  c(20),;
		d  c(30),;
		e  c(20),;
		F  c(20),;
		g  c(20),;
		h  i,;
		i  l)

	lStyleMasterError = .F.
	SELECT tempmain
	LOCATE
	talias=ALIAS()

	SELECT &talias
*		ASSERT .F. MESSAGE "At Carton Range line determination...>>DEBUG<<"
	LOCATE FOR "CARTON RANGE"$UPPER(&talias..a)
	IF FOUND()
		DetRec1 = RECNO()
	ELSE
		WAIT WINDOW "Nanjing inbd CARTON RANGE line missing" TIMEOUT 4
		lnormalexit = .F.
		THROW
	ENDIF

	DELETE FOR (EMPTY(a) AND EMPTY(b) AND EMPTY(c) AND EMPTY(d) AND RECNO()>(DetRec1+1))
	SCAN
		SELECT INX
		APPEND BLANK
		REPLACE a WITH ALLTRIM(TRANSFORM(&talias..a))
		REPLACE b WITH ALLTRIM(TRANSFORM(&talias..b))
		REPLACE c WITH ALLTRIM(TRANSFORM(&talias..c))
		REPLACE d WITH ALLTRIM(TRANSFORM(&talias..d))
		REPLACE e WITH ALLTRIM(TRANSFORM(&talias..e))
		IF RECNO()>DetRec1
			lcRange = ALLTRIM(&talias..a)
			IF "-"$lcRange
				REPLACE F WITH ALLTRIM(LEFT(lcRange,AT("-",lcRange)-1))
				REPLACE g WITH ALLTRIM(SUBSTR(lcRange,AT("-",lcRange)+1))
			ELSE
				REPLACE F WITH lcRange,g WITH lcRange
			ENDIF
			f1 = INT(VAL(F))
			g1 = INT(VAL(g))
			REPLACE h WITH g1-f1+1
		ENDIF
	ENDSCAN

	SELECT INX
	IF lHoldFile OR lTest
		LOCATE
*			BROWSE
	ENDIF
	LOCATE


*!* Added this to correct carton ranges.
	SCAN FOR RECNO()>DetRec1
		IF RECNO()=(DetRec1+1)
			REPLACE F WITH "1"
			REPLACE g WITH ALLTRIM(STR(INX.h))
			nStart = h+1
		ELSE
			REPLACE F WITH ALLTRIM(STR(nStart))
			REPLACE g WITH ALLTRIM(STR(nStart+h-1))
			nStart = INT(VAL(g))+1
		ENDIF
		REPLACE a WITH ALLTRIM(F)+"-"+ALLTRIM(g)
	ENDSCAN

	ALTER TABLE INX DROP COLUMN h

	lnCtr=1
	STORE "" TO lcReference,lcComments,lcAcctRef,lcSeal,lcBrokerref,lcPOnum,ShipDate
	STORE "" TO lcContainer,lcArrivePort,lcPrintComments

	DO WHILE RECNO()<DetRec1
		DO CASE
		CASE "ACCOUNT REF"$a
			lcAcctRef = UPPER(ALLTRIM(b))

		CASE "BROKER REF"$a
			lcBrokerref = UPPER(ALLTRIM(b))

		CASE "VESSEL/AWB"$a
			lcReference = UPPER(ALLTRIM(b))

		CASE "CONTAINER"$a
			lcContainer = UPPER(ALLTRIM(b))

		CASE UPPER(ALLTRIM(a))="COMMENTS"
			lcComments = UPPER(ALLTRIM(b))

		CASE "SEAL"$a
			lcSeal = UPPER(ALLTRIM(b))

		CASE "PRINT COMMENTS"$a
			lcPrintComments = UPPER(ALLTRIM(b))
		ENDCASE
		SKIP 1 IN INX
	ENDDO

	DO WHILE " "$lcContainer
		lcContainer = STRTRAN(lcContainer," ","")
	ENDDO
	lcContainer = STRTRAN(lcContainer,"-","")


	WAIT WINDOW AT 10,10 "Nanjing Inbound.... Processing............" NOWAIT

	SELECT INX
	LOCATE
	GOTO (DetRec1+1)

	lStyleMasterError = .F.
	ASSERT .F. MESSAGE "At Nanjing Style scan"
	IF DATETIME()<DATETIME(2010,11,12,15,45,00)
		SET DELETED OFF
	ENDIF
	useca("upcmast","wh",,,,,"upcmastsql")
	APPEND BLANK
	SCATTER MEMVAR MEMO BLANK
	DELETE NEXT 1 IN upcmastsql
	xsqlexec("select * from upcmast where accountid in (&gnanjingaccounts)","upcmast",,"whall")

	SELECT INX
	SCAN FOR RECNO()>DetRec1 AND !EMPTY(ALLTRIM(a))
		REPLACE INX.i WITH IIF(".COM"$UPPER(INX.c),.T.,.F.) IN INX
		lDotCom = IIF(".COM"$UPPER(INX.c),.T.,.F.)
		cStyle1 = ALLTRIM(INX.b)
		m.accountid = nAcctNum
		lUpper = .T.
		SELECT upcmast
		LOCATE
		LOCATE FOR upcmast.STYLE = PADR(UPPER(ALLT(cStyle1)),20)
		IF !FOUND()
			SET STEP ON
			lUpper = .F.
			LOCATE FOR upcmast.STYLE = LOWER(cStyle1)
			IF !FOUND()
				lStyleMasterError = .T.
				cErrString = "STYLE "+TRIM(cStyle1)+" NOT FOUND IN STYLEMASTER"
				IF EMPTY(lcErrorMessage)
					lcErrorMessage = CHR(13) + CHR(13) + "The following Styles had errors: " + CHR(13) + cErrString
				ELSE
					lcErrorMessage = lcErrorMessage + CHR(13) + CHR(13) + cErrString
				ENDIF
			ENDIF
		ELSE
			SELECT upcmast
			LOCATE
			COUNT TO nsCount FOR upcmast.STYLE = PADR(IIF(lUpper,UPPER(cStyle1),cStyle1),20) AND INLIST(upcmast.accountid,4610,4694)
			LOCATE
			lDual = IIF(nsCount=1,.F.,.T.)
*				SET STEP ON
			IF INX.i = .T.
				chkacct = 4694
				chkacct2 = 4694
			ELSE
				chkacct = 4610
				chkacct2 = 4694
			ENDIF
			LOCATE FOR upcmast.STYLE = PADR(IIF(lUpper,UPPER(cStyle1),cStyle1),20) AND upcmast.accountid = chkacct
			IF !FOUND()
				IF lDotCom
					ASSERT .F. MESSAGE "At dot.com order"
					LOCATE
					LOCATE FOR upcmast.STYLE = PADR(IIF(lUpper,UPPER(cStyle1),cStyle1),20) AND upcmast.accountid = 4610
					SCATTER FIELDS EXCEPT upcmastid MEMVAR MEMO
					m.accountid = 4694
					m.updatedate = DATETIME()
					m.upcmastid=sqlgenpk("upcmast","wh")
					m.pnp = .F.
*						INSERT INTO upcmast FROM MEMVAR
					INSERT INTO upcmastsql FROM MEMVAR
				ENDIF

				LOCATE FOR upcmast.STYLE = PADR(IIF(lUpper,UPPER(cStyle1),cStyle1),20) AND upcmast.accountid = chkacct2
				IF !FOUND()
					lStyleMasterError = .T.
					cErrString = "STYLE "+TRIM(cStyle1)+" IN STYLEMASTER NOT FOUND FOR NANJING ACCOUNT"
					IF EMPTY(lcErrorMessage)
						lcErrorMessage = CHR(13) + CHR(13) + "The following Styles had errors: " + CHR(13) + cErrString
					ELSE
						lcErrorMessage = lcErrorMessage + CHR(13) + CHR(13) + cErrString
					ENDIF
				ELSE
					xacctnum = upcmast.accountid
					REPLACE INX.c WITH IIF(xacctnum=4610,"PIK","PRE") NEXT 1 IN INX
				ENDIF
			ELSE
				xacctnum = upcmast.accountid
				REPLACE INX.c WITH IIF(xacctnum=4610,"PIK","PRE") NEXT 1 IN INX
			ENDIF
		ENDIF
	ENDSCAN
	tu("upcmastsql")

	SET DELETED ON
	SELECT INX

	LOCATE
	GOTO (DetRec1+1)
	SCAN FOR RECNO()>DetRec1 AND !EMPTY(ALLTRIM(a))
		lcRange = ALLTRIM(a)
		IF EMPTY(F) OR EMPTY(g)
			lcAA = ALLTRIM(LEFT(lcRange,AT("-",lcRange)-1))
			lcBB = ALLTRIM(SUBSTR(lcRange,AT("-",lcRange)+1))
			REPLACE F WITH lcAA
			REPLACE g WITH lcBB
		ENDIF
	ENDSCAN
	LOCATE
	IF lHoldFile
*!*				ASSERT .F. MESSAGE "At INX acct type browse"
		BROW
	ENDIF
	talias=ALIAS()  && tAlias is INX

	SELECT &talias
	LOCATE

*		ASSERT .F. MESSAGE "At nan856_in population...check FOR I loop"
	STORE .F. TO lPre,lPack
	nStart = IIF(lHoldFile,2,1)
	FOR Y = nStart TO 2
		SELECT 0
		CREATE CURSOR nan856_in (ctnrange c(20),;
			grouping c(3), ;
			units  l, ;
			STYLE c(20), ;
			COLOR c(10), ;
			ID    c(30), ;
			SIZE  c(10), ;
			PACK  c(10), ;
			LENGTH N(4), ;
			WIDTH  N(4), ;
			acct_ref c(20), ;
			REFERENCE c(20), ;
			depth  N(4), ;
			ctnwt i, ;
			skuqty i, ;
			ctnqty i, ;
			wmcom l)

		SELECT &talias
		LOCATE

		IF Y = 1
			lPick = .T.
			lPrepack = .F.
			xacctnum = 4610
			cPacktype1 = "PIK"
			xacctname = "NANJING - PICK & PACK"
			lCartonAcct = .F.
		ELSE
			lPick = .F.
			lPrepack = .T.
			xacctnum = 4694
			STORE xacctnum TO nAcctNum,Acctnum
			cPacktype1 = "PRE"
			xacctname = "NANJING - PREPACK"
			lCartonAcct = .T.
		ENDIF
		STORE xacctnum TO nAcctNum,Acctnum
		WAIT WINDOW "Processing Acct: "+xacctname TIMEOUT 2

		IF lCartonAcct
			lnGroup = 1
		ELSE
			lnGroup = 0
		ENDIF

		SELECT &talias
		LOCATE
		IF lHoldFile OR DATE() = {^2015-07-09}
			WAIT WINDOW "Browsing file INX" TIMEOUT 2
			BROW
		ENDIF
		LOCATE FOR ((&talias..c) = cPacktype1)
		IF !FOUND()
			WAIT WINDOW "No INX records of type "+cPacktype1+" found...looping" TIMEOUT 2
			LOOP
		ENDIF
		IF cPacktype1 = "PRE"
			lPre = .T.
		ENDIF
		IF cPacktype1 = "PIK"
			lPik = .T.
		ENDIF
		SCAN FOR ((&talias..c) = cPacktype1)
			IF !lCartonAcct
				lnGroup = lnGroup+1
			ENDIF
			lMixRec = .F.

			SELECT nan856_in
			APPEND BLANK
			cAField = ALLTRIM(&talias..a)
			REPLACE nan856_in.ctnrange WITH ALLTRIM(cAField)

			IF "-"$cAField
				STORE cAField TO lcCartonRange
				lDoMix = IIF("MIX"$&talias..d,.T.,.F.)
			ELSE
				SELECT &talias
				nRec1=RECNO()
				cA1 = ALLT(&talias..a)
				cB1 = ALLT(&talias..b)
				cE1 = ALLT(&talias..e)
				COUNT TO nCount1 FOR (&talias..a) = cA1 AND (&talias..b) = cB1 ;
					AND (&talias..e) = cE1
				IF nCount1>1
					SUM VAL(&talias..e) TO nTotq FOR (&talias..a) = cA1 AND (&talias..b) = cB1 ;
						AND (&talias..e) = cE1
					cTotq = ALLTRIM(STR(INT(nTotq)))
					GO nRec1
					SELECT nan856_in
					lcCartonRange = cAField+"-"+cAField
					lMixRec = .T.
				ELSE
					GO nRec1
					SELECT nan856_in
					lcCartonRange = cAField+"-"+cAField
				ENDIF
			ENDIF

			REPLACE nan856_in.units WITH .F.
			REPLACE nan856_in.grouping WITH ALLTRIM(STR(lnGroup))
			IF lDoMix
				cGroup = nan856_in.grouping
			ENDIF
			lcStyle = UPPER(ALLTRIM(&talias..b))

*				ASSERT .F. MESSAGE "At UPCMast pack type check...debug"
			SELECT upcmast
			LOCATE
			LOCATE FOR STYLE= IIF(lUpper,UPPER(cStyle1),cStyle1) AND accountid = xacctnum
			IF FOUND()
				IF (upcmast.pnp AND lPrepack) OR (!upcmast.pnp AND !lPrepack)
					lStyleMasterError = .T.
					cErrString = "STYLE "+TRIM(cStyle1)+" IN STYLEMASTER DOES NOT MATCH PACK TYPE"
					IF EMPTY(lcErrorMessage)
						lcErrorMessage = CHR(13) + CHR(13) + "The following Styles had errors: " + CHR(13) + cErrString
					ELSE
						lcErrorMessage = lcErrorMessage + CHR(13) + CHR(13) + cErrString
					ENDIF
				ENDIF
			ENDIF
			IF lStyleMasterError
				cFilename = ALLTRIM(JUSTFNAME(xfile))
				COPY FILE [&xfile] TO ("F:\0-PACKINGLIST\NANJING\HOLD\"+cFilename)
				DELETE FILE [&xfile]
				WAIT WINDOW "STYLE ERROR(s)...aborting" TIMEOUT 4
				lnormalexit = .F.
				THROW
			ENDIF
			SELECT nan856_in
			REPLACE nan856_in.STYLE WITH lcStyle
			IF (lMixRec AND !lCartonAcct)
				REPLACE PACK WITH cTotq
			ELSE
				REPLACE PACK WITH ALLTRIM(&talias..e)
			ENDIF

			lnPackVal = VAL(ALLTRIM(PACK))
			lnCtnQty = (VAL(ALLTRIM(&talias..g)) - VAL(ALLTRIM(&talias..F)))+1
			REPLACE nan856_in.ctnqty WITH (lnCtnQty) IN nan856_in
			REPLACE nan856_in.wmcom WITH &talias..i IN nan856_in
		ENDSCAN

		SELECT nan856_in
		IF lHoldFile
			WAIT WINDOW "Browsing INX and NAN856_IN files, pre-rollup" TIMEOUT 2
			SELECT &talias
			BROWSE
			SELECT nan856_in
			BROWSE
			ASSERT .F.
		ENDIF
		SCAN FOR !DELETED()
			SCATTER FIELDS STYLE,PACK,wmcom MEMVAR
			nRec = RECNO()
			LOCATE FOR nan856_in.STYLE = m.style AND nan856_in.PACK = m.pack AND nan856_in.wmcom = m.wmcom AND RECNO()>nRec
			IF FOUND()
				nCtnQty = nan856_in.ctnqty
				DELETE NEXT 1
				GO nRec
				REPLACE nan856_in.ctnqty WITH nan856_in.ctnqty+nCtnQty NEXT 1
			ELSE
				GO nRec
			ENDIF
		ENDSCAN

		IF lHoldFile
			WAIT WINDOW "Browsing NAN856_IN file, post-rollup" TIMEOUT 2
			BROWSE
		ENDIF

		lnTotSorts = lnGroup
		lnTotPairs = 0
		lnTotCtns = 0
		lnTotWt = 0
		lnTotCube= 0

		DELETE FOR EMPTY(nan856_in.ctnrange)
		DELETE FOR EMPTY(nan856_in.grouping)
		LOCATE
		IF EOF()
			LOOP
		ENDIF

		IF lHoldFile AND !lCartonAcct
			COPY TO c:\tempfox\nan856_in TYPE XL5
*				ASSERT .F. MESSAGE "At begin data transfer from nan856_in to nan856_out"
		ENDIF

		SELECT 0
		SELECT *,SPACE(10) AS packnum,00000 AS numgrp FROM nan856_in INTO CURSOR nan856_out READWRITE
		SELECT nan856_out
		LOCATE
		IF EOF()
			WAIT WINDOW "EMPTY nan856_OUT cursor...aborting" TIMEOUT 4
			lnormalexit = .F.
			THROW
		ENDIF

		nRecs = RECCOUNT()
		SCAN
			STORE PACK TO m.packnum
			SELECT nan856_out
			GATHER MEMVAR FIELDS packnum
		ENDSCAN
		USE IN nan856_in

		SELECT STYLE,COLOR,ID,PACK,units,IIF(LEN(TRIM(ID))<3,PADL(TRIM(ID),3,"0"),ID) AS id_b,packnum, ;
			SUM(ctnqty) AS ctnqty ;
			FROM nan856_out ;
			GROUP BY STYLE,COLOR,ID,packnum,units ;
			ORDER BY STYLE,COLOR,id_b,packnum,units	;
			INTO CURSOR nan856_grp READWRITE

		SELECT nan856_grp
		LOCATE
		PUBLIC m.ctnqty
		ASSERT .F. MESSAGE "At Style/Pack consolidation...DEBUG HERE"
		SCAN FOR !DELETED()
			SCATTER MEMVAR
			SELECT nan856_out
			LOCATE
			LOCATE FOR UPPER(nan856_out.STYLE) = UPPER(m.style) ;
				AND UPPER(nan856_out.COLOR) = UPPER(m.color) ;
				AND UPPER(nan856_out.ID) == UPPER(m.id) ;
				AND nan856_out.PACK = m.pack ;
				AND nan856_out.wmcom = m.wmcom ;
				AND !DELETED()

			IF FOUND()
				nRecLoc = RECNO()
				WAIT WINDOW TRIM(nan856_out.STYLE)+" "+TRIM(nan856_out.PACK) NOWAIT NOCLEAR
				SCAN FOR UPPER(nan856_out.STYLE) = UPPER(m.style) ;
						AND UPPER(nan856_out.COLOR) = UPPER(m.color) ;
						AND UPPER(nan856_out.ID) = UPPER(m.id) ;
						AND nan856_out.PACK = m.pack ;
						AND nan856_out.wmcom = m.wmcom ;
						AND !DELETED() ;
						AND RECNO()>nRecLoc
					nRecLoc2 = RECNO()
					STORE nan856_out.ctnqty TO nCtnQty
					GO nRecLoc
					REPLACE nan856_out.ctnqty WITH nan856_out.ctnqty+nCtnQty IN nan856_out
					GO nRecLoc2
					DELETE NEXT 1 IN nan856_out
				ENDSCAN
				WAIT CLEAR
			ENDIF
			SELECT nan856_grp
		ENDSCAN
		SELECT nan856_out
		ALTER TABLE nan856_out DROP COLUMN packnum
		LOCATE
		nCount = 1
		SCAN
			REPLACE grouping WITH ALLTRIM(STR(nCount))
			nCount = nCount + 1
		ENDSCAN
		INDEX ON grouping TAG grouping
		REPLACE ALL nan856_out.ID WITH STRTRAN(ID,"00","") FOR "MIX"$nan856_out.ID
		REPLACE ALL nan856_out.COLOR WITH ".COM" FOR nan856_out.wmcom = .T.
		LOCATE
		SUM nan856_out.ctnqty TO lnWOCtns FOR nan856_out.units = .F.
		LOCATE
		SET ORDER TO TAG grouping
		IF lHoldFile OR lTest
			IF lHoldFile AND lPre
				WAIT WINDOW "Browsing Nan856_out for '.COM' color" TIMEOUT 5
				BROWSE
			ENDIF
			COPY TO c:\tempfox\nan856_out FOR !DELETED()
		ENDIF
		LOCATE
		ASSERT .F.
		IF lPik
			SCAN FOR !DELETED()
				nRecLoc = RECNO()
				REPLACE nan856_out.numgrp WITH INT(VAL(nan856_out.grouping)) IN nan856_out NEXT 1
				SCATTER MEMVAR
				m.units = .T.
				m.ctnqty = INT(VAL(m.pack))*m.ctnqty
				m.pack = "1"
				m.numgrp = INT(VAL(m.grouping))
				APPEND BLANK
				GATHER MEMVAR
			ENDSCAN
		ENDIF
		IF lHoldFile
			WAIT WINDOW "At post ctnqty replacement...browsing NAN856_OUT final" TIMEOUT 3
			ASSERT .F. MESSAGE "DEFINITELY PAY ATTENTION HERE...DEBUG!"
			SELECT nan856_out
			LOCATE
			BROWSE FIELDS STYLE,COLOR,PACK,ctnqty,wmcom
		ENDIF
		SUM ctnqty TO lnWOUnits FOR units = .T.

		m.date_rcvd=DATE()
		m.plinqty=0

*	now create the work order
		WAIT WINDOW AT 10,10 "Nanjing Inbound.... Creating the work order........." NOWAIT
		IF USED('gwos')
			USE IN gwos
		ENDIF
		csq1 = [select * from inwolog where accountid = ]+TRANSFORM(xacctnum)
		xsqlexec(csq1,"gwos",,"wh")
		SELECT gwos
*	ASSERT .f. message "At GWOS insertion...debug"
		SCAN
			IF gwos.CONTAINER = lcContainer
				SELECT oldwos
				APPEND BLANK
				REPLACE oldwos.wo_num    WITH gwos.wo_num  IN oldwos
				REPLACE oldwos.wo_date   WITH gwos.wo_date IN oldwos
				REPLACE oldwos.filename  WITH xfile        IN oldwos
				REPLACE oldwos.CONTAINER WITH gwos.CONTAINER IN oldwos
			ENDIF
		ENDSCAN

		SELECT inwolog
		inrec = RECNO()
		csq1 = [select * from pt where accountid = ]+TRANSFORM(xacctnum)+[ and container = ']+lcContainer+[' and ptdate > {]+DTOC(DATE()-5)+[}]
		xsqlexec(csq1,"i1",,"wh")
		LOCATE
		IF !EOF()
			SET STEP ON
			WAIT WINDOW "Container "+lcContainer+" already exists for Acct# "+ALLT(STR(xacctnum)) TIMEOUT 3
			GO inrec
			COPY FILE [&xfile] TO [&archivefile]
			DELETE FILE [&xfile]
			lnormalexit = .T.
			THROW
		ENDIF
		SELECT inwolog

		LOCATE

		SCATTER MEMVAR BLANK
		m.addby='WMSIN'
		m.adddt = {}
		m.adddt=DATETIME()

		m.accountid=xacctnum
		m.acctname=TRIM(xacctname)
		cWH = ("wh"+LOWER(IIF(cOffice="N","I",cOffice)))
		m.wo_num=dygenpk("wonum",cWH)
*		m.wo_num=genpk("wonum","wh",,,.T.,,.T.)
		cWO_Num = ALLTRIM(STR(m.wo_num))
		m.refer_num=whrefnum(m.accountid)
		m.wo_date=DATE()
		m.office=cOffice
		m.acct_ref= lcAcctRef
		m.reference = lcReference
		m.mod = cMod
		m.office = cOffice
		insertinto("inwolog","wh",.T.)
		m.quantity = 0
		SELECT nan856_out
*				rollup_nan()

		LOCATE

		ASSERT .F. MESSAGE "At insertion into PL"
		SELECT * FROM nan856_out ;
			ORDER BY STYLE,PACK,units  ;
			INTO CURSOR nan856_out2
		SELECT nan856_out2
		IF lHoldFile
			WAIT WINDOW "Browsing NAN856_OUT2 sorted final" TIMEOUT 3
			LOCATE
			BROWSE
		ENDIF
		SCAN FOR !DELETED()
			m.totqty  = nan856_out2.ctnqty
			m.units   = nan856_out2.units
			m.po      = ALLTRIM(nan856_out2.grouping)
			m.style   = UPPER(ALLTRIM(nan856_out2.STYLE))
			m.color   = UPPER(ALLTRIM(nan856_out2.COLOR))
			m.id      = UPPER(ALLTRIM(nan856_out2.ID))
			m.pack    = ALLTRIM(nan856_out2.PACK)
			m.inwologid = inwolog.inwologid
			IF lDoPL
				m.office = cOffice
				m.mod = cMod
				insertinto("pl","wh",.T.)
			ELSE
				m.plid = dygenpk("pl",cWhseMod)
				INSERT INTO pl FROM MEMVAR
			ENDIF
		ENDSCAN

		USE IN nan856_out
		USE IN nan856_out2

		SELECT inwolog
		REPLACE plinqty  WITH lnWOCtns IN inwolog
		REPLACE unitsinqty WITH 0 IN inwolog
		REPLACE REFERENCE WITH UPPER(lcReference) IN inwolog
		REPLACE acct_ref  WITH UPPER(lcAcctRef) IN inwolog
		REPLACE brokerref WITH UPPER(lcBrokerref) IN inwolog
		REPLACE comments  WITH UPPER(lcComments) IN inwolog
		REPLACE CONTAINER WITH UPPER(lcContainer) IN inwolog
		REPLACE Seal WITH UPPER(lcSeal) IN inwolog
		REPLACE printcomments WITH UPPER(lcPrintComments) IN inwolog

		SCATTER MEMVAR

		m.filename  = xfile
		m.container = UPPER(lcContainer)
		m.quantity  = lnWOCtns
		m.sorts     = lnTotSorts
		m.packtype  = cPacktype1
		INSERT INTO wos FROM MEMVAR

		lFinalType = IIF(lPre,"Prepack","Pickpack")
		STORE .F. TO lPre,lPik
		WAIT WINDOW AT 10,10 "Nanjing Inbound "+lFinalType+" work order: "+cWO_Num+" created" TIMEOUT 2
		WAIT CLEAR

		IF lBrowse OR lHoldFile
			ASSERT .F. MESSAGE "At Inwolog/PL table browse"
			SELECT inwolog
			GO BOTT
			BROWSE
			SELECT pl
			BROWSE FOR inwologid = inwolog.inwologid AND accountid = xacctnum && AND date_rcvd = DATE()
		ENDIF

*!* Added below 02.19.2014 per Darren
		SELECT inwolog
		STORE "" TO xtruckwonum, xtruckseal
		DO "m:\dev\prg\whtruckwonum.prg" WITH inwolog.wo_num, inwolog.REFERENCE, inwolog.CONTAINER, inwolog.accountid
		REPLACE inwolog.truckwonum WITH xtruckwonum, inwolog.Seal WITH xtruckseal IN inwolog

		ASSERT .F. MESSAGE "At end of PRE/PIK Loop...debug"
		tu("inwolog")
		IF lDoPL
			tu("pl")
		ENDIF
	ENDFOR


CATCH TO oErr
	SET STEP ON
	ptError = .T.
	tmessage = "Error processing Inbound file for Nanjing.... Filename: "+xfile+CHR(13)
	IF FILE("nan856_out")
		SELECT nan856_out
		lnRec= RECNO("nan856_out")
		tmessage = tmessage+CHR(13)+" Error at record "+TRANSFORM(lnRec)+CHR(13)+CHR(13)
	ENDIF
	tmessage = tmessage+STYLE
	tmessage = tmessage+PACK
	IF EMPTY(lcErrorMessage)
		tmessage =tmessage+ "Try/CATCH Exeception Message:"+CHR(13)+;
			[  Error: ] + STR(oErr.ERRORNO) +CHR(13)+;
			[  Procedure: ] + oErr.PROCEDURE +CHR(13)+;
			[  LineNo: ] + STR(oErr.LINENO) +CHR(13)+;
			[  Message: ] + oErr.MESSAGE +CHR(13)+;
			[  Details: ] + oErr.DETAILS +CHR(13)+;
			[  StackLevel: ] + STR(oErr.STACKLEVEL) +CHR(13)+;
			[  LineContents: ] + oErr.LINECONTENTS+CHR(13)+;
			[  UserValue: ] + oErr.USERVALUE+CHR(13)+;
			[  Machine Name: ] + SYS(0)+CHR(13)
	ELSE
		tmessage ="There is an error as follows:"+CHR(13)+lcErrorMessage+CHR(13)
	ENDIF

	tsubject = "Inbound WO Error for Nanjing Inbound at "+TTOC(DATETIME())+"  on file "+xfile
	tattach  = ""
	SELECT 0
	USE F:\3pl\DATA\mailmaster ALIAS mm SHARED
	lUseAlt = IIF(mm.use_alt,.T.,.F.)
	LOCATE FOR edi_type = "MISC" AND taskname = "GENERR"
	tsendto  = ALLTRIM(IIF(lUseAlt,mm.sendtoalt,mm.sendto))
	IF lStyleMasterError AND (!lTest AND !lTestmail)
		LOCATE
		LOCATE FOR edi_type = "MISC" AND taskname = "NANSTYLEINB"
		lUseAlt = IIF(mm.use_alt,.T.,.F.)
		tcc = ALLTRIM(IIF(lUseAlt,mm.ccalt,mm.cc))
	ELSE
		tcc = ""
	ENDIF

	USE IN mm
	DO FORM m:\dev\frm\dartmail2 WITH tsendto,tFrom,tsubject,tcc,tattach,tmessage,"A"
	STORE "" TO tsendto,tcc
	IF !lHoldFile
		DELETE FILE [&xfile]
	ENDIF
	lMainMail = .F.
	lnormalexit = .T.
	THROW
FINALLY
	DELETE FILE (lcpath+"*.csv")

	IF USED('nan856_IN')
		USE IN nan856_in
	ENDIF
ENDTRY

ENDPROC


*************************
PROCEDURE rollup_nan
*************************
*!* Rolls up post-creation Nanjing prepack pre-PL data by style/pack. Modified from Darren's code.
SELECT nan856_out
LOCATE
*	nNanRuns = IIF(lPik,2,1)
ASSERT .F. MESSAGE "In Nanjing roll-up ("+IIF(lPik,"PIK","PRE")+")"
FOR nanloops = 1 TO 1
	lUnits = IIF(lPik,.T.,.F.)
	SELECT nan856_out
	LOCATE
	SCAN FOR units = lUnits
		IF nanloops = 2 AND lPik
			lUnits = .F.
		ENDIF
		lUnits = nan856_out.units
		xxrange=ctnrange
		SCATTER MEMVAR
		DO WHILE .T.
			LOCATE FOR nan856_out.units=lUnits AND nan856_out.STYLE=m.style AND nan856_out.COLOR=m.color AND nan856_out.ID=m.id ;
				AND nan856_out.PACK=m.pack AND nan856_out.ctnrange#m.ctnrange
			IF !FOUND()
				EXIT
			ENDIF
			xaddqty=ctnqty
			DELETE NEXT 1 IN nan856_out
			LOCATE
			LOCATE FOR ctnrange=xxrange AND units = lUnits
			REPLACE ctnqty WITH ctnqty+xaddqty
			xaddqty = 0
		ENDDO
		LOCATE FOR ctnrange=xxrange AND units = lUnits
	ENDSCAN
ENDFOR

IF lHoldFile
	BROWSE FIELDS STYLE,COLOR,ID,PACK,ctnqty
ENDIF

ENDPROC

***************************
PROCEDURE rollup_pickpack
***************************
*!* Rolls up post-creation XPL data by style/color/id/pack. Modified from Darren's code.

SELECT plid FROM xpl WHERE units GROUP BY po HAVING COUNT(plid)>1 INTO CURSOR xmusical      && don't merge units if there is a musical in the pl

IF RECCOUNT()=0
	SELECT xpl
	SCAN FOR units
		xxplid=plid
		xpo=po
		SCATTER MEMVAR
		DO WHILE .T.
			LOCATE FOR units=m.units AND STYLE=m.style AND COLOR=m.color AND ID=m.id AND PACK=m.pack AND plid#m.plid
			IF !FOUND()
				EXIT
			ENDIF
			xduptotqty=totqty
			xdelpo=po
			DELETE
			LOCATE FOR plid=xxplid
			REPLACE totqty WITH totqty+xduptotqty
		ENDDO
		LOCATE FOR plid=xxplid
	ENDSCAN
ENDIF

ENDPROC

*************************
PROCEDURE rollup_prepack
*************************
*!* Rolls up post-creation XPL data by style/color/id/pack. Modified from Darren's code.

SELECT plid FROM xpl WHERE units GROUP BY po HAVING COUNT(plid)>1 INTO CURSOR xmusical      && don't merge units if there is a musical in the pl

IF RECCOUNT()=0
	SELECT xpl
	SCAN FOR !units
		xxplid=plid
		xpo=po
		SCATTER MEMVAR
		DO WHILE .T.
			LOCATE FOR units=m.units AND STYLE=m.style AND COLOR=m.color AND ID=m.id AND PACK=m.pack AND plid#m.plid
			IF !FOUND()
				EXIT
			ENDIF
			xduptotqty=totqty
			xdelpo=po
			DELETE
			LOCATE FOR plid=xxplid
			REPLACE totqty WITH totqty+xduptotqty
			LOCATE FOR po=xdelpo
			xduptotqty=totqty
			DELETE
			LOCATE FOR po=xpo AND units
			REPLACE totqty WITH totqty+xduptotqty
		ENDDO
		LOCATE FOR plid=xxplid
	ENDSCAN
ENDIF

ENDPROC

*****************************
PROCEDURE rollup_prepack_tko
*****************************
*!* Rolls up post-creation XPL data by style/color/id/pack. Modified from Darren's code.

SELECT gen856_out
SCAN FOR !units
	STORE grouping TO m.grouping
	SCATTER MEMVAR
	DO WHILE .T.
		LOCATE FOR units=m.units AND STYLE=m.style AND COLOR=m.color AND ID=m.id AND PACK=m.pack AND gen856_out.grouping # m.grouping
		IF !FOUND()
			EXIT
		ENDIF
		STORE ctnqty TO xduptotqty
		STORE grouping TO xdelgrp
		DELETE
		LOCATE FOR grouping=m.grouping
		REPLACE ctnqty WITH ctnqty+xduptotqty
		LOCATE FOR grouping=xdelgrp
		STORE ctnqty TO xduptotqty
		DELETE
		LOCATE FOR gen856_out.grouping = m.grouping AND units
		REPLACE ctnqty WITH ctnqty+xduptotqty
	ENDDO
	LOCATE FOR grouping=m.grouping AND units
ENDSCAN
LOCATE
IF lTest OR lHoldFile
	BROWSE
ENDIF

ENDPROC

******************************
PROCEDURE closedata
******************************

IF USED('inwolog')
	USE IN inwolog
ENDIF

IF USED('pl')
	USE IN pl
ENDIF

IF USED('upcmast')
	USE IN upcmast
ENDIF

IF USED('upcmastsql')
	USE IN upcmastsql
ENDIF

IF USED('account')
	USE IN account
ENDIF

*!*		IF USED('whgenpk')
*!*			USE IN whgenpk
*!*		ENDIF
ENDPROC

*****************************
PROCEDURE SLErrormail
******************************
IF USED('mm')
	USE IN mm
ENDIF
SELECT 0
USE F:\3pl\DATA\mailmaster ALIAS mm
LOCATE FOR accountid = 4677 AND taskname = "EXCELERROR"
lUseAlt = mm.use_alt
tsendto = ALLTRIM(IIF(lUseAlt,mm.sendtoalt,mm.sendto))
tcc = ALLTRIM(IIF(lUseAlt,mm.ccalt,mm.cc))
USE IN mm
tsubject = "Missing information in Inbound Excel sheet"
tattach = ""
tmessage = "The following required FIELD(s) is/are missing data:"
tmessage = tmessage+CHR(13)+cSLError
DO FORM m:\dev\FRM\dartmail2 WITH tsendto,tFrom,tsubject,tcc,tattach,tmessage,"A"
ENDPROC

******************************
PROCEDURE smerrmail
******************************
&& Used for all GENERIC INBOUND accounts
IF USED('account')
	USE IN account
ENDIF
SELECT 0
xsqlexec("select * from account where inactive=0","account",,"qq")
INDEX ON accountid TAG accountid
SET ORDER TO
IF SEEK(nAcctNum,'account','accountid')
	cAcctname = ALLTRIM(account.acctname)
ENDIF

IF USED('mm')
	USE IN mm
ENDIF
SELECT 0
USE F:\3pl\DATA\mailmaster ALIAS mm
IF !lTest
	IF nAcctNum # 6137
		LOCATE FOR mm.accountid = nAcctNum AND mm.office = goffice AND mm.edi_type = 'PL'
	ELSE
		LOCATE FOR mm.accountid = nAcctNum AND mm.taskname = 'SMMAIL'
	ENDIF
	lUseAlt = mm.use_alt
	tsendto = ALLTRIM(IIF(lUseAlt,mm.sendtoalt,mm.sendto))
	tcc = ALLTRIM(IIF(lUseAlt,mm.ccalt,mm.cc))
ELSE
	LOCATE FOR mm.edi_type = "MISC" AND mm.taskname = "GENERAL"
	lUseAlt = mm.use_alt
	tsendto = ALLTRIM(IIF(lUseAlt,mm.sendtoalt,mm.sendto))
	tcc = ALLTRIM(IIF(lUseAlt,mm.ccalt,mm.cc))
ENDIF
USE IN mm
tsubject = "Style Errors in Inbound Excel sheet: "+cAcctname
tattach = ""
tmessage = "File Name: "+IIF(!EMPTY(cFilename),cFilename,JUSTFNAME(xfile))
tmessage = tmessage+CHR(13)+CHR(13)+"The following Style/Color/Size combination(s) is/are not in our Style Master."
tmessage = tmessage+CHR(13)+"Please send these Style Master Updates, then RE-TRANSMIT the PL file. Thanks."+CHR(13)
tmessage = tmessage+CHR(13)+PADR("STYLE",22)+PADR("COLOR",12)+"SIZE"
tmessage = tmessage+CHR(13)+REPLICATE("=",38)
SELECT sm_err
SCAN
	cStyle = PADR(ALLTRIM(sm_err.STYLE),22)
	cColor = PADR(ALLTRIM(sm_err.COLOR),12)
	cSize = ALLTRIM(sm_err.ID)
	tmessage = tmessage+CHR(13)+cStyle+cColor+cSize
ENDSCAN
USE IN sm_err
DO FORM m:\dev\FRM\dartmail2 WITH tsendto,tFrom,tsubject,tcc,tattach,tmessage,"A"
ENDPROC
