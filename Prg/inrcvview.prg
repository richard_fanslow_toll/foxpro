* creates vinrcv view - MUST BE RUN IN VFP 8.0 !!

set enginebehavior 70
set exclusive off
set safety off

if type("gmilitarytime")="U"
	public gmilitarytime
endif

close databases all
*open database m:\dev\whdata\wh
*open database f:\whi\whdata\wh
*open database f:\whj\whdata\wh
*open database f:\wh1\whdata\wh
*open database f:\wh2\whdata\wh
*open database f:\wh5\whdata\wh
*open database f:\wh6\whdata\wh
*open database f:\wh7\whdata\wh
*open database f:\wh8\whdata\wh
*open database f:\whl\whdata\wh
*open database f:\whm\whdata\wh
*open database f:\whk\whdata\wh
open database f:\whp\whdata\wh

CREATE SQL VIEW "VINRCV" AS SELECT * FROM wh!inrcv where &ginrcvfilter

DBSetProp('VINRCV', 'View', 'UpdateType', 1)
DBSetProp('VINRCV', 'View', 'WhereType', 1)
DBSetProp('VINRCV', 'View', 'FetchMemo', .T.)
DBSetProp('VINRCV', 'View', 'SendUpdates', .T.)
DBSetProp('VINRCV', 'View', 'UseMemoSize', 255)
DBSetProp('VINRCV', 'View', 'FetchSize', 100)
DBSetProp('VINRCV', 'View', 'MaxRecords', -1)
DBSetProp('VINRCV', 'View', 'Tables', 'wh!inrcv')
DBSetProp('VINRCV', 'View', 'Prepared', .F.)
DBSetProp('VINRCV', 'View', 'CompareMemo', .T.)
DBSetProp('VINRCV', 'View', 'FetchAsNeeded', .F.)
DBSetProp('VINRCV', 'View', 'FetchSize', 100)
DBSetProp('VINRCV', 'View', 'Comment', "")
DBSetProp('VINRCV', 'View', 'BatchUpdateCount', 1)
DBSetProp('VINRCV', 'View', 'ShareConnection', .F.)

DBSetProp('VINRCV.inrcvid', 'Field', 'KeyField', .T.)
DBSetProp('VINRCV.inrcvid', 'Field', 'Updatable', .T.)

close databases all 

gunshot()