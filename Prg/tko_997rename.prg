CLOSE DATABASES ALL
_SCREEN.WINDOWSTATE=1

DO M:\DEV\PRG\_SETVARS WITH .t.

SELECT 0
USE "f:\edirouting\ftpsetup"
REPLACE chkbusy WITH .T. FOR transfer = "997-TKO-RENAME" IN ftpsetup

WAIT WINDOW "Now renaming TKO 997 output files" NOWAIT
ASSERT .f.

CD "F:\ftpusers\TKO997OUT\997HOLD\"
FOR i = 1 TO 2
	IF i = 1
		cWord = "TKO"
	ELSE
		cWord = "JDL"
	ENDIF
	cFileName = (cWord)+"997.x12"
	cOutname = ("997"+cWord+TTOC(DATETIME(),1)+".txt")
	IF !FILE(cFileName)
		WAIT WINDOW "No "+cWord+" 997 files to output" TIMEOUT 1
		LOOP
		ELSE
		WAIT WINDOW cWord+" 997 file processing" TIMEOUT 1
	ENDIF
	cOutfile = ("f:\ftpusers\tko-ca\945out\"+cOutname)
	cArchivefile = ("f:\ftpusers\tko-ca\945out\archive\"+cOutname)

	cString = FILETOSTR(cFileName)
	STRTOFILE(cString,cArchivefile)
	STRTOFILE(cString,cOutfile)

	IF FILE(cFileName)
		DELETE FILE [&cFilename]
	ENDIF
ENDFOR

SELECT ftpsetup
REPLACE chkbusy WITH .F. FOR transfer = "997-TKO-RENAME" IN ftpsetup

CLOSE DATABASES ALL
RETURN
