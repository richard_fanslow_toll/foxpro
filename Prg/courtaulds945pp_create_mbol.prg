*!* COURTAULDS 945 MBOL (Whse. Shipping Advice) Creation Program
*!* Creation Date: 11.18.2005 by Joe

PARAMETERS nWO_Num,cBOL,cShip_ref,cOffice,cTime,lJCP

PUBLIC ARRAY thisarray(1)
PUBLIC c_CntrlNum,c_GrpCntrlNum,cReturned,tsendto,tcc,tsendtoerr,tccerr,cProgname,cISA_Num,nAcctNum,lDoCourtFilesOut,tfrom,lEmail,lTesting
PUBLIC cMod,cMBOL,cFilenameShort,cFilenameOut,cFilenameArch,lParcelType,cWO_NumList,lcPath,lcHoldPath,lcArchivePath,cEDIType,cISA_Num,lLoadSQLBL

lIsError = .F.
lDoCatch = .T.
cShip_ref = ""
lCloseOutput = .T.
WAIT WINDOW "At the start of COURTAULDS 945 MBOL PICKPACK process..." TIMEOUT 2

TRY
	lFederated = .F.
	nAcctNum = 4677
	cOffice = "M"
	cMod = cOffice
	gOffice = cMod
	gMasterOffice = cOffice
	lLoadSQLBL = .T.

	cMBOL = ""
	cEDIType = "945"
	lParcelType = .F.

	cPPName = "Courtaulds"
	tfrom = "Toll EDI Operations <toll-edi-ops@tollgroup.com>"
	tmessage=""
	lUPS = .F.
	lSamples = .F.
	cWO_NumList = ""

	lTesting = .F.
	DO m:\dev\prg\_setvars WITH lTesting
	cProgname = "courtaulds945pp_create_mbol"
	ON ESCAPE CANCEL

	lTestinput = .F.
	lISAFlag = .T.
	lSTFlag = .T.
	nLoadid = 1
	lSQLMail = .F.
	cBOLX = "999" && BOL# of order with no shipped cartons
	nSubBillCnt = 0

	IF TYPE("cOffice") = "L"
		IF !lTesting
			WAIT WINDOW "Office not provided...terminating" TIMEOUT 3
			lCloseOutput = .F.
			DO ediupdate WITH "No OFFICE",.T.
			THROW
		ELSE
			CLOSE DATABASES ALL
			IF !USED('edi_trigger')
				cEDIFolder = "F:\3PL\DATA\"
				USE (cEDIFolder+"edi_trigger") IN 0 ALIAS edi_trigger
			ENDIF
			nWO_Num = 4020409
			cBOL = "04907304677492499"
			cShip_ref = "283127-002311"
			cOffice = "C"
			cTime = DATETIME()
			lFedEx = .F.
		ENDIF
	ENDIF

	cBOL=TRIM(cBOL)
	cMBOL = cBOL && Added to distinguish Master Bill # for flag clearing
	xReturn = "XXX"
	xReturn = wf(cOffice,nAcctNum)
	cUseFolder = UPPER(xReturn)
	gOffice=cOffice

	cRetMsg="X"
	DO m:\dev\prg\sqldata-COMPARE WITH cUseFolder,cBOL,nWO_Num,cOffice,nAcctNum,cPPName
	IF cRetMsg<>"OK"
		lCloseOutput = .F.
		DO ediupdate WITH "SQL ERROR",.T.
		THROW
	ENDIF

	IF lJCP
		cBOL = "0"
	ENDIF

	SET ASSERTS ON
	IF TYPE("nWO_Num")<> "N"
		lCloseOutput = .F.
		DO ediupdate WITH "BAD WO#",.T.
		THROW
	ENDIF
	cWO_Num = ALLTRIM(STR(nWO_Num))

	lEmail = .T.
	lTestMail = lTesting && Sends mail to Joe only
	lDoCourtFilesOut = !lTesting && If true, copies output to FTP folder (default = .t.)
	lStandalone = lTesting

*!*	lTestMail = .T.

	STORE "LB" TO cWeightUnit
	lPrepack = .F.
	lPick = .T.

	WAIT WINDOW "Now preparing tables and variables" NOWAIT NOCLEAR

*!* SET CUSTOMER CONSTANTS
	cCustName = "COURTAULDS"  && Customer Identifier
	cX12 = "004050"  && X12 Standards Set used
	cMailName = "Courtaulds"
	SELECT 0
	USE F:\3pl\DATA\mailmaster ALIAS mm
	LOCATE
	LOCATE FOR mm.edi_type = "945" AND mm.office = cOffice AND mm.accountid = nAcctNum
	tsendto = IIF(mm.use_alt,sendtoalt,sendto)
	tcc = IIF(mm.use_alt,ccalt,cc)
	lcPath = ALLTRIM(mm.basepath)
	lcArchivePath = ALLTRIM(mm.archpath)
	lcHoldPath = ALLTRIM(mm.holdpath)
	LOCATE
	LOCATE FOR mm.edi_type = "MISC" AND mm.taskname = "GENERAL"
	tsendtoerr = IIF(mm.use_alt,sendtoalt,sendto)
	tccerr = IIF(mm.use_alt,ccalt,cc)
	tsendtotest = IIF(mm.use_alt,sendtoalt,sendto)
	tcctest = IIF(mm.use_alt,ccalt,cc)
	USE IN mm

*!* SET OTHER CONSTANTS
	DO CASE
	CASE cOffice = "M"
		cCustLoc =  "FL"
		cfmiWarehouse = ""
		cCustPrefix = "945"+"f"
		cDivision = "Florida"
		cFolder = "WHM"
		cSF_Addr1  = "11400 NW 32ND AVE"
		cSF_CSZ    = "MIAMI+FL+33167"

	CASE cOffice = "N"
		cCustLoc =  "NJ"
		cfmiWarehouse = ""
		cCustPrefix = "945"+"j"
		cDivision = "New Jersey"
		cFolder = "WHN"
		cSF_Addr1  = "800 FEDERAL BLVD"
		cSF_CSZ    =  "CARTERET+NJ+07008"

	OTHERWISE
		cCustLoc =  "CA"
		cfmiWarehouse = ""
		cCustPrefix = "945"+"c"
		cDivision = "California"
		cFolder = "WHC"
		cSF_Addr1  = "450 WESTMONT DRIVE"
		cSF_CSZ    = "SAN PEDRO+CA+90731"
	ENDCASE
	cCustFolder = UPPER(cCustName)
	lNonEDI = .F.  && Assumes not a Non-EDI 945
	STORE "" TO lcKey
	STORE 0 TO alength,nLength
	cDelTime = SUBSTR(TTOC(cTime,1),9,4)
	cCarrierType = "M" && Default as "Motor Freight"
	dapptnum = ""

*!* SET OTHER CONSTANTS
	cString = ""

	csendqual = "ZZ"
	csendid = "FMIX"
	crecqual = "14"
	crecid = "5013546000747"

	cfd = "+"
	csegd = "'"
	nSegCtr = 0
	cterminator = ">" && Used at end of ISA segment
	cdate = DTOS(DATE())
	cTruncDate = RIGHT(cdate,6)
	cTruncTime = SUBSTR(TTOC(DATETIME(),1),9,4)
	cfiledate = cdate+cTruncTime
	cOrig = "J"
	nSTCount = 0
	cStyle = ""
	cPTString = ""
	nTotCtnWt = 0
	nTotCtnCount = 0
	nUnitSum = 0

** PAD ID Codes
	csendidlong = PADR(csendid,15," ")
	crecidlong = PADR(crecid,15," ")

	IF USED('OUTWOLOG')
		USE IN outwolog
	ENDIF

	csq1 = [select * from outship where accountid = ]+TRANSFORM(nAcctNum)+[ and office = ']+cOffice+[' and bol_no = ']+cBOL+[']
	xsqlexec(csq1,,,"wh")
	IF RECCOUNT() = 0
		cErrMsg = "MISS BOL "+cBOL
		DO ediupdate WITH cErrMsg,.T.
		THROW
	ENDIF
	INDEX ON bol_no TAG bol_no

	IF !USED('BL')
		IF lLoadSQLBL
			csq1 = [select * from bl where mblnum = ']+cMBOL+[']
			xsqlexec(csq1,,,"wh")
		ELSE
			USE (cUseFolder+"BL") IN 0 ALIAS bl
		ENDIF
		SELECT bl
		LOCATE
		COUNT TO nSubBillCnt FOR bl.mblnum = cMBOL
		LOCATE
	ENDIF

	SELECT outship
	IF SEEK(cBOL,'outship','bol_no')
		IF ("COURTAULDS"$outship.consignee) OR ("STEINBEST"$outship.consignee)
			lSamples = .T.
		ENDIF

		IF ("SYMS"$outship.consignee)
			lSYMS = .T.
		ELSE
			lSYMS = .F.
		ENDIF
	ELSE
		WAIT WINDOW "BOL not found in OUTSHIP" TIMEOUT 2
		lCloseOutput = .F.
		DO ediupdate WITH "BOL NOT FOUND",.T.
		THROW
	ENDIF

	IF USED('OUTDET')
		USE IN outdet
	ENDIF

	selectoutdet()
	SELECT outdet
	INDEX ON outdetid TAG outdetid

&& Added code blocks to the following to overcome BOL vs. WO selection issues, 12.23.2005
	SELECT outship
	LOCATE
	IF USED("sqlwo")
		USE IN sqlwo
	ENDIF
	DELETE FILE "F:\3pl\DATA\sqlwo.dbf"
	SELECT wo_num ;
		FROM outship WHERE bol_no = cBOL ;
		GROUP BY 1 ;
		ORDER BY 1 ;
		INTO DBF F:\3pl\DATA\sqlwo
*!*	BROWSE
	USE IN sqlwo
	USE F:\3pl\DATA\sqlwo IN 0 ALIAS sqlwo

	cRetMsg = ""
*SET STEP ON
	DO m:\dev\prg\sqlconnect_bol  WITH nAcctNum,cCustName,cPPName,.T.,cOffice
	IF cRetMsg<>"OK"
		lCloseOutput = .F.
		DO ediupdate WITH cRetMsg,.T.
		THROW
	ENDIF

*!* Mail sending for all MBOL 945s
	tsubject2 = "NOTICE: Courtaulds 945 Process for MBOL"
	tattach2 = " "
	tsendto2 = tsendtotest
	tcc2 = tcctest
	tmessage2 = "Potential files for MBOL# "+cBOL+"...monitor output for correct number of files."
	tmessage2 = tmessage2 + CHR(13) + "Expected number of 945 files: "+ALLTRIM(STR(nSubBillCnt))
	tmessage2 = tmessage2 + CHR(13) + "Processed at "+TTOC(DATETIME())
	DO FORM m:\dev\frm\dartmail2 WITH tsendto2,tfrom,tsubject2,tcc2,tattach2,tmessage2,"A"
*!* End Mailing section

	IF !USED("scacs")
		xsqlexec("select * from scac","scacs",,"wh")
		INDEX ON scac TAG scac
	ENDIF

	SELECT outship
	LOCATE
	WAIT CLEAR
	WAIT WINDOW "Now creating Header information" NOWAIT NOCLEAR

*!* HEADER LEVEL EDI DATA

	cISACode = IIF(lTesting,"T","P")

	IF !lTesting
		IF EMPTY(cBOL)  && May be empty if triggered from PT screen, or FedEx shipment
			WAIT CLEAR
			WAIT WINDOW "MISSING Bill of Lading Number!" TIMEOUT 2
			lCloseOutput = .F.
			DO ediupdate WITH "BOL# EMPTY",.T.
			THROW
		ELSE
			IF !SEEK(PADR(TRIM(cBOL),20),"outship","bol_no")
				WAIT CLEAR
				WAIT WINDOW "Invalid Bill of Lading Number - Not Found!" TIMEOUT 2
				lCloseOutput = .F.
				DO ediupdate WITH "BOL# NOT FOUND",.T.
				THROW
			ENDIF
		ENDIF
	ENDIF

	SELECT outship
	SET ORDER TO
	LOCATE && in OUTSHIP

*************************************************************************
*1	PTIDSOURCE/OUTSHIP RECORD MAIN LOOP
*	Loops through individual OUTSHIP lines for BOL#
*************************************************************************
	WAIT CLEAR
	WAIT WINDOW "Now creating BOL#-based information..." NOWAIT NOCLEAR
	SELECT bl
	LOCATE
	cMissDel = ""

	oscanstr = "bl.mblnum = cMBOL"

	nBLID = 0
	ASSERT .F. MESSAGE "At MBOL scan"
	SCAN FOR &oscanstr
		cBOL = TRIM(bl.bol_no)
		IF cMBOL = '9999' AND cBOL # '04907304677451588'  && to select only a single sub-BOL to reprocess
			LOOP
		ENDIF
		nBLID = bl.blid
		WAIT WINDOW "Now processing Sub-BOL #"+cBOL TIMEOUT 2
		CREATE CURSOR tempcourt945 (accountid i,isa_num c(10),st_num c(10),wo_num i,bol_no c(20),ship_ref c(20),filename c(50),filedate T)
		DO num_incr_isa

		cISA_Num = PADL(c_CntrlNum,9,"0")
		nISA_Num = INT(VAL(cISA_Num))

		dt2 = DATETIME()
		dt1 = TTOC(dt2,1)
		dtmail = TTOC(dt2)

		lcPath = IIF(lTesting,("F:\FTPUSERS\Courtaulds\945OUT\test\"),lcPath)
		cFilenameHold = (lcHoldPath+cCustPrefix+dt1+".txt")
		cFilenameShort = JUSTFNAME(cFilenameHold)
		cFilenameOut = (lcPath+cFilenameShort)
		cFilenameArch = (lcArchivePath+cFilenameShort)
		nFilenum = FCREATE(cFilenameHold)

		STORE "ISA"+cfd+"00"+cfd+"          "+cfd+"00"+cfd+"          "+cfd+csendqual+cfd+csendidlong+cfd+;
			crecqual+cfd+crecidlong+cfd+cTruncDate+cfd+cTruncTime+cfd+"U"+cfd+"00401"+cfd+;
			cISA_Num+cfd+"0"+cfd+cISACode+cfd+cterminator+csegd TO cString
		DO cstringbreak

		STORE "GS"+cfd+"SW"+cfd+csendid+cfd+crecid+cfd+cdate+cfd+cTruncTime+cfd+c_CntrlNum+;
			cfd+"X"+cfd+cX12+csegd TO cString
		DO cstringbreak

		xsqlexec("select * from bldet where mod='"+gOffice+"' and blid="+TRANSFORM(nBLID),,,"wh")

		SELECT bldet
		SCAN FOR bldet.blid = nBLID AND bldet.accountid = nAcctNum
			cShip_ref = ALLTRIM(bldet.ship_ref)
			SELECT outship
			SET ORDER TO TAG wo_num
			LOCATE

			IF !SEEK(nWO_Num)
				WAIT CLEAR
				WAIT WINDOW "Invalid Work Order Number - Not Found in OUTSHIP!" TIMEOUT 2
				DO ediupdate WITH "WO# NOT FOUND",.T.
				THROW
			ENDIF
			SET ORDER TO
			LOCATE

			LOCATE FOR outship.ship_ref = cShip_ref AND accountid =nAcctNum
			IF !FOUND()
				DO ediupdate WITH "MISS SHIP_REF",.T.
				THROW
			ENDIF
			SCATTER MEMVAR MEMO
			lD2S = IIF("D2S"$m.consignee,.T.,.F.)

			csq1 = [select * from outwolog where accountid = ]+TRANSFORM(nAcctNum)+[ and wo_num = ]+TRANSFORM(outship.wo_num)
			xsqlexec(csq1,,,"wh")
			IF RECCOUNT() = 0
				cErrMsg = "MISS OUTWOLOG REC FOR WO# "+cWO_Num
				DO ediupdate WITH cErrMsg,.T.
				THROW
			ENDIF
			SELECT outwolog
			INDEX ON wo_num TAG wo_num

			IF SEEK(m.wo_num,"outwolog","wo_num")
				lPick = IIF(outwolog.picknpack,.T.,.F.)
				lPrepack = IIF(lPick,.F.,.T.)
			ENDIF

			USE IN outwolog

*!* Added this code to trap miscounts in OUTDET Units
			IF lPrepack
				SELECT outdet
				SET ORDER TO
				SUM totqty TO nUnitTot1 FOR units AND outdet.outshipid = outship.outshipid
				SUM (VAL(PACK)*totqty) TO nUnitTot2 FOR !units AND outdet.outshipid = outship.outshipid
				IF nUnitTot1<>nUnitTot2
					DO ediupdate WITH "OUTDET TOTQTY ERR",.T.
					THROW
				ENDIF
				SELECT vcourtpp
				SUM totqty TO nUnitTot2 FOR units AND vcourtpp.outshipid = outship.outshipid
				IF nUnitTot1<>nUnitTot2
					DO ediupdate WITH "SQL TOTQTY ERR",.T.
					THROW
				ENDIF
				LOCATE
			ENDIF
			SELECT outdet
			SET ORDER TO outdetid
			LOCATE
			SELECT outship
*!* End code addition
			cPO_Num = ALLTRIM(m.cnee_ref)

			IF (("PENNEY"$UPPER(outship.consignee)) OR ("KMART"$UPPER(outship.consignee)) OR ("K-MART"$UPPER(outship.consignee)))
				lApptFlag = .T.
			ELSE
				lApptFlag = .F.
			ENDIF

			cTRNum = ""
			cPRONum = ""

			IF (("WALMART"$outship.consignee) OR ("WAL-MART"$outship.consignee) OR ("WAL MART"$outship.consignee))
				IF LEFT(outship.keyrec,2) = "PR"
					STORE TRIM(keyrec) TO cPRONum
				ENDIF
				IF LEFT(outship.keyrec,2) = "TR"
					STORE TRIM(keyrec) TO cTRNum
				ENDIF
			ENDIF


			IF lTestinput OR EMPTYnul(outship.del_date)
				ddel_date = DATE()
				dapptnum = "99999"
				dapptdate = DATE()
			ELSE
				ddel_date = outship.del_date
				IF EMPTY(ddel_date)
					cMissDel = IIF(EMPTY(cMissDel),"The following PTs had no Delivery Dates:"+CHR(10)+TRIM(cShip_ref),cMissDel+CHR(10)+TRIM(cShip_ref))
				ENDIF
				dapptnum = ALLTRIM(outship.appt_num)
				IF cBOL = cBOLX
					dapptnum = "NONE"
				ENDIF
				IF !lSYMS
					IF EMPTY(dapptnum) && Penney/KMart Appt Number check
						IF (!(lApptFlag) OR (DATE()={^2006-05-18} AND cBOL = "04907314677812395"))
							dapptnum = ""
						ELSE
							DO ediupdate WITH "EMPTY APPT #",.T.
							THROW
						ENDIF
					ENDIF
				ELSE
					dapptnum = "99999"
				ENDIF
				dapptdate = outship.appt
				IF cBOL = cBOLX
					dapptdate = DATE()
				ENDIF

				IF EMPTY(dapptdate)
					dapptdate = outship.del_date
				ENDIF
			ENDIF

			nWO_Num = outship.wo_num
			cWO_Num = ALLTRIM(STR(nWO_Num))
			IF !(cWO_Num$cWO_NumList)
				IF EMPTY(cWO_NumList)
					STORE cWO_Num TO cWO_NumList
				ELSE
					cWO_NumList = (cWO_NumList+CHR(13)+cWO_Num)
				ENDIF
			ENDIF

			IF ALLTRIM(outship.SForCSZ) = ","
				BLANK FIELDS outship.SForCSZ NEXT 1 IN outship
			ENDIF

			SCATTER MEMVAR MEMO
			alength = ALINES(apt,outship.shipins,.T.,CHR(13))

			lPrepack = .F.
			lPick = .T.
			nTotCtnCount = m.qty
			IF cBOL = "99911111111111111"
				cShip_ref = ALLTRIM(STRTRAN(cShip_ref,"OVER",""))
			ENDIF

			nOutshipid = m.outshipid
			xConsignee = PADR(ALLTRIM(m.consignee),30," ")
			xShip_ref = PADR(ALLTRIM(cShip_ref),20," ")
			xBOL = PADR(ALLTRIM(cBOL),20," ")

			cPTString = IIF(EMPTY(cPTString),xConsignee+xShip_ref+cWO_Num,;
				cPTString+CHR(10)+xConsignee+xShip_ref+cWO_Num)
			m.CSZ = TRIM(m.CSZ)

			IF EMPTY(M.CSZ)
				WAIT CLEAR
				WAIT WINDOW "No SHIP-TO City/State/ZIP info...exiting" TIMEOUT 2
				DO ediupdate WITH "BAD ADDRESS INFO",.T.
				THROW
			ENDIF
			m.CSZ = ALLTRIM(STRTRAN(m.CSZ,"  "," "))
			nSpaces = OCCURS(" ",ALLTRIM(m.CSZ))
			IF nSpaces = 0
				WAIT WINDOW "Ship-to CSZ Segment Error" TIMEOUT 3
				DO ediupdate WITH "Ship-to CSZ: "+cShip_ref,.T.
				THROW
			ELSE
				nCommaPos = AT(",",m.CSZ)
				nLastSpace = AT(" ",m.CSZ,nSpaces)
				IF ISALPHA(SUBSTR(TRIM(m.CSZ),AT(" ",m.CSZ,nSpaces-1)+1,2))
					cCity = LEFT(TRIM(m.CSZ),AT(",",m.CSZ)-1)
					cState = SUBSTR(m.CSZ,nCommaPos+2,2)
					cZip = TRIM(SUBSTR(m.CSZ,nLastSpace+1))
				ELSE
					WAIT CLEAR
					WAIT WINDOW "NOT ALPHA: "+SUBSTR(TRIM(m.CSZ),AT(" ",m.CSZ,nSpaces-1)+1,2) TIMEOUT 3
					cCity = LEFT(TRIM(m.CSZ),AT(",",m.CSZ)-1)
					cState = SUBSTR(TRIM(m.CSZ),AT(" ",m.CSZ,nSpaces-2)+1,2)
					cZip = TRIM(SUBSTR(m.CSZ,nLastSpace+1))
				ENDIF
			ENDIF

			STORE "" TO cSForCity,cSForState,cSForZip
			IF !lFederated
				IF !EMPTY(M.SForCSZ)
					m.SForCSZ = ALLTRIM(STRTRAN(m.SForCSZ,"  "," "))
					nSpaces = OCCURS(" ",ALLTRIM(m.SForCSZ))
					IF nSpaces = 0
						m.SForCSZ = STRTRAN(m.SForCSZ,",","")
						cSForCity = ALLTRIM(m.SForCSZ)
						cSForState = ""
						cSForZip = ""
					ELSE
						nCommaPos = AT(",",m.SForCSZ)
						nLastSpace = AT(" ",m.SForCSZ,nSpaces)
						nMinusSpaces = IIF(nSpaces=1,0,1)
						IF ISALPHA(SUBSTR(TRIM(m.SForCSZ),AT(" ",TRIM(m.SForCSZ),nSpaces-nMinusSpaces)+1,2))
							cSForCity = LEFT(TRIM(m.SForCSZ),AT(",",m.SForCSZ)-1)
							cSForState = SUBSTR(m.SForCSZ,nCommaPos+2,2)
							cSForZip = TRIM(RIGHT(TRIM(m.SForCSZ),5))
							IF ISALPHA(cSForZip)
								cSForZip = ""
							ENDIF
						ELSE
							WAIT CLEAR
							WAIT WINDOW "NOT ALPHA: "+SUBSTR(TRIM(m.SForCSZ),AT(" ",m.SForCSZ,nSpaces-1)+1,2) TIMEOUT 3
							cSForCity = LEFT(TRIM(m.SForCSZ),AT(",",m.SForCSZ)-1)
							cSForState = SUBSTR(TRIM(m.SForCSZ),AT(" ",m.SForCSZ,nSpaces-2)+1,2)
							cSForZip = TRIM(RIGHT(TRIM(m.SForCSZ),5))
							IF ISALPHA(cSForZip)
								cSForZip = ""
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ELSE
				cStoreName = segmentget(@apt,"STORENAME",alength)
			ENDIF

			DO num_incr_st
			WAIT CLEAR
			WAIT WINDOW "Now creating Line Item information" NOWAIT NOCLEAR

			INSERT INTO tempcourt945 (accountid,isa_num,st_num,wo_num,bol_no,ship_ref,filename,filedate) ;
				VALUES (m.accountid,c_CntrlNum,c_GrpCntrlNum,m.wo_num,m.bol_no,m.ship_ref,cFilenameShort,dt2)

			STORE "ST"+cfd+"945"+cfd+PADL(c_GrpCntrlNum,9,"0")+csegd TO cString
			DO cstringbreak
			nSTCount = nSTCount + 1
			nSegCtr = 1

			STORE "W06"+cfd+"N"+cfd+cShip_ref+cfd+cdate+cfd+TRIM(cBOL)+cfd+TRIM(cWO_Num)+cfd+cPO_Num+csegd TO cString
			DO cstringbreak
			nSegCtr = nSegCtr + 1
			nCtnNumber = 1  && Seed carton sequence count
*	nLineNum = 1

			IF !lSYMS AND !lSamples
				cStoreNum = ALLTRIM(m.dcnum)
				IF EMPTY(TRIM(cStoreNum))
					IF (!EMPTY(m.storenum) AND m.storenum<>0)
						IF LEN(ALLTRIM(STR(m.storenum))) < 5
							cStoreNum = PADL(ALLTRIM(STR(m.storenum)),4,"0")
						ELSE
							cStoreNum = ALLTRIM(STR(m.storenum))
						ENDIF
					ENDIF
					IF EMPTY(cStoreNum)
						cStoreNum = segmentget(@apt,"STORENUM",alength)
						IF EMPTY(cStoreNum)
							DO ediupdate WITH "MISS STORENUM",.T.
							THROW
						ENDIF
					ENDIF
				ENDIF
			ELSE
				cStoreNum = ""
			ENDIF

			STORE "N1"+cfd+"SF"+cfd+"TGF INTERNATIONAL"+cfd+"91"+cfd+"01"+csegd TO cString
			DO cstringbreak
			nSegCtr = nSegCtr + 1

			IF !lFederated
				STORE "N3"+cfd+TRIM(cSF_Addr1)+csegd TO cString
				DO cstringbreak
				nSegCtr = nSegCtr + 1

				STORE "N4"+cfd+cSF_CSZ+cfd+"USA"+csegd TO cString
				DO cstringbreak
				nSegCtr = nSegCtr + 1
			ENDIF

			IF lSYMS OR lSamples
				STORE "N1"+cfd+"ST"+cfd+ALLTRIM(m.consignee)+cfd+"92"+csegd TO cString
			ELSE
				STORE "N1"+cfd+"ST"+cfd+ALLTRIM(m.consignee)+cfd+"92"+cfd+cStoreNum+csegd TO cString
			ENDIF
			DO cstringbreak
			nSegCtr = nSegCtr + 1

			STORE "N3"+cfd+TRIM(outship.address)+csegd TO cString
			DO cstringbreak
			nSegCtr = nSegCtr + 1

			STORE "N4"+cfd+cCity+cfd+cState+cfd+cZip+cfd+"USA"+csegd TO cString
			DO cstringbreak
			nSegCtr = nSegCtr + 1

			IF !EMPTY(m.sforstore)
				STORE "N1"+cfd+"BY"+cfd+ALLTRIM(m.shipfor)+cfd+"91"+cfd+TRIM(m.sforstore)+csegd TO cString
				DO cstringbreak
				nSegCtr = nSegCtr + 1

				STORE "N3"+cfd+TRIM(m.sforaddr1)+csegd TO cString
				DO cstringbreak
				nSegCtr = nSegCtr + 1

				IF !EMPTY(cSForState)
					STORE "N4"+cfd+cSForCity+cfd+cSForState+cfd+cSForZip+cfd+"USA"+csegd TO cString
				ELSE
					STORE "N4"+cfd+cSForCity+csegd TO cString
				ENDIF
				DO cstringbreak
				nSegCtr = nSegCtr + 1
			ENDIF

			STORE "N9"+cfd+"MN"+cfd+TRIM(cWO_Num)+csegd TO cString
			DO cstringbreak
			nSegCtr = nSegCtr + 1

			STORE "N9"+cfd+"DP"+cfd+TRIM(outship.dept)+csegd TO cString
			DO cstringbreak
			nSegCtr = nSegCtr + 1

			STORE "N9"+cfd+"PUA"+cfd+dapptnum+csegd TO cString
			DO cstringbreak
			nSegCtr = nSegCtr + 1

			IF !EMPTY(cPRONum)
				STORE "N9"+cfd+"RE"+cfd+cPRONum+csegd TO cString
				DO cstringbreak
				nSegCtr = nSegCtr + 1
			ENDIF

*!* For JCPenney orders only
			IF cacctnum = "JCP" OR ("PENNEY"$UPPER(m.consignee))
				IF lTesting AND EMPTY(m.batch_num)
					cLoadID = PADR(ALLTRIM(STR(nLoadid)),6,"0")
					nLoadid = nLoadid + 1
				ELSE
					cLoadID = ALLTRIM(m.batch_num)
					IF EMPTY(cLoadID)
						cLoadID = ALLTRIM(m.appt_num)
						IF EMPTY(cLoadID)
							DO ediupdate WITH "MISS LOADID",.T.
							THROW
						ENDIF
					ENDIF
				ENDIF
				STORE "N9"+cfd+"P8"+cfd+TRIM(cLoadID)+csegd TO cString
				DO cstringbreak
				nSegCtr = nSegCtr + 1
			ENDIF

			STORE "G62"+cfd+"11"+cfd+TRIM(DTOS(ddel_date))+cfd+"D"+cfd+cDelTime+csegd TO cString  && Ship date/time
			DO cstringbreak
			nSegCtr = nSegCtr + 1

			SELECT scacs
			IF lTesting AND (EMPTY(m.scac) OR EMPTY(m.ship_via))
				STORE "RDWY" TO m.scac
				STORE "ROADWAY EXPRESS" TO m.ship_via
			ELSE
				IF !EMPTY(TRIM(outship.scac))
					STORE ALLT(outship.scac) TO m.scac
					lFedEx = .F.
					SELECT scacs
					IF SEEK(m.scac,"scacs","scac")
						IF ("FEDERAL EXPRESS"$NAME) OR ("FEDEX"$NAME)
							lFedEx = .T.
						ENDIF
						SELECT outship

						IF lUPS OR lFedEx
							cCarrierType = "U" && Parcel as UPS or FedEx
						ENDIF
					ELSE
						WAIT CLEAR
						DO ediupdate WITH "MISSING SCAC",.T.
						THROW
					ENDIF
				ENDIF
			ENDIF

			SELECT outship
			IF lUPS AND lD2S
				m.scac = "UPSL"
				m.ship_via = "UPS COLLECT GROUND"
			ENDIF

			IF !EMPTY(cTRNum)
				STORE "W27"+cfd+cCarrierType+cfd+TRIM(m.scac)+cfd+TRIM(m.ship_via)+REPLICATE(cfd,2)+"TL"+;
					REPLICATE(cfd,2)+cTRNum+csegd TO cString
			ELSE
				STORE "W27"+cfd+cCarrierType+cfd+TRIM(m.scac)+cfd+TRIM(m.ship_via)+csegd TO cString
			ENDIF
			DO cstringbreak
			nSegCtr = nSegCtr + 1

*************************************************************************
*2	DETAIL LOOP - MATCHING SHIP_REF FIELD (FROM OUTSHIP)
*************************************************************************
			WAIT CLEAR

			WAIT WINDOW "Now creating Detail information "+cShip_ref NOWAIT NOCLEAR


			SELECT vcourtauldspp
			SET RELATION TO outdetid INTO outdet
			LOCATE
			LOCATE FOR ship_ref = TRIM(cShip_ref) AND outshipid = nOutshipid
			IF !FOUND()
				IF !lTesting
					WAIT WINDOW "PT "+cShip_ref+" NOT FOUND in vcourtauldspp...ABORTING" TIMEOUT 2
					IF !lTesting
						lSQLMail = .T.
					ENDIF
					DO ediupdate WITH "MISS PT-SQL: "+cShip_ref,.T.
					THROW
				ELSE
					LOOP
				ENDIF
			ENDIF

			IF nWO_Num = 233622
				scanstr = "vcourtauldspp.ship_ref = cShip_ref and vcourtpp.outshipid = nOutshipid and userid = 'PAULG'"
			ELSE
				scanstr = "vcourtauldspp.ship_ref = cShip_ref and vcourtauldspp.outshipid = nOutshipid"
			ENDIF
			SELECT vcourtauldspp
			LOCATE
			LOCATE FOR &scanstr
			cCartonNum= "XXX"
			lDoManSegment = .T.
			lDoPALSegment = .T.
			DO WHILE &scanstr
				alength = ALINES(apt,outdet.printstuff,.T.,CHR(13))
				lSkipBack = .T.

				IF TRIM(vcourtauldspp.ucc) <> cCartonNum
					STORE TRIM(vcourtauldspp.ucc) TO cCartonNum
				ENDIF

				STORE "LX"+cfd+ALLTRIM(STR(nCtnNumber))+csegd TO cString   && Carton Seq. # (up to Carton total)
				DO cstringbreak
				nSegCtr = nSegCtr + 1

				DO WHILE vcourtauldspp.ucc = cCartonNum
					IF vcourtauldspp.totqty = 0
						nCtnNumber = nCtnNumber-1
						SKIP 1 IN vcourtauldspp
						LOOP
					ENDIF

					cUCCNumber = vcourtauldspp.ucc
					IF EMPTY(cUCCNumber) OR ISNULL(cUCCNumber)
						WAIT CLEAR
						WAIT WINDOW "Empty UCC Number in vcourtauldspp "+cShip_ref TIMEOUT 2
						lSQLMail = .T.
						DO ediupdate WITH "EMPTY UCC# in "+cShip_ref,.T.
						THROW
					ENDIF
					cUCCNumber = TRIM(STRTRAN(TRIM(cUCCNumber)," ",""))

					IF lDoManSegment
						lDoManSegment = .F.
						STORE "MAN"+cfd+"GM"+cfd+TRIM(cUCCNumber)+csegd TO cString  && SCC-14 Number (Wal-mart spec)
						DO cstringbreak
						nSegCtr = nSegCtr + 1
						nShipDetQty = INT(VAL(outdet.PACK))
						nUnitSum = nUnitSum + nShipDetQty

						IF totqty>0
							IF (EMPTY(outdet.ctnwt) OR outdet.ctnwt = 0)
								cCtnWt = ALLTRIM(STR(INT(outship.weight/outship.ctnqty)))
								IF EMPTY(cCtnWt) OR INT(VAL(cCtnWt)) = 0
									DO ediupdate WITH "WEIGHT ERR: PT "+cShip_ref,.T.
									THROW
								ENDIF
								nTotCtnWt = nTotCtnWt + INT(VAL(cCtnWt))
							ELSE
								STORE ALLTRIM(STR(outdet.ctnwt)) TO cCtnWt
								nTotCtnWt = nTotCtnWt + outdet.ctnwt
							ENDIF
						ELSE
							nTotCtnWt = nTotCtnWt + 0
						ENDIF

					ENDIF

					cColor = TRIM(outdet.COLOR)
					cSize = TRIM(outdet.ID)
					cUPC = TRIM(outdet.upc)
					IF BETWEEN(cShip_ref,"024719-001455","025724-001455") AND cUPC = "1283010032204"
						cUPC = "283010032204"
					ENDIF
					cStyle = TRIM(outdet.STYLE)
					cItemNum = TRIM(outdet.custsku)
					cLineNum = TRIM(outdet.linenum)
					IF EMPTY(cLineNum)
						DO ediupdate WITH "EMPTY LINE #s",.T.
						THROW
					ENDIF

					nShipDetQty = vcourtauldspp.totqty
					IF ISNULL(nShipDetQty)
						nShipDetQty = outdet.totqty
					ENDIF

					nOrigDetQty = vcourtauldspp.qty
					IF ISNULL(nOrigDetQty)
						nOrigDetQty = nShipDetQty
					ENDIF

					IF cBOL = "04907314677771661"
						nShipDetQty = 0
					ENDIF

					IF lDoPALSegment
						STORE "PAL"+REPLICATE(cfd,11)+cCtnWt+cfd+cWeightUnit+csegd TO cString
						DO cstringbreak
						nSegCtr = nSegCtr + 1
						lDoPALSegment = .F.
					ENDIF

*!* Changed the following to utilize original 940 unit codes from Printstuff field
					cUnitCode = TRIM(segmentget(@apt,"UNITCODE",alength))
					IF EMPTY(cUnitCode)
						cUnitCode = "EA"
					ENDIF

					IF nOrigDetQty = nShipDetQty
						nShipStat = "CL"
					ELSE
						nShipStat = "PR"
					ENDIF

					STORE "W12"+cfd+nShipStat+cfd+ALLTRIM(STR(nOrigDetQty))+cfd+ALLTRIM(STR(nShipDetQty))+cfd+;
						ALLTRIM(STR(nOrigDetQty-nShipDetQty))+cfd+cUnitCode+cfd+cfd+"VA"+cfd+cStyle+csegd TO cString
					DO cstringbreak
					nSegCtr = nSegCtr + 1

					STORE "N9"+cfd+"BV"+cfd+cLineNum+csegd TO cString
					DO cstringbreak
					nSegCtr = nSegCtr + 1

					SKIP 1 IN vcourtauldspp
					nCtnNumber = nCtnNumber + 1
					lSkipBack = .T.
				ENDDO
				lDoManSegment = .T.
				lDoPALSegment = .T.
			ENDDO

*************************************************************************
*2^	END DETAIL MATCHING SHIP_REF LOOP
*************************************************************************
			WAIT CLEAR
			WAIT WINDOW "Now creating Section Closure information" NOWAIT NOCLEAR
			STORE "W03"+cfd+ALLTRIM(STR(nUnitSum))+cfd+ALLTRIM(STR(nTotCtnWt))+cfd+;
				cWeightUnit+csegd TO cString   && Units sum, Weight sum, carton count

			nTotCtnWt = 0
			nTotCtnCount = 0
			nUnitSum = 0
			FPUTS(nFilenum,cString)

			STORE  "SE"+cfd+ALLTRIM(STR(nSegCtr+2))+cfd+PADL(c_GrpCntrlNum,9,"0")+csegd TO cString
			FPUTS(nFilenum,cString)

			SELECT bldet
			WAIT CLEAR

		ENDSCAN
		ASSERT .F. MESSAGE "After sub-bill det scan"
*DO LABELCREATE

*************************************************************************
*1^	END OUTSHIP MAIN LOOP
*************************************************************************

		DO close945
		=FCLOSE(nFilenum)
		RELEASE ALL LIKE nFilenum,dt1

		IF !("W06"$cString)
			cTestString = FILETOSTR(cFilenameHold)
			IF !("W06"$cTestString)  && Indicates an empty/junk file
				DELETE FILE &cFilenameHold
				SELECT edi_trigger
				cErrMsg = "FILE EMPTY"
				tsubject = cCustName+" Empty 945 MBOL File at "+TTOC(DATETIME())
				tmessage = "File "+cFilenameHold+" was created empty and deleted before sending."+CHR(13)+"Check edi poller process if this message repeats."
				tattach  = ""
				tcc=""
	\			DO FORM m:\dev\frm\dartmail2 WITH tsendto,tfrom,tsubject,tcc,tattach,tmessage,"A"

				lDoCatch = .F.
				THROW
			ENDIF
		ENDIF

		IF !lTesting
			SELECT edi_trigger
			DO ediupdate WITH "945 CREATED",.F.
			SELECT edi_trigger

			IF !USED("ftpedilog")
				SELECT 0
				USE F:\edirouting\ftpedilog ALIAS ftpedilog
				INSERT INTO ftpedilog (TRANSFER,ftpdate,filename,acct_name,TYPE) VALUES ("945-"+cCustName+"-"+cCustLoc,dt2,cFilenameHold,UPPER(cCustName),"945")
				USE IN ftpedilog
			ENDIF
		ENDIF

		WAIT CLEAR
		WAIT WINDOW cCustFolder+" 945 Process complete..." TIMEOUT 1

*!* Create eMail confirmation message
		IF lTestMail
			tsendto = tsendtotest
			tcc = tcctest
		ENDIF

		tsubject = cMailName+" 945 EDI File from TGF as of "+dtmail+" ("+cCustLoc+")"
		tattach = " "
		tmessage = "945 EDI Info from TGF, file "+cFilenameShort+CHR(13)
		tmessage = tmessage + "Division "+cDivision+", Master BOL# "+cMBOL+CHR(13)
		tmessage = tmessage + "Sub-BOL# "+TRIM(cBOL)+CHR(13)
		tmessage = tmessage + "containing these picktickets:"+CHR(10)+CHR(10)
		tmessage = tmessage + PADR("Consignee",30," ")+PADR("Pickticket#",20," ")+"TGF WO#"+ CHR(10)
		tmessage = tmessage + cPTString + CHR(10)
		IF cBOL=cBOLX
			tmessage = tmessage  +"has been created and will be held briefly before transmission."+CHR(10)+CHR(10)
		ELSE
			tmessage = tmessage +"has been created and will be transmitted ASAP."+CHR(10)+CHR(10)
		ENDIF
		IF !EMPTY(cMissDel)
			tmessage = tmessage+CHR(10)+CHR(10)+cMissDel+CHR(10)+CHR(10)
		ENDIF
		tmessage = tmessage + "If you have any questions, please eMail."
		IF lEmail
			DO FORM m:\dev\frm\dartmail2 WITH tsendto,tfrom,tsubject,tcc,tattach,tmessage,"A"
		ENDIF

		WAIT CLEAR
		WAIT WINDOW cMailName+" 945 EDI File output complete" TIMEOUT 1

*!* Transfers files to correct output folders
		COPY FILE [&cFilenameHold] TO [&cFilenameArch]
		IF lDoCourtFilesOut
			COPY FILE [&cFilenameHold] TO [&cFilenameOut]
			DELETE FILE [&cFilenameHold]
			SELECT tempcourt945
			COPY TO "f:\3pl\data\tempcourt945a.dbf"
			USE IN tempcourt945
			SELECT 0
			USE "f:\3pl\data\tempcourt945a.dbf" ALIAS tempcourt945a
			SELECT 0
			USE "f:\3pl\data\pts_sent945.dbf" ALIAS pts_sent945
			APPEND FROM "f:\3pl\data\tempcourt945a.dbf"
			USE IN pts_sent945
			USE IN tempcourt945a
			DELETE FILE "f:\3pl\data\tempcourt945a.dbf"
		ENDIF
		cPTString = ""

		ASSERT .F. MESSAGE "At end of sub-bill main scan"

		IF !lTesting
			asn_out_data()
		ENDIF

		cWO_NumList = ""

	ENDSCAN
	RELEASE ALL LIKE c_CntrlNum,c_GrpCntrlNum
	closefiles()

CATCH TO oErr
	IF lDoCatch
		SET STEP ON
		lEmail = .F.
		DO ediupdate WITH "ERRHAND ERROR",.T.
		tsubject = cCustName+" Error ("+TRANSFORM(oErr.ERRORNO)+") at "+TTOC(DATETIME())
		tattach  = ""
		IF lTesting
			tsendto  = tsendtotest
			tcc = tcctest
		ELSE
			tsendto  = tsendtoerr
			tcc = tccerr
		ENDIF

		tmessage = cCustName+" Error processing "+CHR(13)

		tmessage =tmessage+ "Try/CATCH Exeception Message:"+CHR(13)+;
			[  Error: ] + STR(oErr.ERRORNO) +CHR(13)+;
			[  LineNo: ] + STR(oErr.LINENO) +CHR(13)+;
			[  Message: ] + oErr.MESSAGE +CHR(13)+;
			[  Procedure: ] + oErr.PROCEDURE +CHR(13)+;
			[  Details: ] + oErr.DETAILS +CHR(13)+;
			[  StackLevel: ] + STR(oErr.STACKLEVEL) +CHR(13)+;
			[  LineContents: ] + oErr.LINECONTENTS+CHR(13)+;
			[  UserValue: ] + oErr.USERVALUE
		tmessage =tmessage+CHR(13)+cProgname

		tsubject = "945 EDI Poller Error at "+TTOC(DATETIME())
		tattach  = ""
		tcc=""
		DO FORM m:\dev\frm\dartmail2 WITH tsendto,tfrom,tsubject,tcc,tattach,tmessage,"A"
	ENDIF
FINALLY
	ON ERROR
ENDTRY


*** END OF CODE BODY

****************************
PROCEDURE close945
****************************
** Footer Creation

STORE  "GE"+cfd+ALLTRIM(STR(nSTCount))+cfd+c_CntrlNum+csegd TO cString
FPUTS(nFilenum,cString)

STORE  "IEA"+cfd+"1"+cfd+PADL(c_CntrlNum,9,"0")+csegd TO cString
FPUTS(nFilenum,cString)

RETURN

****************************
PROCEDURE num_incr_isa
****************************

IF !USED("serfile")
	USE ("f:\3pl\data\serial\"+cCustName+"945_serial") IN 0 ALIAS serfile
ENDIF
SELECT serfile
IF lTesting AND lISAFlag
	nOrigSeq = serfile.seqnum
	lISAFlag = .F.
ENDIF
nISA_Num = serfile.seqnum
c_CntrlNum = ALLTRIM(STR(serfile.seqnum))
REPLACE serfile.seqnum WITH serfile.seqnum + 1 IN serfile
SELECT outship
RETURN

****************************
PROCEDURE num_incr_st
****************************

IF !USED("serfile")
	USE ("f:\3pl\data\serial\"+cCustName+""945_serial") IN 0 ALIAS serfile
ENDIF
SELECT serfile
IF lTesting AND lSTFlag
	nOrigGrpSeq = serfile.grpseqnum
	lSTFlag = .F.
ENDIF
c_GrpCntrlNum = ALLTRIM(STR(serfile.grpseqnum))
REPLACE serfile.grpseqnum WITH serfile.grpseqnum + 1 IN serfile
SELECT outship
RETURN


****************************
PROCEDURE segmentget
****************************

PARAMETER thisarray,lcKey,nLength

FOR i = 1 TO nLength
	IF i > nLength
		EXIT
	ENDIF
	lnEnd= AT("*",thisarray[i])
	IF lnEnd > 0
		lcThisKey =TRIM(SUBSTR(thisarray[i],1,lnEnd-1))
		IF OCCURS(lcKey,lcThisKey)>0
			RETURN SUBSTR(thisarray[i],lnEnd+1)
			i = 1
		ENDIF
	ENDIF
ENDFOR

RETURN ""

****************************
PROCEDURE ediupdate
****************************
PARAMETER cStatus,lIsError
lDoCatch = .F.

IF !lTesting
	SELECT edi_trigger
	nRec = RECNO()
*		DO m:\dev\prg\edistatusmove WITH cMBOL
	LOCATE
	IF !lIsError
		REPLACE edi_trigger.processed WITH .T.,edi_trigger.proc945 WITH .T.,edi_trigger.file945 WITH cFilenameHold,;
			edi_trigger.isa_num WITH cISA_Num,;
			edi_trigger.fin_status WITH "945 CREATED",edi_trigger.errorflag WITH .F.,edi_trigger.when_proc WITH DATETIME(),;
			edi_trigger.masterbill WITH .T. ;
			FOR edi_trigger.bol = cMBOL AND edi_trigger.accountid = nAcctNum
	ELSE
		REPLACE edi_trigger.processed WITH .T.,edi_trigger.proc945 WITH .F.,edi_trigger.file945 WITH "",;
			edi_trigger.fin_status WITH cStatus,edi_trigger.errorflag WITH .T. ;
			FOR edi_trigger.bol = cMBOL AND edi_trigger.accountid = nAcctNum
		IF lCloseOutput
			=FCLOSE(nFilenum)
			ERASE &cFilenameHold
		ENDIF
	ENDIF
ENDIF

IF lIsError AND lEmail AND cStatus<>"SQL ERROR"
	tsubject = "945 Error in COURTAULDS BOL "+TRIM(cBOL)+"(At PT "+cShip_ref+")"
	tattach = " "
	tsendto = tsendtoerr
	tcc = tccerr
	tmessage = "945 Processing for Master BOL# "+cMBOL+", WO# "+cWO_Num+" produced this error: (Office: "+cOffice+"): "+cStatus+CHR(10)+"Check EDI_TRIGGER and re-run"
	IF "TOTQTY ERR"$cStatus
		tmessage = tmessage + CHR(10) + "At OUTSHIPID: "+ALLTRIM(STR(m.outshipid))
	ENDIF
	DO FORM m:\dev\frm\dartmail2 WITH tsendto,tfrom,tsubject,tcc,tattach,tmessage,"A"
ENDIF

RETURN

****************************
PROCEDURE cstringbreak
****************************
cLen = LEN(ALLTRIM(cString))

FPUTS(nFilenum,cString)
RETURN

****************************
PROCEDURE segmentget
****************************
PARAMETER thisarray,lcKey,nLength

FOR i = 1 TO nLength
	IF i > nLength
		EXIT
	ENDIF
	lnEnd= AT("*",thisarray[i])
	IF lnEnd > 0
		lcThisKey =TRIM(SUBSTR(thisarray[i],1,lnEnd-1))
		IF OCCURS(lcKey,lcThisKey)>0
			RETURN SUBSTR(thisarray[i],lnEnd+1)
			i = 1
		ENDIF
	ENDIF
ENDFOR

RETURN ""

***********************
PROCEDURE closefiles
***********************
IF USED('outship')
	USE IN outship
ENDIF
IF USED('outdet')
	USE IN outdet
ENDIF

IF USED('serfile')
	SELECT serfile
	IF lTesting
		REPLACE serfile.seqnum WITH nOrigSeq
		REPLACE serfile.grpseqnum WITH nOrigGrpSeq
	ENDIF
	USE IN serfile
ENDIF
IF USED('scacs')
	USE IN scacs
ENDIF
IF USED('mm')
	USE IN mm
ENDIF
IF USED('tempx')
	USE IN tempx
ENDIF
IF !lTesting
	SELECT edi_trigger
	LOCATE
ENDIF
ENDPROC
