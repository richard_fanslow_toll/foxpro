lparameter lcoffice, xaccountid, xtest

lcoffice ="C"
goffice="6"
xaccountid = "6622"

runack("G3_INVENTORY")

do m:\dev\prg\_setvars with .f.

gsystemmodule="wh"

lcwhpath="F:\WH6\whdata\"

create cursor accts (thisacct int)

insert into accts (thisacct) values (6622)
insert into accts (thisacct) values (6639)
insert into accts (thisacct) values (6679)
insert into accts (thisacct) values (6621)
insert into accts (thisacct) values (6661)
insert into accts (thisacct) values (6666)
insert into accts (thisacct) values (6667)
insert into accts (thisacct) values (6623)
insert into accts (thisacct) values (6678)
insert into accts (thisacct) values (6614)


select accts
scan

*utilsetup("SENDINVENTORY")
	xscreencaption=_screen.caption
	_screen.caption="SENDINVENTORY for Account: "+transform(accts.thisacct)+" and Office: "+lcoffice

	guserid="SENDINVENTORY"
	gprocess=guserid
	gdevelopment=.f.

	xmod="Y"

	xacct = alltrim(transform(accts.thisacct))
	lcquery = "select * from upcmast where accountid = "+xacct
	useca("upcmast","wh",,,lcquery)
	select upcmast

	copy to h:\fox\tempupcmast
	use in upcmast

	use h:\fox\tempupcmast in 0 alias upcmast exclusive
	select upcmast
	index on style+color+id tag style

* get inventory

	xsqlexec("select units, style, color, id, availqty, allocqty, holdqty " + ;
	"from inven where accountid="+transform(accts.thisacct)+" and units=1","xdytemp",,"wh")

	select units, style, color, id, space(15) as upc, availqty, allocqty, holdqty, 0000000 as pickedqty, 0000000 as spqty ;
	from xdytemp into cursor inventory readwrite

	index on transform(units)+style+color+id tag match

* inprocess is a cursor of all outship records where they are not yet delivered and also excluding "notonwip"

	xsqlexec("select * from outship where mod='"+goffice+"' and accountid="+transform(accts.thisacct)+" " + ;
		"and del_date={} and notonwip=0 and qty#0 order by wo_num","inprocess",,"wh")

	xsqlexec("select units, style, color, id, pack, totqty from outship, outdet " + ;
		"where outship.outshipid=outdet.outshipid " + ;
		"and outship.mod='"+goffice+"' and outship.accountid="+transform(accts.thisacct)+" " + ;
		"and del_date={} and notonwip=0 and qty#0","xdy",,"wh") 

	select units, style, color, id, pack, sum(totqty) as pickedqty ;
		from xdy group by units, style, color, id, pack into cursor xunshipped
	
	select xunshipped
	scan
		=seek(transform(units)+style+color+id,"inventory","match")
		replace pickedqty with xunshipped.pickedqty in inventory
	endscan

	select inventory
	delete for availqty=0 and pickedqty=0 and holdqty=0 and spqty=0

	select inventory
	scan
		select upcmast
		=seek(inventory.style+inventory.color+inventory.id,"upcmast","style")
		if found("upcmast")
			replace inventory.upc with upcmast.upc
		else
			replace inventory.upc with "UNK"
		endif
	endscan

	do case
	case accts.thisacct = 6622
		lcfilename = "G-III_"+alltrim(transform(accts.thisacct))+"_ANDREW_MARC_"+ttoc(datetime(),1)
	case accts.thisacct = 6680
		lcfilename = "G-III_"+alltrim(transform(accts.thisacct))+"_BASSCOATS_"+ttoc(datetime(),1)
	case accts.thisacct = 6639
		lcfilename = "G-III_"+alltrim(transform(accts.thisacct))+"_CK_HANDBAGS_"+ttoc(datetime(),1)
	case accts.thisacct = 6679
		lcfilename = "G-III_"+alltrim(transform(accts.thisacct))+"_CK_MENS_OUTERWEAR_"+ttoc(datetime(),1)
	case accts.thisacct = 6621
		lcfilename = "G-III_"+alltrim(transform(accts.thisacct))+"_KENSIE_"+ttoc(datetime(),1)
	case accts.thisacct = 6661
		lcfilename = "G-III_"+alltrim(transform(accts.thisacct))+"_LEVIS_OUTERWEAR_"+ttoc(datetime(),1)
	case accts.thisacct = 6666
		lcfilename = "G-III_"+alltrim(transform(accts.thisacct))+"_MARC_NY_MEN_"+ttoc(datetime(),1)
	case accts.thisacct = 6667
		lcfilename = "G-III_"+alltrim(transform(accts.thisacct))+"_MARC_NY_WOMEN_"+ttoc(datetime(),1)
	case accts.thisacct = 6623
		lcfilename = "G-III_"+alltrim(transform(accts.thisacct))+"_PRIVATE_LABEL_"+ttoc(datetime(),1)
	case accts.thisacct = 6678
		lcfilename = "G-III_"+alltrim(transform(accts.thisacct))+"_TEAM_SPORTS_"+ttoc(datetime(),1)
	case accts.thisacct = 6614
		lcfilename = "G-III_"+alltrim(transform(accts.thisacct))+"_APPAREL_GROUP_"+ttoc(datetime(),1)
	endcase

*lcfilename = "G-III_"+lcoffice+"_"+Ttoc(Datetime(),1)
	lcfilename2 = "G-III_"+lcoffice+"_inproc_"+ttoc(datetime(),1)
	tsendto = 'pgaidis@fmiint.com,esther.ludwig@g-iii.com,agafur@g-iii.com'
*  tsendto = 'pgaidis@fmiint.com'

	xlsfilename = justfname(lcfilename)
	xlsfilename= "f:\ftpusers\G-III\inventory\"+xlsfilename+".csv"
	copy to &xlsfilename fields upc,style,color,id, availqty,allocqty,holdqty csv

	tmessage = "Updated inventory file in the G-III FTP Site.............."+chr(13)+chr(13)+;
	"This email is generated via an automated process"+chr(13)+"For changes in content or distribution please contact: Paul Gaidis @ 732-750-9000x143 "
	tsubject= "G-III Inventory Sent: " +ttoc(datetime())

*Esther Ludwig (esther.ludwig@g-iii.com)

*Afraz Gafur (agafur@g-iii.com)

	tattach = xlsfilename  &&+";"+xlsfilename2

	tfrom ="TGF Corporate <TGFSupplyChain@fmiint.com>"

	if !empty(tattach)
		do form dartmail2 with tsendto,tfrom,tsubject,"",tattach,tmessage,"A"
	endif

	use in upcmast

endscan

schedupdate()

on error
_screen.caption=xscreencaption
