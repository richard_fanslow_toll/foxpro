* MB added 2/15/2018 per Fanz.
sendupdate(alltrim(upper(sys(0))),alltrim(upper(sys(16,0))),"tgfnjsql01")

set safety off
close data all

utilsetup("BCNY_RPT_FILES")

set exclusive off
set enginebehavior 70
set deleted on

ltesting = .f.

_screen.windowstate = iif(ltesting,2,1)

goffice='2'
xsqlexec("select * from pt where mod='"+goffice+"'",,,"wh")
index on ptid tag ptid
set order to

xsqlexec("select * from ptdet where mod='"+goffice+"'",,,"wh")
index on ptdetid tag ptdetid
set order to

********POPULATE SALESREP WITH LONGTRKNUM FROM SHIPINS 

useca("outship","wh",,,"mod='2' and inlist(accountid,6221,6521) and del_date>{"+dtoc(date()-3)+"} and empty(salesrep)")
replace salesrep with getmemodata("shipins","LONGTRKNUM") for "LONGTRKNUM"$shipins
tu("outship")

upcmastsql(,,,,,"inlist(accountid,6221,6521)")
index on style tag style
index on str(accountid,4)+style+color+id tag stylecolid


*** added 10/8/15  TM to delete the last file created in the program and check for the existance of the file in a subsequent program for confirmation the script worked to normal EOJ
try
	delete file f:\ftpusers\synclaire\inventory\inventory.txt
catch
endtry

***********************************************************************************************************************************************************  START BCNY

*********ROUTING REPORT
xsqlexec("select consignee, ship_ref, start, cancel, del_date, bol_no, ship_via, scac, appt, appt_time, " + ;
	"appt_num, apptremarks, apptstatus, wo_num from outship where mod='2' and accountid=6221 and sp=0 " + ;
	"and (del_date>={"+dtoc(date()-20)+"} or del_date={}) and notonwip=0","tempb",,"wh")

select tempb
copy to f:\ftpusers\bcny\routing\routing.txt delimited with " " with character tab
copy to f:\ftpusers\bcny\routing\ARCHIVE\routing.txt delimited with " " with character tab

*set step on 

*********ORDER STATUS REPORT
xsqlexec("select outship.wo_date, ship_ref, consignee, style, totqty, cnee_ref, ship_via, start, cancel, " + ;
	"apptremarks, del_date, outship.wo_num, pulled, del_date, ptdate, pulleddt, picked, labeled, staged, " + ;
	"called, appt, truckloaddt, dept, ctnqty, outship.origqty, weight, cuft, storenum, dcnum, bol_no, " + ;
	"swcnum, vendor_num, scac, sforstore, address, address2, csz, keyrec, cancelrts " + ;
	"from outship, outdet where outship.outshipid=outdet.outshipid and outship.mod='2' " + ;
	"and outship.accountid=6221 and units=1","xdy",,"wh")

select *, space(30) as warehouse, space(30) as acct, "08:00 AM" as wo_time, "company" as company, ;
	"textbox32" as texbox32, "NO" as underalloc, "textbox28" as textbox28, ;
	iif(emptynul(del_date),"INPROCESS","COMPLETE"),"TOLL SANPEDRO" as facility, "          " as status ;
	from xdy into cursor temp1ab readwrite

replace all status with iif(cancelrts,"RET TO STOCK", iif(!pulled,"NOT PULLED",iif(!emptynul(del_date),"DELIVERED",iif(!emptynul(truckloaddt),"LOADED",iif(!emptynul(staged),"STAGED",iif(!emptynul(labeled),;
	"LABELED",iif(!emptynul(picked),"PICKED","PULLED"))))))) in temp1ab
replace all totqty with iif(cancelrts,0,totqty) in temp1ab
replace all ctnqty with iif(cancelrts,0,ctnqty) in temp1ab
replace all origqty with iif(cancelrts,0,origqty) in temp1ab
replace all weight with iif(cancelrts,0,weight) in temp1ab
replace all cuft with iif(cancelrts,0,cuft) in temp1ab



select warehouse, acct, wo_date, wo_time,ship_ref, consignee, style,;
	totqty, cnee_ref, ship_via,company,start,cancel, texbox32,apptremarks,underalloc,;
	textbox28, facility,wo_num,status,del_date	,ptdate,pulleddt,picked,labeled,staged	,;
	called	,appt,truckloaddt,dept,ctnqty	,origqty	,weight	,cuft,storenum	,dcnum	,;
	bol_no	,swcnum,vendor_num,scac,sforstore	,address,address2	,csz	,keyrec;
	from temp1ab into cursor temp2b readwrite

select space(30) as warehouse, space(30) as acct, ptdate as wo_date, space(10) as wo_time,pt.ship_ref, consignee,ptdet.style,;
	ptdet.totqty, pt.cnee_ref, pt.ship_via,"company" as company,start,cancel,"textbox32" as  textbox32,space(30) as apptremarks,"NO" as underalloc,;
	"textbox28" as textbox28, "TOLL SANPEDRO" as facility,0000000 as wo_num,"UNALLO" as status,{} as del_date	,ptdate,{} as pulleddt,{} as picked,{} as labeled,{} as staged	,;
	{} as called	,{} as appt,{} as truckloaddt,dept,000000 as ctnqty	,pt.origqty	,weight	,cuft,storenum	,dcnum	,;
	space(20) as bol_no	,space(7) as swcnum,space(10) as vendor_num,scac,sforstore	,address,address2	,csz	,space(25) as  keyrec;
	from pt, ptdet where pt.ptid=ptdet.ptid and pt.accountid=6221 into cursor temp3b readwrite
select * from temp2b union all select * from temp3b into cursor temp4b readwrite
*SET STEP ON
select temp4b
replace all warehouse with "Warehouse: Toll SanPedro"
replace all acct with "BCNY International"
select * from temp4b where totqty!=0 into cursor temp5b readwrite

select a.*, b.descrip from temp5b a left join upcmast b on a.style=b.style and b.accountid=6221 into cursor temp6b readwrite
select * from temp6b into cursor temp7b readwrite
select temp7b
copy to f:\ftpusers\bcny\orderstatus\orderstatus.txt delimited with " " with character tab
copy to f:\ftpusers\bcny\orderstatus\archive\orderstatus.txt delimited with " " with character tab

!f:\util\gzip\gzip.exe -f f:\ftpusers\bcny\orderstatus\orderstatus.txt

close data all
schedupdate()
**********************  now for the inventory file ***********************
set safety off
*SET STEP ON
upcmastsql(,,,,,"inlist(accountid,6221,6521)")
goffice='2'
xsqlexec("select * from pt where mod='"+goffice+"'",,,"wh")
index on ptid tag ptid
set order to
xsqlexec("select * from ptdet where mod='"+goffice+"'",,,"wh")
index on ptdetid tag ptdetid
set order to

select style,ptdet.units,sum(ptdet.totqty) as unitreq  from pt left outer join ptdet on pt.ptid = ptdet.ptid group by style where pt.finis = .f. ;
	and pt.accountid = 6221 order by style into cursor ptstuff1
select * from ptstuff1 where !isnull(style) into cursor ptstuff


*Use f:\ftpusers\bcny\data\inventory_template In 0
if !used("inventory_template")
	use s:\bcny\inventory_template in 0
endif

xsqlexec("select * from inven where mod='"+goffice+"' and accountid=6221 and totqty>0",,,"wh")

select * from inven where accountid = 6221 and totqty > 0  and units into cursor bcnydata

select inventory_template

copy to f:\ftpusers\bcny\data\inven_test
if !used("inven_test")
	use f:\ftpusers\bcny\data\inven_test in 0
endif
select bcnydata
scan
	select upcmast
	locate for style = bcnydata.style
	if found()
*	If upcmastsql(6221,bcnydata.style)
		select upcmast
		scatter memvar
	endif

	select inven_test
	append blank

	replace f1 with bcnydata.style
	replace f6 with "BCNY BRANDS"
	replace f18 with "TOLL SAN PEDRO"
	replace f20 with  m.descrip
	replace f25 with "EA"
	replace f28 with transform(bcnydata.allocqty)
	replace f29 with transform(bcnydata.availqty)
	replace f30 with transform(bcnydata.allocqty+bcnydata.availqty)
	replace f31 with transform(bcnydata.availqty+bcnydata.holdqty)    &&&  08/12/15  TM added    +bcnydata.holdqty  to show QC hold product
	replace f40 with m.upc
endscan

*Set Step On

select ptstuff
scan
	select inven_test
	locate for alltrim(ptstuff.style) = alltrim(inven_test.f1)
	if found()
		replace f28 with transform((val(alltrim(f28)) + ptstuff.unitreq))
		replace f29 with transform((val(alltrim(f29)) - ptstuff.unitreq))
		replace f31 with transform((val(alltrim(f31)) - ptstuff.unitreq))
		replace f30 with transform( val(alltrim(f30)) - val(alltrim(f28)))

*  replace f28 With Transform(bcnydata.allocqty)
*  replace f29 With Transform(bcnydata.availqty)
*  replace f30 With Transform(bcnydata.allocqty+bcnydata.availqty)
*  replace f31 With Transform(bcnydata.availqty)

	else
		select inven_test
		go top
		skip 1
		scatter memvar
		append blank
		gather memvar
		replace f1  with ptstuff.style
		replace f28 with transform(ptstuff.unitreq)
		replace f29 with transform("0")
		replace f31 with transform("0")

		replace f30 with transform(ptstuff.unitreq* -1)
*!*	    If inven_test.units
*!*	      && already "EA"
*!*	    Else
*!*	      replace f25 With "CS"
*!*	    Endif
*!*	 upcmastsql(,,,,,"inlist(accountid,6221,6521)")
*!*	index on style tag style
*!*	index on str(accountid,4)+style+color+id tag stylecolid
*!*			Select upcmast
*!*			=Seek("6221"+Padr(Alltrim(ptstuff.Style),20," ")+Space(10)+Space(10),"upcmast","stylecolid")
		if upcmastsql(6221,ptstuff.style)
			select upcmast
			scatter memvar
			replace inven_test.f20 with m.descrip
			replace inven_test.f40 with m.upc
		else
			replace inven_test.f20 with "UNK" in inven_test
			replace inven_test.f40 with "UNK" in inven_test
		endif
	endif
endscan

select inven_test
goto top

copy to f:\ftpusers\bcny\inventory\inventory.txt delimited with tab


*********************************************************************************************************************************************************  START SYNCLAIRE


set safety off

set exclusive off
upcmastsql(,,,,,"inlist(accountid,6521)")

goffice='2'
xsqlexec("select * from pt where mod='"+goffice+"'",,,"wh")
index on ptid tag ptid
set order to
xsqlexec("select * from ptdet where mod='"+goffice+"'",,,"wh")
index on ptdetid tag ptdetid
set order to

xsqlexec("select consignee, ship_ref, start, cancel, del_date, bol_no, ship_via, scac, appt, appt_time, " + ;
	"appt_num, apptremarks, apptstatus, wo_num from outship where mod='2' and accountid=6521 and sp=0 " + ;
	"and (del_date>={"+dtoc(date()-20)+"} or del_date={}) and notonwip=0","temp1aa",,"wh")

select temp1aa
copy to f:\ftpusers\synclaire\routing\routing.txt delimited with " " with character tab
copy to f:\ftpusers\synclaire\routing\ARCHIVE\routing.txt delimited with " " with character tab

*********ORDER STATUS REPORT
xsqlexec("select outship.wo_date, ship_ref, consignee, style, totqty, cnee_ref, ship_via, start, cancel, " + ;
	"apptremarks, del_date, outship.wo_num, pulled, del_date, ptdate, pulleddt, picked, labeled, staged, " + ;
	"called, appt, truckloaddt, dept, ctnqty, outship.origqty, weight, cuft, storenum, dcnum, bol_no, " + ;
	"swcnum, vendor_num, scac, sforstore, address, address2, csz, keyrec, cancelrts " + ;
	",salesrep from outship, outdet where outship.outshipid=outdet.outshipid and outship.mod='2' " + ;
	"and outship.accountid=6521 and units=1","xdy",,"wh")

select *, space(30) as warehouse, space(30) as acct, "08:00 AM" as wo_time, "company" as company, ;
	"textbox32" as textbox32, "NO" as underalloc, "textbox28" as textbox28, ;
	iif(emptynul(del_date),"INPROCESS","COMPLETE"),"TOLL SANPEDRO" as facility, "          " as status ;
	from xdy into cursor temp1a readwrite
	
replace all status with iif(cancelrts,"RET TO STOCK", iif(!pulled,"NOT PULLED",iif(!emptynul(del_date),"DELIVERED",iif(!emptynul(truckloaddt),"LOADED",iif(!emptynul(staged),"STAGED",iif(!emptynul(labeled),;
	"LABELED",iif(!emptynul(picked),"PICKED","PULLED"))))))) in temp1a
replace all totqty with iif(cancelrts,0,totqty)     in temp1a
replace all ctnqty with iif(cancelrts,0,ctnqty)   in temp1a
replace all origqty with iif(cancelrts,0,origqty)  in temp1a
replace all weight with iif(cancelrts,0,weight) in temp1a
replace all cuft with iif(cancelrts,0,cuft)   in temp1a
*REPLACE ALL bol_no WITH bol_no1 IN  temp1a
REPLACE ALL bol_no WITH salesrep FOR !emptynul(salesrep) IN  temp1a
******** GATHER OUTSHIP, OUTDET PURGED DATA

select warehouse, acct, wo_date, wo_time,ship_ref, consignee, style,;
	totqty, cnee_ref, ship_via,company,start,cancel, textbox32,apptremarks,underalloc,;
	textbox28, facility,wo_num,status,del_date	,ptdate,pulleddt,picked,labeled,staged	,;
	called	,appt,truckloaddt,dept,ctnqty	,origqty	,weight	,cuft,storenum	,dcnum	,;
	bol_no	,swcnum,vendor_num,scac,sforstore	,address,address2	,csz	,keyrec;
	from temp1a into cursor temp2 readwrite

select space(30) as warehouse, space(30) as acct, ptdate as wo_date, space(10) as wo_time,pt.ship_ref, consignee,ptdet.style,;
	ptdet.totqty, pt.cnee_ref, pt.ship_via,"company" as company,start,cancel,"textbox32" as  textbox32,space(30) as apptremarks,"NO" as underalloc,;
	"textbox28" as textbox28, "TOLL SANPEDRO" as facility,0000000 as wo_num,"UNALLO" as status,{} as del_date	,ptdate,{} as pulleddt,{} as picked,{} as labeled,{} as staged	,;
	{} as called	,{} as appt,{} as truckloaddt,dept,000000 as ctnqty	,pt.origqty	,weight	,cuft,storenum	,dcnum	,;
	space(20) as bol_no	,space(7) as swcnum,space(10) as vendor_num,scac,sforstore	,address,address2	,csz	,space(25) as  keyrec;
	from pt, ptdet where pt.ptid=ptdet.ptid and pt.accountid=6521 into cursor temp3 readwrite
select * from temp2 union all select * from temp3 into cursor temp4 readwrite
select temp4
replace all warehouse with "Warehouse: Toll SanPedro"
replace all acct with "Synclaire "
select * from temp4 where totqty!=0 into cursor temp5 readwrite

select a.*, b.descrip from temp5 a left join upcmast b on a.style=b.style and b.accountid=6521 into cursor temp6 readwrite
select * from temp6 into cursor temp7a readwrite


select temp7a

copy to f:\ftpusers\synclaire\orderstatus\archive\orderstatus.txt delimited with " " with character tab
copy to f:\ftpusers\synclaire\orderstatus\orderstatus.txt delimited with " " with character tab
!f:\util\gzip\gzip.exe -f f:\ftpusers\synclaire\orderstatus\orderstatus.txt




**********************  now for the inventory file ***********************

*set step on

set safety off

*Use f:\ftpusers\bcny\data\inventory_template In 0
if !used("inventory_template")
	use s:\bcny\inventory_template in 0
endif
goffice='2'
xsqlexec("select * from pt where mod='"+goffice+"'",,,"wh")
index on ptid tag ptid
set order to
xsqlexec("select * from ptdet where mod='"+goffice+"'",,,"wh")
index on ptdetid tag ptdetid
set order to

select style,ptdet.units,sum(ptdet.totqty) as unitreq  from pt left outer join ptdet on pt.ptid = ptdet.ptid group by style where pt.finis = .f. ;
	and pt.accountid = 6521 order by style into cursor  ptstuff1
select * from ptstuff1 where !isnull(style) into cursor ptstuff

xsqlexec("select * from inven where mod='"+goffice+"' and accountid=6521 and totqty>0",,,"wh")

select * from inven where accountid = 6521 and totqty > 0  and units into cursor bcnydata
if used("inven_test")
	use in inven_test
endif
select inventory_template
copy to f:\ftpusers\bcny\data\inven_test
if !used("inven_test")
	use f:\ftpusers\bcny\data\inven_test in 0
endif

select bcnydata
scan

*!*		Select upcmast
*!*		Locate For Style = bcnydata.Style
*!*		If Found()
	if upcmastsql(6521,bcnydata.style)
		select upcmast
		scatter memvar
	endif

	select inven_test
	append blank

	replace f1 with bcnydata.style
	replace f6 with "SYNCLAIRE BRANDS"
	replace f18 with "TOLL SAN PEDRO"
	replace f20 with  m.descrip
	replace f25 with "EA"
	replace f28 with transform(bcnydata.allocqty)
	replace f29 with transform(bcnydata.availqty)
	replace f30 with transform(bcnydata.allocqty+bcnydata.availqty)
	replace f31 with transform(bcnydata.availqty+bcnydata.holdqty)    &&&  08/12/15  TM added    +bcnydata.holdqty  to show QC hold product
	replace f40 with m.upc
endscan


select ptstuff
scan
	select inven_test
	locate for alltrim(ptstuff.style) = alltrim(inven_test.f1)
	if found()
		replace f28 with transform((val(alltrim(f28)) + ptstuff.unitreq))
		replace f29 with transform((val(alltrim(f29)) - ptstuff.unitreq))
		replace f31 with transform((val(alltrim(f31)) - ptstuff.unitreq))

		replace f30 with transform( val(alltrim(f30)) - val(alltrim(f28)))

*  replace f28 With Transform(bcnydata.allocqty)
*  replace f29 With Transform(bcnydata.availqty)
*  replace f30 With Transform(bcnydata.allocqty+bcnydata.availqty)
*  replace f31 With Transform(bcnydata.availqty)

	else
		select inven_test
		go top
		skip 1
		scatter memvar
		append blank
		gather memvar
		replace f1  with ptstuff.style
		replace f28 with transform(ptstuff.unitreq)
		replace f29 with transform("0")
		replace f31 with transform("0")

		replace f30 with transform(ptstuff.unitreq* -1)
		if ptstuff.units
&& already "EA"
		else
			replace f25 with "CS"
		endif
		select ptstuff
*!*	upcmastsql(,,,,,"inlist(accountid,6221,6521)")
*!*			Select upcmast
*!*			=Seek("6521"+Padr(Alltrim(ptstuff.Style),20," ")+Space(10)+Space(10),"upcmast","stylecolid")
		if upcmastsql(6521,ptstuff.style)
			select upcmast
			scatter memvar
			replace inven_test.f20 with m.descrip
			replace inven_test.f40 with m.upc
		else
			replace inven_test.f20 with "UNK" in inven_test
			replace inven_test.f40 with "UNK" in inven_test
		endif
	endif
endscan


select inven_test
goto top

copy to f:\ftpusers\synclaire\inventory\inventory.txt delimited with tab
set safety off




if !used("inventory_template")
	use s:\bcny\inventory_template in 0
endif
goffice='2'
xsqlexec("select * from pt where mod='"+goffice+"'",,,"wh")
index on ptid tag ptid
set order to
xsqlexec("select * from ptdet where mod='"+goffice+"'",,,"wh")
index on ptdetid tag ptdetid
set order to

xsqlexec("select * from inven where mod='"+goffice+"' and accountid=6521 and totqty#0",,,"wh")

select * from inven where .f. group by style, color, id into cursor vinven readwrite

select * from inven where accountid=6521 and !(totqty=0 and holdqty=0 and allocqty = 0) into cursor xinven
index on style tag style
set order to

select * from xinven group by style, color, id into cursor xsku
scan
**first insert all the cartons records - where !units AND pack#1 and !empty(length)
	select * from xinven where style=xsku.style and color=xsku.color and id=xsku.id and !units and !empty(length) and val(pack)#1 into cursor xtemp
	xtotqty=0
	xavailqty=0
	xallocqty=0
	xholdqty=0
	scan
		scatter memvar
		insert into vinven from memvar

		xtotqty=xtotqty+(m.totqty*val(pack))
		xavailqty=xavailqty+(m.availqty*val(pack))
		xallocqty=xallocqty+(m.allocqty*val(pack))
		xholdqty=xholdqty+(m.holdqty*val(pack))
	endscan
	use in xtemp

**insert unit record for remaining
	select * from xinven where style=xsku.style and color=xsku.color and id=xsku.id and units into cursor xtemp
	scatter memvar
	m.totqty=m.totqty-xtotqty
	m.availqty=m.availqty-xavailqty
	m.allocqty=m.allocqty-xallocqty
	m.holdqty=m.holdqty-xholdqty

	if m.totqty>0
		insert into vinven from memvar
	endif
	use in xtemp
endscan

use in xinven
use in xsku

select * from vinven into cursor vinven2

**change allocqty and availqty based off unallocated PTs
xsqlexec("select d.* from pt p left join ptdet d on d.ptid=p.ptid where p.accountid=6521 and p.mod='2'",'xpt',,"wh")
select * from xpt group by ship_ref into cursor xtemp
scan
	select xpt
	locate for ship_ref=xtemp.ship_ref and !units
	if found()
		delete for ship_ref=xtemp.ship_ref and units
	endif
endscan
use in xtemp
*SET STEP ON 
select style, pack, sum(totqty) as totqty from xpt group by style,pack into cursor xpt
scan
	select vinven
	locate for style=xpt.style and pack=xpt.pack
	if found()
		replace allocqty with allocqty+xpt.totqty, availqty with availqty-xpt.totqty
	else
		locate for style=xpt.style and units
		if !found()
			m.style=xpt.style
			m.units=.t.
			m.pack="1"
			insert into vinven from memvar
		endif
		replace allocqty with allocqty+(xpt.totqty*val(xpt.pack)), availqty with availqty-(xpt.totqty*val(xpt.pack))
	endif
endscan

use in xpt

**change allocqty and availqty based off pulled/unshipped wos
xsqlexec("select ship_ref, outdet.* from outship, outdet where outship.outshipid=outdet.outshipid and outship.mod='2' " + ;
	"and outship.accountid=6521 and del_date={} and pulleddt#{} and notonwip=0 and cancelrts=0 and sp=0 and qty>0","xout",,"wh")

select * from xout group by ship_ref into cursor xtemp
scan
	select xout
	locate for ship_ref=xtemp.ship_ref and !units
	if found()
		delete for ship_ref=xtemp.ship_ref and units
	endif
endscan
use in xtemp

select style, pack, sum(totqty) as totqty from xout group by style,pack into cursor xout
scan
	select vinven
	locate for style=xout.style and pack=xout.pack
	if found()
		replace totqty with totqty+xout.totqty, allocqty with allocqty+xout.totqty
	else
		locate for style=xout.style and units
		if !found()
			m.style=xout.style
			m.units=.t.
			m.pack="1"
			insert into vinven from memvar
		endif
		replace allocqty with allocqty+(xout.totqty*val(xout.pack)), totqty with totqty+(xout.totqty*val(xout.pack))
	endif
endscan

use in xout
select accountid, style,color,id,pack,totqty,holdqty,allocqty,availqty ,SPACE(2) as uom from vinven into cursor bcnydata readwrite
replace uom WITH 'EA' FOR pack='1'
replace uom WITH 'CS' FOR pack !='1'


if !used("inventory_template")
	use s:\bcny\inventory_template in 0
endif
if used("inven_test")
	use in inven_test
endif
select inventory_template
copy to f:\ftpusers\synclaire\data\inven_test
if !used("inven_test")
	use f:\ftpusers\synclaire\data\inven_test in 0
ENDIF
*SET STEP ON 
SELECT accountid,style,SPACE(10) as color,SPACE(10) as id, pack, SUM(totqty) as totqty, SUM(holdqty) as holdqty, SUM(allocqty) as allocqty, SUM(availqty) as availqty, uom FROM bcnydata GROUP BY accountid,style,pack,uom INTO CURSOR rrt1 READWRITE
SELECT * FROM rrt1 INTO CURSOR bcnydata readwrite


select bcnydata
xcounter=0
scan
*!*		Select upcmast
*!*		Locate For Style = bcnydata.Style
*!*		If Found()
	if upcmastsql(6521,bcnydata.style,bcnydata.color,bcnydata.id)
		select upcmast
		scatter memvar
	endif
	select inven_test
	append blank
	replace f1 with bcnydata.style
	if upcmast.accountid = 6221
		replace f6 with "BCNY BRANDS"
	else
		replace f6 with "SYNCLAIRE BRANDS"
	endif
	replace f18 with "TOLL SAN PEDRO"
	replace f20 with  upcmast.descrip
	replace f25 with bcnydata.uom
	replace f27 with transform(bcnydata.pack)
	replace f28 with transform(bcnydata.allocqty)
	replace f30 with transform(bcnydata.totqty)
	replace f31 with transform(bcnydata.availqty)
	replace f40 with upcmast.upc
	replace f41 with transform(bcnydata.holdqty)
	xcounter=xcounter+1
	wait "Processing Record "+transform(xcounter) window nowait noclear
endscan
wait clear

select inven_test
goto top

copy to f:\ftpusers\synclaire\inventory_cs\inventory.txt delimited with tab
