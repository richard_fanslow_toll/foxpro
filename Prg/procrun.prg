lparameters xproc

do case
case between(time(),"05:50:00","05:59:59") 
	xampm="A1"
case between(time(),"09:50:00","09:59:59") 
	xampm="A2"
case between(time(),"12:50:00","12:59:59") 
	xampm="P1"
case between(time(),"15:50:00","15:59:59")
	xampm="P2"
otherwise
	xampm="?"
endcase

use f:\auto\proccheck in 0

if !seek(dtos(date())+xampm,"proccheck","zdateampm")
	insert into proccheck (zdate, ampm) values (date(), xampm)
endif

xproc=lower(xproc)

do case
case xproc="stmmbennett2"
	xproc="stmmark"
case xproc="stmpayroll2"
	xproc="stmpayroll"
case xproc="stmvalsvr1"
	xproc="stmdev2"
case xproc="wwvalsvr1"
	xproc="wwdev2"
endcase

try
	replace &xproc with .t. in proccheck
catch
	email("Dyoung@fmiint.com","process ran unexpectedly: "+xproc,,,,,.t.,,,,,.t.,,.t.)
endtry

use in proccheck
