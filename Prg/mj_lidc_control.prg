utilsetup("MJ_LIDC_CONTROL")
Set Exclusive Off
Set Deleted On
Set Talk Off
Set Safety Off 
WITH _SCREEN	
	.AUTOCENTER = .T.
	.WINDOWSTATE = 0
	.BORDERSTYLE = 1
	.WIDTH = 320
	.HEIGHT = 210
	.TOP = 290
	.LEFT = 110
	.CLOSABLE = .T.
	.MAXBUTTON = .T.
	.MINBUTTON = .T.
	.SCROLLBARS = 0
	.CAPTION = "MJ_LIDC_CONTROL"
ENDWITH	

xsqlexec("select * from invenloc where mod='J'",,,"wh")

****checks for UNAUTHORIZED user adding to whseloc JLIDC

SELECT style,color,id,locqty,addby,adddt FROM invenloc WHERE WHSELOC='JLIDC' AND addby!='CLOZA' AND addby!='JIMK' AND addby!='LOAD'AND addby!='BMAR'  INTO CURSOR lidc1 readwrite
EXPORT TO C:\unauthorized_LIDC_creation XLS

SELECT lidc1
If Reccount() > 0
  tsendto = "Cesar.Lozada@tollgroup.com,bill.marsh@tollgroup.com"
  tattach = "C:\unauthorized_LIDC_creation.XLS"
  tcc =""
  tFrom ="Toll EDI System Operations <fmi-transload-ops@fmiint.com>"
  tmessage = "Unauthorized user added to JLIDC"
  tSubject = "Unauthorized user added to JLIDC"
  Do Form m:\dev\frm\dartmail2 With tsendto,tFrom,tSubject, tcc,tattach,tmessage  ,"A"
ELSE 
  tsendto = "tmarg@fmiint.com"
  tattach = ""
  tcc =""
  tFrom ="Toll EDI System Operations <fmi-transload-ops@fmiint.com>"
  tmessage = "NO Unauthorized user added to JLIDC  "+Ttoc(Datetime())
  tSubject = "NO Unauthorized user added to JLIDC"
  Do Form m:\dev\frm\dartmail2 With tsendto,tFrom,tSubject, tcc,tattach,tmessage  ,"A"
ENDIF



****** this identifies when a negative cycle (product found) exists that matches a SKU in JLIDC

SELECT style,color,id, LOCqty from invenloc WHERE locqty!=0 AND whseloc='JLIDC' INTO CURSOR LIDC READWRITE
SELECT style,color,id, LOCqty from invenloc WHERE locqty<0 AND whseloc='CYCLEZ'AND ACCOUNTID=6303 INTO CURSOR CYC1 READWRITE
SELECT * FROM CYC1 C LEFT JOIN LIDC L ON C.STYLE=L.STYLE AND C.COLOR=L.COLOR AND C.ID=L.ID INTO CURSOR LIDC_HIT READWRITE
SELECT style_a as style, color_a as color, id_a as id, locqty_a as CYCLEZ_qty, locqty_b as JLIDC_qty FROM LIDC_HIT WHERE !ISNULL(STYLE_B) INTO CURSOR LIDC_MATCH
SELECT lidc_match
EXPORT TO C:\found_sku_in_cyclez_match_to_lidc XLS

SELECT lidc_match
If Reccount() > 0
  tsendto = "Cesar.Lozada@tollgroup.com,bill.marsh@tollgroup.com"
  tattach = "C:\found_sku_in_cyclez_match_to_lidc.XLS"
  tcc =""
  tFrom ="Toll EDI System Operations <fmi-transload-ops@fmiint.com>"
  tmessage = "Found SKU in CYCLEZ that matches SKU in JLIDC"
  tSubject = "Found SKU in CYCLEZ that matches SKU in JLIDC"
  Do Form m:\dev\frm\dartmail2 With tsendto,tFrom,tSubject, tcc,tattach,tmessage  ,"A"
ELSE 
  tsendto = "tmarg@fmiint.com"
  tattach = ""
  tcc =""
  tFrom ="Toll EDI System Operations <fmi-transload-ops@fmiint.com>"
  tmessage = "NO SKU in CYCLEZ that matches SKU in JLIDC  "+Ttoc(Datetime())
  tSubject = "NO SKU in CYCLEZ that matches SKU in JLIDC"
  Do Form m:\dev\frm\dartmail2 With tsendto,tFrom,tSubject, tcc,tattach,tmessage  ,"A"
ENDIF


****** this dumps JLIDC records to the LAN

SELECT style,color,id, LOCqty, addby, adddt from invenloc WHERE locqty!=0 AND whseloc='JLIDC' INTO CURSOR temp21 READWRITE
EXPORT TO S:\MarcJacobsData\Reports\JLIDC\inventory\LIDC_inventory XLS

Close Data All
schedupdate()
_Screen.Caption=gscreencaption