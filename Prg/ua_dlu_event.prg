Parameters eventcode,trailer,IBD,asn,qty,qtybol,offset


  SET DATE YMD
  lcTStamp = STRTRAN(TTOC(DATETIME()),"/","-")
  lcTStamp = STRTRAN(lcTStamp," ","T")
  lcTStamp = STRTRAN(lcTStamp,"TPM","")
  lcTStamp = ALLTRIM(STRTRAN(lcTStamp,"TAM",""))

* 100000

  lcEventTime = STRTRAN(TTOC(Datetime()+offset),"/","-")
  lcEventTime = STRTRAN(lcEventTime," ","T")
  lcEventTime = STRTRAN(lcEventTime,"TPM","")
  lcEventTime = ALLTRIM(STRTRAN(lcEventTime,"TAM",""))

  lcHeader = FILETOSTR("f:\underarmour\xmltemplates\pod_event.txt")

  lcHeader = STRTRAN(lcHeader,"<DOCDATETIME>",lcTStamp)
  lcHeader = STRTRAN(lcHeader,"<IBD>",IBD)
  lcHeader = STRTRAN(lcHeader,"<ASN>",ASN)
  lcHeader = STRTRAN(lcHeader,"<TRAILER>","TR99867")
  lcHeader = STRTRAN(lcHeader,"<BOL>","0877876565445")
  lcHeader = STRTRAN(lcHeader,"<CARTONQTY>",qty)
  lcHeader = STRTRAN(lcHeader,"<CARTONQTYBOL>",qtybol)
*!* For 214 file types:

  lcFilestamp = Ttoc(Datetime())
  lcFilestamp = Strtran(lcFilestamp,"/","")
  lcFilestamp = Strtran(lcFilestamp,":","")
  lcFilestamp = Strtran(lcFilestamp," ","")
  STRTOFILE(lcHeader,"f:\ftpusers\underarmour\outbound\LSPDLU_"+IBD+"_"+"2800"+"_"+lcFilestamp+".xml")

