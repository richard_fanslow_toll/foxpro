* run GREAT WEST Payroll Census for 2008

LOCAL loGreatWestCensus
loGreatWestCensus = CREATEOBJECT('GreatWestCensus')
loGreatWestCensus.MAIN()
CLOSE DATABASES ALL
RETURN

#DEFINE LOGIT 1
#DEFINE WAITIT 2
#DEFINE NOWAITIT 4
#DEFINE SENDIT 8
#DEFINE CRLF CHR(13) + CHR(10)
#DEFINE RETURN_DATA_MANDATORY .F.
#DEFINE RETURN_DATA_NOT_MANDATORY .T.
#DEFINE CSVFORMAT 6
#DEFINE WRITE_ONLY_UNBUFFERED 11
#DEFINE COMMA ","
#DEFINE TAB_DELIMITER CHR(9)
#DEFINE SALARIED_HOURS_IN_YEAR (40 * 52)

DEFINE CLASS GreatWestCensus AS CUSTOM

	cProcessName = 'GreatWestCensus'

	lTestMode = .T.  && .F. FOR NORMAL OPERATION, .T. TO SUPPRESS ALL BUT DEVELOPER EMAILS AND CHANGE INPUT TABLE, ETC.

	nCensusYear = 2010
	cCensusYear = "2010"
	
	cPriorYear = "2009"

	dToday = DATE()

	lAutoYield = .T.

	cStartTime = TTOC(DATETIME())

	* connection properties
	nSQLHandle = 0

	* output file properties
	nCurrentOutputFileHandle = -1

	* wait window properties
	nWaitWindowTimeout = 2
	lWaitWindowIsOn = .T.

	* logfile properties
	lLoggingIsOn = .T.
	cLogFile = 'F:\UTIL\GreatWest\LOGFILES\GREAT_WEST_CENSUS_log.txt'

	* Deds K/O/N properties
	lGetOnlyLatestPaydate = .T.

	* INTERNAL email properties
	lSendInternalEmailIsOn = .T.
	cFrom = 'TGF Corporate <fmicorporate@fmiint.com>'
	cSendTo = 'Mark Bennett <mbennett@fmiint.com>'
	cCC = ''
	cSubject = ''
	cAttach = ''
	cBodyText = ''

	cCOMPUTERNAME = ''
	cUSERNAME = ''


	FUNCTION INIT
		IF NOT DODEFAULT()
			RETURN .F.
		ENDIF
		WITH THIS
			*SET RESOURCE OFF
			CLOSE DATA
			SET CENTURY ON
			SET DATE AMERICAN
			SET HOURS TO 24
			SET ANSI ON
			SET TALK OFF
			SET DELETED ON
			SET CONSOLE OFF
			SET EXCLUSIVE OFF
			SET SAFETY OFF
			SET EXACT OFF
			SET STATUS BAR ON
			SET SYSMENU OFF
			SET ENGINEBEHAVIOR 70
			_VFP.AUTOYIELD = .lAutoYield
			.cCOMPUTERNAME = GETENV("COMPUTERNAME")
			.cUSERNAME = GETENV("USERNAME")
			IF .lTestMode THEN
				.cLogFile = 'F:\UTIL\GreatWest\LOGFILES\GREAT_WEST_CENSUS_log_TESTMODE.txt'
				.cSendTo = 'Mark Bennett <mbennett@fmiint.com>'
				.cCC = ''
			ENDIF
			.lLoggingIsOn = .lLoggingIsOn AND NOT EMPTY(.cLogFile)
			IF .lLoggingIsOn THEN
				SET ALTERNATE TO (.cLogFile) ADDITIVE
				SET ALTERNATE ON
			ENDIF
		ENDWITH
	ENDFUNC


	FUNCTION DESTROY
		WITH THIS
			IF .lLoggingIsOn  THEN
				SET ALTERNATE OFF
				SET ALTERNATE TO
			ENDIF
		ENDWITH
		DODEFAULT()
	ENDFUNC


	FUNCTION MAIN
		WITH THIS
			LOCAL lnNumberOfErrors, loError, lnCensusYear, lcCensusYear
			LOCAL lcSQL, lcSQL2, lcSQL3, lcSQL4, lcSQL5, lcSQL6, lcSQL7, lcSQL8, lcSQL9
			LOCAL lcSQL6a, lcSQL6b, lcSQL6c, lcSQL6d, lcSQL6e, lcSQL6f, lcSQL6g, lcSQL6h
			LOCAL oExcel, oWorkbook, oWorksheet
			LOCAL lcOutputFilename, lcOutputLine, lcYTDPlanComp
			LOCAL lcPlanNumber, lcSSN, lcDivNum, lcLast, lcFirst, lcMiddle, lcSuffix, lcBirthDate, lcGender
			LOCAL lcMarital, lcAddress1, lcAddress2, lcCity, lcState, lcZip, lcHomePhone, lcWorkPhone, lcWorkPhoneExt, lcCountryCode
			LOCAL ldHireDate, lcTermDate, lcRehireDate, lcEEContribution, lcEmployerMatch, lcLoanRepayment, lcYTDHoursWorked, lcYTDTotCompensation
			LOCAL lcPreContrib, lcHighComp, lcBlanks, lcOfficer, lcPartDate, lcEligibleCode
			LOCAL lcName, lcStatus, lcPriorYearContrib, lcEEAfterTaxContrib, lnAdjustedTotPlanComp

			TRY
				lnNumberOfErrors = 0

				.TrackProgress('', LOGIT)
				.TrackProgress('', LOGIT)
				.TrackProgress('=========================================================', LOGIT+SENDIT)
				.TrackProgress('GREAT WEST CENSUS process started....', LOGIT+SENDIT+NOWAITIT)
				.TrackProgress('COMPUTERNAME = ' + .cCOMPUTERNAME, LOGIT+SENDIT)
				.TrackProgress('USERNAME = ' + .cUSERNAME, LOGIT+SENDIT)
				.TrackProgress('=========================================================', LOGIT+SENDIT)
				IF .lTestMode THEN
					.TrackProgress('                        TEST MODE                        ', LOGIT+SENDIT)
					.TrackProgress('=========================================================', LOGIT+SENDIT)
				ENDIF
				.TrackProgress('', LOGIT)

				* get ss#s from ADP
				OPEN DATABASE F:\UTIL\ADPCONNECTION\CONNECT1

				.nSQLHandle = SQLCONNECT("PCPAYWIN","MBENNETT","VP737A")

				IF .nSQLHandle > 0 THEN

					*!*							" AND (CHECKVIEWPAYDATE >= DATE '2005-01-01') " + ;
					*!*							" AND (CHECKVIEWPAYDATE <= DATE '2005-10-21') "
					
					lnCensusYear = .nCensusYear
					lcCensusYear = .cCensusYear
					.cSubject = 'Great West ' + lcCensusYear + ' Year End Census'

*!*						lcSQL = ;
*!*							"SELECT " + ;
*!*							" COMPANYCODE AS ADPCOMP, " + ;
*!*							" NAME, " + ;
*!*							" SOCIALSECURITY# AS SS_NUM, " + ;
*!*							" CHECKVIEWPAYDATE AS PAYDATE, " + ;
*!*							" CHECKVIEWDEDCODE AS DEDCODE, " + ;
*!*							" CHECKVIEWDEDAMT AS DEDAMT, " + ;
*!*							" FILE# AS FILENUM " + ;
*!*							" FROM REPORTS.V_CHK_VW_DEDUCTION " + ;
*!*							" WHERE ((CHECKVIEWDEDCODE = 'K') OR (CHECKVIEWDEDCODE = 'N')) " + ;
*!*							" AND CHECKVIEWYEAR# = " + lcCensusYear


					* per Stephanie, UNION to include those people who were not in 401k in 2008
					lcSQL = ;
						"SELECT " + ;
						" COMPANYCODE AS ADPCOMP, " + ;
						" NAME, " + ;
						" SOCIALSECURITY# AS SS_NUM, " + ;
						" CHECKVIEWPAYDATE AS PAYDATE, " + ;
						" CHECKVIEWDEDCODE AS DEDCODE, " + ;
						" CHECKVIEWDEDAMT AS DEDAMT, " + ;
						" FILE# AS FILENUM " + ;
						" FROM REPORTS.V_CHK_VW_DEDUCTION " + ;
						" WHERE ((CHECKVIEWDEDCODE = 'K') OR (CHECKVIEWDEDCODE = 'N')) " + ;
						" AND CHECKVIEWYEAR# = " + lcCensusYear + ;
						" UNION " + ;
						"SELECT DISTINCT " + ;
						" COMPANYCODE AS ADPCOMP, " + ;
						" NAME, " + ;
						" SOCIALSECURITY# AS SS_NUM, " + ;
						" DATE '1999-01-01' AS PAYDATE, " + ;
						" ' ' AS DEDCODE, " + ;
						" 0000.00 AS DEDAMT, " + ;
						" FILE# AS FILENUM " + ;
						" FROM REPORTS.V_CHK_VW_INFO " + ;
						" WHERE CHECKVIEWYEAR# = " + lcCensusYear						

					* sql for most employee attributes, this selects from the latest companycodes
					lcSQL2 = ;
						"SELECT " + ;
						" NAME, " + ;
						" LASTNAME, " + ;
						" FIRSTNAME, " + ;
						" SOCIALSECURITY# AS SS_NUM, " + ;
						" HOMEDEPARTMENT AS HOMEDEPT, " + ;
						" STREETLINE1 AS STREET1, " + ;
						" {fn IFNULL(STREETLINE2,' ')} AS STREET2, " + ;
						" CITY, " + ;
						" STATE, " + ;
						" ZIPCODE, " + ;
						" GENDER, " + ;
						" HIREDATE, " + ;
						" BIRTHDATE, " + ;
						" TERMINATIONDATE AS TERMDATE, " + ;
						" {FN IFNULL(ANNUALSALARY,0000000.00)} AS SALARY, " + ;
						" STATUS " + ;
						" FROM REPORTS.V_EMPLOYEE " + ;
						" WHERE COMPANYCODE IN ('E87','E88','E89') "

					* sql for most employee attributes, this selects from the OLD companycodes
					lcSQL2a = ;
						"SELECT " + ;
						" NAME, " + ;
						" LASTNAME, " + ;
						" FIRSTNAME, " + ;
						" SOCIALSECURITY# AS SS_NUM, " + ;
						" HOMEDEPARTMENT AS HOMEDEPT, " + ;
						" STREETLINE1 AS STREET1, " + ;
						" {fn IFNULL(STREETLINE2,' ')} AS STREET2, " + ;
						" CITY, " + ;
						" STATE, " + ;
						" ZIPCODE, " + ;
						" GENDER, " + ;
						" HIREDATE, " + ;
						" BIRTHDATE, " + ;
						" TERMINATIONDATE AS TERMDATE, " + ;
						" {FN IFNULL(ANNUALSALARY,0000000.00)} AS SALARY, " + ;
						" STATUS " + ;
						" FROM REPORTS.V_EMPLOYEE " + ;
						" WHERE COMPANYCODE IN ('SXI','ZXU','AXA') "

					lcSQL3 = ;
						"SELECT " + ;
						" SOCIALSECURITY# AS SS_NUM, " + ;
						" CHECKVIEWPAYDATE AS PAYDATE, " + ;
						" {FN IFNULL(CHECKVIEWMEMOAMT,0.00)} AS K_MATCH " + ;
						" FROM REPORTS.V_CHK_VW_MEMO " + ;
						" WHERE (CHECKVIEWMEMOCD = 'K') " + ;
						" AND CHECKVIEWYEAR# = " + lcCensusYear

					* COUNT # OF PAY WEEKS FOR SALARIED EE'S,
					* WILL BE USED TO CALCULATE TOTAL YTD HOURS
					lcSQL4 = ;
						"SELECT " + ;
						" SOCIALSECURITY# AS SS_NUM, " + ;
						" COUNT(DISTINCT CHECKVIEWWEEK#) AS NUM_WEEKS " + ;
						" FROM REPORTS.V_CHK_VW_INFO " + ;
						" WHERE COMPANYCODE NOT IN ('SXI','E87') " + ;
						" AND CHECKVIEWYEAR# = " + lcCensusYear + ;
						" GROUP BY SOCIALSECURITY# "

					* count hours of various types for Hourly
					lcSQL5 = ;
						"SELECT " + ;
						" SOCIALSECURITY# AS SS_NUM, " + ;
						" SUM({fn IFNULL(CHECKVIEWREGHOURS,0000.00)}) AS REG_HOURS, " + ;
						" SUM({fn IFNULL(CHECKVIEWOTHOURS,0000.00)}) AS OT_HOURS, " + ;
						" SUM({fn IFNULL(CHECKVIEWHOURSAMT,0000.00)}) AS OTHER_HRS " + ;
						" FROM REPORTS.V_CHK_VW_HOURS " + ;
						" WHERE COMPANYCODE IN ('SXI','E87') " + ;
						" AND CHECKVIEWYEAR# = " + lcCensusYear + ;
						" GROUP BY SOCIALSECURITY# "

					* census year total compensation
					lcSQL6 = ;
						"SELECT " + ;
						" SOCIALSECURITY# AS SS_NUM, " + ;
						" SUM({FN IFNULL(CHECKVIEWGROSSPAYA,0.00)}) AS GROSSPAY, " + ;
						" COUNT(*) AS NUM_PAYS " + ;
						" FROM REPORTS.V_CHK_VW_INFO " + ;
						" WHERE CHECKVIEWYEAR# = " + lcCensusYear + ;
						" GROUP BY SOCIALSECURITY# "
						
					******************************************************************************************
					******************************************************************************************
					* add more selects to capture: bonuses, commissions, opt-outs, and medical for the total year.
					* per Lucille, these need to be subtracted from the total grosspay (compensation)
					* only for column K - total compensation.
					
					* census year total bonuses
					lcSQL6a = ;
						"SELECT " + ;
						" SOCIALSECURITY# AS SS_NUM, " + ;
						" SUM({FN IFNULL(CHECKVIEWEARNSAMT,0.00)}) AS BONUSES " + ;
						" FROM REPORTS.V_CHK_VW_EARNINGS " + ;
						" WHERE CHECKVIEWYEAR# = " + lcCensusYear + ;
						" AND CHECKVIEWEARNSCD = 'B' " + ;
						" GROUP BY SOCIALSECURITY# " + ;
						" ORDER BY SOCIALSECURITY# "
					
					* census year total commissions
					lcSQL6b = ;
						"SELECT " + ;
						" SOCIALSECURITY# AS SS_NUM, " + ;
						" SUM({FN IFNULL(CHECKVIEWEARNSAMT,0.00)}) AS COMMISS " + ;
						" FROM REPORTS.V_CHK_VW_EARNINGS " + ;
						" WHERE CHECKVIEWYEAR# = " + lcCensusYear + ;
						" AND CHECKVIEWEARNSCD = '10' " + ;
						" GROUP BY SOCIALSECURITY# " + ;
						" ORDER BY SOCIALSECURITY# "
					
					* census year total opt-out
					lcSQL6c = ;
						"SELECT " + ;
						" SOCIALSECURITY# AS SS_NUM, " + ;
						" SUM({FN IFNULL(CHECKVIEWEARNSAMT,0.00)}) AS OPTOUT " + ;
						" FROM REPORTS.V_CHK_VW_EARNINGS " + ;
						" WHERE CHECKVIEWYEAR# = " + lcCensusYear + ;
						" AND CHECKVIEWEARNSCD = '1' " + ;
						" GROUP BY SOCIALSECURITY# " + ;
						" ORDER BY SOCIALSECURITY# "
					
					* census year total medical
					lcSQL6d = ;
						"SELECT " + ;
						" SOCIALSECURITY# AS SS_NUM, " + ;
						" SUM({FN IFNULL(CHECKVIEWDEDAMT,0.00)}) AS MEDICAL " + ;
						" FROM REPORTS.V_CHK_VW_DEDUCTION " + ;
						" WHERE CHECKVIEWYEAR# = " + lcCensusYear + ;
						" AND CHECKVIEWDEDCODE = 'H' " + ;
						" GROUP BY SOCIALSECURITY# " + ;
						" ORDER BY SOCIALSECURITY# "
						
					* census year total dental
					lcSQL6e = ;
						"SELECT " + ;
						" SOCIALSECURITY# AS SS_NUM, " + ;
						" SUM({FN IFNULL(CHECKVIEWDEDAMT,0.00)}) AS DENTAL " + ;
						" FROM REPORTS.V_CHK_VW_DEDUCTION " + ;
						" WHERE CHECKVIEWYEAR# = " + lcCensusYear + ;
						" AND CHECKVIEWDEDCODE = 'T' " + ;
						" GROUP BY SOCIALSECURITY# " + ;
						" ORDER BY SOCIALSECURITY# "

					* census year total car allowance
					lcSQL6f = ;
						"SELECT " + ;
						" SOCIALSECURITY# AS SS_NUM, " + ;
						" SUM({FN IFNULL(CHECKVIEWEARNSAMT,0.00)}) AS CARALLOW " + ;
						" FROM REPORTS.V_CHK_VW_EARNINGS " + ;
						" WHERE CHECKVIEWYEAR# = " + lcCensusYear + ;
						" AND CHECKVIEWEARNSCD = 'M' " + ;
						" GROUP BY SOCIALSECURITY# " + ;
						" ORDER BY SOCIALSECURITY# "

					* census year total severance
					lcSQL6g = ;
						"SELECT " + ;
						" SOCIALSECURITY# AS SS_NUM, " + ;
						" SUM({FN IFNULL(CHECKVIEWEARNSAMT,0.00)}) AS SEVERANCE " + ;
						" FROM REPORTS.V_CHK_VW_EARNINGS " + ;
						" WHERE CHECKVIEWYEAR# = " + lcCensusYear + ;
						" AND CHECKVIEWEARNSCD = 'Z' " + ;
						" GROUP BY SOCIALSECURITY# " + ;
						" ORDER BY SOCIALSECURITY# "
						
					******************************************************************************************
					******************************************************************************************						

					* prior year compensation
					lcSQL7 = ;
						"SELECT " + ;
						" SOCIALSECURITY# AS SS_NUM, " + ;
						" SUM({FN IFNULL(CHECKVIEWGROSSPAYA,0.00)}) AS GROSSPAY, " + ;
						" COUNT(*) AS NUM_PAYS " + ;
						" FROM REPORTS.V_CHK_VW_INFO " + ;
						" WHERE CHECKVIEWYEAR# = " + .cPriorYear + ;
						" GROUP BY SOCIALSECURITY# "

					* census year PRE-ENTRY compensation
					lcSQL8 = ;
						"SELECT " + ;
						" A.SOCIALSECURITY# AS SS_NUM, " + ;
						" SUM(A.CHECKVIEWGROSSPAYA) AS GROSSPAY " + ;
						" FROM REPORTS.V_CHK_VW_INFO A " + ;
						" WHERE A.CHECKVIEWYEAR# = " + lcCensusYear + ;
						" AND A.CHECKVIEWPAYDATE < " + ;						
						" (SELECT MIN(CHECKVIEWPAYDATE) " + ;
						" FROM REPORTS.V_CHK_VW_DEDUCTION " + ;
						" WHERE CHECKVIEWDEDCODE = 'K' " + ;
						" AND CHECKVIEWYEAR# = " + lcCensusYear + ;
						" AND SOCIALSECURITY# = A.SOCIALSECURITY#) " + ;
						" GROUP BY SOCIALSECURITY# "

					* census year PLAN ENTRY compensation
					lcSQL9 = ;
						"SELECT " + ;
						" A.SOCIALSECURITY# AS SS_NUM, " + ;
						" SUM(A.CHECKVIEWGROSSPAYA) AS GROSSPAY " + ;
						" FROM REPORTS.V_CHK_VW_INFO A " + ;
						" WHERE A.CHECKVIEWYEAR# = " + lcCensusYear + ;
						" AND A.CHECKVIEWPAYDATE >= " + ;						
						" (SELECT MIN(CHECKVIEWPAYDATE) " + ;
						" FROM REPORTS.V_CHK_VW_DEDUCTION " + ;
						" WHERE CHECKVIEWDEDCODE = 'K' " + ;
						" AND CHECKVIEWYEAR# = " + lcCensusYear + ;
						" AND SOCIALSECURITY# = A.SOCIALSECURITY#) " + ;
						" GROUP BY SOCIALSECURITY# "



					IF .lTestMode THEN
						.TrackProgress('                        TEST MODE                        ', LOGIT+SENDIT)
						.TrackProgress('lcSQL =' + lcSQL, LOGIT+SENDIT)
						.TrackProgress('                        TEST MODE                        ', LOGIT+SENDIT)
						.TrackProgress('lcSQL6 =' + lcSQL6, LOGIT+SENDIT)
						.TrackProgress('                        TEST MODE                        ', LOGIT+SENDIT)
						.TrackProgress('lcSQL7 =' + lcSQL7, LOGIT+SENDIT)
						.TrackProgress('                        TEST MODE                        ', LOGIT+SENDIT)
						.TrackProgress('lcSQL8 =' + lcSQL8, LOGIT+SENDIT)
						.TrackProgress('                        TEST MODE                        ', LOGIT+SENDIT)
						.TrackProgress('lcSQL9 =' + lcSQL9, LOGIT+SENDIT)
					ENDIF

					IF USED('SQLCURSOR1') THEN
						USE IN SQLCURSOR1
					ENDIF
					IF USED('CUREMPLOYEES') THEN
						USE IN CUREMPLOYEES
					ENDIF
					IF USED('CURKMATCH') THEN
						USE IN CURKMATCH
					ENDIF
					IF USED('CURYTDSALARY') THEN
						USE IN CURYTDSALARY
					ENDIF
					IF USED('CURYTDHOURLY') THEN
						USE IN CURYTDHOURLY
					ENDIF
					IF USED('CURTOTALCOMP') THEN
						USE IN CURTOTALCOMP
					ENDIF
					
					IF USED('CURBONUSES') THEN
						USE IN CURBONUSES
					ENDIF
					IF USED('CURCOMMISS') THEN
						USE IN CURCOMMISS
					ENDIF
					IF USED('CUROPTOUT') THEN
						USE IN CUROPTOUT
					ENDIF
					IF USED('CURMEDICAL') THEN
						USE IN CURMEDICAL
					ENDIF
					
					IF USED('CURPRIORYRPAY') THEN
						USE IN CURPRIORYRPAY
					ENDIF
					IF USED('CURPRECOMP') THEN
						USE IN CURPRECOMP
					ENDIF
					IF USED('CURPLANCOMP') THEN
						USE IN CURPLANCOMP
					ENDIF

					IF USED('CURDENTAL') THEN
						USE IN CURDENTAL
					ENDIF
					IF USED('CURCARALLOW') THEN
						USE IN CURCARALLOW
					ENDIF
					IF USED('CURSEVERANCE') THEN
						USE IN CURSEVERANCE
					ENDIF

					IF .ExecSQL(lcSQL, 'SQLCURSOR1', RETURN_DATA_MANDATORY) ;
							AND .ExecSQL(lcSQL2, 'CUREMPLOYEES', RETURN_DATA_MANDATORY) ;
							AND .ExecSQL(lcSQL2a, 'CUREMPLOYEESOLD', RETURN_DATA_MANDATORY) ;
							AND .ExecSQL(lcSQL3, 'CURKMATCH', RETURN_DATA_MANDATORY) ;
							AND .ExecSQL(lcSQL4, 'CURYTDSALARY', RETURN_DATA_MANDATORY) ;
							AND .ExecSQL(lcSQL5, 'CURYTDHOURLY', RETURN_DATA_MANDATORY) ;
							AND .ExecSQL(lcSQL6, 'CURTOTALCOMP', RETURN_DATA_MANDATORY) ;
							AND .ExecSQL(lcSQL6a, 'CURBONUSES', RETURN_DATA_MANDATORY) ;
							AND .ExecSQL(lcSQL6b, 'CURCOMMISS', RETURN_DATA_MANDATORY) ;
							AND .ExecSQL(lcSQL6c, 'CUROPTOUT', RETURN_DATA_MANDATORY) ;
							AND .ExecSQL(lcSQL6d, 'CURMEDICAL', RETURN_DATA_MANDATORY) ;
							AND .ExecSQL(lcSQL6e, 'CURDENTAL', RETURN_DATA_MANDATORY) ;
							AND .ExecSQL(lcSQL6f, 'CURCARALLOW', RETURN_DATA_MANDATORY) ;
							AND .ExecSQL(lcSQL6g, 'CURSEVERANCE', RETURN_DATA_MANDATORY) ;
							AND .ExecSQL(lcSQL7, 'CURPRIORYRPAY', RETURN_DATA_MANDATORY) ;
							AND .ExecSQL(lcSQL8, 'CURPRECOMP', RETURN_DATA_MANDATORY) ;
							AND .ExecSQL(lcSQL9, 'CURPLANCOMP', RETURN_DATA_MANDATORY) THEN

*!*	SELECT CURBONUSES
*!*	BROWSE
*!*	SELECT CURCOMMISS
*!*	BROWSE
*!*	SELECT CUROPTOUT
*!*	BROWSE
*!*	SELECT CURMEDICAL
*!*	BROWSE

*!*	CLOSE DATABASES ALL

*!*	X = 2*.F.

						IF USED('SQLCURSOR2') THEN
							USE IN SQLCURSOR2
						ENDIF

						* populate nulls
						SELECT CUREMPLOYEES
						SCAN
							IF ISNULL(CUREMPLOYEES.BIRTHDATE) THEN
								REPLACE CUREMPLOYEES.BIRTHDATE WITH {}
							ENDIF
							IF ISNULL(CUREMPLOYEES.HIREDATE) THEN
								REPLACE CUREMPLOYEES.HIREDATE WITH {}
							ENDIF
						ENDSCAN
						LOCATE
						SELECT CUREMPLOYEESOLD
						SCAN
							IF ISNULL(CUREMPLOYEESOLD.BIRTHDATE) THEN
								REPLACE CUREMPLOYEESOLD.BIRTHDATE WITH {}
							ENDIF
							IF ISNULL(CUREMPLOYEESOLD.HIREDATE) THEN
								REPLACE CUREMPLOYEESOLD.HIREDATE WITH {}
							ENDIF
						ENDSCAN
						LOCATE

*!*							* populate nulls
*!*							SELECT CURTOTALCOMP
*!*							SCAN
*!*								IF ISNULL(CURTOTALCOMP.GROSSPAY) THEN
*!*									REPLACE CURTOTALCOMP.GROSSPAY WITH 0.00
*!*								ENDIF
*!*							ENDSCAN
*!*							LOCATE
						
						

*!*							** setup file names

*!*							lcSpreadsheetTemplate = "M:\MarkB\templates\GREAT_WEST_CENSUS_TEMPLATE.XLS"

*!*							IF .lUseDateRange THEN
*!*								lcFiletoSaveAs = "F:\UTIL\GreatWest\REPORTS\MetLife Census File for " + .cRangeStartDate + " thru " + .cRangeEndDate + ".XLS"
*!*								.cSubject = 'Great West Payroll Data Interchange File, for Date Range: ' + .cRangeStartDate + ' thru ' + .cRangeEndDate
*!*							ELSE
*!*								lcFiletoSaveAs = "F:\UTIL\GreatWest\REPORTS\MetLife Census File for " + STRTRAN(DTOC(ldEndingPaydate),"/","-") + ".XLS"
*!*								.cSubject = 'Great West Weekly Report, for Pay Date: ' + DTOC(ldEndingPaydate)
*!*							ENDIF

*!*							IF FILE(lcFiletoSaveAs) THEN
*!*								DELETE FILE (lcFiletoSaveAs)
*!*							ENDIF


						* sum deductions by ss_num, date, paycode for latest date
						* (summing only because their *might* conceivably be more than one check per date,
						* and we don't want to miss anything in later lookups)
						IF USED('SQLCURSOR2') THEN
							USE IN SQLCURSOR2
						ENDIF

						SELECT ;
							SS_NUM, ;
							DEDCODE, ;
							SUM(DEDAMT) AS DEDAMT ;
							FROM SQLCURSOR1 ;
							INTO CURSOR SQLCURSOR2 ;
							GROUP BY 1, 2


						* roll up matches for latest paydate
						IF USED('CURKMATCH2') THEN
							USE IN CURKMATCH2
						ENDIF

						SELECT ;
							SS_NUM, ;
							SUM(K_MATCH) AS K_MATCH ;
							FROM CURKMATCH ;
							INTO CURSOR CURKMATCH2 ;
							GROUP BY 1

						*!*		SELECT CURKMATCH2
						*!*		BROWSE


						*!*							SELECT SQLCURSOR2
						*!*							GOTO TOP
						*!*							BROWSE

						* now create main report cursor of distinct ss#s from sqlcursor1
						IF USED('CURGW') THEN
							USE IN CURGW
						ENDIF

						SELECT ;
							SS_NUM, ;
							ADPCOMP, ;
							SPACE(30) AS NAME, ;
							SPACE(20) AS LASTNAME, ;
							SPACE(20) AS FIRSTNAME, ;
							0000.00 AS K_AMOUNT, ;
							0000.00 AS K_MATCH, ;
							0000.00 AS N_AMOUNT, ;
							00000000000.00 AS TOTCOMP, ;
							00000000000.00 AS BONUSES, ;
							00000000000.00 AS COMMISS, ;
							00000000000.00 AS OPTOUT, ;
							00000000000.00 AS MEDICAL, ;
							00000000000.00 AS DENTAL, ;
							00000000000.00 AS CARALLOW, ;
							00000000000.00 AS SEVERANCE, ;
							00000000000.00 AS PRECOMP, ;
							00000000000.00 AS PLANCOMP, ;
							00000000000.00 AS PRIORYRPAY, ;
							00000000000.00 AS SALARY, ;
							000000 AS YTDHOURS, ;
							SPACE(2) AS DIVISION, ;
							SPACE(30) AS STREET1, ;
							SPACE(30) AS STREET2, ;
							SPACE(20) AS CITY, ;
							SPACE(2) AS STATE, ;
							SPACE(10) AS ZIPCODE, ;
							SPACE(1) AS GENDER, ;
							SPACE(1) AS STATUS, ;
							{} AS HIREDATE, ;
							{} AS BIRTHDATE, ;
							{} AS TERMDATE ;
							FROM SQLCURSOR1 ;
							INTO CURSOR CURGW ;
							GROUP BY SS_NUM ;
							READWRITE

						SELECT CURGW
						SCAN
							* get Ded K amount
							SELECT SQLCURSOR2
							LOCATE FOR (SS_NUM = CURGW.SS_NUM) AND (DEDCODE = 'K')
							IF FOUND() THEN
								REPLACE CURGW.K_AMOUNT WITH SQLCURSOR2.DEDAMT
							ENDIF
							LOCATE


							* get 401k Match amount
							SELECT CURKMATCH2
							LOCATE FOR (SS_NUM = CURGW.SS_NUM)
							IF FOUND() THEN
								REPLACE CURGW.K_MATCH WITH CURKMATCH2.K_MATCH
							ENDIF
							LOCATE


							* get Ded N amount (401k loan repayment)
							SELECT SQLCURSOR2
							LOCATE FOR (SS_NUM = CURGW.SS_NUM) AND (DEDCODE = 'N')
							IF FOUND() THEN
								REPLACE CURGW.N_AMOUNT WITH SQLCURSOR2.DEDAMT
							ENDIF
							LOCATE

							* GET YTD HOURS
							IF INLIST(CURGW.ADPCOMP,'SXI','E87') THEN
								* HOURLY
								SELECT CURYTDHOURLY
								LOCATE FOR (SS_NUM = CURGW.SS_NUM)
								IF FOUND() THEN
									REPLACE CURGW.YTDHOURS WITH CURYTDHOURLY.REG_HOURS + CURYTDHOURLY.OT_HOURS + CURYTDHOURLY.OTHER_HRS
								ENDIF
								LOCATE

							ELSE
								* SALARIED
								SELECT CURYTDSALARY
								LOCATE FOR (SS_NUM = CURGW.SS_NUM)
								IF FOUND() THEN
									REPLACE CURGW.YTDHOURS WITH (CURYTDSALARY.NUM_WEEKS * 80)
								ENDIF
								IF CURGW.YTDHOURS > SALARIED_HOURS_IN_YEAR THEN
									REPLACE CURGW.YTDHOURS WITH SALARIED_HOURS_IN_YEAR
								ENDIF
								LOCATE
							ENDIF

							* GET TOTAL COMPENSATION
							SELECT CURTOTALCOMP
							LOCATE FOR (SS_NUM = CURGW.SS_NUM)
							IF FOUND() THEN
								REPLACE CURGW.TOTCOMP WITH CURTOTALCOMP.GROSSPAY
							ENDIF
							LOCATE

							* GET PLAN COMPENSATION
							SELECT CURPLANCOMP
							LOCATE FOR (SS_NUM = CURGW.SS_NUM)
							IF FOUND() THEN
								REPLACE CURGW.PLANCOMP WITH CURPLANCOMP.GROSSPAY
							ENDIF
							LOCATE
							
							************************************************************
							************************************************************
							* get items which will be subtracted from plan compensation
							
							* bonuses
							SELECT CURBONUSES
							LOCATE FOR (SS_NUM = CURGW.SS_NUM)
							IF FOUND() THEN
								REPLACE CURGW.BONUSES WITH CURBONUSES.BONUSES
							ENDIF
							LOCATE
							
							* commissions
							SELECT CURCOMMISS
							LOCATE FOR (SS_NUM = CURGW.SS_NUM)
							IF FOUND() THEN
								REPLACE CURGW.COMMISS WITH CURCOMMISS.COMMISS
							ENDIF
							LOCATE
							
							* opt-out
							SELECT CUROPTOUT
							LOCATE FOR (SS_NUM = CURGW.SS_NUM)
							IF FOUND() THEN
								REPLACE CURGW.OPTOUT WITH CUROPTOUT.OPTOUT
							ENDIF
							LOCATE
							
							* medical
							SELECT CURMEDICAL
							LOCATE FOR (SS_NUM = CURGW.SS_NUM)
							IF FOUND() THEN
								REPLACE CURGW.MEDICAL WITH CURMEDICAL.MEDICAL
							ENDIF
							LOCATE
							
							* dental
							SELECT CURDENTAL
							LOCATE FOR (SS_NUM = CURGW.SS_NUM)
							IF FOUND() THEN
								REPLACE CURGW.DENTAL WITH CURDENTAL.DENTAL
							ENDIF
							LOCATE
							
							* car allow
							SELECT CURCARALLOW
							LOCATE FOR (SS_NUM = CURGW.SS_NUM)
							IF FOUND() THEN
								REPLACE CURGW.CARALLOW WITH CURCARALLOW.CARALLOW
							ENDIF
							LOCATE
							
							* severance
							SELECT CURSEVERANCE
							LOCATE FOR (SS_NUM = CURGW.SS_NUM)
							IF FOUND() THEN
								REPLACE CURGW.SEVERANCE WITH CURSEVERANCE.SEVERANCE
							ENDIF
							LOCATE
							
							************************************************************
							************************************************************

							* GET PRE-ENTRY COMPENSATION
							SELECT CURPRECOMP
							LOCATE FOR (SS_NUM = CURGW.SS_NUM)
							IF FOUND() THEN
								REPLACE CURGW.PRECOMP WITH CURPRECOMP.GROSSPAY
							ENDIF
							LOCATE

							* GET PRIOR YEAR'S PAY
							SELECT CURPRIORYRPAY
							LOCATE FOR (SS_NUM = CURGW.SS_NUM)
							IF FOUND() THEN
								REPLACE CURGW.PRIORYRPAY WITH CURPRIORYRPAY.GROSSPAY
							ENDIF
							LOCATE

							* get additional info from employee table:
							* FIRST TRY "NEW" COMPANYCODES SELECT
							SELECT CUREMPLOYEES
							LOCATE FOR (SS_NUM = CURGW.SS_NUM)
							IF FOUND() THEN
								REPLACE ;
									CURGW.NAME WITH CUREMPLOYEES.NAME, ;
									CURGW.LASTNAME  WITH CUREMPLOYEES.LASTNAME, ;
									CURGW.FIRSTNAME WITH CUREMPLOYEES.FIRSTNAME, ;
									CURGW.DIVISION WITH LEFT(CUREMPLOYEES.HOMEDEPT,2), ;
									CURGW.STREET1 WITH CUREMPLOYEES.STREET1, ;
									CURGW.STREET2 WITH CUREMPLOYEES.STREET2, ;
									CURGW.CITY WITH CUREMPLOYEES.CITY, ;
									CURGW.STATE WITH CUREMPLOYEES.STATE, ;
									CURGW.ZIPCODE WITH CUREMPLOYEES.ZIPCODE, ;
									CURGW.GENDER WITH IIF(ISNULL(CUREMPLOYEES.GENDER),'?',CUREMPLOYEES.GENDER), ;
									CURGW.STATUS WITH CUREMPLOYEES.STATUS, ;
									CURGW.SALARY WITH CUREMPLOYEES.SALARY, ;
									CURGW.HIREDATE WITH TTOD(CUREMPLOYEES.HIREDATE), ;
									CURGW.BIRTHDATE WITH TTOD(CUREMPLOYEES.BIRTHDATE)

								IF NOT ISNULL(CUREMPLOYEES.TERMDATE) THEN
									REPLACE CURGW.TERMDATE WITH TTOD(CUREMPLOYEES.TERMDATE)
								ENDIF
								
							ELSE
								* match not found for New company codes; try cursor from old ones
								SELECT CUREMPLOYEESOLD
								LOCATE FOR (SS_NUM = CURGW.SS_NUM)
								IF FOUND() THEN
									REPLACE ;
										CURGW.NAME WITH CUREMPLOYEESOLD.NAME, ;
										CURGW.LASTNAME  WITH CUREMPLOYEESOLD.LASTNAME, ;
										CURGW.FIRSTNAME WITH CUREMPLOYEESOLD.FIRSTNAME, ;
										CURGW.DIVISION WITH LEFT(CUREMPLOYEESOLD.HOMEDEPT,2), ;
										CURGW.STREET1 WITH CUREMPLOYEESOLD.STREET1, ;
										CURGW.STREET2 WITH CUREMPLOYEESOLD.STREET2, ;
										CURGW.CITY WITH CUREMPLOYEESOLD.CITY, ;
										CURGW.STATE WITH CUREMPLOYEESOLD.STATE, ;
										CURGW.ZIPCODE WITH CUREMPLOYEESOLD.ZIPCODE, ;
										CURGW.GENDER WITH IIF(ISNULL(CUREMPLOYEESOLD.GENDER),'?',CUREMPLOYEESOLD.GENDER), ;
										CURGW.STATUS WITH CUREMPLOYEESOLD.STATUS, ;
										CURGW.SALARY WITH CUREMPLOYEESOLD.SALARY, ;
										CURGW.HIREDATE WITH TTOD(CUREMPLOYEESOLD.HIREDATE), ;
										CURGW.BIRTHDATE WITH TTOD(CUREMPLOYEESOLD.BIRTHDATE)

									IF NOT ISNULL(CUREMPLOYEESOLD.TERMDATE) THEN
										REPLACE CURGW.TERMDATE WITH TTOD(CUREMPLOYEESOLD.TERMDATE)
									ENDIF									
								ENDIF
								LOCATE
							ENDIF
							LOCATE

						ENDSCAN

						*** SORT HERE !!!!
						IF USED('CURGWCENSUS') THEN
							USE IN CURGWCENSUS
						ENDIF
						SELECT * FROM CURGW INTO CURSOR CURGWCENSUS ORDER BY NAME

						***********************************************************************************
						* Create tab-delimited output census file

						* open output file
						lcOutputFilename = "F:\UTIL\GREATWEST\CENSUSES\CENSUS" + lcCensusYear + ".TXT"
						.nCurrentOutputFileHandle = FCREATE(lcOutputFilename)
						.TrackProgress('nCurrentOutputFileHandle = ' + TRANSFORM(.nCurrentOutputFileHandle), LOGIT+SENDIT)
						IF .nCurrentOutputFileHandle < 0 THEN
							.TrackProgress('***** Error on FCREATE of: ' + lcOutputFilename, LOGIT+SENDIT)
						ELSE
							.TrackProgress('FCREATEed: ' + lcOutputFilename, LOGIT+SENDIT)
						ENDIF
						
*!*							* populate the output file
SELECT CURGWCENSUS
COPY TO C:\A\CENSUS.XLS XL5 && BROW

						
						SELECT CURGWCENSUS
						SCAN
							lcName = UPPER(ALLTRIM(CURGWCENSUS.LASTNAME)) + COMMA + UPPER(ALLTRIM(CURGWCENSUS.FIRSTNAME))
							* status = T for terminated, otherwise blank, per Stephanie 1/30/08 MB
							lcStatus = IIF(CURGWCENSUS.STATUS='T',CURGWCENSUS.STATUS,' ')
						
							lcPlanNumber = "455221-01"
							lcSSN = CURGWCENSUS.SS_NUM
							lcDivNum = ""
							lcLast = CURGWCENSUS.LASTNAME
							lcFirst = CURGWCENSUS.FIRSTNAME
							lcMiddle = ""
							lcSuffix = ""
							lcBirthDate = CURGWCENSUS.BIRTHDATE
							lcGender = CURGWCENSUS.GENDER
							lcMarital = "??marital???"
							lcAddress1 = CURGWCENSUS.STREET1
							lcAddress2 = CURGWCENSUS.STREET2
							lcCity = CURGWCENSUS.CITY
							lcState = UPPER(CURGWCENSUS.STATE)
							lcZip = CURGWCENSUS.ZIPCODE
							lcHomePhone = ""
							lcWorkPhone = ""
							lcWorkPhoneExt = ""
							lcCountryCode = ""
							ldHireDate = CURGWCENSUS.HIREDATE
							IF CURGWCENSUS.STATUS = 'T' AND NOT ISNULL(CURGWCENSUS.TERMDATE) THEN
								lcTermDate = DTOC(CURGWCENSUS.TERMDATE)
							ELSE
								lcTermDate = ""
							ENDIF
							lcRehireDate = ""
							lcEEContribution = ALLTRIM(STR(CURGWCENSUS.K_AMOUNT,11,2))
							lcEmployerMatch = ALLTRIM(STR(CURGWCENSUS.K_MATCH,11,2))
							lcLoanRepayment = ALLTRIM(STR(CURGWCENSUS.N_AMOUNT,11,2))
							lcZeroContrib = ALLTRIM(STR(0.00,11,2))
							lcYTDHoursWorked = ALLTRIM(STR(INT(CURGWCENSUS.YTDHOURS)))
							
							lcYTDTotCompensation = ALLTRIM(STR(INT(CURGWCENSUS.TOTCOMP)))
							
							**************************************************************
							**************************************************************
							* 03/06/2009:
							* per Lucille, subtract some earnings and deductions, from census year total plan comp only
							
							*lcYTDPlanComp = ALLTRIM(STR(CURGWCENSUS.PLANCOMP,11,2))							
							
	lnAdjustedTotPlanComp = ;
	CURGWCENSUS.TOTCOMP - CURGWCENSUS.CARALLOW - CURGWCENSUS.K_AMOUNT - CURGWCENSUS.SEVERANCE - CURGWCENSUS.MEDICAL - CURGWCENSUS.DENTAL
	lcYTDPlanComp = ALLTRIM(STR(lnAdjustedTotPlanComp,11,2))							
							
							**************************************************************
							**************************************************************
							
							*lcPlanContrib = ALLTRIM(STR(CURGWCENSUS.PLANCOMP,11,2))
							
							IF (CURGWCENSUS.K_AMOUNT = 0.00) THEN
								* employee had no 401k deferrals, set their 'Plan Compensation' to zero per Stephanie
								*(this is for the people who were not in 401k who were UNIONed in)
								lcYTDPlanComp = lcZeroContrib
*!*								ELSE
*!*									* person was in 401k plan
*!*									lcYTDPlanComp = lcPlanContrib
							ENDIF
							
							lcPreContrib = ALLTRIM(STR(CURGWCENSUS.PRECOMP,11,2))
							
							lcEEAfterTaxContrib = ALLTRIM(STR(0.00,11,2))
							lcEmployerNonElective = ALLTRIM(STR(0.00,11,2))
							lcEERothContrib = ALLTRIM(STR(0.00,11,2))
							
							IF CURGWCENSUS.PRIORYRPAY > 100000 THEN
								lcPriorYearContrib = ALLTRIM(STR(CURGWCENSUS.PRIORYRPAY,11,2))
							ELSE
								lcPriorYearContrib = ALLTRIM(STR(0.00,11,2))
							ENDIF
							
							IF CURGWCENSUS.SALARY > 80000 THEN
								lcHighComp = "Y"
							ELSE
								lcHighComp = "N"
							ENDIF

							IF INLIST(CURGWCENSUS.ADPCOMP,'AXA','E89') THEN
								lcOfficer = "Y"
							ELSE
								lcOfficer = "N"
							ENDIF
							lcBlanks = ""
							lcPartDate = "?part date?"
							lcEligibleCode = "?elig code?"

							* Write Detail Line
							lcOutputLine = ;
								.CleanUpField( lcName ) + TAB_DELIMITER + ;
								.CleanUpField( lcStatus ) + TAB_DELIMITER + ;
								.CleanUpField( lcSSN ) + TAB_DELIMITER + ;
								.CleanUpField( lcBirthDate ) + TAB_DELIMITER + ;
								.CleanUpField( lcYTDHoursWorked ) + TAB_DELIMITER + ;
								.CleanUpField( ldHireDate ) + TAB_DELIMITER + ;
								.CleanUpField( lcTermDate ) + TAB_DELIMITER + ;
								.CleanUpField( lcRehireDate ) + TAB_DELIMITER + ;
								.CleanUpField( lcPreContrib ) + TAB_DELIMITER + ;
								.CleanUpField( lcYTDPlanComp ) + TAB_DELIMITER + ;
								.CleanUpField( lcYTDTotCompensation ) + TAB_DELIMITER + ;
								.CleanUpField( lcPriorYearContrib ) + TAB_DELIMITER + ;
								.CleanUpField( lcEEContribution ) + TAB_DELIMITER + ;
								.CleanUpField( lcEmployerMatch ) + TAB_DELIMITER + ;
								.CleanUpField( lcEEAfterTaxContrib ) + TAB_DELIMITER + ;
								.CleanUpField( lcEmployerNonElective ) + TAB_DELIMITER + ;
								.CleanUpField( lcEERothContrib ) + TAB_DELIMITER + ;
								.CleanUpField( lcOfficer ) 

							=FPUTS(.nCurrentOutputFileHandle,lcOutputLine)
							
						ENDSCAN


						* close output file
						IF .nCurrentOutputFileHandle > -1 THEN
							llRetval = FCLOSE(.nCurrentOutputFileHandle)
						ENDIF


						***********************************************************************************
						***********************************************************************************
						***********************************************************************************
						***********************************************************************************


*!*							IF FILE(lcOutputFilename) THEN
*!*								* attach xls output file to email
*!*								.cAttach = lcOutputFilename
*!*								.cBodyText = "See attached GREAT WEST CENSUS Files." + ;
*!*									CRLF + CRLF + "(do not reply - this is an automated report)" + ;
*!*									CRLF + CRLF + "<report log follows>" + ;
*!*									CRLF + .cBodyText
*!*							ELSE
*!*								.TrackProgress('ERROR: unable to email file: ' + lcOutputFilename, LOGIT+SENDIT+NOWAITIT)
*!*							ENDIF
						
						CLOSE DATABASES ALL
					ENDIF  &&  .ExecSQL(lcSQL, 'SQLCURSOR1', RETURN_DATA_MANDATORY)

					.TrackProgress('GREAT WEST CENSUS process ended normally.', LOGIT+SENDIT+NOWAITIT)
				ELSE
					* connection error
					.TrackProgress('Unable to connect to ADP Payroll System.', LOGIT+SENDIT)
				ENDIF   &&  .nSQLHandle > 0

			CATCH TO loError

				.TrackProgress('There was an error.',LOGIT+SENDIT)
				.TrackProgress('The Error # is: ' + TRANSFORM(loError.ERRORNO), LOGIT+SENDIT)
				.TrackProgress('The Error Message is: ' + TRANSFORM(loError.MESSAGE), LOGIT+SENDIT)
				.TrackProgress('The Error occurred in Program: ' + TRANSFORM(loError.PROCEDURE), LOGIT+SENDIT)
				.TrackProgress('The Error occurred at line #: ' + TRANSFORM(loError.LINENO), LOGIT+SENDIT)
				lnNumberOfErrors = lnNumberOfErrors + 1
				IF TYPE('oWorkbook') = "O" THEN
					oWorkbook.SAVE()
				ENDIF
				IF TYPE('oExcel') = "O" THEN
					oExcel.QUIT()
				ENDIF
				CLOSE DATABASES ALL

			ENDTRY

			CLOSE DATA
			WAIT CLEAR
			***************** INTERNAL email results ******************************
			.TrackProgress('About to send status email.',LOGIT)
			.TrackProgress('The logfile is: ' + .cLogFile, SENDIT)
			.TrackProgress('GREAT WEST CENSUS process started: ' + .cStartTime, LOGIT+SENDIT)
			.TrackProgress('GREAT WEST CENSUS process finished: ' + TTOC(DATETIME()), LOGIT+SENDIT)

			IF .lSendInternalEmailIsOn THEN
				DO FORM dartmail2 WITH .cSendTo,.cFrom,.cSubject,.cCC,.cAttach,.cBodyText,"A"
				.TrackProgress('Sent status email.',LOGIT)
			ELSE
				.TrackProgress('Did not send status email: THIS.lSendInternalEmailIsOn = FALSE.',LOGIT)
			ENDIF

		ENDWITH
		RETURN
	ENDFUNC && main


*!*		FUNCTION CleanUpField
*!*			* prepare field for inclusion in the string to be written with FPUTS().
*!*			* especially critical to remove any commas, since that creates bogus column in CSV output file.
*!*			LPARAMETERS tuValue
*!*			LOCAL lcRetVal
*!*			lcRetVal = STRTRAN(ALLTRIM(TRANSFORM(tuValue)),",","")
*!*			RETURN lcRetVal
*!*		ENDFUNC
	
	FUNCTION CleanUpField
		LPARAMETERS tuValue
		lcRetVal = ALLTRIM(TRANSFORM(tuValue))
		RETURN lcRetVal
	ENDFUNC


	FUNCTION ExecSQL
		LPARAMETERS tcSQL, tcCursorName, tlNoDataReturnedIsOkay
		LOCAL llRetval, lnResult
		WITH THIS
			* close target cursor if it's open
			IF USED(tcCursorName)
				USE IN (tcCursorName)
			ENDIF
			WAIT WINDOW "Fetching remote data; please wait..." NOWAIT
			lnResult = SQLEXEC(.nSQLHandle, tcSQL, tcCursorName)
			llRetval = ( lnResult > 0 )
			IF llRetval THEN
				* see if any data came back
				IF NOT tlNoDataReturnedIsOkay THEN
					IF (NOT USED(tcCursorName)) OR EOF(tcCursorName) THEN
						llRetval = .F.
						.TrackProgress('No data was returned by this query: ' +CRLF + tcSQL, LOGIT+SENDIT)
						.cSendTo = 'Mark Bennett <mbennett@fmiint.com>'
					ENDIF
				ENDIF
			ELSE
				.TrackProgress("Result Code [" + TRANSFORM(lnResult) + "] returned by this query: " + tcSQL, LOGIT+SENDIT)
				.cSendTo = 'Mark Bennett <mbennett@fmiint.com>'
			ENDIF
			WAIT CLEAR
			RETURN llRetval
		ENDWITH
	ENDFUNC


	PROCEDURE TrackProgress
		* do any combination of Wait Window, writing to logfile, and adding to body of email,
		* based on nFlags parameter.
		LPARAMETERS tcExpression, tnFlags
		WITH THIS
			IF BITAND(tnFlags,LOGIT) = LOGIT THEN
				IF .lLoggingIsOn THEN
					?
					? .cProcessName + "  " + TTOC(DATETIME()) + ": " + tcExpression
				ENDIF
			ENDIF
			IF .lWaitWindowIsOn THEN
				IF BITAND(tnFlags,NOWAITIT) = NOWAITIT THEN
					WAIT WINDOW tcExpression NOWAIT
				ENDIF
				IF BITAND(tnFlags,WAITIT) = WAITIT THEN
					WAIT WINDOW tcExpression TIMEOUT .nWaitWindowTimeout
				ENDIF
			ENDIF
			IF BITAND(tnFlags,SENDIT) = SENDIT THEN
				IF .lSendInternalEmailIsOn THEN
					.cBodyText = .cBodyText + tcExpression + CRLF + CRLF
				ENDIF
			ENDIF
		ENDWITH
	ENDPROC  &&  TrackProgress

ENDDEFINE

