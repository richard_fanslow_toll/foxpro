* updates inven & invenloc when an adj record is added
* updates removeqty field in indet and/or adj if negative

lparameter xnoinven, xfromphys, xonhold, xppfilter, xnoapplyremove, xpalletid, xrem

if gmasteroffice="C"
	xonhold=.f.		&& request from Ed K because of QCMODD moves - dy 6/18/11
endif

if empty(xppfilter)
	xppfilter=".t."
endif

select adj

xinadjchange=abs(adj.totqty)
xholdreturn=.f.

if !adj.inadj	&& remove inventory, set removeqty in either indet/inloc or adj

	if !xnoinven
		if seek(str(adj.accountid,4)+trans(adj.units)+adj.style+adj.color+adj.id+adj.pack,"inven","match")
			replace totqty with inven.totqty-xinadjchange, availqty with inven.totqty-inven.allocqty-inven.holdqty in inven
		endif

		if seek(str(adj.accountid,4)+trans(adj.units)+adj.style+adj.color+adj.id+adj.pack+adj.whseloc,"invenloc","matchwh")
			xholdreturn = (invenloc.hold and invenloc.whseloc#"PSTAT")
			replace locqty with invenloc.locqty-xinadjchange in invenloc
			if invenloc.hold
				replace holdqty with inven.holdqty-xinadjchange, availqty with inven.totqty-inven.allocqty-inven.holdqty in inven
				pnphold(-xinadjchange)
			endif
			if invenloc.locqty=0
				delete in invenloc
			endif
		else
			scatter memvar
			m.locqty=m.totqty
			m.invenid=inven.invenid
			
			if m.invenid=0
set step on
				email("Dyoung@fmiint.com","INFO: invenid=0 when inserting invenloc C",,,,,.t.,,,,,.t.,,.t.)
			endif

			insertinto("invenloc","wh",.t.)
			
			if holdloc(m.whseloc)
				replace hold with .t., holdtype with gholdtype in invenloc
				replace holdqty with inven.holdqty+m.locqty, availqty with inven.totqty-inven.allocqty-inven.holdqty in inven
				pnphold(m.locqty)
			endif
		endif
	endif

	xpalletcount=0

	if !xnoapplyremove
		for qx=1 to 2
			if qx=1
				if empty(xpalletid)
					loop
				else
					xpalletfilter="palletid='"+transform(xpalletid)+"'"
				endif
			else
				xpalletfilter=".t."
			endif

			xinadjchange = indetremove(xinadjchange, adj.accountid, adj.units, adj.style, adj.color, adj.id, adj.pack, adj.whseloc, xppfilter, xpalletfilter, picknpack(adj.accountid))
		next
		
		if xinadjchange>0
			adjremove(xinadjchange, adj.accountid, adj.units, adj.style, adj.color, adj.id, adj.pack, adj.whseloc)
		endif
	endif

else		&& add inventory
	if !xnoinven
		scatter memvar fields accountid, units, style, color, id, pack
		m.date_rcvd=qdate()

		if !seek(str(adj.accountid,4)+trans(adj.units)+adj.style+adj.color+adj.id+adj.pack,"inven","match")
			if xfromphys
				store 0 to m.length, m.width, m.depth, m.ctncube, m.totcube, m.ctnwt, m.totwt
			else
				scatter memvar fields length, width, depth, ctncube, totcube, ctnwt, totwt
			endif
			m.totqty=0
			
			insertinto("inven","wh",.t.)
		endif

		select invenloc
		sum locqty to xholdqty for invenid=inven.invenid and hold

		if inven.units
			scan for !units and style=m.style and color=m.color and id=m.id and hold
				xholdqty=xholdqty+val(invenloc.pack)*invenloc.locqty
			endscan
		endif

		replace totqty with inven.totqty+xinadjchange, holdqty with xholdqty, availqty with inven.totqty-inven.allocqty-inven.holdqty, ;
				totwt with inven.totqty*inven.ctnwt, totcube with inven.totqty*inven.ctncube in inven

		if !seek(str(adj.accountid,4)+trans(adj.units)+adj.style+adj.color+adj.id+adj.pack+adj.whseloc,"invenloc","matchwh")
			m.invenid=inven.invenid
			m.whseloc=adj.whseloc
			m.hold=.f.
			m.holdtype=""
			m.locqty=xinadjchange
			
			if m.invenid=0
				email("Dyoung@fmiint.com","INFO: invenid=0 when inserting invenloc D",,,,,.t.,,,,,.t.,,.t.)
			endif

			insertinto("invenloc","wh",.t.)
			
			if holdloc(m.whseloc)
				replace hold with .t., holdtype with gholdtype in invenloc
				replace holdqty with inven.holdqty+m.locqty, availqty with inven.totqty-inven.allocqty-inven.holdqty in inven
				pnphold(m.locqty)
			endif

		else
			replace locqty with invenloc.locqty+xinadjchange in invenloc

			do case
			case invenloc.hold and !xonhold
				replace holdqty with inven.holdqty+xinadjchange, availqty with inven.totqty-inven.allocqty-inven.holdqty in inven
				pnphold(xinadjchange)
			case !invenloc.hold and xonhold
				replace holdqty with inven.holdqty-xinadjchange, availqty with inven.totqty-inven.allocqty-inven.holdqty in inven
				pnphold(-xinadjchange)
			endcase
		endif
	endif
endif

return xholdreturn
