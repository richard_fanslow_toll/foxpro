* send Qualcomm message for Linehaul

if empty(fxwolog.qualcomm)
	return
endif

if seek(vfxtrips.dis_truck,"trucks","truck_num")
	if empty(trucks.qcserialno)
		return
	endif
	m.qcserialno=trucks.qcserialno
else
	xsqlexec("select qcserialno from otrucks where truck_num='"+vfxtrips.dis_truck+"'","otrucks",,"oo",,,,.t.)
	if reccount("otrucks")=0 and empty(otrucks.qcserialno)
		return
	endif
	m.qcserialno=otrucks.qcserialno
endif

if !directory("k:\")
	email("Dyoung@fmiint.com","No K drive for QCLOAD",transform(daily.dailyid)+crlf+getenv("COMPUTERNAME"),,,,.t.,,,,,.t.,,.t.)
	return
endif

select fxwolog
scatter memvar fields wo_num, wo_date, accountid, acctname

select vfxtrips
scatter memvar fields dis_driver, dis_truck
m.zdate=date()
m.qcuser="MELISSA"
m.macro=37
m.qualcomm=fxwolog.qualcomm
m.fxsendid=sqlgenpk("fxsend","fx")
insert into fxsend from memvar

xqc=trim(m.dis_truck)+crlf + trim(m.qcuser)+crlf + "*Dispatch"+crlf + "false"+crlf + "0"+crlf + transform(m.macro)+crlf+"***THIS IS ONLY A TEST MESSAGE***"+crlf+fxwolog.qualcomm
 
strtofile(xqc,"k:\"+transform(m.fxsendid)+".txt")
