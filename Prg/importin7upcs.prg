* first take the spreadsheet from Maddy and 
*****************************************************************
* In7 UPC Loader
* Open Excell file as received from IN7
* edit file so columns A,B,C,D are style,color,size,upc
* delete any header line, save the file in a convenient place
* 
* Run this program, point to the file just saved, all are uploaded
******************************************************************
Close Data All
Set Exclusive Off
Set Safety off

Create Cursor temp (;
  style char(20),;
  color char(10),;
  size  char(10),;
  upc   char(13),;
  pack_qty char(6),;
  sizenum  char(15),;
  packcode char(4))

Cd h:\fox\in7files\
Select temp
xfile = Getfile("csv")
Append from &xfile delimited

Copy To c:\tempfox\in7upcs
Use f:\3pl\data\in7_stylemaster In 0

Use c:\tempfox\in7upcs alias in7upc In 0
Set Step On 

Select in7upc
Goto top

Scan
  Select in7_stylemaster
  Locate for Alltrim(style)= Alltrim(in7upc.style) and Alltrim(color)= Alltrim(in7upc.color) and Alltrim(size)= Alltrim(in7upc.size) 
  If Found()
    replace in7_stylemaster.upc with Alltrim(Transform(in7upc.upc))
  Else
    Select in7upc
    Scatter memvar
    Select in7_stylemaster
    Append Blank
    Gather memvar
    replace adddt With Date()
  EndIf 
EndScan
Browse
