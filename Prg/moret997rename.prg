DO m:\dev\prg\_setvars WITH .T.
CD F:\ftpusers\moret\Hold997
len1 = ADIR(arymoret,"*.edi")
IF len1 = 0
	WAIT WINDOW "No 997s to process..." TIMEOUT 1
	CLOSE DATABASES ALL
	RETURN
ENDIF
lTesting = .F.

_SCREEN.CAPTION = "Moret 997 Rename Process"
_SCREEN.WINDOWSTATE = IIF(lTesting,2,1)
cFileExt = ".edi"
cString = ""
cFilenameIn = ALLTRIM(arymoret[1])
COPY FILE [&cFilenameIn] TO WFile.997
DELETE FILE [&cFilenameIn]
CREATE CURSOR temp1 (f1 c(254))
SELECT temp1
APPEND FROM WFile.997 TYPE SDF
DELETE FILE WFile.997
LOCATE
ASSERT .F.
SCAN
	cF1 = ALLTRIM(f1)
	IF cF1 = "ISA"
		cTimeString = TTOC(DATETIME(),1)
		cOutfilename = "997MORET"+cTimeString+cFileExt
		IF lTesting
			cOutfile = LOWER("F:\FTPUSERS\MORET\997TestOut\"+cOutfilename)
		ELSE
			cOutfile = LOWER("F:\FTPUSERS\MORETGROUP\SUMMIT\OUT\"+cOutfilename)
			cArchiveFile = LOWER("F:\FTPUSERS\MORET\997archive\"+cOutfilename)
		ENDIF
		nFnum = FCREATE(cOutfile)
		nFnum2 = FCREATE(cArchiveFile)
	ENDIF

	FPUTS(nFnum,cF1)
	IF cF1 = "IEA"
		FCLOSE(nFnum)
		IF !lTesting
			FCLOSE(nFnum2)
		ENDIF
	ENDIF
ENDSCAN

RETURN
