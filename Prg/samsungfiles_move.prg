public cfilename,cfileoutname,csamsungtransfer,normalexit
close data all
do m:\dev\prg\_setvars with .t.
normalexit = .f.
ccustname = "SAMSUNG"

try
	ltesting = .f.
	loverridebusy = ltesting
	loverridebusy = .t.

	csamsungtransfer = "SAMSUNG-MOVE"
	if !ltesting
		select 0
		use f:\edirouting\ftpsetup shared
		locate for ftpsetup.transfer = csamsungtransfer
		if ftpsetup.chkbusy = .t. and !loverridebusy
			wait window "File is processing...exiting" timeout 2
			normalexit = .t.
			throw
		endif
		replace chkbusy with .t. for ftpsetup.transfer = csamsungtransfer
		use in ftpsetup
	endif

	cdirin = "f:\ftpusers\Samsung\in\"
	cd &cdirin

	_screen.caption="Samsung File Move Process"
	_screen.windowstate=iif(ltesting,2,1)

	nfound = adir(ary1,'*.*')
	if nfound = 0
		close databases all
		wait window "No EDI files found to process...exiting" timeout 2
		normalexit = .t.
		throw
	endif

	set step on
	len1 = alen(ary1,1)

	for thisfile = 1 to len1
		assert .f. message "In move loop"
		csafilename = allt(ary1[thisfile,1])
		xfile = (cdirin+csafilename)

		wait window "Processing "+csafilename timeout 1
		do m:\dev\prg\createx856a
		do m:\dev\prg\loadedifile with xfile,"*","TILDE","SAMSUNG"
		select x856
		locate for x856.segment = "GS"
		cfiletype = icase(x856.f1 = 'SC','832',x856.f1 = 'AR','943',x856.f1 = 'FA','997','940')  && Types 832,943,997/940
		cdirxfer = ("F:\ftpusers\Samsung\997out_translate\")
		cfilexfername = (cdirxfer+csafilename)

		do case
		case cfiletype = '997'
			cdirout = ("F:\ftpusers\Samsung\997in\")
		case cfiletype = '832'
			cdirout = ("F:\ftpusers\Samsung\stylemaster\")
		case cfiletype = '940'
			cdirout = ("F:\ftpusers\Samsung\940IN\")
		case cfiletype = '943'
			cdirout = ("F:\ftpusers\Samsung\943IN\")
		endcase
		set step on
		cfileoutname = (cdirout+csafilename)
		copy file [&xfile] to [&cFileOutName]
		if cfiletype # '997'
			copy file [&xfile] to [&cFileXferName]  && Added 08.08.2011 to send 997s for these
		endif
		if file(cfileoutname)
			delete file [&xfile]
		endif
	endfor

	wait window "Samsung file move process complete" timeout 1
	select 0
	use f:\edirouting\ftpsetup shared
	replace chkbusy with .f. for ftpsetup.transfer = csamsungtransfer

	wait clear
	normalexit = .t.

catch to oerr
	if !normalexit
		assert .f. message "At CATCH..."
		set step on

		tsubject = ccustname+" 940 "+cofficeloc+" Upload Error ("+transform(oerr.errorno)+") at "+ttoc(datetime())
		tattach  = ""
		tsendto  = tsendtoerr
		tcc = tccerr
		tmessage = ccustname+" 940 "+cofficeloc+" Upload Error..... Please fix me........!"+chr(13)+cerrmsg
		lcsourcemachine = sys(0)
		lcsourceprogram = sys(16)

		tmessage =tmessage+ "Try/CATCH Exeception Message:"+chr(13)+;
			[  Message: ] + oerr.message +chr(13)+;
			[  Procedure: ] + oerr.procedure +chr(13)+;
			[  LineNo: ] + str(oerr.lineno) +chr(13)+;
			[  Error: ] + str(oerr.errorno) +chr(13)+;
			[  Details: ] + oerr.details +chr(13)+;
			[  StackLevel: ] + str(oerr.stacklevel) +chr(13)+;
			[  LineContents: ] + oerr.linecontents+chr(13)+;
			[  UserValue: ] + oerr.uservalue+chr(13)+;
			[  Computer:  ] +lcsourcemachine+chr(13)+;
			[  940 file:  ] +xfile+chr(13)+;
			[  Program:   ] +lcsourceprogram

		tattach  = ""
		tfrom ="TGF EDI Operations <toll-edi-ops@tollgroup.com>"
		do form m:\dev\frm\dartmail2 with tsendto,tfrom,tsubject,tcc,tattach,tmessage,"A"
	else
		wait window at 10,10 "Normal Exit " timeout 2
	endif
finally
	close databases all
endtry
