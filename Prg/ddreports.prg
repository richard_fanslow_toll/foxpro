* Data Driven reports for HR, Kronos, etc.
* Defined in table: F:\UTIL\DDREPORTS\DATA\DDREPMAST.DBF.  Must be passed a job # parameter such as '0001', '0037' etc.

* Build EXE as F:\UTIL\DDREPORTS\DDREPORTS.exe

LPARAMETERS tcReportCode
LOCAL loDataDrivenReport
loDataDrivenReport = CREATEOBJECT('DataDrivenReport')
loDataDrivenReport.MAIN( tcReportCode )
CLOSE DATABASES ALL
RETURN

#DEFINE LOGIT 1
#DEFINE WAITIT 2
#DEFINE NOWAITIT 4
#DEFINE SENDIT 8
#DEFINE CRLF CHR(13) + CHR(10)
#DEFINE RETURN_DATA_MANDATORY .F.
#DEFINE RETURN_DATA_NOT_MANDATORY .T.
#DEFINE CSVFORMAT 6
#DEFINE WRITE_ONLY_UNBUFFERED 11
#DEFINE COMMA ","

DEFINE CLASS DataDrivenReport AS CUSTOM

	cProcessName = 'DataDrivenReport'

	lTestMode = .F.  && .F. FOR NORMAL OPERATION, .T. TO SUPPRESS ALL BUT DEVELOPER EMAILS AND CHANGE INPUT TABLE, ETC.

	dToday = DATE()

	cMainTable = 'F:\UTIL\DDREPORTS\DATA\DDREPMAST.DBF'
	cChildTable = 'F:\UTIL\DDREPORTS\DATA\DDREPCHILD.DBF'
	cJobName = 'Not Found'
	cReportCode = '????'
	cSubjectDateExpression = ''

	lAutoYield = .T.

	cStartTime = TTOC(DATETIME())

	* report properties
	lShowSalaries = .T.

	* connection properties
	nSQLHandle = 0

	* wait window properties
	nWaitWindowTimeout = 2
	lWaitWindowIsOn = .T.

	* logfile properties
	lLoggingIsOn = .T.
	cLogFile = 'F:\UTIL\DDREPORTS\LOGFILES\DDREPORTS_log.txt'

	* INTERNAL email properties
	lSendInternalEmailIsOn = .T.
	cFrom = 'TGF Corporate <fmicorporate@fmiint.com>'
	cSendTo = ''
	cCC = ''
	cSubject = 'Report Not Found'
	cAttach = ''
	cTopBodyText = ''
	cBodyText = ''

	cCOMPUTERNAME = ''
	cUSERNAME = ''


	FUNCTION FindJob
		LPARAMETERS tcReportCode
		LOCAL llRetVal
		* open the main table and find the passed Report Code; populate class properties
		USE (.cMainTable) IN 0 ALIAS MAINTABLE
		SELECT MAINTABLE
		LOCATE FOR CJOBCODE == tcReportCode
		IF FOUND() THEN
			llRetVal = .T.
			* leave table open on the correct record
			.cJobName = ALLTRIM(MAINTABLE.cJobName)
			.cSubjectDateExpression = ALLTRIM(MAINTABLE.cSubDtExpr)
			IF .lTestMode THEN
				.cSendTo = ALLTRIM(MAINTABLE.cTstSendTo)
			ELSE
				.cSendTo = ALLTRIM(MAINTABLE.cSendTo)
			ENDIF
			.TrackProgress('Found Report: [' + .cJobName + '] for Report Code: [' + tcReportCode + ']', LOGIT+SENDIT)
			.cReportCode = tcReportCode
		ELSE
			llRetVal = .F.
			.TrackProgress('Report Code: [' + tcReportCode + '] could not be found!', LOGIT+SENDIT)
		ENDIF

		RETURN llRetVal
	ENDFUNC


	FUNCTION INIT
		IF NOT DODEFAULT()
			RETURN .F.
		ENDIF
		WITH THIS
			*SET RESOURCE OFF
			CLOSE DATA
			SET CENTURY ON
			SET DATE AMERICAN
			SET HOURS TO 24
			SET ANSI ON
			SET TALK OFF
			SET DELETED ON
			SET CONSOLE OFF
			SET EXCLUSIVE OFF
			SET SAFETY OFF
			SET EXACT OFF
			SET STATUS BAR ON
			SET SYSMENU OFF
			SET ENGINEBEHAVIOR 70
			_VFP.AUTOYIELD = .lAutoYield
			.cCOMPUTERNAME = GETENV("COMPUTERNAME")
			.cUSERNAME = GETENV("USERNAME")
			IF .lTestMode THEN
				.cLogFile = 'F:\UTIL\DDREPORTS\LOGFILES\DDREPORTS_log_TESTMODE.txt'
				.cSendTo = 'mbennett@fmiint.com'
			ENDIF
			.lLoggingIsOn = .lLoggingIsOn AND NOT EMPTY(.cLogFile)
			IF .lLoggingIsOn THEN
				SET ALTERNATE TO (.cLogFile) ADDITIVE
				SET ALTERNATE ON
			ENDIF
		ENDWITH
	ENDFUNC


	FUNCTION DESTROY
		WITH THIS
			IF .lLoggingIsOn  THEN
				SET ALTERNATE OFF
				SET ALTERNATE TO
			ENDIF
		ENDWITH
		DODEFAULT()
	ENDFUNC


	FUNCTION MAIN
		LPARAMETERS tcReportCode
		WITH THIS
			LOCAL lnNumberOfErrors, lcSQL, loError, lcFiletoSaveAs, lcSpreadsheetTemplate
			LOCAL oExcel, oWorkbook, oWorksheet, lnRow, lcRow
			LOCAL lcSubjectDateExpression
			* NOTE: any expressions used in stored cSQL must be PRIVATE, so that they are
			* accessible in the 3 main called routines.
			PRIVATE pdToday
			PRIVATE pdLastDayPriorMonth, pdFirstDayPriorMonth, pdLastDayPriorPriorMonth
			PRIVATE pcLastDayPriorMonth, pcFirstDayPriorMonth, pcLastDayPriorPriorMonth

			TRY
				lnNumberOfErrors = 0

				.TrackProgress('', LOGIT+SENDIT)
				.TrackProgress('=================================================================', LOGIT+SENDIT)
				.TrackProgress("Data Driven Report process started....", LOGIT+SENDIT+NOWAITIT)
				.TrackProgress('COMPUTERNAME = ' + .cCOMPUTERNAME, LOGIT+SENDIT)
				.TrackProgress('USERNAME = ' + .cUSERNAME, LOGIT+SENDIT)
				IF .lTestMode THEN
					.TrackProgress('                        TEST MODE                        ', LOGIT+SENDIT)
				ENDIF
				.TrackProgress('PROJECT = DDREPORTS', LOGIT+SENDIT)


				IF .FindJob( tcReportCode ) THEN

					**********************************************************
					*****  populate PRIVATES here...
					**********************************************************

					pdToday = .dToday
					*pdToday = {^2012-01-01}   &&& to force running on desired back dates

					* this calc gives last day of prior month
					pdLastDayPriorMonth = pdToday - DAY(pdToday)

					pcLastDayPriorMonth = "DATE'" + PADL(YEAR(pdLastDayPriorMonth),4,"0") + "-" + PADL(MONTH(pdLastDayPriorMonth),2,"0") + "-" + ;
						PADL(DAY(pdLastDayPriorMonth),2,"0") + "'"

					* this calc gives first day of prior month
					pdFirstDayPriorMonth = (pdLastDayPriorMonth + 1) - DAY(pdLastDayPriorMonth)

					pcFirstDayPriorMonth = "DATE'" + PADL(YEAR(pdFirstDayPriorMonth),4,"0") + "-" + PADL(MONTH(pdFirstDayPriorMonth),2,"0") + "-" + ;
						PADL(DAY(pdFirstDayPriorMonth),2,"0") + "'"

					pdLastDayPriorPriorMonth = pdFirstDayPriorMonth - 1

					pcLastDayPriorPriorMonth = "DATE'" + PADL(YEAR(pdLastDayPriorPriorMonth),4,"0") + "-" + PADL(MONTH(pdLastDayPriorPriorMonth),2,"0") + "-" + ;
						PADL(DAY(pdLastDayPriorPriorMonth),2,"0") + "'"

					IF .lTestMode THEN
						.TrackProgress('pdToday = ' + TRANSFORM(pdToday), LOGIT+SENDIT)
						.TrackProgress('pdLastDayPriorMonth = ' + TRANSFORM(pdLastDayPriorMonth), LOGIT+SENDIT)
						.TrackProgress('pcLastDayPriorMonth = ' + pcLastDayPriorMonth, LOGIT+SENDIT)
						.TrackProgress('pdFirstDayPriorMonth = ' + TRANSFORM(pdFirstDayPriorMonth), LOGIT+SENDIT)
						.TrackProgress('pcFirstDayPriorMonth = ' + pcFirstDayPriorMonth, LOGIT+SENDIT)
						.TrackProgress('pdLastDayPriorPriorMonth = ' + TRANSFORM(pdLastDayPriorPriorMonth), LOGIT+SENDIT)
						.TrackProgress('pcLastDayPriorPriorMonth = ' + pcLastDayPriorPriorMonth, LOGIT+SENDIT)
					ENDIF

					* create the subject line
					lcSubjectDateExpression = .cSubjectDateExpression
					.cSubject = .cJobName + " for " + &lcSubjectDateExpression.

					* retrieve all steps for current report code
					IF USED('CURSTEPS') THEN
						USE IN CURSTEPS
					ENDIF

					SELECT * ;
						FROM (.cChildTable) ;
						INTO CURSOR CURSTEPS ;
						WHERE CJOBCODE == .cReportCode ;
						AND lactive ;
						ORDER BY NSTEP
						
					IF USED('CURSTEPS') AND NOT EOF('CURSTEPS') THEN
						SELECT CURSTEPS
						SCAN
							DO CASE
								CASE UPPER(ALLTRIM(CURSTEPS.cDATABASE)) == "VFP"
									.RunVFP()
								CASE UPPER(ALLTRIM(CURSTEPS.cDATABASE)) == "ADP"
									.RunADP()
								CASE UPPER(ALLTRIM(CURSTEPS.cDATABASE)) == "KRONOS"
									.RunKRONOS()
								OTHERWISE
									* NOTHING
							ENDCASE
						ENDSCAN
					ENDIF

					CLOSE DATABASES ALL

					*!*					* construct sql filters
					*!*					lcStartDate = "DATE'" + TRANSFORM(YEAR(ldStartDate)) + ;
					*!*						"-" + PADL(MONTH(ldStartDate),2,"0") + "-" + PADL(DAY(ldStartDate),2,"0") + "'"

					*!*					lcEndDate = "DATE'" + TRANSFORM(YEAR(ldEndDate)) + ;
					*!*						"-" + PADL(MONTH(ldEndDate),2,"0") + "-" + PADL(DAY(ldEndDate),2,"0") + "'"

					*!*	*!*					* get ss#s from ADP
					*!*	*!*					OPEN DATABASE F:\UTIL\ADPCONNECTION\CONNECT1

					*!*					.nSQLHandle = SQLCONNECT("PCPAYWIN","MBENNETT","VP737A")

					*!*					IF .nSQLHandle > 0 THEN

					*!*						lcSQL = ;
					*!*							"SELECT " + ;
					*!*							" NAME, " + ;
					*!*							" COMPANYCODE AS ADP_COMP, " + ;
					*!*							" {fn LEFT(CHECKVIEWHOMEDEPT,2)} AS DIVISION, " + ;
					*!*							" FILE# AS FILE_NUM, " + ;
					*!*							" CHECKVIEWPAYDATE AS PAYDATE, " + ;
					*!*							" CHECKVIEWEARNSCD AS EARNSCODE, " + ;
					*!*							" CHECKVIEWEARNSAMT AS COMMISSION " + ;
					*!*							" FROM REPORTS.V_CHK_VW_EARNINGS " + ;
					*!*							" WHERE CHECKVIEWEARNSCD = '10' " + ;
					*!*							" AND CHECKVIEWPAYDATE >= " + lcStartDate + ;
					*!*							" AND CHECKVIEWPAYDATE <= " + lcEndDate + ;
					*!*							" ORDER BY NAME, CHECKVIEWPAYDATE "

					*!*							*" AND {fn LEFT(CHECKVIEWHOMEDEPT,2)} IN ('71','72','73','74','76','78') " + ;

					*!*						IF .lTestMode THEN
					*!*							.TrackProgress('                        TEST MODE                        ', LOGIT+SENDIT)
					*!*							.TrackProgress('lcSQL =' + lcSQL, LOGIT+SENDIT)
					*!*						ENDIF

					*!*						IF USED('SQLCURSOR1') THEN
					*!*							USE IN SQLCURSOR1
					*!*						ENDIF

					*!*						IF .ExecSQL(lcSQL, 'SQLCURSOR1', RETURN_DATA_MANDATORY) THEN

					*!*							*SELECT SQLCURSOR1
					*!*							*BROW

					*!*							lcSpreadsheetTemplate = "F:\UTIL\ADPREPORTS\Commissions_TEMPLATE.XLS"
					*!*							lcFiletoSaveAs = "F:\UTIL\ADPREPORTS\REPORTS\Data Driven Report for " + STRTRAN(DTOC(ldToday),"/","-") + ".XLS"

					*!*							IF FILE(lcFiletoSaveAs) THEN
					*!*								DELETE FILE (lcFiletoSaveAs)
					*!*							ENDIF

					*!*							***********************************************************************************
					*!*							** output to Excel spreadsheet
					*!*							.TrackProgress('Creating spreadsheet: ' + lcFiletoSaveAs, LOGIT+SENDIT+NOWAITIT)

					*!*							oExcel = CREATEOBJECT("excel.application")
					*!*							oExcel.VISIBLE = .F.
					*!*							oWorkbook = oExcel.workbooks.OPEN(lcSpreadsheetTemplate)
					*!*							oWorkbook.SAVEAS(lcFiletoSaveAs)

					*!*							oWorksheet = oWorkbook.Worksheets[1]
					*!*							oWorksheet.RANGE("A3","H500").ClearContents()
					*!*							oWorksheet.RANGE("A1").VALUE = "Data Driven Report for " + TRANSFORM(ldStartDate) + " - " + TRANSFORM(ldEndDate)

					*!*							lnRow = 2
					*!*							SELECT SQLCURSOR1
					*!*							SCAN

					*!*								lnRow = lnRow + 1
					*!*								lcRow = ALLTRIM(STR(lnRow))

					*!*								oWorksheet.RANGE("A"+lcRow).VALUE = SQLCURSOR1.NAME
					*!*								oWorksheet.RANGE("B"+lcRow).VALUE = SQLCURSOR1.ADP_COMP
					*!*								oWorksheet.RANGE("C"+lcRow).VALUE = "'" + SQLCURSOR1.DIVISION
					*!*								oWorksheet.RANGE("D"+lcRow).VALUE = "'" + TRANSFORM(SQLCURSOR1.FILE_NUM)
					*!*								oWorksheet.RANGE("E"+lcRow).VALUE = SQLCURSOR1.PAYDATE
					*!*								oWorksheet.RANGE("F"+lcRow).VALUE = "'" + SQLCURSOR1.EARNSCODE
					*!*								oWorksheet.RANGE("G"+lcRow).VALUE = SQLCURSOR1.COMMISSION

					*!*							ENDSCAN

					*!*							* SAVE AND QUIT EXCEL
					*!*							oWorkbook.SAVE()
					*!*							oExcel.QUIT()

					*!*							IF FILE(lcFiletoSaveAs) THEN
					*!*								* attach output file to email
					*!*								.cAttach = lcFiletoSaveAs
					*!*								.cBodyText = "See attached Data Driven Report." + ;
					*!*									CRLF + CRLF + "(do not reply - this is an automated report)" + ;
					*!*									CRLF + CRLF + "<report log follows>" + ;
					*!*									CRLF + .cBodyText
					*!*							ELSE
					*!*								.TrackProgress('ERROR: unable to email spreadsheet: ' + lcFiletoSaveAs, LOGIT+SENDIT+NOWAITIT)
					*!*							ENDIF

					*!*							CLOSE DATABASES ALL
					*!*						ENDIF  &&  .ExecSQL(lcSQL, 'SQLCURSOR1', RETURN_DATA_MANDATORY)

					*!*						.TrackProgress("Data Driven Report process ended normally.", LOGIT+SENDIT+NOWAITIT)
					*!*					ELSE
					*!*						* connection error
					*!*						.TrackProgress('Unable to connect to ADP Payroll System.', LOGIT+SENDIT)
					*!*					ENDIF   &&  .nSQLHandle > 0

				ENDIF  &&  .FindJob( tcReportCode )
				.TrackProgress("Data Driven Report process ended normally.", LOGIT+SENDIT+NOWAITIT)

			CATCH TO loError

				.TrackProgress('There was an error.',LOGIT+SENDIT)
				.TrackProgress('The Error # is: ' + TRANSFORM(loError.ERRORNO), LOGIT+SENDIT)
				.TrackProgress('The Error Message is: ' + TRANSFORM(loError.MESSAGE), LOGIT+SENDIT)
				.TrackProgress('The Error occurred in Program: ' + TRANSFORM(loError.PROCEDURE), LOGIT+SENDIT)
				.TrackProgress('The Error occurred at line #: ' + TRANSFORM(loError.LINENO), LOGIT+SENDIT)
				lnNumberOfErrors = lnNumberOfErrors + 1
				IF TYPE('oWorkbook') = "O" THEN
					oWorkbook.SAVE()
				ENDIF
				IF TYPE('oExcel') = "O" THEN
					oExcel.QUIT()
				ENDIF
				CLOSE DATABASES ALL

			ENDTRY

			CLOSE DATA
			WAIT CLEAR
			***************** INTERNAL email results ******************************
			.TrackProgress('About to send status email.',LOGIT)
			.TrackProgress('The logfile is: ' + .cLogFile, SENDIT)
			.TrackProgress("Data Driven Report process started: " + .cStartTime, LOGIT+SENDIT)
			.TrackProgress("Data Driven Report process finished: " + TTOC(DATETIME()), LOGIT+SENDIT)
			
			.cTopBodyText = STRTRAN(.cTopBodyText,'~CRLF~',CRLF + CRLF)

			IF .lSendInternalEmailIsOn THEN
				DO FORM dartmail2 WITH .cSendTo,.cFrom,.cSubject,.cCC,.cAttach,(.cTopBodyText + .cBodyText),"A"
				.TrackProgress('Sent status email.',LOGIT)
			ELSE
				.TrackProgress('Did not send status email: THIS.lSendInternalEmailIsOn = FALSE.',LOGIT)
			ENDIF

		ENDWITH
		RETURN
	ENDFUNC && main


	PROCEDURE RunVFP
		WITH THIS
			LOCAL lcSQL, lcOutputCursor, lcBodyText
			lcOutputCursor = ALLTRIM(CURSTEPS.cOUTCURSOR)
			lcSQL = STRTRAN(ALLTRIM(CURSTEPS.cSQL),'<<OUTPUTCURSOR>>',lcOutputCursor)
			IF .lTestMode THEN
				.TrackProgress('lcSQL = ' + lcSQL, LOGIT+SENDIT)
			ENDIF
			* VFP queries we can run natively, without opening a SQL connection..
			&lcSQL
			*SELECT (lcOutputCursor)
			*BROWSE
			lcBodyText = ALLTRIM(CURSTEPS.cBodyText)
			IF NOT EMPTY(lcBodyText) THEN
				*.cTopBodyText = .cTopBodyText + &lcBodyText. + CRLF + CRLF
				.cTopBodyText = .cTopBodyText + &lcBodyText. + '~CRLF~'
			ENDIF
		ENDWITH
		RETURN
	ENDPROC


	PROCEDURE RunADP
		WITH THIS
			LOCAL lcADPSQL, lcADPSQL2, lcOutputCursor, lcBodyText, lcRootFile, lcFiletoSaveAs, lcDateTime
			lcOutputCursor = ALLTRIM(CURSTEPS.cOUTCURSOR)
			lcRootFile = ALLTRIM(CURSTEPS.cROOTFILE)
			lcADPSQL = ALLTRIM(CURSTEPS.cSQL)  &&			*,'<<OUTPUTCURSOR>>',lcOutputCursor)
			IF .lTestMode THEN
				.TrackProgress('lcADPSQL = ' + lcADPSQL, LOGIT+SENDIT)
			ENDIF

			lcADPSQL2 = STRTRAN(lcADPSQL,'<<cLastDayPriorMonth>>',pcLastDayPriorMonth)
			lcADPSQL2 = STRTRAN(lcADPSQL2,'<<cFirstDayPriorMonth>>',pcFirstDayPriorMonth)
			*lcADPSQL2 = EVAL(lcADPSQL2)

			IF .lTestMode THEN
				.TrackProgress('lcADPSQL2 = ' + lcADPSQL2, LOGIT+SENDIT)
			ENDIF

			.nSQLHandle = SQLCONNECT("PCPAYWIN","MBENNETT","VP737A")

			IF .nSQLHandle > 0 THEN

				IF USED(lcOutputCursor) THEN
					USE IN (lcOutputCursor)
				ENDIF

				IF .ExecSQL(lcADPSQL2, lcOutputCursor, RETURN_DATA_MANDATORY) THEN

					*!*	SELECT (lcOutputCursor)
					*!*	BROWSE
					IF NOT EMPTY(lcRootFile) THEN

						lcDateTime = TRANSFORM(DATETIME())
						lcDateTime = STRTRAN(lcDateTime,"/","")
						lcDateTime = STRTRAN(lcDateTime,":","")
						lcDateTime = STRTRAN(lcDateTime," ","_")

						lcFiletoSaveAs = lcRootFile + "_" + lcDateTime + ".XLS"

						SELECT (lcOutputCursor)
						COPY TO (lcFiletoSaveAs) XL5

						IF FILE(lcFiletoSaveAs) THEN
							* attach output file to email
							.cAttach = lcFiletoSaveAs
							*!*							.cBodyText = "See attached Data Driven Report." + ;
							*!*								CRLF + CRLF + "(do not reply - this is an automated report)" + ;
							*!*								CRLF + CRLF + "<report log follows>" + ;
							*!*								CRLF + .cBodyText
						ELSE
							.TrackProgress('ERROR: unable to email spreadsheet: ' + lcFiletoSaveAs, LOGIT+SENDIT+NOWAITIT)
						ENDIF

					ENDIF && NOT EMPTYlcRootFile) THEN

					*!*	07/22/2010 13:08:15
					SELECT (lcOutputCursor)
					GOTO TOP
					lcBodyText = ALLTRIM(CURSTEPS.cBodyText)
					IF NOT EMPTY(lcBodyText) THEN
						*.cTopBodyText = .cTopBodyText + CRLF + CRLF + &lcBodyText. + CRLF + CRLF
						.cTopBodyText = .cTopBodyText + &lcBodyText. 
						.cTopBodyText = .cTopBodyText + '~CRLF~'
					ENDIF

				ENDIF  &&  .ExecSQL(lcSQL, lcOutputCursor, RETURN_DATA_MANDATORY)
			ELSE
				* connection error
				.TrackProgress('Unable to connect to ADP Payroll System.', LOGIT+SENDIT)
			ENDIF   &&  .nSQLHandle > 0
		ENDWITH
		RETURN
	ENDPROC  &&  RunADP


	PROCEDURE RunKRONOS
		WITH THIS

		ENDWITH
		RETURN
	ENDPROC


	FUNCTION ExecSQL
		LPARAMETERS tcSQL, tcCursorName, tlNoDataReturnedIsOkay
		LOCAL llRetVal, lnResult
		WITH THIS
			* close target cursor if it's open
			IF USED(tcCursorName)
				USE IN (tcCursorName)
			ENDIF
			WAIT WINDOW "Fetching remote data; please wait..." NOWAIT
			lnResult = SQLEXEC(.nSQLHandle, tcSQL, tcCursorName)
			llRetVal = ( lnResult > 0 )
			IF llRetVal THEN
				* see if any data came back
				IF NOT tlNoDataReturnedIsOkay THEN
					IF (NOT USED(tcCursorName)) OR EOF(tcCursorName) THEN
						llRetVal = .F.
						.TrackProgress('No data was returned by this query: ' +CRLF + tcSQL, LOGIT+SENDIT)
						.cSendTo = 'mbennett@fmiint.com'
					ENDIF
				ENDIF
			ELSE
				.TrackProgress("Result Code [" + TRANSFORM(lnResult) + "] returned by this query: " + tcSQL, LOGIT+SENDIT)
				.cSendTo = 'mbennett@fmiint.com'
			ENDIF
			WAIT CLEAR
			RETURN llRetVal
		ENDWITH
	ENDFUNC


	PROCEDURE TrackProgress
		* do any combination of Wait Window, writing to logfile, and adding to body of email,
		* based on nFlags parameter.
		LPARAMETERS tcExpression, tnFlags
		WITH THIS
			IF BITAND(tnFlags,LOGIT) = LOGIT THEN
				IF .lLoggingIsOn THEN
					?
					? .cProcessName + "  " + TTOC(DATETIME()) + ": " + tcExpression
				ENDIF
			ENDIF
			IF .lWaitWindowIsOn THEN
				IF BITAND(tnFlags,NOWAITIT) = NOWAITIT THEN
					WAIT WINDOW tcExpression NOWAIT
				ENDIF
				IF BITAND(tnFlags,WAITIT) = WAITIT THEN
					WAIT WINDOW tcExpression TIMEOUT .nWaitWindowTimeout
				ENDIF
			ENDIF
			IF BITAND(tnFlags,SENDIT) = SENDIT THEN
				IF .lSendInternalEmailIsOn THEN
					.cBodyText = .cBodyText + tcExpression + CRLF + CRLF
				ENDIF
			ENDIF
		ENDWITH
	ENDPROC  &&  TrackProgress

ENDDEFINE
