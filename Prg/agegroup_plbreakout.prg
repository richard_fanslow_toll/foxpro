PARAMETERS cOffice
SET EXCLUSIVE OFF
SET SAFETY OFF
SET ENGINEBEHAVIOR 70
cErrorMessage = ""
CLOSE DATABASES ALL
SET ASSERTS ON

TRY
	IF cOffice = "C"
		cState = "ca"
	ELSE
		cState = "nj"
	ENDIF

	lTesting = .F.
	IF !lTesting
		cPLDir   	= ("f:\0-packinglist\agegroup-"+cState+"\")
		cArchDir 	= ("f:\0-packinglist\archive\agegroup-"+cState+"\")
		cOrigPLDir  = ("f:\0-packinglist\agegroup-"+cState+"\originals\")
		cOrigArchDir= ("f:\0-packinglist\archive\agegroup-"+cState+"\originals\")
	ELSE
		cPLDir   	= ("m:\joeb\agegroup\")
		cOrigPLDir  = ("m:\joeb\agegroup\originals\")
		cArchDir 	= ("m:\joeb\archive\agegroup\")
		cOrigDir 	= ("m:\joeb\archive\agegroup\originals\")
	ENDIF

	SET DEFAULT TO &cPLDir
	CD &cPLDir
	DELETE FILE "*.dbf"
	nFileList = ADIR(ary1,"*.xls")
	IF nFileList = 0
		WAIT WINDOW "No AgeGroup Files to process...exiting" TIMEOUT 2
		lDoInbound = .F.
		CLOSE DATABASES ALL
		lNormalExit = .T.
		THROW
	ENDIF

	len1 = ALEN(ary1,1)
	ASSERT .F. MESSAGE "AT INITIAL CURSOR CREATION"
	FOR jj = 1 TO len1
		cFilename1 = ALLTRIM(ary1[jj,1])
		cFilenameag = ALLTRIM(ary1[jj,1])
		WAIT WINDOW "File: "+cFilenameag TIMEOUT 2
		CREATE CURSOR TEMPINTAKE (FA C(30),FB C(30),FC C(50),FD C(30),FE C(30),FF C(30),FG C(30),;
			FH C(30),FI C(30),FJ C(30),FK C(30),FL C(30))

		WAIT WINDOW "Now processing file "+cFilename1 NOWAIT
		cFilenamein = cPLDir+cFilename1
		cFilenameout = cPLDir+cFilenameag
		oExcel = CREATEOBJECT("Excel.Application")
		oExcel.DisplayAlerts = .F.
		oWorkbook = oExcel.Workbooks.OPEN(cFilenamein)
		oWorkbook.SAVEAS(cFilenameout,43)
		oExcel.QUIT()
		RELEASE oExcel
		
		select tempintake
		APPEND FROM [&cFilenameout] TYPE XL8
		LOCATE

		IF "GENERIC INBOUND"$UPPER(TEMPINTAKE.FA)
			USE IN TEMPINTAKE
			COPY FILE [&cFilenameout] TO (cOrigPLDir+cFilenameag)
			wmsinboundexe="f:\3pl\proj\wmsinbound.exe"
			DO (wmsinboundexe) WITH cOffice,nAcctNum
			IF FILE(cFilenameout)
				COPY FILE [&cFilenameout] TO (cArchDir+cFilenameag)
				DELETE FILE [&cFilenameout]
			ENDIF
			LOOP
		ELSE
			COPY FILE [&cFilenameout] TO (cOrigPLDir+cFilenameag)
			IF FILE(cFilenameout)
				DELETE FILE [&cFilenameout]
			ENDIF
		ENDIF
	ENDFOR

	CD &cOrigPLDir
	IF ADIR(aryod,"*.xls") = 0
		WAIT WINDOW "No pending AgeGroup PL files in folder...exiting" TIMEOUT 2
		lDoInbound = .F.
		lNormalExit = .T.
		THROW
	ENDIF
	RELEASE aryod
	len1 = ALEN(ary1,1)
	ASSERT .F. MESSAGE "At WMSInbound setup"
	FOR jj = 1 TO len1
		cFilenameag = ary1[jj,1]
		SELECT 0
		CREATE CURSOR TEMPINTAKE (FA C(30),FB C(30),FC C(50),FD C(30),FE C(30),FF C(30),FG C(30),;
			FH C(30),FI C(30),FJ C(30),FK C(30),FL C(30))
		APPEND FROM [&cFilenameag] TYPE XL8
		LOCATE
		LOCATE FOR ALLTRIM(FE)="SKU#"
		IF FOUND()
			lOldFile = .T.
			WAIT WINDOW "THIS IS AN OLD-FORMAT PL FILE" TIMEOUT 1
		ELSE
			lOldFile = .F.
			WAIT WINDOW "THIS IS A NEW-FORMAT PL FILE" TIMEOUT 1
		ENDIF
		LOCATE
		COUNT TO nTUCount FOR UPPER(TRIM(TEMPINTAKE.FA)) = UPPER("Total Units")
		LOCATE FOR UPPER(TRIM(TEMPINTAKE.FA)) = UPPER("Total Units")

		FOR qq = 1 TO (nTUCount - 1)
			CONTINUE
		ENDFOR
		IF !EOF()
			SKIP
		ENDIF
		DELETE FOR EMPTY(TRIM(FA)) REST

		LOCATE
		cContainer = ""
		ASSERT .F. MESSAGE "At sheet scan"
		SCAN
			IF RECNO() = 1
				cRef = ALLTRIM(TEMPINTAKE.FA)
			ENDIF

			IF UPPER(TRIM(FA)) = UPPER("Container#")
				cContainer = TRIM(FB)
			ENDIF

			IF UPPER(TRIM(FA)) = UPPER("Seal Number")
				cSeal = TRIM(FB)
			ENDIF

			IF UPPER(TRIM(FC)) = UPPER("Vessel")
				SKIP 1
				cVessel = TRIM(FC)
				DO excelfill
			ENDIF

			IF UPPER(TRIM(FE)) = UPPER("SKU#")
				nStart = 1
				SKIP 2
				DO WHILE !EMPTY(TRIM(FE))
					cStyle = TRIM(FE)
					nUnits = INT(VAL(FG))
					nCtns = INT(VAL(FI))
					IF nUnits = 0 OR nCtns = 0 ;
							OR VARTYPE(nUnits)#"N" OR VARTYPE(nCtns)#"N"
						cErrorMessage = CHR(13)+"Units or Cartons empty/incorrect"
						THROW
					ENDIF

					cPack = ALLTRIM(STR((nUnits/nCtns)))
					IF nStart > 1
						nEndCarton = (nStart + nCtns)-1
					ELSE
						nEndCarton = nCtns
					ENDIF

					cRange = ALLTRIM(STR(nStart))+"-"+ALLTRIM(STR(nEndCarton))
					nStart = nStart + nCtns
					INSERT INTO tempexcel (FA,FB,FE) VALUES (cRange,cStyle,cPack)
					SKIP 1 IN TEMPINTAKE
				ENDDO
				SELECT tempexcel
*					BROWSE
				COPY TO (cPLDir+cContainer+".xls") TYPE XL5
				USE IN tempexcel
			ENDIF

			IF UPPER(TRIM(FD)) = UPPER("SKU#")
				nStart = 1
				SKIP 2
				DO WHILE !EMPTY(TRIM(FD))
					cStyle = TRIM(FD)
					nUnits = INT(VAL(FF))
					nCtns = INT(VAL(FH))
					IF nUnits = 0 OR nCtns = 0 ;
							OR VARTYPE(nUnits)#"N" OR VARTYPE(nCtns)#"N"
						cErrorMessage = CHR(13)+"Units or Cartons empty/incorrect"
						THROW
					ENDIF

					cPack = ALLTRIM(STR((nUnits/nCtns)))
					IF nStart > 1
						nEndCarton = (nStart + nCtns)-1
					ELSE
						nEndCarton = nCtns
					ENDIF

					cRange = ALLTRIM(STR(nStart))+"-"+ALLTRIM(STR(nEndCarton))
					nStart = nStart + nCtns
					INSERT INTO tempexcel (FA,FB,FE) VALUES (cRange,cStyle,cPack)
					SKIP 1 IN TEMPINTAKE
				ENDDO
				SELECT tempexcel
*					BROWSE
				COPY TO (cPLDir+cContainer+".xls") TYPE XL5
				USE IN tempexcel
			ENDIF

		ENDSCAN

		COPY FILE [&cFilenameag] TO (cOrigArchDir+cFilenameag)
		DELETE FILE [&cFilenameag]
	ENDFOR
	DELETE FILE (cOrigPLDir+"*.dbf")
	CLOSE DATABASES ALL
	SELECT 0
	USE F:\edirouting\FTPSETUP SHARED
	REPLACE CHKBUSY WITH .F. FOR UPPER(FTPSETUP.TRANSFER) = UPPER("AgeGroup "+cFTPOffice+" PL")
	USE IN FTPSETUP

CATCH TO oErr
	IF !lNormalExit
		tmessage = "Account: "+ALLTRIM(STR(nAcctNum))
		tmessage = tmessage+CHR(13)+"TRY/CATCH Exeception Message:"+CHR(13)+;
			[  Error: ] + STR(oErr.ERRORNO) +CHR(13)+;
			[  LineNo: ] + STR(oErr.LINENO) +CHR(13)+;
			[  Message: ] + oErr.MESSAGE +CHR(13)+;
			[  Procedure: ] + oErr.PROCEDURE +CHR(13)+;
			[  Details: ] + oErr.DETAILS +CHR(13)+;
			[  LineContents: ] + oErr.LINECONTENTS+CHR(13)+;
			[  UserValue: ] + oErr.USERVALUE+CHR(13)
		IF !EMPTY(cErrorMessage)
			tmessage = tmessage + CHR(13) + cErrorMessage
		ENDIF
		SELECT 0
		USE F:\3pl\DATA\mailmaster ALIAS mm SHARED
		LOCATE FOR edi_type = "MISC" AND taskname = "GENERAL"
		tFrom ="TGF WMS Inbound EDI Operations <toll-edi-ops@tollgroup.com>"
		tsubject = "Error Loading AgeGroup PL File "+cFilenameag
		tsendto = IIF(mm.use_alt,mm.sendtoalt,mm.sendto)
		tcc = IIF(mm.use_alt,mm.ccalt,mm.cc)
		USE IN mm
		tattach = ""
		DO FORM m:\dev\frm\dartmail2 WITH tsendto,tFrom,tsubject,tcc,tattach,tmessage,"A"
		MESSAGEBOX(tmessage,0,"EDI Error",5000)
	ENDIF

ENDTRY


***********************
PROCEDURE excelfill
***********************

	CREATE CURSOR tempexcel (FA C(20),FB C(20),FC C(20),FD C(20),FE C(20))
	SELECT tempexcel
	INSERT INTO tempexcel (FA) VALUES ("GenInbd PL")
	INSERT INTO tempexcel (FA,FB) VALUES ("PREPARED BY:","MIS DEPT")
	INSERT INTO tempexcel (FA,FB) VALUES ("ON DATE:",DTOC(DATE()))
	INSERT INTO tempexcel (FA,FB) VALUES ("CONTAINER:",cContainer)
	INSERT INTO tempexcel (FA,FB) VALUES ("ACCOUNT REF",cRef)
	INSERT INTO tempexcel (FA,FB) VALUES ("BROKER REF",cRef)
	INSERT INTO tempexcel (FA,FB) VALUES ("VESSEL/AWB",cVessel)
	INSERT INTO tempexcel (FA,FB) VALUES ("SEAL",cSeal)
	INSERT INTO tempexcel (FA) VALUES ("COMMENTS")
	INSERT INTO tempexcel (FA) VALUES ("PRINT COMMENTS")
	INSERT INTO tempexcel (FA) VALUES ("")
	INSERT INTO tempexcel (FA,FB,FC,FD,FE) VALUES ("Carton Range",;
		"Style","Color","Size/ID","qty/ctn")

*BROW

ENDPROC
