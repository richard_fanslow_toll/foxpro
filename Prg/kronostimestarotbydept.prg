* Report on Labor from both Kronos and TimeStar by Department (within division?)
* based on kronosadpotbydiv3.prg

LPARAMETERS tcMode  && "WEEK" 

runack("KRONOSADPOTBYDIV")

LOCAL lnError, lcProcessName, loKronosADPOTByDivReport

DECLARE INTEGER CreateMutex IN Win32API ;
	STRING @lpMutexAttributes, ;
	STRING bInitialOwner, ;
	STRING @lpName

DECLARE INTEGER GetLastError IN Win32API

* if running in EXE check to see if already running
IF _VFP.STARTMODE > 0 THEN
	lcProcessName = "KRONOSTIMESTAROTBYDEPT"
	CreateMutex(0,CHR(1),@lcProcessName)
	lnError = GetLastError()
	IF lnError = 183
		WAIT WINDOW TIMEOUT 10 "Kronos-TimeStar Labor by Dept Report process is already running..."
		RETURN .F.
	ENDIF
ENDIF

utilsetup("KRONOSTIMESTAROTBYDEPT")

IF EMPTY( tcMode ) THEN
	tcMode = "MONTH"
ENDIF

loKRONOSTIMESTAROTBYDEPT = CREATEOBJECT('KRONOSTIMESTAROTBYDEPT')
loKRONOSTIMESTAROTBYDEPT.MAIN( tcMode )

schedupdate()

CLOSE DATABASES ALL
RETURN

#DEFINE LOGIT 1
#DEFINE WAITIT 2
#DEFINE NOWAITIT 4
#DEFINE SENDIT 8
#DEFINE CRLF CHR(13) + CHR(10)
#DEFINE LF CHR(10)
#DEFINE RETURN_DATA_MANDATORY .F.
#DEFINE RETURN_DATA_NOT_MANDATORY .T.
#DEFINE DAILY_OT_MULTIPLIER 1.5
#DEFINE DAILY_OT20_MULTIPLIER 2.0
#DEFINE xlEdgeBottom 9
#DEFINE xlContinuous 1
#DEFINE xlMedium -4138

DEFINE CLASS KRONOSTIMESTAROTBYDEPT AS CUSTOM

	cProcessName = 'KRONOSTIMESTAROTBYDEPT'

	lTestMode = .F.  && .F. FOR NORMAL OPERATION, .T. TO SUPPRESS ALL BUT DEVELOPER EMAILS AND CHANGE INPUT TABLE, ETC.

	lAutoYield = .T.

	cStartTime = TTOC(DATETIME())

	cMode = ''

	* connection properties
	nSQLHandle = 0

	* file properties
	nFileHandle = 0

	* filter properties
	*cDivWhere = " AND D.LABORLEV2NM <> '71' "
	* TO EXCLUDE DIV 59 7/5/13 because Underarmour has taken over Rialto as of 7/1/13. MB
	*cDivWhere = " AND (D.LABORLEV2NM <> '59') AND (D.LABORLEV2NM <> '71') "
	cDivWhere = ""
	lIncludeSalariedTimeStarData = .F.
	*lIncludeSalariedTimeStarData = .T.

	* date properties
	dtNow = DATETIME()

	dToday = DATE()
	* to force different pay weeks
	*dToday = {^2017-12-01}

	cToday = DTOC(DATE())

	* wait window properties
	nWaitWindowTimeout = 2
	lWaitWindowIsOn = .T.

	* logfile properties
	lLoggingIsOn = .T.
	cLogFile = 'F:\UTIL\KRONOS\Logfiles\KRONOSTIMESTAROTBYDEPT_log.txt'

	* INTERNAL email properties
	lSendInternalEmailIsOn = .T.
	cFrom = 'Mark.Bennett@Tollgroup.com'
	cSendTo = 'mbennett@fmiint.com'
	cCC = ''
	cSubject = 'Labor by Dept Report'
	cAttach = ''
	cBodyText = ''
	cTopBodyText = ''

	cCOMPUTERNAME = ''
	cUSERNAME = ''

	FUNCTION INIT
		IF NOT DODEFAULT()
			RETURN .F.
		ENDIF
		WITH THIS
			CLEAR
			*SET RESOURCE OFF
			CLOSE DATA
			SET CENTURY ON
			SET DATE AMERICAN
			SET HOURS TO 24
			SET ANSI ON
			SET TALK OFF
			SET DELETED ON
			SET CONSOLE OFF
			SET EXCLUSIVE OFF
			SET SAFETY OFF
			SET EXACT OFF
			SET STATUS BAR ON
			SET SYSMENU OFF
			SET ENGINEBEHAVIOR 70
			_VFP.AUTOYIELD = .lAutoYield
			.cCOMPUTERNAME = GETENV("COMPUTERNAME")
			.cUSERNAME = GETENV("USERNAME")
			IF .lTestMode THEN
				.cSendTo = 'mbennett@fmiint.com'
				.cCC = ''
				.cLogFile = 'F:\UTIL\KRONOS\Logfiles\KRONOSTIMESTAROTBYDEPT_log_TESTMODE.txt'
			ENDIF
			.lLoggingIsOn = .lLoggingIsOn AND NOT EMPTY(.cLogFile)
		ENDWITH
	ENDFUNC


	FUNCTION DESTROY
		WITH THIS
			IF .lLoggingIsOn  THEN
				SET ALTERNATE OFF
				SET ALTERNATE TO
			ENDIF
		ENDWITH
		DODEFAULT()
	ENDFUNC


	FUNCTION MAIN
		LPARAMETERS tcMode
		WITH THIS
			LOCAL lnNumberOfErrors, lcSQL, lcSQLADP, lcSQLADP2, ldToday, ldFromDate, ldToDate, lcSQLFromDate, lcSQLToDate
			LOCAL lcOutputFile, lcMonthYear, ldADPFromDate, ldADPToDate
			LOCAL lcSQL2, lcSQL3, lcSQL4, lcSQL5, loError, llValid
			LOCAL lcXLFileName, ldStartDate, ldEndDate, lcStartDate, lcEndDate, ldDate
			LOCAL lcSQLStartDate, lcSQLEndDate, lcXLFileName, ldYesterday, lnTotalRow
			LOCAL oExcel, oWorkbook, oWorksheet, lnRow, lcRow, lnStartRow, lcStartRow, lnEndRow, lnPP, lcPP, lnNN, lcNN, lcJJ, lcEndRow
			LOCAL lcColsToProcess, lnColumn, lcBoldColumn, i, j, lcBoldRow, luCellValue, lcTargetDirectory, ldPayDate, lcFiletoSaveAs
			LOCAL lcFileDate, lcDivision, lcTitle, llSaveAgain, lcPayCode, lcSpecialWhere
			LOCAL lcSQLToday, lcSQLTomorrow, ldToday, ldTomorrow, lcExcluded9999s, lcFoxProTimeReviewerTable, lcMissingApprovals
			LOCAL lcAuditSubject, oWorkbookAUDIT, oWorksheetAUDIT, lcSendTo, lcCC, lcBodyText, lcXLFileNameAUDIT, lcTotalRow
			LOCAL lcTRWhere, lnStraightHours, lnStraightDollars, lnOTDollars, lcFilePrefix
			LOCAL ldPass2Date, lcSpreadsheetTemplate
			LOCAL lcOrderBy, lnTempMarkup, lcOutputFileSteve
			LOCAL lnShiftDiff, lcXLSummaryFileName, lnWage, lnOTPay, lnRegPay, lcRootPDFFilename
			LOCAL ltInpunch, lnTotHours, lcADPFromDate, lcADPToDate, lcADPDivWhere, lcADPCompList, lnLHDCOUNT, lcLHDMessage, lnLHDFTEHours

			TRY
				lnNumberOfErrors = 0

				IF .lLoggingIsOn THEN
					SET ALTERNATE TO (.cLogFile) ADDITIVE
					SET ALTERNATE ON
				ENDIF

				.TrackProgress('Kronos-TimeStar Labor by Dept Report process started....', LOGIT+SENDIT+NOWAITIT)
				.TrackProgress('COMPUTERNAME = ' + .cCOMPUTERNAME, LOGIT+SENDIT)
				.TrackProgress('USERNAME = ' + .cUSERNAME, LOGIT+SENDIT)
				.TrackProgress('PROJECT = KRONOSTIMESTAROTBYDEPT', LOGIT+SENDIT)
				.TrackProgress('MODE = ' + tcMode, LOGIT+SENDIT)
				.TrackProgress('Salaried Data is ' + IIF(.lIncludeSalariedTimeStarData,"INCLUDED","EXCLUDED"), LOGIT+SENDIT)
				
				
				IF .lTestMode THEN
					.TrackProgress('                        TEST MODE                        ', LOGIT+SENDIT)
				ENDIF

				IF EMPTY(tcMode) OR (NOT INLIST(tcMode,"DATERANGE","MONTH","MTD","WEEK")) THEN
					.TrackProgress('Missing or Invalid tcMode = ' + TRANSFORM(tcMode), LOGIT+SENDIT)
					THROW
				ELSE
					.cMode = tcMode
				ENDIF

				ldToday = .dToday

				DO CASE
					CASE .cMode = "WEEK"  && prior pay period
						ldDate = ldToday
						DO WHILE DOW(ldDate,1) <> 7
							ldDate = ldDate - 1
						ENDDO
						ldToDate = ldDate
						ldFromDate = ldDate - 6
					CASE .cMode = "DATERANGE"
						ldFromDate = {^2017-11-01}
						ldToDate = {^2017-11-17}
					CASE .cMode = "MONTH"  && prior month
						ldToDate = ldToday - DAY(ldToday)  && this is last day of prior month
						ldFromDate = GOMONTH(ldToDate + 1,-1)
					CASE .cMode = "MTD"  && month-to-date						
						ldToDate = ldToday
						ldFromDate = ldToday - DAY(ldToday) + 1
				ENDCASE

				*!*	*************************************************************************
				*!*	* force adp paydates, which may be necessary if paydate was not a friday
				*!*	ldADPToDate = {^2013-11-01}
				*!*	ldADPFromDate = {^2013-11-30}
				*!*	*************************************************************************

				lcMonthYear = TRANSFORM(ldFromDate) + "-" + TRANSFORM(ldToDate)

				.cSubject = 'Kronos-TimeStar Labor by Dept Report for: ' + lcMonthYear
				lcOutputFileSummary = "F:\UTIL\KRONOS\TEMPTIMEREPORTS\KRONOSTIMESTAROTBYDEPT_" + STRTRAN(lcMonthYear,"/","") + ".XLS"

				lcSQLFromDate = .GetSQLStartDateTimeString( ldFromDate )
				lcSQLToDate = .GetSQLEndDateTimeString( ldToDate )

*!*					lcADPFromDate = "DATE'" + TRANSFORM(YEAR(ldADPFromDate)) + "-" + PADL(MONTH(ldADPFromDate),2,'0') + "-" + PADL(DAY(ldADPFromDate),2,'0') + "'"
*!*					lcADPToDate = "DATE'" + TRANSFORM(YEAR(ldADPToDate)) + "-" + PADL(MONTH(ldADPToDate),2,'0') + "-" + PADL(DAY(ldADPToDate),2,'0') + "'"

				IF USED('CURWTKHOURS') THEN
					USE IN CURWTKHOURS
				ENDIF
				IF USED('CURWTKHOURSPRE') THEN
					USE IN CURWTKHOURSPRE
				ENDIF
				IF USED('CURTIMESHEET') THEN
					USE IN CURTIMESHEET
				ENDIF
				IF USED('CURTIMESHEET2') THEN
					USE IN CURTIMESHEET2
				ENDIF

				IF USED('CURTEMPS') THEN
					USE IN CURTEMPS
				ENDIF

				IF USED('CURTEMPSUM') THEN
					USE IN CURTEMPSUM
				ENDIF

				IF USED('CURADP') THEN
					USE IN CURADP
				ENDIF
				IF USED('CURDIVS') THEN
					USE IN CURDIVS
				ENDIF

				* main SQL
				lcSQL = ;
					" SELECT " + ;
					" '1' AS CPASS, " + ;
					" D.LABORLEV5NM AS AGENCYNUM, " + ;
					" D.LABORLEV5DSC AS AGENCY, " + ;
					" D.LABORLEV2NM AS DIVISION, " + ;
					" D.LABORLEV3NM AS DEPT, " + ;
					" D.LABORLEV2DSC AS DIVNAME, " + ;
					" D.LABORLEV3DSC AS DEPTNAME, " + ;
					" D.LABORLEV6NM AS NEWHIRE, " + ;
					" D.LABORLEV7NM AS TIMERVWR, " + ;
					" C.FULLNM AS EMPLOYEE, " + ;
					" C.SHORTNM AS SHORTNAME, " + ;
					" C.PERSONNUM AS FILE_NUM, " + ;
					" C.PERSONID, " + ;
					" A.EMPLOYEEID, " + ;
					" E.ABBREVIATIONCHAR AS PAYCODE, " + ;
					" E.NAME AS PAYCODEDESC, " + ;
					" (SUM(A.DURATIONSECSQTY) / 3600.00) AS TOTHOURS, " + ;
					" SUM(A.MONEYAMT) AS TOTPAY, " + ;
					" 0000.00 AS WAGE " + ;
					" FROM WFCTOTAL A " + ;
					" JOIN WTKEMPLOYEE B " + ;
					" ON B.EMPLOYEEID = A.EMPLOYEEID " + ;
					" JOIN PERSON C " + ;
					" ON C.PERSONID = B.PERSONID " + ;
					" JOIN LABORACCT D " + ;
					" ON D.LABORACCTID = A.LABORACCTID " + ;
					" JOIN PAYCODE E " + ;
					" ON E.PAYCODEID = A.PAYCODEID " + ;
					" WHERE (A.APPLYDTM >= " + lcSQLFromDate + ")" + ;
					" AND (A.APPLYDTM <= " + lcSQLToDate + ")" + ;
					" AND (D.LABORLEV1NM = 'TMP') " + ;
					.cDivWhere + ;
					" GROUP BY D.LABORLEV5NM, D.LABORLEV5DSC, D.LABORLEV2NM, D.LABORLEV3NM, D.LABORLEV2DSC, D.LABORLEV3DSC, D.LABORLEV6NM, D.LABORLEV7NM, C.FULLNM, C.SHORTNM, C.PERSONNUM, C.PERSONID, A.EMPLOYEEID, E.ABBREVIATIONCHAR, E.NAME " + ;
					" ORDER BY 1, 2, 3, 4, 7, 8, 9 "

*!*					SET TEXTMERGE ON
*!*					TEXT TO	lcSQL2 NOSHOW
*!*	SELECT
*!*	DISTINCT
*!*	D.LABORLEV2NM AS DIVISION,
*!*	D.LABORLEV2DSC AS DIVNAME
*!*	FROM LABORACCT D
*!*	ORDER BY 1, 2
*!*					ENDTEXT
*!*					SET TEXTMERGE OFF

				SET TEXTMERGE ON
				TEXT TO	lcSQL2 NOSHOW
SELECT 
DISTINCT
NAME AS DIVISION, 
DESCRIPTION AS DIVNAME
FROM LABORLEVELENTRY
WHERE LABORLEVELDEFID = 2
AND INACTIVE <> 1
ORDER BY 1, 2
				ENDTEXT
				SET TEXTMERGE OFF

				lcSQL5 = ;
					" SELECT " + ;
					" PERSONNUM AS FILE_NUM, " + ;
					" EVENTDATE, " + ;
					" INPUNCHDTM " + ;
					" FROM VP_TIMESHTPUNCHV42 " + ;
					" WHERE (EVENTDATE >= " + lcSQLFromDate + ")" + ;
					" AND (EVENTDATE <= " + lcSQLToDate + ")" + ;
					" AND (LABORLEVELNAME1 = 'TMP') " + ;
					" ORDER BY PERSONNUM, EVENTDATE, INPUNCHDTM "


				IF .lTestMode THEN
					.TrackProgress('', LOGIT+SENDIT)
					.TrackProgress('lcSQL =' + lcSQL, LOGIT+SENDIT)
					.TrackProgress('', LOGIT+SENDIT)
					.TrackProgress('lcSQL5 =' + lcSQL5, LOGIT+SENDIT)
					.TrackProgress('', LOGIT+SENDIT)
					.TrackProgress('ldToDate =' + TRANSFORM(ldToDate), LOGIT+SENDIT)
					.TrackProgress('ldFromDate =' + TRANSFORM(ldFromDate), LOGIT+SENDIT)
*!*						.TrackProgress('ldADPToDate =' + TRANSFORM(ldADPToDate), LOGIT+SENDIT)
*!*						.TrackProgress('ldADPFromDate =' + TRANSFORM(ldADPFromDate), LOGIT+SENDIT)
				ENDIF

				.nSQLHandle = SQLCONNECT("WTK2","sa","kr0n1tes")

				IF .nSQLHandle > 0 THEN

					IF .ExecSQL(lcSQL, 'CURWTKHOURSPRE', RETURN_DATA_MANDATORY) AND ;
							.ExecSQL(lcSQL2, 'CURDIVS', RETURN_DATA_MANDATORY) AND ;
							.ExecSQL(lcSQL5, 'CURTIMESHEET2', RETURN_DATA_MANDATORY) THEN
							
							
*!*		SELECT CURDIVS
*!*		browse

						SELECT ;
							FILE_NUM, ;
							TTOD(EVENTDATE) AS EVENTDATE,  ;
							INPUNCHDTM ;
							FROM CURTIMESHEET2 ;
							INTO CURSOR CURTIMESHEET ;
							ORDER BY FILE_NUM, EVENTDATE, INPUNCHDTM ;
							WHERE NOT ISNULL( INPUNCHDTM )

						IF USED('CURTIMESHEET2') THEN
							USE IN CURTIMESHEET2
						ENDIF

						* there was data in entire date range, marked as Pass = 1, which will be deleted later

						* add a foxpro date field to the cursor
						SELECT CTOD('') AS PAYDATE, * ;
							FROM CURWTKHOURSPRE ;
							INTO CURSOR CURWTKHOURS ;
							READWRITE

						* now, loop thru date range and create cursor by each individual date
						ldPass2Date = ldFromDate
						DO WHILE ldPass2Date <= ldToDate
							.BuildDailyCursor( ldPass2Date )
							ldPass2Date =ldPass2Date + 1
						ENDDO

						* at this point both pass 1 & 2 data are combined in CURWTKHOURS

						SELECT CURWTKHOURS
						INDEX ON CPASS TAG TCPASS
						INDEX ON PAYDATE TAG TPAYDATE
						INDEX ON DIVISION TAG TDIV
						INDEX ON AGENCYNUM TAG AGENCYNUM
						INDEX ON DEPT TAG TDEPT
						INDEX ON EMPLOYEE TAG TEMPLOYEE
						INDEX ON FILE_NUM TAG TFILE_NUM

						****** horizontalize cursor so paycodes are in separate columns

						* create cursor with one row per employee which will drive spreadsheet,
						* filter out pass 1, etc.
						lcOrderBy = "ORDER BY A.AGENCY, A.DIVISION, A.DEPT, A.EMPLOYEE, A.FILE_NUM, A.PAYDATE"

						SELECT ;
							A.PAYDATE, ;
							A.DIVISION, ;
							A.AGENCY, ;
							A.AGENCYNUM, ;
							A.DEPT, ;
							A.DEPTNAME, ;
							A.EMPLOYEE, ;
							A.SHORTNAME, ;
							A.FILE_NUM, ;
							A.WAGE, ;
							A.NEWHIRE, ;
							' ' AS CALCDSHIFT, ;
							CTOT('') AS INPUNCHDTM, ;
							0000.00 AS REG_HRS, ;
							0000.00 AS OT_HRS, ;
							0000.00 AS OT20_HRS, ;
							0000.00 AS UNPD_HRS, ;
							000000000.00 AS REGPAY, ;
							000000000.00 AS OTPAY ;
							FROM CURWTKHOURS A ;
							INTO CURSOR CURTEMPS ;
							WHERE CPASS = '2' ;
							GROUP BY A.PAYDATE, A.DIVISION, A.AGENCY, A.AGENCYNUM, A.DEPT, A.DEPTNAME, A.EMPLOYEE, A.SHORTNAME, A.FILE_NUM ;
							&lcOrderBy. ;
							READWRITE

						*A.DIVNAME, ;
						*GROUP BY A.PAYDATE, A.DIVISION, A.DIVNAME, A.AGENCY, A.AGENCYNUM, A.DEPT, A.DEPTNAME, A.EMPLOYEE, A.SHORTNAME, A.FILE_NUM ;

						* populate hours/code fields in main cursor
						SELECT CURTEMPS
						SCAN
							SCATTER MEMVAR
							STORE 0000.00 TO ;
								m.REG_HRS, ;
								m.OT_HRS, ;
								m.OT20_HRS, ;
								m.UNPD_HRS

							SELECT CURWTKHOURS
							SCAN FOR ;
									CPASS = '2' AND ;
									PAYDATE = CURTEMPS.PAYDATE AND ;
									DIVISION = CURTEMPS.DIVISION AND ;
									DEPT = CURTEMPS.DEPT AND ;
									AGENCY = CURTEMPS.AGENCY AND ;
									AGENCYNUM = CURTEMPS.AGENCYNUM AND ;
									EMPLOYEE = CURTEMPS.EMPLOYEE AND ;
									FILE_NUM = CURTEMPS.FILE_NUM

								lcPayCode = UPPER(ALLTRIM(CURWTKHOURS.PAYCODE))

								DO CASE
									CASE lcPayCode == "REG"
										m.REG_HRS = m.REG_HRS + CURWTKHOURS.TOTHOURS
									CASE lcPayCode == "OTM"
										m.OT_HRS = m.OT_HRS + CURWTKHOURS.TOTHOURS
									CASE lcPayCode == "DTM"
										m.OT20_HRS = m.OT20_HRS + CURWTKHOURS.TOTHOURS
									CASE INLIST(lcPayCode,"???")
										m.UNPD_HRS = m.UNPD_HRS + CURWTKHOURS.TOTHOURS
									OTHERWISE
										* NOTHING
								ENDCASE

							ENDSCAN
							* accumulate totals
							SELECT CURTEMPS
							GATHER MEMVAR
						ENDSCAN

						* populate inpunchdtm and calcdshift in main cursor
						SELECT CURTEMPS
						SCAN
							SELECT CURTIMESHEET
							GOTO TOP
							LOCATE FOR (FILE_NUM = CURTEMPS.FILE_NUM) AND (EVENTDATE = CURTEMPS.PAYDATE)
							IF FOUND() THEN
								ltInpunch = CURTIMESHEET.INPUNCHDTM
								*!*									REPLACE CURTEMPS.INPUNCHDTM WITH ltInpunch, ;
								*!*										CURTEMPS.CALCDSHIFT WITH .GetCalculatedShift( ltInpunch, CURTEMPS.DIVISION )
								REPLACE CURTEMPS.INPUNCHDTM WITH ltInpunch, ;
									CURTEMPS.CALCDSHIFT WITH .GetCalculatedShift( CURTEMPS.AGENCYNUM, ltInpunch )
							ENDIF
						ENDSCAN

						WAIT WINDOW NOWAIT "Preparing data..."

						SELECT CURTEMPS
						GOTO TOP
						IF EOF('CURTEMPS') THEN
							.TrackProgress("There was no data to export!", LOGIT+SENDIT)
						ELSE

							SELECT CURTEMPS
							SCAN

								lnTempMarkup = .getTempAgencyMarkup(CURTEMPS.AGENCYNUM, CURTEMPS.SHORTNAME)

								lnShiftDiff = .getShiftDiff(CURTEMPS.AGENCYNUM, CURTEMPS.CALCDSHIFT, CURTEMPS.DEPT, CURTEMPS.WAGE)
								lnWage = ((CURTEMPS.WAGE + lnShiftDiff) * lnTempMarkup)

								lnRegPay = (CURTEMPS.REG_HRS * lnWage)

								lnOTPay = (CURTEMPS.OT_HRS * lnWage * DAILY_OT_MULTIPLIER) + (CURTEMPS.OT20_HRS * lnWage * DAILY_OT20_MULTIPLIER)

								REPLACE CURTEMPS.REGPAY WITH lnRegPay, CURTEMPS.OTPAY WITH lnOTPay, CURTEMPS.WAGE WITH lnWage IN CURTEMPS

							ENDSCAN

							* summarize...
							SELECT ;
								lcMonthYear AS PERIOD, ;
								"TEMP    " AS TYPE, ;
								DIVISION,;
								SUM(REG_HRS) AS REG_HRS, ;
								SUM(OT_HRS) AS OT_HRS, ;
								SUM(OT20_HRS) AS OT20_HRS, ;
								SUM(REG_HRS + OT_HRS + OT20_HRS) AS TOT_HRS, ;
								SUM(REGPAY) AS TOT_REGPAY, ;
								SUM(OTPAY) AS TOT_OTPAY, ;
								SUM(REGPAY + OTPAY) AS TOT_ALLPAY ;
								FROM CURTEMPS ;
								INTO CURSOR CURTEMPSUM ;
								GROUP BY 1, 2, 3 ;
								ORDER BY 1, 2, 3 ;
								READWRITE

							=SQLDISCONNECT(.nSQLHandle)
							
							*** now get same type of data from timestar...

							IF USED('EEINFO') THEN
								USE IN EEINFO
							ENDIF
							IF USED('CURTIMEDATAPRE') THEN
								USE IN CURTIMEDATAPRE
							ENDIF
							IF USED('CURTIMEDATA') THEN
								USE IN CURTIMEDATA
							ENDIF
							IF USED('TIMEDATA') THEN
								USE IN TIMEDATA
							ENDIF

							USE F:\UTIL\TIMESTAR\DATA\EEINFO.DBF IN 0 ALIAS EEINFO

							USE F:\UTIL\TIMESTAR\DATA\TIMEDATA.DBF IN 0 ALIAS TIMEDATA
							
							IF .lIncludeSalariedTimeStarData THEN
								lcADPCompList = "('E87','E88','E89')"
							ELSE
								lcADPCompList = "('E87')"
							ENDIF

							SELECT ;
								ADP_COMP, ;
								NAME, ;
								INT(VAL(FILE_NUM)) AS INSPID, ;
								DIVISION, ;
								DEPT, ;
								WORKDATE, ;
								0.00 AS HOURLYRATE, ;
								CODEHOURS, ;
								REGHOURS, ;
								OTHOURS, ;
								DTHOURS, ;
								(REGHOURS + OTHOURS + DTHOURS) AS TOTHOURS, ;
								0000.00 AS REGPAY, ;
								0000.00 AS OTPAY, ;
								0000.00 AS DTPAY, ;
								0000.00 AS GROSSPAY ;
								FROM TIMEDATA ;
								INTO CURSOR CURTIMEDATAPRE ;
								WHERE ADP_COMP IN &lcADPCompList. ;
								AND WORKDATE >= ldFromDate ;
								AND WORKDATE <= ldToDate ;
								ORDER BY FILE_NUM, WORKDATE ;
								READWRITE

*!*								WHERE ADP_COMP = 'E87' ;

							IF .lIncludeSalariedTimeStarData THEN
								SELECT CURTIMEDATAPRE
								SCAN FOR ADP_COMP <> 'E87'
									REPLACE CURTIMEDATAPRE.REGHOURS WITH CURTIMEDATAPRE.CODEHOURS, ;
										CURTIMEDATAPRE.TOTHOURS WITH CURTIMEDATAPRE.CODEHOURS ;
										IN CURTIMEDATAPRE								
								ENDSCAN
							ENDIF


							* POPULATE HOURLYRATE AND $ IN CURDATAPRE
							SELECT CURTIMEDATAPRE

							SCAN
								SELECT EEINFO
								LOCATE FOR INSPID = CURTIMEDATAPRE.INSPID
								IF FOUND() THEN

									REPLACE CURTIMEDATAPRE.HOURLYRATE WITH EEINFO.HOURLYRATE IN CURTIMEDATAPRE

									REPLACE CURTIMEDATAPRE.REGPAY WITH (CURTIMEDATAPRE.REGHOURS * CURTIMEDATAPRE.HOURLYRATE), ;
										CURTIMEDATAPRE.OTPAY WITH (CURTIMEDATAPRE.OTHOURS * CURTIMEDATAPRE.HOURLYRATE * 1.5), ;
										CURTIMEDATAPRE.DTPAY WITH (CURTIMEDATAPRE.DTHOURS * CURTIMEDATAPRE.HOURLYRATE * 2.0) ;
										IN CURTIMEDATAPRE

									REPLACE CURTIMEDATAPRE.GROSSPAY WITH (CURTIMEDATAPRE.REGPAY + CURTIMEDATAPRE.OTPAY + CURTIMEDATAPRE.DTPAY) IN CURTIMEDATAPRE

								ENDIF
							ENDSCAN

*!*	SELECT CURTIMEDATAPRE
*!*	BROWSE
*!*	THROW


							SELECT ;
								DIVISION, ;
								SUM(REGHOURS) AS REGHOURS, ;
								SUM(OTHOURS) AS OTHOURS, ;
								SUM(DTHOURS) AS DTHOURS, ;
								SUM(REGPAY) AS REGPAY, ;
								SUM(OTPAY) AS OTPAY, ;
								SUM(DTPAY) AS DTPAY, ;
								SUM(GROSSPAY) AS GROSSPAY ;
								FROM CURTIMEDATAPRE ;
								INTO CURSOR CURTIMEDATA ;
								GROUP BY DIVISION ;
								ORDER BY DIVISION


							SELECT CURTIMEDATA
							SCAN

								INSERT INTO CURTEMPSUM (PERIOD, TYPE, DIVISION, REG_HRS, OT_HRS, TOT_HRS, TOT_REGPAY, TOT_OTPAY, TOT_ALLPAY) ;
									VALUES (lcMonthYear, "EMPLOYEE", CURTIMEDATA.DIVISION, CURTIMEDATA.REGHOURS, (CURTIMEDATA.OTHOURS + CURTIMEDATA.DTHOURS), ;
									(CURTIMEDATA.REGHOURS + CURTIMEDATA.OTHOURS + CURTIMEDATA.DTHOURS), CURTIMEDATA.REGPAY, (CURTIMEDATA.OTPAY + CURTIMEDATA.DTPAY), CURTIMEDATA.GROSSPAY)

							ENDSCAN

							
							* add estimated hours for LH Drivers, since they won't have hours in TimeStar
							SELECT COUNT(*) AS LHDCOUNT ;
							FROM EEINFO ;
							INTO CURSOR CURLHD ;
							WHERE ((DIVISION + DEPT) = '020655') AND (STATUS <> 'T')
							
							SELECT CURLHD
							LOCATE
							lnLHDCOUNT = CURLHD.LHDCOUNT
							USE IN CURLHD
							.TrackProgress('lnLHDCOUNT =' + TRANSFORM(lnLHDCOUNT), LOGIT+SENDIT)
							
							lcLHDMessage = 'Note: line haul driver FTE calculated based on # of currently active drivers'							
							DO CASE
								CASE .cMode = "MONTH" 
									lnLHDFTEHours = 160
								CASE .cMode = "WEEK" 
									lnLHDFTEHours = 40
								OTHERWISE
									lnLHDFTEHours = 0
									lcLHDMessage = 'Note: line haul driver FTE not calculated due to non-standard reporting period'							
							ENDCASE
							
							.TrackProgress('lnLHDFTEHours =' + TRANSFORM(lnLHDFTEHours), LOGIT+SENDIT)
							
							IF lnLHDFTEHours > 0 THEN
							
								SELECT CURTEMPSUM
								LOCATE FOR (ALLTRIM(TYPE) = 'EMPLOYEE') AND (DIVISION = '02')
								IF FOUND() THEN
									REPLACE CURTEMPSUM.REG_HRS WITH (CURTEMPSUM.REG_HRS + (lnLHDCOUNT * lnLHDFTEHours)), ;
										CURTEMPSUM.TOT_HRS WITH (CURTEMPSUM.TOT_HRS + (lnLHDCOUNT * lnLHDFTEHours)) ;
										IN CURTEMPSUM
								ELSE
									.TrackProgress('====> ERROR adding lnLHDCOUNT to CURTEMPSUM', LOGIT+SENDIT)
								ENDIF
							
							ENDIF && lnLHDFTEHours > 0 THEN
							

							* delete output file if it already exists...
							IF FILE(lcOutputFileSummary) THEN
								DELETE FILE (lcOutputFileSummary)
							ENDIF

							SELECT B.DIVNAME, A.* ;
								FROM CURTEMPSUM A ;
								LEFT OUTER JOIN CURDIVS B ;
								ON B.DIVISION = A.DIVISION ;
								INTO CURSOR CURFINAL ;
								ORDER BY A.TYPE, A.DIVISION

							***********************************************************************************
							** output to Excel spreadsheet
							* deleet output file if it already exists...
							IF FILE(lcOutputFileSummary) THEN
								DELETE FILE (lcOutputFileSummary)
							ENDIF

							.TrackProgress('Creating spreadsheet: ' + lcOutputFileSummary, LOGIT+SENDIT+NOWAITIT)
							lcSpreadsheetTemplate = 'F:\UTIL\KRONOS\TEMPLATES\KRONOSTIMESTAROTBYDEPT.XLS'

							oExcel = CREATEOBJECT("excel.application")
							oExcel.displayalerts = .F.
							oExcel.VISIBLE = .F.
							oWorkbook = oExcel.workbooks.OPEN(lcSpreadsheetTemplate)
							oWorkbook.SAVEAS(lcOutputFileSummary)

							oWorksheet = oWorkbook.Worksheets[1]
							oWorksheet.RANGE("A2","I100").ClearContents()

							lnRow = 1

							SELECT CURFINAL
							SCAN
								lnRow = lnRow + 1
								lnEndRow = lnRow
								lcRow = ALLTRIM(STR(lnRow))
								oWorksheet.RANGE("A"+lcRow).VALUE = CURFINAL.PERIOD
								oWorksheet.RANGE("B"+lcRow).VALUE = CURFINAL.TYPE
								oWorksheet.RANGE("C"+lcRow).VALUE = CURFINAL.DIVNAME
								oWorksheet.RANGE("D"+lcRow).VALUE = "'" + CURFINAL.DIVISION
								oWorksheet.RANGE("E"+lcRow).VALUE = CURFINAL.REG_HRS
								oWorksheet.RANGE("F"+lcRow).VALUE = CURFINAL.OT_HRS
								oWorksheet.RANGE("G"+lcRow).VALUE = CURFINAL.OT20_HRS
								oWorksheet.RANGE("H"+lcRow).VALUE = CURFINAL.TOT_HRS
								oWorksheet.RANGE("I"+lcRow).VALUE = CURFINAL.TOT_REGPAY
								oWorksheet.RANGE("J"+lcRow).VALUE = CURFINAL.TOT_OTPAY
								oWorksheet.RANGE("K"+lcRow).VALUE = CURFINAL.TOT_ALLPAY
								oWorksheet.RANGE("L"+lcRow).VALUE = "=(J" + lcRow + "/K" + lcRow + ")"
								* ADD FTE COLUMN FOR STEVE 11/03/2015
								IF CURFINAL.TOT_HRS > 0 THEN
									oWorksheet.RANGE("M"+lcRow).VALUE = CURFINAL.TOT_HRS / 160.0
								ELSE
									oWorksheet.RANGE("M"+lcRow).VALUE = 0.0
								ENDIF
								
								* ADD A NOTE FOR DIV 02 FTE CALCS
								IF (ALLTRIM(CURFINAL.DIVISION) =='02') AND (ALLTRIM(CURFINAL.TYPE) =='EMPLOYEE') THEN
									oWorksheet.RANGE("N"+lcRow).VALUE = lcLHDMessage								
								ENDIF && lnLHDFTEHours > 0 
								
							ENDSCAN


							* populate header counts, sums, etc.
							lnTotalRow = lnEndRow + 2
							lcEndRow = ALLTRIM(STR(lnEndRow))
							lcTotalRow = ALLTRIM(STR(lnTotalRow))

							* sum $ columns
							oWorksheet.RANGE("A" + lcTotalRow).VALUE = "Totals:"
							oWorksheet.RANGE("E" + lcTotalRow).VALUE = "=SUM(E2:E" + lcEndRow + ")"
							oWorksheet.RANGE("F" + lcTotalRow).VALUE = "=SUM(F2:F" + lcEndRow + ")"
							oWorksheet.RANGE("G" + lcTotalRow).VALUE = "=SUM(G2:G" + lcEndRow + ")"
							oWorksheet.RANGE("H" + lcTotalRow).VALUE = "=SUM(H2:H" + lcEndRow + ")"
							oWorksheet.RANGE("I" + lcTotalRow).VALUE = "=SUM(I2:I" + lcEndRow + ")"
							oWorksheet.RANGE("J" + lcTotalRow).VALUE = "=SUM(J2:J" + lcEndRow + ")"
							oWorksheet.RANGE("K" + lcTotalRow).VALUE = "=SUM(K2:K" + lcEndRow + ")"

							oWorksheet.RANGE("L" + lcTotalRow).VALUE = "=(J" + lcTotalRow + "/K" + lcTotalRow + ")"
							
							oWorksheet.RANGE("M" + lcTotalRow).VALUE = "=SUM(M2:M" + lcEndRow + ")"

							* UNDERLINE cell columns to be totaled...
							oWorksheet.RANGE("F" + lcEndRow + ":M" + lcEndRow).BORDERS(xlEdgeBottom).LineStyle = xlContinuous
							oWorksheet.RANGE("F" + lcEndRow + ":M" + lcEndRow).BORDERS(xlEdgeBottom).Weight = xlMedium

							* SAVE AND QUIT EXCEL
							oWorkbook.SAVE()
							oExcel.QUIT()
							**************************************************************

							IF FILE(lcOutputFileSummary) THEN
								.TrackProgress('Output to Summary spreadsheet: ' + lcOutputFileSummary, LOGIT+SENDIT)
								* attach output file to email
								.cAttach = lcOutputFileSummary
								.cBodyText = "See attached report. NOTE: #s are subject to change, pending final time card reviews." + ;
									CRLF + CRLF + "(do not reply - this is an automated report)" + ;
									CRLF + CRLF + "<report log follows>" + ;
									CRLF + .cBodyText
							ELSE
								.TrackProgress('ERROR creating Summary spreadsheet: ' + lcOutputFileSummary, LOGIT+SENDIT)
							ENDIF

						ENDIF && EOF('CURTEMPS)

					ENDIF  &&  .ExecSQL(lcSQL, 'CURWTKHOURSPRE', RETURN_DATA_MANDATORY)

					.TrackProgress('Kronos-TimeStar Labor by Dept Report process finished normally', LOGIT+SENDIT)

				ELSE
					* connection error
					.TrackProgress('Unable to connect to WTK System.', LOGIT+SENDIT)
				ENDIF   &&  .nSQLHandle > 0

			CATCH TO loError

				.TrackProgress('There was an error.',LOGIT+SENDIT)
				.TrackProgress('The Error # is: ' + TRANSFORM(loError.ERRORNO), LOGIT+SENDIT)
				.TrackProgress('The Error Message is: ' + TRANSFORM(loError.MESSAGE), LOGIT+SENDIT)
				.TrackProgress('The Error occurred in Program: ' + TRANSFORM(loError.PROCEDURE), LOGIT+SENDIT)
				.TrackProgress('The Error occurred at line #: ' + TRANSFORM(loError.LINENO), LOGIT+SENDIT)
				lnNumberOfErrors = lnNumberOfErrors + 1
				IF TYPE('oExcel') = 'O' AND NOT ISNULL(oExcel) THEN
					oExcel.QUIT()
				ENDIF
				CLOSE DATA

			ENDTRY

			CLOSE DATA
			WAIT CLEAR
			***************** INTERNAL email results ******************************
			.TrackProgress('About to send status email.',LOGIT)
			.TrackProgress('==================================================================================================================', SENDIT)
			.TrackProgress('The logfile is: ' + .cLogFile, SENDIT)
			.TrackProgress('Kronos-TimeStar Labor by Dept Report process started: ' + .cStartTime, LOGIT+SENDIT)
			.TrackProgress('Kronos-TimeStar Labor by Dept Report process finished: ' + TTOC(DATETIME()), LOGIT+SENDIT)

			IF .lSendInternalEmailIsOn THEN
				* try to trap error from not having dartmail dll's registered on user's pc...
				TRY
					.cBodyText = .cTopBodyText + CRLF + .cBodyText

					DO FORM dartmail2 WITH .cSendTo,.cFrom,.cSubject,.cCC,.cAttach,.cBodyText,"A"
					.TrackProgress('Sent status email.',LOGIT)
				CATCH TO loError
					*=MESSAGEBOX('There was an error sending the status email. Please contact Mark Bennett.',0+64,"KRONOS Temp Time")
					.TrackProgress('There was an error sending the status email.',LOGIT)
					.TrackProgress('The Error # is: ' + TRANSFORM(loError.ERRORNO), LOGIT)
					.TrackProgress('The Error Message is: ' + TRANSFORM(loError.MESSAGE), LOGIT)
					.TrackProgress('The Error occurred in Program: ' + TRANSFORM(loError.PROCEDURE), LOGIT)
					.TrackProgress('The Error occurred at line #: ' + TRANSFORM(loError.LINENO), LOGIT)
					lnNumberOfErrors = lnNumberOfErrors + 1
					CLOSE DATA
				ENDTRY

			ELSE
				.TrackProgress('Did not send status email: THIS.lSendInternalEmailIsOn = FALSE.',LOGIT)
			ENDIF

		ENDWITH
		RETURN
	ENDFUNC && main


	FUNCTION GetSQLStartDateTimeString
		* returns a timestamp string for use in SQL queries
		LPARAMETERS tdDate
		LOCAL lcSetDate, lcSQLDateTimeString
		lcSetDate = SET('DATE')
		SET DATE YMD
		lcSQLDateTimeString   = "{ts '" + TRANSFORM(YEAR(tdDate)) + "-" + PADL(MONTH(tdDate),2,"0") + "-" + PADL(DAY(tdDate),2,"0") + " 00:00:00.0'}"
		SET DATE &lcSetDate.
		RETURN lcSQLDateTimeString
	ENDFUNC


	FUNCTION GetSQLEndDateTimeString
		* returns a timestamp string for use in SQL queries
		LPARAMETERS tdDate
		LOCAL lcSetDate, lcSQLDateTimeString
		lcSetDate = SET('DATE')
		SET DATE YMD
		lcSQLDateTimeString = "{ts '" + TRANSFORM(YEAR(tdDate)) + "-" + PADL(MONTH(tdDate),2,"0") + "-" + PADL(DAY(tdDate),2,"0") + " 23:59:59.9'}"
		SET DATE &lcSetDate.
		RETURN lcSQLDateTimeString
	ENDFUNC


	FUNCTION ExecSQL
		LPARAMETERS tcSQL, tcCursorName, tlNoDataReturnedIsOkay
		LOCAL llRetval, lnResult
		WITH THIS
			* close target cursor if it's open
			IF USED(tcCursorName)
				USE IN (tcCursorName)
			ENDIF
			WAIT WINDOW "Fetching remote data; please wait..." NOWAIT
			lnResult = SQLEXEC(.nSQLHandle, tcSQL, tcCursorName)
			llRetval = ( lnResult > 0 )
			IF llRetval THEN
				* see if any data came back
				IF NOT tlNoDataReturnedIsOkay THEN
					IF (NOT USED(tcCursorName)) OR EOF(tcCursorName) THEN
						llRetval = .F.
						.TrackProgress('No data was returned by this query: ' +CRLF + tcSQL, LOGIT+SENDIT)
					ENDIF
				ENDIF
			ELSE
				.TrackProgress("Result Code [" + TRANSFORM(lnResult) + "] returned by this query: " + tcSQL, LOGIT+SENDIT)
			ENDIF
			WAIT CLEAR
			RETURN llRetval
		ENDWITH
	ENDFUNC


	PROCEDURE TrackProgress
		* do any combination of Wait Window, writing to logfile, and adding to body of email,
		* based on nFlags parameter.
		LPARAMETERS tcExpression, tnFlags
		WITH THIS
			IF BITAND(tnFlags,LOGIT) = LOGIT THEN
				IF .lLoggingIsOn THEN
					?
					? .cProcessName + "  " + TTOC(DATETIME()) + ": " + tcExpression
				ENDIF
			ENDIF
			IF .lWaitWindowIsOn THEN
				IF BITAND(tnFlags,NOWAITIT) = NOWAITIT THEN
					WAIT WINDOW tcExpression NOWAIT
				ENDIF
				IF BITAND(tnFlags,WAITIT) = WAITIT THEN
					WAIT WINDOW tcExpression TIMEOUT .nWaitWindowTimeout
				ENDIF
			ENDIF
			IF BITAND(tnFlags,SENDIT) = SENDIT THEN
				IF .lSendInternalEmailIsOn THEN
					.cBodyText = .cBodyText + tcExpression + CRLF + CRLF
				ENDIF
			ENDIF
		ENDWITH
	ENDPROC  &&  TrackProgress


	FUNCTION GetCalculatedShift
		LPARAMETERS tcTempCo, tInpunchDTM
		LOCAL lcShift, lcTimePortion
		lcShift = "?"
		lcTimePortion = RIGHT(TTOC(tInpunchDTM),8)
		tcTempCo = UPPER(ALLTRIM(tcTempCo))
		WITH THIS
			DO CASE
				CASE tcTempCo = 'A'
					DO CASE
						CASE lcTimePortion < "13:00:00"
							lcShift = "1"
						CASE lcTimePortion < "20:00:00"
							lcShift = "2"
						OTHERWISE
							lcShift = "3"
					ENDCASE
				CASE INLIST(tcTempCo,'B','BJ','BS')
					DO CASE
						CASE lcTimePortion < "12:00:00"
							lcShift = "1"
						OTHERWISE
							lcShift = "2"
					ENDCASE
				OTHERWISE
					* nothing
			ENDCASE
		ENDWITH
		RETURN lcShift
	ENDFUNC



	FUNCTION getTempAgencyMarkup
		LPARAMETERS tcTempCo, tcSHORTNAME
		LOCAL lnMarkup
		tcTempCo = UPPER(ALLTRIM(tcTempCo))
		DO CASE
			CASE tcTempCo = 'Z'
					lnMarkup = 1.370
			OTHERWISE
				* another temp co
				lnMarkup = 1.310
		ENDCASE
		RETURN lnMarkup
	ENDFUNC


	FUNCTION getShiftDiff
		LPARAMETERS tcTempCo, tcCalcdShift, tcDept, tnBaseWage
		LOCAL lnShiftDiff, lcDept, lcCalcdShift
		tcTempCo = UPPER(ALLTRIM(tcTempCo))
		lnShiftDiff = 0.00
		lcDept = ALLTRIM( tcDept )
		lcCalcdShift = ALLTRIM( tcCalcdShift )
		DO CASE
			CASE tcTempCo = 'A'
				* Tri-State rules, updated per Neftali for base wage check 10/27/09 MB.
				* basic rules for shift differential:
				* if lcDept = '0622' (forklift drivers) or CalcdShift = '1' or base wage > 8.00 then no differential
				* otherwise, give differential.
				DO CASE
					CASE (lcDept = '0622') OR (lcCalcdShift = '1') OR (tnBaseWage > 8.00)
						lnShiftDiff = 0.00
					OTHERWISE
						lnShiftDiff = 0.20
				ENDCASE
			CASE INLIST(tcTempCo,'B','BJ','BS')
				* Select rules, per Alex Figueroa and Juan Santisteban 2/10/10 MB.
				* basic rules for shift differential:
				* simply, if temp works 2nd shift, they get $0.20 extra
				DO CASE
					CASE (lcCalcdShift = '1')
						lnShiftDiff = 0.00
					OTHERWISE
						lnShiftDiff = 0.20
				ENDCASE
			OTHERWISE
				lnShiftDiff = 0.00
		ENDCASE
		RETURN lnShiftDiff
	ENDFUNC


	PROCEDURE BuildDailyCursor
		LPARAMETERS tdPass2Date
		WITH THIS
			LOCAL lcSQLStartDate, lcSQLEndDate, lcSQLPass2, lcSQLPass2A

			SET DATE YMD

			*	 create "YYYYMMDD" safe date format for use in SQL query
			* NOTE: for SQL we are going to use < EndDate ( not <= ), so we are going to add 1 to that date here for querying purposes.
			lcSQLStartDate = STRTRAN(DTOC(tdPass2Date),"/","")
			lcSQLEndDate = STRTRAN(DTOC(tdPass2Date + 1),"/","")

			SET DATE AMERICAN

			IF USED('CURWTKHOURSTEMPPRE') THEN
				USE IN CURWTKHOURSTEMPPRE
			ENDIF
			IF USED('CURWTKHOURSTEMP') THEN
				USE IN CURWTKHOURSTEMP
			ENDIF
			IF USED('CURWAGES') THEN
				USE IN CURWAGES
			ENDIF

			* pass 2 SQL
			* the join to basewagehistory table was causing duplicate records; had to get wage from separate sql
			* 9/23/09 MB

			* pass 2 SQL
			lcSQLPass2 = ;
				" SELECT " + ;
				" '2' AS CPASS, " + ;
				" D.LABORLEV5NM AS AGENCYNUM, " + ;
				" D.LABORLEV5DSC AS AGENCY, " + ;
				" D.LABORLEV2NM AS DIVISION, " + ;
				" D.LABORLEV3NM AS DEPT, " + ;
				" D.LABORLEV2DSC AS DIVNAME, " + ;
				" D.LABORLEV3DSC AS DEPTNAME, " + ;
				" D.LABORLEV6NM AS SHIFT, " + ;
				" D.LABORLEV7NM AS TIMERVWR, " + ;
				" C.FULLNM AS EMPLOYEE, " + ;
				" C.SHORTNM AS SHORTNAME, " + ;
				" C.PERSONNUM AS FILE_NUM, " + ;
				" C.PERSONID, " + ;
				" A.EMPLOYEEID, " + ;
				" E.ABBREVIATIONCHAR AS PAYCODE, " + ;
				" E.NAME AS PAYCODEDESC, " + ;
				" (SUM(A.DURATIONSECSQTY) / 3600.00) AS TOTHOURS, " + ;
				" SUM(A.MONEYAMT) AS TOTPAY, " + ;
				" 0000.00 AS WAGE " + ;
				" FROM WFCTOTAL A " + ;
				" JOIN WTKEMPLOYEE B " + ;
				" ON B.EMPLOYEEID = A.EMPLOYEEID " + ;
				" JOIN PERSON C " + ;
				" ON C.PERSONID = B.PERSONID " + ;
				" JOIN LABORACCT D " + ;
				" ON D.LABORACCTID = A.LABORACCTID " + ;
				" JOIN PAYCODE E " + ;
				" ON E.PAYCODEID = A.PAYCODEID " + ;
				" WHERE (A.APPLYDTM >= '" + lcSQLStartDate + "')" + ;
				" AND (A.APPLYDTM < '" + lcSQLEndDate + "')" + ;
				" AND (D.LABORLEV1NM = 'TMP') " + ;
				.cDivWhere + ;
				" GROUP BY D.LABORLEV5NM, D.LABORLEV5DSC, D.LABORLEV2NM, D.LABORLEV3NM, D.LABORLEV2DSC, D.LABORLEV3DSC, D.LABORLEV6NM, D.LABORLEV7NM, C.FULLNM, C.SHORTNM, C.PERSONNUM, C.PERSONID, A.EMPLOYEEID, E.ABBREVIATIONCHAR, E.NAME " + ;
				" ORDER BY 1, 2, 3, 4, 5, 6, 7, 8, 9 "

			lcSQLPass2A = ;
				" SELECT " + ;
				" EMPLOYEEID, " + ;
				" BASEWAGEHOURLYAMT AS WAGE, " + ;
				" EFFECTIVEDTM, " + ;
				" EXPIRATIONDTM " + ;
				" FROM BASEWAGERTHIST " + ;
				" WHERE '" + lcSQLStartDate + "' BETWEEN EFFECTIVEDTM AND EXPIRATIONDTM " + ;
				" ORDER BY EMPLOYEEID DESC, EXPIRATIONDTM DESC "

			IF .lTestMode THEN
				.TrackProgress('', LOGIT+SENDIT)
				.TrackProgress('lcSQLPass2 =' + lcSQLPass2, LOGIT+SENDIT)
				.TrackProgress('', LOGIT+SENDIT)
				.TrackProgress('lcSQLPass2A =' + lcSQLPass2A, LOGIT+SENDIT)
			ENDIF

			IF .ExecSQL(lcSQLPass2, 'CURWTKHOURSTEMPPRE', RETURN_DATA_NOT_MANDATORY) AND ;
					.ExecSQL(lcSQLPass2A, 'CURWAGES', RETURN_DATA_MANDATORY) THEN

				IF USED('CURWTKHOURSTEMPPRE') AND NOT EOF('CURWTKHOURSTEMPPRE') THEN

					* data was returned

					* add a foxpro date field to the cursor
					SELECT tdPass2Date AS PAYDATE, * ;
						FROM CURWTKHOURSTEMPPRE ;
						INTO CURSOR CURWTKHOURSTEMP ;
						ORDER BY EMPLOYEE ;
						READWRITE

					*!*	SELECT CURWTKHOURSTEMP
					*!*	BROWSE
					*!*	SELECT CURWAGES
					*!*	BROWSE

					* populate WAGE field in CURWTKHOURSTEMP
					SELECT CURWTKHOURSTEMP
					SCAN
						SELECT CURWAGES
						LOCATE FOR EMPLOYEEID = CURWTKHOURSTEMP.EMPLOYEEID
						IF FOUND() THEN
							REPLACE CURWTKHOURSTEMP.WAGE WITH CURWAGES.WAGE
						ENDIF
					ENDSCAN

					*!*	SELECT CURWTKHOURSTEMP
					*!*	BROWSE


					* now append it to CURWTKHOURS
					SELECT CURWTKHOURS
					APPEND FROM DBF('CURWTKHOURSTEMP')

				ELSE
					* NO DATA WAS RETURNED -- MAKE A NOTE OF IT IN CASE THERE SHOULD HAVE BEEN!
					.cTopBodyText = .cTopBodyText + "NOTE: no TEMP data found for date: " + TRANSFORM(tdPass2Date) + CRLF

				ENDIF  &&  USED('CURWTKHOURSTEMPPRE') AND NOT EOF('CURWTKHOURSTEMPPRE')

			ENDIF  &&  .ExecSQL(lcSQLPass2, 'CURWTKHOURSTEMPPRE', RETURN_DATA_NOT_MANDATORY)

		ENDWITH
		RETURN
	ENDPROC && BuildDailyCursor


ENDDEFINE
