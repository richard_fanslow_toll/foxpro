*!* tjmaxx212outbound_create.prg (new EDI document), for TJX Corp.
*!* Created 10.08.2014, Joe

PARAMETERS nWO_Num

*ASSERT .F. MESSAGE "At start of TJMAXX 212 process"

PUBLIC c_CntrlNum,c_GrpCntrlNum,cWO_Num,cGroupName,cCustName,cFilename,cFilename2,cFilename3,div214,cST_CSZ,cPrgName
PUBLIC tsendto,tcc,tsendtoerr,tccerr,tattach,cDelName,cMacysnet,cTripid,cMailName,lTestfile,lTJFilesOut,lEmail,cErrMsg212,cOffice,xOffice
cErrMsg212 = " "
xOffice = " "
cPrgName = "TJMAXX 212"

TRY
	lTesting = .F.
	lTestfile = .F. && Remove this 'lTestfile' variable when testing of the edi_trigger records is complete
	DO m:\dev\prg\_setvars WITH lTesting
	ON ESCAPE CANCEL

	closefiles()
	STORE "" TO cFilename,cFilename2,cFilename3,cShip_ref,cST_CSZ,cMacysnet,cTripid,cWO_Num,cLeg,cFileInfo

	lIsError = .F.
	lDoCatch = .T.
	lEmail = !lTesting
	lDoError = .F.
	lTJFilesOut = !lTesting

	nAcctNum = 6565
	cOffice = "I"

	IF VARTYPE(nWO_Num)= "L"
		IF lTesting
			CLOSE DATABASES ALL
			nWO_Num = 1627663
			dUseDate = DATE()
		ELSE
			cMsg = "No params specified"
			WAIT WINDOW cMsg TIMEOUT 2
			DO ediupdate WITH cMsg,.T.
		ENDIF
	ENDIF
	cWO_Num = ALLTRIM(STR(nWO_Num))

	xReturn = "XXX"
	DO m:\dev\prg\wf_alt WITH cOffice,nAcctNum
	IF EMPTY(ALLTRIM(xReturn))
		DO ediupdate WITH "NO WHSE",.T.
		THROW
	ENDIF
	cUseFolder = UPPER(xReturn)

	DIMENSION thisarray(1)
	tfrom = "TGF EDI Operations <tgf-edi-ops@fmiint.com>"

	IF USED('mm')
	USE IN mm
	ENDIF
	
	SELECT 0
	USE F:\3pl\DATA\mailmaster ALIAS mm
	LOCATE FOR mm.edi_type = "MISC" AND mm.taskname = "GENERAL"
	tsendto = IIF(mm.use_alt,mm.sendtoalt,mm.sendto)
	tcc = IIF(mm.use_alt,mm.ccalt,mm.cc)
	USE IN mm
	tattach = ""

	cCustName = "TJMAXX"
	cCustFolder = "TJMAX"
	cCustPrefix = "TJM212"+LOWER(cOffice)
	dtmail = TTOC(DATETIME())
	dt2 = DATETIME()
	cMailName = PROPER(cCustName)
	lcpath = "f:\ftpusers\"+cCustFolder+"\212-Staging\"
	CD &lcpath

*!* Open Tables
	IF USED('manifest')
		USE IN manifest
	ENDIF
	USE F:\wo\wodata\manifest IN 0 ALIAS manifest SHARED

	xsqlexec("select * from dellocs",,,"wo")
	index on str(accountid,4)+location tag acct_loc
	set order to

	cST_Name = "TGF INC."
	DO CASE
		CASE cOffice = "C"
			cFMICity = "SAN PEDRO"
			cFMIState = "CA"
		CASE cOffice = "M"
			cFMICity = "MIAMI"
			cFMIState = "FL"
		OTHERWISE
			cFMICity = "CARTERET"
			cFMIState = "NJ"
	ENDCASE

	cterminator = ">"  && Used at end of ISA segment
	cfd = "*"  && Field delimiter
	csegd = "~" && Segment delimiter

	csendqual = "ZZ"  && Sender qualifier
	csendid = "TGFUSA"  && Sender ID code
	crecqual = "12"  && Recip qualifier
	crecid = "5084758255"   && Recip ID Code
	crecid = IIF(lTesting OR lTestfile,crecid+"T",crecid)

	cDate = DTOS(DATE())
	cTruncDate = RIGHT(cDate,6)
	cTruncTime = SUBSTR(TTOC(DATETIME(),1),9,4)
	cfiledate = cDate+cTruncTime
	csendidlong = PADR(csendid,15," ")
	crecidlong = PADR(crecid,15," ")
	cString = ""

	nSTCount = 0
	nSegCtr = 0
	nLXNum = 1
	cISACode = IIF(lTesting OR lTestfile,"T","P")

	WAIT CLEAR
	WAIT WINDOW "Now creating MANIFEST WO#-based 212 information..." NOWAIT
	SELECT manifest
	SET ORDER TO TAG wo_num
	LOCATE
	IF !SEEK(nWO_Num,"manifest","wo_num")
		THROW
	ENDIF

	SELECT dellocs
	LOCATE FOR accountid = 6565 AND location=manifest.delloc

	lcLocID = dellocs.LABEL
	whichShipTo = dellocs.acct_name
	
	**need to change 28W0882A to 28W0881B for manifests that conatin any pos starting with "10" per TJX and Juan - mvw 03/10/17
	if lcLocID='28W0882A'
		select count(1) as cnt from manifest where wo_num = nwo_num and po_num = '10' into cursor xtemp
		if xtemp.cnt>0
			lcLocID='28W0881B'
		endif
		use in xtemp
	endif

*!* More Variables

	dt1 = TTOC(DATETIME(),1)
	cFilename = (cCustPrefix+dt1+".edi")
	cFilenameShort = JUSTFNAME(cFilename)

	cFilename2 = ("f:\ftpusers\tjmax\212out\"+cFilenameShort)  && added this 9/28/2015, AS2 makes copies in the archive
	cFilename3 = ("f:\ftpusers\tjmax\OUT\"+cFilenameShort)

	nHandle = FCREATE(cFilename)

	DO num_incr_isa
	cISA_Num = PADL(c_CntrlNum,9,"0")

	STORE "ISA"+cfd+"00"+cfd+"          "+cfd+"00"+cfd+"          "+cfd+csendqual+cfd+csendidlong+cfd+;
		crecqual+cfd+crecidlong+cfd+cTruncDate+cfd+cTruncTime+cfd+"U"+cfd+"00401"+cfd+;
		cISA_Num+cfd+"0"+cfd+cISACode+cfd+cterminator+csegd TO cString
	DO cstringbreak

	STORE "GS"+cfd+"TM"+cfd+csendid+cfd+crecid+cfd+cDate+cfd+cTruncTime+cfd+c_CntrlNum+;
		cfd+"X"+cfd+"004010"+csegd TO cString
	DO cstringbreak

	DO num_incr_st
	STORE "ST"+cfd+"212"+cfd+PADL(c_GrpCntrlNum,9,"0")+csegd TO cString
	DO cstringbreak
	nSTCount = nSTCount + 1
	nSegCtr = nSegCtr + 1

	nLXNum = 1
	cLXNum = ALLTRIM(STR(nLXNum))
*cLXNum = PADR(ALLTRIM(STR(nLXNum)),4,"0")

	cSCAC = ALLTRIM(manifest.carrier)
	DO CASE
    CASE cSCAC = "HJBT"
    	cSCAC = "HJBC"
	CASE cSCAC = "HJBI"
		cSCAC = "HJBC"
	CASE cSCAC = "SWIF"
		cSCAC = "SWIC"
	CASE cSCAC = "PTFS"
		cSCAC = "GLTN"
	CASE cSCAC = "WERN"
		cSCAC = "WRNW"
	OTHERWISE
		cSCAC = cSCAC
	ENDCASE

	cTrailer = ALLTRIM(manifest.trailer)

	STORE "ATA"+cfd+"TGFJ"+cfd+cWO_Num+cfd+cDate+csegd TO cString
	DO cstringbreak
	nSegCtr = nSegCtr + 1

	STORE "B2A"+cfd+"00"+csegd TO cString
	DO cstringbreak
	nSegCtr = nSegCtr + 1

	STORE "L11"+cfd+LEFT(ALLTRIM(lcLocID),2)+cfd+"19"+csegd TO cString
	DO cstringbreak
	nSegCtr = nSegCtr + 1

	STORE "L11"+cfd+cWO_Num+cfd+"CN"+csegd TO cString
	DO cstringbreak
	nSegCtr = nSegCtr + 1

	STORE "L11"+cfd+cWO_Num+cfd+"OU"+csegd TO cString
	DO cstringbreak
	nSegCtr = nSegCtr + 1

	STORE "L11"+cfd+"06C0084A"+cfd+"TT"+csegd TO cString
	DO cstringbreak
	nSegCtr = nSegCtr + 1

	STORE "L11"+cfd+"06C0084A"+cfd+"4F"+csegd TO cString
	DO cstringbreak
	nSegCtr = nSegCtr + 1

	STORE "N1"+cfd+"ST"+cfd+ALLTRIM(whichShipTo)+cfd+"92"+cfd+ALLTRIM(lcLocID)+csegd TO cString
	DO cstringbreak
	nSegCtr = nSegCtr + 1

	STORE "N1"+cfd+"CS"+cfd+"TOLL GROUP"+cfd+"92"+cfd+"06C0084A"+csegd TO cString
	DO cstringbreak
	nSegCtr = nSegCtr + 1

	STORE "AT7"+cfd+"P1"+cfd+"NS"+cfd+cfd+cfd+cDate+csegd TO cString
	DO cstringbreak
	nSegCtr = nSegCtr + 1

	STORE "MS2"+cfd+cSCAC+cfd+cTrailer+csegd TO cString
	DO cstringbreak
	nSegCtr = nSegCtr + 1


*!*		Detail Information Loop

	IF lTesting
		SET STEP ON
	ENDIF

	SELECT manifest
	SCAN FOR manifest.wo_num = nWO_Num
		cPONum =  STRTRAN(ALLTRIM(manifest.po_num),".","")
		cContainer = ALLTRIM(manifest.CONTAINER)

		IF EMPTY(ALLTRIM(cContainer))
			IF manifest.loose
				cContainer = ALLTRIM(manifest.brokref)
			ELSE
				cErrMsg212 = "NO CONTAINER NUMBER"
				DO ediupdate WITH cErrMsg212,.T.
				THROW
			ENDIF
		ENDIF

		cQty = ALLTRIM(STR(manifest.rcv_qty))
		cWeight = ALLTRIM(STR(manifest.weight))
*		cDel_Date =  DTOS(manifest.delivered)
		cDel_Date =  DTOS(DATE())

		STORE "LX"+cfd+cLXNum+csegd TO cString
		nLXNum = nLXNum+1
		cLXNum = ALLTRIM(STR(nLXNum))
		DO cstringbreak
		nSegCtr = nSegCtr + 1

		STORE "L11"+cfd+"Y"+cfd+"6U"+csegd TO cString
		DO cstringbreak
		nSegCtr = nSegCtr + 1

		STORE "L11"+cfd+cContainer+cfd+"OC"+csegd TO cString
		DO cstringbreak
		nSegCtr = nSegCtr + 1

		STORE "G62"+cfd+"85"+cfd+cDel_Date+csegd TO cString
		DO cstringbreak
		nSegCtr = nSegCtr + 1

		STORE "G62"+cfd+"05"+cfd+cDel_Date+csegd TO cString
		DO cstringbreak
		nSegCtr = nSegCtr + 1

		STORE "SPO"+cfd+cPONum+cfd++cfd+"CT"+cfd+cQty+cfd+"L"+cfd+cWeight+cfd+cfd+"93"+csegd TO cString
		DO cstringbreak
		nSegCtr = nSegCtr + 1

		STORE "N1"+cfd+"VN"+cfd+"HOME GOODS VENDOR"+csegd TO cString
		DO cstringbreak
		nSegCtr = nSegCtr + 1

	ENDSCAN

	close212()
	lClosedFile=FCLOSE(nHandle)

	IF lClosedFile AND lTesting
		WAIT WINDOW "TJM 212 File closed successfully" TIMEOUT 1
*		MODIFY FILE &cFilename
		RELEASE nHandle
	ENDIF

	DO ediupdate WITH "212 CREATED",.F.

	IF lTJFilesOut
		COPY FILE [&cFilename] TO [&cFilename3]
		COPY FILE [&cFilename] TO [&cFilename2]
		DELETE FILE [&cFilename]
	ENDIF

	IF !lTesting
		IF !USED("ftpedilog")
			SELECT 0
			USE F:\edirouting\ftpedilog ALIAS ftpedilog
			INSERT INTO ftpedilog (TRANSFER,ftpdate,filename,acct_name,TYPE) ;
				VALUES ("212-"+cCustName,dt2,cFilename,UPPER(cCustName),"212")
			USE IN ftpedilog
		ENDIF
	ENDIF

*!* Finish processing
	IF lTesting OR lTestfile
		tsubject = "TJMAX 212 *TEST* EDI from TGF as of "+dtmail
	ELSE
		tsubject = "TJMAX 212 EDI from TGF as of "+dtmail
	ENDIF

	tmessage = cMailName+" 212 EDI FILE (TGF manifest WO# "+cWO_Num+")"
	tmessage = tmessage+CHR(13)+"has been created."
	IF lTesting OR lTestfile
		tmessage = tmessage + CHR(13)+CHR(13)+"This is a TEST RUN..."
	ENDIF

	IF lEmail
		DO FORM dartmail2 WITH tsendto,tfrom,tsubject,tcc,tattach,tmessage,"A"
	ENDIF
	RELEASE ALL LIKE c_CntrlNum,c_GrpCntrlNum
	RELEASE ALL LIKE nOrigSeq,nOrigGrpSeq
	WAIT CLEAR
	WAIT WINDOW cMailName+" 212 EDI File output complete" TIMEOUT 2
	closefiles()

CATCH TO oErr
	IF lDoCatch
		ASSERT .F. MESSAGE "In CATCH...debug"
		SET STEP ON
		lEmail = .T.
		DO ediupdate WITH "TRY/CATCH ERROR",.T.
		tsubject = cMailName+" 212 Error ("+TRANSFORM(oErr.ERRORNO)+") at "+TTOC(DATETIME())
		tattach  = ""
		tmessage = cCustName+" Error processing "+CHR(13)
		tmessage = tmessage+TRIM(PROGRAM())+CHR(13)

		tmessage =tmessage+ "Try/CATCH Exeception Message:"+CHR(13)+;
			[  Error: ] + STR(oErr.ERRORNO) +CHR(13)+;
			[  LineNo: ] + STR(oErr.LINENO) +CHR(13)+;
			[  Message: ] + oErr.MESSAGE +CHR(13)+;
			[  Procedure: ] + oErr.PROCEDURE +CHR(13)+;
			[  Details: ] + oErr.DETAILS +CHR(13)+;
			[  StackLevel: ] + STR(oErr.STACKLEVEL) +CHR(13)+;
			[  LineContents: ] + oErr.LINECONTENTS+CHR(13)+;
			[  UserValue: ] + oErr.USERVALUE

		tsubject = "212 Process Error at "+TTOC(DATETIME())
		tattach  = ""
		tcc=""
		tfrom    ="TGF EDI 212 Process Ops <fmi-warehouse-ops@fmiint.com>"
		DO FORM dartmail2 WITH tsendto,tfrom,tsubject,tcc,tattach,tmessage,"A"

		IF "NO CONTAINER"$cErrMsg212
			tsubject = "212 Process Error at "+TTOC(DATETIME())
			tsendto ="pgaidis@fmiint.com,juan.rocio@tollgroup.com"
			tattach  = ""
			tcc=""
			tmessage = cErrMsg212+CHR(13)+"WO#: "+ALLTRIM(TRANSFORM(nWO_Num))
			tfrom    ="TGF EDI 212 Process Ops <fmi-warehouse-ops@fmiint.com>"
			DO FORM dartmail2 WITH tsendto,tfrom,tsubject,tcc,tattach,tmessage,"A"
		ENDIF

		lEmail = .F.
	ENDIF
FINALLY
	closefiles()
ENDTRY

&& END OF MAIN CODE SECTION

**************************
PROCEDURE ediupdate
**************************
	PARAMETERS cFin_status,lIsError
	IF !lTesting
		lDoCatch = IIF(EMPTY(cFin_status),.T.,.F.)
		SELECT edi_trigger
		LOCATE
		IF lIsError
			REPLACE edi_trigger.processed WITH .T.,edi_trigger.created WITH .F.,;
				edi_trigger.fin_status WITH cFin_status,edi_trigger.errorflag WITH .T.;
				edi_trigger.when_proc WITH DATETIME() ;
				FOR edi_trigger.wo_num = nWO_Num AND edi_trigger.edi_type ="212"
			IF FILE(cFilename)
				FCLOSE(nHandle)
				RELEASE nHandle
				DELETE FILE &cFilename
			ENDIF
			closefiles()
			errormail(cFin_status)
			THROW
		ELSE
			REPLACE edi_trigger.processed WITH .T.,edi_trigger.created WITH .T.,proc214 WITH .T.;
				edi_trigger.when_proc WITH DATETIME(),;
				edi_trigger.fin_status WITH "212 CREATED";
				edi_trigger.errorflag WITH .F.,file214 WITH cFilename ;
				FOR edi_trigger.wo_num = nWO_Num AND edi_trigger.edi_type ="212"
		ENDIF
	ENDIF
ENDPROC

****************************
PROCEDURE errormail
****************************
	PARAMETERS cFin_status
	lDoCatch = .F.

	ASSERT .F. MESSAGE "At Errormail...Debug"
	tsubject = cCustName+" 212 EDI File Error"
	tmessage = "212 EDI File Error, WO# "+cWO_Num+CHR(13)
	tmessage = tmessage+CHR(13)+CHR(13)+"Error: "+cFin_status

	IF lTesting OR lTestfile
		tmessage = tmessage + CHR(10)+CHR(10)+"This is TEST run..."
	ENDIF
	IF lEmail
		DO FORM dartmail2 WITH tsendto,tfrom,tsubject,tcc,tattach,tmessage,"A"
	ENDIF
ENDPROC

*********************
PROCEDURE closefiles
*********************
	IF USED('ftpedilog')
		USE IN ftpedilog
	ENDIF
	IF USED('serfile')
		USE IN serfile
	ENDIF
	IF USED('fedcodes')
		USE IN FEDCODES
	ENDIF
	IF USED('account')
		USE IN account
	ENDIF
	IF USED('xref')
		USE IN xref
	ENDIF
ENDPROC

****************************
PROCEDURE num_incr_isa
****************************

	IF !USED("serfile")
		USE ("F:\3pl\data\serial\tjx_serial") IN 0 ALIAS serfile
	ENDIF
	SELECT serfile
	IF lTesting
		nOrigSeq = serfile.seqnum
		nOrigGrpSeq = serfile.grpseqnum
		lISAFlag = .F.
	ENDIF
	nISA_Num = serfile.seqnum
	c_CntrlNum = ALLTRIM(STR(serfile.seqnum))
	REPLACE serfile.seqnum WITH serfile.seqnum + 1 IN serfile
ENDPROC

****************************
PROCEDURE num_incr_st
****************************

	IF !USED("serfile")
		USE ("F:\3pl\data\serial\tjx_serial") IN 0 ALIAS serfile
	ENDIF
	SELECT serfile
	IF lTesting
		nOrigSeq = serfile.seqnum
		nOrigGrpSeq = serfile.grpseqnum
		lSTFlag = .F.
	ENDIF
	c_GrpCntrlNum = ALLTRIM(STR(serfile.grpseqnum))
	REPLACE serfile.grpseqnum WITH serfile.grpseqnum + 1 IN serfile
ENDPROC

****************************
PROCEDURE segmentget
****************************

	PARAMETER thisarray,lcKey,nLength

	FOR i = 1 TO nLength
		IF i > nLength
			EXIT
		ENDIF
		lnEnd= AT("*",thisarray[i])
		IF lnEnd > 0
			lcThisKey =TRIM(SUBSTR(thisarray[i],1,lnEnd-1))
			IF OCCURS(lcKey,lcThisKey)>0
				RETURN SUBSTR(thisarray[i],lnEnd+1)
				i = 1
			ENDIF
		ENDIF
	ENDFOR

	RETURN ""

****************************
PROCEDURE close212
****************************
	STORE  "SE"+cfd+ALLTRIM(STR(nSegCtr+1))+cfd+PADL(c_GrpCntrlNum,9,"0")+csegd TO cString
	DO cstringbreak

	STORE  "GE"+cfd+ALLTRIM(STR(nSTCount))+cfd+c_CntrlNum+csegd TO cString
	DO cstringbreak

	STORE  "IEA"+cfd+"1"+cfd+PADL(c_CntrlNum,9,"0")+csegd TO cString
	DO cstringbreak

ENDPROC


****************************
PROCEDURE cstringbreak
****************************
	cLen = LEN(ALLTRIM(cString))
	=FPUTS(nHandle,cString)
ENDPROC
