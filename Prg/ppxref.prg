lparameters xaccountid, xunits, xstyle, xcolor, xid, xpack

if used("xppxrefrpt")	&& dy 2/2/17
	use in xppxrefrpt
endif

xsqlexec("select indetid, accountid, units, style, color, id, pack, totqty, remainqty, wo_num, po, cast(' ' as char(7)) as whseloc from indet where .f.","xppxrefrpt",,"wh")

if !xunits
	xsqlexec("select * from indet where accountid="+transform(xaccountid)+" and units="+transform(xunits)+" and style='"+xstyle+"' and color='"+xcolor+"' and id='"+xid+"' and pack='"+xpack+"'","xindet",,"wh")
	index on indetid tag indetid
	
	scan
		xtindetid=indetid
		xtpack=pack
		xttotqty=totqty
		scatter memvar fields accountid, units, style, color, id, pack, wo_num, po
		
		xsqlexec("select * from inloc where indetid="+transform(xindet.indetid),"xinloc",,"wh")
		scan 
			scatter memvar fields remainqty, whseloc, indetid
			m.totqty=locqty
			insert into xppxrefrpt from memvar
		endscan
		
		xsqlexec("select * from indet where wo_num="+transform(m.wo_num)+" and po='"+m.po+"' and units=.t.","xindet2",,"wh")
		scan
			scatter memvar
			m.whseloc=""
			insert into xppxrefrpt from memvar
		endscan

		=seek(xtindetid,"xindet","indetid")
	endscan

else
	xsqlexec("select * from indet where accountid="+transform(xaccountid)+" and units=.t. and style='"+xstyle+"' and color='"+xcolor+"' and id='"+xid+"' and pack='"+xpack+"'","xindet",,"wh")
	index on indetid tag indetid

	scan
		xunitsindetid=indetid
		xpo=po
		xwo_num=wo_num

		xsqlexec("select * from indet where wo_num="+transform(xwo_num)+" and po='"+xpo+"' and units=.f.","xindet2",,"wh")
		scan
			xindetid=indetid
			scatter memvar fields accountid, units, style, color, id, pack, wo_num, po

			xsqlexec("select * from inloc where indetid="+transform(xindet2.indetid),"xinloc",,"wh")
			scan 
				scatter memvar fields remainqty, whseloc, indetid
				m.totqty=locqty
				insert into xppxrefrpt from memvar
			endscan
			
			select xindet
			scan for wo_num=m.wo_num and po=m.po and units
				scatter memvar
				m.whseloc=""
				insert into xppxrefrpt from memvar
			endscan
			
			=seek(xindetid,"xindet","indetid")
		endscan
		=seek(xunitsindetid,"xindet","indetid")
	endscan
endif

select xppxrefrpt
