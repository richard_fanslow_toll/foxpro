*******************************************************************************************************
* ZAP ML Auto Billing Data -- BE CAREFUL NOT TO DO THIS ON PRODUCTION!
*******************************************************************************************************
LOCAL loMLAUTOBILLINGProc, lcClient

SET PROCEDURE TO M:\DEV\PRG\MLAUTOBILLINGCLASS

loMLAUTOBILLINGProc = CREATEOBJECT('MLAUTOBILLING')

*RETURN

* specify client name
*!*	lcClient = "XYZ" && THIS IS A BAD CLIENT NAME  FOR TESTING
*!*	lcClient = "DSG" && PRODUCTION!
*!*	lcClient = "SEARS"  && PRODUCTION!
lcClient = "DSG-LOCALDATA"
*!*	lcClient = "SEARS-LOCALDATA"

IF NOT loMLAUTOBILLINGProc.SetClient( lcClient ) THEN
	=MESSAGEBOX('Error Setting Client',0+16,'ML Auto Billing Load Data')
ENDIF

* for testing
loMLAUTOBILLINGProc.ShowDataFiles()
loMLAUTOBILLINGProc.ShowEMailBodyText()

loMLAUTOBILLINGProc.PackData()

RETURN
