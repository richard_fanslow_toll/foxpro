PARAMETER lTestIndicator
CLOSE DATA ALL
PUBLIC lTesting,len1

*utilsetup('g3_945-997_ack')
_SCREEN.CAPTION = "Billabong 945-997 Ack. Checker"
_setvars(.t.)

*lTesting = lTestIndicator
lTesting = .t.
lTestmail = .t.

cInfolder = "F:\FTPUSERS\BILLABONG\997IN\"
cArchiveFolder = "F:\FTPUSERS\BILLABONG\997IN\ARCHIVE\"

USE F:\3pl\DATA\edi_trigger IN 0 ALIAS edi_trigger

DO m:\dev\prg\lookups
dDaysback = 10

CD &cInfolder
lenary1 = ADIR(ary1,"*.*")

lDo997Loops = .T.
IF lenary1 = 0
	WAIT WINDOW "No 997 files to process...skipping to reports" TIMEOUT 1
	lDo997Loops = .F.
ELSE
	WAIT WINDOW "Will now scan through "+TRANSFORM(lenary1)+" 997 files" TIMEOUT 2
ENDIF

nFound = 0
nNotFound = 0

IF lDo997Loops
	FOR runthis = 1 TO lenary1
		cFilename = ALLTRIM(ary1[runthis,1])
		WAIT WINDOW "Processing file "+cFilename TIMEOUT 1
		xfile = (cInfolder+cFilename)
		cArchivefile = (cArchiveFolder+cFilename)

		dDate = ary1[runthis,3]
		IF dDate < DATE()-dDaysback
			WAIT WINDOW "File "+cFilename+" too old...looping" NOWAIT
			COPY FILE [&xfile] TO [&cArchiveFile]
			DELETE FILE [&xfile]
			LOOP
		ENDIF

		lcStr = FILETOSTR(xfile)
		cDelimiter = "|" 
		RELEASE lcStr
		DO m:\dev\prg\createx856a
		DO m:\dev\prg\loadedifile WITH xfile,cDelimiter,"PIPE2","BILLABONG"

		SELECT x856
		COUNT TO n FOR x856.segment = "AK1"
		? "AK1 recs: "+TRANSFORM(n)
		LOCATE

*		SET STEP ON 
		SCAN FOR x856.segment = "AK1"
			nRec1 = RECNO()
			cISA_Num = PADL(ALLTRIM(x856.f2),9,"0")
			WAIT WINDOW "Checking ISA# "+cISA_Num NOWAIT noclear
			SELECT edi_trigger
			LOCATE 
			LOCATE FOR INLIST(edi_trigger.accountid,&gbcnyrecaccounts) AND edi_trigger.isa_num = cISA_Num
			IF FOUND()
				WAIT WINDOW "ISA# found : "+cISA_Num NOWAIT noclear
				nFound = nFound+1
				REPLACE edi_trigger.ack945 WITH .T. FOR INLIST(edi_trigger.accountid,&gbcnyrecaccounts) AND edi_trigger.isa_num = cISA_Num IN edi_trigger
			ELSE
				WAIT WINDOW "ISA# NOT found in Triggers: "+cISA_Num NOWAIT noclear
				nNotFound = nNotFound+1
			ENDIF
			SELECT x856
			GO nRec1
		ENDSCAN
		
		COPY FILE [&xfile] TO [&cArchiveFile]
		IF !lTesting
			DELETE FILE [&xfile]
		ENDIF

		USE IN x856
		WAIT CLEAR
	ENDFOR

	WAIT CLEAR

	?"ISAs found: "+TRANSFORM(nFound)
	?"ISAs NOT found: "+TRANSFORM(nNotFound)
ENDIF

IF lTesting
	SET STEP ON
ENDIF

*!* This part of the program sets up and sends the missing  945-997 list to Spring

xsqlexec("select * from account",,,"qq")

SELECT a.accountid,b.acctname,a.isa_num,a.bol,a.ship_ref AS pickticket,a.scac,a.wo_num,a.trig_time AS processed,a.when_proc,a.fin_status ;
	FROM edi_trigger a,account b;
	WHERE INLIST(a.accountid,&gbcnyrecaccounts) ;
	AND a.ack945 # .T. ;
	AND !(" OV"$a.ship_ref) ;
	AND BETWEEN(a.trig_time,DATETIME()-(dDaysback*24*3600), DATETIME()-(6*3600)) ;
	AND a.fin_status # "NO 945" ;
	AND a.accountid = b.accountid ;
	GROUP BY a.accountid,a.bol,a.ship_ref ;
	ORDER BY a.accountid,a.bol,a.ship_ref ;
	INTO CURSOR temp1

SELECT accountid,acctname,isa_num,bol,when_proc FROM temp1 GROUP BY 1,3,4 INTO CURSOR temp1a
USE IN temp1
SELECT temp1a
LOCATE

IF lTesting
	BROWSE
ENDIF

tFrom ="TGF EDI Operations <toll-edi-ops@tollgroup.com>"
SELECT temp1a
locate
IF !EOF()
	cMiss997File = ("h:\fox\bbgmiss_997s_"+TTOC(DATETIME(),1)+".xls")
	COPY TO &cMiss997File TYPE XL5
	LOCATE
	IF lTesting
		BROWSE TIMEOUT 3
	ENDIF
	IF lTesting OR lTestmail
		USE F:\3pl\DATA\mailmaster ALIAS mm
		LOCATE FOR taskname = 'JOETEST'
		tsendto = ALLTRIM(mm.sendto)
		tcc = ALLTRIM(mm.cc)
		USE IN mm
	ELSE
		USE F:\3pl\DATA\mailmaster ALIAS mm
		LOCATE FOR taskname = 'BBG FA MAIL'
		tsendto = ALLTRIM(mm.sendto)
		tcc = ALLTRIM(mm.cc)
		USE IN mm
	ENDIF
	tsubject= "TGF Billabong Missing 997s as of " +TTOC(DATETIME())
	tattach = cMiss997File
	tmessage = "Attached is a list of Billabong 945s/shipments for which no 997 has been received"
	IF lTesting
		tmessage = tmessage+CHR(13)+"THIS IS A TEST MAILING"
	ENDIF
	DO FORM m:\dev\frm\dartmail2 WITH tsendto,tFrom,tsubject,tcc,tattach,tmessage,"A"
	DELETE FILE &cMiss997File
ELSE
	IF lTesting OR lTestmail
		USE F:\3pl\DATA\mailmaster ALIAS mm
		LOCATE FOR taskname = 'JOETEST'
		tsendto = ALLTRIM(mm.sendto)
		tcc = ALLTRIM(mm.cc)
		USE IN mm
	ELSE
		USE F:\3pl\DATA\mailmaster ALIAS mm
		LOCATE FOR taskname = 'BBG FA MAIL'
		tsendto = ALLTRIM(mm.sendto)
		tcc = ALLTRIM(mm.cc)
		USE IN mm
	ENDIF
	tsubject= "TGF Billabong 945-997 status as of " +TTOC(DATETIME())
	tattach = ""
	tmessage = "All Billabong 997s have been received for shipments made over the past week"
	IF lTesting
		tmessage = tmessage+CHR(13)+"THIS IS A TEST MAILING"
	ENDIF
	DO FORM m:\dev\frm\dartmail2 WITH tsendto,tFrom,tsubject,tcc,tattach,tmessage,"A"
ENDIF

CLOSE DATA ALL
*schedupdate()

RETURN
