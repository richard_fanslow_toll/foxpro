*!* m:\dev\prg\bbc940_bkdn.prg

CLEAR
lcheckstyle = .T.
lprepack = .F.  && Inventory to be kept as PnP
m.color=""
cuccnumber = ""
cusepack = ""
lcuccseed = 1

SELECT x856
COUNT FOR TRIM(segment) = "ST" TO nsegcnt
csegcnt = ALLTRIM(STR(nsegcnt))
SELECT x856
LOCATE

ptctr = 0

WAIT WINDOW "Now in "+cmailname+" 940 Breakdown" NOWAIT

WAIT "There are "+csegcnt+" P/Ts in this file" WINDOW NOWAIT
SELECT x856
LOCATE

*!* Constants
m.acct_name = ICASE(nacctnum = 6757,"BBC INT. - ROBERT WAYNE",nacctnum = 6763,"BBC INT - CLUB FOOT",nacctnum = 6759,"BBC INT. - FEIYUE","BBC INTERNATIONAL")

STORE nacctnum TO m.accountid
m.careof = " "
m.sf_addr1 = "1515 N FEDERAL HWY #206"
m.sf_addr2 = ""
m.sf_csz = "BOCA RATON, FL 33432"
m.printed = .F.
m.print_by = ""
m.printdate = DATE()
*!* End constants

SELECT xpt
DELETE ALL
SELECT xptdet
DELETE ALL

STORE 0 TO pt_total_eaches
STORE 0 TO pt_total_cartons
STORE 0 TO nctncount

SELECT x856
SET FILTER TO
LOCATE
CREATE CURSOR curppk (;
	upc CHAR(12),;
	qty N(6),;
	STYLE CHAR(20),;
	COLOR CHAR(10),;
	ID    CHAR(20),;
	musical CHAR(40))

STORE "" TO cisa_num
llhasprepacks = .F.

WAIT WINDOW "NOW IN INITAL HEADER BREAKDOWN" TIMEOUT 1
WAIT WINDOW "NOW IN INITAL HEADER BREAKDOWN" NOWAIT
*ASSERT .F. MESSAGE "At header bkdn...debug"
DO WHILE !EOF("x856")
	csegment = TRIM(x856.segment)
	ccode = TRIM(x856.f1)

	IF TRIM(x856.segment) = "ISA"
		cisa_num = ALLTRIM(x856.f13)
		currentisanum = ALLTRIM(x856.f13)
		WAIT WINDOW "This is a "+cmailname+" PT upload" NOWAIT
*  ptctr = 0
		SELECT x856
		SKIP
		LOOP
	ENDIF

	IF TRIM(x856.segment) = "GS"
		cgs_num = ALLTRIM(x856.f6)
		lccurrentgroupnum = ALLTRIM(x856.f6)
		SELECT x856
		SKIP
		LOOP
	ENDIF

	IF TRIM(x856.segment) = "ST" AND TRIM(x856.f1) = "940"
		IF !ltesting
			SELECT x856
			SKIP
			LOOP
		ENDIF
	ENDIF

	IF TRIM(x856.segment) = "W05"  && new shipment
		nctncount = 0
		m.style = ""
		SELECT xpt
		ndetcnt = 0
		APPEND BLANK

		REPLACE xpt.shipins WITH xpt.shipins+CHR(13)+"FILENAME*"+cfilename IN xpt
		REPLACE xpt.shipins WITH xpt.shipins+CHR(13)+"CONAME*BBC" IN xpt
		REPLACE xpt.qty WITH 0 IN xpt
		REPLACE xpt.origqty WITH 0 IN xpt
		STORE 0 TO pt_total_eaches
		STORE 0 TO pt_total_cartons
		ptctr = ptctr +1
		WAIT WINDOW AT 10,10 "Now processing PT# "+ ALLTRIM(x856.f2)+" Number "+CHR(13)+TRANSFORM(ptctr)+" of "+csegcnt NOWAIT

		REPLACE xpt.ptid       WITH ptctr IN xpt
		REPLACE xpt.accountid  WITH nacctnum IN xpt
		REPLACE xpt.ptdate     WITH dfiledate IN xpt
		m.ship_ref = ALLTRIM(x856.f2)
		cship_ref = ALLTRIM(x856.f2)
		REPLACE xpt.ship_ref   WITH m.ship_ref IN xpt  && Pick Ticket
		m.pt = ALLTRIM(x856.f2)
		REPLACE xpt.cnee_ref   WITH ALLTRIM(x856.f3)  && cust PO
		llhasprepacks = .F.  && always reset this to .f. no prepacks
		SELECT x856
		SKIP
		LOOP
	ENDIF

	IF TRIM(x856.segment) = "N1" AND TRIM(x856.f1) = "ST" && Ship-to data
		cconsignee = UPPER(ALLTRIM(x856.f2))
		IF "~"$cconsignee
*   cConsignee = SUBSTR(cConsignee,AT("~",cConsignee,1)+1)
		ENDIF
		SELECT xpt
		m.st_name = cconsignee
		REPLACE xpt.consignee WITH  cconsignee IN xpt
		REPLACE xpt.NAME WITH  cconsignee IN xpt

		cstorenum = ALLTRIM(x856.f4)
		IF EMPTY(xpt.shipins)
			REPLACE xpt.shipins WITH "STORENUM*"+cstorenum IN xpt
		ELSE
			REPLACE xpt.shipins WITH xpt.shipins+CHR(13)+"STORENUM*"+cstorenum IN xpt
		ENDIF
		IF EMPTY(xpt.dcnum)
			REPLACE xpt.dcnum WITH cstorenum IN xpt
		ENDIF
		IF LEN(cstorenum)>5
			REPLACE xpt.storenum  WITH VAL(RIGHT(cstorenum,5)) IN xpt
		ELSE
			REPLACE xpt.storenum  WITH VAL(cstorenum) IN xpt
		ENDIF

		SKIP 1 IN x856
		DO CASE
			CASE TRIM(x856.segment) = "N2"  && name ext. info
				m.name = UPPER(ALLTRIM(x856.f1))
				REPLACE xpt.NAME WITH m.name IN xpt
				SKIP 1 IN x856
				IF TRIM(x856.segment) = "N3"  && address info
					m.st_addr1 = UPPER(ALLTRIM(x856.f1))
					REPLACE xpt.address  WITH m.st_addr1 IN xpt
					m.st_addr2 = UPPER(ALLTRIM(x856.f2))
					REPLACE xpt.address2 WITH m.st_addr2 IN xpt
				ENDIF
				SKIP 1 IN x856
				IF TRIM(x856.segment) = "N4"  && address info
					m.zipbar = UPPER(ALLTRIM(x856.f3))
					m.st_csz = UPPER(ALLTRIM(x856.f1))+", "+UPPER(ALLTRIM(x856.f2))+" "+TRIM(x856.f3)
					REPLACE xpt.csz WITH m.st_csz IN xpt
					REPLACE xpt.shipins WITH xpt.shipins+CHR(13)+"COUNTRY*"+ALLTRIM(x856.f4) IN xpt
				ENDIF
			CASE TRIM(x856.segment) = "N3"  && address info
				m.st_addr1 = UPPER(ALLTRIM(x856.f1))
				REPLACE xpt.address  WITH m.st_addr1 IN xpt
				m.st_addr2 = UPPER(ALLTRIM(x856.f2))
				REPLACE xpt.address2 WITH m.st_addr2 IN xpt
				SKIP 1 IN x856
				IF TRIM(x856.segment) = "N4"  && address info
					m.zipbar = UPPER(ALLTRIM(x856.f3))
					m.st_csz = UPPER(TRIM(x856.f1))+", "+UPPER(TRIM(x856.f2))+" "+TRIM(x856.f3)
					REPLACE xpt.csz WITH m.st_csz IN xpt
					REPLACE xpt.shipins WITH xpt.shipins+CHR(13)+"COUNTRY*"+ALLTRIM(x856.f4) IN xpt
					REPLACE xpt.country WITH ALLTRIM(x856.f4) IN xpt
				ENDIF
			OTHERWISE
				WAIT WINDOW "UNKNOWN ADDRESS SEGMENT" TIMEOUT 2
				THROW
		ENDCASE
		SELECT x856
		SKIP
		LOOP
	ENDIF

	IF TRIM(x856.segment) = "N1" AND INLIST(TRIM(x856.f1),"Z7","SD","MA","PF") && Returned data
		cnom = ALLTRIM(x856.f1)
		REPLACE xpt.shipfor  WITH  ALLTRIM(x856.f2) IN xpt
		REPLACE xpt.sforstore WITH ALLTRIM(x856.f4) IN xpt
		REPLACE xpt.storenum  WITH VAL(cstorenum) IN xpt
		REPLACE xpt.shipins WITH xpt.shipins+CHR(13)+cnom+"_N1*"+ALLTRIM(x856.f2) IN xpt
		SKIP 1 IN x856

		DO CASE
			CASE TRIM(x856.segment) = "N2"
				REPLACE xpt.sforaddr1 WITH ALLTRIM(x856.f1) IN xpt
				REPLACE xpt.shipins WITH xpt.shipins+CHR(13)+cnom+"_N2*"+ALLTRIM(x856.f1) IN xpt
				SKIP 1 IN x856
				IF TRIM(x856.segment) = "N3"
					REPLACE xpt.shipfor  WITH  ALLTRIM(x856.f1) IN xpt
					REPLACE xpt.sforaddr1 WITH ALLTRIM(x856.f1) IN xpt
					REPLACE xpt.sforaddr2 WITH ALLTRIM(x856.f2) IN xpt

					REPLACE xpt.shipins WITH xpt.shipins+CHR(13)+cnom+"_N3*"+ALLTRIM(x856.f1)+"|"+ALLTRIM(x856.f2) IN xpt
					SKIP 1 IN x856
				ENDIF
				IF TRIM(x856.segment) = "N4"
					REPLACE xpt.shipins WITH xpt.shipins+CHR(13)+cnom+"_N4*"+ALLTRIM(x856.f1)+"|"+ALLTRIM(x856.f2)+"|"+ALLTRIM(x856.f3) IN xpt
					REPLACE xpt.sforcsz WITH ALLTRIM(x856.f1)+", "+ALLTRIM(x856.f2)+" "+ALLTRIM(x856.f3) IN xpt
				ENDIF

			CASE TRIM(x856.segment) = "N3"
				REPLACE xpt.shipfor  WITH  ALLTRIM(x856.f1) IN xpt
				REPLACE xpt.sforaddr1 WITH ALLTRIM(x856.f1) IN xpt
				REPLACE xpt.sforaddr2 WITH ALLTRIM(x856.f2) IN xpt
				REPLACE xpt.shipins WITH xpt.shipins+CHR(13)+cnom+"_N3*"+ALLTRIM(x856.f1)+"|"+ALLTRIM(x856.f2) IN xpt
				SKIP 1 IN x856
				IF TRIM(x856.segment) = "N4"
					REPLACE xpt.shipins WITH xpt.shipins+CHR(13)+cnom+"_N4*"+ALLTRIM(x856.f1)+"|"+ALLTRIM(x856.f2)+"|"+ALLTRIM(x856.f3) IN xpt
					REPLACE xpt.sforcsz WITH ALLTRIM(x856.f1)+", "+ALLTRIM(x856.f2)+" "+ALLTRIM(x856.f3) IN xpt
				ENDIF

			CASE TRIM(x856.segment) = "N4"
				REPLACE xpt.shipins WITH xpt.shipins+CHR(13)+cnom+"_N4*"+ALLTRIM(x856.f1)+"|"+ALLTRIM(x856.f2)+"|"+ALLTRIM(x856.f3) IN xpt
				REPLACE xpt.sforcsz WITH ALLTRIM(x856.f1)+", "+ALLTRIM(x856.f2)+" "+ALLTRIM(x856.f3) IN xpt
		ENDCASE

		SELECT x856
		SKIP
		LOOP
	ENDIF

	IF TRIM(x856.segment) = "N9"  && Additional info segments
		DO CASE

      CASE ALLTRIM(x856.f1) = "PH"  && ofr joutneys pack list
        REPLACE xpt.shipins WITH xpt.shipins+CHR(13)+"PHONE*"+ALLTRIM(x856.f2) IN xpt

			CASE ALLTRIM(x856.f1) = "CR"  && ofr joutneys pack list
				REPLACE xpt.shipins WITH xpt.shipins+CHR(13)+"JOURNEYREF*"+ALLTRIM(x856.f2) IN xpt

			CASE ALLTRIM(x856.f1) = "DP"  && Department
				REPLACE xpt.dept WITH ALLTRIM(x856.f2) IN xpt

			CASE ALLTRIM(x856.f1) = "CO"  && Customer Order number
				REPLACE xpt.shipins WITH xpt.shipins+CHR(13)+"CUSTORDER*"+ALLTRIM(x856.f2) IN xpt

      CASE ALLTRIM(x856.f1) = "CP"  && Belk Customer Order number
        REPLACE xpt.shipins WITH xpt.shipins+CHR(13)+"BELKORDER*"+ALLTRIM(x856.f2) IN xpt

			CASE ALLTRIM(x856.f1) = "98" && Container/Packaging Specification Number
*REPLACE xpt.shipins WITH xpt.shipins+CHR(13)+"PRIORITY*"+ALLTRIM(x856.f2) IN xpt

			CASE ALLTRIM(x856.f1) = "12"  && Billing account
				REPLACE xpt.shipins WITH xpt.shipins+CHR(13)+"CUSTACCT*"+ALLTRIM(x856.f2) IN xpt  && use to be the cacctnum
				REPLACE xpt.upsacctnum WITH ALLTRIM(x856.f2) IN xpt

			CASE ALLTRIM(x856.f1) = "DU"  && Billing account
				REPLACE xpt.duns WITH ALLTRIM(x856.f2) IN xpt

			CASE ALLTRIM(x856.f1) = "15"  && used for label template selection
				REPLACE xpt.cacctnum WITH ALLTRIM(x856.f2) IN xpt
				REPLACE xpt.shipins WITH xpt.shipins+CHR(13)+"LABEL*"+ALLTRIM(x856.f2) IN xpt

			CASE ALLTRIM(x856.f1) = "14"  && Master Account Number
				REPLACE xpt.shipins WITH xpt.shipins+CHR(13)+"MASTERACCT*"+ALLTRIM(x856.f2) IN xpt

			CASE ALLTRIM(x856.f1) = "OP"  && Order priority/type
				REPLACE xpt.batch_num WITH ALLTRIM(x856.f2) IN xpt
				REPLACE xpt.shipins WITH xpt.shipins+CHR(13)+"BATCHNUM*"+ALLTRIM(x856.f2) IN xpt
*      Replace xpt.consignee With  Alltrim(x856.f2)+"~"+Alltrim(xpt.consignee) In xpt  && removed dy 9/22/17

			CASE ALLTRIM(x856.f1) = "ST"  && Original Purchase Order
				REPLACE xpt.shipfor WITH ALLTRIM(x856.f2) IN xpt
				REPLACE xpt.shipins WITH xpt.shipins+CHR(13)+"STORENAME*"+ALLTRIM(x856.f2) IN xpt

		ENDCASE
	ENDIF

	IF TRIM(x856.segment) = "G62" AND TRIM(x856.f1) == "37"  && start date
		lcdate = ALLTRIM(x856.f2)
		lxdate = SUBSTR(lcdate,5,2)+"/"+SUBSTR(lcdate,7,2)+"/"+LEFT(lcdate,4)
		lddate = CTOD(lxdate)
		STORE lddate TO dstart
		REPLACE xpt.START WITH lddate IN xpt
		SELECT x856
		SKIP
		LOOP
	ENDIF

	IF TRIM(x856.segment) = "G62" AND TRIM(x856.f1) == "38"  && cancel date
		lcdate = ALLTRIM(x856.f2)
		lxdate = SUBSTR(lcdate,5,2)+"/"+SUBSTR(lcdate,7,2)+"/"+LEFT(lcdate,4)
		lddate = CTOD(lxdate)
		STORE lddate TO dcancel
		REPLACE xpt.CANCEL WITH lddate IN xpt
		SELECT x856
		SKIP
		LOOP
	ENDIF

	llprepackflag = .F.

	IF TRIM(x856.segment) = "W66"
		IF ALLTRIM(x856.f1) = "PP"
			llprepackflag = .T.
		ENDIF
		REPLACE carrcode WITH ALLTRIM(x856.f5) IN xpt
		lcshipcode =ALLTRIM(x856.f10)

		IF xsqlexec("select * from altscac where accountid=6757 and shipmode='"+ALLTRIM(lcshipcode)+"'",,,"wh") # 0
			REPLACE carrcode WITH ALLTRIM(altscac.shipmode) IN xpt
			REPLACE ship_via WITH ALLTRIM(altscac.shipname) IN xpt
			REPLACE scac     WITH ALLTRIM(altscac.scaccode) IN xpt
		ELSE
			REPLACE carrcode WITH "UNK" IN xpt
			REPLACE ship_via WITH "UNKNOWN" IN xpt
			REPLACE scac     WITH "" IN xpt
		ENDIF

		SELECT x856
		SKIP
		LOOP
	ENDIF


	IF TRIM(x856.segment) = "LX" && start of PT detail, stay here until this PT is complete
		lnLXCtr = VAL(TRIM(x856.f1))
		ASSERT .F. MESSAGE cship_ref+" Detail, Type = "+IIF(lprepack,"Prepack","Pickpack")+"...debug"
		m.units = IIF(lprepack,.F.,.T.)
		llprepackflag = .F.

		WAIT "NOW IN DETAIL LOOP PROCESSING" WINDOW NOWAIT

		SELECT xptdet
		CALCULATE MAX(ptdetid) TO m.ptdetid
		m.ptdetid = m.ptdetid + 1

		SELECT xptdet
		SKIP 1 IN x856
		lnlinenum=1
		DO WHILE ALLTRIM(x856.segment) != "SE"
			IF TRIM(x856.segment) = "LX"  && new detail loop, decide whether its a prepack, full case or open stock and process
				lnLXCtr = VAL(TRIM(x856.f1))
			ENDIF

			csegment = TRIM(x856.segment)
			ccode = TRIM(x856.f1)

			IF TRIM(x856.segment) = "W01" && Destination Quantity
				lcitemtype = ALLTRIM(x856.f2)  && either CA or EA  CA full case or prepack
				IF lcitemtype = "CA"
					m.units = .F.
					llhasprepacks = .T.
					lnprepackqty = VAL(TRIM(x856.f1))
					lcmasterupc  = ALLTRIM(x856.f7)
					lcvendorsku  = ALLTRIM(x856.f16)
					SELECT curppk
					ZAP
					SELECT x856
				ELSE
					m.units = .T.
				ENDIF
				lncureentrec = RECNO("x856")
				SKIP 2 IN x856
				IF TRIM(x856.segment) = "N9" AND TRIM(x856.f1) = "MR" && if this, then this is a prepack
					llprepackflag = .T.
					SKIP -1 IN x856
					cprtstr = "DESC*"+UPPER(TRIM(x856.f1))
					REPLACE xptdet.printstuff WITH xptdet.printstuff+CHR(13)+cprtstr IN xptdet
					SKIP 1 IN x856
					DO WHILE TRIM(x856.segment) = "N9" AND TRIM(x856.f1) = "MR"
						lcquery = [select * from upcmast where upc = ']+TRIM(x856.f2)+[']
						xsqlexec(lcquery,"xupcmast",,"wh")
						SELECT xupcmast
						IF RECCOUNT("xupcmast") > 0
							lcstyle = xupcmast.STYLE
							lcsize  = xupcmast.ID
							lccolor = xupcmast.COLOR
						ELSE

						ENDIF
						INSERT INTO curppk (upc,STYLE,COLOR,ID,qty) VALUES (TRIM(x856.f2),lcstyle,lccolor,lcsize,VAL(TRIM(x856.f3)))
						SKIP 1 IN x856
					ENDDO

&& let roll out the detail lines
					lcprepackstr  = "PREPACKUPC*"+ALLTRIM(lcmasterupc)+CHR(13)
					lcprepackstr  = lcprepackstr+"PREPACKLINE*"+TRANSFORM(lnLXCtr)+CHR(13)
					lcprepackstr  = lcprepackstr+"PREPACKSKU*"+ALLTRIM(lcvendorsku)+CHR(13)
					lcprepackstr  = lcprepackstr+"PREPACKQTY*"+TRANSFORM(lnprepackqty)+CHR(13)
					SELECT curppk
					GO TOP
					SCAN
						lcprepackstr = lcprepackstr + ALLTRIM(TRANSFORM(curppk.qty))+" "+ALLTRIM(curppk.upc)+"-"+"Style:"+ ALLTRIM(curppk.STYLE)+"  Color: "+ALLTRIM(curppk.COLOR)+" Size: "+ ALLTRIM(curppk.ID)+"  Qty: "+TRANSFORM(curppk.qty)+CHR(13)
					ENDSCAN

					SELECT curppk
					GO TOP
					SCAN
						REPLACE curppk.qty WITH curppk.qty * lnprepackqty
					ENDSCAN

					SELECT curppk
					LOCATE
					IF ltesting
						BROWSE
					ENDIF
					SCAN
						SELECT xptdet
						APPEND BLANK
						REPLACE xptdet.ptid     WITH xpt.ptid  IN xptdet
						REPLACE xptdet.ptdetid  WITH m.ptdetid IN xptdet
						REPLACE xptdet.units    WITH .T.       IN xptdet
						REPLACE xptdet.linenum  WITH ALLTRIM(TRANSFORM(lnLXCtr)) IN xptdet
						REPLACE xptdet.ship_ref WITH xpt.ship_ref  IN xptdet
						m.ptdetid = m.ptdetid + 1
						REPLACE xptdet.accountid WITH xpt.accountid IN xptdet
						REPLACE xptdet.PACK  WITH "1" IN xptdet
						REPLACE xptdet.STYLE WITH curppk.STYLE IN xptdet
						REPLACE xptdet.COLOR WITH curppk.COLOR IN xptdet
						REPLACE xptdet.ID    WITH curppk.ID    IN xptdet
						REPLACE xptdet.upc   WITH curppk.upc   IN xptdet
						REPLACE xptdet.size_scale WITH "1"     IN xptdet  && this used later to ensure that prepack detail lines are first during rollup

						REPLACE xptdet.totqty  WITH curppk.qty IN xptdet
						REPLACE xpt.qty        WITH xpt.qty + xptdet.totqty IN xpt
						REPLACE xpt.origqty    WITH xpt.qty IN xpt
						REPLACE xptdet.origqty WITH xptdet.totqty IN xptdet

						REPLACE xptdet.custsku    WITH lcvendorsku  IN xptdet
						REPLACE xptdet.shipstyle  WITH lcvendorsku  IN xptdet
						REPLACE xptdet.printstuff WITH lcprepackstr IN xptdet
*            Replace xptdet.printstuff With xptdet.printstuff+Chr(13)+"MASTERUPC*"+lcMasterUPC In xptdet
*            Replace xptdet.printstuff With xptdet.printstuff+Chr(13)+"VENDORSKU*"+lcVendorSKU In xptdet
*            Replace xptdet.printstuff With xptdet.printstuff+Chr(13)+"PREPACK*"+lcVendorSKU   In xptdet

					ENDSCAN
					REPLACE shipins WITH shipins+CHR(13)+ lcprepackstr+"ENDPREPACK"+CHR(13) IN xpt
					SELECT x856
					SKIP -1 IN x856
					lcmiscstr =""
					DO WHILE TRIM(x856.segment) != "W20"
						IF TRIM(x856.segment) = "N9" AND TRIM(x856.f1) = "VSZ"
							lcmiscstr = lcmiscstr+CHR(13)+"PREPACKSIZE*"+TRIM(x856.f2)
						ENDIF
						IF TRIM(x856.segment) = "N9" AND TRIM(x856.f1) = "VCL"
							lcmiscstr=lcmiscstr+CHR(13)+"PREPACKCOLOR*"+TRIM(x856.f2)
						ENDIF
						IF TRIM(x856.segment) = "N9" AND TRIM(x856.f1) = "CD"
							lcmiscstr=lcmiscstr+CHR(13)+"PREPACKCD*"+TRIM(x856.f2)
						ENDIF
						SKIP 1 IN x856
					ENDDO
					SELECT xptdet
					SCAN FOR ALLTRIM(xptdet.linenum) = ALLTRIM(STR(lnLXCtr))
						REPLACE xptdet.printstuff WITH xptdet.printstuff+lcmiscstr IN xptdet
					ENDSCAN
				ELSE
					SKIP -2 IN x856
				ENDIF

				IF llprepackflag = .F.
					SELECT xptdet
					APPEND BLANK

					REPLACE xptdet.ptid    WITH xpt.ptid IN xptdet
					REPLACE xptdet.ptdetid WITH m.ptdetid IN xptdet
					REPLACE xptdet.units   WITH m.units IN xptdet
					REPLACE xptdet.linenum WITH ALLTRIM(STR(lnLXCtr)) IN xptdet
					m.ptdetid = m.ptdetid + 1
					REPLACE xptdet.accountid WITH xpt.accountid IN xptdet
					cusepack = "1"
					REPLACE xptdet.size_scale WITH "2"     IN xptdet  && this used later to ensure that prepack detail lines are first during rollup
					REPLACE xptdet.PACK WITH cusepack IN xptdet
					REPLACE xptdet.ship_ref WITH xpt.ship_ref  IN xptdet
					m.casepack = INT(VAL(cusepack))
					REPLACE xptdet.casepack WITH m.casepack IN xptdet

					REPLACE xptdet.totqty WITH INT(VAL(x856.f1)) IN xptdet
					REPLACE xpt.qty WITH xpt.qty + xptdet.totqty IN xpt
					REPLACE xpt.origqty WITH xpt.qty IN xpt
					REPLACE xptdet.origqty WITH xptdet.totqty IN xptdet

					IF EMPTY(TRIM(xptdet.printstuff))
						REPLACE xptdet.printstuff WITH "UOM*"+ALLTRIM(x856.f2) IN xptdet
					ELSE
						REPLACE xptdet.printstuff WITH xptdet.printstuff+CHR(13)+"UOM*"+ALLTRIM(x856.f2) IN xptdet
					ENDIF

					IF ALLTRIM(x856.f4) = "IN"
						m.style = ALLTRIM(x856.f5)
						REPLACE xptdet.STYLE WITH ALLTRIM(x856.f5) IN xptdet
					ENDIF

					IF ALLTRIM(x856.f6) = "UP"
						m.upc = ALLTRIM(x856.f7)
						thisupc = ALLTRIM(x856.f7)
						REPLACE xptdet.upc WITH ALLTRIM(x856.f7) IN xptdet
					ENDIF

					IF ALLTRIM(x856.f15) = "VN"
						m.shipstyle = ALLTRIM(x856.f16)
						m.custsku   = ALLTRIM(x856.f16)
						REPLACE xptdet.shipstyle WITH ALLTRIM(x856.f16) IN xptdet
						REPLACE xptdet.custsku WITH ALLTRIM(x856.f16) IN xptdet
					ENDIF
				ENDIF

			ENDIF

			IF TRIM(x856.segment) = "G69" && desc
				cprtstr = "DESC*"+UPPER(TRIM(x856.f1))
				REPLACE xptdet.printstuff WITH xptdet.printstuff+CHR(13)+cprtstr IN xptdet
			ENDIF


			IF TRIM(x856.segment) = "N9"  AND ALLTRIM(x856.f1) = "VSZ"
				m.id = ALLTRIM(x856.f2)
				REPLACE xptdet.ID WITH ALLTRIM(x856.f2) IN xptdet
			ENDIF

			IF TRIM(x856.segment) = "N9"  AND ALLTRIM(x856.f1) = "VCL"
				m.color = ALLTRIM(x856.f2)
				REPLACE xptdet.COLOR WITH ALLTRIM(x856.f2) IN xptdet
			ENDIF

			IF TRIM(x856.segment) = "W20"
*set step On
				REPLACE xptdet.printstuff WITH xptdet.printstuff+CHR(13)+"OPENUPC*"+ALLTRIM(xptdet.upc)+"*"+ALLTRIM(TRANSFORM(xptdet.linenum))+"*"+ALLTRIM(TRANSFORM(xptdet.totqty)) IN xptdet
				IF lcitemtype = "CA"
					REPLACE xptdet.PACK WITH ALLTRIM(x856.f1) IN xptdet
					REPLACE xptdet.casepack WITH VAL(ALLTRIM(x856.f1)) IN xptdet
				ENDIF
				IF llprepackflag = .F.
					REPLACE xpt.shipins WITH xpt.shipins+CHR(13)+"Next Line is UPC Pack Qty"  IN xpt
					REPLACE xpt.shipins WITH xpt.shipins+CHR(13)+ALLTRIM(thisupc)+"*"+ALLTRIM(x856.f1) IN xpt
				ENDIF
			ENDIF

			SELECT x856
			SKIP 1 IN x856
			llprepackflag = .F.
		ENDDO

		SELECT xptdet
	ENDIF

	IF llhasprepacks = .T.
		REPLACE xpt.batch_num WITH "PREPACK" IN xpt
	ENDIF

	SELECT x856
	IF !EOF()
		SKIP 1 IN x856
	ENDIF
ENDDO

IF ltesting
	BROWSE FIELDS STYLE,COLOR,ID,totqty,linenum,printstuff
ENDIF

*do rollup_ptdet_units
newrollup()

WAIT WINDOW "At end of ROLLUP procedure" NOWAIT
*lbrowfiles = .t.
*Set Step On

IF lbrowfiles OR ltesting
	WAIT WINDOW "If tables are incorrect, CANCEL when XPTDET window opens"+CHR(13)+"Otherwise, will load PT table" TIMEOUT 2
	SELECT xpt
	LOCATE
	BROWSE
	SELECT xptdet
	LOCATE
	BROWSE
*  cancel
ENDIF

WAIT WINDOW ccustname+" Breakdown Round complete..." NOWAIT
SELECT x856
IF USED("PT")
	USE IN pt
ENDIF
IF USED("PTDET")
	USE IN ptdet
ENDIF
WAIT CLEAR
SELECT xpt
REPLACE ALL office WITH "L" IN xpt
REPLACE ALL MOD WITH "L" IN xpt

SELECT xptdet

REPLACE ALL office WITH "L" IN xptdet
REPLACE ALL MOD WITH "L" IN xptdet

*!*  Select ptid From xpt Into Cursor justptids Group By ptid

*!*  Select justptids
*!*  Go top
*!*  Scan
*!*    Select xptdet
*!*    Sum totqty To lnptqty For xptdet.ptid = justptids.ptid
*!*    Select xpt
*!*    replace All qty With lnptqty For xpt.ptid = justptids.ptid In xpt
*!*  Endscan



*Set Step On
RETURN

*************************************************************************************************
*!* Error Mail procedure - Cancel Before Start Date
*************************************************************************************************
PROCEDURE errordates

	IF lemail
		tsendto = "pgaidis@fmiint.com"   &&tsendtotest
		tcc =  "pgaidis@fmiint.com" &&tcctest
		cmailloc = IIF(coffice = "C","West",IIF(coffice = "N","New Jersey","Florida"))
		tsubject= "TGF "+cmailloc+" "+ccustname+" Pickticket Error: " +TTOC(DATETIME())
		tmessage = "File: "+cfilename+CHR(13)+" has Cancel date(s) which precede Start date(s)"
		tmessage = tmessage + "File needs to be checked and possibly resubmitted."

		IF llemailtest
			DO FORM m:\dev\frm\sendmail_au WITH tsendto,tfrom,tsubject,tcc,tattach,tmessage,"A",.T.
		ELSE
			DO FORM m:\dev\frm\dartmail2 WITH tsendto,tfrom,tsubject,tcc,tattach,tmessage,"A"
		ENDIF

	ENDIF
ENDPROC

*****************************************************************************************************
PROCEDURE newrollup
*****************************************************************************************************
** ZZZ
	nChangedRecs = 0
	SELECT xptdet
	SELECT COUNT(1) AS dup,ptid,STYLE,COLOR,ID,PACK,upc,linenum FROM xptdet INTO CURSOR xptids GROUP BY ptid,STYLE,COLOR,ID,PACK,upc

	SET STEP ON
	SELECT xptdet
	GO TOP
	SUM(totqty) TO lnBeforeCount

	SELECT xptids
	COUNT FOR dup >1 TO nChangedRecs

	SELECT xptdet
	SET DELETED ON
	LOCATE
	SELECT xptids
	SCAN FOR dup >1

		lcStr =""
		lcprepackstr =""
		SELECT xptdet
		SCATTER MEMVAR MEMO
		SELECT xptdet

		SCAN FOR ptid=xptids.ptid AND STYLE=xptids.STYLE AND COLOR=xptids.COLOR AND ID=xptids.ID AND PACK=xptids.PACK AND upc=xptids.upc

			IF !"PREPACKUPC"$xptdet.printstuff
				lcUPC     = xptdet.upc
				nTotqty   = xptdet.totqty
				lcLineNum = xptdet.linenum
				lcStr = lcStr +xptdet.printstuff+CHR(13)
			ELSE
				lcprepackstr = lcprepackstr+xptdet.printstuff+CHR(13)+CHR(13)
			ENDIF
		ENDSCAN

		LOCATE FOR ptid=xptids.ptid AND STYLE=xptids.STYLE AND COLOR=xptids.COLOR AND ID=xptids.ID AND PACK=xptids.PACK AND upc=xptids.upc
		SCATTER MEMVAR MEMO
		SELECT xptdet
		lcUPC     = xptdet.upc
		nTotqty   = xptdet.totqty
		lcLineNum = xptdet.linenum
*  If "PREPACKUPC"$printstuff And xptdet.linenum = xptids.linenum
*    lcRolledupString = ""
*  Else
*    lcRolledupString ="OPENUPC*"+Alltrim(lcUPC)+"*"+Alltrim(lcLineNum)+"*"+Alltrim(Transform(nTotQty))
		lcRolledupString =lcprepackstr+CHR(13)+lcStr
*  Endif
*set step on


		SUM(totqty) TO nTotqty1  FOR ptid=xptids.ptid AND STYLE=xptids.STYLE AND COLOR=xptids.COLOR AND ID=xptids.ID AND PACK=xptids.PACK AND upc=xptids.upc
		SELECT xptdet
		DELETE FOR ptid=xptids.ptid AND STYLE=xptids.STYLE AND COLOR=xptids.COLOR AND ID=xptids.ID AND PACK=xptids.PACK AND upc=xptids.upc

		INSERT INTO xptdet FROM MEMVAR
*  replace xptdet.printstuff WITH xptdet.printstuff+CHR(13)+lcRolledupString IN  xptdet

		REPLACE xptdet.printstuff WITH lcRolledupString IN  xptdet

		REPLACE totqty  WITH nTotqty1 IN xptdet
		REPLACE origqty WITH nTotqty1 IN xptdet

	ENDSCAN

*	SELECT xptdet
*	SELECT STYLE,COLOR,ID,totqty,VAL(linenum) AS LINE ,printstuff FROM xptdet ORDER BY LINE

*	SET STEP ON
	RETURN

************************************************************************************

*!*  	IF ltesting
*!*  *SET STEP ON
*!*  	ENDIF
*!*  	IF !ltesting
*!*  		ASSERT .F. MESSAGE "In rollup procedure"
*!*  	ENDIF
*!*  	nChangedRecs = 0

*!*  	SELECT xptdet
*!*  	SET DELETED ON
*!*  	LOCATE
*!*  	IF ltesting
*!*  *    WAIT WINDOW "Browsing XPTDET pre-rollup" TIMEOUT 2
*!*  *    BROW
*!*  	ENDIF
*!*  	cMsgStr = "PT/OSID/ODIDs changed: "
*!*  	SCAN FOR !DELETED()
*!*  		SCATTER MEMVAR
*!*  		nRecno = RECNO()
*!*  		COUNT TO nLoops FOR xptdet.ptid = m.ptid   AND xptdet.STYLE = m.style AND xptdet.COLOR = m.color ;
*!*  			AND xptdet.ID = m.id AND xptdet.PACK = m.pack AND xptdet.upc = m.upc AND RECNO('xptdet') > nRecno
*!*  		GO nRecno
*!*  		IF nLoops = 0
*!*  			LOOP
*!*  		ENDIF

*!*  		IF EOF()
*!*  			EXIT
*!*  		ENDIF

*!*  		lcUPC = ""
*!*  		nTotqty =0
*!*  		lcLineNum = ""
*!*  		lcRolledupString =""

*!*  		SELECT xptdet
*!*  		GO nRecno
*!*  		REPLACE xptdet.printstuff WITH xptdet.printstuff+CHR(13)+"ORIGQTY*"+ALLTRIM(TRANSFORM(xptdet.totqty))+CHR(13)+"ORIGUPC*"+lcUPC IN xptdet

*!*  		FOR tt = 1 TO nLoops
*!*  			LOCATE FOR xptdet.ptid = m.ptid ;
*!*  				AND xptdet.STYLE = m.style ;
*!*  				AND xptdet.COLOR = m.color ;
*!*  				AND xptdet.ID = m.id ;
*!*  				AND xptdet.PACK = m.pack ;
*!*  				AND xptdet.upc = m.upc ;
*!*  				AND RECNO('xptdet') > nRecno
*!*  			IF FOUND()
*!*  				SELECT xpt
*!*  				LOCATE FOR xpt.ptid = m.ptid
*!*  				IF !FOUND()
*!*  					WAIT WINDOW "PROBLEM!"
*!*  				ENDIF
*!*  				cship_ref = ALLT(xpt.ship_ref)
*!*  				IF nChangedRecs > 10
*!*  					cMsgStr = "More than 10 records rolled up"
*!*  				ELSE
*!*  					cMsgStr = cMsgStr+CHR(13)+cship_ref+" "+TRANSFORM(m.ptid)+" "+TRANSFORM(m.ptdetid)
*!*  				ENDIF
*!*  				nChangedRecs = nChangedRecs+1
*!*  				SELECT xptdet
*!*  *        alength = ALINES(aptdet,xptdet.printstuff,.T.,CHR(13))
*!*  *        cStoredUCC = ALLTRIM(segmentget(@aptdet,"UPC",alength))
*!*  				lcUPC     = xptdet.upc
*!*  				nTotqty   = xptdet.totqty
*!*  				lcLineNum = xptdet.linenum
*!*  *      lcLoopLine= alltrim(Transform(tt))
*!*  				IF "OPENUPC"$printstuff
*!*  					lcRolledupString = "OPENUPC*"+ALLTRIM(lcUPC)+"*"+ALLTRIM(lcLineNum)+"*"+ALLTRIM(TRANSFORM(nTotqty))
*!*  				ELSE
*!*  					lcRolledupString ="OPENUPC*"+ALLTRIM(lcUPC)+"*"+ALLTRIM(lcLineNum)+"*"+ALLTRIM(TRANSFORM(nTotqty))
*!*  				ENDIF

*!*  *        lcRolledupString = CHR(13)+CHR(13)+"SUBLINE*"+lcLineNum+CHR(13)+"SUBQTY*"+TRANSFORM(nTotqty)
*!*  				DELETE NEXT 1 IN xptdet
*!*  			ENDIF
*!*  			GO nRecno
*!*  			IF !EMPTY(lcRolledupString)
*!*  				REPLACE xptdet.printstuff WITH xptdet.printstuff+CHR(13)+lcRolledupString IN  xptdet
*!*  			ENDIF
*!*  			REPLACE xptdet.totqty WITH xptdet.totqty+nTotqty IN xptdet
*!*  			REPLACE xptdet.origqty WITH xptdet.totqty IN xptdet
*!*  		ENDFOR

*!*  		SELECT xptdet
*!*  	ENDSCAN
*!*  	IF ltesting
*!*  *    SET DELETED OFF
*!*  *    BROWSE
*!*  *    SET DELETED ON
*!*  	ENDIF

*!*  *!*    IF nChangedRecs > 0
*!*  *!*      WAIT WINDOW cMsgStr  &waittime&& TIMEOUT 2
*!*  *!*      WAIT WINDOW "There4 were "+TRANS(nChangedRecs)+" records rolled up" &waittime&& TIMEOUT 1
*!*  *!*    ELSE
*!*  *!*      WAIT WINDOW "There were NO records rolled up" &waittime&&  TIMEOUT 1
*!*  *!*    ENDIF


ENDPROC
