*!* m:\dev\prg\synclaire940edi_main.prg

PARAMETERS cOfficeIn
CLOSE DATA ALL
PUBLIC nAcctNum,cDelimiter,nRun_num,cTranslateOption,cWhichFile,cOffice,cCustname,cPropername,cStorenum,tsendtopt,tccpt
PUBLIC cFilter,nRunID,lComplete,lTesting,m.UCCNUMBER,lEmail,lTestImport,emailcommentstr,cUseFolder,lNoDetPrepack
PUBLIC ptid,ptctr,ptqty,xfile,cfilename,UploadCount,nFileCount,units,cISA_Num,tsendtodups,tccdups,tfrom
PUBLIC cOldScreen,tsendto,tcc,lcPath,lcArchivePath,nPTQty,cPTQty,archivefile,lBrowfiles,cMod,nFileSize
PUBLIC tsendtoerr,tccerr,cOfficeLoc,ldeletefile,cErrMsg,lJCP,lJCPMail,lN2,m.custsku,cXferName,lDoLineInsert,lLoadSQL
PUBLIC lDoImport
PUBLIC llEmailTest

PUBLIC ARRAY a856(1)

PUBLIC message_timeout,message_timeout2
tfrom = "Toll EDI Operations <toll-edi-ops@tollgroup.com>"

message_timeout ="nowait"
message_timeout2 ="timeout 2"

cErrMsg = ""
IF VARTYPE(cOfficeIn)="L"
	cOfficeIn = "C"
	cOffice = cOfficeIn
	cOfficeLoc = IIF(cOfficeIn = "N","NJ","CA")
ENDIF
cOffice = cOfficeIn
cMod = IIF(cOffice = "C","2",cOffice)
gOffice = cMod
lLoadSQL = .t.


lTesting =  .f.

lTestImport = lTesting
lOverridebusy = lTesting && Will override FTPSETUP chkbusy flag if .t.
lEmail = !lTesting
lBrowfiles = lTesting  && Will allow browsing of XPT/XPTDET cursors before upload if .t.

*lOverridebusy = .t.
*lBrowfiles = .t.
*lEmail = .f.

lDoImport = .T.

DO m:\dev\prg\_setvars WITH .T.
*SET ASSERTS OFF 
llEmailTest=.t.

TRY
	_SCREEN.WINDOWSTATE = IIF(lTesting OR lTestImport OR lOverridebusy,2,1)

	STORE "" TO cOldScreen
	STORE _SCREEN.CAPTION TO cOldScreen

	cCustname = "SYNCLAIRE"
	cUseName = "SYNCLAIRE"
	cOfficeLoc = " "
	cPropername = "Synclaire"
	nAcctNum  = 6521
	cMod = "2"
	xfile = ""
	lNoDetPrepack = .F.
	ldeletefile = .T.
	NormalExit = .F.
	lN2 = .F.
	tsendto =""
	tsendtoerr =""
	tcc=""
	tccerr=""
	cXferName = "940-SYNC-EDI-"
	STORE cPropername+" 940 Process" TO _SCREEN.CAPTION
	CLEAR
	WAIT WINDOW "Now setting up "+cPropername+" 940 process..." &message_timeout
set step on 	
	IF !lTesting
	SELECT 0
	USE F:\edirouting\FTPSETUP SHARED
	LOCATE FOR FTPSETUP.TRANSFER = cXferName
	ASSERT .F.
	IF FOUND()
		IF chkbusy AND !lOverridebusy
			WAIT WINDOW "940 Process is busy...exiting" TIMEOUT 2
			NormalExit = .T.
			THROW
		ENDIF
	ENDIF
	REPLACE chkbusy WITH .T.,trig_time WITH DATETIME() FOR FTPSETUP.TRANSFER = cXferName
	USE IN FTPSETUP
	endif

	DO m:\dev\prg\createx856a

	cfile = ""
	dXdate1 = ""
	dXxdate2 = DATE()
	nFileCount = 0

	nRun_num = 999
	lnRunID = nRun_num

	ASSERT .F. MESSAGE "At MM Config load"
	SELECT 0
	USE F:\3pl\DATA\mailmaster ALIAS mm SHARED
	LOCATE FOR mm.accountid = nAcctNum AND mm.office = cOffice AND edi_type = "940" AND acctname = 'SYNCLAIRE-EDI'

	IF FOUND()
		STORE TRIM(mm.basepath) TO lcPath
		STORE TRIM(mm.archpath) TO lcArchivePath

		tsendto = IIF(mm.use_alt,TRIM(mm.sendtoalt),TRIM(mm.sendto))
		tcc = IIF(mm.use_alt,TRIM(mm.ccalt),TRIM(mm.cc))

		STORE TRIM(mm.scaption) TO thiscaption
		STORE mm.testflag 	   TO lTest
		_SCREEN.CAPTION = thiscaption

		LOCATE FOR (mm.edi_type = "MISC") AND (mm.taskname = "GENERAL")
	
  	IF lTesting
			tsendto = IIF(mm.use_alt,TRIM(mm.sendtoalt),TRIM(mm.sendto))
			tcc = IIF(mm.use_alt,TRIM(mm.ccalt),TRIM(mm.cc))
		ENDIF

		tsendtoerr = IIF(mm.use_alt,TRIM(mm.sendtoalt),TRIM(mm.sendto))
		tccerr = IIF(mm.use_alt,TRIM(mm.ccalt),TRIM(mm.cc))

		LOCATE
		LOCATE FOR mm.edi_type = "MISC" AND mm.acctname = "SYNCLAIRE" AND mm.taskname = "DUPPTNOTICE"
		STORE TRIM(IIF(mm.use_alt,mm.sendtoalt,mm.sendto)) TO tsendtopt
		STORE TRIM(IIF(mm.use_alt,mm.ccalt,mm.cc)) TO tccpt

		LOCATE FOR mm.edi_type = "MISC" AND mm.acctname = "SYNCLAIRE" AND mm.taskname = "940DUPES"
		STORE TRIM(IIF(mm.use_alt,mm.sendtoalt,mm.sendto)) TO tsendtodups
		STORE TRIM(IIF(mm.use_alt,mm.ccalt,mm.cc)) TO tccdups
		USE IN mm
	ELSE
		WAIT WINDOW AT 10,10  "No parameters set for this acct# "+ALLTRIM(STR(nAcctNum))+"  ---> Office "+cOffice TIMEOUT 2
		USE IN mm
		NormalExit = .T.
		THROW
	ENDIF

	IF lTesting
		lcPath = "F:\FTPUSERS\Synclaire\940INTEST\"
		lcArchivePath = "F:\FTPUSERS\Synclaire\940INTEST\archive\"
		cUseFolder = "F:\WHP\WHDATA\"
		WAIT WINDOW "Test data...Importing into WHP tables"  &message_timeout
	ELSE
		xReturn = "XXX"
		DO m:\dev\prg\wf_alt WITH cOffice,nAcctNum
		cUseFolder = UPPER(xReturn)
	ENDIF

	DO ("m:\dev\PRG\"+cCustname+"940EDI_PROCESS")

	SELECT 0
	USE F:\edirouting\FTPSETUP SHARED
	REPLACE chkbusy WITH .F. FOR FTPSETUP.TRANSFER = cXferName
	USE IN FTPSETUP
	NormalExit = .T.

CATCH TO oErr
	IF !NormalExit
		ASSERT .F. MESSAGE "At CATCH..."
		SET STEP ON

		IF cErrMsg # "Failed duplicate line check"
			tsubject = cCustname+" 940 "+cOfficeLoc+" Upload Error ("+TRANSFORM(oErr.ERRORNO)+") at "+TTOC(DATETIME())
			tattach  = ""
			tsendto  = tsendtoerr
			tcc = tccerr
			tmessage = cCustname+" 940 "+cOfficeLoc+" Upload Error..... Please fix me........!"+CHR(13)+cErrMsg
			lcSourceMachine = SYS(0)
			lcSourceProgram = SYS(16)

			tmessage =tmessage+ "Try/CATCH Exeception Message:"+CHR(13)+;
				[  Message: ] + oErr.MESSAGE +CHR(13)+;
				[  Procedure: ] + oErr.PROCEDURE +CHR(13)+;
				[  LineNo: ] + STR(oErr.LINENO) +CHR(13)+;
				[  Error: ] + STR(oErr.ERRORNO) +CHR(13)+;
				[  Details: ] + oErr.DETAILS +CHR(13)+;
				[  StackLevel: ] + STR(oErr.STACKLEVEL) +CHR(13)+;
				[  LineContents: ] + oErr.LINECONTENTS+CHR(13)+;
				[  UserValue: ] + oErr.USERVALUE+CHR(13)+;
				[  Computer:  ] +lcSourceMachine+CHR(13)+;
				[  940 file:  ] +xfile+CHR(13)+;
				[  Program:   ] +lcSourceProgram

			tattach  = ""
      Do sendmail_au With tsendto,"noreply@tollgroup.com",tsubject,tcc,tattach,tmessage,""
               **Parameter lcTO, lcFrom,                lcSubject,lcCC,lcAttach,lcBody,lcBCC
		ELSE
			SELECT 0
			USE F:\edirouting\FTPSETUP SHARED
			REPLACE chkbusy WITH .F. FOR FTPSETUP.TRANSFER = cXferName
			USE IN FTPSETUP
		ENDIF
	ELSE
		WAIT WINDOW AT 10,10 "Normal Exit " TIMEOUT 2
	ENDIF
FINALLY
	CLOSE DATABASES ALL
ENDTRY
