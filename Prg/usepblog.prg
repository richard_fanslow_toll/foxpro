lparameters xprocess

for j=1 to 100
	for upi=1 to 1000
		try
			use (wf(goffice)+"pblog") exclusive in 0
			upi=9999
		catch
		endtry
	next
	
	xpblogok=.f.
	try		&& sometimes pblog is opened read-only  ???
		replace finished with .t. in pblog
		xpblogok=.t.
	catch
	endtry
	
	if !xpblogok
		use in pblog
		errmsg("PBLOG was opened read-only - "+transform(j),"usepblog")		
		loop
	else
		exit
	endif
next

if !used("pblog")
	email("Dyoung@fmiint.com","PBLOG can't be used by "+xprocess,,,,,.t.,,,,,.t.,,.t.)
	return .f.
endif