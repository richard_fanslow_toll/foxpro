* create / maintain ADP_FILES which is used by trucking to pay drivers. div = 02  dept = 0655
* Should be run weekly AROUND Wednesday.
* This utility should look for the ADP_FILES which will be used on the next 2 PayDays (typically next Tuesday).
* If either of the 2 files do not exist, create the files and import all employees in the usual way.
* THEN, make sure both files have all div = 02  dept = 0655 employees, even those that have been terminated within last 1 months

** 02/05/2008 MB: if this process is run on a Monday or Tuesday, make it affect the current paydate and the one after that.
** If it is run on any other day, make it affect the paydate after next sunday and the one after that.

utilsetup("ADPFILES")

LOCAL loADPFilesMaintenance
loADPFilesMaintenance = CREATEOBJECT('ADPFilesMaintenance')
loADPFilesMaintenance.MAIN()

schedupdate()

CLOSE DATABASES ALL
RETURN

#DEFINE LOGIT 1
#DEFINE WAITIT 2
#DEFINE NOWAITIT 4
#DEFINE SENDIT 8
#DEFINE CRLF CHR(13) + CHR(10)
#DEFINE RETURN_DATA_MANDATORY .F.
#DEFINE RETURN_DATA_NOT_MANDATORY .T.
#DEFINE CSVFORMAT 6
#DEFINE WRITE_ONLY_UNBUFFERED 11
#DEFINE COMMA ","

DEFINE CLASS ADPFilesMaintenance AS CUSTOM

	cProcessName = 'ADPFilesMaintenance'

	lTestMode = .F.  && .F. FOR NORMAL OPERATION, .T. TO SUPPRESS ALL BUT DEVELOPER EMAILS AND CHANGE INPUT TABLE, ETC.

	dToday = DATE()

	lAutoYield = .T.

	cStartTime = TTOC(DATETIME())
	
	* processing properties
	lUseDevADPTable = .F.
	lProcessThisWeeksPay = .F.

	cFoxProEmployeeTable = "F:\HR\HRDATA\EMPLOYEE"
	*cFoxProTermTable = "F:\HR\HRDATA\TERMINAT"
	cADPTable = "F:\HR\HRDATA\ADP"
	cHRGENPKTable = "F:\HR\HRDATA\HRGENPK"

	* wait window properties
	nWaitWindowTimeout = 2
	lWaitWindowIsOn = .T.

	* logfile properties
	lLoggingIsOn = .T.
	cLogFile = 'F:\UTIL\ADPFILES\LOGFILES\ADPFiles Maintenance_REPORT_log.txt'

	* INTERNAL email properties
	lSendInternalEmailIsOn = .T.
	cFrom = 'TGF Corporate <fmicorporate@fmiint.com>'
	cSendTo = 'Mark Bennett <mbennett@fmiint.com>'
	cCC = ''
	cSubject = 'ADPFiles Maintenance Report for ' + DTOC(DATE())
	cAttach = ''
	cBodyText = ''

	cCOMPUTERNAME = ''
	cUSERNAME = ''


	FUNCTION INIT
		IF NOT DODEFAULT()
			RETURN .F.
		ENDIF
		WITH THIS
			*SET RESOURCE OFF
			CLOSE DATA
			SET CENTURY ON
			SET DATE MDY
			SET HOURS TO 24
			SET ANSI ON
			SET TALK OFF
			SET DELETED ON
			SET CONSOLE OFF
			SET EXCLUSIVE OFF
			SET SAFETY OFF
			SET EXACT OFF
			SET STATUS BAR ON
			SET SYSMENU OFF
			SET ENGINEBEHAVIOR 70
			SET MULTILOCKS ON
			_VFP.AUTOYIELD = .lAutoYield
			.cCOMPUTERNAME = GETENV("COMPUTERNAME")
			.cUSERNAME = GETENV("USERNAME")
			IF .lTestMode THEN
				.cLogFile = 'F:\UTIL\ADPFILES\LOGFILES\ADPFiles Maintenance_REPORT_log_TESTMODE.txt'
				.cSendTo = 'Mark Bennett <mbennett@fmiint.com>'
			ENDIF
			.lLoggingIsOn = .lLoggingIsOn AND NOT EMPTY(.cLogFile)
			IF .lLoggingIsOn THEN
				SET ALTERNATE TO (.cLogFile) ADDITIVE
				SET ALTERNATE ON
			ENDIF
			IF .lUseDevADPTable THEN
				.cADPTable = "M:\DEV\HRDATA\ADP"
				.cHRGENPKTable = "M:\DEV\HRDATA\HRGENPK"
			ENDIF
			
		ENDWITH
	ENDFUNC


	FUNCTION DESTROY
		WITH THIS
			IF .lLoggingIsOn  THEN
				SET ALTERNATE OFF
				SET ALTERNATE TO
			ENDIF
		ENDWITH
		DODEFAULT()
	ENDFUNC


	FUNCTION MAIN
		WITH THIS
			LOCAL ldToday, ldLastSunday, ldNextSunday, ldNextSaturday, ldFridayAfterNextSunday, ldTwoMonthsAgo, ldTwoWeeksAgo
			TRY
				lnNumberOfErrors = 0

				.TrackProgress('', LOGIT)
				.TrackProgress('', LOGIT)
				.TrackProgress('=========================================================', LOGIT+SENDIT)
				.TrackProgress("ADPFiles Maintenance REPORT process started....", LOGIT+SENDIT+NOWAITIT)
				.TrackProgress('COMPUTERNAME = ' + .cCOMPUTERNAME, LOGIT+SENDIT)
				.TrackProgress('USERNAME = ' + .cUSERNAME, LOGIT+SENDIT)
				.TrackProgress('=========================================================', LOGIT+SENDIT)
				IF .lTestMode THEN
					.TrackProgress('                        TEST MODE                        ', LOGIT+SENDIT)
					.TrackProgress('=========================================================', LOGIT+SENDIT)
				ENDIF
				.TrackProgress('', LOGIT+SENDIT)
				.TrackProgress('New ADPFILE = ' + .cADPTable, LOGIT+SENDIT)
				.TrackProgress('', LOGIT+SENDIT)

				ldToday = .dToday
				* to force 'today' for testing
				*ldToday = {^2007-04-20}
				
				* if runing on Monday or Tuesday, process current paydate
				.lProcessThisWeeksPay = INLIST(DOW(ldToday,1),2,3)

				ldTwoMonthsAgo = GOMONTH( .dToday, -2 )
				ldTwoWeeksAgo = .dToday - 14

				* CALC DATES FOR LAST SUNDAY AND NEXT SUNDAY
				ldLastSunday = ldToday
				DO WHILE DOW( ldLastSunday, 1) <> 1
					ldLastSunday = ldLastSunday - 1
				ENDDO
				ldNextSunday = ldLastSunday + 7
				ldNextSaturday = ldLastSunday + 6
				ldFridayAfterNextSunday = ldNextSunday + 5

				.TrackProgress('Today = ' + TRANSFORM(ldToday), LOGIT+SENDIT)
				
				.TrackProgress('HR EMPLOYEE TABLE = ' + .cFoxProEmployeeTable, LOGIT+SENDIT)
				
				*.TrackProgress('HR TERMINATED TABLE = ' + .cFoxProTermTable, LOGIT+SENDIT)

				* create a cursor of active div 2, dept 655 employees 
				IF USED('CUREMPS') THEN
					USE IN CUREMPS
				ENDIF
				SELECT ADP_COMP, emp_num, LAST, FIRST, EMPLOYEE, ss_num, hire_date, DEPT, COMPANY, DIVISION, SALARY, IIF(ACTIVE,"A","T") AS STATUS ;
					FROM (.cFoxProEmployeeTable) ;
					INTO CURSOR CUREMPS ;
					WHERE ADP_COMP = "SXI" AND DIVISION = 2 AND DEPT = 655 AND ACTIVE ;
					READWRITE
					
					*OR (NOT ACTIVE AND (TERM_DATE >= ldTwoWeeksAgo))) ;

				* process for next paydate AND paydate after that
				IF .lProcessThisWeeksPay THEN
					.NewADPFileProcessing( ldLastSunday - 7, ldNextSunday - 7, ldNextSaturday - 7, ldFridayAfterNextSunday - 7 )
					.NewADPFileProcessing( ldLastSunday, ldNextSunday, ldNextSaturday, ldFridayAfterNextSunday )
				ELSE
					.NewADPFileProcessing( ldLastSunday, ldNextSunday, ldNextSaturday, ldFridayAfterNextSunday )
					.NewADPFileProcessing( ldLastSunday + 7, ldNextSunday + 7, ldNextSaturday + 7, ldFridayAfterNextSunday + 7 )
				ENDIF

*!*					* process for 
*!*					IF .lProcessThisWeeksPay THEN
*!*						.NewADPFileProcessing( ldLastSunday, ldNextSunday, ldNextSaturday, ldFridayAfterNextSunday )
*!*					ELSE
*!*						.NewADPFileProcessing( ldLastSunday + 7, ldNextSunday + 7, ldNextSaturday + 7, ldFridayAfterNextSunday + 7 )
*!*					ENDIF
				******************************************************************************

			CATCH TO loError

				.TrackProgress('There was an error.',LOGIT+SENDIT)
				.TrackProgress('The Error # is: ' + TRANSFORM(loError.ErrorNo), LOGIT+SENDIT)
				.TrackProgress('The Error Message is: ' + TRANSFORM(loError.MESSAGE), LOGIT+SENDIT)
				.TrackProgress('The Error occurred in Program: ' + TRANSFORM(loError.PROCEDURE), LOGIT+SENDIT)
				.TrackProgress('The Error occurred at line #: ' + TRANSFORM(loError.LINENO), LOGIT+SENDIT)
				lnNumberOfErrors = lnNumberOfErrors + 1
				IF TYPE('oWorkbook') = "O" THEN
					oWorkbook.SAVE()
				ENDIF
				IF TYPE('oExcel') = "O" THEN
					oExcel.QUIT()
				ENDIF
				CLOSE DATABASES ALL

			ENDTRY

			CLOSE DATA
			WAIT CLEAR
			***************** INTERNAL email results ******************************
			.TrackProgress('About to send status email.',LOGIT)
			.TrackProgress('The logfile is: ' + .cLogFile, SENDIT)
			.TrackProgress("ADPFiles Maintenance REPORT process started: " + .cStartTime, LOGIT+SENDIT)
			.TrackProgress("ADPFiles Maintenance REPORT process finished: " + TTOC(DATETIME()), LOGIT+SENDIT)

			IF .lSendInternalEmailIsOn THEN
				DO FORM dartmail2 WITH .cSendTo,.cFrom,.cSubject,.cCC,.cAttach,.cBodyText,"A"
				.TrackProgress('Sent status email.',LOGIT)
			ELSE
				.TrackProgress('Did not send status email: THIS.lSendInternalEmailIsOn = FALSE.',LOGIT)
			ENDIF

		ENDWITH
		RETURN
	ENDFUNC && main


	PROCEDURE NewADPFileProcessing
		LPARAMETERS tdLastSunday, tdNextSunday, tdNextSaturday, tdFridayAfterNextSunday
		LOCAL lcEmployeesAdded
		
		* make sure all drivers are in HRDATA\ADP.DBF for the passed paydate
		IF NOT USED('NEWADPFILE') THEN
			USE (.cADPTable) IN 0 ALIAS NEWADPFILE
		ENDIF
		IF NOT USED('GENERATEPK') THEN
			USE (.cHRGENPKTable) IN 0 ALIAS GENERATEPK
		ENDIF
		
		.TrackProgress('', LOGIT+SENDIT)
		.TrackProgress('===============================================================================', LOGIT+SENDIT)
		.TrackProgress('Last Sunday = ' + TRANSFORM(tdLastSunday), LOGIT+SENDIT)
		.TrackProgress('Next Saturday = ' + TRANSFORM(tdNextSaturday), LOGIT+SENDIT)
		.TrackProgress('Next Sunday = ' + TRANSFORM(tdNextSunday), LOGIT+SENDIT)
		.TrackProgress('Friday After Next Sunday (i.e., Paydate) = ' + TRANSFORM(tdFridayAfterNextSunday), LOGIT+SENDIT)

		.TrackProgress("Adding Div 2 Dept 655 Employees to " + .cADPTable + "...", LOGIT+SENDIT)
		lcEmployeesAdded = ""
		SELECT CUREMPS
		SCAN
			SCATTER MEMVAR MEMO
			m.pay_rate = CUREMPS.SALARY
			m.pay_date = tdFridayAfterNextSunday
			m.weekof = TRANSFORM( tdLastSunday ) + " - " + TRANSFORM( tdNextSaturday )
			m.WEEK = tdLastSunday
			SELECT NEWADPFILE
			LOCATE FOR (emp_num = CUREMPS.emp_num) AND (pay_date = m.pay_date)
			IF NOT FOUND() THEN
				m.ADPID = genpk("adp","hr")
				INSERT INTO NEWADPFILE FROM MEMVAR
				lcEmployeesAdded = lcEmployeesAdded + m.EMPLOYEE + " File#: " + TRANSFORM(m.EMP_NUM) + "    Status: " + m.Status + CRLF
			ENDIF
		ENDSCAN
		
		IF EMPTY( lcEmployeesAdded ) THEN
			lcEmployeesAdded = "* No employees needed to be added to " + .cADPTable + " *"
		ENDIF
		
		.TrackProgress(lcEmployeesAdded, LOGIT+SENDIT)
		
		SELECT NEWADPFILE
		LOCATE
	ENDPROC


	FUNCTION ExecSQL
		LPARAMETERS tcSQL, tcCursorName, tlNoDataReturnedIsOkay
		LOCAL llRetval, lnResult
		WITH THIS
			* close target cursor if it's open
			IF USED(tcCursorName)
				USE IN (tcCursorName)
			ENDIF
			WAIT WINDOW "Fetching remote data; please wait..." NOWAIT
			lnResult = sqlexec(.nSQLHandle, tcSQL, tcCursorName)
			llRetval = ( lnResult > 0 )
			IF llRetval THEN
				* see if any data came back
				IF NOT tlNoDataReturnedIsOkay THEN
					IF (NOT USED(tcCursorName)) OR EOF(tcCursorName) THEN
						llRetval = .F.
						.TrackProgress('No data was returned by this query: ' +CRLF + tcSQL, LOGIT+SENDIT)
						.cSendTo = 'Mark Bennett <mbennett@fmiint.com>'
					ENDIF
				ENDIF
			ELSE
				.TrackProgress("Result Code [" + TRANSFORM(lnResult) + "] returned by this query: " + tcSQL, LOGIT+SENDIT)
				.cSendTo = 'Mark Bennett <mbennett@fmiint.com>'
			ENDIF
			WAIT CLEAR
			RETURN llRetval
		ENDWITH
	ENDFUNC


	PROCEDURE TrackProgress
		* do any combination of Wait Window, writing to logfile, and adding to body of email,
		* based on nFlags parameter.
		LPARAMETERS tcExpression, tnFlags
		WITH THIS
			IF BITAND(tnFlags,LOGIT) = LOGIT THEN
				IF .lLoggingIsOn THEN
					?
					? .cProcessName + "  " + TTOC(DATETIME()) + ": " + tcExpression
				ENDIF
			ENDIF
			IF .lWaitWindowIsOn THEN
				IF BITAND(tnFlags,NOWAITIT) = NOWAITIT THEN
					WAIT WINDOW tcExpression NOWAIT
				ENDIF
				IF BITAND(tnFlags,WAITIT) = WAITIT THEN
					WAIT WINDOW tcExpression TIMEOUT .nWaitWindowTimeout
				ENDIF
			ENDIF
			IF BITAND(tnFlags,SENDIT) = SENDIT THEN
				IF .lSendInternalEmailIsOn THEN
					.cBodyText = .cBodyText + tcExpression + CRLF + CRLF
				ENDIF
			ENDIF
		ENDWITH
	ENDPROC  &&  TrackProgress


*!*		PROCEDURE MainProcessing
*!*			LPARAMETERS tdLastSunday, tdNextSunday, tdNextSaturday, tdFridayAfterNextSunday
*!*			LOCAL lcEmployeesAdded

*!*			* check to see if the ADP file exists, if not then create it

*!*			LOCAL llSalaried, lcFileName, lcRootFileName

*!*			.TrackProgress('', LOGIT+SENDIT)
*!*			.TrackProgress('===============================================================================', LOGIT+SENDIT)
*!*			.TrackProgress('Last Sunday = ' + TRANSFORM(tdLastSunday), LOGIT+SENDIT)
*!*			.TrackProgress('Next Saturday = ' + TRANSFORM(tdNextSaturday), LOGIT+SENDIT)
*!*			.TrackProgress('Next Sunday = ' + TRANSFORM(tdNextSunday), LOGIT+SENDIT)
*!*			.TrackProgress('Friday After Next Sunday (i.e., Paydate) = ' + TRANSFORM(tdFridayAfterNextSunday), LOGIT+SENDIT)

*!*			lcRootFileName = "ADP" + ;
*!*				PADL(MONTH(tdLastSunday),2,"0") + PADL(DAY(tdLastSunday),2,"0") + PADL(YEAR(tdLastSunday),4,"0")

*!*			IF .lTestMode THEN
*!*				lcFileName = "F:\UTIL\ADPFILES\TESTDATA\" + lcRootFileName + ".DBF"
*!*			ELSE
*!*				lcFileName = "F:\TIMECLK\ADPFILES\" + lcRootFileName + ".DBF"
*!*			ENDIF
*!*			
*!*			IF USED("ADP_File")
*!*				USE IN ADP_File
*!*			ENDIF
*!*			IF FILE( lcFileName ) THEN
*!*				.TrackProgress("File: " + lcFileName + " already exists.", LOGIT+SENDIT)
*!*			ELSE
*!*				.TrackProgress("File: " + lcFileName + " does not exist -- about to create it!", LOGIT+SENDIT)

*!*				* create it
*!*				USE F:\UTIL\ADPFILES\PAYRLSUM IN 0 ALIAS PAYRLSUM EXCLUSIVE
*!*				SELECT PAYRLSUM
*!*				COPY TO ( lcFileName )
*!*				USE IN PAYRLSUM

*!*				* target file should exist when we get to here, unless there was an error creating it
*!*				IF NOT FILE( lcFileName ) THEN
*!*					.TrackProgress('***** Error creating file: ' + lcFileName, LOGIT+SENDIT)
*!*					RETURN
*!*				ENDIF

*!*				* open the target table and index it
*!*				USE ( lcFileName ) IN 0 EXCL ALIAS ADP_File
*!*				
*!*				*SELECT ADP_File
*!*				*BROWSE

*!*				SELECT ADP_File
*!*				
*!*				INDEX ON ALLTRIM(STR(DEPT))+ALLTRIM(STR(DIVISION))+LAST TAG DEPT
*!*				INDEX ON REG_HRS   TAG REG_HRS DESCENDING
*!*				INDEX ON COMPANY   TAG COMPANY
*!*				INDEX ON EMPLOYEE  TAG EMPLOYEE
*!*				INDEX ON OT_HRS    TAG OT_HRS DESCENDING
*!*				INDEX ON HDAY_HRS  TAG HDAY_HRS DESCENDING
*!*				INDEX ON SICK_HRS  TAG SICK_HRS DESCENDING
*!*				INDEX ON VAC_HRS   TAG VAC_HRS DESCENDING
*!*				INDEX ON OTHER_HRS TAG OTHER_HRS DESCENDING
*!*				INDEX ON STR(DEPT) + UPPER(EMPLOYEE) TAG adp_order

*!*				USE IN ADP_File

*!*			ENDIF

*!*			* now make sure ADP_File is fully populated, regardless of whether it was newly created or not
*!*			.TrackProgress("Adding Div 2 Dept 655 Employees to File: " + lcFileName + "...", LOGIT+SENDIT)
*!*			lcEmployeesAdded = ""
*!*			USE ( lcFileName ) IN 0 SHARED ALIAS ADP_File
*!*			SELECT CUREMPS
*!*			SCAN
*!*				SCATTER MEMVAR MEMO
*!*				m.pay_rate = CUREMPS.SALARY
*!*				m.pay_date = tdFridayAfterNextSunday
*!*				m.weekof = TRANSFORM( tdLastSunday ) + " - " + TRANSFORM( tdNextSaturday )
*!*				SELECT ADP_File
*!*				LOCATE FOR emp_num = CUREMPS.emp_num
*!*				IF NOT FOUND() THEN
*!*					INSERT INTO ADP_File FROM MEMVAR
*!*					lcEmployeesAdded = lcEmployeesAdded + m.EMPLOYEE + " File#: " + TRANSFORM(m.EMP_NUM) + "    Status: " + m.Status + CRLF
*!*				ENDIF
*!*			ENDSCAN
*!*			
*!*			IF EMPTY( lcEmployeesAdded ) THEN
*!*				lcEmployeesAdded = "* No employees needed to be added *"
*!*			ENDIF
*!*			
*!*			.TrackProgress(lcEmployeesAdded, LOGIT+SENDIT)

*!*			*!*			SELECT ADP_File
*!*			*!*			SET ORDER TO ADP_ORDER
*!*			*!*			LOCATE
*!*			*!*			BROWSE
*!*			
*!*		ENDPROC



ENDDEFINE

