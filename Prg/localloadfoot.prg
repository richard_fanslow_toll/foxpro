

*******************************************************************************************************
* Process Footstar pm data to produce a bill.
*
*******************************************************************************************************
LPARAMETERS tcMode

LOCAL loFootstarProc

SET PROCEDURE TO F:\FOOTSTAR\PRG\FOOTCLASS

loFootstarProc = CREATEOBJECT('FOOTSTAR')

loFootstarProc.UseLocalData()

loFootstarProc.Load()

RETURN
