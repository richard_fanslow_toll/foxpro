* Create spreadsheet of Kronos Vacation Balances FOR THE 2008 CODES ONLY!

LOCAL loKronosVacationLiabilityReport
loKronosVacationLiabilityReport = CREATEOBJECT('KronosVacationLiabilityReport')
loKronosVacationLiabilityReport.MAIN()
CLOSE DATABASES ALL
RETURN

#DEFINE LOGIT 1
#DEFINE WAITIT 2
#DEFINE NOWAITIT 4
#DEFINE SENDIT 8
#DEFINE CRLF CHR(13) + CHR(10)
#DEFINE CARRIAGE_RETURN CHR(13)
#DEFINE LINE_FEED CHR(10)
#DEFINE RETURN_DATA_MANDATORY .F.
#DEFINE RETURN_DATA_NOT_MANDATORY .T.
#DEFINE SECS_PER_HOUR 3600
#DEFINE SECS_PER_DAY (3600.00 * 8.00)

#DEFINE ACCRUAL_TAKEN 1
#DEFINE ACCRUAL_GRANT 2
#DEFINE ACCRUAL_RESET 3
#DEFINE ACCRUAL_CARRY_FORWARD 11


#DEFINE ACCRUAL_VACATION_AVAILABLE_CODE 801
#DEFINE ACCRUAL_SICK_AVAILABLE_CODE 802


DEFINE CLASS KronosVacationLiabilityReport AS CUSTOM

	cProcessName = 'KronosVacationLiabilityReport'

	lTestMode = .T.  && .F. FOR NORMAL OPERATION, .T. TO SUPPRESS ALL BUT DEVELOPER EMAILS AND CHANGE INPUT TABLE, ETC.

	lAutoYield = .T.
	
	lCalcSickBalance = .T.

	cStartTime = TTOC(DATETIME())

	* connection properties
	nSQLHandle = 0
	nSQLHandle2 = 0

	* file properties
	nFileHandle = 0

	* date properties
	dAsOfDate = DATE()
	*dAsOfDate = {^2007-12-15}
	dtNow = DATETIME()
	dToday = DATE()
	cToday = DTOC(DATE())

	* filter properties
	*cCompanyCodesToReport = "('SXI')"
	cCompanyCodesToReport = "('AXA','ZXU','SXI')"
	*cCompanyCodesToReport = "('SXI')"

	cMiscWhere = ""
	*cMiscWhere = " AND (L.LABORLEV2NM IN ('71','72','73','74','76','78')) "
	*cMiscWhere = " AND (L.LABORLEV2NM IN ('73') "
	
	lExclude9999s = .T.
	*lExclude9999s = .F.
	
	lExcludeInactiveNOW = .T.
	
	lExcludeInactiveTHEN = .T.

	* table properties
	cAccrualsDataTablePath = 'F:\UTIL\KRONOS\ACCRUALSDATA\'

	* wait window properties
	nWaitWindowTimeout = 2
	lWaitWindowIsOn = .T.

	* logfile properties
	lLoggingIsOn = .T.
	cLogFile = 'F:\UTIL\KRONOS\Logfiles\KronosVacationLiabilityReport_log.txt'

	* INTERNAL email properties
	lSendInternalEmailIsOn = .T.
	cFrom = 'TGF Corporate <fmicorporate@fmiint.com>'
	*cSendTo = 'Mark Bennett <mbennett@fmiint.com>'
	cSendTo = 'Lucille Waldrip <lwaldrip@fmiint.com>, Stephanie Kochanski <skochanski@fmiint.com>, Mark Bennett <mbennett@fmiint.com>'
	cCC = ''
	cSubject = 'Kronos Vacation Liability Report for: ' + TTOC(DATETIME())
	cAttach = ''
	cBodyText = ''

	cCOMPUTERNAME = ''
	cUSERNAME = ''

	FUNCTION INIT
		IF NOT DODEFAULT()
			RETURN .F.
		ENDIF
		WITH THIS
			CLEAR
			*SET RESOURCE OFF
			CLOSE DATA
			SET CENTURY ON
			SET DATE AMERICAN
			SET DECIMALS TO 3
			SET HOURS TO 24
			SET ANSI ON
			SET TALK OFF
			SET DELETED ON
			SET CONSOLE OFF
			SET EXCLUSIVE OFF
			SET SAFETY OFF
			SET EXACT OFF
			SET STATUS BAR ON
			SET SYSMENU OFF
			SET ENGINEBEHAVIOR 70
			_VFP.AUTOYIELD = .lAutoYield
			.cCOMPUTERNAME = GETENV("COMPUTERNAME")
			.cUSERNAME = GETENV("USERNAME")
			IF .lTestMode THEN
				.cSendTo = 'Mark Bennett <mbennett@fmiint.com>'
				.cLogFile = 'F:\UTIL\KRONOS\Logfiles\KronosVacationLiabilityReport_log_TESTMODE.txt'
			ENDIF
			.lLoggingIsOn = .lLoggingIsOn AND NOT EMPTY(.cLogFile)
			IF .lLoggingIsOn THEN
				SET ALTERNATE TO (.cLogFile) ADDITIVE
				SET ALTERNATE ON
			ENDIF
			* re-init date properties after all SETTINGS
			.dtNow = DATETIME()
			.dToday = DATE()
			.cToday = DTOC(DATE())
		ENDWITH
	ENDFUNC


	FUNCTION DESTROY
		WITH THIS
			IF .lLoggingIsOn  THEN
				SET ALTERNATE OFF
				SET ALTERNATE TO
			ENDIF
		ENDWITH
		DODEFAULT()
	ENDFUNC


	FUNCTION MAIN
		WITH THIS
			LOCAL lnNumberOfErrors, lcSQL, loError, lcTopBodyText, llValid
			LOCAL lcXLFileName, ldStartDate, ldEndDate, lcStartDate, lcEndDate, ldDate
			LOCAL lcSQLStartDate, lcSQLEndDate, lcXLFileName
			LOCAL oExcel, oWorkbook, oWorksheet, lnRow, lcRow, lnStartRow, lcStartRow, lnEndRow, lnPP, lcPP, lnNN, lcNN, lcJJ
			LOCAL lcColsToProcess, lnColumn, lcBoldColumn, i, j, lcBoldRow, luCellValue, lcTargetDirectory, ldAsOfDate, lcFiletoSaveAs
			LOCAL lcFileDate, lcDivision, lcTitle, llSaveAgain, lcPayCode, lcCompanyCodesToReport, lcMiscWhere
			LOCAL ldAsOfDate, lcAsOfDate, lcSQLAsOfDate, lcExcludeInactiveTHENWhere, lcTS_EffectiveFrom, lcTS_TODAY_EffectiveTo
			LOCAL lcTS_EffectiveFrom, lcTS_EffectiveTo, lcAccrualsTable, lcSQL2, lnAdjustment, lc9999Where, lcExcludeInactiveNOWWhere 


			TRY
				lnNumberOfErrors = 0
				lcTopBodyText = ''

				*.TrackProgress('==================================================================================================================', LOGIT+SENDIT)
				.TrackProgress('Kronos Vacation Liability Report process started....', LOGIT+SENDIT+NOWAITIT)
				.TrackProgress('COMPUTERNAME = ' + .cCOMPUTERNAME, LOGIT+SENDIT)
				.TrackProgress('USERNAME = ' + .cUSERNAME, LOGIT+SENDIT)
				.TrackProgress('==================================================================================================================', LOGIT+SENDIT)
				IF .lTestMode THEN
					.TrackProgress('', LOGIT)
					.TrackProgress('==================================================================================================================', LOGIT+SENDIT)
					.TrackProgress('                        TEST MODE                        ', LOGIT+SENDIT)
					.TrackProgress('==================================================================================================================', LOGIT+SENDIT)
				ENDIF
				.TrackProgress('', LOGIT)

				ldAsOfDate = .dAsOfDate
								
				lcAsOfDate = STRTRAN(DTOC(ldAsOfDate),"/","-")

				SET DATE YMD

				lcSQLAsOfDate = STRTRAN(DTOC(ldAsOfDate),"/","")
				lcTS_EffectiveFrom   = "{ts '" + TRANSFORM(YEAR(.dAsOfDate)) + "-" + PADL(MONTH(.dAsOfDate),2,"0") + "-" + PADL(DAY(.dAsOfDate),2,"0") + " 00:00:00.0'}"
				lcTS_EffectiveTo = "{ts '" + TRANSFORM(YEAR(.dAsOfDate)) + "-" + PADL(MONTH(.dAsOfDate),2,"0") + "-" + PADL(DAY(.dAsOfDate),2,"0") + " 23:59:59.9'}"

				* for getting those ee's active as of today
				lcTS_TODAY_EffectiveFrom   = "{ts '" + TRANSFORM(YEAR(.dToday)) + "-" + PADL(MONTH(.dToday),2,"0") + "-" + PADL(DAY(.dToday),2,"0") + " 00:00:00.0'}"
				lcTS_TODAY_EffectiveTo = "{ts '" + TRANSFORM(YEAR(.dToday)) + "-" + PADL(MONTH(.dToday),2,"0") + "-" + PADL(DAY(.dToday),2,"0") + " 23:59:59.9'}"

				SET DATE AMERICAN

				.cSubject = 'Kronos Vacation Liability (TUG) Report for: ' + lcAsOfDate
				lcXLFileName = "F:\UTIL\KRONOS\REPORTS\VACATION_LIABILITY_REPORT_" + lcAsOfDate + ".XLS"

				* delete output file
				IF FILE(lcXLFileName) THEN
					DELETE FILE (lcXLFileName)
				ENDIF

				IF USED('CURACCRUALS') THEN
					USE IN CURACCRUALS
				ENDIF

				* PREP FOR MAIN SQL
				
				lcCompanyCodesToReport = .cCompanyCodesToReport
				
				*************************************
				*************************************
				*************************************
				** Further filtering !
				lcMiscWhere = .cMiscWhere
				*************************************
				*************************************
				*************************************
				
				IF .lExclude9999s THEN
					lc9999Where = " AND L.LABORLEV7NM <> '9999' "
				ELSE
					lc9999Where = ""
				ENDIF

				IF .lExcludeInactiveNOW THEN   
					SET TEXTMERGE ON
TEXT TO	lcExcludeInactiveNOWWhere NOSHOW
AND (( EXISTS
	 (SELECT * FROM PERSONSTATUSMM
      WHERE ((PERSONSTATUSMM.USERACCTSTATID = 1)
		AND (PERSONSTATUSMM.EFFECTIVEDTM <= <<lcTS_TODAY_EffectiveTo>>)
		AND (PERSONSTATUSMM.EXPIRATIONDTM > <<lcTS_TODAY_EffectiveFrom>>))
		AND (PERSONSTATUSMM.PERSONID = J.PERSONID))		
OR EXISTS
	 (SELECT * FROM PERSONSTATUSMM
      WHERE ((PERSONSTATUSMM.EMPLOYMENTSTATID = 1)
		AND (PERSONSTATUSMM.EFFECTIVEDTM <= <<lcTS_TODAY_EffectiveTo>>)
		AND (PERSONSTATUSMM.EXPIRATIONDTM > <<lcTS_TODAY_EffectiveFrom>>))
		AND (PERSONSTATUSMM.PERSONID = J.PERSONID)))
		AND (J.PERSONID > 0)
		AND (J.DELETEDSW = 0))
ENDTEXT
					SET TEXTMERGE OFF
				ELSE
					lcExcludeInactiveNOWWhere = ""
				ENDIF


				IF .lExcludeInactiveTHEN THEN
					SET TEXTMERGE ON
TEXT TO	lcExcludeInactiveTHENWhere NOSHOW
AND (( EXISTS
	 (SELECT * FROM PERSONSTATUSMM
      WHERE ((PERSONSTATUSMM.USERACCTSTATID = 1)
		AND (PERSONSTATUSMM.EFFECTIVEDTM <= <<lcTS_EffectiveTo>>)
		AND (PERSONSTATUSMM.EXPIRATIONDTM > <<lcTS_EffectiveFrom>>))
		AND (PERSONSTATUSMM.PERSONID = J.PERSONID))		
OR EXISTS
	 (SELECT * FROM PERSONSTATUSMM
      WHERE ((PERSONSTATUSMM.EMPLOYMENTSTATID = 1)
		AND (PERSONSTATUSMM.EFFECTIVEDTM <= <<lcTS_EffectiveTo>>)
		AND (PERSONSTATUSMM.EXPIRATIONDTM > <<lcTS_EffectiveFrom>>))
		AND (PERSONSTATUSMM.PERSONID = J.PERSONID)))
		AND (J.PERSONID > 0)
		AND (J.DELETEDSW = 0))
ENDTEXT
					SET TEXTMERGE OFF
				ELSE
					lcExcludeInactiveTHENWhere = ""
				ENDIF

				* main SQL

				*************************************************************************************************
				SET TEXTMERGE ON

TEXT TO	lcSQL NOSHOW

SELECT
	A.PERSONID, P.PERSONNUM AS FILE_NUM, P.FULLNM AS EMPLOYEE, J.EMPLOYEEID,
	L.LABORLEV1NM AS ADP_COMP, L.LABORLEV2NM AS DIVISION, L.LABORLEV3NM AS DEPT, L.LABORLEV4NM AS WORKSITE, R.ACTUALCUSTOMDTM AS SENIORDATE,
	P.COMPANYHIREDTM AS HIREDATE,
	(SUM(CASE A.ACCRUALCODEID WHEN ACCRUAL_SICK_AVAILABLE_CODE THEN A.AMOUNT ELSE 0.000 END) / SECS_PER_HOUR) AS S_ACCRUAL,
	(SUM(CASE A.ACCRUALCODEID WHEN ACCRUAL_VACATION_AVAILABLE_CODE THEN A.AMOUNT ELSE 0.000 END) / SECS_PER_HOUR) AS V_ACCRUAL,
	0.000 AS VBE_ACCRUAL,
	0000.00 AS HOURLY_WAGE, 0 AS LOTR, 0 AS IN_ADP
FROM ACCRUALTRAN A
INNER JOIN PERSON P
ON P.PERSONID = A.PERSONID
INNER JOIN JAIDS J
ON J.PERSONID = P.PERSONID
INNER JOIN COMBHOMEACCT C
ON C.EMPLOYEEID = J.EMPLOYEEID
INNER JOIN LABORACCT L
ON L.LABORACCTID = C.LABORACCTID
LEFT OUTER JOIN PRSNCSTMDATEMM R
ON R.PERSONID = J.PERSONID
AND R.CUSTOMDATETYPEID = 1
WHERE A.ACCRUALCODEID IN (ACCRUAL_VACATION_AVAILABLE_CODE,ACCRUAL_SICK_AVAILABLE_CODE)
AND A.EFFECTIVEDATE >=
	(
	SELECT MAX(EFFECTIVEDATE)
	FROM ACCRUALTRAN
	WHERE PERSONID = A.PERSONID
	AND ACCRUALCODEID = A.ACCRUALCODEID
	AND TYPE IN (ACCRUAL_RESET,ACCRUAL_CARRY_FORWARD)
	)
AND A.EFFECTIVEDATE <= <<lcTS_EffectiveTo>>
AND J.PERSONID > 0
AND J.DELETEDSW = 0
AND C.EFFECTIVEDTM <= <<lcTS_EffectiveTo>>
AND C.EXPIRATIONDTM > <<lcTS_EffectiveFrom>>
AND L.LABORLEV1NM IN <<lcCompanyCodesToReport>> <<lcMiscWhere>>
<<lc9999Where>> <<lcExcludeInactiveNOWWhere>> <<lcExcludeInactiveTHENWhere>>
GROUP BY A.PERSONID, P.PERSONNUM, P.FULLNM, J.EMPLOYEEID, L.LABORLEV1NM, L.LABORLEV2NM, L.LABORLEV3NM, L.LABORLEV4NM, R.ACTUALCUSTOMDTM, P.COMPANYHIREDTM
ORDER BY P.FULLNM

ENDTEXT

				*AND L.LABORLEV4NM = 'NJ1'
*AND P.PERSONNUM = '1102'


				* 5/16/06 MB: Now using APPLYDTM instead of ADJSTARTDTM to solve problem where hours crossing Midnight Saturday were not being reported.
				*					" WHERE (A.ADJSTARTDTM >= '" + lcSQLStartDate + "')" + ;
				*					" AND (A.ADJSTARTDTM < '" + lcSQLEndDate + "')" + ;

				IF .lTestMode THEN
					.TrackProgress('', LOGIT+SENDIT)
					.TrackProgress('lcSQL =' + lcSQL, LOGIT+SENDIT)
				ENDIF

				.TrackProgress('Connecting to WTK System....', LOGIT+SENDIT)

				* get ss#s from ADP
				OPEN DATABASE F:\UTIL\KRONOS\KRONOSEDI

				*.nSQLHandle = SQLCONNECT("KRONOSEDI","tkcsowner","tkcsowner")
				.nSQLHandle = SQLCONNECT("WTK_ON_PAYROLL2","sa","kr0n1tes")

				IF .nSQLHandle > 0 THEN

					IF .ExecSQL(lcSQL, 'CURACCRUALS', RETURN_DATA_MANDATORY) THEN

						SELECT CURACCRUALS
						GOTO TOP

						IF EOF() THEN
							.TrackProgress("There was no data to export!", LOGIT+SENDIT)
						ELSE
							lcFileDate = PADL(MONTH(ldAsOfDate),2,"0") + "-"  + PADL(DAY(ldAsOfDate),2,"0") + "-" + PADL(YEAR(ldAsOfDate),4,"0")

							*!*	************************************************************************************************
							*!*	************************************************************************************************
							*!*	IF .F. THEN
							*!*		* create a DBF which can be used by the Vacation Warnings Program to compare balances with FoxPro.
							*!*		lcAccrualsTable = .cAccrualsDataTablePath + "Kronos_VACATION_LIABILITY_" + lcFileDate
							*!*		IF FILE(lcAccrualsTable+'.DBF') THEN
							*!*			DELETE FILE (lcAccrualsTable+'.DBF')
							*!*		ENDIF

							*!*		IF USED('CURBALANCECHECK') THEN
							*!*			USE IN CURBALANCECHECK
							*!*		ENDIF

							*!*		SELECT ;
							*!*			LEFT(ADP_COMP,3) AS ADP_COMP, ;
							*!*			INT(VAL(LEFT(FILE_NUM,4))) AS FILE_NUM, ;
							*!*			EMPLOYEE, ;
							*!*			INT(S_ACCRUAL)/8 AS K_SIK_AVL, ;
							*!*			INT(V_ACCRUAL)/8 AS K_VAC_AVL, ;
							*!*			INT(VBE_ACCRUAL)/8 AS K_VAC_BE ;
							*!*			FROM CURACCRUALS ;
							*!*			INTO CURSOR CURBALANCECHECK ;
							*!*			ORDER BY ADP_COMP, FILE_NUM

							*!*		SELECT CURBALANCECHECK
							*!*		GOTO TOP
							*!*		COPY TO (lcAccrualsTable)

							*!*		IF USED('CURBALANCECHECK') THEN
							*!*			USE IN CURBALANCECHECK
							*!*		ENDIF

							*!*		IF FILE(lcAccrualsTable+'.DBF') THEN
							*!*			.TrackProgress('Created Accruals Table : ' + lcAccrualsTable+'.DBF', LOGIT+SENDIT)
							*!*		ENDIF
							*!*	ENDIF && .F. THEN

							=SQLDISCONNECT(.nSQLHandle)

							CLOSE DATABASES

							************************************************************************************************
							************************************************************************************************

							* Now get Hourly Rates from ADP into another cursor so we can add them to the accruals cursor
							* get ss#s from ADP
							OPEN DATABASE F:\UTIL\ADPCONNECTION\CONNECT1

							.nSQLHandle = SQLCONNECT("PCPAYWIN","MBENNETT","VP737A")

							IF .nSQLHandle > 0 THEN
								lcSQL2 = ;
									"SELECT " + ;
									" COMPANYCODE AS ADP_COMP, " + ;
									" {fn IFNULL(FILE#,0000)} AS FILE_NUM, " + ;
									" {fn LEFT(HOMEDEPARTMENT,2)} AS DIVISION, " + ;
									" 0 AS LOTR, " + ;
									" {fn IFNULL(NAME,'                             ')} AS NAME, " + ;
									" {fn IFNULL(RATE1AMT,00000000.00)} AS HOURLY_WAGE " + ;
									" FROM REPORTS.V_EMPLOYEE "
									
*!*										 + ;
*!*										" WHERE COMPANYCODE = 'SXI' "

								IF .lTestMode THEN
									.TrackProgress('lcSQL2 = ' + lcSQL2, LOGIT+SENDIT)
								ENDIF

								IF USED('CURADPWAGES') THEN
									USE IN CURADPWAGES
								ENDIF

								IF .ExecSQL(lcSQL2, 'CURADPWAGES', RETURN_DATA_MANDATORY) THEN


									* Over The Road drivers have $0 hourly wage, use $20 for report purposes per Ken 10/25/06 MB
									*!*										SELECT CURADPWAGES
									*!*										SCAN FOR (DIVISION = '02') AND (HOURLY_WAGE = 0)
									*!*											REPLACE HOURLY_WAGE WITH 20.00, LOTR WITH 1
									*!*										ENDSCAN
									
									
									* calculate hourly rates for non Hourlys in the cursor
									SELECT CURADPWAGES
									SCAN
										DO CASE
											CASE CURADPWAGES.ADP_COMP <> 'SXI'
												* SALARY IS BIWEEKLY, CONVERT TO HOURLY
												REPLACE CURADPWAGES.HOURLY_WAGE WITH ( CURADPWAGES.HOURLY_WAGE / 80 )
											CASE (CURADPWAGES.DIVISION = '02') AND (CURADPWAGES.HOURLY_WAGE = 0)
												* OTR driver, use $20
												REPLACE CURADPWAGES.HOURLY_WAGE WITH 20.00, CURADPWAGES.LOTR WITH 1
											OTHERWISE
												* regular hourly employee - nothing to change
										ENDCASE									
									ENDSCAN

									* now populate hourly_wage in CURACCRUALS
									SELECT CURACCRUALS
									SCAN
										SELECT CURADPWAGES
										LOCATE FOR FILE_NUM = VAL(CURACCRUALS.FILE_NUM)
										IF FOUND() THEN
											REPLACE CURACCRUALS.HOURLY_WAGE WITH CURADPWAGES.HOURLY_WAGE, ;
												CURACCRUALS.IN_ADP WITH 1, CURACCRUALS.lotr WITH CURADPWAGES.LOTR
										ENDIF
									ENDSCAN
									
									* populate NULL senioritydates with Hiredate
									SELECT CURACCRUALS
									SCAN FOR ISNULL(SENIORDATE)
										REPLACE CURACCRUALS.SENIORDATE WITH CURACCRUALS.HIREDATE
									ENDSCAN


								ENDIF  &&  .ExecSQL(lcSQL2, 'CURADPWAGES', RETURN_DATA_MANDATORY)

							ELSE
								* connection error
								.TrackProgress('Unable to connect to ADP Payroll System.', LOGIT+SENDIT)
							ENDIF   &&  .nSQLHandle > 0


							************************************************************************************************
							************************************************************************************************

							WAIT WINDOW NOWAIT "Opening Excel..."
							oExcel = CREATEOBJECT("excel.application")
							oWorkbook = oExcel.workbooks.OPEN("F:\UTIL\KRONOS\TEMPLATES\KronosVacationLiabilityReport.XLS")

							***********************************************************************************************************************
							***********************************************************************************************************************
							WAIT WINDOW NOWAIT "Looking for target directory..."
							* see if target directory exists, and, if not, create it

							lcTargetDirectory = "F:\UTIL\KRONOS\ACCRUALSREPORTS\"
							* for testing
							*lcTargetDirectory = "C:\TIMECLK\ADPFILES\" + lcFileDate + "\"

							* create directory if it doesn't exist
							IF NOT DIRECTORY(lcTargetDirectory) THEN
								MKDIR (lcTargetDirectory)
								WAIT WINDOW TIMEOUT 1 "Made Target Directory " + lcTargetDirectory
							ENDIF

							* if target directory exists, save there
							llSaveAgain = .F.
							IF DIRECTORY(lcTargetDirectory) THEN
								lcFiletoSaveAs = lcTargetDirectory + "Kronos_VACATION_LIABILITY_2008_CODES_" + lcFileDate
								lcXLFileName = lcFiletoSaveAs + ".XLS"
								IF FILE(lcXLFileName) THEN
									DELETE FILE (lcXLFileName)
								ENDIF
								oWorkbook.SAVEAS(lcFiletoSaveAs)
								* set flag to save again after sheet is populated
								llSaveAgain = .T.
							ENDIF
							***********************************************************************************************************************
							***********************************************************************************************************************

							lnRow = 2
							lnStartRow = lnRow + 1
							lcStartRow = ALLTRIM(STR(lnStartRow))

							oExcel.VISIBLE = .F.
							oWorksheet = oWorkbook.Worksheets[1]
							oWorksheet.RANGE("A" + lcStartRow,"V1000").clearcontents()

							oWorksheet.RANGE("A" + lcStartRow,"V1000").FONT.SIZE = 10
							oWorksheet.RANGE("A" + lcStartRow,"V1000").FONT.NAME = "Arial Narrow"
							oWorksheet.RANGE("A" + lcStartRow,"V1000").FONT.bold = .T.

							lcTitle = "Kronos Vacation Liability Balances (TUG) as of: " + DTOC(ldAsOfDate) + LINE_FEED + "For Active Employees"
							oWorksheet.RANGE("A1").VALUE = lcTitle


							* main scan/processing
							SELECT CURACCRUALS
							SCAN FOR IN_ADP = 1

								lnRow = lnRow + 1
								lcRow = LTRIM(STR(lnRow))
								oWorksheet.RANGE("A" + lcRow).VALUE = "'" + ALLTRIM(CURACCRUALS.DIVISION)
								oWorksheet.RANGE("B" + lcRow).VALUE = ALLTRIM(CURACCRUALS.DEPT)
								oWorksheet.RANGE("C" + lcRow).VALUE = ALLTRIM(CURACCRUALS.EMPLOYEE)
								oWorksheet.RANGE("D" + lcRow).VALUE = ALLTRIM(CURACCRUALS.FILE_NUM)
								oWorksheet.RANGE("E" + lcRow).VALUE = ALLTRIM(CURACCRUALS.WORKSITE)
								oWorksheet.RANGE("F" + lcRow).VALUE = CURACCRUALS.SENIORDATE
								oWorksheet.RANGE("G" + lcRow).VALUE = CURACCRUALS.V_ACCRUAL
								oWorksheet.RANGE("H" + lcRow).VALUE = CURACCRUALS.VBE_ACCRUAL
								oWorksheet.RANGE("I" + lcRow).VALUE = "=G" + lcRow + "+H" + lcRow
								oWorksheet.RANGE("J" + lcRow).VALUE = HOURLY_WAGE
								oWorksheet.RANGE("K" + lcRow).VALUE = "=I" + lcRow + "*J" + lcRow
								*oWorksheet.RANGE("L" + lcRow).VALUE = IIF(CURACCRUALS.IN_ADP = 0,"x","")
								IF .lCalcSickBalance THEN
									oWorksheet.RANGE("L" + lcRow).VALUE = CURACCRUALS.S_ACCRUAL
								ENDIF
								
*!*									oWorksheet.RANGE("L" + lcRow).VALUE = CURACCRUALS.V_ACCRUAL
*!*									
*!*									lnAdjustment = ROUND(CURACCRUALS.V_ACCRUAL,0) - CURACCRUALS.V_ACCRUAL
*!*									oWorksheet.RANGE("M" + lcRow).VALUE = lnAdjustment
								

								oWorksheet.RANGE("K" + lcRow,"K" + lcRow).NumberFormat = "$#,##0.00"

								oWorksheet.RANGE("A" + lcRow,"L" + lcRow).Interior.ColorIndex = IIF(((lnRow % 2) = 1),15,0)

							ENDSCAN

							lnEndRow = lnRow
							lcEndRow = ALLTRIM(STR(lnEndRow))

							*oWorksheet.RANGE("A" + lcStartRow,"T" + lcRow).SELECT
							oWorksheet.PageSetup.PrintArea = "$A$"  + lcStartRow + ":$L$" + lcRow

							* save again
							IF llSaveAgain THEN
								oWorkbook.SAVE()
							ENDIF
							oWorkbook.CLOSE()


							.cBodyText = lcTopBodyText + CRLF + CRLF + ;
								"==================================================================================================================" + ;
								CRLF + "<report log follows>" + ;
								CRLF + CRLF + .cBodyText


						ENDIF  && EOF() CURACCRUALS

					ENDIF  &&  .ExecSQL(lcSQL, 'CURWTKEMPLOYEE', RETURN_DATA_MANDATORY)

					IF FILE(lcXLFileName) THEN
						.cAttach = lcXLFileName
						.TrackProgress('Created Vacation Liability File : ' + lcXLFileName, LOGIT+SENDIT)
						.TrackProgress('Kronos Vacation Liability Report process ended normally.', LOGIT+SENDIT+NOWAITIT)
					ELSE
						.TrackProgress('!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!', LOGIT+SENDIT)
						.TrackProgress("ERROR: There was a problem Creating Accruals File : " + lcXLFileName, LOGIT+SENDIT)
						.TrackProgress('!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!', LOGIT+SENDIT)
					ENDIF

					CLOSE DATABASES ALL
				ELSE
					* connection error
					.TrackProgress('Unable to connect to WTK System.', LOGIT+SENDIT)
				ENDIF   &&  .nSQLHandle > 0
			CATCH TO loError

				.TrackProgress('There was an error.',LOGIT+SENDIT)
				.TrackProgress('The Error # is: ' + TRANSFORM(loError.ErrorNo), LOGIT+SENDIT)
				.TrackProgress('The Error Message is: ' + TRANSFORM(loError.MESSAGE), LOGIT+SENDIT)
				.TrackProgress('The Error occurred in Program: ' + TRANSFORM(loError.PROCEDURE), LOGIT+SENDIT)
				.TrackProgress('The Error occurred at line #: ' + TRANSFORM(loError.LINENO), LOGIT+SENDIT)
				lnNumberOfErrors = lnNumberOfErrors + 1
				IF TYPE('oExcel') = 'O' AND NOT ISNULL(oExcel) THEN
					oExcel.QUIT()
				ENDIF
				CLOSE DATA
				CLOSE ALL

			ENDTRY

			CLOSE DATA
			WAIT CLEAR
			***************** INTERNAL email results ******************************
			.TrackProgress('About to send status email.',LOGIT)
			.TrackProgress('==================================================================================================================', SENDIT)
			.TrackProgress('The logfile is: ' + .cLogFile, SENDIT)
			.TrackProgress('Kronos Vacation Liability Report process started: ' + .cStartTime, LOGIT+SENDIT)
			.TrackProgress('Kronos Vacation Liability Report process finished: ' + TTOC(DATETIME()), LOGIT+SENDIT)

			IF .lSendInternalEmailIsOn THEN
				* try to trap error from not having dartmail dll's registered on user's pc...
				TRY
					DO FORM dartmail2 WITH .cSendTo,.cFrom,.cSubject,.cCC,.cAttach,.cBodyText,"A"
					.TrackProgress('Sent status email.',LOGIT)
				CATCH TO loError
					=MESSAGEBOX('There was an error sending the status email. Please contact Mark Bennett.',0+64,"Kronos Vacation Liability Report")
					.TrackProgress('There was an error sending the status email.',LOGIT)
					.TrackProgress('The Error # is: ' + TRANSFORM(loError.ErrorNo), LOGIT)
					.TrackProgress('The Error Message is: ' + TRANSFORM(loError.MESSAGE), LOGIT)
					.TrackProgress('The Error occurred in Program: ' + TRANSFORM(loError.PROCEDURE), LOGIT)
					.TrackProgress('The Error occurred at line #: ' + TRANSFORM(loError.LINENO), LOGIT)
					lnNumberOfErrors = lnNumberOfErrors + 1
					CLOSE DATA
				ENDTRY
			ELSE
				.TrackProgress('Did not send status email: THIS.lSendInternalEmailIsOn = FALSE.',LOGIT)
			ENDIF

		ENDWITH
		RETURN
	ENDFUNC && main


	FUNCTION ExecSQL
		LPARAMETERS tcSQL, tcCursorName, tlNoDataReturnedIsOkay
		LOCAL llRetval, lnResult
		WITH THIS
			* close target cursor if it's open
			IF USED(tcCursorName)
				USE IN (tcCursorName)
			ENDIF
			WAIT WINDOW "Fetching remote data; please wait..." NOWAIT
			lnResult = sqlexec(.nSQLHandle, tcSQL, tcCursorName)
			llRetval = ( lnResult > 0 )
			IF llRetval THEN
				* see if any data came back
				IF NOT tlNoDataReturnedIsOkay THEN
					IF (NOT USED(tcCursorName)) OR EOF(tcCursorName) THEN
						llRetval = .F.
						.TrackProgress('No data was returned by this query: ' +CRLF + tcSQL, LOGIT+SENDIT)
					ENDIF
				ENDIF
			ELSE
				.TrackProgress("Result Code [" + TRANSFORM(lnResult) + "] returned by this query: " + tcSQL, LOGIT+SENDIT)
			ENDIF
			WAIT CLEAR
			RETURN llRetval
		ENDWITH
	ENDFUNC


	PROCEDURE TrackProgress
		* do any combination of Wait Window, writing to logfile, and adding to body of email,
		* based on nFlags parameter.
		LPARAMETERS tcExpression, tnFlags
		WITH THIS
			IF BITAND(tnFlags,LOGIT) = LOGIT THEN
				IF .lLoggingIsOn THEN
					?
					? .cProcessName + "  " + TTOC(DATETIME()) + ": " + tcExpression
				ENDIF
			ENDIF
			IF .lWaitWindowIsOn THEN
				IF BITAND(tnFlags,NOWAITIT) = NOWAITIT THEN
					WAIT WINDOW tcExpression NOWAIT
				ENDIF
				IF BITAND(tnFlags,WAITIT) = WAITIT THEN
					WAIT WINDOW tcExpression TIMEOUT .nWaitWindowTimeout
				ENDIF
			ENDIF
			IF BITAND(tnFlags,SENDIT) = SENDIT THEN
				IF .lSendInternalEmailIsOn THEN
					.cBodyText = .cBodyText + tcExpression + CRLF + CRLF
				ENDIF
			ENDIF
		ENDWITH
	ENDPROC  &&  TrackProgress

ENDDEFINE

