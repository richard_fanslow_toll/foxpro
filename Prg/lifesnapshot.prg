* Daily snapshot file for Lifefactory

utilsetup("LIFESNAPSHOT")

xsqlexec("select * from account where inactive=0","account",,"qq")
index on accountid tag accountid
set order to

goffice="I"

xsqlexec("select * from outship where accountid=6034 and sp=0 and (del_date={} or del_date>{"+dtoc(date()-8)+"})",,,"wh")

xsqlexec("select * from pt where mod='I' and accountid=6034",,,"wh")
xsqlexec("select * from ptsave where ptdate>{"+dtoc(date()-8)+"} and accountid=6034",,,"wh")

*

wait window "step 1" nowait

create cursor xrpt (acctname c(30), qty n(7), xgroup c(1))
index on acctname tag acctname

do case
case inlist(dow(qdate()),1,2,3,4)
	xnext=2
case inlist(dow(qdate()),5,6)
	xnext=4
otherwise
	xnext=3
endcase

xnext5=6

if inlist(dow(qdate()),2,3,4)
	xprev=5
else
	xprev=3
endif

select outship
sum qty to xappt for accountid=6034 and !sp and between(appt,qdate()-10,qdate()+xnext) and !picknpack and emptynul(del_date) and !notonwip
sum qty to xapptstage for accountid=6034 and !sp and between(appt,qdate()-10,qdate()+xnext) and !emptynul(staged) and emptynul(del_date) and !picknpack and emptynul(del_date) and !notonwip
sum qty to xapptpnp for accountid=6034 and !sp and between(appt,qdate()-10,qdate()+xnext) and picknpack and emptynul(del_date) and !notonwip
sum qty to xapptstagepnp for accountid=6034 and !sp and between(appt,qdate()-10,qdate()+xnext) and !emptynul(staged) and emptynul(del_date) and picknpack and emptynul(del_date) and !notonwip

xappttoprocess=xappt-xapptstage
xappttoprocesspnp=xapptpnp-xapptstagepnp

select outship
sum qty to xappt5 for accountid=6034 and !sp and between(appt,qdate()-10,qdate()+xnext5) and !picknpack and emptynul(del_date) and !notonwip
sum qty to xapptstage5 for accountid=6034 and !sp and between(appt,qdate()-10,qdate()+xnext5) and !emptynul(staged) and emptynul(del_date) and !picknpack and emptynul(del_date) and !notonwip
sum qty to xapptpnp5 for accountid=6034 and !sp and between(appt,qdate()-10,qdate()+xnext5) and picknpack and emptynul(del_date) and !notonwip
sum qty to xapptstagepnp5 for accountid=6034 and !sp and between(appt,qdate()-10,qdate()+xnext5) and !emptynul(staged) and emptynul(del_date) and picknpack and emptynul(del_date) and !notonwip

xappttoprocess5=xappt5-xapptstage5
xappttoprocesspnp5=xapptpnp5-xapptstagepnp5

select outship
sum qty to xalloc for accountid=6034 and !sp and emptynul(del_date) and !notonwip and !picknpack
sum qty to xallocstage for accountid=6034 and !sp and !emptynul(staged) and emptynul(del_date) and !notonwip and !picknpack
sum qty to xallocpnp for accountid=6034 and !sp and emptynul(del_date) and !notonwip and picknpack
sum qty to xallocstagepnp for accountid=6034 and !sp and !emptynul(staged) and emptynul(del_date) and !notonwip and picknpack

xalloctoprocess=xalloc-xallocstage
xalloctoprocesspnp=xallocpnp-xallocstagepnp

wait window "step 2" nowait

select outship
sum qty to xshiplast3 for accountid=6034 and !sp and between(del_date,qdate()-xprev,qdate()-1)
xshipavg=round(xshiplast3/3,0)
select outship
sum qty to xshiptoday for accountid=6034 and !sp and appt=qdate() and !notonwip

select pt
sum qty to aa for accountid=6034
if aa>0
	insert into xrpt (acctname, qty) values ("LIFEFACTORY", aa)
endif

*

j=0
for i=1 to 7
	xdate=date()-i+1
	
	j=j+1
	jj=transform(j)
	xptrcv&jj = dtoc(xdate)

	wait window "step 3 day "+jj nowait
	
	select count(qty) as qty, sum(iif(goh,qty,0)) as pnpqty, sum(iif(goh,0,qty)) as preqty ;
		from ptsave ;
		where ptdate=xdate and accountid=6034 ;
		into cursor xptsave

	select count(qty) as qty, sum(iif(goh,qty,0)) as pnpqty, sum(iif(goh,0,qty)) as preqty ;
		from pt ;
		where ptdate=xdate and accountid=6034 ;
		into cursor xpt
	
	xptrcvqty&jj=xptsave.qty+xpt.qty
	xpnpptrcvqty&jj=xptsave.pnpqty+xpt.pnpqty
	xpreptrcvqty&jj=xptsave.preqty+xpt.preqty
next

wait clear

xpnppttotal = xpnpptrcvqty1+xpnpptrcvqty2+xpnpptrcvqty3+xpnpptrcvqty4+xpnpptrcvqty5+xpnpptrcvqty6+xpnpptrcvqty7
xprepttotal = xpreptrcvqty1+xpreptrcvqty2+xpreptrcvqty3+xpreptrcvqty4+xpreptrcvqty5+xpreptrcvqty6+xpreptrcvqty7
xpnpptavg = int(xpnppttotal/7)
xpreptavg = int(xprepttotal/7)

if reccount("xrpt")=0
	append blank in xrpt
endif

xheader = "Lifefactory"

select xrpt	
set step on
rpt2pdf("wipsumm","h:\fox\snapshot.pdf")	

*
	
tattach="h:\fox\snapshot.pdf"
*tsendto="Gina@Lifefactory.com"
tsendto="chris.malcolm@tollgroup.com"
*tsendto="dyoung@fmiint.com"
tcc=""
tFrom ="TGF Operations <fmi-transload-ops@fmiint.com>"
tmessage = "See attached files"
tmessage="Gina@Lifefactory.com"
tSubject = "Daily snapshot report"

Do Form dartmail2 With tsendto,tFrom,tSubject," ",tattach,tmessage,"A"

schedupdate()

_screen.Caption=gscreencaption
on error
