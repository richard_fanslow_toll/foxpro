utilsetup("MJ_INVENTORY_CONTROL")
Set Exclusive Off
Set Deleted On
Set Talk Off
Set Safety Off 
WITH _SCREEN	
	.AUTOCENTER = .T.
	.WINDOWSTATE = 0
	.BORDERSTYLE = 1
	.WIDTH = 320
	.HEIGHT = 210
	.TOP = 290
	.LEFT = 110
	.CLOSABLE = .T.
	.MAXBUTTON = .T.
	.MINBUTTON = .T.
	.SCROLLBARS = 0
	.CAPTION = "MJ_INVENTORY_CONTROL"
ENDWITH	

xsqlexec("select * from inven where mod='J'",,,"wh")
xsqlexec("select * from invenloc where mod='J'",,,"wh")

*!*	****checks for inventory in CUTU
*!*	SELECT ACCOUNTID AS ACCOUNT, STYLE AS STYLE, COLOR AS COLOR, ID AS ID, WHSELOC AS LOCATION, LOCQTY AS QTY,  ADDDT AS DATE_CREATED;
*!*	 FROM invenloc where whseloc='CUT' AND LOCQTY!=0 ORDER BY 1,7 INTO CURSOR CUTU readwrite
*!*	EXPORT TO C:\inventory_in_CUTU XLS

*!*	SELECT cutu
*!*	If Reccount() > 0
*!*	  tsendto = "tmarg@fmiint.com,Cesar.Lozada@tollgroup.com,bill.marsh@tollgroup.com"
*!*	  tattach = "C:\inventory_in_CUTU.XLS"
*!*	  tcc =""
*!*	  tFrom ="Toll EDI System Operations <fmi-transload-ops@fmiint.com>"
*!*	  tmessage = "Inventory exists in CUTU, see attached"
*!*	  tSubject = "Inventory exists in CUTU"
*!*	  Do Form m:\dev\frm\dartmail2 With tsendto,tFrom,tSubject, tcc,tattach,tmessage  ,"A"
*!*	ELSE 
*!*	*!*	  tsendto = "tmarg@fmiint.com"
*!*	*!*	  tattach = ""
*!*	*!*	  tcc =""
*!*	*!*	  tFrom ="Toll EDI System Operations <fmi-transload-ops@fmiint.com>"
*!*	*!*	  tmessage = "NO Inventory exists in CUTU  "+Ttoc(Datetime())
*!*	*!*	  tSubject = "NO Inventory exists in CUTU"
*!*	*!*	  Do Form m:\dev\frm\dartmail2 With tsendto,tFrom,tSubject, tcc,tattach,tmessage  ,"A"
*!*	ENDIF



*!*	****checks for inventory in CYCLEZ
*!*	SELECT ACCOUNTID AS ACCOUNT, STYLE AS STYLE, COLOR AS COLOR, ID AS ID, WHSELOC AS LOCATION, LOCQTY AS QTY,  ADDDT AS DATE_CREATED;
*!*	 FROM invenloc where whseloc='CYCLEZ' AND LOCQTY!=0 ORDER BY 1,6 DESC INTO CURSOR CYCLEZ READWRITE
*!*	EXPORT TO C:\inventory_in_CYCLEZ XLS

*!*	SELECT CYCLEZ
*!*	If Reccount() > 0
*!*	  tsendto = "tmarg@fmiint.com,Cesar.Lozada@tollgroup.com,bill.marsh@tollgroup.com"
*!*	  tattach = "C:\inventory_in_CYCLEZ.XLS"
*!*	  tcc =""
*!*	  tFrom ="Toll EDI System Operations <fmi-transload-ops@fmiint.com>"
*!*	  tmessage = "Inventory exists in CYCLEZ, see attached"
*!*	  tSubject = "Inventory exists in CYCLEZ"
*!*	  Do Form m:\dev\frm\dartmail2 With tsendto,tFrom,tSubject, tcc,tattach,tmessage  ,"A"
*!*	ELSE 
*!*	*!*	  tsendto = "tmarg@fmiint.com"
*!*	*!*	  tattach = ""
*!*	*!*	  tcc =""
*!*	*!*	  tFrom ="Toll EDI System Operations <fmi-transload-ops@fmiint.com>"
*!*	*!*	  tmessage = "NO Inventory exists in CYCLEZ  "+Ttoc(Datetime())
*!*	*!*	  tSubject = "NO Inventory exists in CYCLEZ"
*!*	*!*	  Do Form m:\dev\frm\dartmail2 With tsendto,tFrom,tSubject, tcc,tattach,tmessage  ,"A"
*!*	ENDIF


*!*	****checks for inventory in CYCLEAA or CYCLEBB
*!*	SELECT ACCOUNTID AS ACCOUNT, STYLE AS STYLE, COLOR AS COLOR, ID AS ID, WHSELOC AS LOCATION, LOCQTY AS QTY,  ADDDT AS DATE_CREATED;
*!*	 FROM invenloc where INLIST(whseloc,'CYCLEAA','CYCLEBB') AND LOCQTY!=0 ORDER BY 1,7 INTO CURSOR CYCLEAB readwrite
*!*	EXPORT TO C:\inventory_in_CYCLEAA_BB XLS

*!*	SELECT CYCLEAB
*!*	If Reccount() > 0
*!*	  tsendto = "tmarg@fmiint.com,Cesar.Lozada@tollgroup.com,bill.marsh@tollgroup.com"
*!*	  tattach = "C:\inventory_in_CYCLEAA_BB.XLS"
*!*	  tcc =""
*!*	  tFrom ="Toll EDI System Operations <fmi-transload-ops@fmiint.com>"
*!*	  tmessage = "Inventory exists in CYCLEAA or CYCLEBB, see attached"
*!*	  tSubject = "Inventory exists in CYCLEAA or CYCLEBB"
*!*	  Do Form m:\dev\frm\dartmail2 With tsendto,tFrom,tSubject, tcc,tattach,tmessage  ,"A"
*!*	ELSE 
*!*	*!*	  tsendto = "tmarg@fmiint.com"
*!*	*!*	  tattach = ""
*!*	*!*	  tcc =""
*!*	*!*	  tFrom ="Toll EDI System Operations <fmi-transload-ops@fmiint.com>"
*!*	*!*	  tmessage = "NO Inventory exists in CYCLEAA or CYCLEBB  "+Ttoc(Datetime())
*!*	*!*	  tSubject = "NO Inventory exists in CYCLEAA or CYCLEBB"
*!*	*!*	  Do Form m:\dev\frm\dartmail2 With tsendto,tFrom,tSubject, tcc,tattach,tmessage  ,"A"
*!*	ENDIF


****checks for non retai 6304 inventory in 2AA or 2BB racks
SELECT ACCOUNTID, STYLE,COLOR,ID,LOCQTY, WHSELOC, ADDBY, ADDDT FROM INVENLOC WHERE INLIST(WHSELOC,'2AA','2BB','2CC','2DD') AND !INLIST(ACCOUNTID,6304,6306) AND LOCQTY!=0 ;
AND whseloc!='PL' INTO CURSOR temp22 readwrite
EXPORT TO C:\non_6304_inventory_in_retail_locations XLS

SELECT temp22
If Reccount() > 0
  tsendto = "tmarg@fmiint.com,brian.mcglone@tollgroup.com,bill.marsh@tollgroup.com"
  tattach = "C:\non_6304_inventory_in_retail_locations.XLS"
  tcc =""
  tFrom ="Toll EDI System Operations <fmi-transload-ops@fmiint.com>"
  tmessage = "Non 6304 (Retail) inventory exists in Retail racks (2AA, 2BB...), see attached                    mj_inventory_control"
  tSubject = "Non 6304 (Retail) inventory exists in Retail racks (2AA, 2BB...)"
  Do Form m:\dev\frm\dartmail2 With tsendto,tFrom,tSubject, tcc,tattach,tmessage  ,"A"
ELSE 
*!*	  tsendto = "tmarg@fmiint.com"
*!*	  tattach = ""
*!*	  tcc =""
*!*	  tFrom ="Toll EDI System Operations <fmi-transload-ops@fmiint.com>"
*!*	  tmessage = "NO Non 6304 (Retail) inventory exists in Retail racks (2AA, 2BB)  "+Ttoc(Datetime())
*!*	  tSubject = "NO Non 6304 (Retail) inventory exists in Retail racks (2AA, 2BB)"
*!*	  Do Form m:\dev\frm\dartmail2 With tsendto,tFrom,tSubject, tcc,tattach,tmessage  ,"A"
ENDIF


****checks for retai 6304 inventory not in 2AA or 2BB racks or jewelry locations
*!*	SELECT ACCOUNTID, STYLE,COLOR,ID,LOCQTY, WHSELOC FROM invenloc where accountid=6304 AND whseloc!='2AA' AND whseloc!='2BB'  AND whseloc!='2CC' AND whseloc!='2DD';
*!*	AND whseloc!='CUT'   AND whseloc!='CYC'  AND whseloc!='CART'    AND whseloc!='2HA1'     AND whseloc!='RETA'   AND whseloc!='2NN'    AND whseloc!='2UH'   AND whseloc!='2XD'    AND LOCQTY!=0 AND whseloc!='PL' ;
*!*	  AND whseloc!='2RJ'    AND whseloc!='2RT'      AND !('RACK' $WHSELOC)  AND whseloc!='RLS'  AND whseloc!='2SR'  AND whseloc!='RTLR'     ;
*!*	INTO CURSOR temp23 readwrite
*!*	EXPORT TO C:\6304_inventory_not_in_retail_locations XLS

*!*	SELECT temp23
*!*	If Reccount() > 0
*!*	  tsendto = "tmarg@fmiint.com,brian.mcglone@tollgroup.com,bill.marsh@tollgroup.com"
*!*	  tattach = "C:\6304_inventory_not_in_retail_locations.XLS"
*!*	  tcc =""
*!*	  tFrom ="Toll EDI System Operations <fmi-transload-ops@fmiint.com>"
*!*	  tmessage = "6304 (Retail) inventory exists in non Retail racks (2AA, 2BB...), see attached                mj_inventory_control"
*!*	  tSubject = "6304 (Retail) inventory exists in non Retail racks (2AA, 2BB...)"
*!*	  Do Form m:\dev\frm\dartmail2 With tsendto,tFrom,tSubject, tcc,tattach,tmessage  ,"A"
*!*	ELSE 
*!*	*!*	  tsendto = "tmarg@fmiint.com"
*!*	*!*	  tattach = ""
*!*	*!*	  tcc =""
*!*	*!*	  tFrom ="Toll EDI System Operations <fmi-transload-ops@fmiint.com>"
*!*	*!*	  tmessage = "NO 6304 (Retail) inventory exists in non Retail racks (2AA, 2BB)  "+Ttoc(Datetime())
*!*	*!*	  tSubject = "NO 6304 (Retail) inventory exists in non Retail racks (2AA, 2BB)"
*!*	*!*	  Do Form m:\dev\frm\dartmail2 With tsendto,tFrom,tSubject, tcc,tattach,tmessage  ,"A"
*!*	ENDIF

SET STEP ON 
****identifies negative inventory, most likely cause by incorrect cyclez
SELECT accountid, style, color,id, totqty FROM inven where totqty<0;
order BY 1,5 DESC  INTO CURSOR temp24 readwrite
EXPORT TO C:\mj_negative_inventory XLS

SELECT temp24
If Reccount() > 0
  tsendto = "tmarg@fmiint.com,brian.mcglone@tollgroup.com,bill.marsh@tollgroup.com"
  tattach = "C:\mj_negative_inventory.XLS"
  tcc =""
  tFrom ="Toll EDI System Operations <fmi-transload-ops@fmiint.com>"
  tmessage = "Negative MJ inventory exists"
  tSubject = "Negative MJ inventory exists"
  Do Form m:\dev\frm\dartmail2 With tsendto,tFrom,tSubject, tcc,tattach,tmessage  ,"A"
ELSE 
*!*	  tsendto = "tmarg@fmiint.com"
*!*	  tattach = ""
*!*	  tcc =""
*!*	  tFrom ="Toll EDI System Operations <fmi-transload-ops@fmiint.com>"
*!*	  tmessage = "NO Negative MJ inventory exists  "+Ttoc(Datetime())
*!*	  tSubject = "NO Negative MJ inventory exists"
*!*	  Do Form m:\dev\frm\dartmail2 With tsendto,tFrom,tSubject, tcc,tattach,tmessage  ,"A"
ENDIF


SELECT distinct accountid,whseloc FROM INVENLOC WHERE LOCQTY!=0 AND WHSELOC!='2XD' AND WHSELOC!='CYC' AND WHSELOC!='CUT'  INTO CURSOR t1 READWRITE
select whseloc, count(1) AS CNT from t1 group by 1 INTO CURSOR T2 READWRITE
SELECT * FROM T2 WHERE CNT!=1 INTO CURSOR T3 READWRITE
EXPORT TO c:\multiple_accts_in_location xls
SELECT t3
If Reccount() > 0
  tsendto = "tmarg@fmiint.com,brian.mcglone@tollgroup.com,bill.marsh@tollgroup.com"
  tattach = "c:\multiple_accts_in_location.XLS"
  tcc =""
  tFrom ="Toll EDI System Operations <fmi-transload-ops@fmiint.com>"
  tmessage = "Multiple Accounts exist in one location, please fix     mj_inventory_control"
  tSubject = "MULTIPLE ACCOUNTS EXIST IN 1 LOCATION"
  Do Form m:\dev\frm\dartmail2 With tsendto,tFrom,tSubject, tcc,tattach,tmessage  ,"A"
ELSE 
  tsendto = "tmarg@fmiint.com"
  tattach = ""
  tcc =""
  tFrom ="Toll EDI System Operations <fmi-transload-ops@fmiint.com>"
  tmessage = "NO Negative MJ inventory exists  "+Ttoc(Datetime())
  tSubject = "NO Negative MJ inventory exists"
  Do Form m:\dev\frm\dartmail2 With tsendto,tFrom,tSubject, tcc,tattach,tmessage  ,"A"
ENDIF

Close Data All
schedupdate()
_Screen.Caption=gscreencaption