* LF_DAILY_SHIP_TABLE
*
* 05/19/2015 MB
*
* populates a daily shipment status table used in LF_IBQS_RPT project.
*
* specc'ed by Chris Malcolm 
*
* EXE = F:\UTIL\LF\LF_DAILY_SHIP_TABLE.EXE
*
* Run daily at 8pm?


runack("LF_DAILY_SHIP_TABLE")

goffice="I"

LOCAL lnError, lcProcessName, loLF_DAILY_SHIP_TABLE

DECLARE INTEGER CreateMutex IN Win32API ;
	STRING @lpMutexAttributes, ;
	STRING bInitialOwner, ;
	STRING @lpName

DECLARE INTEGER GetLastError IN Win32API

* if running in EXE check to see if already running
IF _VFP.STARTMODE > 0 THEN
	lcProcessName = "LF_DAILY_SHIP_TABLE"
	CreateMutex(0,CHR(1),@lcProcessName)
	lnError = GetLastError()
	IF lnError = 183
		WAIT WINDOW TIMEOUT 10 "LF_DAILY_SHIP_TABLE..."
		RETURN .F.
	ENDIF
ENDIF

utilsetup("LF_DAILY_SHIP_TABLE")


loLF_DAILY_SHIP_TABLE = CREATEOBJECT('LF_DAILY_SHIP_TABLE')
loLF_DAILY_SHIP_TABLE.MAIN()

schedupdate()

CLOSE DATABASES ALL
RETURN

#DEFINE LOGIT 1
#DEFINE WAITIT 2
#DEFINE NOWAITIT 4
#DEFINE SENDIT 8
#DEFINE CRLF CHR(13) + CHR(10)
#DEFINE LINEFEED CHR(10)
#DEFINE RETURN_DATA_MANDATORY .F.
#DEFINE RETURN_DATA_NOT_MANDATORY .T.
#DEFINE DAILY_OT_MULTIPLIER 1.5
#DEFINE DAILY_OT20_MULTIPLIER 2.0

DEFINE CLASS LF_DAILY_SHIP_TABLE AS CUSTOM

	cProcessName = 'LF_DAILY_SHIP_TABLE'

	lTestMode = .T.  && .F. FOR NORMAL OPERATION, .T. TO SUPPRESS ALL BUT DEVELOPER EMAILS AND CHANGE INPUT TABLE, ETC.

	lAutoYield = .T.

	cStartTime = TTOC(DATETIME())

	* processing properties
	cProcessDesc = "Lifefactory Populate Daily Shipment Table process"
	nAccountID = 6034
	nUnwavedQty = 0
	nShippedQty = 0
	
	* data properties
	cDailyShipTable = 'F:\UTIL\LF\DATA\DAILYSHIP.DBF'


	* date properties
	dtNow = DATETIME()

	dToday = DATE()
	*dToday = {^2017-09-02}

	cToday = DTOC(DATE())

	* wait window properties
	nWaitWindowTimeout = 2
	lWaitWindowIsOn = .T.

	* logfile properties
	lLoggingIsOn = .T.
	cLogFile = 'F:\UTIL\LF\LOGFILES\LF_DAILY_SHIP_TABLE_LOG.TXT'

	* INTERNAL email properties
	lSendInternalEmailIsOn = .T.
	cFrom = 'Mark.Bennett@Tollgroup.com'
	cSendTo = 'mbennett@fmiint.com'
	cCC = ''
	cSubject = ''
	cAttach = ''
	cBodyText = ''
	cTopBodyText = ''

	cCOMPUTERNAME = ''
	cUSERNAME = ''

	FUNCTION INIT
		IF NOT DODEFAULT()
			RETURN .F.
		ENDIF
		WITH THIS
			CLEAR
			*SET RESOURCE OFF
			CLOSE DATA
			SET CENTURY ON
			SET DATE AMERICAN
			SET HOURS TO 24
			SET ANSI ON
			SET TALK OFF
			SET DELETED ON
			SET CONSOLE OFF
			SET EXCLUSIVE OFF
			SET SAFETY OFF
			SET EXACT OFF
			SET STATUS BAR ON
			SET SYSMENU OFF
			SET ENGINEBEHAVIOR 70
			_VFP.AUTOYIELD = .lAutoYield
			.cCOMPUTERNAME = GETENV("COMPUTERNAME")
			.cUSERNAME = GETENV("USERNAME")
			IF .lTestMode THEN
				.cSendTo = 'mbennett@fmiint.com'
				.cCC = ''
				.cLogFile = 'F:\UTIL\LF\LF_DAILY_SHIP_TABLE_LOG_TESTMODE.TXT'
			ENDIF
			.lLoggingIsOn = .lLoggingIsOn AND NOT EMPTY(.cLogFile)
		ENDWITH
	ENDFUNC


	FUNCTION DESTROY
		WITH THIS
			IF .lLoggingIsOn  THEN
				SET ALTERNATE OFF
				SET ALTERNATE TO
			ENDIF
		ENDWITH
		DODEFAULT()
	ENDFUNC


	FUNCTION MAIN

		WITH THIS
			LOCAL lnNumberOfErrors, lnAccountID, ldToday

			TRY
				lnNumberOfErrors = 0

				IF .lLoggingIsOn THEN
					SET ALTERNATE TO (.cLogFile) ADDITIVE
					SET ALTERNATE ON
				ENDIF

				.cSubject = .cProcessDesc + ' for ' + DTOC(.dToday)

				.TrackProgress(.cProcessDesc + ' started....', LOGIT+SENDIT+NOWAITIT)
				.TrackProgress('COMPUTERNAME = ' + .cCOMPUTERNAME, LOGIT+SENDIT)
				.TrackProgress('USERNAME = ' + .cUSERNAME, LOGIT+SENDIT)
				.TrackProgress('PROJECT = LF_DAILY_SHIP_TABLE', LOGIT+SENDIT)
				IF .lTestMode THEN
					.TrackProgress('                        TEST MODE                        ', LOGIT+SENDIT)
				ENDIF

				*OPEN DATABASE F:\WHI\WHDATA\WH

				*USE F:\WHI\WHDATA\PROJECT IN 0 SHARED
				*USE F:\WHI\WHDATA\PROJDET IN 0 SHARED
				*USE F:\WHI\WHDATA\PL IN 0 SHARED
				*USE F:\WHI\WHDATA\OUTWOLOG IN 0 SHARED
				*USE F:\WHI\WHDATA\OUTCOMP IN 0 SHARED
				
*!*					USE F:\WHI\WHDATA\OUTSHIP IN 0 SHARED
*!*					USE F:\WHI\WHDATA\OUTDET IN 0 SHARED

				* revised to use usesqloutwo() 02/13/2017 MB
				xsqlexec("select * from outship where mod='I' and accountid=6034","outship",,"wh")
				xsqlexec("select * from outdet where mod='I' and accountid=6034","outdet",,"wh")
				
				*USE F:\WHI\WHDATA\SHIPMENT IN 0 SHARED
				*USE F:\WHI\WHDATA\COMPHEAD IN 0 SHARED
				*USE F:\WHI\WHDATA\COMPON IN 0 SHARED
				*USE F:\WHI\WHDATA\INVEN IN 0 SHARED
				*USE F:\WHI\WHDATA\INDET IN 0 SHARED
				*USE F:\WHI\WHDATA\INWOLOG IN 0 SHARED
				*USE F:\WH\UPCMAST.DBF IN 0 ALIAS UPCMAST
				*USE F:\WHI\WHDATA\CTNUCC IN 0 SHARED
				
				* revised to use sql shipment table 11/22/2016 MB
				xsqlexec("select * from shipment where mod='I' and accountid=6034","shipment",,"wh")

				USE F:\UTIL\LF\DATA\DAILYSHIP.DBF IN 0 SHARED
				
				ldToday = .dToday
				
				
				*.PopulateShippedQty()
				.PopulateShippedQty( ldToday )
				
				.PopulatePickPackQty()				
				
				.PopulateStatusQty()
				
				.PopulateUnwavedQty()
				
				.PopulateBacklogQty()				

				* REVISED PopulateShippedQty processing 1/19/2016 per Chris Malcolm to also recalc prior 3 days if today is Monday; otherwise just recalc yesterday.
				* This is to correct #s that are not accurate due to late closings.
				IF DOW(ldToday,1) = 2 THEN
					.PopulateShippedQty( ldToday -1 )
					.PopulateShippedQty( ldToday -2 )
					.PopulateShippedQty( ldToday -3 )
				ELSE
					.PopulateShippedQty( ldToday -1 )
				ENDIF

				* FINALLY, enter the datetime() into the updatedt field. 
				* This will let us distinguish between dates that had all zero data vs. dates when the process did not run at all, or errored out and did not run to completion.
				SELECT DAILYSHIP
				LOCATE FOR (SHIPDATE = ldToday)
				IF FOUND() THEN
					REPLACE DAILYSHIP.UPDATEDT WITH DATETIME() IN DAILYSHIP
				ENDIF

				.TrackProgress(.cProcessDesc + ' ended normally....', LOGIT+SENDIT+NOWAITIT)

			CATCH TO loError

				.TrackProgress('There was an error.',LOGIT+SENDIT)
				.TrackProgress('The Error # is: ' + TRANSFORM(loError.ERRORNO), LOGIT+SENDIT)
				.TrackProgress('The Error Message is: ' + TRANSFORM(loError.MESSAGE), LOGIT+SENDIT)
				.TrackProgress('The Error occurred in Program: ' + TRANSFORM(loError.PROCEDURE), LOGIT+SENDIT)
				.TrackProgress('The Error occurred at line #: ' + TRANSFORM(loError.LINENO), LOGIT+SENDIT)
				lnNumberOfErrors = lnNumberOfErrors + 1
				IF TYPE('.oExcel') = 'O' AND NOT ISNULL(.oExcel) THEN
					.oExcel.QUIT()
				ENDIF

			ENDTRY

			WAIT CLEAR
			***************** INTERNAL email results ******************************
			.TrackProgress('About to send status email.',LOGIT)
			.TrackProgress('==================================================================================================================', SENDIT)
			.TrackProgress('The logfile is: ' + .cLogFile, LOGIT+SENDIT)
			.TrackProgress(.cProcessDesc + ' started: ' + .cStartTime, LOGIT+SENDIT)
			.TrackProgress(.cProcessDesc + ' finished: ' + TTOC(DATETIME()), LOGIT+SENDIT)

			IF .lSendInternalEmailIsOn THEN
				* try to trap error from not having dartmail dll's registered on user's pc...
				TRY
					.cBodyText = .cTopBodyText + CRLF + .cBodyText

					DO FORM dartmail2 WITH .cSendTo,.cFrom,.cSubject,.cCC,.cAttach,.cBodyText,"A"
					.TrackProgress('Sent status email.',LOGIT)
				CATCH TO loError
					.TrackProgress('There was an error sending the status email.',LOGIT)
					.TrackProgress('The Error # is: ' + TRANSFORM(loError.ERRORNO), LOGIT)
					.TrackProgress('The Error Message is: ' + TRANSFORM(loError.MESSAGE), LOGIT)
					.TrackProgress('The Error occurred in Program: ' + TRANSFORM(loError.PROCEDURE), LOGIT)
					.TrackProgress('The Error occurred at line #: ' + TRANSFORM(loError.LINENO), LOGIT)
					lnNumberOfErrors = lnNumberOfErrors + 1
				ENDTRY

			ELSE
				.TrackProgress('Did not send status email: THIS.lSendInternalEmailIsOn = FALSE.',LOGIT)
			ENDIF

		ENDWITH
		RETURN
	ENDFUNC && main
	
	
	PROCEDURE PopulateShippedQty
		LPARAMETERS tdDate
		WITH THIS

			.TrackProgress('Getting Shipped Qty for ' + TRANSFORM(tdDate) + '...', LOGIT+SENDIT+NOWAITIT)
			
			LOCAL lnAccountID, ldToday, lnShippedQty
			lnAccountID = .nAccountID
			
			*ldToday = .dToday
			ldToday = tdDate
			
			IF USED('CURLF') THEN
				USE IN CURLF
			ENDIF
			
*!*		********************************
*!*		* activate for testing
*!*		ldToday = {^2015-06-07}
*!*		********************************
			
*!*				SELECT ;
*!*					SUM(C.TOTQTY) AS UNITQTY ;
*!*					FROM OUTSHIP A ;
*!*					INNER JOIN SHIPMENT B ;
*!*					ON B.PICKTICKET = A.SHIP_REF ;
*!*					INNER JOIN OUTDET C ;
*!*					ON C.OUTSHIPID = A.OUTSHIPID ;
*!*					INTO CURSOR CURLF ;
*!*					WHERE A.ACCOUNTID = lnAccountID ;
*!*					AND B.SENT ;
*!*					AND B.SHIPDATE = ldToday ;
*!*					AND B.ACCOUNTID = lnAccountID ;
*!*					AND C.UNITS
			
			* per Chris M. 6/8/2015
			* shipment for wip by pt screen (shipment.frm) is showing data not in shipment table (i.e. can't find the pickticket in shipment) ;
			* try eliminating shipment table and match by del_date instead of shipdate
			SELECT ;
				SUM(C.TOTQTY) AS UNITQTY ;
				FROM OUTSHIP A ;
				INNER JOIN OUTDET C ;
				ON C.OUTSHIPID = A.OUTSHIPID ;
				INTO CURSOR CURLF ;
				WHERE A.ACCOUNTID = lnAccountID ;
				AND A.DEL_DATE = ldToday ;
				AND C.UNITS
			
				
			lnShippedQty = CURLF.UNITQTY	
			.TrackProgress('lnShippedQty = ' + TRANSFORM(lnShippedQty), LOGIT+SENDIT+NOWAITIT)
			
			* STORE AS PROPERTY for use in PopulateBacklogQTY()
			.nShippedQty = lnShippedQty
				
			SELECT DAILYSHIP
			LOCATE FOR (SHIPDATE = ldToday)
			IF NOT FOUND() THEN
				APPEND BLANK
				REPLACE DAILYSHIP.SHIPDATE WITH ldToday IN DAILYSHIP
			ENDIF
			REPLACE DAILYSHIP.DUNITQTY WITH lnShippedQty IN DAILYSHIP
			
			IF USED('CURLF') THEN
				USE IN CURLF
			ENDIF
				
		ENDWITH
		RETURN
	ENDPROC  &&  PopulateShippedQty
		
	
	PROCEDURE PopulatePickPackQty
		WITH THIS

			.TrackProgress('Getting PickPack Qty...', LOGIT+SENDIT+NOWAITIT)
			* per PG, this is a count of totqty from table CARTONS on SQL4, which is where SCANPACKs are logged.
			* also, in outship memo field SHIPINS you can see if it was a SCANPACK or PICKPACK.
			
			LOCAL lnAccountID, ldToday, lnPickPackQty, ldTomorrow, lcSQLToday, lcSQLTomorrow, lcSetDate			
			LOCAL lcSqlPassword, lcDSNLess, lcServer, lnHandle, lcSQLCommand, lnRetVal	
					
			lcSetDate = SET('DATE')
			lnAccountID = .nAccountID
			ldToday = .dToday
			ldTomorrow = ldToday + 1

			SET DATE YMD

			*	 create "YYYYMMDD" safe date format for use in SQL query
			lcSQLToday = STRTRAN(DTOC(ldToday),"/","")
			lcSQLTomorrow = STRTRAN(DTOC(ldTomorrow),"/","")
			
			SET DATE &lcSetDate

			.TrackProgress('lcSQLToday = ' + lcSQLToday, LOGIT+SENDIT)
			.TrackProgress('lcSQLTomorrow = ' + lcSQLTomorrow, LOGIT+SENDIT)
			
			IF USED('CURLF') THEN
				USE IN CURLF
			ENDIF
						
			USE F:\WH\SQLPASSWORD IN 0
			lcSqlPassword = ALLTRIM(sqlpassword.PASSWORD)
			USE IN sqlpassword

			lcServer = "tgfnjsql01"
			SQLSETPROP(0,'DispLogin',3)
			SQLSETPROP(0,"dispwarnings",.F.)

			lcDSNLess = "driver=SQL Server;server=&lcServer;uid=SA;pwd=&lcSqlPassword;DATABASE=PICKPACK"
			lnHandle = SQLSTRINGCONNECT(m.lcDSNLess,.T.)
			IF lnHandle <= 0 THEN
				.TrackProgress('ERROR: could not connect to tgfnjsql01', LOGIT+SENDIT+NOWAITIT)
			ENDIF
			
			lcSQLCommand = "SELECT SUM(TOTQTY) AS UNITQTY FROM CARTONS WHERE (ACCOUNTID = 6034) AND (INSDTTM >= '" + lcSQLToday + "') AND (INSDTTM < '" + lcSQLTomorrow + "')"
			.TrackProgress('lcSQLCommand = ' + lcSQLCommand, LOGIT+SENDIT)

			lnRetVal = SQLEXEC(lnHandle, lcSQLCommand, "CURLF")
			
			IF lnRetVal < 0 then
				.TrackProgress('ERROR returned by SQLEXEC on this command: ' + lcSQLCommand, LOGIT+SENDIT)
			ELSE
				* OKAY

				*SELECT CURLF
				*BROWSE
					
				lnPickPackQty = CURLF.UNITQTY
				
				IF ISNULL(lnPickPackQty) THEN
					lnPickPackQty = 0
				ENDIF
					
				.TrackProgress('lnPickPackQty = ' + TRANSFORM(lnPickPackQty), LOGIT+SENDIT+NOWAITIT)
					
				SELECT DAILYSHIP
				LOCATE FOR (SHIPDATE = ldToday)
				IF FOUND() THEN
					REPLACE DAILYSHIP.PUNITQTY WITH lnPickPackQty IN DAILYSHIP
				ENDIF
			
			ENDIF
			
			IF USED('CURLF') THEN
				USE IN CURLF
			ENDIF
			
			=SQLDISCONNECT(lnHandle)
				
		ENDWITH
		RETURN
	ENDPROC  && PopulatePickPackQty
		
	
	PROCEDURE PopulateStatusQty
		WITH THIS
			* TOTAL UNIT QTY FOR STATUS = LABELED/STAGED/LOADED IN WIP
			.TrackProgress('Getting Status Qty...', LOGIT+SENDIT+NOWAITIT)

			LOCAL lnAccountID, ldToday, lnStatusQty
			lnAccountID = .nAccountID
			ldToday = .dToday

			IF USED('CURORDERWIP') THEN
				USE IN CURORDERWIP
			ENDIF

			************************************************************************************************************
			* CODE BELOW IS BASED ON LIFEWIP.PRG by Darren

			SELECT ship_ref, SPACE(15) AS STATUS, wo_num, wo_date, consignee, cnee_ref, ship_via, ctnqty, qty AS unitqty, ;
				START, CANCEL, called, appt, appt_time, appt_num, apptremarks, del_date, pulled, picked, labeled, staged, truckloaddt, ptdate ;
				FROM outship ;
				WHERE (ACCOUNTID = lnAccountID) AND (EMPTYnul(del_date) OR del_date>DATE()-30) AND !notonwip ;
				INTO CURSOR CURORDERWIP READWRITE

			SELECT CURORDERWIP

			REPLACE ALL CURORDERWIP.STATUS WITH ;
				IIF(!pulled,"NOT PULLED",IIF(!EMPTYnul(del_date),"DELIVERED",IIF(!EMPTYnul(truckloaddt),"LOADED",IIF(!EMPTYnul(staged),"STAGED",IIF(!EMPTYnul(labeled),"LABELED",IIF(!EMPTYnul(picked),"PICKED","PULLED")))))) ;
				IN CURORDERWIP
			************************************************************************************************************				

			lnStatusQty = 0
			SELECT CURORDERWIP
			SCAN
				IF INLIST(ALLTRIM(CURORDERWIP.STATUS),"LABELED","STAGED","LOADED") THEN
					lnStatusQty = lnStatusQty + CURORDERWIP.unitqty
				ENDIF
			ENDSCAN

			.TrackProgress('lnStatusQty = ' + TRANSFORM(lnStatusQty), LOGIT+SENDIT+NOWAITIT)

			SELECT DAILYSHIP
			LOCATE FOR (SHIPDATE = ldToday)
			IF FOUND() THEN
				REPLACE DAILYSHIP.LSUNITQTY WITH lnStatusQty IN DAILYSHIP
			ENDIF
			
			IF USED('CURORDERWIP') THEN
				USE IN CURORDERWIP
			ENDIF

		ENDWITH
		RETURN
	ENDPROC  && PopulateStatusQty
		
	
	PROCEDURE PopulateUnwavedQty
		WITH THIS
			* TOTAL UNIT QTY FOR UNALLOCATED PTs, PER CHRIS M.
			* THIS IS JUST THE SUM OF THE UNIT QTYS IN PTDET, SINCE ANYTHING THAT IS ALLOCATED IS MOVED OUT OF PT INTO OUTSHIP (ACCORDING TO MVW)
			.TrackProgress('Getting Unwaved Qty...', LOGIT+SENDIT+NOWAITIT)
			
			LOCAL lnAccountID, ldToday, lnUnwavedQty
			lnAccountID = .nAccountID
			ldToday = .dToday
			
			IF USED('CURLF') THEN
				USE IN CURLF
			ENDIF
			
			xsqlexec("select totqty from ptdet where accountid="+transform(lnaccountid)+" and units=1",,,"wh")
			select sum(totqty) as unitqty from ptdet into cursor curlf
				
			lnUnwavedQty = CURLF.UNITQTY	
			.TrackProgress('lnUnwavedQty = ' + TRANSFORM(lnUnwavedQty), LOGIT+SENDIT+NOWAITIT)
			
			* store to a property because PopulateBacklogQty() will also use the #
			.nUnwavedQty = lnUnwavedQty
				
			SELECT DAILYSHIP
			LOCATE FOR (SHIPDATE = ldToday)
			IF FOUND() THEN
				REPLACE DAILYSHIP.UWUNITQTY WITH lnUnwavedQty IN DAILYSHIP
			ENDIF
			
			IF USED('CURLF') THEN
				USE IN CURLF
			ENDIF
				
		ENDWITH
		RETURN
	ENDPROC  && PopulateUnwavedQty
	

		
	
	PROCEDURE PopulateBacklogQty
		WITH THIS
			* TOTAL UNIT QTY FOR NOT SP AND (STATUS <> delivered) IN WIP  + OPEN PICK TICKETS (I.E. THE UNWAVED QTY which was calculated earlier in this process)
			.TrackProgress('Getting Backlog Qty...', LOGIT+SENDIT+NOWAITIT)

			LOCAL lnAccountID, ldToday, lnBacklogQty, lnPerCentShipped
			lnAccountID = .nAccountID
			ldToday = .dToday

			IF USED('CURORDERWIP') THEN
				USE IN CURORDERWIP
			ENDIF

			************************************************************************************************************
			* CODE BELOW IS BASED ON LIFEWIP.PRG by Darren

			SELECT ship_ref, SPACE(15) AS STATUS, wo_num, wo_date, consignee, cnee_ref, ship_via, ctnqty, qty AS unitqty, ;
				START, CANCEL, called, appt, appt_time, appt_num, apptremarks, del_date, pulled, picked, labeled, staged, truckloaddt, ptdate ;
				FROM outship ;
				WHERE (ACCOUNTID = lnAccountID) AND (EMPTYnul(del_date) OR del_date>DATE()-30) AND !notonwip AND (NOT SP) ;
				INTO CURSOR CURORDERWIP READWRITE

			SELECT CURORDERWIP

			REPLACE ALL CURORDERWIP.STATUS WITH ;
				IIF(!pulled,"NOT PULLED",IIF(!EMPTYnul(del_date),"DELIVERED",IIF(!EMPTYnul(truckloaddt),"LOADED",IIF(!EMPTYnul(staged),"STAGED",IIF(!EMPTYnul(labeled),"LABELED",IIF(!EMPTYnul(picked),"PICKED","PULLED")))))) ;
				IN CURORDERWIP
			************************************************************************************************************				

			lnBacklogQty = 0
			SELECT CURORDERWIP
			SCAN
				IF NOT (ALLTRIM(CURORDERWIP.STATUS) == "DELIVERED") THEN
					lnBacklogQty = lnBacklogQty + CURORDERWIP.unitqty
				ENDIF
			ENDSCAN
			
			.TrackProgress('lnBacklogQty = ' + TRANSFORM(lnBacklogQty), LOGIT+SENDIT+NOWAITIT)
			
			* ADD OPEN PICK TICKETS (UNWAVED QTY)
			lnBacklogQty = lnBacklogQty + .nUnwavedQty

			.TrackProgress('lnBacklogQty + .nUnwavedQty = ' + TRANSFORM(lnBacklogQty), LOGIT+SENDIT+NOWAITIT)
			
			* calc % shipped vs backlog; .nShippedQty was calced prior to here
			lnPerCentShipped = 0
			IF lnBacklogQty > 0 THEN
				lnPerCentShipped = (.nShippedQty / lnBacklogQty) * 100
			ENDIF			

			.TrackProgress('lnPerCentShipped = ' + TRANSFORM(lnPerCentShipped), LOGIT+SENDIT+NOWAITIT)

			SELECT DAILYSHIP
			LOCATE FOR (SHIPDATE = ldToday)
			IF FOUND() THEN
				REPLACE DAILYSHIP.BLOGUNITS WITH lnBacklogQty, DAILYSHIP.PCENTSHPD WITH lnPerCentShipped IN DAILYSHIP
			ENDIF
			
			IF USED('CURORDERWIP') THEN
				USE IN CURORDERWIP
			ENDIF

		ENDWITH
		RETURN
	ENDPROC  && PopulateBacklogQty
		
	
	PROCEDURE TrackProgress
		* do any combination of Wait Window, writing to logfile, and adding to body of email,
		* based on nFlags parameter.
		LPARAMETERS tcExpression, tnFlags
		WITH THIS
			IF BITAND(tnFlags,LOGIT) = LOGIT THEN
				IF .lLoggingIsOn THEN
					?
					? .cProcessName + "  " + TTOC(DATETIME()) + ": " + tcExpression
				ENDIF
			ENDIF
			IF .lWaitWindowIsOn THEN
				IF BITAND(tnFlags,NOWAITIT) = NOWAITIT THEN
					WAIT WINDOW tcExpression NOWAIT
				ENDIF
				IF BITAND(tnFlags,WAITIT) = WAITIT THEN
					WAIT WINDOW tcExpression TIMEOUT .nWaitWindowTimeout
				ENDIF
			ENDIF
			IF BITAND(tnFlags,SENDIT) = SENDIT THEN
				IF .lSendInternalEmailIsOn THEN
					.cBodyText = .cBodyText + tcExpression + CRLF + CRLF
				ENDIF
			ENDIF
		ENDWITH
	ENDPROC  &&  TrackProgress
	

	PROCEDURE InitializeShipDates
		WITH THIS
		
			USE (.cDailyShipTable) IN 0 ALIAS SHIPTABLE
			
			LOCAL ldDate
			ldDate = {^2015-04-30}			
			
			DO WHILE ldDate < {^2021-01-01}
				ldDate = ldDate + 1	
				SELECT SHIPTABLE
				APPEND BLANK
				REPLACE SHIPTABLE.SHIPDATE WITH ldDate
				WAIT WINDOW NOWAIT TRANSFORM(ldDate)
			ENDDO
			
			WAIT CLEAR
			
			SELECT SHIPTABLE
			BROWSE
			
			USE IN SHIPTABLE
			
		ENDWITH
	ENDPROC  &&  TrackProgress


ENDDEFINE
