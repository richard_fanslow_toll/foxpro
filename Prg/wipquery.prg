xxrptfilter = ""
if xintransit
	xxrptfilter=" status='In Transit'"
endif
if xnotpulled
	xxrptfilter=xxrptfilter+iif(empty(xxrptfilter),""," or")+" status='Not Pulled'"
endif
if xpulled
	xxrptfilter=xxrptfilter+iif(empty(xxrptfilter),""," or")+" status='Pulled'"
endif
if xpicked
	xxrptfilter=xxrptfilter+iif(empty(xxrptfilter),""," or")+" status='Picked'"
endif
if xlabeled
	xxrptfilter=xxrptfilter+iif(empty(xxrptfilter),""," or")+" status='Labeled'"
endif
if xstaged
	xxrptfilter=xxrptfilter+iif(empty(xxrptfilter),""," or")+" status='Staged'"
endif
if xloaded
	xxrptfilter=xxrptfilter+iif(empty(xxrptfilter),""," or")+" status='Loaded'"
endif
if xdeliv
	xxrptfilter=xxrptfilter+iif(empty(xxrptfilter),""," or")+" status='Shipped/Delivered'"
endif


**San Pedro
use (wf("C",xaccountid)+"outship") in 0
use (wf("C",xaccountid)+"pt") in 0

**if multiple pos, separated by commas
xcpofilter=iif(empty(xcustpo), '', 'inlist(cnee_ref')
if !empty(xcustpo)
	xpo=xcustpo
	for i = 0 to occurs(",",xcustpo)
		xcpofilter=xcpofilter+',"'+padr(iif(at(",",xpo)=0,xpo,left(xpo,at(",",xpo)-1)),len(outship.cnee_ref))+'"'
		xpo=right(xpo,len(xpo)-at(",",xpo))
	endfor
	xcpofilter=xcpofilter+')'
endif

xupofilter=iif(empty(xuapo), '', 'inlist(ship_ref')
if !empty(xuapo)
	xpo=xuapo
	for i = 0 to occurs(",",xuapo)
		*changed to allow for partials... needed for pos that come in twice ("4500134960" & "4500134960^1") - mvw 03/29/10
*		xupofilter=xupofilter+',"'+padr(iif(at(",",xpo)=0,xpo,left(xpo,at(",",xpo)-1)),len(outship.ship_ref))+'"'
		xupofilter=xupofilter+',"'+alltrim(iif(at(",",xpo)=0,xpo,left(xpo,at(",",xpo)-1)))+'"'
		xpo=right(xpo,len(xpo)-at(",",xpo))
	endfor
	xupofilter=xupofilter+')'
endif
**end po separation

**if enter a cust po or ua po, ignore date range filter
xfilter=iif(empty(xstartdt) or !empty(xcustpo) or !empty(xuapo), '', ' and between(start,{'+dtoc(xstartdt)+'},{'+dtoc(xenddt)+'})')
xfilter = xfilter+iif(empty(xcustpo),'',' and '+xcpofilter)
xfilter = xfilter+iif(empty(xuapo),'',' and '+xupofilter)

select "C" as office, space(20) as status, {} as eta, * from outship ;
	where accountid=xaccountid &xfilter &xcneefilter and !notonwip into cursor xrpt readwrite

if empty(xxrptfilter) or xnotpulled
	select xrpt
	scatter memvar blank

	select "C" as office, padr("Not Pulled",20) as status, qty as ctnqty, * from pt ;
		where accountid=xaccountid &xfilter &xcneefilter and !finis into cursor xtemp
	scan
		scatter memvar
		m.eta=ctod(getmemodata("shipins","ETA"))
		insert into xrpt from memvar
	endscan
	use in xtemp
endif

use in pt
use in outship
set database to wh
close database


**Rialto
use (wf("R",xaccountid)+"outship") in 0
use (wf("R",xaccountid)+"pt") in 0

select xrpt
scatter memvar blank

select "R" as office, space(20) as status, * from outship ;
	where accountid=xaccountid &xfilter &xcneefilter and !notonwip into cursor xtemp
scan
	scatter memvar
	insert into xrpt from memvar
endscan

if empty(xxrptfilter) or xnotpulled
	select xrpt
	scatter memvar blank

	select "R" as office, padr("Not Pulled",20) as status, qty as ctnqty, * from pt ;
		where accountid=xaccountid &xfilter &xcneefilter and !finis into cursor xtemp
	scan
		scatter memvar
		m.eta=ctod(getmemodata("shipins","ETA"))
		insert into xrpt from memvar
	endscan
endif

use in xtemp

use in pt
use in outship
set database to wh
close database


**Miami
use (wf("M",xaccountid)+"outship") in 0
use (wf("M",xaccountid)+"pt") in 0

select xrpt
scatter memvar blank

select "M" as office, space(20) as status, * from outship ;
	where accountid=xaccountid &xfilter &xcneefilter and !notonwip into cursor xtemp
scan
	scatter memvar
	insert into xrpt from memvar
endscan

if empty(xxrptfilter) or xnotpulled
	select xrpt
	scatter memvar blank

	select "M" as office, padr("Not Pulled",20) as status, qty as ctnqty, * from pt ;
		where accountid=xaccountid &xfilter &xcneefilter and !finis into cursor xtemp
	scan
		scatter memvar
		m.eta=ctod(getmemodata("shipins","ETA"))
		insert into xrpt from memvar
	endscan
endif

use in xtemp

use in pt
use in outship
set database to wh
close database


**Carteret
use (wf("N",xaccountid)+"outship") in 0
use (wf("N",xaccountid)+"pt") in 0

select xrpt
scatter memvar blank

select "N" as office, space(20) as status, * from outship ;
	where accountid=xaccountid &xfilter &xcneefilter and !notonwip into cursor xtemp
scan
	scatter memvar
	insert into xrpt from memvar
endscan

if empty(xxrptfilter) or xnotpulled
	select xrpt
	scatter memvar blank

	select "N" as office, padr("Not Pulled",20) as status, qty as ctnqty, * from pt ;
		where accountid=xaccountid &xfilter &xcneefilter and !finis into cursor xtemp
	scan
		scatter memvar
		m.eta=ctod(getmemodata("shipins","ETA"))
		insert into xrpt from memvar
	endscan
endif

use in xtemp

use in pt
use in outship
set database to wh
close database

select xrpt
**blank memvar vars for shiphdr insert
scatter memvar blank

if empty(xxrptfilter) or xintransit
	use f:\underarmour\data\shiphdr in 0

	xfilter=iif(empty(xstartdt), '', ' and between(start,{'+dtoc(xstartdt)+'},{'+dtoc(xenddt)+'})')
	xfilter = xfilter+iif(empty(xcustpo),'',' and style = "'+padr(xcustpo,len(shiphdr.style))+'"')
	xfilter = xfilter+iif(empty(xuapo),'',' and po_num = "'+padr(xuapo,len(shiphdr.po_num))+'"')

	xcneefilter=strtran(xcneefilter,"inlist(consignee","inlist(whdelloc")

	select whdelloc as consignee, po_num as ship_ref, style as cnee_ref, padr("In Transit",20) as status, pl_qty as ctnqty, * from shiphdr ;
		where empty(wo_num) &xfilter &xcneefilter into cursor xtemp
	scan
		scatter memvar
		insert into xrpt from memvar
	endscan
	use in xtemp

	use in shiphdr
endif

select xrpt
scan for empty(status)
	do case
	case !pulled
		xstatus="Not Pulled"
	case !emptynul(del_date)
		xstatus="Shipped/Delivered"
	case !emptynul(truckloaddt)
		xstatus="Loaded"
	case !emptynul(staged)
		xstatus="Staged"
	case !emptynul(labeled)
		xstatus="Labeled"
	case !emptynul(picked)
		xstatus="Picked"
	otherwise
		xstatus="Pulled"
	endcase

	replace status with xstatus
endscan

xxrptfilter=iif(empty(xxrptfilter), ".t.", xxrptfilter)
select * from xrpt where &xxrptfilter order by start, ship_ref into cursor xrpt
