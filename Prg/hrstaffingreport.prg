* Trending report for Ken K. by month
* needs headcounts for each div/dept for each month; will need this to come from check info history table in ADP
* EXE = F:\UTIL\HR\HRSTAFFINGREPORT\HRSTAFFINGREPORT.EXE

LOCAL lTestMode
lTestMode = .T.

runack("HRSTAFFINGREPORT")

IF NOT lTestMode THEN
	utilsetup("HRSTAFFINGREPORT")
ENDIF

DECLARE INTEGER CreateMutex IN Win32API ;
	STRING @lpMutexAttributes, ;
	STRING bInitialOwner, ;
	STRING @lpName

DECLARE INTEGER GetLastError IN Win32API

* if running in EXE check to see if already running
IF _VFP.STARTMODE > 0 THEN
	lcProcessName = "HRSTAFFINGREPORT"
	CreateMutex(0,CHR(1),@lcProcessName)
	lnError = GetLastError()
	IF lnError = 183
		WAIT WINDOW TIMEOUT 5 "This program is already running..."
		IF NOT lTestMode THEN
			schedupdate()
		ENDIF
		RETURN .F.
	ENDIF
ENDIF


loHRSTAFFINGREPORT = CREATEOBJECT('HRSTAFFINGREPORT')
loHRSTAFFINGREPORT.MAIN( lTestMode )

IF NOT lTestMode THEN
	schedupdate()
ENDIF


CLOSE DATABASES ALL
RETURN

#DEFINE LOGIT 1
#DEFINE WAITIT 2
#DEFINE NOWAITIT 4
#DEFINE SENDIT 8
#DEFINE CRLF CHR(13) + CHR(10)
#DEFINE RETURN_DATA_MANDATORY .F.
#DEFINE RETURN_DATA_NOT_MANDATORY .T.
#DEFINE CSVFORMAT 6
#DEFINE WRITE_ONLY_UNBUFFERED 11
#DEFINE COMMA ","
#DEFINE xlEdgeBottom 9
#DEFINE xlContinuous 1
#DEFINE xlMedium -4138

DEFINE CLASS HRSTAFFINGREPORT AS CUSTOM

	cProcessName = 'HRSTAFFINGREPORT'

	lTestMode = .T.  && .F. FOR NORMAL OPERATION, .T. TO SUPPRESS ALL BUT DEVELOPER EMAILS AND CHANGE INPUT TABLE, ETC.

	dToday = DATE()

	lAutoYield = .T.

	cStartTime = TTOC(DATETIME())
	
	nSQLHandle = 0

	* Excel properties
	oExcel = NULL
	oWorkbook = NULL

	* wait window properties
	nWaitWindowTimeout = 2
	lWaitWindowIsOn = .T.

	* logfile properties
	lLoggingIsOn = .T.
	cLogFile = 'F:\UTIL\HR\HRSTAFFINGREPORT\LOGFILES\HRSTAFFINGREPORT_log.txt'

	* INTERNAL email properties
	lSendInternalEmailIsOn = .T.
	cFrom = 'TGF Corporate <fmicorporate@fmiint.com>'
	cSendTo = 'mbennett@fmiint.com'
	cCC = ''
	cSubject = 'Staff Trending report for ' + TRANSFORM(DATETIME())
	cAttach = ''
	cBodyText = ''

	cCOMPUTERNAME = ''
	cUSERNAME = ''


	FUNCTION INIT
		IF NOT DODEFAULT()
			RETURN .F.
		ENDIF
		WITH THIS
			*SET RESOURCE OFF
			CLOSE DATA
			SET CENTURY ON
			*SET DATE AMERICAN
			*SET HOURS TO 24
			SET DECIMALS TO 0
			SET ANSI ON
			SET TALK OFF
			SET DELETED ON
			SET CONSOLE OFF
			SET EXCLUSIVE OFF
			SET SAFETY OFF
			SET EXACT OFF
			SET STATUS BAR ON
			SET SYSMENU OFF
			SET ENGINEBEHAVIOR 70
			SET MULTILOCKS ON
			_VFP.AUTOYIELD = .lAutoYield
			.cCOMPUTERNAME = GETENV("COMPUTERNAME")
			.cUSERNAME = GETENV("USERNAME")
			.lLoggingIsOn = .lLoggingIsOn AND NOT EMPTY(.cLogFile)
		ENDWITH
	ENDFUNC


	FUNCTION DESTROY
		WITH THIS
			IF .lLoggingIsOn  THEN
				SET ALTERNATE OFF
				SET ALTERNATE TO
			ENDIF
		ENDWITH
		IF TYPE('.oExcel') = 'O' AND NOT ISNULL(.oExcel) THEN
			.oExcel.QUIT()
		ENDIF
		DODEFAULT()
	ENDFUNC


	FUNCTION MAIN
		LPARAMETERS tlTestMode
		WITH THIS
			LOCAL lnNumberOfErrors, lnAccountID, lnNumFiles1, lnNumFiles2, laFiles[1,5]
			LOCAL ldADPFromDate, lcADPFromDate, ldPayDate, lcPayDate, lnMonth, lnPrevMonth
			LOCAL lnHeadCnt, lnWorksheet, lcPayDateList, lcSQLADP
			LOCAL lcInputSheet, lcOutputSheet, lnRow, lcRow, loWorksheet, lcDateColumns, lcColumn
			LOCAL lnColumnOffset, lnHeadCount
			LOCAL lnWorkSheetNumber, lcTotalsRow, lcFirstRow
			LOCAL lcOpersTotalsRow, lcGATotalsRow
			
			* starting DATE, will work forward
			ldADPFromDate = {^2012-05-01}

			TRY
				lnNumberOfErrors = 0

				.lTestMode = tlTestMode

				IF .lTestMode THEN
					.cLogFile = 'F:\UTIL\HR\HRSTAFFINGREPORT\LOGFILES\HRSTAFFINGREPORT_log_TESTMODE.txt'
					.cSendTo = 'mbennett@fmiint.com'
				ENDIF
				IF .lLoggingIsOn THEN
					SET ALTERNATE TO (.cLogFile) ADDITIVE
					SET ALTERNATE ON
				ENDIF

				.cStartTime = TTOC(DATETIME())

				.TrackProgress('Staff Trending process started....', LOGIT+SENDIT)
				.TrackProgress('COMPUTERNAME = ' + .cCOMPUTERNAME, LOGIT+SENDIT)
				.TrackProgress('USERNAME = ' + .cUSERNAME, LOGIT+SENDIT)
				.TrackProgress('PROJECT = HRSTAFFINGREPORT', LOGIT+SENDIT)
				IF .lTestMode THEN
					.TrackProgress('                        TEST MODE                        ', LOGIT+SENDIT)
				ENDIF
				
				* OPEN Excel
				.oExcel = CREATEOBJECT("excel.application")
				.oExcel.displayalerts = .F.
				.oExcel.VISIBLE = .F.

				lcADPFromDate = "DATE'" + TRANSFORM(YEAR(ldADPFromDate)) + "-" + PADL(MONTH(ldADPFromDate),2,'0') + "-" + PADL(DAY(ldADPFromDate),2,'0') + "'"
				*lcADPToDate = "DATE'" + TRANSFORM(YEAR(ldADPToDate)) + "-" + PADL(MONTH(ldADPToDate),2,'0') + "-" + PADL(DAY(ldADPToDate),2,'0') + "'"

				.nSQLHandle = SQLCONNECT("PCPAYWIN","MBENNETT","VP737A")

				IF .nSQLHandle > 0 THEN

					IF USED('CURPAYDATES') THEN
						USE IN CURPAYDATES
					ENDIF

					SET TEXTMERGE ON
					TEXT TO	lcSQLPAYDATES NOSHOW
SELECT
DISTINCT
CHECKVIEWPAYDATE AS PAYDATE,
'N' AS GOODDATA,
0000 AS HEADCNT
FROM REPORTS.V_CHK_VW_INFO 
WHERE CHECKVIEWPAYDATE >= <<lcADPFromDate>>
ORDER BY CHECKVIEWPAYDATE
					ENDTEXT
					SET TEXTMERGE OFF

					.TrackProgress('lcSQLPAYDATES =' + lcSQLPAYDATES, LOGIT+SENDIT)

					IF .ExecSQL(lcSQLPAYDATES, 'CURPAYDATES', RETURN_DATA_NOT_MANDATORY) THEN
					
						IF USED('CURPAYDATES') AND NOT EOF('CURPAYDATES') THEN

				
*!*								SELECT CURPAYDATES
*!*								BROWSE
							
							* WE NEED TO LOOP THRU CURPAYDATES AND MARK EARLIEST FRIDAY PAYDATE (TO EXCLUDE BONUS PAYDATES)
							* FOR EACH MONTH WITH BOTH E87 AND E88 DATA AND ??TOTAL HEADCOUNT > 700?? AS GOODDATA = 'Y'
							
							SELECT CURPAYDATES
							LOCATE
							DO WHILE NOT EOF('CURPAYDATES')
							
								ldPayDate = CURPAYDATES.PAYDATE
								IF DOW(ldPayDate,1) = 6 then
									* its's a friday, continue
									lcPayDate = "DATE'" + TRANSFORM(YEAR(ldPayDate)) + "-" + PADL(MONTH(ldPayDate),2,'0') + "-" + PADL(DAY(ldPayDate),2,'0') + "'"
									WAIT WINDOW NOWAIT 'lcPayDate = ' + lcPayDate
								
					SET TEXTMERGE ON
					TEXT TO	lcSQLPAYDATES2 NOSHOW
SELECT
DISTINCT NAME
FROM REPORTS.V_CHK_VW_INFO 
WHERE CHECKVIEWPAYDATE = <<lcPayDate>>
AND COMPANYCODE = 'E88'
					ENDTEXT
					SET TEXTMERGE OFF
					
									IF USED('CURTESTDATE') THEN
										USE IN CURTESTDATE
									ENDIF
								
									IF .ExecSQL(lcSQLPAYDATES2, 'CURTESTDATE', RETURN_DATA_MANDATORY) THEN
										* MARK GOODDATA = 'Y' AND SKIP UNTIL MONTH CHANGES
										lnHeadCnt = RECCOUNT('CURTESTDATE')
										REPLACE CURPAYDATES.GOODDATA WITH 'Y', CURPAYDATES.HEADCNT WITH lnHeadCnt IN CURPAYDATES
										lnPrevMonth = MONTH(ldPayDate)
										DO WHILE NOT EOF('CURPAYDATES')
											SKIP IN CURPAYDATES									
											lnMonth = MONTH(CURPAYDATES.PAYDATE)
											IF lnMonth <> lnPrevMonth THEN
												EXIT
											ENDIF
										ENDDO  &&  WHILE NOT EOF('CURPAYDATES')
									ELSE
										*WAIT WINDOW 'NO SALARIED FOR ' + lcPayDate
										SKIP IN CURPAYDATES
									ENDIF  &&  .ExecSQL
								ELSE
									* not a friday
									SKIP IN CURPAYDATES
								ENDIF
								
							ENDDO  &&  WHILE NOT EOF('CURPAYDATES')
							
							* now select data for all the dates we want into one big  cursor which
							* we can slice and dice any way we want
							* first build string to select dates
							lcPayDateList = ''
							SELECT CURPAYDATES
							SCAN FOR GOODDATA = 'Y'
								ldPayDate = CURPAYDATES.PAYDATE
								lcPayDateList = lcPayDateList + "DATE'" + TRANSFORM(YEAR(ldPayDate)) + "-" + PADL(MONTH(ldPayDate),2,'0') + "-" + PADL(DAY(ldPayDate),2,'0') + "',"							
							ENDSCAN
							* remove final comma
							lcPayDateList = LEFT(lcPayDateList,LEN(lcPayDateList)-1)
							
					SET TEXTMERGE ON
					TEXT TO	lcSQLADP NOSHOW
SELECT
DISTINCT
A.CHECKVIEWPAYDATE AS PAYDATE,
A.NAME, 
'                              ' AS MASTERCOMP, 
{fn LEFT(A.CHECKVIEWHOMEDEPT,2)} AS DIVISION, 
{fn SUBSTRING(A.CHECKVIEWHOMEDEPT,3,4)} AS DEPT, 
B.DESCRIPTION AS DEPTDESC 
FROM REPORTS.V_CHK_VW_INFO A, 
REPORTS.V_DEPARTMENT B 
WHERE A.CHECKVIEWPAYDATE IN (<<lcPayDateList>>)
AND B.COMPANYCODE(+) = A.COMPANYCODE 
AND B.DEPARTMENT(+) = A.CHECKVIEWHOMEDEPT
ORDER BY 1, 5
					ENDTEXT
					SET TEXTMERGE OFF
							
							.TrackProgress('lcSQLADP =' + lcSQLADP, LOGIT+SENDIT)
							
							IF USED('CURALLDATAPRE') THEN
								USE IN CURALLDATAPRE
							ENDIF
							IF USED('CURALLDATA') THEN
								USE IN CURALLDATA
							ENDIF
									
							IF .ExecSQL(lcSQLADP, 'CURALLDATAPRE', RETURN_DATA_MANDATORY) THEN
							
							
								SET PROCEDURE TO VALIDATIONS
								SELECT CURALLDATAPRE
								REPLACE ALL CURALLDATAPRE.MASTERCOMP WITH GetCompanyFromStringDivision(DIVISION) IN CURALLDATAPRE
								
								* JOIN WITH DEPTRANS TABLE TO GET KEN'S STAFFING DEPARTMENT DESCRIPTIONS
								SELECT A.*, B.STAFFDEPT ;
									FROM CURALLDATAPRE A ;
									LEFT OUTER JOIN ;
									F:\UTIL\HR\HRSTAFFINGREPORT\DATA\DEPTRANS B ;
									ON B.DEPT = A.DEPT ;
									INTO CURSOR CURALLDATA ;
									ORDER BY A.PAYDATE, A.DEPT ;
									READWRITE


								* ADDED FOR REPORT FOR GEORGE G.
								IF USED('CURDIVSUMMARY') THEN
									USE IN CURDIVSUMMARY
								ENDIF
								IF USED('CURALLDIVS') THEN
									USE IN CURALLDIVS
								ENDIF

								SELECT DISTINCT DIVISION ;
									FROM CURALLDATA ;
									INTO CURSOR CURALLDIVS;
									ORDER BY DIVISION

								SELECT PAYDATE, DIVISION, COUNT(*) AS HEADCOUNT ;
									FROM CURALLDATA ;
									INTO CURSOR CURDIVSUMMARY ;
									GROUP BY PAYDATE, DIVISION ;
									ORDER BY PAYDATE, DIVISION
	
*!*	SELECT CURDIVSUMMARY 
*!*	COPY TO C:\A\HRSTAFFDIVS.XLS XL5

*!*	THROW

*!*	SELECT CURALLDIVS
*!*	BROWSE
*!*	THROW


								lcInputSheet = 'F:\UTIL\HR\HRSTAFFINGREPORT\TEMPLATES\HRSTAFFINGREPORTTEMPLATE.XLS'
								lcOutputSheet = 'F:\UTIL\HR\HRSTAFFINGREPORT\REPORTS\STAFF_TRENDING_' + DTOS(DATE()) + '.XLS'
								.oWorkbook = .oExcel.workbooks.OPEN(lcInputSheet)
								
								.oWorkbook.SAVEAS(lcOutputSheet)



								* DIVISION TRENDING FOR GEORGE G.
								loWorksheet = .oWorkbook.Worksheets[1]								

								loWorksheet.RANGE("A1").VALUE = 'Division Trending'
								
								
								lcDateColumns = 'BCDEFGHIJKLMNOPQRSTUVWXYZ'
								
								lnRow = 3
								SELECT CURALLDIVS
								SCAN
									lnRow = lnRow + 1									
									lcRow = ALLTRIM(STR(lnRow))
									loWorksheet.RANGE("A"+lcRow).VALUE = "'" + CURALLDIVS.DIVISION
								
									lnColumnOffset = 0
									SELECT CURPAYDATES
									SCAN FOR GOODDATA = 'Y'	
										lnColumnOffset = lnColumnOffset + 1									
										lcColumn = SUBSTR(lcDateColumns,lnColumnOffset,1)
										loWorksheet.RANGE(lcColumn + "2").VALUE = "'" + LEFT(CMONTH(CURPAYDATES.PAYDATE),3) + ' ' + TRANSFORM(YEAR(CURPAYDATES.PAYDATE))
										
										SELECT CURDIVSUMMARY
										LOCATE FOR (PAYDATE = CURPAYDATES.PAYDATE) ;
											AND (DIVISION = CURALLDIVS.DIVISION)
										
										IF FOUND() THEN
											loWorksheet.RANGE(lcColumn + lcRow).VALUE = CURDIVSUMMARY.HEADCOUNT	
										ELSE
											loWorksheet.RANGE(lcColumn + lcRow).VALUE = 0	
										ENDIF
										
									ENDSCAN  && CURPAYDATES
								
								ENDSCAN  &&  CURALLDIVS
								
								
								
								
								
*!*									* make Operations section summary...
*!*									lnRow = 3
*!*									lcFirstRow = ALLTRIM(STR(lnRow + 1))
*!*									SELECT CURALLSTAFFDEPT
*!*									SCAN FOR UPPER(LEFT(STAFFDEPT,5)) = 'OPERS'
*!*										lnRow = lnRow + 1									
*!*										lcRow = ALLTRIM(STR(lnRow))
*!*										loWorksheet.RANGE("A"+lcRow).VALUE = CURALLSTAFFDEPT.STAFFDEPT
*!*										
*!*										lnColumnOffset = 0
*!*										SELECT CURPAYDATES
*!*										SCAN FOR GOODDATA = 'Y'	
*!*											lnColumnOffset = lnColumnOffset + 1									
*!*											lcColumn = SUBSTR(lcDateColumns,lnColumnOffset,1)
*!*											loWorksheet.RANGE(lcColumn + "2").VALUE = "'" + LEFT(CMONTH(CURPAYDATES.PAYDATE),3) + ' ' + TRANSFORM(YEAR(CURPAYDATES.PAYDATE))
*!*											
*!*											SELECT CURALLDATA
*!*											COUNT FOR (MASTERCOMP = CURALLMASTERCOMP.MASTERCOMP) ;
*!*												AND (STAFFDEPT = CURALLSTAFFDEPT.STAFFDEPT) ;
*!*												AND (PAYDATE = CURPAYDATES.PAYDATE) ;
*!*												TO lnHeadCount
*!*												
*!*											loWorksheet.RANGE(lcColumn + lcRow).VALUE = lnHeadCount										
*!*											
*!*										ENDSCAN  && CURPAYDATES
*!*										
*!*										SELECT CURALLSTAFFDEPT									
*!*									ENDSCAN &&  FOR UPPER(LEFT(STAFFDEPT)) = 'OPERS'



								************************************************
								* DETAIL LIST OF EMPLOYEES
								loWorksheet = .oWorkbook.Worksheets[2]
								
								lnRow = 3
								SELECT CURALLDATA
								SCAN
									lnRow = lnRow + 1									
									lcRow = ALLTRIM(STR(lnRow))
									
									WAIT WINDOW NOWAIT 'Processing Detail Sheet Row ' + lcRow
									
									loWorksheet.RANGE("A"+lcRow).VALUE = CURALLDATA.PAYDATE
									loWorksheet.RANGE("B"+lcRow).VALUE = CURALLDATA.NAME
									loWorksheet.RANGE("C"+lcRow).VALUE = CURALLDATA.MASTERCOMP
									loWorksheet.RANGE("D"+lcRow).VALUE = CURALLDATA.DIVISION
									loWorksheet.RANGE("E"+lcRow).VALUE = CURALLDATA.DEPT
									loWorksheet.RANGE("F"+lcRow).VALUE = CURALLDATA.DEPTDESC
									loWorksheet.RANGE("G"+lcRow).VALUE = CURALLDATA.STAFFDEPT
								ENDSCAN
								
								WAIT CLEAR	
								
								* get master list of all 'master companies'
								IF USED('CURALLMASTERCOMP') THEN
									USE IN CURALLMASTERCOMP
								ENDIF
								
								SELECT DISTINCT MASTERCOMP ;
									FROM CURALLDATA ;
									INTO CURSOR CURALLMASTERCOMP ;
									ORDER BY 1
									
								* GET MASTER LIST OF ALL 'STAFF DEPT'
								IF USED('CURALLSTAFFDEPT') THEN
									USE IN CURALLSTAFFDEPT
								ENDIF
								
								SELECT DISTINCT STAFFDEPT ;
									FROM CURALLDATA ;
									INTO CURSOR CURALLSTAFFDEPT ;
									ORDER BY 1
									
								* get master list of all depts
								IF USED('CURALLDEPTS') THEN
									USE IN CURALLDEPTS
								ENDIF
								
								* NOTE NOT GROUPING BY DEPTDESC -- WE CAN TAKE ANY THAT MATCH DEPT
								* THIS IMPROPER GROUPING SQL IS NOT ALLOWED FOR LATER VERSIONS OF VFP ENGINE, BUT IS USEFUL HERE!
								SELECT DEPT, DEPTDESC ;
									FROM CURALLDATA ;
									INTO CURSOR CURALLDEPTS ;
									GROUP BY DEPT ;
									ORDER BY DEPT
							
								*SELECT CURALLDEPTS
								*BROWSE
								
								* WORKSHEET 2 -- TRENDING FOR ALL COMPANIES
								loWorksheet = .oWorkbook.Worksheets[3]
								
								lcDateColumns = 'CDEFGHIJKLMNOPQRSTUVWXYZ'
								
								* make Operations section summary...
								lnRow = 3
								lcFirstRow = ALLTRIM(STR(lnRow + 1))
								SELECT CURALLSTAFFDEPT
								SCAN FOR UPPER(LEFT(STAFFDEPT,5)) = 'OPERS'
									lnRow = lnRow + 1									
									lcRow = ALLTRIM(STR(lnRow))
									loWorksheet.RANGE("A"+lcRow).VALUE = CURALLSTAFFDEPT.STAFFDEPT
									
									lnColumnOffset = 0
									SELECT CURPAYDATES
									SCAN FOR GOODDATA = 'Y'	
										lnColumnOffset = lnColumnOffset + 1									
										lcColumn = SUBSTR(lcDateColumns,lnColumnOffset,1)
										loWorksheet.RANGE(lcColumn + "2").VALUE = "'" + LEFT(CMONTH(CURPAYDATES.PAYDATE),3) + ' ' + TRANSFORM(YEAR(CURPAYDATES.PAYDATE))
										
										SELECT CURALLDATA
										COUNT FOR (STAFFDEPT = CURALLSTAFFDEPT.STAFFDEPT) ;
											AND (PAYDATE = CURPAYDATES.PAYDATE) ;
											TO lnHeadCount
											
										loWorksheet.RANGE(lcColumn + lcRow).VALUE = lnHeadCount										
										
									ENDSCAN  && CURPAYDATES
									
									SELECT CURALLSTAFFDEPT									
								ENDSCAN &&  FOR UPPER(LEFT(STAFFDEPT)) = 'OPERS'

								* add Totals
								lcTotalsRow = ALLTRIM(STR(lnRow + 1))
								lcOpersTotalsRow = lcTotalsRow
								loWorksheet.RANGE("A"+lcTotalsRow).VALUE = '     Total Operations:'
								lnColumnOffset = 0
								SELECT CURPAYDATES
								SCAN FOR GOODDATA = 'Y'	
									lnColumnOffset = lnColumnOffset + 1									
									lcColumn = SUBSTR(lcDateColumns,lnColumnOffset,1)
									loWorksheet.RANGE(lcColumn + lcTotalsRow).formula = "=SUM(" + lcColumn + lcFirstRow + ":" + lcColumn + lcRow +")"
								ENDSCAN  && CURPAYDATES

								* UNDERLINE cell columns to be totaled...
								loWorksheet.RANGE("C" + lcRow + ":" + lcColumn + lcRow).BORDERS(xlEdgeBottom).LineStyle = xlContinuous
								loWorksheet.RANGE("C" + lcRow + ":" + lcColumn + lcRow).BORDERS(xlEdgeBottom).Weight = xlMedium
								* Bold totals line...
								loWorksheet.RANGE("A" + lcTotalsRow,lcColumn + lcTotalsRow).FONT.bold = .T.

								
								* make G&A section summary...
								lnRow = lnRow + 2
								lcFirstRow = ALLTRIM(STR(lnRow + 1))
								SELECT CURALLSTAFFDEPT
								SCAN FOR UPPER(LEFT(STAFFDEPT,3)) = 'G&A'
									lnRow = lnRow + 1									
									lcRow = ALLTRIM(STR(lnRow))
									loWorksheet.RANGE("A"+lcRow).VALUE = CURALLSTAFFDEPT.STAFFDEPT
									
									lnColumnOffset = 0
									SELECT CURPAYDATES
									SCAN FOR GOODDATA = 'Y'	
										lnColumnOffset = lnColumnOffset + 1									
										lcColumn = SUBSTR(lcDateColumns,lnColumnOffset,1)
										
										SELECT CURALLDATA
										COUNT FOR (STAFFDEPT = CURALLSTAFFDEPT.STAFFDEPT) ;
											AND (PAYDATE = CURPAYDATES.PAYDATE) ;
											TO lnHeadCount
											
										loWorksheet.RANGE(lcColumn + lcRow).VALUE = lnHeadCount										
										
									ENDSCAN  && CURPAYDATES
									
									SELECT CURALLSTAFFDEPT									
								ENDSCAN &&  FOR UPPER(LEFT(STAFFDEPT,3)) = 'G&A'

								* add Totals
								lcTotalsRow = ALLTRIM(STR(lnRow + 1))
								lcGATotalsRow = lcTotalsRow
								loWorksheet.RANGE("A"+lcTotalsRow).VALUE = '     Total G&A:'
								lnColumnOffset = 0
								SELECT CURPAYDATES
								SCAN FOR GOODDATA = 'Y'	
									lnColumnOffset = lnColumnOffset + 1									
									lcColumn = SUBSTR(lcDateColumns,lnColumnOffset,1)
									loWorksheet.RANGE(lcColumn + lcTotalsRow).formula = "=SUM(" + lcColumn + lcFirstRow + ":" + lcColumn + lcRow +")"
								ENDSCAN  && CURPAYDATES

								* UNDERLINE cell columns to be totaled...
								loWorksheet.RANGE("C" + lcRow + ":" + lcColumn + lcRow).BORDERS(xlEdgeBottom).LineStyle = xlContinuous
								loWorksheet.RANGE("C" + lcRow + ":" + lcColumn + lcRow).BORDERS(xlEdgeBottom).Weight = xlMedium
								* Bold totals line...
								loWorksheet.RANGE("A" + lcTotalsRow,lcColumn + lcTotalsRow).FONT.bold = .T.
								
								* grand totals of Opers and G&A
								lnRow = lnRow + 2
								lcTotalsRow = ALLTRIM(STR(lnRow + 1))
								loWorksheet.RANGE("A"+lcTotalsRow).VALUE = '     Total All Companies:'
								lnColumnOffset = 0
								SELECT CURPAYDATES
								SCAN FOR GOODDATA = 'Y'	
									lnColumnOffset = lnColumnOffset + 1									
									lcColumn = SUBSTR(lcDateColumns,lnColumnOffset,1)
									loWorksheet.RANGE(lcColumn + lcTotalsRow).formula = "=(" + lcColumn + lcOpersTotalsRow + "+" + lcColumn + lcGATotalsRow +")"
								ENDSCAN  && CURPAYDATES
								* Bold grand totals line...
								loWorksheet.RANGE("A" + lcTotalsRow,lcColumn + lcTotalsRow).FONT.bold = .T.
								
								* make detail section (actually summary by dept)
								lnRow = lnRow + 3
								lcRow = ALLTRIM(STR(lnRow))
								loWorksheet.RANGE("A"+lcRow).VALUE = 'P/R DESCRIPTION'
								loWorksheet.RANGE("B"+lcRow).VALUE = 'P/R CODE'
								loWorksheet.RANGE("A"+lcRow,"B"+lcRow).FONT.bold = .T.
								
								*lnRow = 3
								SELECT CURALLDEPTS
								SCAN
									lnRow = lnRow + 1									
									lcRow = ALLTRIM(STR(lnRow))
									loWorksheet.RANGE("A"+lcRow).VALUE = CURALLDEPTS.DEPTDESC
									loWorksheet.RANGE("B"+lcRow).VALUE = CURALLDEPTS.DEPT								
									
									lnColumnOffset = 0
									SELECT CURPAYDATES
									SCAN FOR GOODDATA = 'Y'	
										lnColumnOffset = lnColumnOffset + 1									
										lcColumn = SUBSTR(lcDateColumns,lnColumnOffset,1)
										loWorksheet.RANGE(lcColumn + "2").VALUE = "'" + LEFT(CMONTH(CURPAYDATES.PAYDATE),3) + ' ' + TRANSFORM(YEAR(CURPAYDATES.PAYDATE))
										
										SELECT CURALLDATA
										COUNT FOR (DEPT = CURALLDEPTS.DEPT) ;
											AND (PAYDATE = CURPAYDATES.PAYDATE) ;
											TO lnHeadCount
											
										loWorksheet.RANGE(lcColumn + lcRow).VALUE = lnHeadCount										
										
									ENDSCAN  && CURPAYDATES
									
									SELECT CURALLDEPTS									
								ENDSCAN  && CURALLDEPTS

								* WORKSHEETS 3+ -- EACH HAS TRENDING FOR ONE MASTER COMPANY
								lnWorkSheetNumber = 3
								SELECT CURALLMASTERCOMP
								SCAN
									lnWorkSheetNumber = lnWorkSheetNumber + 1
									
									WAIT WINDOW NOWAIT 'Processing Worksheet for: ' + CURALLMASTERCOMP.MASTERCOMP
									
									loWorksheet = .oWorkbook.Worksheets[lnWorkSheetNumber]								

									loWorksheet.RANGE("A1").VALUE = 'Staffing Report for: ' + CURALLMASTERCOMP.MASTERCOMP
									* RENAME THE WORKSHEET TAB
									loWorksheet.Name = ALLTRIM(CURALLMASTERCOMP.MASTERCOMP)
									
									* make Operations section summary...
									lnRow = 3
									lcFirstRow = ALLTRIM(STR(lnRow + 1))
									SELECT CURALLSTAFFDEPT
									SCAN FOR UPPER(LEFT(STAFFDEPT,5)) = 'OPERS'
										lnRow = lnRow + 1									
										lcRow = ALLTRIM(STR(lnRow))
										loWorksheet.RANGE("A"+lcRow).VALUE = CURALLSTAFFDEPT.STAFFDEPT
										
										lnColumnOffset = 0
										SELECT CURPAYDATES
										SCAN FOR GOODDATA = 'Y'	
											lnColumnOffset = lnColumnOffset + 1									
											lcColumn = SUBSTR(lcDateColumns,lnColumnOffset,1)
											loWorksheet.RANGE(lcColumn + "2").VALUE = "'" + LEFT(CMONTH(CURPAYDATES.PAYDATE),3) + ' ' + TRANSFORM(YEAR(CURPAYDATES.PAYDATE))
											
											SELECT CURALLDATA
											COUNT FOR (MASTERCOMP = CURALLMASTERCOMP.MASTERCOMP) ;
												AND (STAFFDEPT = CURALLSTAFFDEPT.STAFFDEPT) ;
												AND (PAYDATE = CURPAYDATES.PAYDATE) ;
												TO lnHeadCount
												
											loWorksheet.RANGE(lcColumn + lcRow).VALUE = lnHeadCount										
											
										ENDSCAN  && CURPAYDATES
										
										SELECT CURALLSTAFFDEPT									
									ENDSCAN &&  FOR UPPER(LEFT(STAFFDEPT)) = 'OPERS'

									* add Totals
									lcTotalsRow = ALLTRIM(STR(lnRow + 1))
									lcOpersTotalsRow = lcTotalsRow
									loWorksheet.RANGE("A"+lcTotalsRow).VALUE = '     Total Operations:'
									lnColumnOffset = 0
									SELECT CURPAYDATES
									SCAN FOR GOODDATA = 'Y'	
										lnColumnOffset = lnColumnOffset + 1									
										lcColumn = SUBSTR(lcDateColumns,lnColumnOffset,1)
										loWorksheet.RANGE(lcColumn + lcTotalsRow).formula = "=SUM(" + lcColumn + lcFirstRow + ":" + lcColumn + lcRow +")"
									ENDSCAN  && CURPAYDATES

									* UNDERLINE cell columns to be totaled...
									loWorksheet.RANGE("C" + lcRow + ":" + lcColumn + lcRow).BORDERS(xlEdgeBottom).LineStyle = xlContinuous
									loWorksheet.RANGE("C" + lcRow + ":" + lcColumn + lcRow).BORDERS(xlEdgeBottom).Weight = xlMedium
									* Bold totals line...
									loWorksheet.RANGE("A" + lcTotalsRow,lcColumn + lcTotalsRow).FONT.bold = .T.
									
									* make G&A section summary...
									lnRow = lnRow + 2
									lcFirstRow = ALLTRIM(STR(lnRow + 1))
									SELECT CURALLSTAFFDEPT
									SCAN FOR UPPER(LEFT(STAFFDEPT,3)) = 'G&A'
										lnRow = lnRow + 1									
										lcRow = ALLTRIM(STR(lnRow))
										loWorksheet.RANGE("A"+lcRow).VALUE = CURALLSTAFFDEPT.STAFFDEPT
										
										lnColumnOffset = 0
										SELECT CURPAYDATES
										SCAN FOR GOODDATA = 'Y'	
											lnColumnOffset = lnColumnOffset + 1									
											lcColumn = SUBSTR(lcDateColumns,lnColumnOffset,1)
											
											SELECT CURALLDATA
											COUNT FOR (MASTERCOMP = CURALLMASTERCOMP.MASTERCOMP) ;
												AND (STAFFDEPT = CURALLSTAFFDEPT.STAFFDEPT) ;
												AND (PAYDATE = CURPAYDATES.PAYDATE) ;
												TO lnHeadCount
												
											loWorksheet.RANGE(lcColumn + lcRow).VALUE = lnHeadCount										
											
										ENDSCAN  && CURPAYDATES
										
										SELECT CURALLSTAFFDEPT									
									ENDSCAN &&  FOR UPPER(LEFT(STAFFDEPT,3)) = 'G&A'

									* add Totals
									lcTotalsRow = ALLTRIM(STR(lnRow + 1))
									lcGATotalsRow = lcTotalsRow
									loWorksheet.RANGE("A"+lcTotalsRow).VALUE = '     Total G&A:'
									lnColumnOffset = 0
									SELECT CURPAYDATES
									SCAN FOR GOODDATA = 'Y'	
										lnColumnOffset = lnColumnOffset + 1									
										lcColumn = SUBSTR(lcDateColumns,lnColumnOffset,1)
										loWorksheet.RANGE(lcColumn + lcTotalsRow).formula = "=SUM(" + lcColumn + lcFirstRow + ":" + lcColumn + lcRow +")"
									ENDSCAN  && CURPAYDATES

									* UNDERLINE cell columns to be totaled...
									loWorksheet.RANGE("C" + lcRow + ":" + lcColumn + lcRow).BORDERS(xlEdgeBottom).LineStyle = xlContinuous
									loWorksheet.RANGE("C" + lcRow + ":" + lcColumn + lcRow).BORDERS(xlEdgeBottom).Weight = xlMedium
									* Bold totals line...
									loWorksheet.RANGE("A" + lcTotalsRow,lcColumn + lcTotalsRow).FONT.bold = .T.
									
									
									* grand totals of Opers and G&A
									lnRow = lnRow + 2
									lcTotalsRow = ALLTRIM(STR(lnRow + 1))
									loWorksheet.RANGE("A"+lcTotalsRow).VALUE = '     Total Company:'
									lnColumnOffset = 0
									SELECT CURPAYDATES
									SCAN FOR GOODDATA = 'Y'	
										lnColumnOffset = lnColumnOffset + 1									
										lcColumn = SUBSTR(lcDateColumns,lnColumnOffset,1)
										loWorksheet.RANGE(lcColumn + lcTotalsRow).formula = "=(" + lcColumn + lcOpersTotalsRow + "+" + lcColumn + lcGATotalsRow +")"
									ENDSCAN  && CURPAYDATES
									* Bold grand totals line...
									loWorksheet.RANGE("A" + lcTotalsRow,lcColumn + lcTotalsRow).FONT.bold = .T.
									
									
									* make detail section (actually summary by dept)
									lnRow = lnRow + 3
									lcRow = ALLTRIM(STR(lnRow))
									loWorksheet.RANGE("A"+lcRow).VALUE = 'P/R DESCRIPTION'
									loWorksheet.RANGE("B"+lcRow).VALUE = 'P/R CODE'
									loWorksheet.RANGE("A"+lcRow,"B"+lcRow).FONT.bold = .T.
									
									SELECT CURALLDEPTS
									SCAN
										lnRow = lnRow + 1									
										lcRow = ALLTRIM(STR(lnRow))
										loWorksheet.RANGE("A"+lcRow).VALUE = CURALLDEPTS.DEPTDESC
										loWorksheet.RANGE("B"+lcRow).VALUE = CURALLDEPTS.DEPT								
										
										lnColumnOffset = 0
										SELECT CURPAYDATES
										SCAN FOR GOODDATA = 'Y'	
											lnColumnOffset = lnColumnOffset + 1									
											lcColumn = SUBSTR(lcDateColumns,lnColumnOffset,1)
											loWorksheet.RANGE(lcColumn + "2").VALUE = "'" + LEFT(CMONTH(CURPAYDATES.PAYDATE),3) + ' ' + TRANSFORM(YEAR(CURPAYDATES.PAYDATE))
											
											SELECT CURALLDATA
											COUNT FOR (MASTERCOMP = CURALLMASTERCOMP.MASTERCOMP) ;
												AND (DEPT = CURALLDEPTS.DEPT) ;
												AND (PAYDATE = CURPAYDATES.PAYDATE) ;
												TO lnHeadCount
												
											loWorksheet.RANGE(lcColumn + lcRow).VALUE = lnHeadCount										
											
										ENDSCAN  && CURPAYDATES
										
										SELECT CURALLDEPTS									
									ENDSCAN  && CURALLDEPTS
								
								ENDSCAN  &&  CURALLMASTERCOMP
														
								* SAVE AND QUIT EXCEL
								.oWorkbook.SAVE()
								.oExcel.QUIT()

							ELSE
								.TrackProgress('!!ERROR retrieving CURALLDATA!!', LOGIT+SENDIT)
							ENDIF  &&  .ExecSQL(lcSQLADP, 'CURALLDATA', RETURN_DATA_MANDATORY)
								
						ELSE

							.TrackProgress('!!No data found!!', LOGIT+SENDIT)
							
						ENDIF

					ENDIF  &&  .ExecSQL

					SQLDISCONNECT(.nSQLHandle)

					*CLOSE DATABASES ALL
				ELSE
					* connection error
					.TrackProgress('Unable to connect to ADP System.', LOGIT+SENDIT)
				ENDIF   &&  .nSQLHandle > 0
				

				CLOSE DATABASES ALL				

				.TrackProgress('Staff Trending process ended normally.', LOGIT+SENDIT)

				IF FILE(lcOutputSheet) THEN
					* attach output file to email
					.cAttach = lcOutputSheet
					.cBodyText = "See attached report."  + ;
						CRLF + CRLF + "<report log follows>" + ;
						CRLF + .cBodyText
				ELSE
					.TrackProgress('ERROR attaching spreadsheet: ' + lcOutputSheet, LOGIT+SENDIT)
				ENDIF

			CATCH TO loError

				.TrackProgress('There was an error.',LOGIT+SENDIT)
				.TrackProgress('The Error # is: ' + TRANSFORM(loError.ERRORNO), LOGIT+SENDIT)
				.TrackProgress('The Error Message is: ' + TRANSFORM(loError.MESSAGE), LOGIT+SENDIT)
				.TrackProgress('The Error occurred in Program: ' + TRANSFORM(loError.PROCEDURE), LOGIT+SENDIT)
				.TrackProgress('The Error occurred at line #: ' + TRANSFORM(loError.LINENO), LOGIT+SENDIT)
				lnNumberOfErrors = lnNumberOfErrors + 1
				.lSendInternalEmailIsOn = .T.
				IF TYPE('.oExcel') = 'O' AND NOT ISNULL(.oExcel) THEN
					.oExcel.QUIT()
				ENDIF

			ENDTRY

			WAIT CLEAR
			***************** INTERNAL email results ******************************
			.TrackProgress('About to send status email.',LOGIT+SENDIT)
			.TrackProgress('The logfile is: ' + .cLogFile, LOGIT+SENDIT)
			.TrackProgress('Staff Trending process started: ' + .cStartTime, LOGIT+SENDIT)
			.TrackProgress('Staff Trending process finished: ' + TTOC(DATETIME()), LOGIT+SENDIT)

			IF .lSendInternalEmailIsOn THEN
				DO FORM dartmail2 WITH .cSendTo,.cFrom,.cSubject,.cCC,.cAttach,.cBodyText,"A"
				.TrackProgress('Sent status email.',LOGIT)
			ELSE
				.TrackProgress('Did not send status email: THIS.lSendInternalEmailIsOn = FALSE.',LOGIT+SENDIT)
			ENDIF

		ENDWITH
		RETURN
	ENDFUNC && main
	
	
	FUNCTION ExecSQL
		LPARAMETERS tcSQL, tcCursorName, tlNoDataReturnedIsOkay
		LOCAL llRetval, lnResult
		WITH THIS
			* close target cursor if it's open
			IF USED(tcCursorName)
				USE IN (tcCursorName)
			ENDIF
			WAIT WINDOW "Fetching remote data; please wait..." NOWAIT
			lnResult = SQLEXEC(.nSQLHandle, tcSQL, tcCursorName)
			llRetval = ( lnResult > 0 )
			IF llRetval THEN
				* see if any data came back
				IF NOT tlNoDataReturnedIsOkay THEN
					IF (NOT USED(tcCursorName)) OR EOF(tcCursorName) THEN
						llRetval = .F.
						.TrackProgress('No data was returned by this query: ' +CRLF + tcSQL, LOGIT+SENDIT)
						.cSendTo = 'Mark Bennett <mbennett@fmiint.com>'
					ENDIF
				ENDIF
			ELSE
				.TrackProgress("Result Code [" + TRANSFORM(lnResult) + "] returned by this query: " + tcSQL, LOGIT+SENDIT)
				.cSendTo = 'Mark Bennett <mbennett@fmiint.com>'
			ENDIF
			WAIT CLEAR
			RETURN llRetval
		ENDWITH
	ENDFUNC


	PROCEDURE TrackProgress
		* do any combination of Wait Window, writing to logfile, and adding to body of email,
		* based on nFlags parameter.
		LPARAMETERS tcExpression, tnFlags
		WITH THIS
			IF BITAND(tnFlags,LOGIT) = LOGIT THEN
				IF .lLoggingIsOn THEN
					?
					? .cProcessName + "  " + TTOC(DATETIME()) + ": " + tcExpression
				ENDIF
			ENDIF
			IF .lWaitWindowIsOn THEN
				IF BITAND(tnFlags,NOWAITIT) = NOWAITIT THEN
					WAIT WINDOW tcExpression NOWAIT
				ENDIF
				IF BITAND(tnFlags,WAITIT) = WAITIT THEN
					WAIT WINDOW tcExpression TIMEOUT .nWaitWindowTimeout
				ENDIF
			ENDIF
			IF BITAND(tnFlags,SENDIT) = SENDIT THEN
				IF .lSendInternalEmailIsOn THEN
					.cBodyText = .cBodyText + tcExpression + CRLF + CRLF
				ENDIF
			ENDIF
		ENDWITH
	ENDPROC  &&  TrackProgress

ENDDEFINE
