*!* BBC 945 Creation Program
*!* Creation Date: 04.18.2017, Joe from Billabong prog

Parameters cBOL,nAcctNum

Public Array thisarray(1)
Public c_CntrlNum,c_GrpCntrlNum,cReturned,cWO_Num,cWO_NumStr,nWO_Num1,lROFilesOut,lPrepack
Public nUnitsum,nCtnCount,tsendto,tcc,tsendtoerr,tccerr,lTesting,lTestinput,nPTCount,cUseFolder
Public nOrigSeq,nOrigGrpSeq,cProgname,cISA_Num,cRefBOL,cMailName,cOutPath,cHoldPath,cArchivePath
Public cMod,cMBOL,cEDIType,lParcelType,cFilenameShort,cFilenameOut,cFilenameArch,lcPath,cWO_NumList
Wait Window "BBC 945 Process" Nowait

lOverflow = .F.

lTesting   = .F.  && Set to .t. for testing
lTestinput = .F. && lTesting  &&  Set to .t. for test input files only!
lEmail = .T. && !lTesting
cBillto = ""

Do m:\dev\prg\_setvars With lTesting

If !lTesting
  Set Asserts Off
Endif

Set Deleted On
cProgname = "bbc945_create"
If lTesting
  Assert .F. Message "At beginning of BBC 945 creation"
Endif
Set Escape On
On Escape Cancel
*ON ERROR debug
lIsError = .F.
lDoCatch = .T.
cShip_ref = ""
lCloseOutput = .T.
Store 0 To nFilenum,nOrigSeq,nOrigGrpSeq
cCarrierType = "M" && Default as "Motor Freight"
ccustname = ""
cWO_NumStr = ""
cWO_NumList = ""
cStatus=""
cRefBOL = '9999'
cUseFolder = ""
dDaysBack = 20
Do m:\dev\prg\lookups

Try
  lFederated = .F.
  cPPName = "BBC"
  tfrom = "TGF Warehouse Operations <toll-edi-ops@tollgroup.com>"
  tmessage=""
  tcc = ""
  lDoBOLGEN = .F.

  lISAFlag = .T.
  lSTFlag = .T.
  nLoadid = 1
  lSQLMail = .F.
  lDoCompare = Iif(lTesting Or lTestinput,.F.,.T.)

  lDoCompare = .F.
  cOffice = "L"
  cMod = cOffice
  gMasterOffice = cOffice
  goffice = cMod
  cMBOL = ""
  cEDIType = "945"
  lParcelType = .F.

  If Vartype(cBOL) = "L"
    If !lTesting And !lTestinput && AND DATE()<>{^2006-10-20}
      Wait Window "Office not provided...terminating" Timeout 1
      lCloseOutput = .F.
      Do ediupdate With "No OFFICE",.T.
      Throw
    Else
      Close Databases All
      If !Used('edi_trigger')
        cEDIFolder = "F:\3PL\DATA\"
        Use (cEDIFolder+"edi_trigger") In 0 Alias edi_trigger
      Endif
      Do m:\dev\prg\lookups
*!* TEST DATA AREA
      cBOL = "743774645748"
      nAcctNum = 6757
      cTime = Datetime()
      lFedEx = .T.
    Endif
  Endif

  cBOL=Trim(cBOL)

  Create Cursor temp945 (accountid i,isa_num c(10),st_num c(10),wo_num i,bol_no c(20),ship_ref c(20),filename c(50),filedate T)
  If lTestinput
    cUseFolder = "F:\WHP\WHDATA\"
  Else
    cUseFolder = "F:\WHL\WHDATA\"
  Endif

  cSCAC = ""

  swc_cutctns(cBOL)

  If Used('OUTSHIP')
    Use In outship
  Endif
*
  csq1 = [select * from outship where accountid = ]+Transform(nAcctNum)+[ and office = ']+cOffice+[' and bol_no = ']+cBOL+[']
  xsqlexec(csq1,,,"wh")
  If Reccount() = 0
    cErrMsg = "MISS BOL "+cBOL
    Do ediupdate With cErrMsg,.T.
    Throw
  Endif
  Index On bol_no Tag bol_no
  Index On wo_num Tag wo_num
  Index On ship_ref Tag ship_ref

  Select outship
  If Seek(cBOL,'outship','bol_no')
    cSCAC = Trim(outship.scac)
  Else
    Set Step On
    Do ediupdate With "BAD SCAC",.T.
    Throw
  Endif
  Select outship
  Scan For outship.bol_no = cBOL
    If Empty(outship.dcnum)
      Replace outship.dcnum With '9999' In outship Next 1
      Replace outship.storenum With 9999 In outship Next 1
    Endif
  Endscan
  Locate

  Select Distinct ship_ref From outship Where bol_no = cBOL Into Cursor temp1bbc
  Select temp1bbc
  Select 0
  Create Cursor ovpts (outshipid N(10))
  Select ovpts
  Index On outshipid Tag outshipid

  Select temp1bbc
  Scan
    cShip_ref = Alltrim(temp1bbc.ship_ref)
    Select outship
    Scan For outship.ship_ref = cShip_ref
      If "OV"$outship.ship_ref
        Insert Into ovpts (outshipid) Values (outship.outshipid)
        Wait Window "Overflow PT Found at Ship_ref: "+Alltrim(outship.ship_ref) Nowait
        Exit
      Endif
    Endscan
  Endscan
  Use In temp1bbc
  cRefBOL = Iif(lOverflow,cBOL,"9999")

  Select bol_no,wo_num ;
    FROM outship Where bol_no = cBOL ;
    AND accountid = nAcctNum ;
    GROUP By 1,2 ;
    ORDER By 1,2 ;
    INTO Cursor sqldc
  Locate
  Scan
    nWO_Num = sqldc.wo_num
    If lDoCompare
      cRetMsg = "X"
      Do m:\dev\prg\sqldata-Compare With cUseFolder,cBOL,nWO_Num,cOffice,nAcctNum,cPPName
      If cRetMsg<>"OK"
        Set Step On
        lCloseOutput = .T.
        cWO_Num = Alltrim(Str(nWO_Num1))
        Do ediupdate With "QTY COMPARE ERROR",.T.
        Throw
      Endif
    Endif
  Endscan

  If Used('parcel_carriers')
    Use In parcel_carriers
  Endif
  Use F:\3pl\Data\parcel_carriers In 0 Alias parcel_carriers

  If Seek(cSCAC,'parcel_carriers','scac')
    lParcelType = Iif(parcel_carriers.ups Or parcel_carriers.fedex,.T.,.F.)
  Endif
  lParcelType = Iif(Left(cBOL,2)="PS",.F.,lParcelType)

  cCarrierType = Iif(lParcelType,"U","M")

  Use In parcel_carriers
  If lParcelType And Len(Alltrim(cBOL)) = 17
    Do ediupdate With "PARCEL WITH WRONG BOL#",.T.
    Throw
  Endif

  If Type("nWO_Num")<> "N"
    lCloseOutput = .T.
    Do ediupdate With "BAD WO#",.T.
    Throw
  Endif
  cWO_Num = Alltrim(Str(nWO_Num))

  lTestMail = lTesting && Sends mail to Joe only
  lROFilesOut = !lTesting && If true, copies output to FTP folder (default = .t.)
  lStandalone = lTesting

*!*  lTestMail = .T.
*!* lROFilesOut = .F.

  Store "LB" To cWeightUnit
  lPrepack = .F.

  Wait Window "Now preparing tables and variables" Nowait Noclear

*!* SET CUSTOMER CONSTANTS
  ccustname = Upper("BBC")  && Customer Identifier
  cX12 = "004050"  && X12 Standards Set used

  cMailName = "BBC"
  Select 0
  Use F:\3pl\Data\mailmaster Alias mm Shared
  If lTesting Or lTestMail
    Locate For mm.edi_type = "MISC" And mm.taskname = "GENERAL"
    lUseAlt = mm.use_alt
    tsendto = Iif(lUseAlt,mm.sendtoalt,mm.sendto)
    tcc = Iif(lUseAlt,mm.ccalt,mm.cc)
    tsendtoerr = tsendto
    tccerr = tcc
    Locate
    Locate For mm.edi_type = "945" And mm.office = "L" And mm.accountid = nAcctNum
    cOutPath = Alltrim(mm.basepath)
    cArchivePath = Alltrim(mm.archpath)
    cHoldPath = Alltrim(mm.holdpath)
  Else
    Locate For mm.edi_type = "945" And mm.office = "L" And mm.accountid = nAcctNum
    If !Found()
      Do ediupdate With "MAILMASTER ACCT PROBLEM",.T.
      Throw
    Endif
    lUseAlt = mm.use_alt
    tsendto = Iif(lUseAlt,mm.sendtoalt,mm.sendto)
    tcc = Iif(lUseAlt,mm.ccalt,mm.cc)
    cOutPath = Alltrim(mm.basepath)
    cArchivePath = Alltrim(mm.archpath)
    cHoldPath = Alltrim(mm.holdpath)
    Locate
    Locate For office = 'X' And accountid = 9999
    lUseAlt = mm.use_alt
    tsendtoerr = Iif(lUseAlt,mm.sendtoalt,mm.sendto)
    tccerr = Iif(lUseAlt,mm.ccalt,mm.cc)
  Endif
  Use In mm

*!* SET OTHER CONSTANTS
  Do Case
  Case cOffice = "M"
    cCustLoc =  "FL"
    cFMIWarehouse = ""
    cDivision = "Florida"
    cFolder = "WHM"
    cSF_Addr1  = "11400 NW 32ND AVE"
    cSF_CSZ    = "MIAMI+FL+33167"

  Case cOffice = "N"
    cCustLoc =  "NJ"
    cFMIWarehouse = ""
    cDivision = "New Jersey"
    cFolder = "WHI"
    cSF_Addr1  = "800 FEDERAL BLVD"
    cSF_CSZ    =  "CARTERET+NJ+07008"

  Case cOffice = "C"
    cCustLoc =  "CA"
    cFMIWarehouse = ""
    cDivision = "San Pedro"
    cFolder = "WH2"
    cSF_Addr1  = "450 WESTMONT DRIVE"
    cSF_CSZ    = "SAN PEDRO+CA+90731"
  Otherwise
    cCustLoc =  "ML"
    cFMIWarehouse = ""
    cDivision = "Mira Loma"
    cFolder = "WHL"
    cSF_Addr1  = "3355 DULLES DR"
    cSF_CSZ    = "MIRA LOMA+CA+91752"
  Endcase

*!* Determination of file prefix (and Company Name, if needed)
  Do Case
  Case nAcctNum = 6757
    cCOName = "RW"
    cCustPrefix = 'RW'

  Case nAcctNum = 6761
    cCOName = "BBC"
    cCustPrefix = 'BB'

  Case nAcctNum = 6763
    cCOName = "CF"
    cCustPrefix = 'CF'

  Case nAcctNum = 6771
    cCOName = "CFB"
    cCustPrefix = 'CF'

  Case nAcctNum = 6781
    cCOName = "BBCB"
    cCustPrefix = 'BB'

  Case nAcctNum = 6783
    cCOName = "BBCIN"
    cCustPrefix = 'BB'

  Case nAcctNum = 6799
    cCOName = "CFN"
    cCustPrefix = 'CF'
  Endcase

  cCustFolder = Upper(ccustname)
  lNonEDI = .F.  && Assumes not a Non-EDI 945
  Store "" To lcKey
  Store 0 To alength,nLength
  cTime = Datetime()
  cDelTime = Substr(Ttoc(cTime,1),9,4)
  cApptNum = ""

*!* SET OTHER CONSTANTS
  dt1 = Ttoc(Datetime(),1)
  dtmail = Ttoc(Datetime())
  dt2 = Datetime()
  cString = ""

  csendqual = "ZZ"
  csendid = Iif(lTesting,"TOLLTEST","TOLL")
  crecqual = "ZZ"
  crecid = Iif(lTesting,"BBCTOLL","BBCTOLLTEST")

  cfd = "|"
  csegd = "~"
  cterminator = ">" && Used at end of ISA segment

  nSegCtr = 0
  cdate = Dtos(Date())
  cTruncDate = Right(cdate,6)
  cTruncTime = Substr(Ttoc(Datetime(),1),9,4)
  cfiledate = cdate+cTruncTime
  cOrig = "J"
  nSTCount = 0
  cStyle = ""
  cPTString = ""
  nTotCtnWt = 0
  nTotCtnCount = 0
  nUnitsum = 0
  cTargetStyle = ""

** PAD ID Codes
  csendidlong = Padr(csendid,15," ")
  crecidlong = Padr(crecid,15," ")

  lcPath = cOutPath
  cFilenameHold = (cHoldPath+cCustPrefix+dt1+".945")
  cFilenameShort = Justfname(cFilenameHold)
  cFilenameOut = (lcPath+cFilenameShort)
  cFilenameArch = (cArchivePath+cFilenameShort)
  nFilenum = Fcreate(cFilenameHold)

  If Used('SHIPMENT')
    Use In shipment
  Endif

  If Used('PACKAGE')
    Use In package
  Endif

*!*    dDate = DATE()-dDaysBack
*!*    csq1 = [select * from shipment where right(rtrim(str(accountid)),4) in (]+gbbcaccounts+[) and shipdate >= {]+DTOC(dDate)+[}]
*!*    xsqlexec(csq1,,,"wh")

*  Set Step On
  xsqlexec("select * from shipment where 1=0",,,"wh")  && Sets up an empty cursor
  Select outship && temp cursor with all PTs on the BOL
  Scan
    cSR = Alltrim(outship.ship_ref)
    xsqlexec("select * from shipment where right(rtrim(str(accountid)),4) in ("+gbbcaccounts+")  and pickticket = '"+cSR+"'","sm1",,"wh")
    Select sm1
    Locate
*!*        IF RECCOUNT()=0 AND lParcelType
*!*          cErrMsg = "NO SHIPMENT DATA"
*!*          THROW
*!*        ENDIF

    Select shipment
    Append From Dbf('sm1')
    Use In sm1
  Endscan
  Release cSR

  Select shipment
  Index On shipmentid Tag shipmentid
  Index On pickticket Tag pickticket

  Locate
  If Reccount("shipment") > 0
    xjfilter="shipmentid in ("
    Scan
      nShipmentId = shipment.shipmentid
      xsqlexec("select * from package where shipmentid="+Transform(nShipmentId),,,"wh")

      If Reccount("package")>0
        xjfilter=xjfilter+Transform(shipmentid)+","
      Else
        xjfilter="1=0"
      Endif
    Endscan
    xjfilter=Left(xjfilter,Len(xjfilter)-1)+")"

    xsqlexec("select * from package where "+xjfilter,,,"wh")
  Else
    xsqlexec("select * from package where .f.",,,"wh")
  Endif
  Select package

  Index On shipmentid Tag shipmentid
  Set Order To Tag shipmentid

  If !Used('outship')
*    ASSERT .F. MESSAGE "Found: OUTSHIP not open...debug"
    Use (cUseFolder+"outship") In 0 Alias outship
  Endif
  If !Seek(cBOL,'outship','bol_no')
    Wait Window "BOL not found in OUTSHIP" Timeout 1
    Do ediupdate With "BOL NOT FOUND",.T.
    Throw
  Endif

  Select dcnum From outship Where outship.bol_no = cBOL Group By 1 Into Cursor tempx
  Locate

  If Eof()
    Do ediupdate With "EMPTY DC/STORE#",.T.
    Throw
  Endif

  If Empty(tempx.dcnum)
    Use In tempx
    Select storenum From outship Where outship.bol_no = cBOL Group By 1 Into Cursor tempx
    If Eof()
      Do ediupdate With "EMPTY DC/STORE#",.T.
      Throw
    Endif

    If Empty(tempx.storenum)
      Do ediupdate With "EMPTY DC/STORE#",.T.
      Throw
    Else
      If Reccount('tempx')  > 1 And cBOL<>"04907314677854548"
        Do ediupdate With "MULTIPLE DC #'s",.T.
        Throw
      Endif
    Endif
  Endif

  If Used('tempx')
    Use In tempx
  Endif

  If Used('OUTDET')
    Use In outdet
  Endif

  selectoutdet()
  Select outdet
  Index On outdetid Tag outdetid

&& Added code blocks to the following to overcome BOL vs. WO selection issues, 12.23.2005
  Select outship
  Locate

*!*    IF USED("sqlwo")
*!*      USE IN sqlwo
*!*    ENDIF
*!*    DELETE FILE "F:\3pl\DATA\sqlwo.dbf"
*!*    SELECT bol_no,wo_num ;
*!*      FROM outship ;
*!*      WHERE bol_no = cBOL ;
*!*      AND INLIST(accountid,nAcctNum) ;
*!*      GROUP BY 1,2 ;
*!*      ORDER BY 1,2 ;
*!*      INTO DBF F:\3pl\DATA\sqlwo
*!*    USE IN sqlwo
*!*    SELECT 0
*!*    USE F:\3pl\DATA\sqlwo ALIAS sqlwo
*!*    IF lTesting
*!*      LOCATE
*!*      IF EOF()
*!*        BROWSE
*!*      ENDIF
*!*    ENDIF

  If File("F:\3pl\DATA\sqlwopt.dbf")
    Delete File "F:\3pl\DATA\sqlwopt.dbf"
  Endif
*  Set Step On

  Select bol_no,ship_ref,Iif("PROCESSMODE*SCANPACK"$outship.shipins,.T.,.F.) As scanpack, Space(10) As sqlover_ride ;
    FROM outship ;
    WHERE bol_no = cBOL ;
    AND accountid = nAcctNum ;
    AND del_date > Date()-dDaysBack;
    GROUP By 1,2 ;
    ORDER By 1,2 ;
    INTO Cursor sqlwopt Readwrite

  Select sqlwopt
  Replace sqlover_ride With "tgfnjsql01" In sqlwopt

  Locate
  If Eof()
    cErrMsg = "NO SQL DATA"
    Do ediupdate With cErrMsg,.T.
    Throw
  Endif

  cRetMsg = ""

  Set Step On
  Do m:\dev\prg\sqlconnect_pt  With nAcctNum,ccustname,cPPName,.T.,cOffice,.T.,,,,"tgfnjsql01"

*  Do m:\dev\prg\sqlconnect_pt With nAcctNum,ccustname,cPPName,.T.,cOffice,.T.,,,,.F.

  If cRetMsg<>"OK"
    Do ediupdate With "SQLCONN ERR: "+cRetMsg,.T.
    Throw
  Endif

  #If 0
    Select vbbcpp

    Select wo_num From vbbcpp Group By 1 Into Cursor temprunid1
    Select runid From vbbcpp Group By 1 Into Cursor temprunid2
    Select temprunid1
    Store Reccount() To nWORecs
    Select temprunid2
    Store Reccount() To nRIDRecs

    If nWORecs#nRIDRecs
      Do ediupdate With "MULTIPLE RUNIDS",.T.
      Throw
    Endif
    Use In temprunid1
    Use In temprunid2
  #Endif

  If !Used("scacs")
    xsqlexec("select * from scac","scacs",,"wh")
    Index On scac Tag scac
  Endif

  Select outship
  Locate
  Wait Clear
  Wait Window "Now creating Header information" Nowait Noclear

*!* HEADER LEVEL EDI DATA
  Do num_incr_isa

  cISA_Num = Padl(c_CntrlNum,9,"0")
  nISA_Num = Int(Val(cISA_Num))

  cISACode = Iif(lTesting,"T","P")

  Select outship
  crecidlong = Padr(crecid,15," ")

  Store "ISA"+cfd+"00"+cfd+"          "+cfd+"00"+cfd+"          "+cfd+csendqual+cfd+csendidlong+cfd+;
    crecqual+cfd+crecidlong+cfd+cTruncDate+cfd+cTruncTime+cfd+"U"+cfd+"00401"+cfd+;
    cISA_Num+cfd+"0"+cfd+cISACode+cfd+cterminator+csegd To cString
  Do cstringbreak

  Store "GS"+cfd+"SW"+cfd+csendid+cfd+crecid+cfd+cdate+cfd+cTruncTime+cfd+c_CntrlNum+;
    cfd+"X"+cfd+cX12+csegd To cString
  Do cstringbreak

  Select outship
  Locate

  If !Seek(nWO_Num,'outship','wo_num')
    Wait Clear
    Wait Window "Invalid Work Order Number - Not Found in OUTSHIP!" Timeout 1
    Do ediupdate With "WO "+Transform(nWO_Num)+" NOT FOUND",.T.
    Throw
  Endif

  If !lTesting
    If Empty(cBOL)  && May be empty if triggered from PT screen, or FedEx shipment
      Wait Clear
      Wait Window "MISSING Bill of Lading Number!" Timeout 1
      Do ediupdate With "BOL# EMPTY",.T.
      Throw
    Else
      If !Seek(Padr(Trim(cBOL),20),"outship","bol_no")
        Wait Clear
        Wait Window "Invalid Bill of Lading Number - Not Found!" Timeout 1
        Do ediupdate With "BOL# NOT FOUND",.T.
        Throw
      Endif
    Endif
  Endif

*************************************************************************
*1  PTIDSOURCE/OUTSHIP RECORD MAIN LOOP
*  Loops through individual OUTSHIP lines for BOL#
*************************************************************************
  Wait Clear
  Wait Window "Now creating BOL#-based information..." Nowait Noclear

  Select outship
  If !lTesting
    Count To N For outship.bol_no = cBOL And !EMPTYnul(del_date)
  Else
    Count To N For outship.bol_no = cBOL
  Endif

  If N=0
    Do ediupdate With "INCOMP BOL",.T.
    Throw
  Endif

  Locate
  cMissDel = ""

  oscanstr = "outship.bol_no = PADR(cBOL,20) AND !EMPTYnul(outship.del_date) AND INLIST(outship.accountid,nAcctNum)"

  Select outship
  nPTCount = 0
  Scan For &oscanstr
    If "OV"$outship.ship_ref
      Loop
    Endif
    lPrepack = !outship.picknpack

    Scatter Memvar Memo
    If "~"$m.Consignee  && Added to remove codes added by Paul
      m.Consignee = Substr(m.Consignee,At("~",m.Consignee,1)+1)
    Endif


    nWO_Num = m.wo_num
    cWO_Num = Alltrim(Str(nWO_Num))
    nAcctNum = outship.accountid
    If !(cWO_Num$cWO_NumStr)
      cWO_NumStr = Iif(Empty(cWO_NumStr),cWO_Num,cWO_NumStr+","+cWO_Num)
    Endif
    If !(cWO_Num$cWO_NumList)
      cWO_NumList = Iif(Empty(cWO_NumList),cWO_Num,cWO_NumList+Chr(13)+cWO_Num)
    Endif

    cPO_Num = Alltrim(m.cnee_ref)
    cShip_ref = Alltrim(m.ship_ref)
    nPTCount = nPTCount+1

*!* Added this code to trap miscounts in OUTDET/SQL\
    Wait Window "Now summing outdet/SQL by outshipid" Nowait

    If lPrepack
      If Seek(outship.outshipid,'ovpts','outshipid') Or cBOL = '04907315910258581'
        Set Deleted Off
        cRefBOL = cBOL
      Else
        Set Deleted On
      Endif
      Select outdet
      If Set("Deleted") = "OFF"
        Sum origqty To nUnitTot1 ;
          FOR !units ;
          AND Iif(cBOL = '04907315910258581',Inlist(outdet.outshipid,3214058,3227100),outdet.outshipid = outship.outshipid) ;
          AND outdet.wo_num = nWO_Num
      Else
        Sum totqty To nUnitTot1 ;
          FOR !units ;
          AND outdet.outshipid = outship.outshipid ;
          AND outdet.wo_num = nWO_Num
      Endif
      Select vbbcpp
      If Inlist(nWO_Num,5049591)
        Sum (totqty/Int(Val(Pack))) To nUnitTot2 For vbbcpp.outshipid = outship.outshipid
      Else
        Count To nUnitTot2 For vbbcpp.outshipid = outship.outshipid And vbbcpp.totqty > 0
      Endif

      If nUnitTot1<>nUnitTot2
        Set Step On
        Assert .F. Message "At SQL UNITQTY ERR"
        Do ediupdate With "SQL UNITQTY ERR-OSID "+Transform(outship.outshipid),.T.
        Throw
      Endif
    Else
*      ASSERT .f. MESSAGE "At OSID Qty scanning main loop"
      Select outdet
      Sum totqty To nUnitTot1 ;
        FOR units ;
        AND outdet.outshipid = outship.outshipid ;
        AND outdet.wo_num = nWO_Num
      Select vbbcpp
      Sum totqty To nUnitTot2 ;
        FOR vbbcpp.outshipid = outship.outshipid ;
        AND vbbcpp.totqty > 0
      If nUnitTot1<>nUnitTot2
        Assert .F. Message "At SQL TOTQTY ERR"
        Set Step On
        Do ediupdate With "SQL TOTQTY ERR-PIK",.T.
        Throw
      Endif
    Endif
    Select outdet
    Set Order To outdetid
    Locate

*!* End code addition

    If ("PENNEY"$Upper(outship.Consignee) Or "KMART"$Upper(outship.Consignee);
        or "K-MART"$Upper(outship.Consignee) Or "AMAZON"$Upper(outship.Consignee);
        or "ZAPPOS"$Upper(outship.Consignee))
      lApptFlag = .T.
    Else
      lApptFlag = .F.
    Endif

    If lTestinput
      ddel_date = Date()
      cApptNum = "99999"
      dapptdate = Date()
    Else
      ddel_date = outship.del_date
      If Empty(ddel_date)
        cMissDel = Iif(Empty(cMissDel),"The following PTs had no Delivery Dates:"+Chr(13)+Trim(cShip_ref),cMissDel+Chr(13)+Trim(cShip_ref))
      Endif
      cApptNum = Alltrim(outship.appt_num)

      If Empty(cApptNum) And !lParcelType && Penney/KMart Appt Number check
        Do ediupdate With "EMPTY APPT #",.T.
        Throw
      Endif
    Endif
    dapptdate = outship.appt

    If Empty(dapptdate)
      dapptdate = outship.del_date
    Endif

*  Endif

  If Alltrim(outship.SForCSZ) = ","
    Blank Fields outship.SForCSZ Next 1 In outship
  Endif

  alength = Alines(apt,outship.shipins,.T.,Chr(13))

  cTRNum = ""
  cPRONum = ""

  If (("WALMART"$outship.Consignee) Or ("WAL-MART"$outship.Consignee) ;
      OR ("WAL MART"$outship.Consignee) Or ("AMAZON"$outship.Consignee) Or m.scac = "ABFS")
    cKeyRec = Alltrim(outship.keyrec)
    cKeyRec = Trim(Strtran(cKeyRec,"#",""))
    cPRONum = Trim(Strtran(cKeyRec,"PR",""))

    If Empty(cPRONum)
      If lParcelType
        cPRONum = Alltrim(outship.bol_no)
      Endif
    Endif
  Endif

  nOutshipid = m.outshipid
  If lParcelType
    cPTString = Iif(Empty(cPTString),m.Consignee+" "+cBOL+" "+cShip_ref,cPTString+Chr(13)+m.Consignee+" "+cBOL+" "+cShip_ref)
  Else
    cPTString = Iif(Empty(cPTString),m.Consignee+" "+cShip_ref,cPTString+Chr(13)+m.Consignee+" "+cShip_ref)
  Endif
  m.CSZ = Trim(m.CSZ)
  Do Case
  Case m.CSZ = ","
    Do ediupdate With "MISSING CSZ "+cShip_ref,.T.
    Throw
  Case !(", "$m.CSZ)
    Do ediupdate With "UNSPACED COMMA IN CSZ",.T.
    Throw
  Endcase

  If Empty(M.CSZ)
    Wait Clear
    Wait Window "No SHIP-TO City/State/ZIP info...exiting" Timeout 1
    Do ediupdate With "NO CSZ ADDRESS INFO",.T.
    Throw
  Endif
  m.CSZ = Alltrim(Strtran(m.CSZ,"  "," "))
  nSpaces = Occurs(" ",Alltrim(m.CSZ))
  If nSpaces = 0
    Wait Window "SHIP-TO CSZ Segment Error" Timeout 1
    Do ediupdate With "SHIP-TO CSZ ERR: "+cShip_ref,.T.
    Throw
  Else
    nCommaPos = At(",",m.CSZ)
    nLastSpace = At(" ",m.CSZ,nSpaces)
    If Isalpha(Substr(Trim(m.CSZ),At(" ",m.CSZ,nSpaces-1)+1,2))
      cCity = Left(Trim(m.CSZ),At(",",m.CSZ)-1)
      cState = Substr(m.CSZ,nCommaPos+2,2)
      cZip = Trim(Substr(m.CSZ,nLastSpace+1))
    Else
      Wait Clear
      Wait Window "NOT ALPHA: "+Substr(Trim(m.CSZ),At(" ",m.CSZ,nSpaces-1)+1,2) Timeout 1
      cCity = Left(Trim(m.CSZ),At(",",m.CSZ)-1)
      cState = Substr(Trim(m.CSZ),At(" ",m.CSZ,nSpaces-2)+1,2)
      cZip = Trim(Substr(m.CSZ,nLastSpace+1))
    Endif
  Endif

  Store "" To cSForCity,cSForState,cSForZip
  cStoreName = segmentget(@apt,"STORENAME",alength)
  If !lFederated
    If !Empty(M.SForCSZ)
      m.SForCSZ = Alltrim(Strtran(m.SForCSZ,"  "," "))
      nSpaces = Occurs(" ",Alltrim(m.SForCSZ))
      If nSpaces = 0
        m.SForCSZ = Strtran(m.SForCSZ,",","")
        cSForCity = Alltrim(m.SForCSZ)
        cSForState = ""
        cSForZip = ""
      Else
        nCommaPos = At(",",m.SForCSZ)
        nLastSpace = At(" ",m.SForCSZ,nSpaces)
        nMinusSpaces = Iif(nSpaces=1,0,1)
        If Isalpha(Substr(Trim(m.SForCSZ),At(" ",Trim(m.SForCSZ),nSpaces-nMinusSpaces)+1,2))
          cSForCity = Left(Trim(m.SForCSZ),At(",",m.SForCSZ)-1)
          cSForState = Substr(m.SForCSZ,nCommaPos+2,2)
          cSForZip = Alltrim(Substr(m.SForCSZ,nCommaPos+4))
          If Isalpha(cSForZip)
            cSForZip = ""
          Endif
        Else
          Wait Clear
          Wait Window "NOT ALPHA: "+Substr(Trim(m.SForCSZ),At(" ",m.SForCSZ,nSpaces-1)+1,2) Timeout 1
          cSForCity = Left(Trim(m.SForCSZ),At(",",m.SForCSZ)-1)
          cSForState = Substr(Trim(m.SForCSZ),At(" ",m.SForCSZ,nSpaces-2)+1,2)
          cSForZip = Alltrim(m.SForCSZ)
          If Isalpha(cSForZip)
            cSForZip = ""
          Endif
        Endif
      Endif
    Endif
  Else
    Store "" To cCity,cState,cZip
  Endif

  Do num_incr_st
  Wait Clear
  Wait Window "Now creating Line Item information" Nowait Noclear

  Insert Into temp945 (accountid,isa_num,st_num,wo_num,bol_no,ship_ref,filename,filedate) ;
    VALUES (m.accountid,c_CntrlNum,c_GrpCntrlNum,m.wo_num,m.bol_no,m.ship_ref,cFilenameShort,dt2)

  Store "ST"+cfd+"945"+cfd+Padl(c_GrpCntrlNum,9,"0")+csegd To cString
  Do cstringbreak
  nSTCount = nSTCount + 1
  nSegCtr = 1

  cSalesOrder = segmentget(@apt,"SALESORDER",alength)
  Store "W06"+cfd+"J"+cfd+cShip_ref+cfd+cdate+cfd+Alltrim(cWO_Num)+cfd+cBOL+cfd+cPO_Num+csegd To cString
  Do cstringbreak
  nSegCtr = nSegCtr + 1
  nCtnNumber = 1  && Seed carton sequence count

  cStoreNum = segmentget(@apt,"STORENUM",alength)
  If Empty(cStoreNum)
    cStoreNum = Alltrim(m.dcnum)
  Endif
  Store "N1"+cfd+"ST"+cfd+Alltrim(m.Consignee)+cfd+"93"+cfd+cStoreNum+csegd To cString
  Do cstringbreak
  nSegCtr = nSegCtr + 1
  If Empty(M.ADDRESS2)
    Store "N3"+cfd+Alltrim(M.ADDRESS)+csegd To cString
  Else
    Store "N3"+cfd+Alltrim(M.ADDRESS)+cfd+Alltrim(M.ADDRESS2)+csegd To cString
  Endif
  Do cstringbreak
  nSegCtr = nSegCtr + 1

  cCountry = segmentget(@apt,"COUNTRY",alength)
  cCountry = Allt(cCountry)
  If Empty(cCountry)
    cCountry = "US"
  Endif
  Store "N4"+cfd+cCity+cfd+cState+cfd+cZip+cfd+cCountry+csegd To cString
  Do cstringbreak
  nSegCtr = nSegCtr + 1

*!*      STORE "REF"+cfd+"ST"+cfd+ALLTRIM(m.dcnum)+csegd TO cString
*!*      DO cstringbreak
*!*      nSegCtr = nSegCtr + 1

*!*      cRegion = segmentget(@apt,"REGION",alength)
*!*      STORE "N1"+cfd+"UD"+cfd+ALLTRIM(cRegion)+csegd TO cString
*!*      DO cstringbreak
*!*      nSegCtr = nSegCtr + 1

  If !Empty(Alltrim(m.shipfor))
    Store "N1"+cfd+"Z7"+cfd+Alltrim(m.shipfor)+cfd+"92"+cfd+Alltrim(m.sforstore)+csegd To cString
    Do cstringbreak
    nSegCtr = nSegCtr + 1
    If Empty(M.ADDRESS2)
      Store "N3"+cfd+Alltrim(M.SFORADDR1)+csegd To cString
    Else
      Store "N3"+cfd+Alltrim(M.SFORADDR1)+cfd+Alltrim(M.SFORADDR2)+csegd To cString
    Endif
    Do cstringbreak
    nSegCtr = nSegCtr + 1
    If !Empty(cSForState)
      Store "N4"+cfd+cSForCity+cfd+cSForState+cfd+cSForZip+cfd+"US"+csegd To cString
    Else
      Store "N4"+cfd+cSForCity+csegd To cString
    Endif
    Do cstringbreak
    nSegCtr = nSegCtr + 1
  Endif


************** Here we check for either the special Amazon routing code or a standard dispatch code or appt#

  llARN= .F.  && check for the specific Amazon code and verify the format........
  If ("AMAZON"$outship.Consignee Or "AMAZON"$Upper(cBillto)) And !("MARKETPLACE"$Upper(cBillto))
    llARN= .T.
    cAmazonRoutCode = Allt(outship.appt_num)
    cAmazonRoutCode = Iif(Empty(cAmazonRoutCode),Alltrim(outship.keyrec),cAmazonRoutCode)
    llAllNumeric=.T.
    For lndigctr = 1 To Len(cAmazonRoutCode)
      If !Isdigit(Substr(cAmazonRoutCode,lndigctr,1))
        llAllNumeric = .F.
      Endif
    Next
    If (Empty(cAmazonRoutCode) Or Len(cAmazonRoutCode)!= 10 Or !llAllNumeric)
      Set Step On
      cErrMsg = "EMPTY/BAD AMZ ARN Number "+cShip_ref
      Do ediupdate With cErrMsg,.T.
      Throw
    Endif
    Store "N9"+cfd+"LO"+cfd+Allt(cAmazonRoutCode)+csegd To cString
    Do cstringbreak
    nSegCtr = nSegCtr + 1
  Endif

  If llARN = .F. And lApptFlag = .T.  && this flag set earlier in the code
    Store "N9"+cfd+"LO"+cfd+Alltrim(outship.appt_num)+csegd To cString
    Do cstringbreak
    nSegCtr = nSegCtr + 1
  Endif
************** end of appt#'s **********************************************************************

  If lParcelType
    Do Case
    Case outship.terms = "BS"
      cBilling = "PP"
    Case outship.terms = "BTP"
      cBilling = "TP"
    Case outship.terms = "BR"
      cBilling = "CC"
    Otherwise
      cBilling = "PP"
    Endcase
    Store "W27"+cfd+"M"+cfd+Alltrim(outship.scac)+cfd+Alltrim(outship.carrcode)+cfd+Alltrim(cBilling)+csegd To cString
    Do cstringbreak
    nSegCtr = nSegCtr + 1
  Else
    Store "W27"+cfd+"M"+cfd+Alltrim(outship.scac)+cfd+Alltrim(outship.carrcode)+csegd To cString
    Do cstringbreak
    nSegCtr = nSegCtr + 1
  Endif

  If lParcelType
    If lTesting
      cFreight = "2750"
    Else
      Select shipment
      Assert .F. Message "At UPS billing and  freight calculation"
      If Seek(cShip_ref,"shipment","pickticket")
        cBilling = Alltrim(shipment.billing)
        cSMP = Icase(cBilling = "COL","CC",cBilling = "PPD","PP",Inlist(cBilling,"B3P","STP"),"TP","PU")
*        STORE "W27"+cfd+"M"+cfd+ALLTRIM(m.scac)+cfd+ALLTRIM(carrcode)+cfd+cSMP+csegd TO cString
*          DO cstringbreak
*          nSegCtr = nSegCtr + 1
        Select package
        Sum package.pkgcharge To nFreight For package.shipmentid=shipment.shipmentid
        cFreight = Strtran(Alltrim(Str(nFreight,10,2)),".","")
      Endif
    Endif
    Store "G72"+cfd+"504"+cfd+"15"+cfd+Alltrim(carrcode)+Replicate(cfd,5)+Allt(cFreight)+csegd To cString
    Do cstringbreak
    nSegCtr = nSegCtr + 1
  Endif

  If !lTestinput
    Select trknumber,ucc,ref4 From package Where package.shipmentid=shipment.shipmentid Into Cursor package1
    Locate
*      BROWSE
  Else
    Create Cursor package1 (trknumber c(20))
    Append Blank
    Replace package1.trknumber With cBOL In package
  Endif


*************************************************************************
*2  DETAIL LOOP - MATCHING SHIP_REF FIELD (FROM OUTSHIP)
*************************************************************************
*    ASSERT .F. MESSAGE "In Detail Loop"
  Store "LB" To cWeightUnit
  Wait Clear
  Wait Window "Now creating Detail information "+cShip_ref Nowait Noclear
  Select vbbcpp
  Locate
  Select outdet
  Set Order To Tag outdetid

  If cBOL = cRefBOL
    Set Deleted Off
  Else
    Set Deleted On
  Endif
  Select vbbcpp
  Set Relation To outdetid Into outdet

  Locate
  Locate For ship_ref = Trim(cShip_ref) And outshipid = nOutshipid
  If !Found()
    If !lTesting
      Wait Window "PT "+cShip_ref+" NOT FOUND in vbbcpp...ABORTING" Timeout 1
      If !lTesting
        lSQLMail = .T.
      Endif
      Do ediupdate With "MISS PT-SQL: "+cShip_ref,.T.
      Throw
    Else
      Loop
    Endif
  Endif

&& OK here we grab the carton data from Cartons for this pick ticket

&& we also ned to grab the UPC/Line# data so wee can maintain outbound lineto UPC
*!*  Create Cursor upcdata (;
*!*  ship_ref Char(20),;
*!*  upc Char(12),;
*!*  STYLE Char(20),;
*!*  COLOR Char(10),;
*!*  ID Char(10),;
*!*  origqty  N(6),;
*!*  linenum  Char(15),;
*!*  remainqty N(6))

*!*  lcPt = "660072799TST"

*!*  useca("outship","wh",,,"select * from outship where ship_ref='"+lcPt+"'")
*!*  useca("outdet","wh",,,"select * from outdet where outshipid = "+Transform(outship.outshipid))

*!*  Select outdet
*!*  Go top

*!*  Scan
*!*    alength= Alines(a,printstuff)
*!*    done = .F.
*!*    izzz=1
*!*    Do While !done
*!*      If "OPENUPC"$a[izzz]
*!*        lnUPCStart=Atc("*",a[izzz])+1
*!*        lnUPCEnd  =Atc("*",a[izzz],2)
*!*        lnUPCLength = lnUPCEnd-lnUPCStart
*!*        m.upc = Substr(a[izzz],lnUPCStart,lnUPCLength)
*!*        lnLineNumStart = Atc("*",a[izzz],2)+1
*!*        lnLineNumEnd   = Atc("*",a[izzz],3)
*!*        lnLineNumLength = lnLineNumEnd-lnLineNumStart
*!*        m.linenum = Substr(a[izzz],lnLineNumStart,lnLineNumLength)
*!*        lnQtyStart =Atc("*",a[izzz],3)+1
*!*        m.qty = Substr(a[izzz],lnQtyStart)
*!*        Insert Into upcdata(ship_ref,upc,Style,Color,Id,linenum,origqty,remainqty) Values (lcPt,m.upc,outdet.Style,outdet.Color,outdet.Id, m.linenum,Val(m.qty),Val(m.qty))
*!*      Endif
*!*      izzz=izzz+1
*!*      If izzz > alength
*!*        done = .T.
*!*      Endif
*!*    Enddo

*!*  Endscan


  scanstr = "vbbcpp.ship_ref = cShip_ref and vbbcpp.outshipid = nOutshipid"
  Select vbbcpp
  Locate
  Locate For &scanstr
  cCartonNum= "XXX"

  Do While &scanstr
    nUnitsum  = outship.qty
    nTotCtnWt = outship.weight
    lDoManSegment = .T.
    lDoPALSegment = .F.
    llPrepackSpecial = .F.

    Select outdet
    alength = Alines(apt,outdet.printstuff,.T.,Chr(13))
    isPrepack = getmemodata("printstuff","PREPACKUPC")
    If vbbcpp.loccode ="PREPACK"
      llPrepackSpecial = .T.
      Select outdet
      PrepackUPC   = getmemodata("outdet.printstuff","PREPACKUPC")
      Prepackline  = getmemodata("printstuff","PREPACKLINE")

      If Len(Alltrim(vbbcpp.scc214)) >5   && try and accmmodate the chnage over in the format of the SQL data
        PrepackUPC   = Substr(vbbcpp.scc214,1,12)
        Prepackline  = Substr(vbbcpp.scc214,14)
      Endif

      PrepackStyle = outdet.Style
      PrepackSize  = getmemodata("outdet.printstuff","PREPACKSIZE")
      PrepackCOLOR = getmemodata("outdet.printstuff","PREPACKCOLOR")

      alength = Alines(ashipins,outship.shipins,.T.,Chr(13))
      izzz=1
      done = .F.
      Do While !done
        If PrepackUPC$ashipins[izzz]
          done = .T.
        Else
          izzz=izzz+1
        Endif
        If izzz >= alength
          done = .T.
        Endif
      Enddo
      izzz=izzz+2
      PrepackCOLOR = Substr(ashipins[izzz],Atc("*",ashipins[izzz])+1,Atc("-",ashipins[izzz])-Atc("*",ashipins[izzz])-1)
      PrepackSize  = Substr(ashipins[izzz],Atc("-",ashipins[izzz])+1)
    Else
      llPrepackSpecial = .F.
    Endif

    lSkipBack = .T.

    If Trim(vbbcpp.ucc) <> cCartonNum
      Store Trim(vbbcpp.ucc) To cCartonNum
    Endif

    Store "LX"+cfd+Alltrim(Str(nCtnNumber))+csegd To cString   && Carton Seq. # (up to Carton total)
    Do cstringbreak
    nSegCtr = nSegCtr + 1

    Do While vbbcpp.ucc = cCartonNum
      If vbbcpp.totqty < 1
        Skip 1 In vbbcpp
        Loop
      Endif

      cUCCNumber = vbbcpp.ucc
      If Empty(cUCCNumber) Or Isnull(cUCCNumber)
        Wait Clear
        Wait Window "Empty UCC Number in vbbcpp "+cShip_ref Timeout 1
        lSQLMail = .T.
        Do ediupdate With "EMPTY UCC# in "+cShip_ref,.T.
        Throw
      Endif
      cUCCNumber = Trim(Strtran(Trim(cUCCNumber)," ",""))

      If lDoManSegment
        nCtnNumber = nCtnNumber + 1
        lDoManSegment = .F.

        If lTesting
          cTrkNumber = ""
        Else
          Select package1
          Locate For  package1.ucc = cUCCNumber
          If Found()
            cTrkNumber = Alltrim(package1.trknumber)
          Else
            cTrkNumber = ""
          Endif
        Endif

        Store "MAN"+cfd+"GM"+cfd+Trim(cUCCNumber)+cfd+cfd+"CP"+cfd+cTrkNumber+csegd To cString
        Do cstringbreak
        nSegCtr = nSegCtr + 1
        nTotCtnCount = nTotCtnCount + 1
      Endif

      If llPrepackSpecial = .T.
*           Do While vbbcpp.ucc = cUCCNumber  And !Eof("vbbcpp")
        lcThisUCC = cUCCNumber

        Store "W12"+cfd+"CL"+cfd+"1"+cfd+"1"+cfd+cfd+"CA"+cfd+Trim(PrepackUPC)+csegd To cString
        Do cstringbreak
        nSegCtr = nSegCtr + 1

        Store "N9"+cfd+"LI"+cfd+Prepackline+csegd To cString
        Do cstringbreak
        nSegCtr = nSegCtr + 1

        Store "N9"+cfd+"IX"+cfd+PrepackStyle+csegd To cString
        Do cstringbreak
        nSegCtr = nSegCtr + 1

        Store "N9"+cfd+"CO"+cfd+Alltrim(PrepackCOLOR)+csegd To cString
        Do cstringbreak
        nSegCtr = nSegCtr + 1

        Store "N9"+cfd+"IZ"+cfd+Alltrim(PrepackSize)+csegd To cString
        Do cstringbreak
        nSegCtr = nSegCtr + 1
        Select vbbcpp
        Do While vbbcpp.ucc = cUCCNumber  And !Eof("vbbcpp")
          Skip 1 In vbbcpp
        Enddo
        Loop
      Else
        If Empty(outdet.ctnwt) Or outdet.ctnwt = 0
          cCtnWt = Alltrim(Str(Int(outship.weight/outship.ctnqty)))
          If (Empty(cCtnWt) Or Int(Val(cCtnWt)) = 0) And outdet.totqty >0
            If lTesting
              cCtnWt = '5'
            Else
              Do ediupdate With "WEIGHT ERR: PT "+cShip_ref,.T.
              Throw
            Endif
          Endif
          nTotCtnWt = nTotCtnWt + Int(Val(cCtnWt))
        Else
          Store Alltrim(Str(outdet.ctnwt)) To cCtnWt
          nTotCtnWt = nTotCtnWt + outdet.ctnwt
        Endif
      Endif

*        getmemodata
*
*        WAIT WINDOW "OD OUTDETID: "+TRANSFORM(outdet.outdetid)+CHR(13)+"V- OUTDETID: "+TRANSFORM(vbbcpp.outdetid) NOWAIT
      cStyle = Trim(outdet.Style)
      cColor = Trim(outdet.Color)
      cSize  = Trim(outdet.Id)
      cUPC   = Trim(vbbcpp.upc)
	
	cLineNum = ""
      cLineNum = getmemodata("outdet.printstuff","REPLENLINE")
      If cLineNum ="NA"
        cLineNum = outdet.linenum
      Endif
      cItemNum = Trim(outdet.custsku)
*        nShipDetQty = outdet.totqty
      nODID = outdet.outdetid

      If Empty(cLineNum)
        Assert .F. Message "In LINENUM error"
        Set Step On
        Do ediupdate With "EMPTY LINE #s-ODID "+Transform(nODID),.T.
        Throw
      Endif

      nShipDetQty = vbbcpp.totqty
      If Isnull(nShipDetQty)
        nShipDetQty = outdet.totqty
      Endif
      nUnitsum = nUnitsum + nShipDetQty

      nOrigDetQty = vbbcpp.qty
      If Isnull(nOrigDetQty)
        nOrigDetQty = nShipDetQty
      Endif

      If lDoPALSegment
        Store "PAL"+Replicate(cfd,11)+cCtnWt+cfd+cWeightUnit+csegd To cString
        Do cstringbreak
        nSegCtr = nSegCtr + 1
        lDoPALSegment = .F.
      Endif

*!* Changed the following to utilize original 940 unit codes from Printstuff field
      cUnitCode = Trim(segmentget(@apt,"UNITCODE",alength))
      If Empty(cUnitCode)
        cUnitCode = "EA"
      Endif

*!*          IF nOrigDetQty = nShipDetQty
*!*            nShipStat = "CL"
*!*          ELSE
*!*            nShipStat = "PR"
*!*          ENDIF

      nShipStat = "CL"

      Store "W12"+cfd+nShipStat+cfd+Alltrim(Str(nOrigDetQty))+cfd+Alltrim(Str(nShipDetQty))+cfd+cfd+cUnitCode+cfd+Trim(cUPC)+csegd To cString
      Do cstringbreak
      nSegCtr = nSegCtr + 1

      Store "N9"+cfd+"LI"+cfd+cLineNum+csegd To cString
      Do cstringbreak
      nSegCtr = nSegCtr + 1

      Store "N9"+cfd+"IX"+cfd+cStyle+csegd To cString
      Do cstringbreak
      nSegCtr = nSegCtr + 1

      Store "N9"+cfd+"CO"+cfd+Alltrim(outdet.Color)+csegd To cString
      Do cstringbreak
      nSegCtr = nSegCtr + 1

      Store "N9"+cfd+"IZ"+cfd+Alltrim(outdet.Id)+csegd To cString
      Do cstringbreak
      nSegCtr = nSegCtr + 1

      Skip 1 In vbbcpp
    Enddo
    lSkipBack = .T.

*      ASSERT .F. MESSAGE "At end of detail looping-debug"
  Enddo

*************************************************************************
*2^  END DETAIL MATCHING SHIP_REF LOOP
*************************************************************************
  Wait Clear
  Wait Window "Now creating Section Closure information" Nowait Noclear
  Store "W03"+cfd+Alltrim(Str(nUnitsum))+cfd+Alltrim(Str(nTotCtnWt))+cfd+;
    cWeightUnit+cfd+Alltrim(Str(outship.cuft,10,2))+cfd+"CF"+cfd+Alltrim(Str(nTotCtnCount))+cfd+"CT"+csegd To cString   && Units sum, Weight sum, carton count

  nTotCtnWt = 0
  nTotCtnCount = 0
  nUnitsum = 0
  Do cstringbreak

  Store  "SE"+cfd+Alltrim(Str(nSegCtr+2))+cfd+Padl(c_GrpCntrlNum,9,"0")+csegd To cString
  Do cstringbreak

  Select outship
  Wait Clear
Endscan


*************************************************************************
*1^  END OUTSHIP MAIN LOOP
*************************************************************************

Do close945
=Fclose(nFilenum)

set step On 

*  ASSERT .F. MESSAGE "At end of main loop...debug here"
If !lTesting
  Select edi_trigger
  Do ediupdate With "945 CREATED",.F.
  Select edi_trigger
  Locate

  If !Used("ftpedilog")
    Select 0
    Use F:\edirouting\ftpedilog Alias ftpedilog
    Insert Into ftpedilog (TRANSFER,ftpdate,filename,acct_name,Type) Values ("945-"+ccustname+"-"+cCustLoc,dt2,cFilenameHold,Upper(ccustname),"945")
    Use In ftpedilog
  Endif
Endif

Wait Clear
If lTesting
  Wait Window cCustFolder+" 945 Process complete..." Timeout 2
Endif

*!* Create eMail confirmation message
tsubject = cCustPrefix+" 945 EDI File from TGF as of "+dtmail+" ("+cCustLoc+")"
tattach = " "
tmessage = "945 EDI Info from TGF, file "+cFilenameShort+Chr(13)
tmessage = tmessage + "Co. "+cCOName+", Division "+cDivision+", BOL# "+Trim(cBOL)+Chr(13)
tmessage = tmessage + "For Work Orders: "+cWO_NumStr+","+Chr(13)
tmessage = tmessage + "containing these "+Transform(nPTCount)+" picktickets:"+Chr(13)
tmessage = tmessage + cPTString + Chr(13)
tmessage = tmessage +"has been created and will be transmitted ASAP."+Chr(13)+Chr(13)
If !Empty(cMissDel)
  tmessage = tmessage+Chr(13)+Chr(13)+cMissDel+Chr(13)+Chr(13)
Endif
tmessage = tmessage + "If you have any questions, please eMail or call 732-750-9000 x217."

If lEmail
  Do Form m:\dev\frm\dartmail2 With tsendto,tfrom,tsubject,tcc,tattach,tmessage,"A"
Endif
nPTCount = 0

Wait Clear
Wait Window cMailName+" 945 EDI File output complete" Nowait

*!* Transfers files to correct output folders
*  COPY FILE [&cFilenameHold] TO [&cFilenameArch]
oFSO=Createobject("Scripting.FileSystemObject")
oFSO.CopyFile("&cFilenameHold","&cFilenameArch",.T.)
If lROFilesOut And !lTesting
  oFSO=Createobject("Scripting.FileSystemObject")
  oFSO.CopyFile("&cFilenameHold","&cFilenameOUT",.T.)
**    COPY FILE [&cFilenameHold] TO [&cFilenameOut]
  Delete File [&cFilenameHold]
  Select temp945
  Copy To "f:\3pl\data\temp945ms.dbf"
  Use In temp945
  Select 0
  Use "f:\3pl\data\temp945ms.dbf" Alias temp945ms
  Select 0
  Use "f:\3pl\data\pts_sent945.dbf" Alias pts_sent945
  Append From "f:\3pl\data\temp945ms.dbf"
  Use In pts_sent945
  Use In temp945ms
  Delete File "f:\3pl\data\temp945ms.dbf"
Endif
lDoCatch = .F.
asn_out_data()
Release All Like c_CntrlNum,c_GrpCntrlNum

Catch To oErr
  If lDoCatch
    Assert .F. Message "In error CATCH..."
    Set Step On
    lEmail = .F.
    Do ediupdate With Alltrim(oErr.Message),.T.
    tsubject = ccustname+" Error ("+Transform(oErr.ErrorNo)+") at "+Ttoc(Datetime())
    tattach  = ""
    tsendto  = "pgaidis@fmiint.com"&&tsendtoerr
    tcc = tccerr

    tmessage = ccustname+" Error processing "+Chr(13)

    tmessage =tmessage+ "Try/CATCH Exeception Message:"+Chr(13)+;
      [  Error: ] + Str(oErr.ErrorNo) +Chr(13)+;
      [  LineNo: ] + Str(oErr.Lineno) +Chr(13)+;
      [  Message: ] + oErr.Message +Chr(13)+;
      [  Procedure: ] + oErr.Procedure +Chr(13)+;
      [  Details: ] + oErr.Details +Chr(13)+;
      [  StackLevel: ] + Str(oErr.StackLevel) +Chr(13)+;
      [  LineContents: ] + oErr.LineContents+Chr(13)+;
      [  UserValue: ] + oErr.UserValue
    tmessage = tmessage + Chr(13)+"PROGRAM: "+cProgname
    If !Empty(cStatus)
      tmessage = tmessage + Chr(13)+"Error: "+cStatus
    Endif
    tsubject = "945 EDI Poller Error at "+Ttoc(Datetime())
    tattach  = ""
    tfrom    ="TGF EDI 945 Poller Operations <transload-ops@tollgroup.com>"
    Do Form m:\dev\frm\dartmail2 With tsendto,tfrom,tsubject,tcc,tattach,tmessage,"A"
  Endif
Endtry

*** END OF CODE BODY

****************************
Procedure close945
****************************
** Footer Creation

Store  "GE"+cfd+Alltrim(Str(nSTCount))+cfd+c_CntrlNum+csegd To cString
Do cstringbreak

Store  "IEA"+cfd+"1"+cfd+Padl(c_CntrlNum,9,"0")+csegd To cString
Do cstringbreak
Return

****************************
Procedure num_incr_isa
****************************

If !Used("serfile")
  Use ("f:\3pl\data\serial\"+ccustname+"945_serial") In 0 Alias serfile
Endif
Select serfile
If lTesting And lISAFlag
  Assert .F. Message "At group seqnum...debug"
  nOrigSeq = serfile.seqnum
  lISAFlag = .F.
Endif
nISA_Num = serfile.seqnum
c_CntrlNum = Alltrim(Str(serfile.seqnum))
Replace serfile.seqnum With serfile.seqnum + 1 In serfile
Select outship
Return

****************************
Procedure num_incr_st
****************************

If !Used("serfile")
  Use ("f:\3pl\data\serial\"+ccustname+""945_serial") IN 0 ALIAS serfile
Endif
Select serfile
If lTesting And lSTFlag
  nOrigGrpSeq = serfile.grpseqnum
  lSTFlag = .F.
Endif
c_GrpCntrlNum = Alltrim(Str(serfile.grpseqnum))
Replace serfile.grpseqnum With serfile.grpseqnum + 1 In serfile
Select outship
Return


****************************
Procedure segmentget
****************************

Parameter thisarray,lcKey,nLength

For i = 1 To nLength
  If i > nLength
    Exit
  Endif
  lnEnd= At("*",thisarray[i])
  If lnEnd > 0
    lcThisKey =Trim(Substr(thisarray[i],1,lnEnd-1))
    If Occurs(lcKey,lcThisKey)>0
      Return Substr(thisarray[i],lnEnd+1)
      i = 1
    Endif
  Endif
Endfor

Return ""

****************************
Procedure ediupdate
****************************
Parameter cStatus,lIsError
*  lDoCatch = IIF(lIsError,.T.,.F.)
lDoCatch = .F.


If !lTesting
  Select edi_trigger
  If !lIsError
    If cStatus = "JILDOR NO 945"
      Replace processed With .T.,proc945 With .F.,file945 With "",comments With comments+Chr(13)+"No 945 per Highline";
        fin_status With "NO 945 NEEDED",errorflag With .F.,when_proc With Datetime() ;
        FOR edi_trigger.bol = cBOL And accountid = nAcctNum

*        xsqlexec("update edi_trigger set processed=1, proc945=0, file945 With '', " + ;
"fin_status='NO 945 NEEDED', errorflag=0, when_proc={"+TTOC(DATETIME())+"} " + ;
"where bol='"+cBOL+"' and accountid="+TRANSFORM(nAcctNum),,,"stuff")

*         "comments=comments+'"+chr(13)+"No 945 per Highline', " + ; doesn't work in SQL

    Else
      Replace processed With .T.,proc945 With .T.,file945 With cFilenameHold,isa_num With cISA_Num,;
        fin_status With "945 CREATED",errorflag With .F.,when_proc With Datetime() ;
        FOR edi_trigger.bol = cBOL And accountid = nAcctNum

*        xsqlexec("update edi_trigger set processed=1, proc945=1, file945='"+cFilenameHold+"', isa_num='"+cISA_Num+"', fin_status='945 CREATED', errorflag=0, when_proc={"+TTOC(DATETIME())+"} where bol='"+cBOL+"' and accountid="+TRANSFORM(nAcctNum),,,"stuff")

    Endif
  Else
    Replace processed With .T.,proc945 With .F.,file945 With "",;
      fin_status With cStatus,errorflag With .T. ;
      FOR edi_trigger.bol = cBOL And accountid = nAcctNum

*      xsqlexec("update edi_trigger set processed=1, proc945=0, file945='', fin_status='"+cStatus+"', errorflag=1 where bol='"+cBOL+"' and accountid="+TRANSFORM(nAcctNum),,,"stuff")

    If lCloseOutput
      =Fclose(nFilenum)
      Erase &cFilenameHold
    Endif
  Endif
Endif


If lIsError And lEmail && AND cStatus # "SQL ERROR"
  tsubject = "945 Error in BBC BOL "+Trim(cBOL)+"(At PT "+cShip_ref+")"
  tattach = " "
  tsendto = Alltrim(tsendtoerr)
  tcc = Alltrim(tccerr)
  tmessage = "945 Processing for BOL# "+cBOL+", WO# "+cWO_Num+" produced this error: (Office: "+cOffice+"): "+cStatus+Chr(13)+"Check EDI_TRIGGER and re-run"
  If "TOTQTY ERR"$cStatus
    tmessage = tmessage + Chr(13) + "At OUTSHIPID: "+Alltrim(Str(m.outshipid))
  Endif
  Do Form m:\dev\frm\dartmail2 With tsendto,tfrom,tsubject,tcc,tattach,tmessage,"A"
Endif

If Used('outship')
  Use In outship
Endif
If Used('outdet')
  Use In outdet
Endif
If Used('shipment')
  Use In shipment
Endif
If Used('package')
  Use In package
Endif

If Used('serfile')
  Select serfile
  If lTesting
    If nOrigSeq > 0
      Replace serfile.seqnum With nOrigSeq
    Endif
    If nOrigGrpSeq > 0
      Replace serfile.grpseqnum With nOrigGrpSeq
    Endif
  Endif
  Use In serfile
Endif
If Used('scacs')
  Use In scacs
Endif
If Used('mm')
  Use In mm
Endif
If Used('tempx')
  Use In tempx
Endif
If Used('vbbcpp')
  Use In vbbcpp
Endif

If !lTesting
  Select edi_trigger
  Locate
Endif
If lIsError
Endif
Return

****************************
Procedure cstringbreak
****************************
cLen = Len(Alltrim(cString))

Fputs(nFilenum,cString)

*  FWRITE(nFilenum,cString)

Return
