*!* m:\dev\prg\alpha6_940_process.prg

CD &lcPath

LogCommentStr = ""

delimchar = cDelimiter
lcTranOpt= cTranslateOption

lnNum = ADIR(tarray,"*.*")

IF lnNum = 0
	WAIT WINDOW AT 10,10 "      No "+cCustname+" 940s to import     " TIMEOUT 3
	NormalExit = .T.
	THROW
ENDIF

	IF lTesting
	SET STEP ON
	ENDIF

FOR thisfile = 1  TO lnNum
	Xfile = lcPath+tarray[thisfile,1] && +"."
	cfilename = ALLTRIM(LOWER(tarray[thisfile,1]))
	nFileSize = VAL(TRANSFORM(tarray(thisfile,2)))
	ArchiveFile = (lcArchivepath+cfilename)
	IF !lTesting
		COPY FILE [&xfile] TO ("F:\ftpusers\Merkury\940translate\"+cfilename) && to create 997
	ENDIF
	WAIT WINDOW "Importing file: "+cfilename NOWAIT
		DO m:\dev\prg\createx856a
	SELECT x856
	APPEND FROM [&xfile] TYPE DELIMITED WITH CHARACTER "*"

*	DO m:\dev\prg\loadedifile WITH Xfile,delimchar,lcTranOpt,cCustname

	SELECT x856
	LOCATE FOR x856.segment = 'GS'
	IF x856.f1 = "FA"
		WAIT WINDOW "This is a 997 FA file...moving to correct folder" TIMEOUT 2
		cfile997in = ("F:\ftpusers\"+cCustname+"\997in\"+cfilename)
		COPY FILE [&xfile] TO [&cfile997in]
		DELETE FILE [&xfile]
		LOOP
	ELSE
		LOCATE
	ENDIF

*	SET STEP ON 
	DO create_pt_cursors WITH cUseFolder  && .t. =create from SQL

	lOK = .T.
	DO ("m:\dev\prg\alpha6_940_bkdn")


	IF lOK && If no divide-by-zero error
		DO "m:\dev\prg\all940_import"
*		DO ("m:\dev\prg\merkury940_import")
	ELSE
 		archivefile  = (lcArchivepath+cfilename)
		IF !FILE(archivefile)
			COPY FILE [&Xfile] TO [&archivefile]
		ENDIF
		IF !lTesting
		IF FILE(Xfile)
			DELETE FILE [&xfile]
		ENDIF
		endif
		LOOP
	ENDIF

NEXT thisfile

&& now clean up the 940in archive files, delete for the upto the last 10 days
IF !lTesting
deletefile(lcarchivepath,20)
endif

WAIT CLEAR

WAIT "Entire "+cCustname+" 940 Process Now Complete" WINDOW TIMEOUT 2
NormalExit = .T.
