**This script will capture all files that have been successfully FTP'd
utilsetup("FTP_FILES_CONTROL")
Set Exclusive Off
Set Deleted On
Set Talk Off
Set Safety Off 
WITH _SCREEN	
	.AUTOCENTER = .T.
	.WINDOWSTATE = 0
	.BORDERSTYLE = 1
	.WIDTH = 320
	.HEIGHT = 210
	.TOP = 290
	.LEFT = 110
	.CLOSABLE = .T.
	.MAXBUTTON = .t.
	.MINBUTTON = .T.
	.SCROLLBARS = 0
	.CAPTION = "FTP_FILES_CONTROL"
ENDWITH	



If !Used("ftplog")
  use f:\wh\ftplog.dbf IN 0 EXCLUSIVE 
ENDIF
DELETE FROM ftplog WHERE dd<DATE()-30
pack
If !Used("ftplog2")
  use f:\wh\ftplog2.dbf IN 0
ENDIF

SET STEP ON
CREATE CURSOR ftplogtm (logdata C(250))
Xpath='F:\FTPLogs\'
len1 = Adir(ary1,Xpath+"*.*")
If len1 = 0
	Wait Window "No files found...exiting" Timeout 2
	Close Data All
	On Error
	Return
Endif
For xrow = 1 To len1
COPY FILE \\app1\fmisys\FTPLogs\*.txt TO \\app1\fmisys\FTPLogs\copy\ftpcopy2.txt
	Tfile=Alltrim(ary1[xrow,1])
	tfilepath=Xpath+Alltrim(ary1[xrow,1])
	Appe From "&tfilepath" Type Deli
APPE from \\app1\fmisys\FTPLogs\copy\ftpcopy2.txt TYPE DELI
ENDFOR
SET STEP ON 
SELECT * FROM ftplogtm WHERE 'Sent' $(logdata) INTO CURSOR t2a READWRITE
SELECT distinct(logdata) as logdata FROM t2a INTO cursor t2 READWRITE
SET STEP ON 
SELECT SUBSTR(logdata,9,2)as d1, SUBSTR(logdata,11,3)as d2,SUBSTR(logdata,14,11)as d3, *   FROM t2 INTO CURSOR dt2 READWRITE

REPLACE d2 WITH '01' FOR d2 ='Jan' in dt2  
REPLACE d2 WITH '02' FOR d2 ='Feb' in dt2    
REPLACE d2 WITH '03' FOR d2 ='Mar' in dt2 
REPLACE d2 WITH '04' FOR d2 ='Apr' in dt2 
REPLACE d2 WITH '05' FOR d2 ='May' in dt2 
REPLACE d2 WITH '06' FOR d2 ='Jun' in dt2 
REPLACE d2 WITH '07' FOR d2 ='Jul' in dt2 
REPLACE d2 WITH '08' FOR d2 ='Aug' in dt2 
REPLACE d2 WITH '09' FOR d2 ='Sep' in dt2 
REPLACE d2 WITH '10' FOR d2 ='Oct' in dt2 
REPLACE d2 WITH '11' FOR d2 ='Nov' in dt2  
REPLACE d2 WITH '12' FOR d2 ='Dec' in dt2


select CTOT(ALLTRIM(d2)+"/"+ALLTRIM(d1)+"/"+ALLTRIM(d3)) as dd, *  from dt2 ORDER BY dd into cursor dt3 readwrite  
SET STEP ON 
**select * from dT3 where dd > (select maxdate-30 from ftplog2) iNTO CURSOR T3 READWRITE

SELECT * FROM FTPLOG UNION  SELECT * FROM dT3 INTO CURSOR T4 READWRITE
USE IN FTPLOG
SELECT T4
COPY TO F:\WH\FTPLOG.DBF
USE F:\WH\FTPLOG.DBF IN 0

SET STEP ON 
SELECT MAX(dd) as maxdate FROM dt3 INTO CURSOR t99 READWRITE
USE IN FTPLOG2
SELECT T99
copy to f:\wh\ftplog2.dbf   
DELETE file \\app1\fmisys\FTPLogs\copy\ftpcopy2.txt


SET STEP ON 
Close Data All

DO m:\dev\prg\mj_ftp_files_control.prg
DO m:\dev\prg\bcny_ftp_files_control.prg
**DO m:\dev\prg\merk_ftp_files_control.prg  3/21/17 moved to AS2  TMARG
DO m:\dev\prg\bio_ftp_files_control.prg
DO m:\dev\prg\steel_ftp_files_control.prg
DO m:\dev\prg\2xu_ftp_files_control.prg
schedupdate()
_Screen.Caption=gscreencaption