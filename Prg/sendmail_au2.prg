*******************************************************************************
*Program    	: sendmail_au
*Author     	: Richard (Fanz) Fanslow
*        		: Toll Group
*Project    	:
*Created    	: 20171211
*Copyright  	: � 2017, Toll
*Description  	: replace dart mail with  SQL dbMail
*
*20171211 Fanz - Initial Rollout of  new Email Form
*20171213 Fanz - Added DoNotSend Parameter for testing to capture but DO NOT send email
*20171215 Fanz - Created Method  so buttons could work  rather  then  from Init
*20171215 PG   - collapsed top a simple prg
*20180105 Fanz - adjust array for Error, potential null and numeric values being passed
*20180305 Fanz - modified incoming parameters
*
********************************************************************************
*20171211 Fanz - Creating execution of dbMail
Parameter lcTO,lcFrom,lcSubject,lcCC,lcBody,lcBCC,lcAttach1,lcAttach2,lcAttach3,lcAttach4,lcAttach5,lcAttach6

Local llAutoSend,lcServer,lcAttach
lcServer = "TGFNJSQL01"
If !Directory("")
	Mkdir "f:\MailDump"
Endif
lcAttach = ""

If Type("lcAttach1")#"U"
	If Len(Alltrim(lcAttach1))>3
		lcFilename1 = Alltrim(Substr(lcAttach1,Rat("\",lcAttach1)+1,100))
		lcNewFilename1 = "F:\MailDump\" + lcFilename1
		If File(lcNewFilename1)
			Erase (lcNewFilename1)
		Endif
		oFso = Createobject('Scripting.FileSystemObject')
		oFso.CopyFile(lcAttach1,lcNewFilename1)
		lcAttach = lcAttach+lcNewFilename
		RELEASE oFso
	Endif
Endif

If Type("lcAttach2")#"U"
	If Len(Alltrim(lcAttach2))>3
		lcFilename2 = Alltrim(Substr(lcAttach2,Rat("\",lcAttach2)+1,100))
		lcNewFilename2 = "F:\MailDump\" + lcFilename2
		If File(lcNewFilename2)
			Erase (lcNewFilename2)
		Endif
		oFso = Createobject('Scripting.FileSystemObject')
		oFso.CopyFile(lcAttach2,lcNewFilename2)
		lcAttach = lcAttach+";"+lcNewFilename2
	Endif
Endif

gnconnhandle= Sqlstringconnect("Driver=SQL Server;Server=" + lcServer + ";UID=sa;PWD=B%g23!7#$;Database=Carteret")
okproperties1= SQLSetprop(gnconnhandle, 'Displogin',3)
okproperties2= SQLSetprop(gnconnhandle, 'asynchronous', .F.)
lcSql = ""
lcSql = lcSql + "exec [msdb].[dbo].[sp_send_dbmail] "
lcSql = lcSql + "@profile_name =  'Fanz',"
lcSql = lcSql + "@reply_to = 'no-reply@tollgroup.com',"
lcSql = lcSql + "@body_format =  'TEXT',"
lcSql = lcSql + "@recipients = '" + Strtran(lcTO,",",";") + "',"
lcSql = lcSql + Iif(!Empty(lcCC)     ,"@copy_recipients = '" + Strtran(lcCC,",",";") + "',","")
lcSql = lcSql + Iif(!Empty(lcBCC)    ,"@blind_copy_recipients = '" + Strtran(lcBBC,",",";") + "',","")
lcSql = lcSql + Iif(!Empty(lcFrom)   ,"@from_address = '" + Strtran(lcFrom,",",";") + "',","")
lcSql = lcSql + Iif(!Empty(lcSubject),"@subject = '" + lcSubject + "',","")
lcSql = lcSql + Iif(!Empty(lcBody)   ,"@body = '" + lcBody + "',","")
IF lcAttach# ""
	lcSql = lcSql + Iif(!Empty(lcAttach) ,"@file_attachments  = '" + lcAttach + "'","")
endif
lcSql = Iif(Right(Alltrim(lcSql),1)=","  ,Substr(Alltrim(lcSql),1,Len(Alltrim(lcSql))-1), Substr(Alltrim(lcSql),1,Len(Alltrim(lcSql))))
lcSql = lcSql + ";"
*20171213 Fanz - Added Criteria for DONOT SEND eMail

gConnected = SQLExec(gnconnhandle, lcSql,"vData")
If gConnected<=0
	Set Step On
	Aerror(arrError)
	lcMsg1 = Alltrim(Padl(arrError[1,1],250," "))
	lcMsg2 = Alltrim(Padl(arrError[1,2],250," "))
	lcMsg3 = Alltrim(Padl(arrError[1,3],250," "))
	lcMsg4 = Alltrim(Padl(arrError[1,4],250," "))
	lcMsg5 = Alltrim(Padl(arrError[1,5],250," "))
	lcMsg6 = Alltrim(Padl(arrError[1,6],250," "))
	lcMsg7 = Alltrim(Padl(arrError[1,7],250," "))
	tmessagey = "SendMail_Au Error: " +Ttoc(Datetime()) + Chr(13);
		+ " Array Value 01 : " + lcMsg1 + Chr(13) ;
		+ " Array Value 02 : " + lcMsg2 + Chr(13) ;
		+ " Array Value 03 : " + lcMsg3 + Chr(13) ;
		+ " Array Value 04 : " + lcMsg4 + Chr(13) ;
		+ " Array Value 05 : " + lcMsg5 + Chr(13) ;
		+ " Array Value 06 : " + lcMsg6 + Chr(13) ;
		+ " Array Value 07 : " + lcMsg7 + Chr(13)

	tFrom     = "NoReply <noreply@tollgroup.com>"
	tsubjecty= "SendMail_Au Error: " +Ttoc(Datetime())
	tattachy = ""
	lDontSendy = .F.
	tsendtoy = "richard.fanslow@tollgroup.com,paul.gaidis@tollgroup.com"
	tccy = ""
	tbbcy = " "
	Do m:\dev\prg\sendmail_au With tsendtoy,tFrom,tsubjecty,tccy,tattachy,tmessagey,tbbcy
Endif
gConnected = SQLDisconnect(gnconnhandle)


*20171211 Fanz - Need to added a SQL insert to capture data
Release gnconnhandle, okproperties1, okproperties2,lcSql,gConnected
gnconnhandle= Sqlstringconnect("Driver=SQL Server;Server=" + lcServer + ";UID=sa;PWD=B%g23!7#$;Database=Carteret")
okproperties1= SQLSetprop(gnconnhandle, 'Displogin',3)
okproperties2= SQLSetprop(gnconnhandle, 'asynchronous', .F.)
lcSql = ""
lcSql = lcSql + "Insert into [Carteret].[dbo].[MailSent] ([Servername],[to],[from],[cc],[bcc],[Subject],[Body],[Sent],[Attachments]) values ("
lcSql = lcSql + "'" + Alltrim(lcServer)  + "',"
lcSql = lcSql + "'" + Alltrim(Strtran(lcTO,",",";"))      + "',"
lcSql = lcSql + "'" + Alltrim(Strtran(lcFrom,",",";"))    + "',"
lcSql = lcSql + "'" + Alltrim(Strtran(lcCC,",",";"))      + "',"
lcSql = lcSql + "'" + Alltrim(Strtran(lcBCC,",",";"))     + "',"
lcSql = lcSql + "'" + Alltrim(Strtran(lcSubject,",","")) + "',"
lcSql = lcSql + "'" + Alltrim(Strtran(lcBody,",","")) + "',"
lcSql = lcSql + "'" + "1" + "',"
lcSql = lcSql + "'" + Alltrim(lcAttach)  + "')"
gConnected = SQLExec(gnconnhandle, lcSql,"vData")
If gConnected<=0
	Aerror(arrError)
	lcMsg1 = Alltrim(Padl(arrError[1,1],250," "))
	lcMsg2 = Alltrim(Padl(arrError[1,2],250," "))
	lcMsg3 = Alltrim(Padl(arrError[1,3],250," "))
	lcMsg4 = Alltrim(Padl(arrError[1,4],250," "))
	lcMsg5 = Alltrim(Padl(arrError[1,5],250," "))
	lcMsg6 = Alltrim(Padl(arrError[1,6],250," "))
	lcMsg7 = Alltrim(Padl(arrError[1,7],250," "))
	tmessagey = "SendMail_Au Error: " +Ttoc(Datetime()) + Chr(13);
		+ " Array Value 01 : " + lcMsg1 + Chr(13) ;
		+ " Array Value 02 : " + lcMsg2 + Chr(13) ;
		+ " Array Value 03 : " + lcMsg3 + Chr(13) ;
		+ " Array Value 04 : " + lcMsg4 + Chr(13) ;
		+ " Array Value 05 : " + lcMsg5 + Chr(13) ;
		+ " Array Value 06 : " + lcMsg6 + Chr(13) ;
		+ " Array Value 07 : " + lcMsg7 + Chr(13)

	tFrom     = "NoReply <noreply@tollgroup.com>"
	tsubjecty= "SendMail_Au Error: " +Ttoc(Datetime())
	tattachy = ""
	lDontSendy = .F.
	lcModey = ""
	tsendtoy = "richard.fanslow@tollgroup.com"
	tccy = ""
	tbbcy = ""

	Do m:\dev\prg\sendmail_au With tsendtoy,tFrom,tsubjecty,tccy,tattachy,tmessagey
Endif
gConnected = SQLDisconnect(gnconnhandle)
Release gnconnhandle, okproperties1, okproperties2,lSql,gConnected
*************************************************************************************************************
