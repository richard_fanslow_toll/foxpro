* Create spreadsheet of temps who had more than 8 hours in the prior day.
* Parameter determine Temp co - currently supports only Diamond Staffing NJ param = 'J'
*
* 4/25/2018 MB: changed fmiint.com emails to Toll emails
*
* EXE = F:\UTIL\KRONOS\KRONOSLONGSHIFTRPT.EXE

LPARAMETERS tcTempCo

*!*	runack("KRONOSLONGSHIFTRPT")

LOCAL lnError, lcProcessName

DECLARE INTEGER CreateMutex IN Win32API ;
	STRING @lpMutexAttributes, ;
	STRING bInitialOwner, ;
	STRING @lpName

DECLARE INTEGER GetLastError IN Win32API

* if running in EXE check to see if already running
IF _VFP.STARTMODE > 0 THEN
	lcProcessName = "KRONOSLONGSHIFTRPT"
	CreateMutex(0,CHR(1),@lcProcessName)
	lnError = GetLastError()
	IF lnError = 183
		WAIT WINDOW TIMEOUT 10 "KRONOS LONG SHIFT RPT process is already running..."
		RETURN .F.
	ENDIF
ENDIF

_SCREEN.CAPTION = "KRONOSLONGSHIFTRPT"

utilsetup("KRONOSLONGSHIFTRPT")


loKRONOSLONGSHIFTRPT = CREATEOBJECT('KRONOSLONGSHIFTRPT')
loKRONOSLONGSHIFTRPT.MAIN( tcTempCo )
CLOSE DATABASES ALL

SCHEDUPDATE()

WAIT WINDOW TIMEOUT 60  && TO FORCE THIS PROCESS TO RUN FOR > 1 MINUTE, I THINK IF IT RUNS WITHIN 1 MINUTE STM RUNS IT TWICE

RETURN

#DEFINE LOGIT 1
#DEFINE WAITIT 2
#DEFINE NOWAITIT 4
#DEFINE SENDIT 8
#DEFINE NOCRLF 16
#DEFINE CRLF CHR(13) + CHR(10)
#DEFINE LF CHR(10)
#DEFINE RETURN_DATA_MANDATORY .F.
#DEFINE RETURN_DATA_NOT_MANDATORY .T.
#DEFINE DAILY_OT_MULTIPLIER 1.5
#DEFINE DAILY_OT20_MULTIPLIER 2.0

DEFINE CLASS KRONOSLONGSHIFTRPT AS CUSTOM

	cProcessName = 'KRONOSLONGSHIFTRPT'

	lTestMode = .F.  && .F. FOR NORMAL OPERATION, .T. TO SUPPRESS ALL BUT DEVELOPER EMAILS AND CHANGE INPUT TABLE, ETC.

	* which payroll server, and associated connections, to hit
	lUsePAYROLL2Server = .T.

	lAutoYield = .T.

	cStartTime = TTOC(DATETIME())

	cTempCo = ''
	cTempCoName = ''

	* date properties
	dtNow = DATETIME()

	dEndDate = {}
	dStartDate = {}
	dToday = DATE()
	* to force different pay weeks
	*dToday = {^2015-01-26}

	cToday = DTOC(DATE())
	
	* connection properties
	nSQLHandle = 0

	* file properties
	nFileHandle = 0

	* sql/report filter properties
	cTempCoWhere = ""
	cTempCoWhere2 = ""
	cTempCo = ""
	cTempCoName = ""
	cExtraWhere = ""
	cExtraWhere2 = ""
	* hours discrepancy testing props
	nMinHoursTest = 4
	nMaxHoursTest = 12


	* wait window properties
	nWaitWindowTimeout = 2
	lWaitWindowIsOn = .T.

	* logfile properties
	lLoggingIsOn = .T.
	cLogFile = 'F:\UTIL\KRONOS\Logfiles\KRONOSLONGSHIFTRPT_log.txt'

	* processing properties
	lCheckBio = .T.

	* INTERNAL email properties
	lSendInternalEmailIsOn = .T.
	cFrom = 'mark.bennett@tollgroup.com'
	cSendTo = 'mark.bennett@tollgroup.com'
	cCC = ''
	cSubject = 'Temp Long shift Report'
	cAttach = ''
	cBodyText = ''

	cCOMPUTERNAME = ''
	cUSERNAME = ''

	FUNCTION INIT
		IF NOT DODEFAULT()
			RETURN .F.
		ENDIF
		WITH THIS
			CLEAR
			*SET RESOURCE OFF
			CLOSE DATA
			SET CENTURY ON
			SET DATE YMD
			SET HOURS TO 24
			SET ANSI ON
			SET TALK OFF
			SET DELETED ON
			SET CONSOLE OFF
			SET EXCLUSIVE OFF
			SET SAFETY OFF
			SET EXACT OFF
			SET STATUS BAR ON
			SET SYSMENU OFF
			SET ENGINEBEHAVIOR 70
			SET DECIMALS TO 4
			_VFP.AUTOYIELD = .lAutoYield
			.cCOMPUTERNAME = GETENV("COMPUTERNAME")
			.cUSERNAME = GETENV("USERNAME")
			IF .lTestMode THEN
				.cLogFile = 'F:\UTIL\KRONOS\Logfiles\KRONOSLONGSHIFTRPT_log_TESTMODE.txt'
			ENDIF
			.lLoggingIsOn = .lLoggingIsOn AND NOT EMPTY(.cLogFile)
			IF .lLoggingIsOn THEN
				SET ALTERNATE TO (.cLogFile) ADDITIVE
				SET ALTERNATE ON
			ENDIF
		ENDWITH
	ENDFUNC


	FUNCTION DESTROY
		WITH THIS
			IF .lLoggingIsOn  THEN
				SET ALTERNATE OFF
				SET ALTERNATE TO
			ENDIF
		ENDWITH
		DODEFAULT()
	ENDFUNC


	FUNCTION MAIN
		LPARAMETERS tcTempCo
		WITH THIS
			LOCAL lnNumberOfErrors, lcSQL, lcSQL2, lcSQL3, lcSQL4, lcSQL5, loError, lcTopBodyText, llValid
			LOCAL lcXLFileName, ldStartDate, ldEndDate, lcStartDate, lcEndDate, ldDate
			LOCAL lcSQLStartDate, lcSQLEndDate, lcXLFileName, ldYesterday
			LOCAL oExcel, oWorkbook, oWorksheet, lnRow, lcRow, lnStartRow, lcStartRow, lnEndRow, lnPP, lcPP, lnNN, lcNN, lcJJ
			LOCAL lcColsToProcess, lnColumn, lcBoldColumn, i, j, lcBoldRow, luCellValue, lcTargetDirectory, ldPayDate, lcFiletoSaveAs
			LOCAL lcFileDate, lcDivision, lcTitle, llSaveAgain, lcPayCode, lcSpecialWhere
			LOCAL lcSQLToday, lcSQLTomorrow, ldToday, ldTomorrow, lcExcluded9999s, lcFoxProTimeReviewerTable, lcMissingApprovals


			TRY
				lnNumberOfErrors = 0
				lcTopBodyText = ''

				.TrackProgress('Kronos Long Shift Report Report process started....', LOGIT+SENDIT)
				.TrackProgress('COMPUTERNAME = ' + .cCOMPUTERNAME, LOGIT+SENDIT)
				.TrackProgress('USERNAME = ' + .cUSERNAME, LOGIT+SENDIT)
				.TrackProgress('Project =  KRONOSLONGSHIFTRPT', LOGIT+SENDIT)
				IF .lTestMode THEN
					.TrackProgress('                        TEST MODE                        ', LOGIT+SENDIT)
				ENDIF
				
				IF EMPTY(tcTempCo) OR (TYPE('tcTempCo') <> 'C') THEN
					.TrackProgress('ERROR:  missing or invalid tempco parameter!', LOGIT+SENDIT)
					THROW
				ENDIF
				.TrackProgress('TEMPCO parameter = ' + tcTempCo, LOGIT+SENDIT)
				
				.cTempCo = tcTempCo
				
				DO CASE
					CASE .cTempCo = 'D'
						.cTempCoName = 'Quality Temps NJ'
						.cSendTo = 'sueoqualitytemps@outlook.com, yerangeles04@outlook.com'
						.cCC = 'mark.bennett@tollgroup.com'
					CASE .cTempCo = 'G'
						.cTempCoName = 'Chartwell NJ'
						.cSendTo = 'toll@chartwellstaff.com'
						.cCC = 'mark.bennett@tollgroup.com'
					OTHERWISE
						.TrackProgress('Unexpected tempco parameter = ' + .cTempCo, LOGIT+SENDIT)
						THROW
				ENDCASE
				
				IF .lTestMode THEN
					.cSendTo = 'mark.bennett@tollgroup.com'
					.cCC = ''
				ENDIF
				
				ldToday = .dToday
				ldYesterday = ldToday - 1
				
				.cSubject = .cTempCoName + ' Long shift Report for ' + TRANSFORM(ldYesterday)

				* report on yesterday

				ldStartDate = ldYesterday
				ldEndDate = ldYesterday


				*!*	*!*			***********************************
				*!*							ldStartDate = ldYesterday
				*!*							ldEndDate = ldYesterday
				*!*	*!*			***********************************


				.dEndDate = ldEndDate
				.dStartDate = ldStartDate

				lcSpecialWhere = ""
				************************************************************

				SET DATE AMERICAN

				lcStartDate = STRTRAN(DTOC(ldStartDate),"/","-")
				lcEndDate = STRTRAN(DTOC(ldEndDate),"/","-")

				SET DATE YMD

				*	 create "YYYYMMDD" safe date format for use in SQL query
				* NOTE: for SQL we are going to use < EndDate ( not <= ), so we are going to add 1 to that date here for querying purposes.
				lcSQLStartDate = STRTRAN(DTOC(ldStartDate),"/","")
				lcSQLEndDate = STRTRAN(DTOC(ldEndDate + 1),"/","")
				lcSQLToday = STRTRAN(DTOC(ldToday),"/","")


				SET DATE AMERICAN


				IF USED('CURWTKHOURS') THEN
					USE IN CURWTKHOURS
				ENDIF
				IF USED('CURWTKHOURSPRE') THEN
					USE IN CURWTKHOURSPRE
				ENDIF


				SET TEXTMERGE ON
				TEXT TO	lcSQL NOSHOW

SELECT
D.LABORLEV2NM AS DIVISION,
D.LABORLEV3NM AS DEPT,
D.LABORLEV3DSC AS DEPTDESC,
C.FULLNM AS EMPLOYEE,
SUM(A.DURATIONSECSQTY / 3600.00) AS TOTHOURS
FROM WFCTOTAL A
JOIN WTKEMPLOYEE B
ON B.EMPLOYEEID = A.EMPLOYEEID
JOIN PERSON C
ON C.PERSONID = B.PERSONID
JOIN LABORACCT D
ON D.LABORACCTID = A.LABORACCTID
WHERE (A.APPLYDTM >= '<<lcSQLStartDate>>')
AND (A.APPLYDTM < '<<lcSQLEndDate>>')
AND A.DURATIONSECSQTY > 0.00
AND (D.LABORLEV1NM = 'TMP')
AND (D.LABORLEV5NM  = '<<.cTempCo>>')
GROUP BY D.LABORLEV2NM, D.LABORLEV3NM, D.LABORLEV3DSC, C.FULLNM
ORDER BY D.LABORLEV2NM, D.LABORLEV3NM, C.FULLNM

				ENDTEXT
				SET TEXTMERGE OFF

*!*	JOIN PAYCODE E
*!*	ON E.PAYCODEID = A.PAYCODEID

				IF .lTestMode THEN
					.TrackProgress('', LOGIT+SENDIT)
					.TrackProgress('lcSQL =' + lcSQL, LOGIT+SENDIT)
				ENDIF

				.nSQLHandle = SQLCONNECT("WTK2","sa","kr0n1tes")

				IF .nSQLHandle > 0 THEN

					IF .ExecSQL(lcSQL, 'CURWTKHOURSPRE', RETURN_DATA_MANDATORY) THEN

						SELECT CURWTKHOURSPRE
						SCAN
							IF TOTHOURS <= 8.0 THEN
								DELETE
							ENDIF
						ENDSCAN

						WAIT WINDOW NOWAIT "Preparing data..."

						SELECT CURWTKHOURSPRE
						GOTO TOP
						IF EOF() THEN
							.TrackProgress("There were no shifts longer than 8 hours!", LOGIT+SENDIT)
						ELSE

							lcFilePrefix = " Long Shift Report "
							lcFileDate = DTOS(ldYesterday)

							lcTargetDirectory = "F:\UTIL\Kronos\TEMPTIMEREPORTS\" 

							lcFiletoSaveAs = lcTargetDirectory + .cTempCoName + lcFilePrefix + lcFileDate
							lcXLFileName = lcFiletoSaveAs + ".XLS"

							* create primary, detail spreadsheet

							WAIT WINDOW NOWAIT "Opening Excel..."
							oExcel = CREATEOBJECT("excel.application")
							oExcel.VISIBLE = .F.
							oWorkbook = oExcel.workbooks.OPEN("F:\UTIL\KRONOS\TEMPLATES\KRONOSLONGSHIFTRPT.XLS")

							* if target directory exists, save there
							llSaveAgain = .F.
							IF DIRECTORY(lcTargetDirectory) THEN
								IF FILE(lcXLFileName) THEN
									DELETE FILE (lcXLFileName)
								ENDIF
								oWorkbook.SAVEAS(lcFiletoSaveAs)
								* set flag to save again after sheet is populated
								llSaveAgain = .T.
							ENDIF
							***********************************************************************************************************************
							***********************************************************************************************************************
							WAIT WINDOW NOWAIT "Building spreadsheet..."

							lnRow = 1

							oWorksheet = oWorkbook.Worksheets[1]
							oWorksheet.RANGE("A2","F100").clearcontents()

							oWorksheet.RANGE("A2","F100").FONT.SIZE = 12
							oWorksheet.RANGE("A2","F100").FONT.NAME = "Arial"
							oWorksheet.RANGE("A2","F100").FONT.bold = .F.

							* main scan/processing
							SELECT CURWTKHOURSPRE

							SCAN
								lnRow = lnRow + 1
								lcRow = LTRIM(STR(lnRow))
								
								oWorksheet.RANGE("A" + lcRow).VALUE = ldYesterday
								oWorksheet.RANGE("B" + lcRow).VALUE = "'" + CURWTKHOURSPRE.DIVISION
								oWorksheet.RANGE("C" + lcRow).VALUE = "'" + CURWTKHOURSPRE.DEPT
								oWorksheet.RANGE("D" + lcRow).VALUE = CURWTKHOURSPRE.DEPTDESC
								oWorksheet.RANGE("E" + lcRow).VALUE = ALLTRIM(CURWTKHOURSPRE.EMPLOYEE)
								oWorksheet.RANGE("F" + lcRow).VALUE = CURWTKHOURSPRE.TOTHOURS

							ENDSCAN


							*******************************************************************************************
							*******************************************************************************************

							* save again
							IF llSaveAgain THEN
								oWorkbook.SAVE()
							ENDIF
							oWorkbook.CLOSE()

							oExcel.QUIT()

							IF FILE(lcXLFileName) THEN
								.cAttach = lcXLFileName
								.TrackProgress('Attached Temp time File : ' + lcXLFileName, LOGIT+SENDIT)
								.TrackProgress('Kronos Long Shift Report process ended normally.', LOGIT+SENDIT+NOWAITIT)
							ELSE
								.TrackProgress('!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!', LOGIT+SENDIT)
								.TrackProgress("ERROR: There was a problem attaching File : " + lcXLFileName, LOGIT+SENDIT)
								.TrackProgress('!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!', LOGIT+SENDIT)
							ENDIF

						ENDIF  && EOF() CURTEMPS

					ENDIF  &&  .ExecSQL(lcSQL, 'CURWTKEMPLOYEE', RETURN_DATA_MANDATORY)

					CLOSE DATABASES ALL

				ELSE
					* connection error
					.TrackProgress('Unable to connect to WTK System.', LOGIT+SENDIT)
				ENDIF   &&  .nSQLHandle > 0
				
			CATCH TO loError

				.TrackProgress('There was an error.',LOGIT+SENDIT)
				.TrackProgress('The Error # is: ' + TRANSFORM(loError.ERRORNO), LOGIT+SENDIT)
				.TrackProgress('The Error Message is: ' + TRANSFORM(loError.MESSAGE), LOGIT+SENDIT)
				.TrackProgress('The Error occurred in Program: ' + TRANSFORM(loError.PROCEDURE), LOGIT+SENDIT)
				.TrackProgress('The Error occurred at line #: ' + TRANSFORM(loError.LINENO), LOGIT+SENDIT)
				lnNumberOfErrors = lnNumberOfErrors + 1
				IF TYPE('oExcel') = 'O' AND NOT ISNULL(oExcel) THEN
					oExcel.QUIT()
				ENDIF

			ENDTRY

			WAIT CLEAR
			***************** INTERNAL email results ******************************
			.TrackProgress('About to send status email.',LOGIT)
			.TrackProgress('==================================================================================================================', SENDIT)
			.TrackProgress('The logfile is: ' + .cLogFile, SENDIT)
			.TrackProgress('Kronos Long Shift Report process started: ' + .cStartTime, LOGIT+SENDIT)
			.TrackProgress('Kronos Long Shift Report process finished: ' + TTOC(DATETIME()), LOGIT+SENDIT)

			IF .lSendInternalEmailIsOn THEN
				* try to trap error from not having dartmail dll's registered on user's pc...
				TRY
					DO FORM dartmail2 WITH .cSendTo,.cFrom,.cSubject,.cCC,.cAttach,.cBodyText,"A"
					.TrackProgress('Sent status email.',LOGIT)
				CATCH TO loError
					*=MESSAGEBOX('There was an error sending the status email. Please contact Mark Bennett.',0+64,"Kronos Long Shift Report")
					.TrackProgress('There was an error sending the status email.',LOGIT)
					.TrackProgress('The Error # is: ' + TRANSFORM(loError.ERRORNO), LOGIT)
					.TrackProgress('The Error Message is: ' + TRANSFORM(loError.MESSAGE), LOGIT)
					.TrackProgress('The Error occurred in Program: ' + TRANSFORM(loError.PROCEDURE), LOGIT)
					.TrackProgress('The Error occurred at line #: ' + TRANSFORM(loError.LINENO), LOGIT)
					lnNumberOfErrors = lnNumberOfErrors + 1
					CLOSE DATA
				ENDTRY

			ELSE
				.TrackProgress('Did not send status email: THIS.lSendInternalEmailIsOn = FALSE.',LOGIT)
			ENDIF

		ENDWITH
		RETURN
	ENDFUNC && main





	FUNCTION ExecSQL
		LPARAMETERS tcSQL, tcCursorName, tlNoDataReturnedIsOkay
		LOCAL llRetval, lnResult
		WITH THIS
			* close target cursor if it's open
			IF USED(tcCursorName)
				USE IN (tcCursorName)
			ENDIF
			WAIT WINDOW "Fetching remote data; please wait..." NOWAIT
			lnResult = SQLEXEC(.nSQLHandle, tcSQL, tcCursorName)
			llRetval = ( lnResult > 0 )
			IF llRetval THEN
				* see if any data came back
				IF NOT tlNoDataReturnedIsOkay THEN
					IF (NOT USED(tcCursorName)) OR EOF(tcCursorName) THEN
						llRetval = .F.
						.TrackProgress('No data was returned by this query: ' +CRLF + tcSQL, LOGIT+SENDIT)
					ENDIF
				ENDIF
			ELSE
				.TrackProgress("Result Code [" + TRANSFORM(lnResult) + "] returned by this query: " + tcSQL, LOGIT+SENDIT)
			ENDIF
			WAIT CLEAR
			RETURN llRetval
		ENDWITH
	ENDFUNC


	PROCEDURE TrackProgress
		* do any combination of Wait Window, writing to logfile, and adding to body of email,
		* based on nFlags parameter.
		LPARAMETERS tcExpression, tnFlags
		LOCAL lcCRLF
		WITH THIS
			IF BITAND(tnFlags,NOCRLF) = NOCRLF THEN
				lcCRLF = ""
			ELSE
				lcCRLF = CRLF
			ENDIF
			IF BITAND(tnFlags,LOGIT) = LOGIT THEN
				IF .lLoggingIsOn THEN
					?
					? .cProcessName + "  " + TTOC(DATETIME()) + ": " + tcExpression
				ENDIF
			ENDIF
			IF .lWaitWindowIsOn THEN
				IF BITAND(tnFlags,NOWAITIT) = NOWAITIT THEN
					WAIT WINDOW tcExpression NOWAIT
				ENDIF
				IF BITAND(tnFlags,WAITIT) = WAITIT THEN
					WAIT WINDOW tcExpression TIMEOUT .nWaitWindowTimeout
				ENDIF
			ENDIF
			IF BITAND(tnFlags,SENDIT) = SENDIT THEN
				IF .lSendInternalEmailIsOn THEN
					.cBodyText = .cBodyText + tcExpression + CRLF + lcCRLF
				ENDIF
			ENDIF
		ENDWITH
	ENDPROC  &&  TrackProgress


ENDDEFINE
