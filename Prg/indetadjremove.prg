* runs sqlexecs to update removeqty in indet/inloc/inwolog/adj
* has to be a prg because this is sometimes called from a prg
* used to be in frmpmdataentryscado.updatebuffers()


* update indet & inwolog removeqty/remainqty

if used("indetremove")
	set step on 
	
	select indetremove
	scan for useqty>0
		xsqlexec("update indet set removeqty=removeqty+"+transform(indetremove.useqty)+", " + ;
			"remainqty=remainqty-"+transform(indetremove.useqty)+" " + ;
			"where indetid="+transform(indetremove.indetid),,,"wh")

		if !indetremove.units
			xsqlexec("update inwolog set removeqty=removeqty+"+transform(indetremove.useqty)+", " + ;
				"remainqty=remainqty-"+transform(indetremove.useqty)+" " + ;
				"where wo_num="+transform(indetremove.wo_num),,,"wh")
		else
			xsqlexec("update inwolog set unitsoutqty=unitsoutqty+"+transform(indetremove.useqty)+", " + ;
				"unitsopenqty=unitsopenqty-"+transform(indetremove.useqty)+" " + ;
				"where wo_num="+transform(indetremove.wo_num),,,"wh")
		endif
	endscan
	
	use in indetremove
endif

if used("indetbackin")
	select indetbackin
	scan for useqty>0
		xsqlexec("update indet set removeqty=removeqty-"+transform(indetbackin.useqty)+", " + ;
			"remainqty=remainqty+"+transform(indetbackin.useqty)+" " + ;
			"where indetid="+transform(indetbackin.indetid),,,"wh")

		if !indetbackin.units
			xsqlexec("update inwolog set removeqty=removeqty-"+transform(indetbackin.useqty)+", " + ;
				"unitsopenqty=unitsopenqty-"+transform(indetbackin.useqty)+" " + ;
				"where wo_num="+transform(indetbackin.wo_num),,,"wh")
		else
			xsqlexec("update inwolog set unitsoutqty=unitsoutqty-"+transform(indetbackin.useqty)+", " + ;
				"remainqty=remainqty-"+transform(indetbackin.useqty)+" " + ;
				"where wo_num="+transform(indetbackin.wo_num),,,"wh")
		endif
	endscan
	
	use in indetbackin
endif

* update inloc removeqty/remainqty

if used("inlocremove")
	select inlocremove
	scan for useqty>0
		xsqlexec("update inloc set removeqty=removeqty+"+transform(inlocremove.useqty)+", " + ;
			"remainqty=remainqty-"+transform(inlocremove.useqty)+" " + ;
			"where inlocid="+transform(inlocremove.inlocid),,,"wh")
	endscan
	
	use in inlocremove
endif

if used("inlocbackin")
	select inlocbackin
	scan for useqty>0
		xsqlexec("update inloc set removeqty=removeqty-"+transform(inlocbackin.useqty)+", " + ;
			"remainqty=remainqty+"+transform(inlocbackin.useqty)+" " + ;
			"where inlocid="+transform(inlocbackin.inlocid),,,"wh")
	endscan
	
	use in inlocbackin
endif

* update adj removeqty/remainqty

if used("adjremove")
	select adjremove
	scan for useqty>0
		xsqlexec("update adj set removeqty=removeqty+"+transform(adjremove.useqty)+", " + ;
			"remainqty=remainqty-"+transform(adjremove.useqty)+" " + ;
			"where adjid="+transform(adjremove.adjid),,,"wh")
	endscan
	
	use in adjremove
endif

if used("adjbackin")
	select adjbackin
	scan for useqty>0
		xsqlexec("update adj set removeqty=removeqty-"+transform(adjbackin.useqty)+", " + ;
			"remainqty=remainqty+"+transform(adjbackin.useqty)+" " + ;
			"where adjid="+transform(adjbackin.adjid),,,"wh")
	endscan
	
	use in adjbackin
endif
