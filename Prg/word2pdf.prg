lparameters xfilename

oWord = CREATEOBJECT("word.application")
oWord.VISIBLE = .F.
oWord.DisplayAlerts = .F.

oWordDocument = oWord.Documents.Add() 

***************************************************************************
* optional steps; but should make imported images larger and easier to read
*!*	1 Inch = 72 Points [Postscript]
* so 36 points = 1/2 inch

WITH oWordDocument.PageSetup
	.Orientation = 1	&& portrait=0 
	.TopMargin = oWord.InchesToPoints(0.5)
	.BottomMargin = oWord.InchesToPoints(0.5) 
	.LeftMargin = oWord.InchesToPoints(0.5) 
	.RightMargin = oWord.InchesToPoints(0.5) 
	.Gutter = oWord.InchesToPoints(0)
	.HeaderDistance = oWord.InchesToPoints(0.5)
	.FooterDistance = oWord.InchesToPoints(0.5)
ENDWITH

oWordDocument.SaveAs("F:\UTIL\MAKEPDF\DELETEME.DOC")
oRange = oWordDocument.Range()

xfiletype=upper(justext(xfilename))

xpdffilename=justpath(xfilename)+"\"+juststem(xfilename)+".pdf"

do case
case inlist(xfiletype,"TXT")
	lcText = FILETOSTR(xfilename)
	oRange.InsertAfter(lcText)
case inlist(xfiletype,"TIF","JPG")
	oWordDocument.InlineShapes.AddPicture(xfilename,.f.,.t.)
otherwise
	email("Dyoung@fmiint.com","INFO: unexpected file type in wopdf",xfilename,,,,.t.,,,,,.t.)
	xpdffilename=""
endcase

if !empty(xpdffilename)
	oWordDocument.SAVEAS(xpdffilename,17)
endif

oWordDocument.CLOSE()
oWordDocument = NULL
oWord.QUIT()
oWord = NULL

return xpdffilename