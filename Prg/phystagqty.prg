lparameters xwhseloc, xaccountid, xphystable

if xphystable
	xphyscursor="phys"
else
	xphyscursor="vphys"
endif

select phystag
if reccount()=0
	select phystag
	scatter memvar memo blank
	m.accountid=xaccountid
	m.acctname=acctname(xaccountid)
	m.whseloc=xwhseloc
	m.loctype=loctype(xwhseloc)
	m.office=gmasteroffice
	m.mod=goffice
	m.phystagid=dygenpk("phystag","whall")
	insert into phystag from memvar
endif

create cursor xphystag (units l, upc c(14), style c(20), color c(10), id c(10), ;
	pack c(10), whseloc c(7), physqty n(6), invqty n(6))

index on transform(units)+style+color+id+pack tag zorder

select accountid, units, style, color, id, pack, whseloc, sum(totqty*val(pack)) as totqty ;
	from &xphyscursor with (buffering=.t.) where whseloc=xwhseloc and accountid=xaccountid and zcount="F" ;
	group by accountid, units,style,color,id,pack,whseloc ;
	into cursor xphys

index on trans(units)+style+color+id+pack+whseloc tag whmatch
set order to

scan
	if !seek(str(accountid,4)+trans(units)+style+color+id+pack+whseloc,"invenloc","matchwh")
		scatter memvar
		m.physqty=m.totqty
		m.invqty=0
		insert into xphystag from memvar
	else
		if xphys.totqty#invenloc.locqty
			scatter memvar
			m.physqty=xphys.totqty
			m.invqty=invenloc.locqty*val(invenloc.pack)
			insert into xphystag from memvar
		endif			
	endif
endscan

select invenloc
scan for whseloc=xwhseloc and accountid=xaccountid and !inlist(whseloc,"CUT","CYCLE","XRACK")
	if !seek(trans(units)+style+color+id+pack+whseloc,"xphys","whmatch")
		scatter memvar
		m.physqty=0
		m.invqty=m.locqty*val(m.pack)
		insert into xphystag from memvar
	endif
endscan

xdiffqty=0

select xphystag
scan 
	xdiffqty=xdiffqty+abs(physqty-invqty)
endscan

return xdiffqty
