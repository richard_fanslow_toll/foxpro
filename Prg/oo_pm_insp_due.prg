* Report on active OO Trucks that have not had a PM Inspection in 75 days.
* For Gary Fechtenburg
* EXE = F:\UTIL\TRUCKING\OO_PM_INSP_DUE.EXE



runack("OO_PM_INSP_DUE")

LOCAL lnError, lcProcessName, loOO_PM_INSP_DUE

DECLARE INTEGER CreateMutex IN Win32API ;
	STRING @lpMutexAttributes, ;
	STRING bInitialOwner, ;
	STRING @lpName

DECLARE INTEGER GetLastError IN Win32API

* if running in EXE check to see if already running
IF _VFP.STARTMODE > 0 THEN
	lcProcessName = "OO_PM_INSP_DUE"
	CreateMutex(0,CHR(1),@lcProcessName)
	lnError = GetLastError()
	IF lnError = 183
		WAIT WINDOW TIMEOUT 10 "OO Pm Inspection Due report process is already running..."
		RETURN .F.
	ENDIF
ENDIF

utilsetup("OO_PM_INSP_DUE")


loOO_PM_INSP_DUE = CREATEOBJECT('OO_PM_INSP_DUE')
loOO_PM_INSP_DUE.MAIN()

schedupdate()

CLOSE DATABASES ALL
RETURN

#DEFINE LOGIT 1
#DEFINE WAITIT 2
#DEFINE NOWAITIT 4
#DEFINE SENDIT 8
#DEFINE CRLF CHR(13) + CHR(10)
#DEFINE LF CHR(10)
#DEFINE RETURN_DATA_MANDATORY .F.
#DEFINE RETURN_DATA_NOT_MANDATORY .T.
#DEFINE DAILY_OT_MULTIPLIER 1.5
#DEFINE DAILY_OT20_MULTIPLIER 2.0
#DEFINE xlContinuous 1
#DEFINE xlMedium -4138
#DEFINE xlDiagonalDown 5
#DEFINE xlDiagonalUp 6
#DEFINE xlInsideHorizontal 12
#DEFINE xlInsideVertical 11
#DEFINE xlThick 4
#DEFINE xlThin 2
#DEFINE xlNone -4142
#DEFINE xlEdgeBottom 9
#DEFINE xlEdgeLeft 7
#DEFINE xlEdgeRight 10
#DEFINE xlEdgeTop 8

DEFINE CLASS OO_PM_INSP_DUE AS CUSTOM

	cProcessName = 'OO_PM_INSP_DUE'

	lTestMode = .F.  && .F. FOR NORMAL OPERATION, .T. TO SUPPRESS ALL BUT DEVELOPER EMAILS AND CHANGE INPUT TABLE, ETC.

	lAutoYield = .T.

	cStartTime = TTOC(DATETIME())

	* processing properties
	cProcessDesc = "OO PM Inspection Due Report"

	* object properties
	oExcel = NULL
	oWorkbook = NULL
	oWorksheet = NULL

	* file properties
	nFileHandle = 0
	cFiletoSaveAs = ""

	* date properties
	dtNow = DATETIME()

	dToday = DATE()

	cToday = DTOC(DATE())

	* wait window properties
	nWaitWindowTimeout = 2
	lWaitWindowIsOn = .T.

	* logfile properties
	lLoggingIsOn = .T.
	cLogFile = 'F:\UTIL\TRUCKING\LOGFILES\OO_PM_INSP_DUE_LOG.TXT'

	* INTERNAL email properties
	lSendInternalEmailIsOn = .T.
	cFrom = 'Mark.Bennett@Tollgroup.com'
	*cSendTo = 'mbennett@fmiint.com'
	cSendTo = 'April.Fraze@Tollgroup.com, Joe.Nazzaro@Tollgroup.com, Manny.Leandro@Tollgroup.com'
	cCC = 'Gary.Fechtenburg@Tollgroup.com, mbennett@fmiint.com'
	cSubject = ''
	cAttach = ''
	cBodyText = ''
	cTopBodyText = ''

	cCOMPUTERNAME = ''
	cUSERNAME = ''

	FUNCTION INIT
		IF NOT DODEFAULT()
			RETURN .F.
		ENDIF
		WITH THIS
			CLEAR
			*SET RESOURCE OFF
			CLOSE DATA
			SET CENTURY ON
			SET DATE AMERICAN
			SET HOURS TO 24
			SET ANSI ON
			SET TALK OFF
			SET DELETED ON
			SET CONSOLE OFF
			SET EXCLUSIVE OFF
			SET SAFETY OFF
			SET EXACT OFF
			SET STATUS BAR ON
			SET SYSMENU OFF
			SET ENGINEBEHAVIOR 70
			_VFP.AUTOYIELD = .lAutoYield
			.cCOMPUTERNAME = GETENV("COMPUTERNAME")
			.cUSERNAME = GETENV("USERNAME")
			IF .lTestMode THEN
				.cSendTo = 'mbennett@fmiint.com'
				.cCC = ''
				.cLogFile = 'F:\UTIL\TRUCKING\LOGFILES\OO_PM_INSP_DUE_LOG_TESTMODE.TXT'
			ENDIF
			.lLoggingIsOn = .lLoggingIsOn AND NOT EMPTY(.cLogFile)
		ENDWITH
	ENDFUNC


	FUNCTION DESTROY
		WITH THIS
			IF .lLoggingIsOn  THEN
				SET ALTERNATE OFF
				SET ALTERNATE TO
			ENDIF
		ENDWITH
		DODEFAULT()
	ENDFUNC


	FUNCTION MAIN

		WITH THIS
			LOCAL lnNumberOfErrors, lnAccountID, ldToday, lcAccountID
			LOCAL lcOffice, lcWHpath, lcrptfilename, lcInvenOutPath
			LOCAL lcComment, lcTemplateFile, lnRow ,lcRow

			TRY
				lnNumberOfErrors = 0

				IF .lLoggingIsOn THEN
					SET ALTERNATE TO (.cLogFile) ADDITIVE
					SET ALTERNATE ON
				ENDIF

				.cSubject = .cProcessDesc + ' for ' + DTOC(.dToday)

				.TrackProgress(.cProcessDesc + ' started....', LOGIT+SENDIT+NOWAITIT)
				.TrackProgress('COMPUTERNAME = ' + .cCOMPUTERNAME, LOGIT+SENDIT)
				.TrackProgress('USERNAME = ' + .cUSERNAME, LOGIT+SENDIT)
				.TrackProgress('PROJECT = OO_PM_INSP_DUE', LOGIT+SENDIT)
				IF .lTestMode THEN
					.TrackProgress('                        TEST MODE                        ', LOGIT+SENDIT)
				ENDIF

				ldToday = .dToday
				
				lcrptfilename = 'F:\UTIL\TRUCKING\REPORTS\OO_PM_RPT_' + DTOS(ldToday) + '.XLS'
				
				xsqlexec("select * from owners where active=1","owners",,"oo")
				xsqlexec("select * from otrucks where active=1","otrucks",,"oo")
				
				SELECT A.TRUCK_NUM, A.PMINSDATE, B.CO_NAME, B.PHONE, 00000000 AS NUMDAYS, SPACE(100) AS COMMENT ;
					FROM OTRUCKS A ;
					LEFT OUTER JOIN OWNERS B ;
					ON B.OWNERSID = A.OWNERSID ;					
					INTO CURSOR CURPMINSP1 ;  
					WHERE A.ACTIVE ;
					READWRITE
					
				* get rid of null pminsdates
				SELECT CURPMINSP1
				SCAN
					IF ISNULL(CURPMINSP1.PMINSDATE) THEN
						REPLACE CURPMINSP1.PMINSDATE WITH {} IN CURPMINSP1
					ENDIF
				ENDSCAN
				
*!*	SELECT CURPMINSP1 
*!*	BROWSE
				
				*SET STEP ON 
				
				SELECT CURPMINSP1
				SCAN
					IF EMPTY(CURPMINSP1.PMINSDATE) THEN
						lcComment = 'INSPECTION DUE: Last PM Inspection Date is not known.'
					ELSE
						REPLACE CURPMINSP1.NUMDAYS WITH (ldToday - CURPMINSP1.PMINSDATE) IN CURPMINSP1
						lcComment = TRANSFORM(CURPMINSP1.NUMDAYS) + ' days since last PM Inspection.'
						DO CASE
							CASE CURPMINSP1.NUMDAYS < 75
								lcComment = 'OK: ' + lcComment
							CASE BETWEEN(CURPMINSP1.NUMDAYS,75,90)
								lcComment = 'INSPECTION DUE WITHIN ' + ALLTRIM(STR(90 - CURPMINSP1.NUMDAYS)) + ' DAYS : ' + lcComment
							CASE CURPMINSP1.NUMDAYS > 90
								lcComment = 'INSPECTION OVERDUE: ' + lcComment
						ENDCASE
					ENDIF
					REPLACE CURPMINSP1.COMMENT WITH lcComment IN CURPMINSP1
				ENDSCAN
				
				*BROWSE


				* populate copy of template file
				.oExcel = CREATEOBJECT("Excel.Application")
				.oExcel.VISIBLE = .F.
				.oExcel.displayalerts = .F.
				
				lcTemplateFile = 'F:\UTIL\TRUCKING\TEMPLATES\OO_PM_RPT_TEMPLATE.XLS'

				.oWorkbook = .oExcel.workbooks.OPEN(lcTemplateFile)
				.oWorkbook.SAVEAS(lcrptfilename)

				.oWorksheet = .oWorkbook.Worksheets[1]
				*.oWorksheet.RANGE("A5","W20").clearcontents()

*SET STEP ON 				
				
				SELECT CURPMINSP1
				lnRow = 1
				SCAN
					lnRow = lnRow + 1
					lcRow = ALLTRIM(STR(lnRow))
					
					.oWorksheet.RANGE("A"+lcRow).VALUE = CURPMINSP1.TRUCK_NUM
					.oWorksheet.RANGE("B"+lcRow).VALUE = IIF(EMPTY(CURPMINSP1.PMINSDATE),'',CURPMINSP1.PMINSDATE)
					.oWorksheet.RANGE("C"+lcRow).VALUE = CURPMINSP1.CO_NAME
					.oWorksheet.RANGE("D"+lcRow).VALUE = CURPMINSP1.PHONE
					.oWorksheet.RANGE("E"+lcRow).VALUE = CURPMINSP1.NUMDAYS
					.oWorksheet.RANGE("F"+lcRow).VALUE = CURPMINSP1.COMMENT				
				ENDSCAN
				
				.oWorkbook.SAVE()
				.oExcel.QUIT()				

				IF FILE(lcrptfilename) THEN
					.TrackProgress('Successfully created: ' + lcrptfilename, LOGIT+SENDIT)
					.cAttach = lcrptfilename
				ELSE
					.TrackProgress('!!! ERROR: there was a problem creating: ' + lcrptfilename, LOGIT+SENDIT)
				ENDIF


				.TrackProgress(.cProcessDesc + ' ended normally....', LOGIT+SENDIT+NOWAITIT)

			CATCH TO loError

				.TrackProgress('There was an error.',LOGIT+SENDIT)
				.TrackProgress('The Error # is: ' + TRANSFORM(loError.ERRORNO), LOGIT+SENDIT)
				.TrackProgress('The Error Message is: ' + TRANSFORM(loError.MESSAGE), LOGIT+SENDIT)
				.TrackProgress('The Error occurred in Program: ' + TRANSFORM(loError.PROCEDURE), LOGIT+SENDIT)
				.TrackProgress('The Error occurred at line #: ' + TRANSFORM(loError.LINENO), LOGIT+SENDIT)
				lnNumberOfErrors = lnNumberOfErrors + 1
				
				IF TYPE('.oExcel') = 'O' AND NOT ISNULL(.oExcel) THEN
					.oExcel.QUIT()
				ENDIF

			ENDTRY

			WAIT CLEAR
			***************** INTERNAL email results ******************************
			.TrackProgress('About to send status email.',LOGIT)
			.TrackProgress('==================================================================================================================', SENDIT)
			.TrackProgress('The logfile is: ' + .cLogFile, LOGIT+SENDIT)
			.TrackProgress(.cProcessDesc + ' started: ' + .cStartTime, LOGIT+SENDIT)
			.TrackProgress(.cProcessDesc + ' finished: ' + TTOC(DATETIME()), LOGIT+SENDIT)

			IF .lSendInternalEmailIsOn THEN
				* try to trap error from not having dartmail dll's registered on user's pc...
				TRY
					.cTopBodyText = 'See attached report on OwnerOp PM inspections due.' + CRLF + CRLF + "<< report log follows>>" + CRLF
					
					.cBodyText = .cTopBodyText + CRLF + .cBodyText

					DO FORM dartmail2 WITH .cSendTo,.cFrom,.cSubject,.cCC,.cAttach,.cBodyText,"A"
					.TrackProgress('Sent status email.',LOGIT)
				CATCH TO loError
					.TrackProgress('There was an error sending the status email.',LOGIT)
					.TrackProgress('The Error # is: ' + TRANSFORM(loError.ERRORNO), LOGIT)
					.TrackProgress('The Error Message is: ' + TRANSFORM(loError.MESSAGE), LOGIT)
					.TrackProgress('The Error occurred in Program: ' + TRANSFORM(loError.PROCEDURE), LOGIT)
					.TrackProgress('The Error occurred at line #: ' + TRANSFORM(loError.LINENO), LOGIT)
					lnNumberOfErrors = lnNumberOfErrors + 1
				ENDTRY

			ELSE
				.TrackProgress('Did not send status email: THIS.lSendInternalEmailIsOn = FALSE.',LOGIT)
			ENDIF

		ENDWITH
		RETURN
	ENDFUNC && main


	PROCEDURE TrackProgress
		* do any combination of Wait Window, writing to logfile, and adding to body of email,
		* based on nFlags parameter.
		LPARAMETERS tcExpression, tnFlags
		WITH THIS
			IF BITAND(tnFlags,LOGIT) = LOGIT THEN
				IF .lLoggingIsOn THEN
					?
					? .cProcessName + "  " + TTOC(DATETIME()) + ": " + tcExpression
				ENDIF
			ENDIF
			IF .lWaitWindowIsOn THEN
				IF BITAND(tnFlags,NOWAITIT) = NOWAITIT THEN
					WAIT WINDOW tcExpression NOWAIT
				ENDIF
				IF BITAND(tnFlags,WAITIT) = WAITIT THEN
					WAIT WINDOW tcExpression TIMEOUT .nWaitWindowTimeout
				ENDIF
			ENDIF
			IF BITAND(tnFlags,SENDIT) = SENDIT THEN
				IF .lSendInternalEmailIsOn THEN
					.cBodyText = .cBodyText + tcExpression + CRLF + CRLF
				ENDIF
			ENDIF
		ENDWITH
	ENDPROC  &&  TrackProgress


ENDDEFINE
