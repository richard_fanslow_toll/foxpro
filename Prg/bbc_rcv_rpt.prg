* This program extracts cutu data from inven
utilsetup("BBC_RCV_RPT")
set exclusive off
set deleted on
set talk off
set safety off   &&Allows for overwrite of existing files

close databases all

useca('inwolog','wh',,,'a-6757')
useca('pl','wh',,,'a-6757')
useca('indet','wh',,,'a-6757')
select wo_date, wo_num, accountid, acctname,container,reference,brokerref,;
	acct_ref,seal,quantity as actual_ctn,plinqty as expected_ctn,plunitsinqty as total_expected_qty,confirmdt from inwolog where ;
	container !='PPA' and container !='CYCL' and brokerref!='FROM' into cursor t1 readwrite
select a.*, style,color,id,pack,totqty as expected_qty from t1 a left join pl b on a.wo_num=b.wo_num into cursor c1 readwrite
select wo_num,style,color,id,pack,sum(totqty) as expected_qty from pl where units group by wo_num,style,color,id,pack into cursor cc1 readwrite
select wo_num, style,color,id,pack, sum(totqty) as rcvdqty from indet where units group by wo_num, style,color,id,pack into cursor vindet readwrite
select * from cc1 a full join vindet b on a.wo_num=b.wo_num and a.style=b.style and a.color=b.color and a.id=b.id and a.pack=b.pack  into  cursor cc2 readwrite
replace wo_num_a with wo_num_b for isnull(wo_num_a) in cc2
replace style_a with style_b for isnull(style_a) in cc2
replace color_a with color_b for isnull(color_a) in cc2
replace id_a with id_b for isnull(id_a) in cc2
replace pack_a with pack_b for isnull(pack_a) in cc2
replace expected_qty with 0 for isnull(expected_qty) in cc2
select wo_num_a as wo_num, style_a as style, color_a as color,id_a as id, pack_a as pack,expected_qty, rcvdqty from cc2 into cursor cc3 readwrite
select t1
delete for confirmdt < date()-10
select * from t1 a left join cc3 b on a.wo_num=b.wo_num into cursor cc4 readwrite
*!*	scan
*!*	  if seek(STR(xtemp.accountid,4)+xtemp.style+xtemp.color+xtemp.size,"upcmast","stylecolid")
*!*	      replace div with getmemodata("upcmast.info","DIV") in xtemp
*!*	  endif
*!*	ENDSCAN



select cc4
if reccount() > 0
	copy to f:\ftpusers\bbcrw\reports\bbc_rcv_rpt.xls type xls
	xfile='F:\FTPUSERS\bbcrw\reports\bbc_rcv_rpt.xls'
	oexcel = createobject("excel.application")
	oexcel.displayalerts = .f.
	oworkbook = oexcel.workbooks.open(xfile,,.f.)
	xfile=xfile+"x"
	oworkbook.saveas(xfile) && "39" converts the save format to excel95/97
	oworkbook.close()
	oexcel.quit()
DELETE FILE F:\FTPUSERS\bbcrw\reports\bbc_rcv_rpt.xls
*		export TO "F:\FTPUSERS\BBCRW\reports\bbc_rcv_rpt"   TYPE xls
	tsendto = "tmarg@fmiint.com"
	tattach = ""
	tcc =""
	tfrom ="Toll EDI System Operations <fmi-transload-ops@fmiint.com>"
	tmessage = "BBC RECEIVING REPORT COMPLETE_:"+ttoc(datetime())
	tsubject = "BBC RECEIVING REPORT COMPLETE"
	do form m:\dev\frm\dartmail2 with tsendto,tfrom,tsubject, tcc,tattach,tmessage  ,"A"

else


endif
close data all




schedupdate()
_screen.caption=gscreencaption
on error
