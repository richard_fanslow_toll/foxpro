*!* m:\dev\prg\merkury940_process.prg

Wait Window "This 940 is processing for office "+cOffice Timeout 2
Cd &lcPath

LogCommentStr = ""

delimchar = cDelimiter
lcTranOpt= cTranslateOption

ll940test = .f.

If ll940test
  xpath = "f:\ftpusers\merkury\940in\test\"
  lnNum = Adir(tarray,xpath+"*.*")
Else
  lnNum = Adir(tarray,"*.*")
Endif

If lnNum = 0
  Wait Window At 10,10 "      No "+cCustname+" 940s to import     " Timeout 3
  NormalExit = .T.
  Throw
Endif

For thisfile = 1  To lnNum
  If ll940test
    xfile = xpath+tarray[thisfile,1] && +"."
  Else
    xfile = lcPath+tarray[thisfile,1] && +"."
  Endif
  cfilename = Alltrim(Lower(tarray[thisfile,1]))
  nFileSize = Val(Transform(tarray(thisfile,2)))
  ArchiveFile = (lcArchivepath+cfilename)

  If !lTesting
    Copy File [&xfile] To ("F:\ftpusers\Merkury\940translate\"+cfilename) && to create 997
  Endif

  Wait Window "Importing file: "+cfilename Nowait

*  DO m:\dev\prg\loadedifile WITH Xfile,delimchar,lcTranOpt,cCustname
*  DO m:\dev\prg\loadedifile WITH Xfile,delimchar,"TILDE",cCustname
  Do m:\dev\prg\loadedifile With xfile,delimchar,"CARROT",cCustname

  Select x856
  Locate
*  BROWSE TIMEOUT 10
  Locate For x856.segment = 'GS'
  If x856.f1 = "FA"
    Wait Window "This is a 997 FA file...moving to correct folder" Timeout 2
    cfile997in = ("F:\ftpusers\"+cCustname+"\997in\"+cfilename)
    Copy File [&xfile] To [&cfile997in]
    Delete File [&xfile]
    Loop
  Else
    Locate
  Endif

  Do create_pt_cursors With cUseFolder,lLoadSQL  && .t. =create from SQL

  lOK = .T.
  Do ("m:\dev\prg\merkury940_bkdn")


  If lOK && If no divide-by-zero error
    Do "m:\dev\prg\all940_import"
*     DO ("m:\dev\prg\merkury940_import")
    cErrMsg="at after all940 import 5......"
  Else
    ArchiveFile  = (lcArchivepath+cfilename)
    If !File(ArchiveFile)
      Copy File [&Xfile] To [&archivefile]
    Endif
    If File(xfile)
      Delete File [&xfile]
    Endif
    Loop
  Endif
  cErrMsg="at after all940 import 5......"

Next thisfile

&& now clean up the 940in archive files, delete for the upto the last 10 days
** removed these as it takes topo long with 30,000 files in the archive moved to another location

* deletefile(lcArchivepath,20) 

Wait Clear

Wait "Entire "+cCustname+" 940 Process Now Complete" Window Timeout 2
NormalExit = .T.
