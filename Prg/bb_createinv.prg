******************************************************************************************************************
cInvDetCompany = 'R'
nAccountID = 6695
nBillToID = 6695
nInvoiceWONum = 0
*********************************************************************************************************************
* The csrInvoice needs to be created before coming here
* Started 02/02/2017 PG
*********************************************************************************************************************

PUBLIC gnocalc, gmakecheckspayableto, GSYSTEMMODULE
PRIVATE pnInvoiceID
gnocalc = .T.  && this prevents an AR stored procedure from calculating invoice.invamt
gmakecheckspayableto = "TGF Management Group Holdco Inc."

LOCAL llRetVal, lnAcctID, lnLineNum, lnTotalAmount, lnInvoiceNum
llRetVal = .T.
lnAcctID = 6695
m.billtoid = lnAcctID

SELECT curInvoice
SUM AMOUNT TO lnTotalAmount

*lTestInvoice = .f.
lTestInvoice = glBBB_210_Testing

IF lTestInvoice THEN

	IF !USED("vinvoice")
		USE M:\DEV\ARDATA\invoice ALIAS vinvoice SHARED IN 0
	ENDIF
	
	IF !USED("vinvdet")
		USE M:\DEV\ARDATA\invdet ALIAS vinvdet SHARED IN 0
	ENDIF
	
	*!*	IF !USED("GLCHART")
	*!*		USE M:\DEV\ARDATA\GLCHART ALIAS GLCHART SHARED IN 0
	*!*	ENDIF
	xsqlexec("select * from glchart",,,"ar")
	
	IF !USED("farhdr")
		USE M:\DEV\ARDATA\farhdr SHARED IN 0
	ENDIF
	
	IF !USED("farlin")
		USE M:\DEV\ARDATA\farlin SHARED IN 0
	ENDIF
	
	IF !USED("ARGENPK")
		USE M:\DEV\ARDATA\ARGENPK AGAIN IN 200
	ENDIF
	
ELSE

	IF !USED("vinvoice") THEN
	     OPEN DATABASE f:\watusi\ardata\arsql
	     userv("vinvoice","arsql")
*			USE F:\WATUSI\ARDATA\invoice ALIAS vinvoice SHARED IN 0
	ENDIF
	
	IF !USED("vinvdet") THEN
	     OPEN DATABASE f:\watusi\ardata\arsql
	     userv("vinvdet","arsql")
	     CURSORSETPROP("buffering",3,"vinvdet")
	     SELECT vinvdet
	     INDEX ON invdetid tag invdetid
	     SET ORDER TO
	     CURSORSETPROP("BUFFERING",5,"VINVDET")
*			USE F:\WATUSI\ARDATA\invdet ALIAS vinvdet SHARED IN 0
	ENDIF
	
	IF !USED("ARGENPK")
		USE F:\WATUSI\ARDATA\ARGENPK AGAIN IN 200
	ENDIF
	
	xsqlexec("select * from glchart",,,"ar")
	useca("farhdr","ar",.T.)
	useca("farlin","ar",.T.)
	
ENDIF

xsqlexec("select * from account where inactive=0","account",,"qq")
SELECT account
INDEX ON accountid TAG accountid
SET ORDER TO

xsqlexec("select * from acctbill","acctbill",,"qq")
INDEX ON billtoid TAG billtoid

=SEEK(lnAcctID,"account","accountid")
=SEEK(m.billtoid,"acctbill","billtoid")

SELECT curInvoice
GO TOP

IF curInvoice.TYPE ="IN"
	lcDetailDesc ="INBOUND RECEIVING"
	lcHeader = "REFERENCE: INVOICE FOR RECEIVING ACTIVITIES" + CHR(13)+;
		"FOR TRAILER: " + curInvoice.trailer +CHR(13)+;
		"BOL:" + ALLTRIM(curInvoice.manifest)+CHR(13)+ ;
		"CARTONS: "+ ALLTRIM(TRANSFORM(curInvoice.qty))+CHR(13)+ ;
		"FILENAME: " + curInvoice.filename
ENDIF

IF curInvoice.TYPE ="TNB"
	lcDetailDesc ="BULK OUTBOUND SHIPPING"
	lcHeader = "REFERENCE: INVOICE FOR BULK OUTBOUND ACTIVITIES" + CHR(13)+ ;
		"FOR MANIFEST:" + curInvoice.manifest +CHR(13)+;
		"CARTONS: "+ ALLTRIM(TRANSFORM(curInvoice.qty)) +CHR(13)+;
		"FILENAME : " + curInvoice.filename
ENDIF

IF curInvoice.TYPE ="TNS"
	lcDetailDesc ="STORE MANIFESTED SHIPPING"
	lcHeader = "REFERENCE: INVOICE FOR STORE OUTBOUND ACTIVITIES" + CHR(13) + ;
		"FOR MANIFEST: " + ALLTRIM(curInvoice.manifest) +CHR(13)+;
		"CARTONS: "+ ALLTRIM(TRANSFORM(curInvoice.qty)) +CHR(13)+;
		"FILENAME: " + curInvoice.filename
ENDIF

*SET STEP ON 
*!*	IF !lTestInvoice
*!*		lcInvoiceNum= ALLTRIM(TRANSFORM(dygenpk("INVNUM","ar")))
*!*	ELSE
	*!*	*lcInvoiceNum ="0001234"
	*!*	lnInvoiceNum = dygenpk("INVNUM","ar")+1000000
	*!*	lcInvoiceNum = ALLTRIM(TRANSFORM(lnInvoiceNum))
	
	* This code is for both PROD and DEV
	SELECT ARGENPK
	LOCATE FOR gpk_pk = "INVNUM"
	IF FOUND() THEN
		IF RLOCK() THEN
			REPLACE ARGENPK.gpk_currentnumber WITH (ARGENPK.gpk_currentnumber + 1) IN ARGENPK
			lcInvoiceNum = PADL(TRANS(ARGENPK.gpk_currentnumber),6,'0')
			UNLOCK
			*WAIT WINDOW TIMEOUT 2 '<=======RLOCKED======>'
		ELSE
			gcUserMessage = '===> RLOCK() failed getting INVNUM from ARGENPK'
			THROW
		ENDIF
	ELSE
		gcUserMessage = '===> INVNUM key not found in dev ARGENPK'
		THROW
	ENDIF
*!*	ENDIF

SELECT vinvoice
SCATTER MEMVAR BLANK
m.invnum = lcInvoiceNum
m.wo_num = 0
m.accountid = lnAcctID
m.billtoid = lnAcctID

m.acctname = account.acctname
m.billname = acctbill.billname
m.billname2 = acctbill.billname2
m.address = acctbill.address
m.city = acctbill.city
m.state = acctbill.state
m.zip = acctbill.zip
m.billparty = acctbill.billparty
m.header = lcHeader
m.invdt = DATE()

*!*	IF !lTestInvoice
*!*		pnInvoiceID = dygenpk("invoice","ar")
*!*	ELSE	
*!*		*pnInvoiceID = dygenpk("invoice","ar")+1000000

	SELECT ARGENPK
	LOCATE FOR gpk_pk = "INVOICE"
	IF FOUND() THEN
		IF RLOCK() THEN
			REPLACE ARGENPK.gpk_currentnumber WITH (ARGENPK.gpk_currentnumber + 1) IN ARGENPK
			pnInvoiceID = ARGENPK.gpk_currentnumber
			UNLOCK
		ELSE
			gcUserMessage = '===> RLOCK() failed getting INVOICEID from ARGENPK'
			THROW
		ENDIF
	ELSE
		gcUserMessage = '===> INVOICE key not found in dev ARGENPK'
		THROW
	ENDIF
	
*!*	ENDIF
*set step on

m.invoiceid = pnInvoiceID
m.invamt = lnTotalAmount
************************************
m.visibleby = lnAcctID
************************************
STORE "BB-EDIPROC" TO m.addby, m.updateby
STORE DATE() TO m.adddt, m.updatedt
STORE "BB-EDIPROC" TO m.updproc, m.addproc
*INSERT INTO vinvoice FROM MEMVAR
insertinto('vinvoice','ar',.f.)

lnLineNum = 0
SELECT curInvoice
SCAN FOR AMOUNT > 0
	lnLineNum = lnLineNum + 1
	SELECT vinvdet
	SCATTER MEMVAR BLANK
	m.invoiceid = vinvoice.invoiceid
	m.invnum = vinvoice.invnum
	m.linenum = lnLineNum

	m.description = DTOC(curInvoice.TRANSDATE)+" - "+ALLTRIM(lcDetailDesc) + TRANSFORM(curInvoice.AMOUNT,"@$ 999,999.99")
	m.glcode = '13'
	m.invdetamt = curInvoice.AMOUNT
	m.COMPANY = "P"
	
*!*		IF lTestInvoice THEN

		SELECT ARGENPK
		LOCATE FOR gpk_pk = "INVDET"
		IF FOUND() THEN
			IF RLOCK() THEN
				REPLACE ARGENPK.gpk_currentnumber WITH (ARGENPK.gpk_currentnumber + 1) IN ARGENPK
				m.invdetid = ARGENPK.gpk_currentnumber
				UNLOCK
			ELSE
				gcUserMessage = '===> RLOCK() failed getting INVDETID from ARGENPK'
				THROW
			ENDIF
		ELSE
			gcUserMessage = '===> INVDET key not found in dev ARGENPK'
			THROW
		ENDIF
		
*!*		ELSE
*!*			m.invdetid = dygenpk("invdet","ar")
*!*		ENDIF
	
	STORE "BB-EDIPROC" TO m.addby, m.updateby
	STORE DATE() TO m.adddt, m.updatedt
	STORE "BB-EDIPROC" TO m.updproc, m.addproc
	*INSERT INTO vinvdet FROM MEMVAR
	insertinto('vinvdet','ar',.f.)
ENDSCAN


*!*	SELECT vinvdet
*!*	BROWSE

*SET STEP ON 
*SET FILTER TO invoiceid = pnInvoiceID

* create PDF version of invoice and merge it with consolidated bill...
*    .PrintInvoiceToPDF()

*!*	SELECT vinvdet
*!*	browse
*SET FILTER TO invoiceid = pnInvoiceID

IF lTestInvoice THEN
	* NOTHING
ELSE

	*************************************************************************************************************************
	DO loadplat  && this is Darren's program for loading the invoice into Platinum - you just use it as is - don't modify it!
	*************************************************************************************************************************
					
ENDIF

*!*  Use In vinvoice
*!*  Use In vinvdet
USE IN farhdr
USE IN farlin
USE IN ARGENPK
USE IN account
USE IN acctbill
USE IN GLCHART

RETURN llRetVal
ENDFUNC  &&  GenerateInvoice
