*!*	EDI_CONFIRM_ISA Program, 09.28.2011

PUBLIC lTesting,lStartDigit,lEmail
CLOSE DATA ALL

lTesting = .f.
lOverrideBusy = lTesting
lEmail = .t.

DO m:\dev\prg\_setvars WITH lTesting
DO m:\dev\prg\lookups
SET ESCAPE ON
ON ESCAPE CANCEL

SELECT 0
USE F:\edirouting\FTPSETUP SHARED
LOCATE FOR FTPSETUP.TRANSFER = "997-MORET-IN-SP"
IF chkbusy AND !lOverrideBusy
	WAIT WINDOW '997 Confirm Busy...try again later' TIMEOUT 2
	CLOSE DATABASES ALL
	CANCEL
	RETURN
ENDIF
REPLACE chkbusy WITH .T. FOR FTPSETUP.TRANSFER = "997-MORET-IN-SP"
USE IN FTPSETUP

USE F:\3pl\DATA\edi_trigger IN 0 ALIAS edi_trigger

SELECT 0
CREATE CURSOR temp1 (ISA_Num c(9),file997in c(40))

lStartDigit = IIF(lTesting,2,1)
FOR i = lStartDigit TO 3
	cWhseCode = ICASE(i=1,"FL",i=2,"ML","SP")
	cDirIn = ("F:\FTPUSERS\Moret-"+cWhseCode+"\997in\")
	cDirOut = ("F:\FTPUSERS\Moret-"+cWhseCode+"\997in\archive\")
	SET DEFAULT TO &cDirIn
	CD &cDirIn

	len1 = ADIR(ary1,"*.edi")
	IF len1 = 0
		WAIT WINDOW "There are no "+cWhseCode+" 997 files to process...looping" TIMEOUT 2
		LOOP
	ELSE
		WAIT WINDOW "There are "+TRANSFORM(len1)+" files to process" TIMEOUT 2
	ENDIF

	FOR j = 1 TO len1
		dFileDate = ary1[j,3]
		cFilename = ALLTRIM(ary1[j,1])
		cFileIn = (cDirIn+cFilename)
		cFileOut = (cDirOut+cFilename)
		IF dFileDate < DATE()-30
			filecopydel()
			LOOP
		ENDIF

		DO m:\dev\prg\createx856a
		SELECT x856
		APPEND FROM [&cFilename] TYPE DELIMITED WITH CHARACTER "*"
		LOCATE
*		BROWSE
		SCAN FOR x856.segment = "AK1"
			cISANum = PADL(ALLTRIM(x856.f2),9,"0")
			IF SEEK(cISANum,'edi_trigger','isa_num')
				IF lTesting
					WAIT WINDOW "ISA # "+cISANum+" found...simulating replacing ACK945 flag" TIMEOUT 1
				ELSE
					WAIT WINDOW "ISA # "+cISANum+" found...replacing ACK945 flag" NOWAIT
					REPLACE edi_trigger.ack945 WITH .T.,edi_trigger.confirm_dt WITH DATETIME() ;
						FOR edi_trigger.ISA_Num = cISANum ;
						IN edi_trigger
				ENDIF
				INSERT INTO temp1 (ISA_Num,file997in) VALUES (cISANum,cFilename)
			ENDIF
		ENDSCAN
		filecopydel()
	ENDFOR
ENDFOR
SELECT temp1
LOCATE
IF lTesting AND !EOF()
	BROWSE
ENDIF

WAIT WINDOW "All 997 Cross-matching complete...exiting" TIMEOUT 2

USE F:\edirouting\FTPSETUP SHARED
REPLACE chkbusy WITH .f. FOR FTPSETUP.TRANSFER = "997-MORET-IN-SP"
USE IN FTPSETUP

SET STATUS BAR ON
close data all 
RETURN

****************************
PROCEDURE filecopydel
****************************
	ASSERT .F. MESSAGE "In file copy/delete routine"
	COPY FILE [&cFileIn] TO [&cFileOut]
	IF FILE(cFileOut) AND !lTesting
		DELETE FILE [&cFileIn]
	ENDIF
ENDPROC


