PARAMETERS lcwo_num

PUBLIC c_cntrlnum,c_grpcntrlnum,cwo_num,cgroupname,cdivision,ccustname,cfilename,div214,cst_csz
PUBLIC tmessage,tsendto,tcc,tsendtoerr,tccerr,cfin_status,ldocatch,ldojfilesout,cprgname,goffice
PUBLIC ARRAY thisarray(1)
cprgname = "ARIAT 861"
goffice="K"
gmasteroffice="K"


tccerr ="Early Error: "
tmessage = tccerr
tsendto ="pgaidis@fmiint.com"

IF EMPTY(lcwo_num)
	MESSAGEBOX("Need proper parameters",0,"Ariat Outbound-861")
	RETURN
ELSE
	nwo_num = INT(VAL(ALLTRIM(TRANSFORM(lcwo_num))))
ENDIF

ltesting = .F.
lemail = !ltesting
*!*  IF lTesting AND EMPTY(nWo_num)
*!*    nWo_num = "9900432"
*!*    CLOSE DATA ALL
*!*  ENDIF

DO m:\dev\prg\_setvars WITH ltesting

ldofilesout= !ltesting

ccustname="Ariat"
cfilename = ""
lfclose = .T.
liserror = .F.
ldocatch = .T.
cship_ref = ""
cst_csz = ""
cfin_status = ""
nfilenum = 0
dt1   = TTOC(DATETIME(),1)
cfin_status = "UNTRAPPED ERROR"

*ASSERT .f.
TRY
	csystemname = "UNASSIGNED"
	nacctnum = 6532
	cwo_num = ""
	ldoerror = .F.

	IF !ltesting
		xreturn = "XX"
		DO m:\dev\prg\wf_alt WITH "K",6532
		cusefolder = UPPER(xreturn)
	ELSE
		cusefolder = "F:\WHP\WHDATA\"
	ENDIF

	IF USED("inwolog")
		USE IN inwolog
	ENDIF

	xsqlexec("select * from inwolog where wo_num = "+TRANSFORM(nwo_num),,,"wh")
	SELECT inwolog
	INDEX on wo_num TAG wo_num

	IF USED("indet")
		USE IN indet
	ENDIF

	xsqlexec("select * from indet where wo_num = "+TRANSFORM(nwo_num),,,"wh")

	IF USED('ackdata')
		USE IN ackdata
	ENDIF
set step On 

	useca("ackdata","wh")

	xsqlexec("select * from ctnucc where inwonum="+transform(nwo_num),"ocartons",,"wh")

	SELECT upc,"0000000000"+ALLTRIM(STYLE) AS STYLE,rcvdt,VAL(PACK) AS totqty,IIF(VAL(PACK)= totqty,0,totqty) AS qcqty, PADR(getmemodata("suppdata","LINENUM"),10," ") AS linenum,PADR(getmemodata("suppdata","CAT"),15," ") AS cat,;
		getmemodata("suppdata","ITEMTYPE") AS TYPE,PADR(getmemodata("suppdata","ORIGSIZE"),10," ") AS SIZE ;
		FROM ocartons WHERE inwonum = nwo_num INTO CURSOR odetail Readwrite
	
	SELECT linenum,cat,TYPE,STYLE,upc,SIZE,SUM(qcqty) AS allqcqty,SUM(totqty) AS qty ;
		FROM odetail ;
		GROUP BY linenum ;
		INTO CURSOR odetail2

	IF RECCOUNT("oCartons") =0
		WAIT WINDOW "No cartons available for this work order" TIMEOUT 3
		ldocatch = .F.
		THROW
	ENDIF

	=SEEK(nwo_num,"inwolog","wo_num")
	IF !FOUND("inwolog")
		WAIT WINDOW "Not a valid work order number" TIMEOUT 3
		ldocatch = .F.
		THROW
	ENDIF

	IF emptynul(inwolog.confirmdt)
		WAIT WINDOW "Work Order not confirmed" TIMEOUT 3
		ldocatch = .F.
		THROW
	ELSE
		ldconfirmdate = inwolog.confirmdt
	ENDIF

	IF USED('mm')
		USE IN mm
	ENDIF
	SELECT 0
	USE F:\3pl\DATA\mailmaster ALIAS mm SHARED
	LOCATE FOR edi_type = "861" AND accountid = nacctnum
	SCATTER MEMVAR
	_SCREEN.CAPTION = ALLTRIM(mm.scaption)
	tsendto = IIF(mm.use_alt,mm.sendtoalt,mm.sendto)
	tcc = IIF(mm.use_alt,mm.ccalt,mm.cc)

	cfilename      = ALLTRIM(mm.holdpath)+("ARIAT_861_"+dt1+".EDI")
	cfilenameshort = JUSTFNAME(cfilename)
	cfilenamearch  = (ALLTRIM(mm.archpath)+cfilenameshort)
	cfilenameout   = (ALLTRIM(mm.basepath)+cfilenameshort)


	tfrom = "TGF Warehouse Operations <warehouse-ops@fmiint.com>"
	tattach = ""

	SELECT 0
	nfilenum = FCREATE(cfilename)
	IF nfilenum < 1
		cfin_status = "Cant Create file"
		THROW
	ENDIF

	cfd = "*"  && Field delimiter
	csegd = "~"  && Segment delimiter
	cterminator = ">"  && Used at end of ISA segment

	csendqual = "ZZ"  && Sender qualifier
	csendid = "TOLLPROD"   && Sender ID code
	crecqual = "01"   && Recip qualifier
	IF ltesting
		crecid = "789995313"   && Recip ID Code
		cgsid  = "789995313"   && Recip ID Code
	ELSE
		crecid = "789995313"   && Recip ID Code
		cgsid  = "789995313"   && Recip ID Code
	ENDIF

	cdate = DTOS(DATE())
	ctruncdate = RIGHT(cdate,6)
	ctrunctime = SUBSTR(TTOC(DATETIME(),1),9,4)
	cfiledate = cdate+ctrunctime
	csendidlong = PADR(csendid,15," ")
	crecidlong = PADR(crecid,15," ")
	cstring = ""

	nstcount = 0
	nsegctr = 0
	nlxnum = 1

	STORE "" TO cequipnum,cb10

	WAIT CLEAR
	WAIT WINDOW "Now creating an Ariat 861.........." NOWAIT

	cisacode = IIF(ltesting,"T","P")

	DO num_incr_isa
	cisa_num = PADL(c_cntrlnum,9,"0")

	STORE "ISA"+cfd+"00"+cfd+"          "+cfd+"00"+cfd+"          "+cfd+csendqual+cfd+csendidlong+cfd+;
		crecqual+cfd+crecidlong+cfd+ctruncdate+cfd+ctrunctime+cfd+"U"+cfd+"00401"+cfd+;
		cisa_num+cfd+"0"+cfd+cisacode+cfd+">"+csegd TO cstring  && +cfd+cterminator
	DO cstringbreak

	STORE "GS"+cfd+"RC"+cfd+csendid+cfd+cgsid+cfd+cdate+cfd+ctrunctime+cfd+c_cntrlnum+;
		cfd+"X"+cfd+"004010"+csegd TO cstring
	DO cstringbreak

	DO num_incr_st

	STORE "ST"+cfd+"861"+cfd+PADL(c_grpcntrlnum,9,"0")+csegd TO cstring
	DO cstringbreak
	nsegctr = nsegctr + 1

	nstcount = nstcount + 1

	SELECT ocartons
	LOCATE FOR inwonum = ocartons.inwonum
	IF !FOUND()
		MESSAGEBOX("No Wonum = Cartons........",0,"Ariat Outbound 861 ")
		ldocatch = .F.
		THROW
	ENDIF

&& OK lets do the header segments........

	lcshipid = ocartons.shipid
	lcdocumentdate = ALLTRIM(getmemodata("suppdata","DOCUMENTDATE"))  && document date is from the inbound asn

	STORE "BRA"+cfd+ALLTRIM(lcshipid)+cfd+cdate+cfd+"00"+cfd+"1"+csegd TO cstring
	DO cstringbreak
	nsegctr = nsegctr + 1

	lcbol = ALLTRIM(ocartons.bol)
	STORE "REF"+cfd+"BL"+cfd+lcbol+csegd TO cstring
	DO cstringbreak
	nsegctr = nsegctr + 1

	lcpackinglistnumber = ALLTRIM(getmemodata("suppdata","PACKINGLISTNUMBER"))
	STORE "REF"+cfd+"PK"+cfd+lcpackinglistnumber+csegd TO cstring
	DO cstringbreak
	nsegctr = nsegctr + 1

	lcctr = ALLTRIM(ocartons.CONTAINER)
	STORE "REF"+cfd+"CN"+cfd+lcctr+csegd TO cstring
	DO cstringbreak
	nsegctr = nsegctr + 1

* this was from testing............... changed 03/04/2016 PG
* now its the actual confirm date from inwolog
*!*    cDate = Dtos(Date()-2)
*!*    cTruncDate = Right(cDate,8)  && to be the actual date of the inbound was confirmed

	cdate = DTOS(ldconfirmdate)
	ctruncdate = RIGHT(cdate,8)  && to be the actual date of the inbound was confirmed

	STORE "DTM"+cfd+"050"+cfd+ctruncdate+cfd+ctrunctime+csegd TO cstring
	DO cstringbreak
	nsegctr = nsegctr + 1

*!*    lcShipto = Alltrim(getmemodata("suppdata","WAREHOUSE"))
*!*    Store "N1"+cfd+"ST"+cfd+lcShipto+csegd To cString
*!*    Do cstringbreak
*!*    nSegCtr = nSegCtr + 1

	lnrcd_ctr = 1

	SELECT odetail2
	GO TOP
	SCAN &&For oCartons.inwonum = 1234

		lcitemtype  = odetail2.TYPE   &&Alltrim(getmemodata("suppdata","ITEMTYPE"))
		lcqcqty =ALLTRIM(TRANSFORM(odetail2.allqcqty))

		STORE "RCD"+cfd++cfd+ALLTRIM(TRANSFORM(odetail2.qty))+cfd+lcitemtype+cfd+lcqcqty+csegd TO cstring  && one ctn short
		DO cstringbreak
		nsegctr = nsegctr + 1

		lclinenum = ALLTRIM(odetail2.linenum) && Alltrim(getmemodata("suppdata","LINENUM"))
		lccat     = ALLTRIM(odetail2.cat) && Alltrim(getmemodata("suppdata","CAT"))
		lcorigsize= ALLTRIM(odetail2.SIZE) && Alltrim(getmemodata("suppdata","ORIGSIZE"))

		STORE "LIN"+cfd+lclinenum+cfd+"IN"+cfd+ALLTRIM(odetail2.STYLE)+cfd+"UP"+cfd+ALLTRIM(odetail2.upc)+cfd+"SZ"+cfd+lcorigsize+cfd+"CG"+cfd+lccat+csegd TO cstring
		DO cstringbreak
		nsegctr = nsegctr + 1

		STORE "SN1"+cfd++cfd+ALLTRIM(TRANSFORM(odetail2.qty))+cfd+lcitemtype+csegd TO cstring  && one ctn short
		DO cstringbreak
		nsegctr = nsegctr + 1
		lnrcd_ctr = lnrcd_ctr+1
	ENDSCAN

	STORE  "SE"+cfd+ALLTRIM(STR(nsegctr+1))+cfd+PADL(c_grpcntrlnum,9,"0")+csegd TO cstring
	DO cstringbreak
	STORE  "GE"+cfd+"1"+cfd+c_cntrlnum+csegd TO cstring
	DO cstringbreak
	STORE  "IEA"+cfd+"1"+cfd+PADL(c_cntrlnum,9,"0")+csegd TO cstring
	DO cstringbreak

	IF nfilenum>0
		=FCLOSE(nfilenum)
	ENDIF
	COPY FILE &cfilename TO &cfilenamearch

*Set Step On

	SELECT ocartons
	SELECT ucc FROM ocartons INTO CURSOR tctn GROUP BY ucc
	lctotalshipped  = RECCOUNT("tctn")

	SELECT ocartons
	SELECT STYLE,COLOR,SUM(VAL(PACK)) AS totasn,SPACE(100) AS checked FROM ocartons INTO CURSOR tasnqtys READWRITE GROUP BY STYLE,COLOR

	SELECT indet
	SELECT STYLE,COLOR,SUM(totqty) AS totindet,SPACE(100) AS checked FROM indet WHERE wo_num  = nwo_num  AND units INTO CURSOR tindetqtys READWRITE GROUP BY STYLE,COLOR


	SELECT tasnqtys
	SCAN
		SELECT tindetqtys
		LOCATE FOR STYLE= tasnqtys.STYLE AND COLOR=tasnqtys.COLOR
		IF !FOUND()

		ELSE
			IF tindetqtys.totindet != tasnqtys.totasn
				REPLACE tindetqtys.checked WITH "ASN/Indet error at "+TRIM(STYLE)+"/"+TRIM(COLOR)+"/"+TRIM(TRANSFORM(tindetqtys.totindet))+"/"+TRIM(TRANSFORM(tasnqtys.totasn))
			ELSE
				REPLACE tindetqtys.checked WITH "OK"
			ENDIF
		ENDIF
	ENDSCAN

	SELECT tindetqtys
	SCAN
		SELECT tasnqtys
		LOCATE FOR STYLE= tindetqtys.STYLE AND COLOR=tindetqtys.COLOR
		IF !FOUND()

		ELSE
			IF tasnqtys.totasn != tindetqtys.totindet
				REPLACE tasnqtys.checked WITH "Indet/ASN error at "+TRIM(STYLE)+"/"+TRIM(COLOR)+"/"+TRIM(TRANSFORM(tasnqtys.totasn))+"/"+TRIM(TRANSFORM(tindetqtys.totindet))
			ELSE
				REPLACE tasnqtys.checked WITH "OK"
			ENDIF
		ENDIF
	ENDSCAN

	SELECT tindetqtys
	SCAN FOR EMPTY(checked)
		REPLACE checked WITH "Indet error not found in ASN "+TRIM(STYLE)+"/"+TRIM(COLOR)
	ENDSCAN

	SELECT tasnqtys
	SCAN FOR EMPTY(checked)
		REPLACE checked WITH "ASN error not found in Indet "+TRIM(STYLE)+"/"+TRIM(COLOR)
	ENDSCAN

	lcreconmsg =CHR(13)
	SELECT tindetqtys
	SCAN FOR EMPTY(checked) OR checked!="OK"
		lcreconmsg = lcreconmsg+checked+CHR(13)
	ENDSCAN

	SELECT tasnqtys
	SCAN FOR EMPTY(checked) OR checked!="OK"
		lcreconmsg = lcreconmsg+checked+CHR(13)
	ENDSCAN

	SELECT odetail2
	SUM odetail2.qty TO lntotalunits

	SELECT indet
	SUM totqty TO lnconfirmedctns  FOR wo_num = nwo_num  AND !units
	SUM totqty TO lnconfirmedunits FOR wo_num = nwo_num  AND units

	*Set Step On
	lldofilesout = .T.
	IF  (lnconfirmedunits != lntotalunits) OR LEN(TRIM(lcreconmsg)) > 5
		tsubject = "Ariat Outbound 861 EDI ERROR from Toll, Inbound WO: "+ALLTRIM(TRANSFORM(lcwo_num))
		tmessage = "861-EDI Info from TGF-CA"+CHR(13)
		tmessage = tmessage + "for Ariat has been created."+CHR(13)+"Filename: "+cfilename+CHR(13)+"861 contains "+ALLTRIM(TRANSFORM(lctotalshipped))+" Cartons and "+ALLTRIM(TRANSFORM(lntotalunits))+" Units"+CHR(13)
		tmessage = tmessage + "INWOLOG indicates "+ALLTRIM(TRANSFORM(lnconfirmedctns))+" Cartons and "+ALLTRIM(TRANSFORM(lnconfirmedunits))+" Units"+CHR(13)
		tmessage = tmessage +" Need to verify inbound quantities vs Scanned ASN quantities, No 861 file has been sent........"+CHR(13)+CHR(13)+lcreconmsg
		DO FORM dartmail2 WITH tsendto,tfrom,tsubject,tcc,tattach,tmessage,"A"
		lldofilesout = .F.
	ENDIF

	IF lldofilesout
		COPY FILE &cfilename TO &cfilenameout
		DELETE FILE &cfilename
	ENDIF
	SET STEP ON
	IF !ltesting
		IF !USED("edi_trigger")
			USE F:\3pl\DATA\edi_trigger IN 0
		ENDIF
		SELECT edi_trigger
		LOCATE
		LOCATE FOR accountid = 6532 AND wo_num = nwo_num AND edi_type ="861"
		IF FOUND()
			REPLACE edi_trigger.processed WITH .T.,edi_trigger.when_proc WITH DATETIME(),;
				edi_trigger.fin_status WITH "861 Outbound Created",edi_trigger.file945 WITH cfilename, edi_trigger.bol WITH lcbol, edi_trigger.errorflag WITH .F. NEXT 1 IN edi_trigger&&added  tmarg errorflag 6/8/16
		ENDIF

		m.groupnum=c_cntrlnum
		m.isanum=cisa_num
		m.transnum=PADL(c_grpcntrlnum,9,"0")
		m.edicode="RC"
		m.accountid=m.accountid
		m.loaddt=DATE()
		m.loadtime=DATETIME()
		m.filename=cfilename
		m.ship_ref=TRANSFORM(nwo_num)
		insertinto("ackdata","wh",.T.)

*		insert into edi_trigger (edi_type,accountid,office,processed,errorflag,fin_status,trig_time,when_proc,bol,consignee,trig_from,file945,outshipid,wo_num) values ;
("861",6532,"C",.t.,.f.,"861 Outbound",datetime(),datetime(),lcbol,"ARIAT","ARIAT EDI",cfilename,lctotalshipped,nwo_num)
	ENDIF

	IF lldofilesout
		tsubject = "Ariat Outbound 861 EDI from Toll, Inbound WO: "+ALLTRIM(TRANSFORM(lcwo_num))
		tmessage = "861-EDI Info from TGF-CA"+CHR(13)
		tmessage = tmessage + "for Ariat has been created."+CHR(13)+"Filename: "+cfilename+CHR(13)+"861 contains "+ALLTRIM(TRANSFORM(lctotalshipped))+" Cartons and "+ALLTRIM(TRANSFORM(lntotalunits))+" Units"
		DO FORM dartmail2 WITH tsendto,tfrom,tsubject,tcc,tattach,tmessage,"A"
	ENDIF

CATCH TO oerr
	SET STEP ON
	IF ldocatch
		lemail = .T.

		IF cfin_status !="UNEXPECTED"
			DO ediupdate WITH cfin_status,.T.
		ENDIF

		tsubject = "Ariat 861 Error ("+TRANSFORM(oerr.ERRORNO)+") at "+TTOC(DATETIME())
		tattach  = ""
		tsendto  = tsendtoerr
		tmessage = tccerr

		tmessage =tmessage+ "Try/CATCH Exeception Message:"+CHR(13)+;
			[  Error: ] + STR(oerr.ERRORNO) +CHR(13)+;
			[  LineNo: ] + STR(oerr.LINENO) +CHR(13)+;
			[  Message: ] + oerr.MESSAGE +CHR(13)+;
			[  Procedure: ] + oerr.PROCEDURE +CHR(13)+;
			[  Details: ] + oerr.DETAILS +CHR(13)+;
			[  StackLevel: ] + STR(oerr.STACKLEVEL) +CHR(13)+;
			[  LineContents: ] + oerr.LINECONTENTS+CHR(13)+;
			[  UserValue: ] + oerr.USERVALUE

		tmessage = tmessage+CHR(13)+cfin_status

		tsubject = "861/ARIAT EDI Poller Error at "+TTOC(DATETIME())
		tattach  = ""
		tcc=""
		tfrom    ="TGF/ARIAT EDI 861 Poller Operations <transload-ops@fmiint.com>"
		DO FORM dartmail2 WITH tsendto,tfrom,tsubject,tcc,tattach,tmessage,"A"
		lemail = .F.
	ENDIF
FINALLY
	SET HOURS TO 12
	FCLOSE(nfilenum)
	closefiles()
ENDTRY

tu("ackdata")

&& END OF MAIN CODE SECTION



****************************
PROCEDURE num_incr_isa
****************************

	IF !USED("serfile")
		USE ("f:\3pl\data\serial\ariat945_serial") IN 0 ALIAS serfile
	ENDIF

	SELECT serfile
	IF ltesting
		norigseq = serfile.seqnum
		noriggrpseq = serfile.grpseqnum
		lisaflag = .F.
	ENDIF

	nisa_num = serfile.seqnum
	c_cntrlnum = ALLTRIM(STR(serfile.seqnum))
	REPLACE serfile.seqnum WITH serfile.seqnum + 1 IN serfile
	USE IN serfile

ENDPROC

****************************
PROCEDURE num_incr_st
****************************

	IF !USED("serfile")
		USE ("f:\3pl\data\serial\ARIAT945_serial") IN 0 ALIAS serfile
	ENDIF

	SELECT serfile
	IF ltesting
		norigseq = serfile.seqnum
		noriggrpseq = serfile.grpseqnum
		lstflag = .F.
	ENDIF

	c_grpcntrlnum = ALLTRIM(STR(serfile.grpseqnum))
	REPLACE serfile.grpseqnum WITH serfile.grpseqnum + 1 IN serfile
	USE IN serfile

ENDPROC

****************************
PROCEDURE segmentget
****************************

	PARAMETER thisarray,lckey,nlength

	FOR i = 1 TO nlength
		IF i > nlength
			EXIT
		ENDIF
		lnend= AT("*",thisarray[i])
		IF lnend > 0
			lcthiskey =TRIM(SUBSTR(thisarray[i],1,lnend-1))
			IF OCCURS(lckey,lcthiskey)>0
				RETURN SUBSTR(thisarray[i],lnend+1)
				i = 1
			ENDIF
		ENDIF
	ENDFOR

	RETURN ""

****************************
PROCEDURE close214
****************************
	STORE  "SE"+cfd+ALLTRIM(STR(nsegctr+1))+cfd+PADL(c_grpcntrlnum,9,"0")+csegd TO cstring
	DO cstringbreak

	STORE  "GE"+cfd+ALLTRIM(STR(nstcount))+cfd+c_cntrlnum+csegd TO cstring
	DO cstringbreak

	STORE  "IEA"+cfd+"1"+cfd+PADL(c_cntrlnum,9,"0")+csegd TO cstring
	DO cstringbreak

	RETURN

****************************
PROCEDURE cstringbreak
****************************
	clen = LEN(ALLTRIM(cstring))
	FPUTS(nfilenum,cstring)
	RETURN



**************************
PROCEDURE ediupdate
**************************
	PARAMETERS cfin_status, liserror,lfclose
	IF !ltesting
		SELECT edi_trigger
		LOCATE
		IF liserror
			SET STEP ON
			REPLACE edi_trigger.processed WITH .T.,edi_trigger.created WITH .F.,edi_trigger.fin_status WITH cfin_status,edi_trigger.errorflag WITH .T.,;
				edi_trigger.when_proc WITH DATETIME() FOR edi_trigger.wo_num = nwo_num AND edi_trigger.edi_type = "861" IN edi_trigger
			IF nfilenum>0
				=FCLOSE(nfilenum)
			ENDIF
			IF FILE(cfilename)
				DELETE FILE &cfilename
			ENDIF
			closefiles()
		ELSE
			REPLACE edi_trigger.processed WITH .T.,edi_trigger.created WITH .T.,proc214 WITH .T.,	edi_trigger.when_proc WITH DATETIME(),;
				edi_trigger.fin_status WITH cstatuscode+" 861 CREATED",	edi_trigger.errorflag WITH .F.,file214 WITH cfilename	FOR edi_trigger.wo_num = nwo_num IN edi_trigger
		ENDIF
	ENDIF
ENDPROC

*********************
PROCEDURE closefiles
*********************
	IF USED('ftpedilog')
		USE IN ftpedilog
	ENDIF
	IF USED('serfile')
		USE IN serfile
	ENDIF
	IF USED('wolog')
		USE IN wolog
	ENDIF
	IF USED('xref')
		USE IN xref
	ENDIF
	IF USED('ftpjobs')
		USE IN ftpjobs
	ENDIF
ENDPROC
