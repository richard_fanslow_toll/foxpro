LOCAL ldDate
ldDate = {^2008-01-01}

CLOSE DATABASES ALL


SELECT DISTINCT ;
A.NAME, ;
A.FILE_NUM, ;
(SELECT MIN(DATE) ;
FROM f:\util\snapshots\adp\snapall.dbf ;
WHERE NAME = A.NAME ;
AND RATE1AMT = A.RATE1AMT ;
AND DATE >= ldDate ) AS RDATE, ;
A.RATE1AMT ; 
FROM f:\util\snapshots\adp\snapall.dbf A ;
INTO CURSOR CURSNAP1 ;
WHERE A.STATUS <> 'T' ;
AND A.DATE >= ldDate ;
AND A.ADP_COMP IN ('E87','E88','E89')

SELECT NAME, COUNT(*) ;
FROM CURSNAP1 GROUP BY NAME HAVING COUNT(*) > 1 ;
INTO CURSOR CURSNAP2

SELECT * ;
FROM CURSNAP1 ;
INTO CURSOR CURSNAP3 ;
WHERE NAME IN ;
(SELECT NAME FROM CURSNAP2) ;
ORDER BY NAME, RDATE

SELECT * ;
FROM CURSNAP1 ;
INTO CURSOR CURSNAP4 ;
WHERE NAME NOT IN ;
(SELECT NAME FROM CURSNAP2) ;
ORDER BY NAME, RDATE


SELECT CURSNAP3
*BROWSE
COPY TO F:\UTIL\SNAPSHOTS\REPORTS\RAISES.XLS XL5

SELECT CURSNAP4
*BROWSE
COPY TO F:\UTIL\SNAPSHOTS\REPORTS\NORAISES.XLS XL5

CLOSE DATABASES ALL

RETURN


**** COMPARE CURRENT ANNSAL WITH LAST YEAR
LOCAL ldOldDate, ldNewDate

ldOldDate = {^2008-09-29}
ldNewDate = {^2009-09-28}

SELECT A.NAME, A.ADP_COMP, A.DIVISION, A.DEPARTMENT, A.DATE AS DATE2009, A.RATE1AMT AS RATE2009, ldOldDate AS DATE2008, ;
(SELECT MAX(RATE1AMT) FROM f:\util\snapshots\adp\snapall WHERE NAME=A.NAME AND DATE=ldOldDate) AS RATE2008 ;
FROM f:\util\snapshots\adp\snapall A ;
INTO CURSOR CURYEARCOMP ;
WHERE A.DATE = ldNewDate ;
AND A.STATUS <> 'T' ;
ORDER BY A.NAME

*AND A.NAME IN (SELECT NAME FROM f:\util\snapshots\adp\snapall WHERE DATE={^2008-08-29}) ;

SELECT CURYEARCOMP
COPY TO C:\A\YEARCOMP_09292009.XLS XL5

CLOSE DATABASES ALL

RETURN


*** ad hoc query to get rates at particular date
SELECT NAME, ADP_COMP, RATE1AMT, DATE ;
FROM f:\util\snapshots\adp\snapall ;
WHERE DATE = {^2008-10-01} ;
AND STATUS <> 'T' ;
AND ADP_COMP <> 'E87' ;
ORDER BY NAME

CLOSE DATABASES ALL

**** COMPARE RATES FOR 2 DATES
SELECT A.NAME, A.FILE_NUM, A.ADP_COMP, A.RATE1AMT AS RTJULY2010, ;
(SELECT MAX(RATE1AMT) FROM f:\util\snapshots\adp\snapall WHERE DATE = {^2008-10-01} AND FILE_NUM = A.FILE_NUM AND ADP_COMP <> 'E87') AS RTOCT2008 ;
FROM f:\util\snapshots\adp\snapall A ;
WHERE A.DATE = {^2010-06-30} ;
AND A.STATUS <> 'T' ;
AND A.ADP_COMP <> 'E87' ;
ORDER BY A.NAME

COPY TO C:\A\SALARY_RATE_COMP_07012010.XLS XL5

CLOSE DATABASES ALL

*!*	AND FILE_NUM IN ;
*!*	(SELECT FILE_NUM FROM f:\util\snapshots\adp\snapall WHERE DATE = {^2008-10-01} AND STATUS <> 'T' AND ADP_COMP <> 'E87') ;

***************************************************************************************
***************************************************************************************
** HR statistics queries
***************************************************************************************
***************************************************************************************

** active count for a date
SET TALK on
SELECT NAME, date, ADP_COMP, status ;
	FROM f:\util\snapshots\adp\snapall ;
	WHERE DATE = {^2010-08-31} ;
	AND STATUS <> 'T'

* how many hired in a year
SELECT DISTINCT EMP_NUM ;
	FROM F:\HR\HRDATA\EMPLOYEE ;
	WHERE HIRE_DATE >= {^2010-01-01} AND HIRE_DATE <= {^2010-12-31}	

* how many terminated in a year
SELECT DISTINCT EMP_NUM ;
	FROM F:\HR\HRDATA\EMPLOYEE ;
	WHERE TERM_DATE >= {^2008-01-01} AND TERM_DATE <= {^2008-12-31}	
	
	
*** ACTIVE COUNTS for all months
SELECT date, COUNT(*) as headcnt ;
FROM f:\util\snapshots\adp\snapall ;
WHERE DATE IN ;
( {^2010-01-31},{^2010-02-28},{^2010-03-31},{^2010-04-30},{^2010-05-31},{^2010-06-30},{^2010-07-31},{^2010-08-31},{^2010-09-30},{^2010-10-31},{^2010-11-30},{^2010-12-31} ) ;
AND STATUS <> 'T' ;
GROUP BY DATE ;
ORDER BY DATE

	