* Create accrued (unbilled) revenue table for QlikView/Rob Petti.
* Code based on Darren's arunbill form.
*
* To be run nightly.
*
* EXE = F:\UTIL\QLIK\QLIKVIEWUNBILLPROC.EXE



LOCAL loQLIKVIEWUNBILLPROC, lcProcessName, lnError
lcProcessName = "QLIKVIEWUNBILLPROC"

DECLARE INTEGER CreateMutex IN Win32API ;
	STRING @lpMutexAttributes, ;
	STRING bInitialOwner, ;
	STRING @lpName

DECLARE INTEGER GetLastError IN Win32API

* if running in EXE check to see if already running
IF _VFP.STARTMODE > 0 THEN
	CreateMutex(0,CHR(1),@lcProcessName)
	lnError = GetLastError()
	IF lnError = 183
		WAIT WINDOW TIMEOUT 30 lcProcessName + " process is already running..."
		RETURN .F.
	ENDIF
ENDIF

runack(lcProcessName)

utilsetup(lcProcessName)

_SCREEN.Caption = "QLIKVIEWUNBILLPROC"

loQLIKVIEWUNBILLPROC = CREATEOBJECT('QLIKVIEWUNBILLPROC')
loQLIKVIEWUNBILLPROC.MAIN()

schedupdate()

CLOSE DATABASES ALL
RETURN

#DEFINE LOGIT 1
#DEFINE WAITIT 2
#DEFINE NOWAITIT 4
#DEFINE SENDIT 8
#DEFINE CRLF CHR(13) + CHR(10)
#DEFINE LF CHR(10)
#DEFINE RETURN_DATA_MANDATORY .F.
#DEFINE RETURN_DATA_NOT_MANDATORY .T.
#DEFINE xlEdgeBottom 9
#DEFINE xlContinuous 1
#DEFINE xlMedium -4138

DEFINE CLASS QLIKVIEWUNBILLPROC AS CUSTOM

	cProcessName = 'QLIKVIEWUNBILLPROC'

	lTestMode = .T.  && .F. FOR NORMAL OPERATION, .T. TO SUPPRESS ALL BUT DEVELOPER EMAILS AND CHANGE INPUT TABLE, ETC.

	lAutoYield = .T.

	cStartTime = TTOC(DATETIME())
	
	* object properties
	oCusRates = NULL

	* connection properties
	nSQLHandle = 0

	* file properties
	nFileHandle = 0

	* date properties
	dtNow = DATETIME()

	dToday = DATE()
	* to force different pay weeks
	*dToday = {^2010-07-01}

	cToday = DTOC(DATE())

	* wait window properties
	nWaitWindowTimeout = 2
	lWaitWindowIsOn = .T.

	* logfile properties
	lLoggingIsOn = .T.
	cLogFile = 'F:\UTIL\QLIK\LOGFILES\QLIKVIEWUNBILLPROC_log.txt'

	* INTERNAL email properties
	lSendInternalEmailIsOn = .T.
	cFrom = 'TGF Corporate <fmicorporate@fmiint.com>'
	cSendTo = 'mbennett@fmiint.com'
	cErrorSendTo = 'mbennett@fmiint.com'
	cCC = ''
	cSubject = ''
	cAttach = ''
	cBodyText = ''

	cCOMPUTERNAME = ''
	cUSERNAME = ''

	FUNCTION INIT
		IF NOT DODEFAULT()
			RETURN .F.
		ENDIF
		WITH THIS
			CLEAR
			*SET RESOURCE OFF
			SET MULTILOCKS ON
			CLOSE DATA
			SET CENTURY ON
			SET DATE AMERICAN
			SET HOURS TO 24
			SET ANSI ON
			SET TALK OFF
			SET DELETED ON
			SET CONSOLE OFF
			SET EXCLUSIVE OFF
			SET SAFETY OFF
			SET EXACT OFF
			SET STATUS BAR ON
			SET SYSMENU OFF
			SET ENGINEBEHAVIOR 70
			_VFP.AUTOYIELD = .lAutoYield
			.cCOMPUTERNAME = GETENV("COMPUTERNAME")
			.cUSERNAME = GETENV("USERNAME")
			IF .lTestMode THEN
				.cSendTo = 'mbennett@fmiint.com'
				.cLogFile = 'F:\UTIL\QLIK\LOGFILES\QLIKVIEWUNBILLPROC_log_TESTMODE.txt'
			ENDIF
			.lLoggingIsOn = .lLoggingIsOn AND NOT EMPTY(.cLogFile)
			IF .lLoggingIsOn THEN
				SET ALTERNATE TO (.cLogFile) ADDITIVE
				SET ALTERNATE ON
			ENDIF
		ENDWITH
	ENDFUNC


	FUNCTION DESTROY
		WITH THIS
			IF .lLoggingIsOn  THEN
				SET ALTERNATE OFF
				SET ALTERNATE TO
			ENDIF
		ENDWITH
		DODEFAULT()
	ENDFUNC


	FUNCTION MAIN
		WITH THIS
			LOCAL lnNumberOfErrors, lcSQL, ldToday, ldFromDate, ldToDate, lcSQLFromDate, lcSQLToDate, lcSpreadsheet
			LOCAL lcMonthYear, lcMonthYear2, lcOutputFileTotHours, lcOutputFileShipping, lcCC, lcAttach, lcDivision


			TRY
				lnNumberOfErrors = 0

				.TrackProgress('Qlik Late Bill process started....', LOGIT+SENDIT+NOWAITIT)
				.TrackProgress('Logfile = ' + .cLogFile, LOGIT+SENDIT)
				.TrackProgress('COMPUTERNAME = ' + .cCOMPUTERNAME, LOGIT+SENDIT)
				.TrackProgress('USERNAME = ' + .cUSERNAME, LOGIT+SENDIT)
				.TrackProgress('PROJECT = QLIKVIEWUNBILLPROC', LOGIT+SENDIT)
				IF .lTestMode THEN
					.TrackProgress('                        TEST MODE                        ', LOGIT+SENDIT)
				ENDIF
				
				*SET CLASSLIB TO AR ADDITIVE
				*.oCusRates = CREATEOBJECT('CUSRATES')
				
				PUBLIC goffice


				gnocalc=.T.
				
				*xstartdt=DATE()-DAY(DATE())
				xstartdt=DATE()
				
				*xlimitdt=GOMONTH(xstartdt,-8)
				xlimitdt = xstartdt - 90
				
				*m.womonth=dt2month(GOMONTH(DATE(),-1))
				
				m.invdt=xstartdt

				lcMonthYear = TRANSFORM(xlimitdt) + " - " + TRANSFORM(xstartdt)
				lcMonthYear2 = DTOS(xlimitdt) + "-" + DTOS(xstartdt)
				
				*lcSpreadsheet = "S:\QLIKVIEW\BUDGET\UNBILLED_" + lcMonthYear2 + ".XLS"
				lcSpreadsheet = "S:\QLIKVIEW\BUDGET\UNBILLED.XLS"
				

				.cSubject = 'Qlik Late Bill Process for: ' + lcMonthYear

				USE F:\AUTO\HOLIDAYS IN 0 ALIAS HOLIDAYS
		
				USE F:\UTIL\QLIK\DATA\QLATEBILL IN 0 ALIAS LATEBILL EXCLUSIVE
				
				xsqlexec("select * from whoffice",,,"wh")

				OPEN DATABASE F:\WATUSI\ARDATA\AR

				*USE RATEACCT IN 0 ALIAS RATEACCT 
				xsqlexec("select * from rateacct","rateacct",,"ar")
				index on accountid tag accountid
				set order to
				
				

				CREATE CURSOR xrpt (wo_num N(7), wo_date d, office c(1), TYPE c(4), accountid i, acctname c(30), revenue N(9,2))

				SELECT * FROM LATEBILL WHERE .F. INTO CURSOR xlatebill READWRITE

				.zzwmsin()
				.zzwmsout()
				.zzwmsmisc()
				.zzwmssp()
				*.MiscProrated()

				DELETE IN LATEBILL FOR .T.  && womonth=m.womonth

				SELECT xlatebill
				REPLACE invnum WITH "WOREV" FOR invnum#"WOREV"
				SCAN
					* calc Division
					* FROM DY:
					*!*	04  f:\whi
					*!*	14  f:\whj
					*!*	07  f:\whm
					*!*	56  f:\wh7
					*!*	59  f:\whr
					*!*	57: f:\wh1, f:\wh2, f:\wh5 and accounts 4610,4677,4694,5102,6137,6178,6182
					*!*	52  f:\wh1, f:\wh2, f:\wh5 all other accounts
					DO CASE
						CASE UPPER(OFFICE) == 'I'
							lcDivision = '04'
						CASE UPPER(OFFICE) == 'J'
							lcDivision = '14'
						CASE (UPPER(OFFICE) == 'L') AND (ACCOUNTID = 5837)
							lcDivision = '50'
						CASE UPPER(OFFICE) == 'M'
							lcDivision = '07'
						CASE UPPER(OFFICE) == '7'
							lcDivision = '56'
						CASE UPPER(OFFICE) == 'R'
							lcDivision = '59'
						CASE INLIST(UPPER(OFFICE),'1','2','5')
							IF INLIST(ACCOUNTID,4610,4677,4694,5102,6137,6178,6182) THEN
								lcDivision = '57'
							ELSE
								lcDivision = '52'
							ENDIF
						OTHERWISE
							lcDivision = SPACE(2)
					ENDCASE
					
				
					SCATTER MEMVAR
					m.DIVISION = lcDivision
					m.ADDDT = DATETIME()
					m.ADDPROC = "QLATEBILL"
					
					IF m.INVDETAMT <> 0.0 THEN
						INSERT INTO LATEBILL FROM MEMVAR
					ENDIF
					
				ENDSCAN
				
*!*					******************************************
*!*					IF .lTestMode THEN
*!*						SELECT LATEBILL
*!*						BROWSE
*!*					ENDIF
*!*					******************************************

				* COPY AS SPREADSHEET TO QLIK BUDGET FOLDER PER ROBB AND FIONA 10/1/13
				SELECT LATEBILL
				COPY TO (lcSpreadsheet) XL5
				
				IF FILE(lcSpreadsheet) THEN
					.TrackProgress("** Created spreadsheet: "+ lcSpreadsheet, LOGIT+SENDIT)
				ELSE
					.TrackProgress("**** Error creating spreadsheet: "+ lcSpreadsheet, LOGIT+SENDIT)
				ENDIF


				IF DAY(DATE()) = 1 THEN
					.TrackProgress("It's the 1st of the month: Packing QlikView Late Bill table...", LOGIT+SENDIT)
					SELECT LATEBILL
					PACK
				ENDIF

				WAIT CLEAR

				.TrackProgress('Qlik Late Bill process ended normally.', LOGIT+SENDIT)
				.TrackProgress('Qlik Late Bill process started: ' + .cStartTime, LOGIT+SENDIT)
				.TrackProgress('Qlik Late Bill process finished: ' + TTOC(DATETIME()), LOGIT+SENDIT)
				
				***************** INTERNAL email results ******************************
				.TrackProgress('About to send status email.',LOGIT)

				IF .lSendInternalEmailIsOn THEN
					DO FORM dartmail2 WITH .cSendTo,.cFrom,.cSubject,.cCC,.cAttach,.cBodyText,"A"
					.TrackProgress('Sent status email.',LOGIT)
				ELSE
					.TrackProgress('Did not send status email: THIS.lSendInternalEmailIsOn = FALSE.',LOGIT)
				ENDIF
			
			CATCH TO loError

				.TrackProgress('There was an error.',LOGIT+SENDIT)
				.TrackProgress('The Error # is: ' + TRANSFORM(loError.ERRORNO), LOGIT+SENDIT)
				.TrackProgress('The Error Message is: ' + TRANSFORM(loError.MESSAGE), LOGIT+SENDIT)
				.TrackProgress('The Error occurred in Program: ' + TRANSFORM(loError.PROCEDURE), LOGIT+SENDIT)
				.TrackProgress('The Error occurred at line #: ' + TRANSFORM(loError.LINENO), LOGIT+SENDIT)
				lnNumberOfErrors = lnNumberOfErrors + 1
				lcCC = ""
				lcAttach = ""
				DO FORM dartmail2 WITH .cErrorSendTo,.cFrom,("ERRORS in " + .cSubject),lcCC,lcAttach,.cBodyText,"A"
				
			FINALLY

			ENDTRY

		ENDWITH
		RETURN
	ENDFUNC && main



	PROCEDURE zzwmsin
		m.type="WMS Inbound"
		
		xsqlexec("select * from account where inactive=0","account",,"qq")
		index on accountid tag accountid
		set order to

		SELECT whoffice
		SCAN FOR !test
			xoffice=office
			*xcompany=ratecompany(xoffice)
			xmasteroffice=IIF(INLIST(xoffice,"1","2","5","6","7","8"),"C",IIF(INLIST(xoffice,"I","J"),"N",xoffice))
			xfolder=folder+"\whdata\"
			
			goffice = ALLTRIM(xoffice)  && needed by usesqlinwo()

			xsqlexec("select * from inwolog where office='" + ALLTRIM(xoffice) + "'","inwolog",,"wh")

*!*					USE (xfolder+"inwolog") IN 0
*!*					*USE (xfolder+"indet") IN 0
*!*					*USE (xfolder+"inloc") IN 0


			SELECT ;
				inwologid, wo_num, wo_date, accountid, acctname, .F. AS notrated, ;
				picknpack, ;
				revenue AS invdetamt ;
				FROM inwolog ;
				WHERE (EMPTY(invnum) OR invdt>=xstartdt) AND !unbilled ;
				AND !archived ;
				AND !sp ;
				AND !EMPTYnul(confirmdt) ;
				AND BETWEEN(confirmdt,xlimitdt,xstartdt) ;
				AND (quantity>0 OR unitsinqty>0) ;
				INTO CURSOR xarunbill READWRITE
				
			SELECT xarunbill
			SCAN
				WAIT WINDOW "WMS IN, office "+xoffice+": "+TRANSFORM(RECNO())+" of "+TRANSFORM(RECCOUNT()) NOWAIT

				SCATTER MEMVAR

				m.company=wmscompany(accountid, xoffice)

				=SEEK(xarunbill.accountid,"account","accountid")
				IF xarunbill.picknpack AND account.pnp&xmasteroffice
					m.glcode="31"
				ELSE
					m.glcode="13"
				ENDIF

				m.OFFICE = xoffice
				INSERT INTO xlatebill FROM MEMVAR
			ENDSCAN

			USE IN inwolog
			*USE IN outship
		ENDSCAN
		
		USE IN account		
		WAIT CLEAR		
		RETURN
	ENDPROC  && zzwmsin



	PROCEDURE zzwmsout
		m.type="WMS Outbound"
	
		xsqlexec("select * from account where inactive=0","account",,"qq")
		index on accountid tag accountid
		set order to

		SELECT whoffice
		SCAN FOR !test
			xoffice=office
			xmasteroffice=IIF(INLIST(xoffice,"1","2","5","6","7","8"),"C",IIF(INLIST(xoffice,"I","J"),"N",xoffice))
			xfolder=folder+"\whdata\"

			*USE (xfolder+"outwolog") IN 0
			*USE (xfolder+"outship") IN 0
			
			xsqlexec("select * from outwolog where mod='" + xoffice + "'","outwolog",,"wh")
			xsqlexec("select * from outship where mod='" + xoffice + "'","outship",,"wh")

			SELECT ;
				wo_num, ;
				pulleddt AS wo_date, ;
				accountid, ;
				acctname, ;
				picknpack, ;
				revenue AS invdetamt ;
				FROM outwolog ;
				WHERE (EMPTY(invnum) OR invdt>=xstartdt) AND !unbilled ;
				AND revenue#0 ;
				AND !archived ;
				AND pulled ;
				AND revenue#0 ;
				AND !EMPTYnul(pulleddt) ;
				AND BETWEEN(pulleddt,xlimitdt,xstartdt) ;
				INTO CURSOR xarunbill

			SELECT xarunbill
			SCAN
				WAIT WINDOW "WMS OUT, office "+xoffice+": "+TRANSFORM(RECNO())+" of "+TRANSFORM(RECCOUNT()) NOWAIT

				SELECT outship
				LOCATE FOR wo_num=xarunbill.wo_num AND ctnqty#0 AND EMPTYnul(del_date) AND !notonwip
				IF FOUND()
					LOOP
				ENDIF

				SELECT xarunbill
				SCATTER MEMVAR

				*m.company=ratecompany(accountid, xoffice)
				m.company=ratecompany(xoffice)

				=SEEK(xarunbill.accountid,"account","accountid")
				IF xarunbill.picknpack AND account.pnp&xmasteroffice
					m.glcode="31"
				ELSE
					m.glcode="13"
				ENDIF

				m.OFFICE = xoffice
				INSERT INTO xlatebill FROM MEMVAR
			ENDSCAN

			USE IN outwolog
			USE IN outship
		ENDSCAN
		
		USE IN account		
		WAIT CLEAR		
		RETURN
		
	ENDPROC  && zzwmsout



	PROCEDURE zzwmsmisc

		m.type="wms miscellaneous"
		
		xsqlexec("select * from account where inactive=0","account",,"qq")
		index on accountid tag accountid
		set order to

		xquery="select wo_num, wo_date, accountid, acctname, totamt as invdetamt, comment, storage, sp, mod " + ;
			"from miscwo " + ;
			"where (empty(invnum) or invdt>={"+dtoc(xstartdt)+"}) and unbilled=0 and wo_date>{"+dtoc(date()-365)+"} " + ;
			"and totamt#0 and between(wo_date,{"+dtoc(xlimitdt)+"},{"+dtoc(xstartdt)+"})"
			
		xsqlexec(xquery,"xarunbill",,"wh")

		select xarunbill
		scan
			wait window "wms misc: "+transform(recno())+" of "+transform(reccount()) nowait

			scatter memvar
			m.company=wmscompany(accountid,m.mod)

			do case
				case m.accountid=4328
					m.glcode='16'
				case m.accountid=5998
					m.glcode='31'
				case m.accountid=3819 and "PALLET"$comment
					m.glcode='13'
				case m.storage
					m.glcode='14'
				case m.sp
					m.glcode='16'
				otherwise
					m.glcode='13'
			endcase

			*!*			if xoffice="L"
			*!*				m.company="A"
			*!*			endif

			m.office = m.mod
			insert into xlatebill from memvar
		endscan
		
		WAIT CLEAR		
		RETURN
	ENDPROC  &&  zzwmsmisc
	


	PROCEDURE zzwmssp
		m.type="WMS Special Project"
		
		xsqlexec("select * from account where inactive=0","account",,"qq")
		index on accountid tag accountid
		set order to

		xquery="select wo_num, completeddt as wo_date, accountid, acctname, totrevenue as invdetamt, mod " + ;
			"from project " + ;
			"where (empty(invnum) or invdt>={"+dtoc(xstartdt)+"}) and unbilled=0 " + ;
			"and totrevenue#0 and between(completeddt,{"+dtoc(xlimitdt)+"},{"+dtoc(xstartdt)+"})"
			
		xsqlexec(xquery,"xarunbill",,"wh")

		SELECT xarunbill
		SCAN
			*WAIT WINDOW "WMS SP, office "+xoffice+": "+TRANSFORM(RECNO())+" of "+TRANSFORM(RECCOUNT()) NOWAIT
			WAIT WINDOW "WMS SP, mod "+mod+": "+TRANSFORM(RECNO())+" of "+TRANSFORM(RECCOUNT()) NOWAIT

			SCATTER MEMVAR
			m.office = m.mod
			m.company=wmscompany(m.accountid, m.office)
			m.glcode="16"
			INSERT INTO xlatebill FROM MEMVAR
		ENDSCAN
		
		WAIT CLEAR		
		RETURN
	ENDPROC  && zzwmssp


	PROCEDURE MiscProrated
		* new from Darren 11/4/13
	
*!*			CLOSE DATABASES ALL
*!*			SET DELETED ON

*!*			USE F:\util\qlik\DATA\qlatebill IN 0
*!*			USE F:\wh\whoffice IN 0
*!*			USE F:\AUTO\holidays IN 0

		m.type="Misc prorate"
		* i'm already deleting all recs each time it runs...
		*DELETE IN qlatebill FOR TYPE=m.type
		xsqlexec("select * from account where inactive=0","account",,"qq")
		index on accountid tag accountid
		set order to

		SELECT whoffice
		SCAN FOR !test AND !NOVALID AND office="I"
			xoffice=whoffice.office
			xfolder=whoffice.folder+"\whdata\"

			USE (xfolder+"miscwo") IN 0

			m.office=xoffice

			SELECT miscwo
			SCAN FOR EMPTY(invnum) AND !unbilled AND profromdt#{}
				SCATTER MEMVAR FIELDS accountid, acctname, wo_num

				xdate=miscwo.profromdt
				xnumdays=0
				DO WHILE .T.
					IF !INLIST(DOW(xdate),7,1) AND !holiday(xdate)
						xnumdays=xnumdays+1
					ENDIF
					xdate=xdate+1
					IF xdate>miscwo.protodt
						EXIT
					ENDIF
				ENDDO

				SELECT xlatebill
				LOCATE FOR wo_num=miscwo.wo_num
				IF FOUND()
					SCATTER MEMVAR FIELDS company, division, glcode

					IF BETWEEN(miscwo.wo_date,miscwo.profromdt,miscwo.protodt)
						xprorateamt=ROUND(miscwo.totamt/xnumdays,2)
						xfudge1=xprorateamt*xnumdays-miscwo.totamt
						xfudge2=0
					ELSE
						xprorateamt=ROUND(miscwo.totamt/xnumdays,2)
						xfudge2=xprorateamt*xnumdays-miscwo.totamt
						m.invdetamt=-miscwo.totamt
						m.wo_date=miscwo.wo_date
						INSERT INTO xlatebill FROM MEMVAR
					ENDIF

					xdate=miscwo.profromdt
					DO WHILE .T.
						IF !INLIST(DOW(xdate),7,1) AND !holiday(xdate)
							m.wo_date=xdate
							IF miscwo.wo_date=xdate
								m.invdetamt=xprorateamt-miscwo.totamt-xfudge1
							ELSE
								m.invdetamt=xprorateamt-xfudge2
								xfudge2=0
							ENDIF
							INSERT INTO xlatebill FROM MEMVAR
						ENDIF

						xdate=xdate+1
						IF xdate>miscwo.protodt
							EXIT
						ENDIF
					ENDDO
				ENDIF
			ENDSCAN

			USE IN miscwo
		ENDSCAN

		WAIT CLEAR
		RETURN
	ENDPROC  && MiscProrated



	PROCEDURE TrackProgress
		* do any combination of Wait Window, writing to logfile, and adding to body of email,
		* based on nFlags parameter.
		LPARAMETERS tcExpression, tnFlags
		WITH THIS
			IF BITAND(tnFlags,LOGIT) = LOGIT THEN
				IF .lLoggingIsOn THEN
					?
					? .cProcessName + "  " + TTOC(DATETIME()) + ": " + tcExpression
				ENDIF
			ENDIF
			IF .lWaitWindowIsOn THEN
				IF BITAND(tnFlags,NOWAITIT) = NOWAITIT THEN
					WAIT WINDOW tcExpression NOWAIT
				ENDIF
				IF BITAND(tnFlags,WAITIT) = WAITIT THEN
					WAIT WINDOW tcExpression TIMEOUT .nWaitWindowTimeout
				ENDIF
			ENDIF
			IF BITAND(tnFlags,SENDIT) = SENDIT THEN
				IF .lSendInternalEmailIsOn THEN
					.cBodyText = .cBodyText + tcExpression + CRLF + CRLF
				ENDIF
			ENDIF
		ENDWITH
	ENDPROC  &&  TrackProgress

* did not use this method code from arunbill form - instead modeled it after zzwmsout(), per DY 9/20/13
*!*		PROCEDURE zzwmsin
*!*			m.type="WMS Inbound"

*!*			SELECT whoffice
*!*			SCAN FOR !test
*!*				xoffice=office
*!*				xcompany=ratecompany(xoffice)
*!*				xfolder=folder+"\whdata\"

*!*				USE (xfolder+"inwolog") IN 0
*!*				USE (xfolder+"indet") IN 0
*!*				USE (xfolder+"inloc") IN 0

*!*				SELECT ;
*!*					inwologid, wo_num, wo_date, accountid, acctname, .F. AS notrated ;
*!*					FROM inwolog ;
*!*					WHERE (EMPTY(invnum) OR invdt>=xstartdt) AND !unbilled ;
*!*					AND !archived ;
*!*					AND !sp ;
*!*					AND !EMPTY(confirmdt) ;
*!*					AND BETWEEN(confirmdt,xlimitdt,xstartdt) ;
*!*					AND (quantity>0 OR unitsinqty>0) ;
*!*					INTO CURSOR xarunbill READWRITE

*!*				SELECT xarunbill
*!*				SCAN
*!*					*		assert wo_num#7030461

*!*					WAIT WINDOW "WMS IN, office "+xoffice+": "+TRANSFORM(RECNO())+" of "+TRANSFORM(RECCOUNT()) NOWAIT

*!*					=SEEK(inwologid,"inwolog","inwologid")

*!*					m.invoiceid=99999999
*!*					m.invnum="WOREV"
*!*					m.wo_num=inwolog.wo_num
*!*					m.wo_date=inwolog.wo_date
*!*					m.accountid=inwolog.accountid
*!*					m.acctname=inwolog.acctname
*!*					INSERT INTO vinvoice FROM MEMVAR

*!*					xglacct = ""
*!*					xacctname = ""
*!*					xbillname = ""
*!*					xwonum = vinvoice.wo_num
*!*					xacctnum = vinvoice.accountid
*!*					xbillnum = vinvoice.billtoid
*!*					m.invoiceid = vinvoice.invoiceid
*!*					xconsolidated=.F.

*!*	* needed?
*!*	*!*					.oCusRates.zz701(.F.)

*!*					IF RECCOUNT("vinvdet")=0
*!*						REPLACE notrated WITH .T. IN xarunbill
*!*					ELSE
*!*						SELECT company, glcode, SUM(invdetamt) AS invdetamt FROM vinvdet WITH (BUFFERING=.T.) ;
*!*							WHERE invdetamt#0 AND !MAIN GROUP BY 1,2 INTO CURSOR xdy

*!*						SCAN
*!*							SCATTER MEMVAR FIELDS company, glcode, invdetamt
*!*							IF m.company="W"
*!*								m.company="U"
*!*							ENDIF
*!*							INSERT INTO xlatebill FROM MEMVAR
*!*						ENDSCAN
*!*					ENDIF

*!*					TABLEREVERT(.T.,"vinvoice")
*!*					TABLEREVERT(.T.,"vinvdet")
*!*				ENDSCAN

*!*				*	select xarunbill
*!*				*	browse last for notrated

*!*				USE IN inwolog
*!*				USE IN indet
*!*				USE IN inloc
*!*			ENDSCAN
*!*			
*!*			USE IN VINVOICE
*!*			USE IN VINVDET
*!*			
*!*			RETURN
*!*		ENDPROC  && zzwmsin


ENDDEFINE
