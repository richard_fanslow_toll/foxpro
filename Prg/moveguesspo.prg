************************************************************************************************************
** This program will look for a Guess PO file , rename it and create 
** an FTP job to move it to Lognet
**
** MODIFICATIONS:
**	- copy to directory for ICON processing - mvw 12/21/10
**	- strip last 2 fields (added for ICON po file) from file and copy to necesaary folders  - mvw 12/21/10
************************************************************************************************************
*
utilsetup("MOVEGUESSPO")

set exclusive off
set safety off
set century off

lcpath = "f:\ftpusers\guess\"
lcarchivepath = "f:\ftpusers\guess\archive\"
lc850inpath = "f:\ftpusers\guess\850in\"

xnewformat=.t.

if file("f:\ftpusers\guess\guesspo.txt")
	set date ymd
	xfilename="sglgesfmi"+strtran(strtran(strtran(ttoc(datetime()),":","")," ",""),"/")+".850"
	set date american

	if !xnewformat
		copy file &lcpath.guesspo.txt to (lcpath+xfilename)
		copy file &lcpath.guesspo.txt to (lcarchivepath+xfilename)

		**copy to 850in folder for ICON processing - mvw 12/21/10
		copy file &lcpath.guesspo.txt to (lc850inpath+xfilename)
	else
		**do a strtofile() later to put revised files into guess filder for ftp move - mvw 12/21/10
	*!*		copy file &lcpath.guesspo.txt to (lcpath+xfilename)
	*!*		copy file &lcpath.guesspo.txt to (lcarchivepath+xfilename)

		**copy to 850in folder for ICON processing - mvw 12/21/10
		xfile=lc850inpath+xfilename
		copy file &lcpath.guesspo.txt to &xfile

		**strip last to fields that were added for ICON (for lognet ftp) - mvw 12/21/10
		xcolcnt=25
		xseparator=chr(13)+chr(10)

		select 0
		*!*	create cursor x850 (field1 c(254))
		create cursor x850 (f1 c(30))
		for i=2 to xcolcnt &&add fields f2 to fx
			xfield="f"+transform(i)
			alter table x850 add column &xfield c(30)
		endfor

		xchar=chr(9)
		append from "&xfile" type delimited with character '&xchar'
		locate

		xfilestr=""
		scan
			for i = 1 to 17 &&17 fields delimited by tab (chr(9)), lines separated by chr(13)+chr(10)
				xfield="f"+transform(i)
				**for whatever reason style (field 7) isnt trimmed, always 14 chars
				if i=7
					xfilestr=xfilestr+left(&xfield,14)+chr(9)
				else
					xfilestr=xfilestr+alltrim(&xfield)+chr(9)
				endif
			endfor
			**get rid of trailing chr(9), add linefeed
			xfilestr=left(xfilestr,len(xfilestr)-1)+chr(13)+chr(10)
		endscan

		strtofile(xfilestr,lcpath+xfilename)
		strtofile(xfilestr,lcarchivepath+xfilename)
	endif

	delete file &lcpath.guesspo.txt

	insert into f:\edirouting\ftpjobs (jobname) values ("GUESS-PO-TOLOGNET")
	wait window at 10,10 "FTP Job Inserted: " timeout 2
else
	wait window at 10,10 "No Guess 850 files..................." timeout 2
endif

schedupdate()
