*!* MORET 945 EDI MBOL (Whse. Shipping Advice) Creation Program
*!* Creation Date: 01.25.2011 by Joe

PARAMETERS cBOL,cOffice,nAcctNum,ctime1

PUBLIC ARRAY thisarray(1)
PUBLIC c_CntrlNum,c_GrpCntrlNum,cReturned,cWO_Num,cWO_NumStr,nWO_Num1,cPRONum,cTRNum,nBOLQty,nBOLWt,cCustFolder,cRolledRecs
PUBLIC lEmail,lDoFilesOut,dDateTimeCal,nFilenum,tsendto,tcc,tsendtoerr,tccerr,nPTCount,cTrkNumber,cFreight,nCuFT,cFilenameHold,cPTString
PUBLIC cxCoCode,lSQL3,lDoCatch,cTruncDate,cTruncTime,cDate,dt1,dt2,dtmail,cString,cCoCode,cMBOL,cISA_Num,cShip_refSH,cStatus
PUBLIC cShip_ref_out,lTesting,lDoUpdate,nCutUCCs,cLoadid,cHoldFolder,cOutFolder,lOverflow,lToys,tsendtozero,tcczero,cRetMsg,cArchfolder
PUBLIC lJCPenney,lMacy,lWalmart,dRundate,nOrigSeq,nOrigGrpSeq,lAAFES,cCoCodeGrp,lAmazon,cSubBOLDetail,lVFO
PUBLIC cMod,cMBOL,cFilenameShort,cFilenameOut,cFilenameArch,lParcelType,cWO_NumList,lcPath,cEDIType,lLoadSQLBL
SET REPROCESS TO 500


lTesting   = .f.  && Set to .t. for testing (default = .f.)
lTestinput = .F.  && Set to .t. for test input files only! (default = .f.)
lFakeDelivery = .F. && If true, creates a 945 for undelivered BOL, does not move to 945OUT folder

WAIT WINDOW "This is a "+IIF(lTesting,"Test","Production")+" 945 MBOL run" TIMEOUT 3
lDoCatch = .T.
DO m:\dev\prg\_setvars WITH lTesting
ON ESCAPE CANCEL
cOffice = IIF(lTesting,"C",cOffice)
cfd = "*"
csd = "%"
csegd = ""
nSubCnt = 0
nTotSubs = 0
cStatus = ""
dRundate = {^2012-11-16}
cxCoCode = ""
cCoCodeGrp = ""
cSubBOLDetail = ""
cMBOL = ""
lParcelType = .F.
cEDIType = "945"

TRY
	DO CASE
		CASE cOffice = "L"
			cCustLoc =  "ML"
			cFMIWarehouse = ""
			cDivision = "Mira Loma"
			cSF_Addr1  = "3355 DULLES DR"
			cSF_CSZ    = "MIRA LOMA"+cfd+"CA"+cfd+"91752"

		CASE cOffice = "M"
			cCustLoc =  "FL"
			cFMIWarehouse = ""
			cDivision = "Florida"
			cSF_Addr1  = "11400 NW 32ND AVE"
			cSF_CSZ    = "MIAMI"+cfd+"FL"+cfd+"33167"

		CASE cOffice = "I"
			cCustLoc =  "NJ"
			cFMIWarehouse = ""
			cDivision = "New Jersey"
			cSF_Addr1  = "800 FEDERAL BLVD"
			cSF_CSZ    =  "CARTERET"+cfd+"NJ"+cfd+"07008"

		CASE cOffice = "X"
			cCustLoc =  "CR"
			cFMIWarehouse = ""
			cDivision = "Carson"
			cSF_Addr1  = "2000 E CARSON ST"
			cSF_CSZ    = "CARSON"+cfd+"CA"+cfd+"90745"

		OTHERWISE
			cCustLoc =  "SP"
			cFMIWarehouse = ""
			cDivision = "San Pedro"
			cSF_Addr1  = "450 WESTMONT DRIVE"
			cSF_CSZ    = "SAN PEDRO"+cfd+"CA"+cfd+"90731"
	ENDCASE

	WAIT WINDOW "At start of MBOL process" NOWAIT

	lTestMail = lTesting && Sends mail to Joe only
	lDoFilesOut = !lTesting && IIF(lTesting ,.F.,.T.) && If true, copies output to FTP folder (defaults to .t.)
	lDoCartons = IIF(DATE() = dRundate,.F.,.T.) && Set to false to scan SQL Labels file (default = .t.)
	lDoCompare = IIF(DATE() = dRundate,.F.,.T.)

*lTestMail = .t.
*lDoFilesOut = .f.

	IF lFakeDelivery
		WAIT WINDOW "This is a FAKE DELIVERY process" TIMEOUT 1
		dFakeDate = {^2012-06-06}
		cOffice = "C"
		lDoCompare = .F.
		lDoCartons = .F.
		lDoFilesOut = .F.
	ENDIF
	cMod = IIF(cOffice = "C","7",cOffice)
	goffice = cMod
	gMasterOffice = IIF(cOffice = "I","N",cOffice)
	lLoadSQL = .T.

	lStandalone = lTesting
	ON ERROR THROW
	lBrowVPP = .F. && lTesting
	lBrowBOLs = .F. && lTesting
	lDoUpdate = .T.
	lUCC = .T.  && Set to false to order SQL results by carton ID instead of UCC (default = .t.)
	lEmail = .T.

	lSkipCompare = !lDoCartons

	IF !lDoUpdate
		lTestMail = .T.
		lDoFilesOut = .F.
	ENDIF

	STORE "" TO cShip_ref,cErrMsg,tattach,ccustname,cWO_NumStr,cWO_NumList,tcc,tmessage,cFreight,lcKey
	STORE "" TO cStyle,cTargetStyle,cString,cPTString,cMBOL,cShip_ref_out,cShip_refSH,cLoadid
	STORE .F. TO lFederated,lIsError,lDoBOLGEN,lSamples,lSQLMail,lNonEDI
	STORE .T. TO lCloseOutput,lISAFlag,lSTFlag
	STORE "LB" TO cWeightUnit
	STORE "CF" TO cCubeUnit
	cPPName = "MoretEDI"
	cMailName = cPPName
	tfrom = "Toll Warehouse Operations <toll-edi-ops@tollgroup.com>"
	nLoadid = 1
	nCutUCCs = 0
	STORE 0 TO nFilenum,alength,nLength,nSegCtr,nSTCount,nTotCtnWt,nTotCtnCount,nUnitSum
	cterminator = ">" && Used at end of ISA segment
	cOrig = "J"
	lPrepack = .F.
	cCoNum = ""

*!* SET CUSTOMER CONSTANTS
	ccustname = "MORET"  && Customer Identifier
	cX12 = "004010"  && X12 Standards Set used
	SELECT 0

	WAIT WINDOW "At the start of MORET EDI MBOL 945 process..." TIMEOUT 1

	IF TYPE("cOffice") = "L" AND !lTesting AND !lTestinput
		WAIT WINDOW "Office not provided...terminating" TIMEOUT 3
		lCloseOutput = .F.
		DO ediupdate WITH "No OFFICE",.T.
		THROW
	ENDIF

	IF lTesting OR lTestinput
		CLOSE DATABASES ALL
		DO m:\dev\prg\lookups
		SELECT 0
		IF !USED('edi_trigger')
			cEDIFolder = "F:\3PL\DATA\"
			USE (cEDIFolder+"edi_trigger") IN 0 ALIAS edi_trigger
		ENDIF
*!* TEST DATA AREA
		nAcctNum = 5451
		cBOL = "07636183000189109" && Master BOL #
		cOffice = "C"
		ctime1 = DATETIME()
		lFedEx = .F.
	ENDIF

	SELECT 0
	CREATE CURSOR mfilesout945 (ship_ref c(20),filename c(60),isa_num c(9))
	SELECT mfilesout945
	INDEX ON ship_ref TAG ship_ref

	cBOL=TRIM(cBOL)
	cOffice = IIF(cOffice = '7','C',cOffice)
	IF VARTYPE(ctime1) = "L"
		ctime1 = DATETIME()
	ENDIF

	IF USED('xref')
		USE IN xref
	ENDIF
	USE F:\3pl\DATA\moretpt_xref IN 0 ALIAS xref

	IF lTestinput
		cUseFolder = "F:\WHP\WHDATA\"
	ENDIF
	
	IF lTesting
		lDoFilesOut = .F.
	ENDIF

	WAIT WINDOW "Now preparing tables and variables" NOWAIT NOCLEAR

	IF USED('mm')
		USE IN mm
	ENDIF
	SELECT 0
	USE F:\3pl\DATA\mailmaster ALIAS mm SHARED
	LOCATE FOR mm.edi_type = "945" AND mm.GROUP = "MORET" AND accountid = 25000 && AMT/EDI Mails
	IF !FOUND()
		cErrMsg = "MISS MAIL DATA"
		DO ediupdate WITH cErrMsg,.T.
		THROW
	ENDIF
	lUseAlt = mm.use_alt
	tsendto = ALLTRIM(IIF(lUseAlt,mm.sendtoalt,mm.sendto))
	tcc = ALLTRIM(IIF(lUseAlt,mm.ccalt,mm.cc))
	cHoldFolder = ALLTRIM(mm.holdpath)
	cArchfolder = ALLTRIM(mm.archpath)
	cOutFolder = ALLTRIM(mm.basepath)
	IF lTesting OR lTestMail
		LOCATE FOR mm.edi_type = "MISC" AND mm.taskname = "GENERAL"
		lUseAlt = mm.use_alt
		tsendto = ALLTRIM(IIF(lUseAlt,mm.sendtoalt,mm.sendto))
		tcc = ALLTRIM(IIF(lUseAlt,mm.ccalt,mm.cc))
		tsendtoerr = ALLTRIM(IIF(lUseAlt,mm.sendtoalt,mm.sendto))
		tccerr = ALLTRIM(IIF(lUseAlt,mm.ccalt,mm.cc))
	ELSE
		LOCATE FOR office = 'X' AND mm.taskname = 'MORETZEROBOL'
		lUseAlt = mm.use_alt
		tsendtozero = ALLTRIM(IIF(lUseAlt,mm.sendtoalt,mm.sendto))
		tcczero = ALLTRIM(IIF(lUseAlt,mm.ccalt,mm.cc))
		LOCATE FOR mm.edi_type = "MISC" AND mm.taskname = "GENERAL"
		lUseAlt = mm.use_alt
		tsendtotest = ALLTRIM(IIF(lUseAlt,mm.sendtoalt,mm.sendto))
		tcctest = ALLTRIM(IIF(lUseAlt,mm.ccalt,mm.cc))
		tsendtoerr = tsendtotest
		tccerr = tcctest
	ENDIF
	USE IN mm

*!* SET OTHER CONSTANTS

	cWhseAbbrev = IIF(INLIST(cOffice,"O","X"),"TOLLO",'FMI'+cCustLoc)
	cCustFolder = UPPER(ccustname)+"-"+cCustLoc
	cArchfolder = STRTRAN(ALLTRIM(cArchfolder),'-SP','-'+cCustLoc)
	cDelTime = SUBSTR(TTOC(ctime1,1),9,4)
	cCarrierType = "M" && Default as "Motor Freight"
	dApptNum = ""

*!* SET OTHER CONSTANTS
	csendqual = "ZZ"
	csendid = cWhseAbbrev
** PAD ID Codes
	csendidlong = PADR(csendid,15," ")

	CD &cHoldFolder
	DELETE FILE *.*
	STORE cBOL TO cMBOL
	closedata()

	xsqlexec("select * from outship where bol_no = '"+cBOL+"' and (accountid in ("+gMoretAcctList+") or accountid in ("+gMoretAcctList2+"))",,,"wh")
	SELECT outship
	INDEX ON outshipid TAG outshipid
	INDEX ON bol_no TAG bol_no
	INDEX ON ship_ref TAG ship_ref
	INDEX ON wo_num TAG wo_num
	selectoutdet()
	SELECT outdet
	INDEX ON outdetid TAG outdetid

	csq1 = [select * from BL where mblnum = ']+cMBOL+[' and (inlist(accountid,]+gMoretAcctList+[) or inlist(accountid,]+gMoretAcctList2+[))]
	xsqlexec(csq1,,,"wh")

	SELECT 0
	CREATE CURSOR tempinfo (cocode c(3),subbol c(20),ptcount c(10),ptstring m)

	SELECT bl
	LOCATE
	SELECT 0
	SELECT bol_no FROM bl WHERE bl.mblnum = cMBOL AND (INLIST(bl.accountid,&gMoretAcctList) OR INLIST(bl.accountid,&gMoretAcctList2)) INTO CURSOR tempbl1 GROUP BY 1
	IF lBrowBOLs
		SELECT tempbl1
		LOCATE
		BROWSE TIMEOUT 10
	ENDIF
	STORE RECCOUNT() TO nSubBillCnt

	IF nSubBillCnt = 0
		cErrMsg = "NO SUBBOLS: "+cMBOL
		ediupdate(cErrMsg,.T.)
	ENDIF

	cSubBillCnt = ALLTRIM(STR(nSubBillCnt))
	USE IN tempbl1
	SELECT bl
	LOCATE

*!* Mail notice for all MBOL 945s
	IF lTesting
		WAIT CLEAR
		WAIT WINDOW "Expected number of 945 files: "+cSubBillCnt TIMEOUT 2
	ELSE
		tsubject2 = "NOTICE: Moret AMT 945 MBOL Process"
		tattach2 = " "
		SELECT 0
		USE F:\3pl\DATA\mailmaster ALIAS mm SHARED
		LOCATE FOR edi_type = "MISC" AND taskname = "MORETMBOL"
		lUseAlt = mm.use_alt
		tsendto2 = ALLTRIM(IIF(lUseAlt,mm.sendtoalt,mm.sendto))
		tcc2 = ALLTRIM(IIF(lUseAlt,mm.ccalt,mm.cc))
		IF lTesting
			tcc2 = ""
		ENDIF
		USE IN mm

		tmessage2 = "Potential files for Moret AMT MBOL# "+cBOL+"...monitor output for correct number of files."
		tmessage2 = tmessage2 + CHR(13) + "Office "+cOffice+", Expected number of 945 files: "+cSubBillCnt
		tmessage2 = tmessage2 + CHR(13) + "Processed at "+TTOC(DATETIME())
		DO FORM m:\dev\FRM\dartmail2 WITH tsendto2,tfrom,tsubject2,tcc2,tattach2,tmessage2,"A"
	ENDIF
*!* End Mailing section

	cxCoCode = ""
	SELECT outship
	=SEEK(cBOL,'outship','bol_no')
	alength = ALINES(apt,outship.SHIPINS,.T.,CHR(13))

	SELECT 0
	CREATE CURSOR tempov (ship_ref c(20))

	lOverflow = .F.
	SELECT 0
	SELECT ship_ref ;
		FROM outship ;
		WHERE outship.bol_no = cMBOL ;
		AND !("OV"$outship.ship_ref) ;
		INTO CURSOR tempout1

	SCAN
		cPTCheck = ALLTRIM(tempout1.ship_ref)
		SELECT outship
		COUNT TO N FOR outship.ship_ref = cPTCheck
		IF N > 1
			INSERT INTO tempov (ship_ref) VALUES (cPTCheck)
			lOverflow = .T.
		ENDIF
	ENDSCAN
	RELEASE cPTCheck
	USE IN tempout1
	IF lOverflow
		WAIT WINDOW "There are one or more OV PTs attached to this Master BOL" TIMEOUT 2
	ENDIF

	SELECT outship
	IF !SEEK(cBOL,'outship','bol_no')
		WAIT WINDOW "BOL not found in OUTSHIP" TIMEOUT 2
		DO ediupdate WITH "BOL NOT IN O/S",.T.
		THROW
		USE F:\3pl\DATA\parcel_carriers ALIAS pacas IN 0
		IF SEEK(outship.scac,'pacas','scac')
			DO ediupdate WITH "PARCEL SCAC",.T.
			THROW
		ENDIF
		USE IN pacas
	ENDIF

	SELECT dcnum FROM outship WHERE outship.bol_no = cBOL GROUP BY 1 INTO CURSOR tempx
	LOCATE
	IF EOF()
		cErrMsg = "EMPTY DC/STORE#"
		ediupdate(cErrMsg,.T.)
		THROW
	ENDIF
	IF USED('tempx')
		USE IN tempx
	ENDIF

	nWO_Num = outship.wo_num
	cWO_Num = ALLTRIM(STR(nWO_Num))
	cShip_ref = ALLTRIM(outship.ship_ref)
	cRetMsg = ""

	csql="tgfnjsql01"
	
*!*		SELECT outdet
*!*		LOCATE
*!*		SELECT outship
*!*		LOCATE
*!*		IF SEEK(cBOL,'outship','bol_no')
*!*			alength = ALINES(apt,outship.SHIPINS)  && Outship SHIPINS array
*!*			cSQLServer = segmentget(@apt,'SQLSERVER',alength)
*!*	*xgetsql
*!*			cSQL = IIF(LEFT(cSQLServer,3)="SQL","tgfnjsql01",cSQLServer)
*!*			cSQL = IIF(LEFT(cSQLServer,3)="SP3","tgfnjsql01",cSQLServer)
*!*			IF !lTesting AND !lSkipCompare
*!*				DO m:\dev\prg\sqldata-comparemoret WITH cUseFolder,cBOL,cOffice,nAcctNum,cPPName,cSQL
*!*				IF cRetMsg<>"OK"
*!*					lCloseOutput = .T.
*!*					cStatus = cRetMsg
*!*					DO ediupdate WITH cStatus,.T. && "SQL ERROR, WO "+cWO_Num,.T.
*!*					THROW
*!*				ENDIF
*!*			ENDIF
*!*		ENDIF

	IF USED("sqlwomoret")
		USE IN sqlwomoret
	ENDIF
	DELETE FILE "F:\3pl\DATA\sqlwomoret.dbf"

	WAIT WIND "Now in SQL Connect process..." NOWAIT NOCLEAR
	cRetMsg = ""
	DO m:\dev\prg\sqlconnectmoret_bol  WITH nAcctNum,cBOL,cPPName,lUCC,cOffice,lDoCartons,cSQL
	IF cRetMsg<>"OK"
		WAIT WINDOW "Error in cartonization extract" TIMEOUT 2
		DO ediupdate WITH cRetMsg,.T.
		THROW
	ENDIF

	IF lTesting OR lTestMail OR DATETIME()<DATETIME(2013,09,04,22,30,00)
		SELECT vmoretedipp
		LOCATE
		BROWSE TIMEOUT 20
	ENDIF

	WAIT WIND "Now performing VMORETPP cleanup chores" NOWAIT
	SELECT wo_num FROM vmoretedipp GROUP BY 1 INTO CURSOR temprunid1
	SELECT runid  FROM vmoretedipp GROUP BY 1 INTO CURSOR temprunid2
	SELECT temprunid1
	STORE RECCOUNT() TO nWORecs
	SELECT temprunid2
	STORE RECCOUNT() TO nRIDRecs

	IF nWORecs#nRIDRecs
		SET STEP ON
		DO ediupdate WITH "MULTI RUNIDS",.T.
		THROW
	ENDIF
	USE IN temprunid1
	USE IN temprunid2

	IF lBrowVPP
		DEFINE WINDOW WINSML FROM 3,3 TO 40,60 FLOAT
		SELECT vmoretedipp
		LOCATE
		BROWSE IN WINDOW WINSML
	ENDIF

	IF !USED("scacs")
		xsqlexec("select * from scac","scacs",,"wh")
		INDEX ON scac TAG scac
	ENDIF

	SELECT outship
	SET ORDER TO
	LOCATE
	WAIT CLEAR
	WAIT WINDOW "Now creating Header information" TIMEOUT 1

	IF !SEEK(nWO_Num,'outship','wo_num')
		WAIT CLEAR
		WAIT WINDOW "Invalid Work Order Number - Not Found in OUTSHIP!" TIMEOUT 2
		DO ediupdate WITH "WO NOT FOUND",.T.
		THROW
	ENDIF

*************************************************************************
* 	OUTSHIP RECORD MAIN LOOP
*	Loops through individual OUTSHIP lines for BOL#
*************************************************************************
	WAIT CLEAR
	WAIT WINDOW "Now creating MBOL#-based information..." NOWAIT NOCLEAR


	SELECT outship
	SET ORDER TO
	IF  lFakeDelivery
		STORE dFakeDate TO m.del_date
	ELSE
		COUNT TO N FOR outship.bol_no = cBOL AND !EMPTYnul(del_date)
		IF N=0
			DO ediupdate WITH "INCOMP BOL, Del Date",.T.
			THROW
		ENDIF
	ENDIF
	LOCATE

	WAIT CLEAR
	WAIT WINDOW "Now retrieving Master BOL#-based information..." NOWAIT NOCLEAR
	nSubBillCnt = 0
	nAtNewSubBill = 0
	SELECT bl
	LOCATE
	cMissDel = ""
	nBLID = 0
	nFileID = 0
	lMultiAcct = .F.
	bolscanstr = "bl.mblnum = cMBOL"
	SELECT DISTINCT accountid ;
		FROM bl ;
		WHERE &bolscanstr ;
		INTO CURSOR tempacctid
	IF RECCOUNT() > 1
		WAIT CLEAR
		WAIT WINDOW "There are multiple accounts in this Master BOL" TIMEOUT 2
		lMultiAcct = .T.
		cAcctStr = ""
		LOCATE
		SCAN
			IF EMPTY(cAcctStr)
				cAcctStr = ALLTRIM(STR(tempacctid.accountid))
			ELSE
				cAcctStr = cAcctStr+","+ALLTRIM(STR(tempacctid.accountid))
			ENDIF
		ENDSCAN
	ENDIF
	IF cMBOL = '00863233000322693'
		cAcctStr = cAcctStr+",5741"
	ENDIF

	USE IN tempacctid

	SELECT bl
	COUNT TO nTotSubs FOR &bolscanstr
	cTotSubs = TRANSFORM(nTotSubs)
	WAIT WINDOW "Will scan through "+cTotSubs+" sub-BOLs..." TIMEOUT 1
	LOCATE FOR &bolscanstr
	nCurrSub = 0
	SCAN FOR &bolscanstr
*!* HEADER LEVEL EDI DATA
		IF bl.bol_no='07636183000189093'
			LOOP
		endif

		lDoISAHeader = .T.
		DO num_incr_isa

		cISA_Num = PADL(c_CntrlNum,9,"0")
		nISA_Num = INT(VAL(cISA_Num))

		IF lTesting
			cISACode = "T"
		ELSE
			cISACode = "P"
		ENDIF
		nBOLQty = 0
		nBOLWt  = 0

		cRolledRecs = ""
		CREATE CURSOR temp945 (accountid i,isa_num c(10),st_num c(10),wo_num i,bol_no c(20),ship_ref c(20),filename c(50),filedate T)
		renewdatetime()
		WAIT WINDOW "" TIMEOUT 2
		nAcctNum = bl.accountid
		cBOL = ALLTRIM(bl.bol_no)
		cWO = ALLTRIM(STR(nWO_Num))
		nBLID = bl.blid

		nCurrSub = nCurrSub+1
		cCurrSub = ALLTRIM(STR(nCurrSub))
		IF lTesting
			WAIT WINDOW "Now processing sub-BOL "+cBOL TIMEOUT 2
		ENDIF

*!* This section added to trap cut sub-BOLs or PTs
		WAIT WINDOW "Check for cut orders..." TIMEOUT 2
		CREATE CURSOR addpts (ship_ref c(20),del_date d,delenterdt T)

		xsqlexec("select * from bldet where mod='"+goffice+"' and blid="+TRANSFORM(nBLID),,,"wh")

		SELECT ship_ref,combinedpt FROM bldet WHERE bldet.blid = nBLID INTO CURSOR tmpbldet
		SCAN
			IF tmpbldet.combinedpt
				SELECT ship_ref,del_date,emptynul(delenterdt) FROM outship WHERE outship.combpt = tmpbldet.ship_ref INTO CURSOR t1
			ELSE
				SELECT ship_ref,del_date,emptynul(delenterdt) FROM outship WHERE outship.ship_ref = tmpbldet.ship_ref INTO CURSOR t1
			ENDIF
			SELECT addpts
			APPEND FROM DBF('t1')
			USE IN t1
		ENDSCAN
		SELECT addpts
		nRecAP= RECCOUNT('addpts')
		COUNT TO nempties FOR EMPTY(addpts.del_date) AND EMPTY(addpts.delenterdt)
		USE IN addpts
		IF nempties = nRecAP
			LOOP
		ENDIF
*!* End of addition

		swc_cutctns(cBOL)

		IF lTesting
			SET STEP ON
		ENDIF
		SELECT bl
		cBOL = ALLTRIM(bl.bol_no)
		nBLID = bl.blid
		nAcctNum = bl.accountid

*!*		Insert MBOL/SBOL numbers below to run single sub-BOLs
		IF (cMBOL = "00863233000826825" AND !INLIST(cBOL,"00863233000826818","00863233000826801"))  && and lTesting
			LOOP
		ENDIF


		WAIT CLEAR
		cWaitStr = "At BL MBOL scan. MBOL: "+cMBOL+", SubBOL: "+cBOL+CHR(13)+"Loop "+cCurrSub+" of "+cTotSubs
		IF lTesting
			WAIT WINDO cWaitStr NOWAIT
		ELSE
			WAIT WINDO cWaitStr TIME 2

		ENDIF
		SCATTER FIELDS accountid MEMVAR

		nAtNewSubBill = nAtNewSubBill + 1
		cAtNewSubBill = ALLTRIM(STR(nAtNewSubBill))
		nFileID = nFileID+1
		cFileID = "-"+ALLT(STR(nFileID))
		cxCoCode = ""
*		cxCoCode = segmentget(@apt,"COCODE",alength)

		IF lDoISAHeader
			lDoISAHeader = .F.
			IF EMPTY(cxCoCode)
				IF USED('mdivs')
					USE IN mdivs
				ENDIF

				IF lTesting
*				SET STEP ON
				ENDIF

				USE F:\3pl\DATA\moretedidivs IN 0 ALIAS mdivs
				IF nAcctNum = 5448
					IF !SEEK(6313,'mdivs','accountid')
						cErrMsg = "ACCT# "+ALLTRIM(STR(nAcctNum))+" NOT IN MDIVS"
						DO ediupdate WITH cErrMsg,.T.
						THROW
					ELSE
						IF lTesting
							cxCoCode = "REG"
						ELSE
							cxCoCode = TRIM(mdivs.cocode)
						ENDIF
						USE IN mdivs
					ENDIF
				ELSE
					IF !SEEK(nAcctNum,'mdivs','accountid')
						SET STEP ON
						DO ediupdate WITH "ACCT# NOT FOUND"+ALLTRIM(STR(nAcctNum)),.T.
						THROW
					ELSE
						cxCoCode = mdivs.cocode
					ENDIF
				ENDIF
			ENDIF



			crecqual = "ZZ"
			STORE "" TO cRecID,cCoNum

			DO CASE
				CASE cxCoCode = "SBH"
					cRecID = "SBHINTMTS"
					cCoNum = "02"
				CASE cxCoCode = "HBE"
					cRecID = "HBESTLMTD"
					cCoNum = "03"
				CASE cxCoCode = "HPD"
					cRecID = "HIGHPOINT"
					cCoNum = "04"
				CASE cxCoCode = "REG"
					cRecID = "REGENTSUT"  && Regent-Sutton
					cCoNum = "05"
				CASE cxCoCode = "BOZ"
					cRecID = "BOZKEELLC"  && Bozkee
					cCoNum = "08"
				CASE cxCoCode = "SK"
					cRecID = "MORET00SK"  && Moret SK
					cCoNum = "06"
				CASE cxCoCode = "BMG"
					cRecID = "BOYMEETSG"  && Boy Meets Girl
					cCoNum = "07"
				CASE cxCoCode = "SPM"
					cRecID = "SPRAYMORET"  && Spray Moret
					cCoNum = "09"
				OTHERWISE
					cRecID = "JMORETINC"
					cCoNum = "01"
			ENDCASE

			STORE "" TO cFilenameShort,cFilenameHold,cFilenameOut,cFilenameArch
			lcPath = cOutFolder
			cFilenameShort = cCoNum+"_"+cWhseAbbrev+"_945_"+TTOC(DATETIME(),1)+".edi"
			cFilenameHold = (cHoldFolder+cFilenameShort)
			cFilenameOut = (cOutFolder+cFilenameShort)
			cFilenameArch = (cArchfolder+cFilenameShort)

			nFilenum = FCREATE(cFilenameHold)
			IF nFilenum=(-1)
				WAIT WINDOW "Can't create LL File" TIMEOUT 2
				DO ediupdate WITH .T.,"BAD LL FILE CREATE"
			ENDIF

			STORE cxCoCode TO cCoCode
			IF lMultiAcct
				cCoCodeGrp = IIF(EMPTY(cCoCodeGrp),cCoCode,cCoCodeGrp+","+cCoCode)
			ELSE
				cCoCodeGrp = cCoCode
			ENDIF
			cxCoCode = ""
			crecidlong = PADR(cRecID,15," ")

			STORE "ISA"+cfd+"00"+cfd+"          "+cfd+"00"+cfd+"          "+cfd+csendqual+cfd+csendidlong+cfd+;
				crecqual+cfd+crecidlong+cfd+cTruncDate+cfd+cTruncTime+cfd+"U"+cfd+"00401"+cfd+;
				cISA_Num+cfd+"0"+cfd+cISACode+cfd+cterminator+csegd TO cString
			DO cstringbreak

			STORE "GS"+cfd+"SW"+cfd+csendid+cfd+cRecID+cfd+cDate+cfd+cTruncTime+cfd+c_CntrlNum+;
				cfd+"X"+cfd+cX12+csegd TO cString
			DO cstringbreak
		ENDIF

		cOSScanStr=""
		WAIT WINDOW "" TIMEOUT 3

		SELECT 0
		CREATE CURSOR zeroqty (consignee c(40),ship_ref c(20),cnee_ref c(25))

		xsqlexec("select * from bldet where mod='"+goffice+"' and blid="+TRANSFORM(nBLID),,,"wh")

		IF !FOUND()
			SET STEP ON
			cErrMsg = "MISS BLID ERR: "+ALLTRIM(STR(nBLID))
			ediupdate(cErrMsg,.T.)
		ENDIF
		SELECT 0
		CREATE CURSOR temppt (ship_ref c(20))
		nSubCnt = nSubCnt + 1

		SELECT bldet
		WAIT WINDOW "Scanning BLID: "+TRANSFORM(nBLID) NOWAIT
		LOCATE
		nPTCount = 0
		SCAN FOR bldet.blid = nBLID
			CREATE CURSOR holdsplits (ship_ref1 c(10))
			SELECT temppt
			INDEX ON ship_ref TAG ship_ref
*			ASSERT .F. MESSA "AT BLDET SCAN...DEBUG HERE!!!"
			cBLShip_ref = ALLTRIM(bldet.ship_ref)
			lCombPT = IIF(bldet.combinedpt,.T.,.F.)
			nAcctNumDet = bldet.accountid
			IF lMultiAcct  && If multiple accounts in a single MBOL.
				IF lCombPT
					cOSScanStr = "outship.combpt = PADR(cBLShip_ref,20)" && AND INLIST(OUTSHIP.ACCOUNTID,&cAcctStr)"
				ELSE
					cOSScanStr = "outship.ship_ref = PADR(cBLShip_ref,20)" && AND INLIST(OUTSHIP.ACCOUNTID,&cAcctStr)"
				ENDIF
			ELSE
				IF lCombPT
					cOSScanStr = "outship.combpt = PADR(cBLShip_ref,20) AND OUTSHIP.ACCOUNTID=nAcctNumDet"
				ELSE
					cOSScanStr = "outship.ship_ref = PADR(cBLShip_ref,20) AND OUTSHIP.ACCOUNTID=nAcctNumDet"
				ENDIF
			ENDIF


			SELECT outship
			LOCATE FOR &cOSScanStr
			IF !FOUND()
				ASSERT .F. MESSAGE "OS-BLDET Mismatch...debug"
				SET STEP ON
				cErrMsg = "No OS/BLDET match, SUBBOL "+cBOL
				ediupdate(cErrMsg,.T.)
			ENDIF

			SELECT outship
			LOCATE
			SUM(outship.ctnqty) TO nSubBOLQty FOR &cOSScanStr
			LOCATE
			SUM(outship.weight) TO nSubBOLWt FOR &cOSScanStr
			IF EMPTY(nBOLWt) AND (lTestinput OR lTesting)
				nBOLWt = 5*nBOLQty
			ENDIF
			nBOLQty = nBOLQty + nSubBOLQty
			nBOLWt  = nBOLWt  + nSubBOLWt
			SELECT outship
			LOCATE FOR &cOSScanStr
			WAIT WIND "Scanning for outship.ship_ref =  "+cBLShip_ref NOWAIT
			SCAN FOR &cOSScanStr
				IF ((" OV"$outship.ship_ref) OR (outship.qty = 0) OR (outship.ctnqty = 0 AND !lMasterpack))
					IF outship.qty = 0
						INSERT INTO zeroqty (consignee,ship_ref,cnee_ref) VALUES (outship.consignee,outship.ship_ref,outship.cnee_ref)
					ENDIF
					LOOP
				ENDIF
				IF EMPTY(outship.del_date)
					LOOP
				ENDIF

				SCATTER MEMVAR
				IF  lFakeDelivery
					STORE dFakeDate TO m.del_date
				ENDIF
*				ASSERT .F. MESSAGE "At OUTSHIP scan loop...>>DEBUG<<"
				nWO_Num = outship.wo_num
				cShip_ref = ALLTRIM(outship.ship_ref)
				lMasterpack = outship.masterpack
				nPTCount = nPTCount + 1
				cPO = ALLTRIM(outship.cnee_ref)
				IF EMPTY(cShip_ref)
					DO ediupdate WITH "EMPTY PT-WO "+cWO_Num,.T.
					THROW
				ENDIF
				INSERT INTO mfilesout945 (ship_ref,filename,isa_num) VALUES (cShip_ref,cFilenameShort,cISA_Num)

				alength = ALINES(apt,outship.SHIPINS)  && Outship SHIPINS array
				cFOBPymt = segmentget(@apt,'FOBPYMT',alength)
				cFOB = segmentget(@apt,'FOB',alength)

				lRepeatPT = IIF("~"$cShip_ref,.T.,.F.)
				lRepeatPT = IIF(cMBOL#'88784233000012886',lRepeatPT,.F.)
				IF lRepeatPT
					WAIT WINDOW "This is a SPLIT PT" TIMEOUT 1
				ELSE
					tx = 1
				ENDIF

				IF "~"$cShip_ref AND lRepeatPT
					m.ship_ref1 = LEFT(cShip_ref,AT("~",cShip_ref)-1)
					SELECT holdsplits
					LOCATE FOR holdsplits.ship_ref1 = m.ship_ref1
					IF !FOUND()
						INSERT INTO holdsplits FROM MEMVAR
						SELECT outship
					ENDIF
				ENDIF

				IF lRepeatPT
					cShip_ref_out = ALLT(LEFT(cShip_ref,AT('~',cShip_ref)-1))
					cAltShip_ref = IIF("U"$cShip_ref,cShip_ref_out+" ~C",cShip_ref_out+" ~U")
					IF SEEK(cShip_ref_out,'temppt','ship_ref')
						LOOP
					ELSE
						INSERT INTO temppt (ship_ref) VALUES (cShip_ref_out)
					ENDIF
				ELSE
					STORE cShip_ref TO cShip_ref_out
				ENDIF

				IF EMPTY(ALLT(m.appt_time))
					cApptTime = "153000"
				ELSE
					nApptTime = INT(VAL(LEFT(m.appt_time,4)))
					IF SUBSTR(m.appt_time,5,1)="P"
						IF BETWEEN(nApptTime,1200,1259)
							cApptTime = ALLT(STR(nApptTime))+"00"
						ELSE
							cApptTime = ALLT(STR(nApptTime + 1200))+"00"
						ENDIF
					ELSE
						cApptTime = PADL(ALLT(STR(nApptTime)),4,"0")+"00"
					ENDIF
				ENDIF

*!* Added this code to trap miscounts in OUTDET Units

				xsqlexec("select * from outwolog where wo_num = "+TRANSFORM(nWO_Num)+"and (accountid in ("+gMoretAcctList+") or accountid in ("+gMoretAcctList2+"))",,,"wh")
				lPick = IIF(outwolog.picknpack # .T.,.F.,.T.)
				lPrepack = !lPick

				WAIT WINDOW "This is a "+IIF(lPrepack,"Pre-pack","PnP")+" order" NOWAIT
				IF lPrepack

					SET DELETED ON
					IF lOverflow
						SELECT tempov
						LOCATE FOR tempov.ship_ref = cShip_ref
						IF FOUND()
							SET DELETED OFF
						ENDIF
					ENDIF

					SELECT outdet
					SET ORDER TO
					SUM IIF(lOverflow,outdet.origqty,outdet.totqty) TO nUnitTot1 FOR units AND outdet.outshipid = outship.outshipid
					SUM (VAL(outdet.PACK)*IIF(lOverflow,outdet.origqty,outdet.totqty)) TO nUnitTot2 FOR !units AND outdet.outshipid = outship.outshipid
					IF nUnitTot1<>nUnitTot2
						SET STEP ON
						WAIT WINDOW "TOTQTY ERROR AT PT "+cShip_ref TIMEOUT 5
						DO ediupdate WITH "OUTDET UNITQTY ERR",.T.
						THROW
					ENDIF
					SELECT vmoretedipp
					SUM totqty TO nUnitTot2 FOR vmoretedipp.outshipid = outship.outshipid AND vmoretedipp.ucc # 'CUTS'
					IF nUnitTot1<>nUnitTot2
						SET STEP ON
						DO ediupdate WITH "SQL TOTQTY ERR "+TRANSFORM(outship.wo_num),.T.
						THROW
					ENDIF
				ENDIF
				SET DELETED ON
*!* End code addition

				cPTString = IIF(EMPTY(cPTString),PADR(ALLTRIM(m.consignee),42)+cShip_ref_out,cPTString+CHR(13)+;
					PADR(ALLTRIM(m.consignee),42)+cShip_ref_out)
*nPTCount = nPTCount+1

				nCuFT = outship.cuft
				IF EMPTY(nCuFT)
					nCuFT = (2*outship.qty)
				ENDIF
				lMacy = IIF("MACY"$m.consignee,.T.,.F.)
				lJCPenney = IIF(("JCP"$m.consignee OR "PENNEY"$m.consignee),.T.,.F.)
				lWalmart = IIF((m.consignee="WAL" AND "MART"$m.consignee),.T.,.F.)
				lToys = IIF("TOYS"$m.consignee,.T.,.F.)
				lDillard = IIF("DILLARD"$m.consignee,.T.,.F.)
				lVFO = IIF(m.consignee="VF",.T.,.F.)
				lAmazon = IIF("AMAZON"$m.consignee,.T.,.F.)
				lBoscov = IIF("BOSCOV"$m.consignee,.T.,.F.)
*				SET STEP ON
				lAAFES = IIF(("ARMY"$m.consignee AND "AIR"$m.consignee AND "FORCE"$m.consignee),.T.,.F.)
				IF lAAFES
					WAIT WINDOW "THIS IS AN ARMY/AIR FORCE EXCHANGE ORDER" TIMEOUT 1
				ENDIF



				cWO_Num = ALLTRIM(STR(m.wo_num))

				IF !(cWO_Num$cWO_NumStr)
					cWO_NumStr = IIF(EMPTY(cWO_NumStr),cWO_Num,cWO_NumStr+","+cWO_Num)
				ENDIF
				IF !(cWO_Num$cWO_NumList)
					cWO_NumList = IIF(EMPTY(cWO_NumList),cWO_Num,cWO_NumList+CHR(13)+cWO_Num)
				ENDIF

				nTotCtnCount = m.qty
				cPO_Num = ALLTRIM(m.cnee_ref)

				IF ("PENNEY"$UPPER(outship.consignee) OR "DICK"$UPPER(outship.consignee) OR "KMART"$UPPER(outship.consignee) OR "K-MART"$UPPER(outship.consignee))
					lApptFlag = .T.
				ELSE
					lApptFlag = .F.
				ENDIF

				ddel_date = outship.del_date
				dexpdel_date = outship.expdeldt  && Expected delivery date (at facility)
				IF EMPTY(dexpdel_date)
					dexpdel_date = DATE()+3
				ENDIF
				IF lFakeDelivery
					ddel_date = dFakeDate
					dexpdel_date = dFakeDate+3
				ENDIF

				IF lTestinput
					IF EMPTY(ddel_date)
						ddel_date = DATE()
					ENDIF
					dApptNum = "99999"
					dapptdate = DATE()
				ELSE
					ddel_date = outship.del_date
					IF EMPTY(ddel_date)
						cMissDel = IIF(EMPTY(cMissDel),"The following PTs had no Delivery Dates:"+CHR(13)+TRIM(cShip_ref),cMissDel+CHR(13)+TRIM(cShip_ref))
					ENDIF
					dApptNum = ALLTRIM(outship.appt_num)

					IF !lTesting
						IF EMPTY(dApptNum) && Penney/KMart Appt Number check
							IF !(lApptFlag)
								dApptNum = ""
							ELSE
								DO ediupdate WITH "EMPTY APPT #",.T.
								THROW
							ENDIF
						ENDIF
					ELSE
						dApptNum = "99999"
					ENDIF
					dapptdate = outship.appt

					IF EMPTY(dapptdate)
						dapptdate = outship.del_date
					ENDIF
				ENDIF

				IF ALLTRIM(outship.SForCSZ) = ","
					BLANK FIELDS outship.SForCSZ NEXT 1 IN outship
				ENDIF

				alength = ALINES(apt,outship.SHIPINS,.T.,CHR(13))

				cTRNum = ""
				cPRONum = ""
				STORE STRTRAN(TRIM(outship.keyrec),"PR","") TO cPRONum  && Changed to always use as PRO# per Juan, 10.21.2013
*!*					IF LEFT(outship.keyrec,2) = "PR"
*!*						STORE STRTRAN(TRIM(outship.keyrec),"PR","") TO cPRONum
*!*					ENDIF
*!*					IF LEFT(outship.keyrec,2) = "TR"
*!*						STORE STRTRAN(TRIM(outship.keyrec),"TR","") TO cTRNum
*!*					ENDIF

				nOutshipid = m.outshipid
				m.CSZ = TRIM(m.CSZ)
				IF OCCURS(" ",m.CSZ) < 2
					SET STEP ON
					cErrMsg = "TOO FEW SPACES-CSZ "+cShip_ref
					ediupdate(cErrMsg,.T.)
					THROW
				ENDIF

				IF EMPTY(M.CSZ)
					WAIT CLEAR
					WAIT WINDOW "No SHIP-TO City/State/ZIP info...exiting" TIMEOUT 2
					DO ediupdate WITH "BAD ADDRESS INFO",.T.
					THROW
				ENDIF
				IF !lAAFES
					m.CSZ = TRIM(STRTRAN(m.CSZ,"  "," "))
				ENDIF
				IF !","$m.CSZ
					DO ediupdate WITH "NO COMMA CSZ-"+cShip_ref,.T.
					THROW
				ENDIF
				nSpaces = OCCURS(' ',m.CSZ)
				IF nSpaces < 2
					WAIT WINDOW "Ship-to CSZ Segment Error" TIMEOUT 3
					DO ediupdate WITH "Ship-to CSZ: "+cShip_ref,.T.
					THROW
				ELSE
					IF outship.outshipid = 2120893
						SET STEP ON
					ENDIF
					nCommaPos = AT(",",m.CSZ)
					nLastSpace = AT(" ",m.CSZ,nSpaces)
					IF ISALPHA(SUBSTR(TRIM(m.CSZ),AT(" ",m.CSZ,nSpaces-1)+1,2)) OR lAAFES
						cCity = LEFT(TRIM(m.CSZ),AT(",",m.CSZ)-1)
						cState = SUBSTR(m.CSZ,nCommaPos+2,2)
						cZip = TRIM(SUBSTR(m.CSZ,nLastSpace+1))
					ELSE
						WAIT CLEAR
						WAIT WINDOW "NOT ALPHA: "+SUBSTR(TRIM(m.CSZ),AT(" ",m.CSZ,nSpaces-1)+1,2) TIMEOUT 3
						cCity = LEFT(TRIM(m.CSZ),AT(",",m.CSZ)-1)
						cState = SUBSTR(TRIM(m.CSZ),AT(" ",m.CSZ,nSpaces-2)+1,2)
						cZip = TRIM(SUBSTR(m.CSZ,nLastSpace+1))
					ENDIF
				ENDIF

				STORE "" TO cSForCity,cSForState,cSForZip
				IF !lFederated
					IF !EMPTY(M.SForCSZ)
						m.SForCSZ = TRIM(STRTRAN(m.SForCSZ,"  "," "))
						nSpaces = OCCURS(" ",TRIM(m.SForCSZ))
						IF nSpaces = 0
							m.SForCSZ = STRTRAN(m.SForCSZ,",","")
							cSForCity = ALLTRIM(m.SForCSZ)
							cSForState = ""
							cSForZip = ""
						ELSE
							nCommaPos = AT(",",m.SForCSZ)
							nLastSpace = AT(" ",m.SForCSZ,nSpaces)
							nMinusSpaces = IIF(nSpaces=1,0,1)
							IF ISALPHA(SUBSTR(TRIM(m.SForCSZ),AT(" ",TRIM(m.SForCSZ),nSpaces-nMinusSpaces)+1,2)) OR lAAFES
								cSForCity = LEFT(TRIM(m.SForCSZ),AT(",",m.SForCSZ)-1)
								cSForState = SUBSTR(m.SForCSZ,nCommaPos+2,2)
								cSForZip = TRIM(RIGHT(TRIM(m.SForCSZ),5))
								IF ISALPHA(cSForZip)
									cSForZip = ""
								ENDIF
							ELSE
								WAIT CLEAR
								WAIT WINDOW "NOT ALPHA: "+SUBSTR(TRIM(m.SForCSZ),AT(" ",m.SForCSZ,nSpaces-1)+1,2) TIMEOUT 3
								cSForCity = LEFT(TRIM(m.SForCSZ),AT(",",m.SForCSZ)-1)
								cSForState = SUBSTR(TRIM(m.SForCSZ),AT(" ",m.SForCSZ,nSpaces-2)+1,2)
								cSForZip = TRIM(RIGHT(TRIM(m.SForCSZ),5))
								IF ISALPHA(cSForZip)
									cSForZip = ""
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ELSE
					cStoreName = segmentget(@apt,"STORENAME",alength)
				ENDIF

				m.ctnqty = IIF(lMasterpack,1,outship.ctnqty)
				m.weight = outship.weight
				IF lRepeatPT
					nRepLoops = 2
					SELECT outship
					nRec = RECNO()
					LOCATE
					LOCATE FOR outship.ship_ref = PADR(TRIM(cAltShip_ref),20)
					IF !FOUND()
						cErrMsg = "Problem: Alternate PT "+cAltShip_ref+" not found"
						WAIT WINDOW cErrMsg TIMEOUT 5
						ediupdate(cErrMsg,.T.)
						THROW

					ENDIF
					IF lTestinput
						IF outship.weight = 0
							REPLACE outship.weight WITH 2*IIF(lMasterpack,1,outship.ctnqty)
						ENDIF
					ENDIF
					m.ctnqty = m.ctnqty + IIF(lMasterpack,1,outship.ctnqty)
					m.weight = m.weight + outship.weight
					nAltOutshipid = outship.outshipid
					GO nRec
				ELSE
					nRepLoops = 1
				ENDIF

				DO num_incr_st
				WAIT CLEAR
				WAIT WINDOW "Now creating Line Item information" NOWAIT NOCLEAR

				INSERT INTO temp945 (accountid,isa_num,st_num,wo_num,bol_no,ship_ref,filename,filedate) ;
					VALUES (m.accountid,c_CntrlNum,c_GrpCntrlNum,m.wo_num,m.bol_no,m.ship_ref,cFilenameShort,dt2)

				STORE "ST"+cfd+"945"+cfd+PADL(c_GrpCntrlNum,9,"0")+csegd TO cString
				DO cstringbreak
				nSTCount = nSTCount + 1
				nSegCtr = 1

				IF EMPTY(cShip_ref_out)
					WAIT WIND "MISSING SHIP REF IN W06...DEBUG!!"
					SET STEP ON
				ENDIF
				cShip_ref_out = IIF(cMBOL#'00863233000393273',cShip_ref_out,LEFT(cShip_ref_out,8))
				STORE "W06"+cfd+"N"+cfd+cShip_ref_out+cfd+cDate+cfd+TRIM(cWO_Num)+cfd+cfd+cPO_Num+csegd TO cString
				DO cstringbreak
				nSegCtr = nSegCtr + 1
				nCtnNumber = 1  && Seed carton sequence count

				cDCNum = ALLTRIM(m.dcnum)
				cDCNumStored = ALLTRIM(segmentget(@apt,"STORENUM",alength))
				IF cDCNum # cDCNumStored
					cDCNum = cDCNumStored
				ENDIF
				IF EMPTY(TRIM(cDCNum))
					IF (!EMPTY(m.storenum) AND m.storenum<>0)
						cDCNum = ALLTRIM(STR(m.storenum))
					ENDIF
					IF EMPTY(cDCNum)
						DO ediupdate WITH "MISS STORENUM",.T.
						THROW
					ENDIF
				ENDIF

				STORE "N1"+cfd+"SF"+cfd+"FMI INTERNATIONAL INC. -"+UPPER(cDivision)+cfd+"91"+cfd+"FMI"+cCustLoc+csegd TO cString
				DO cstringbreak
				nSegCtr = nSegCtr + 1

				IF !lFederated
					STORE "N3"+cfd+TRIM(cSF_Addr1)+csegd TO cString
					DO cstringbreak
					nSegCtr = nSegCtr + 1

					STORE "N4"+cfd+cSF_CSZ+cfd+"USA"+csegd TO cString
					DO cstringbreak
					nSegCtr = nSegCtr + 1
				ENDIF

				STORE "N1"+cfd+"ST"+cfd+ALLTRIM(m.consignee)+cfd+"92"+cfd+cDCNum+csegd TO cString
				DO cstringbreak
				nSegCtr = nSegCtr + 1

				STORE "N3"+cfd+TRIM(outship.address)+csegd TO cString
				DO cstringbreak
				nSegCtr = nSegCtr + 1

				STORE "N4"+cfd+cCity+cfd+cState+cfd+cZip+cfd+"USA"+csegd TO cString
				DO cstringbreak
				nSegCtr = nSegCtr + 1

				cSforstore = TRIM(m.sforstore)
				IF !EMPTY(m.sforstore)
					STORE "N1"+cfd+"BY"+cfd+ALLTRIM(m.shipfor)+cfd+"91"+cfd+TRIM(m.sforstore)+csegd TO cString
					DO cstringbreak
					nSegCtr = nSegCtr + 1
				ENDIF

				STORE "N9"+cfd+"MB"+cfd+ALLTRIM(cMBOL)+csegd TO cString
				DO cstringbreak
				nSegCtr = nSegCtr + 1

				STORE "N9"+cfd+"BM"+cfd+ALLTRIM(cBOL)+csegd TO cString
				DO cstringbreak
				nSegCtr = nSegCtr + 1

				IF !EMPTY(cPRONum)
					STORE "N9"+cfd+"CN"+cfd+cPRONum+csegd TO cString
					DO cstringbreak
					nSegCtr = nSegCtr + 1
				ENDIF

				IF !EMPTY(cTRNum)
					STORE "N9"+cfd+"TR"+cfd+cTRNum+csegd TO cString
					DO cstringbreak
					nSegCtr = nSegCtr + 1
				ENDIF

*!* For JCPenney orders only
				IF (lJCPenney OR lWalmart OR lToys OR lDillard OR lAmazon OR lVFO OR lBoscov) && Changed per Avner, 05.18.2012. Further revised per Liana, 11.05.2015.
					IF lTesting AND EMPTY(outship.appt_num)
						cLoadid = PADR(ALLTRIM(STR(nLoadid)),6,"0")
						nLoadid = nLoadid + 1
					ELSE
						cLoadid = ALLTRIM(outship.appt_num)
*!*						cLoadid = ALLTRIM(m.batch_num)
*!*						IF (EMPTY(cLoadid) OR cLoadid=="C" OR cLoadid=="U")
*!*							cLoadid = ALLTRIM(m.appt_num)
*!*						ENDIF
					ENDIF
					IF (EMPTY(cLoadid) OR cLoadid=="C" OR cLoadid=="U")
						DO ediupdate WITH "BAD/MISS LOADID",.T.
						THROW
					ENDIF
*!*					ENDIF
					IF !EMPTY(cLoadid)
						STORE "N9"+cfd+"P8"+cfd+TRIM(cLoadid)+csegd TO cString
						DO cstringbreak
						nSegCtr = nSegCtr + 1
					ENDIF
				ENDIF
				STORE "G62"+cfd+"10"+cfd+TRIM(DTOS(ddel_date))+cfd+"D"+cfd+cDelTime+csegd TO cString  && Ship date/time
				DO cstringbreak
				nSegCtr = nSegCtr + 1

				STORE "G62"+cfd+"17"+cfd+TRIM(DTOS(dexpdel_date))+cfd+"D"+cfd+cDelTime+csegd TO cString  && Ship date/time
				DO cstringbreak
				nSegCtr = nSegCtr + 1

				SELECT scacs
				IF lTesting AND (EMPTY(m.scac) OR EMPTY(m.ship_via))
					STORE "RDWY" TO m.scac
					STORE "ROADWAY" TO m.ship_via
				ELSE
					IF !EMPTY(TRIM(outship.scac))
						STORE ALLTRIM(outship.scac) TO m.scac
						STORE outship.ship_via TO m.ship_via
					ELSE
						WAIT CLEAR
						DO ediupdate WITH "MISSING SCAC",.T.
						THROW
					ENDIF
				ENDIF

				SELECT outship

				STORE "W27"+cfd+cCarrierType+cfd+TRIM(m.scac)+cfd+TRIM(m.ship_via)+csegd TO cString
				DO cstringbreak
				nSegCtr = nSegCtr + 1

*************************************************************************
*2	DETAIL LOOP - MATCHING SHIP_REF FIELD (FROM OUTSHIP)
**********4***************************************************************
*				ASSERT .F. MESSAGE "At start of Detail loop"
				WAIT CLEAR
				WAIT WINDOW "Now creating Detail information "+cShip_ref NOWAIT NOCLEAR

				SET DELETED ON
				IF lOverflow
					SELECT tempov
					LOCATE FOR tempov.ship_ref = cShip_ref
					IF FOUND()
						SET DELETED OFF
					ENDIF
				ENDIF

				FOR tx = 1 TO nRepLoops
					IF lRepeatPT
						lPrepack=IIF("C"$cShip_ref,.T.,.F.)
						IF tx = 2
							SELECT outship
							LOCATE
							LOCATE FOR outship.ship_ref = PADR(TRIM(cAltShip_ref),20)
							nAltOutshipid = outship.outshipid
							lPrepack = IIF("~C"$cAltShip_ref,.T.,.F.)
							lPick = IIF(lPrepack,.F.,.T.)
						ENDIF
					ENDIF

					SELECT vmoretedipp

					IF lRepeatPT
						LOCATE
						LOCATE FOR vmoretedipp.outshipid = outship.outshipid
					ENDIF

*			COPY TO f:\3pl\data\vmoretedipp  && Backup copy

					IF lPrepack
						SELECT ucc,outdetid,SUM(totqty) AS totqty ;
							FROM vmoretedipp ;
							WHERE totqty > 0 ;
							GROUP BY 1,2 ;
							INTO CURSOR vtemp
					ELSE
						SELECT ucc,outdetid,totqty ;
							FROM vmoretedipp ;
							INTO CURSOR vtemp
					ENDIF
					LOCATE

					SELECT outdet
					SET ORDER TO TAG outdetid
					SELECT vmoretedipp
*!*				SET RELATION TO outdetid INTO outdet

					scanstr = "vmoretedipp.outshipid = IIF(tx=1,nOutshipid,nAltOutshipid)"
					LOCATE
					LOCATE FOR &scanstr
					IF !FOUND()
						IF !lTesting
							SET STEP ON
							WAIT WINDOW "PT "+IIF(tx=1,cShip_ref,cAltShip_ref)+" NOT FOUND in vmoretedipp...ABORTING" TIMEOUT 2
							IF !lTesting
								lSQLMail = .T.
							ENDIF
							cErrMsg = "MISS PT-SQL: "+cShip_ref
							DO ediupdate WITH cErrMsg,.T.
							THROW
						ELSE
							LOOP
						ENDIF
					ENDIF

*		ASSERT .F. MESSAGE "At outdet line scan"
					SELECT vmoretedipp
					LOCATE
					LOCATE FOR &scanstr
					IF !FOUND()
						ASSERT .F. MESSAGE "At OSID loop seg error"
						SET STEP ON

						IF !lTesting
							WAIT CLEAR
							cOSID = ALLT(STR(outship.outshipid))
							WAIT WINDOW "OSID "+cOSID+" NOT FOUND in V"+ccustname+"...ABORTING" TIMEOUT 15
							IF !lTesting
								lSQLMail = .T.
							ENDIF
							cErrMsg = "MISS OSID-SQL: "+cOSID
							ediupdate(cErrMsg,.T.)
							THROW
						ELSE
							LOOP
						ENDIF
					ENDIF

					cUCCChk= "XXX"

					DO WHILE &scanstr
						cUCCLookup = ALLT(vmoretedipp.ucc)
						IF cUCCLookup = '00008878428201920955'
							SET STEP ON
						ENDIF
						nmorrec = RECNO()
						IF SEEK(cUCCLookup,'cutuccs','ucc')
							WAIT CLEAR
							WAIT WINDOW "Cutting UCC "+cUCCLookup TIMEOUT 1
							nCutUCCs = nCutUCCs + 1
							SKIP 1 IN vmoretedipp
							LOOP
						ENDIF

						IF "CUTS"$cUCCLookup
							ASSERT .F. MESSAGE "At CUTS message"
*					BROWSE TIMEOUT 10
							SKIP 1 IN vmoretedipp
							LOOP
						ENDIF

						IF TRIM(vmoretedipp.ucc) <> cUCCChk
							STORE TRIM(vmoretedipp.ucc) TO cUCCChk
						ENDIF

						STORE "LX"+cfd+ALLTRIM(STR(nCtnNumber))+csegd TO cString   && Carton Seq. # (up to Carton total)
						lDoManSegment = .T.
						lDoPALSegment = .F.
						DO cstringbreak
						nSegCtr = nSegCtr + 1

						SELECT * ;
							FROM vmoretedipp ;
							WHERE vmoretedipp.ucc = cUCCChk ;
							INTO CURSOR tempmoretctn READWRITE

						ctnrollup()

						SELECT vmoretedipp
						GO nmorrec
						nOutdetid = outdet.outdetid
						WAIT WIND nOutdetid NOWAIT NOCLEAR

						SELECT tempmoretctn
						SET RELATION TO outdetid INTO outdet
						LOCATE
						lMANRec = .T.
						SCAN FOR !DELETED() AND totqty > 0
							IF EMPTY(ALLTRIM(outdet.printstuff))  && Added to trap re-added detail lines, 10.05.2012
								SET STEP ON
								cErrMsg = "EMPTY PRINTSTUFF at OUTDETID "+TRANSFORM(outdet.outdetid)
								DO ediupdate WITH cErrMsg,.T.
								THROW
							ENDIF
							alength = ALINES(aptdet,outdet.printstuff,.T.,CHR(13))
							cUCCNumber = tempmoretctn.ucc
							IF EMPTY(cUCCNumber) OR ISNULL(cUCCNumber)
								WAIT CLEAR
								WAIT WINDOW "Empty UCC Number in vmoretedipp "+cShip_ref TIMEOUT 2
								lSQLMail = .T.
								cErrMsg = "EMPTY UCC# in "+cShip_ref
								DO ediupdate WITH cErrMsg,.T.
								THROW
							ENDIF
							cUCCNumber = TRIM(STRTRAN(TRIM(cUCCNumber)," ",""))

							IF lDoManSegment
								lDoManSegment = .F.
								STORE "MAN"+cfd+"GM"+cfd+TRIM(cUCCNumber)+csegd TO cString

								IF EMPTY(outdet.ctnwt) OR outdet.ctnwt = 0
									cCtnWt = ALLTRIM(STR(CEILING(outship.weight/IIF(lMasterpack,1,outship.ctnqty))))
									IF EMPTY(cCtnWt) OR INT(VAL(cCtnWt)) = 0
										IF lTesting
											cCtnWt = "5"
										ELSE
											cErrMsg = "WEIGHT ERR: PT "+cShip_ref
											DO ediupdate WITH cErrMsg,.T.
											THROW
										ENDIF
									ENDIF
									nTotCtnWt = nTotCtnWt + CEILING(VAL(cCtnWt))
								ELSE
									STORE ALLTRIM(STR(outdet.ctnwt)) TO cCtnWt
									nTotCtnWt = nTotCtnWt + outdet.ctnwt
								ENDIF

								nShipDetQty = tempmoretctn.totqty
								IF ISNULL(nShipDetQty) OR nShipDetQty = 0
									nShipDetQty = outdet.totqty
								ENDIF
								IF nShipDetQty = 0
									IF lMANRec
										lDoManSegment = .T.
									ENDIF
									LOOP
								ENDIF

								lMANRec = .F.
								nUnitSum = nUnitSum + nShipDetQty
								DO cstringbreak
								nSegCtr = nSegCtr + 1
							ELSE
								nShipDetQty = tempmoretctn.totqty
								IF ISNULL(nShipDetQty) OR nShipDetQty = 0
									nShipDetQty = outdet.totqty
								ENDIF
								IF nShipDetQty = 0
									LOOP
								ENDIF
								nUnitSum = nUnitSum + nShipDetQty
							ENDIF

							cColor = TRIM(outdet.COLOR)
							cSize = TRIM(outdet.ID)
							cUPC = TRIM(outdet.upc)

							IF (ISNULL(cUPC) OR EMPTY(cUPC))
								cUPC = TRIM(tempmoretctn.upc)
							ENDIF

							cStyle = TRIM(outdet.STYLE)
							cUOM = "EA"
							cStyleOut = ""
							cColorout = TRIM(segmentget(@aptdet,"COLOROUT",alength))
							cStyleOut = TRIM(segmentget(@aptdet,"ORIGSTYLE",alength))

*					ASSERT .F. MESSAGE "At color check"
							IF !EMPTY(cStyleOut) && If a converted style
								cStyle = STRTRAN(cStyleOut,'-','/')
							ELSE
								cStyle = cStyle+'/'+cColorout
							ENDIF

							cItemNum = TRIM(outdet.custsku)

							nOrigDetQty = tempmoretctn.qty
							IF ISNULL(nOrigDetQty)
								nOrigDetQty = nShipDetQty
							ENDIF

							nShipStat = "SH"  && "Shipped", per Avner
							STORE "W12"+cfd+nShipStat+cfd+cfd+ALLTRIM(STR(nShipDetQty))+cfd+cfd+;
								cUOM+cfd+cfd+"VA"+cfd+cStyle+csegd TO cString
							DO cstringbreak
							nSegCtr = nSegCtr + 1
							nCtnNumber = nCtnNumber + 1
						ENDSCAN
						SELECT vmoretedipp
						DO WHILE vmoretedipp.ucc = cUCCChk
							SKIP 1 IN vmoretedipp
						ENDDO
					ENDDO
				ENDFOR

*************************************************************************
*2^	END DETAIL MATCHING SHIP_REF LOOP
*************************************************************************
				WAIT CLEAR
				WAIT WINDOW "Now creating Section Closure information" NOWAIT NOCLEAR
				nCuFT = CEILING(nCuFT)
				STORE "W03"+cfd+ALLTRIM(STR(nUnitSum))+cfd+ALLTRIM(STR(nTotCtnWt))+cfd+;
					cWeightUnit+cfd+ALLTRIM(STR(nCuFT,10,0))+cfd+cCubeUnit+csegd TO cString   && Units sum, Weight sum, carton count

				nTotCtnWt = 0
				nTotCtnCount = 0
				nUnitSum = 0
				FPUTS(nFilenum,cString)
				STORE  "SE"+cfd+ALLTRIM(STR(nSegCtr+2))+cfd+PADL(c_GrpCntrlNum,9,"0")+csegd TO cString
				FPUTS(nFilenum,cString)

				SELECT outship
				WAIT CLEAR
				SET DELETED ON
				IF tx => 2 AND nRepLoops = 2
					GO nRec
				ENDIF
*				ASSERT .F. MESSA "At end of outship loop"
			ENDSCAN
		ENDSCAN

*************************************************************************
*1^	END OUTSHIP MAIN LOOP
*************************************************************************

		DO close945
		=FCLOSE(nFilenum)
		IF DATETIME()>DATETIME(2016,11,28,22,45,00)
			IF !lTesting
				SELECT edi_trigger
				LOCATE

				IF !USED("ftpedilog")
					SELECT 0
					USE F:\edirouting\ftpedilog ALIAS ftpedilog
					INSERT INTO ftpedilog (TRANSFER,ftpdate,filename,acct_name,TYPE) VALUES ("945-"+ccustname+"-"+cCustLoc,dt2,cFilenameShort,UPPER(ccustname),"945")
					USE IN ftpedilog
				ENDIF
			ENDIF
		ENDIF

		WAIT CLEAR
		WAIT WINDOW cCustFolder+" 945 Process complete for subBOL "+cBOL TIMEOUT 1

*!* Create eMail confirmation message

		RELEASE ALL LIKE c_CntrlNum,c_GrpCntrlNum
		WAIT CLEAR

		IF !EMPTY(cRolledRecs)
			cCustLocMail = cCustLoc
			tsubject = cMailName+" 945 "+IIF(lTesting,"TEST ","")+"AMT EDI File from Toll as of "+dtmail+" ("+cCustLocMail+")"
			tmessage = "The following PT(s) from BOL# "+cBOL+" had style rollups in the Cartons data"+CHR(13)+CHR(13)+cRolledRecs
			IF lEmail
				DO FORM m:\dev\frm\dartmail2 WITH tsendto,tfrom,tsubject,tcc,tattach,tmessage,"A"
			ENDIF
		ENDIF


*!* Transfers files to correct output folders
		IF !lTesting AND !lFakeDelivery
*SET STEP ON
			SELECT temp945
			COPY TO "f:\3pl\data\temp945mm.dbf"
			USE IN temp945
			USE "f:\3pl\data\temp945mm.dbf" IN 0 ALIAS temp945mm
			SELECT 0
			USE "f:\3pl\data\pts_sent945.dbf" ALIAS pts_sent945
			APPEND FROM "f:\3pl\data\temp945mm.dbf"
			USE IN pts_sent945
			USE IN temp945mm
			DELETE FILE "f:\3pl\data\temp945mm.dbf"
		ENDIF
		cPTCount = TRANSFORM(nPTCount)
		cTotSubs = TRANSFORM(nTotSubs)
		cSubCnt = TRANSFORM(nSubCnt)

		INSERT INTO tempinfo (cocode,subbol,ptcount,ptstring) VALUES (cCoCode,cBOL,cPTCount,cPTString)
		WAIT CLEAR
		WAIT WINDOW cMailName+" 945 EDI File output complete for"+CHR(13)+"Sub-BOL: "+cBOL NOWAIT
		cPTString = ""
		nPTCount = 0
*		ASSERT .F. MESSAGE "At end of BL scan loop"
		lDoISAHeader = .T.

	IF !lTesting
*		asn_out_data()
	endif

		cWO_NumList = ""

	ENDSCAN

	IF lDoFilesOut AND !lTesting AND !lTestinput AND !lFakeDelivery
		CD &cHoldFolder
		TRY
			COPY FILE (cHoldFolder+"*.*") TO (cOutFolder+"*.*")
			COPY FILE (cHoldFolder+"*.*") TO (cArchfolder+"*.*")
			DELETE FILE (cHoldFolder+"*.*")
		CATCH TO oERR
			IF oERR.ERRORNO =  1102  && Can't create file
				DELETE FILE [&cFilenameHold]
				tsendto = tsendtoerr
				tcc = tccerr
				tsubject = "945 File Copy Error at "+TTOC(DATETIME())
				tattach  = ""
				tfrom    ="Toll EDI 945 Poller Operations <toll-edi-ops@tollgroup.com>"
				tmessage = ALLTRIM(oERR.MESSAGE)
				DO FORM m:\dev\frm\dartmail2 WITH tsendto,tfrom,tsubject,tcc,tattach,tmessage,"A"
				THROW
			ENDIF
		ENDTRY
	ENDIF

	WAIT WINDOW "" TIMEOUT 2
*SET STEP ON
	cCustLocMail = cCustLoc
	tsubject = cMailName+" 945 "+IIF(lTesting,"TEST ","")+"EDI MBOL File from Toll as of "+dtmail+" ("+cCustLocMail+")"
	tattach = " "
	tmessage = "Filename: "+TRIM(cFilenameShort)+CHR(13)
	tmessage = tmessage+"Office: "+cCustLoc+CHR(13)
	tmessage = tmessage+"945 EDI Info from Toll, for Co. "+cCoCodeGrp+", Master BOL# "+ALLTRIM(cMBOL)
	tmessage = tmessage+" (Within our WO: "+cWO_NumStr+"), "+CHR(13)
	tmessage = tmessage + "for the following "+cTotSubs+" Sub-BOLs/PTs:"+CHR(13)+CHR(13)
	tmessage = tmessage + PADR("CO.",5)+PADR("SUB-BOL",22)+"PT COUNT"+CHR(13)
	SELECT tempinfo
	IF lTesting
		COPY TO c:\tempfox\tempinfo
	ENDIF
	SCAN
		tmessage = tmessage + PADR(ALLTRIM(tempinfo.cocode),5)+PADR(ALLTRIM(tempinfo.subbol),22)+tempinfo.ptcount+CHR(13)
		tmessage = tmessage + ALLTRIM(tempinfo.ptstring)+CHR(13)+CHR(13)
	ENDSCAN
	tmessage = tmessage +CHR(13)+"has been created and will be transmitted ASAP."+CHR(13)+CHR(13)
	IF !EMPTY(cMissDel) AND !lFakeDelivery
		tmessage = tmessage+CHR(13)+CHR(13)+cMissDel+CHR(13)+CHR(13)
	ENDIF

	IF lFakeDelivery
		tmessage = tmessage+CHR(13)+"*** THIS IS A 'TEST' 945 PER YOUR INSTRUCTIONS ***"
		tmessage = tmessage+CHR(13)+"*** File(s) will be sent by separate email ***"+CHR(13)+CHR(13)
	ENDIF

	tmessage = tmessage + "If you have any questions, please eMail or call 732-750-9000 x217."
	IF lEmail
		DO FORM m:\dev\frm\dartmail2 WITH tsendto,tfrom,tsubject,tcc,tattach,tmessage,"A"
	ENDIF

	SELECT zeroqty
	IF RECCOUNT()>0
		tsendto = tsendtozero
		tcc = tcczero
		tsubject = cMailName+" Zero Qty PTs in MBOL "+cMBOL
		tmessage = "The following picktickets on MBOL# "+cMBOL+", BOL# "+cBOL+" show zero quantity in the outbound table:"+CHR(13)
		tmessage = PADR("CONSIGNEE",42)+PADR("PT#",22)+"PO#"+CHR(13)
		SCAN
			tmessage = tmessage+CHR(13)+PADR(ALLT(zeroqty.consignee),42)+PADR(ALLT(zeroqty.ship_ref),22)+ALLT(zeroqty.cnee_ref)
		ENDSCAN
		IF lEmail
			DO FORM m:\dev\frm\dartmail2 WITH tsendto,tfrom,tsubject,tcc,tattach,tmessage,"A"
		ENDIF
	ENDIF

	IF USED('zeroqty')
		USE IN zeroqty
	ENDIF
	cString = ""

	SELECT edi_trigger
	DO ediupdate WITH "945 CREATED",.F.
	lDoCatch = .F.

CATCH TO oERR
	IF lDoCatch
		WAIT WINDOW "In catch...debug" TIMEOUT 2
		SET STEP ON
		cSpecialMsg=""
		DO CASE
			CASE BETWEEN(oERR.LINENO,1035,1046)
				cSpecialMsg="SHIPFOR CSZ error"
			CASE BETWEEN(oERR.LINENO,1006,1008)
				cSpecialMsg="SHIPTO CSZ error"
			CASE cRetMsg # "OK"
				cSpecialMsg = "SQL MSG: "+cRetMsg
		ENDCASE
		IF !EMPTY(cStatus) AND !EMPTY(cSpecialMsg)
			DO ediupdate WITH IIF(EMPTY(cSpecialMsg),UPPER(ALLTRIM(oERR.MESSAGE)),UPPER(cSpecialMsg)),.T.
			tsendto = tsendtoerr
			tcc = tccerr
			tsubject = ccustname+" Error ("+TRANSFORM(oERR.ERRORNO)+") at "+TTOC(DATETIME())
			tattach  = ""

			tmessage = ccustname+" Error processing "+CHR(13)

			tmessage =tmessage+ "Try/CATCH Exeception Message:"+CHR(13)+;
				[  Error: ] + STR(oERR.ERRORNO) +CHR(13)+;
				[  LineNo: ] + STR(oERR.LINENO) +CHR(13)+;
				[  Message: ] + oERR.MESSAGE +CHR(13)+;
				[  Procedure: ] + oERR.PROCEDURE +CHR(13)+;
				[  Details: ] + oERR.DETAILS +CHR(13)+;
				[  StackLevel: ] + STR(oERR.STACKLEVEL) +CHR(13)+;
				[  LineContents: ] + oERR.LINECONTENTS+CHR(13)+;
				[  UserValue: ] + oERR.USERVALUE

			tsubject = "945 EDI Poller Error at "+TTOC(DATETIME())
			tattach  = ""
			tfrom    ="Toll EDI 945 Poller Operations <toll-edi-ops@tollgroup.com>"
			DO FORM m:\dev\frm\dartmail2 WITH tsendto,tfrom,tsubject,tcc,tattach,tmessage,"A"
		ENDIF
	ENDIF
FINALLY
	WAIT WINDOW "Entire Moret MBOL process complete for"+CHR(13)+"MBOL: "+cMBOL TIMEOUT 2
	SET DELETED ON
	ON ERROR
	IF lTesting
		CLOSE DATABASES ALL
	ENDIF
ENDTRY

*** END OF CODE BODY

****************************
PROCEDURE close945
****************************
** Footer Creation

	STORE  "GE"+cfd+ALLTRIM(STR(nSTCount))+cfd+c_CntrlNum+csegd TO cString
	FPUTS(nFilenum,cString)
	nSTCount = 0

	STORE  "IEA"+cfd+"1"+cfd+PADL(c_CntrlNum,9,"0")+csegd TO cString
	FPUTS(nFilenum,cString)

ENDPROC

****************************
PROCEDURE num_incr_isa
****************************

	IF !USED("serfile")
		USE ("f:\3pl\data\serial\"+ccustname+"945_serial") IN 0 ALIAS serfile
	ENDIF
	SELECT serfile
	IF lTesting AND lISAFlag
		nOrigSeq = serfile.seqnum
		lISAFlag = .F.
	ENDIF
	nISA_Num = serfile.seqnum
	c_CntrlNum = ALLTRIM(STR(serfile.seqnum))
	REPLACE serfile.seqnum WITH serfile.seqnum + 1 IN serfile
	SELECT outship
ENDPROC

****************************
PROCEDURE num_incr_st
****************************

	IF !USED("serfile")
		USE ("f:\3pl\data\serial\"+ccustname+""945_serial") IN 0 ALIAS serfile
	ENDIF
	SELECT serfile
	IF lTesting AND lSTFlag
		nOrigGrpSeq = serfile.grpseqnum
		lSTFlag = .F.
	ENDIF
	c_GrpCntrlNum = ALLTRIM(STR(serfile.grpseqnum))
	REPLACE serfile.grpseqnum WITH serfile.grpseqnum + 1 IN serfile
	SELECT outship
ENDPROC


****************************
PROCEDURE segmentget
****************************

	PARAMETER thisarray,lcKey,nLength

	FOR i = 1 TO nLength
		IF i > nLength
			EXIT
		ENDIF
		lnEnd= AT("*",thisarray[i])
		IF lnEnd > 0
			lcThisKey =TRIM(SUBSTR(thisarray[i],1,lnEnd-1))
			IF OCCURS(lcKey,lcThisKey)>0
				RETURN SUBSTR(thisarray[i],lnEnd+1)
				i = 1
			ENDIF
		ENDIF
	ENDFOR

	RETURN ""

****************************
PROCEDURE ediupdate
****************************
	PARAMETER cStatus,lIsError
	lDoCatch = IIF(!EMPTY(cStatus),.T.,.F.)
*SET STEP ON
	ASSERT .F. MESSAGE "In EDIUpdate process"
	IF !lTesting
		SELECT edi_trigger
		DO WHILE ISFLOCKED('edi_trigger')
			LOOP
		ENDDO
		IF !lIsError
			REPLACE edi_trigger.processed WITH .T.,edi_trigger.proc945 WITH .T.,edi_trigger.file945 WITH UPPER(cFilenameArch),;
				edi_trigger.masterbill WITH .T.,edi_trigger.fin_status WITH "945 CREATED",edi_trigger.errorflag WITH .F.,;
				edi_trigger.when_proc WITH DATETIME() ;
				FOR edi_trigger.bol = cMBOL AND !edi_trigger.processed
		ELSE
			REPLACE edi_trigger.processed WITH .T.,edi_trigger.proc945 WITH .F.,edi_trigger.file945 WITH "",;
				edi_trigger.masterbill WITH .T.,edi_trigger.fin_status WITH cStatus,edi_trigger.errorflag WITH .T. ;
				FOR edi_trigger.bol = cMBOL AND !edi_trigger.processed
			IF lCloseOutput
				=FCLOSE(nFilenum)
			ENDIF
		ENDIF
	ENDIF

*!*	"This section runs the file945 renaming in edi_trigger (using current BOL#) in the triggers is the MBOL#"
	IF !lTesting
*SET STEP ON
		SELECT mfilesout945
		LOCATE
		IF !EOF() AND !lIsError
			WAIT WINDOW "Now updating actual 945 file names for MBOL "+cMBOL TIMEOUT 1
			ASSERT .F. MESSAGE "In File Name update for MBOL"
			SELECT edi_trigger
*!*			DO WHILE ISFLOCKED("edi_trigger")
*!*				LOOP
*!*			ENDDO
			LOCATE
			SCAN FOR edi_trigger.bol = cMBOL
				cxEDIShip_ref = ALLTRIM(edi_trigger.ship_ref)
				SELECT mfilesout945
				IF SEEK(cxEDIShip_ref,'mfilesout945','ship_ref')
					cxEDIFilename = ALLTRIM(mfilesout945.filename)
					cxEDIISA_Num = ALLTRIM(mfilesout945.isa_num)
				ENDIF
				REPLACE edi_trigger.file945 WITH cxEDIFilename,edi_trigger.isa_num WITH cxEDIISA_Num NEXT 1 IN edi_trigger
			ENDSCAN
			USE IN mfilesout945
			RELEASE ALL LIKE cxEDI*
		ENDIF
	ENDIF

	IF lIsError AND lEmail
		tsubject = "945 Error in MORET BOL "+TRIM(cBOL)+"(At PT "+cShip_ref+")"
		tattach = " "
		IF !lTesting AND !lTestinput
			tsendto = tsendtoerr
			tcc = tccerr
		ENDIF
		tmessage = "945 Processing for"+CHR(13)
		tmessage = tmessage+CHR(13)+"MBOL# "+cMBOL+", sub-BOL# "+cBOL+", WO# "+cWO_Num+" produced this error: (Office: "+cOffice+"): "+;
			ALLTRIM(cStatus)+CHR(13)+"Check EDI_TRIGGER and re-run"
		IF "TOTQTY ERR"$cStatus
			tmessage = tmessage + CHR(13) + "At OUTSHIPID: "+ALLTRIM(STR(m.outshipid))
		ENDIF

		DO FORM m:\dev\frm\dartmail2 WITH tsendto,tfrom,tsubject,tcc,tattach,tmessage,"A"
	ENDIF
	closedata()
ENDPROC

****************************
PROCEDURE cstringbreak
****************************
	cLen = LEN(ALLTRIM(cString))
	FPUTS(nFilenum,cString)
ENDPROC

****************************
PROCEDURE closedata
****************************
	IF USED('outship')
		USE IN outship
	ENDIF
	IF USED('outdet')
		USE IN outdet
	ENDIF
	IF USED('outwolog')
		USE IN outwolog
	ENDIF
	IF USED('vmoretedipp')
		USE IN vmoretedipp
	ENDIF
	IF USED('scacs')
		USE IN scacs
	ENDIF
	IF USED('mm')
		USE IN mm
	ENDIF
	IF USED('tempx')
		USE IN tempx
	ENDIF
	IF USED('pacas')
		USE IN pacas
	ENDIF
	IF USED('bl')
		USE IN bl
	ENDIF
	IF USED('bldet')
		USE IN bldet
	ENDIF
	IF !lTesting
		SELECT edi_trigger
		LOCATE
	ENDIF
	IF USED('serfile')
		SELECT serfile
		IF lTesting
			REPLACE serfile.seqnum WITH nOrigSeq
			REPLACE serfile.grpseqnum WITH nOrigGrpSeq
		ENDIF
		USE IN serfile
	ENDIF
ENDPROC

****************************
PROCEDURE renewdatetime
****************************
	IF INLIST(cOffice,'L','C','E','1','2','7','O')
		dDateTimeCal = (DATETIME()-(3*3600))
	ELSE
		dDateTimeCal = DATETIME()
	ENDIF
	dt1 = TTOC(dDateTimeCal,1)
	dtmail = TTOC(dDateTimeCal)
	dt2 = dDateTimeCal
	cDate = DTOS(DATE())
	cTruncDate = RIGHT(cDate,6)
	cTruncTime = SUBSTR(dt1,9,4)
ENDPROC

****************************
PROCEDURE ctnrollup
****************************
	SELECT tempmoretctn
	nreccnt = RECCOUNT()
	SCAN FOR RECNO()<nreccnt
		nRect = RECNO()
		cStyle = tempmoretctn.STYLE
		LOCATE FOR tempmoretctn.STYLE = cStyle AND RECNO() > nRect
		IF FOUND()
			IF EMPTY(cRolledRecs)
				cRolledRecs = PADR("PICKTICKET",25)+PADR("UCC#",25)+"STYLE"
				cRolledRecs = cRolledRecs+CHR(13)+(PADR(cShip_ref,25)+PADR(cUCCChk,25)+tempmoretctn.STYLE)
			ELSE
				cRolledRecs = cRolledRecs+CHR(13)+(PADR(cShip_ref,25)+PADR(cUCCChk,25)+tempmoretctn.STYLE)
			ENDIF

			nAddQty = tempmoretctn.totqty
			DELETE NEXT 1 IN tempmoretctn
			GO nRect
			REPLACE tempmoretctn.totqty WITH tempmoretctn.totqty + nAddQty
			REPLACE tempmoretctn.qty WITH tempmoretctn.qty + nAddQty
		ENDIF
		GO nRect
	ENDSCAN
ENDPROC