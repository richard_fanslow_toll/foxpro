lparameters xaccountid, xinwonum, xstyle, xcolor, xid, xoutwonum

xunits=0
xgohunits=0

if type("xgohrate")="U"
	xgohrate=0
endif

do case
case !empty(xinwonum)
	xsqlexec("select * from indet where wo_num="+transform(xinwonum)+" and units=.t.",,,"wh")
	
	scan for wo_num=xinwonum and units
		m.goh=.f.
		m.rate=rate.rate
		
		upcmastsql(accountid, style, color, id)
		
		if reccount()=0
			do case
			case len(trim(id))=4 and !inlist(id,"XXXS","XXXL")
				xupcmastunits=30
			case len(trim(id))=3 and !inlist(id,"XXS","XXL")
				xupcmastunits=6
			otherwise
				xupcmastunits=1
			endcase
		else
			if "STYLETYPE*GOH"$upcmast.info
				m.goh=.t.
				m.rate=xgohrate
				m.charge=indet.totqty*m.rate
				xupcmastunits=1
				xgohunits=xgohunits+indet.totqty
			else
				xupcmastunits=upcmast.unitqty
			endif
		endif
		
		if !m.goh
			m.charge=indet.totqty*xupcmastunits*rate.rate
			xunits=xunits+indet.totqty*xupcmastunits
		endif
			
		select indet
		scatter memvar field style, color, id, pack, totqty, accountid
		m.unitqty=xupcmastunits
		try
			insertinto("samsin","wh",.t.)
		catch
		endtry
	endscan
	
case !empty(xoutwonum)
	select voutship
	scan for wo_num=xoutwonum
		m.ship_ref=ship_ref

		select voutdet
		scan for outshipid=voutship.outshipid and units
			m.goh=.f.
			m.rate=rate.rate

			upcmastsql(accountid, style, color, id)

			if reccount()=0
				do case
				case len(trim(id))=4 and !inlist(id,"XXXS","XXXL")
					xupcmastunits=30
				case len(trim(id))=3 and !inlist(id,"XXS","XXL")
					xupcmastunits=6
				otherwise
					xupcmastunits=1
				endcase
			else
				if "STYLETYPE*GOH"$upcmast.info
					m.goh=.t.
					m.rate=xgohrate
					m.charge=voutdet.totqty*m.rate
					xupcmastunits=1
					xgohunits=xgohunits+voutdet.totqty
				else
					xupcmastunits=upcmast.unitqty
				endif
			endif

			if !m.goh
				m.charge=voutdet.totqty*xupcmastunits*rate.rate
				xunits=xunits+voutdet.totqty*xupcmastunits
			endif

			select voutdet
			scatter memvar field style, color, id, pack, totqty, accountid
			m.unitqty=xupcmastunits
			try
				insertinto("samsin","wh",.t.)
			catch
			endtry
		endscan
	endscan
		
otherwise
	upcmastsql(xaccountid, xstyle, xcolor, xid)
	if reccount()=0
		do case
		case len(trim(id))=4
			xupcmastunits=30
		case len(trim(id))=3
			xupcmastunits=6
		otherwise
			xupcmastunits=1
		endcase
	else
		if "STYLETYPE*GOH"$upcmast.info
			xupcmastunits=0
			xgoh=.t.
		else
			xupcmastunits=upcmast.unitqty
		endif
	endif
	xunits=xupcmastunits
endcase

return xunits
