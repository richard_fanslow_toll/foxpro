*!* m:\dev\prg\moret940_import.prg
EmailcommentStr = ""
cPTString = ""
cPTString = PADR("PICKTICKET",21)+PADR("CONSIGNEE",31)+PADR("START",11)+PADR("CANCEL",11)+"QTY"
m.ptqty = 0
m.UploadCount = 0
m.acct_num = nAcct_Num
lEmail = .T.

*ASSERT .f. message "At Import process...debug"
IF lTesting
	lTestImport = .T.
ENDIF

WAIT WINDOW "Now in IMPORT phase" nowait
IF lTestImport
	WAIT WINDOW "This is a TEST Import into F:\WHP\WHDATA" TIMEOUT 2
	cUseFolder = ("F:\WHP\WHDATA\")
ENDIF


USE (cUseFolder+"PT") IN 0 ALIAS pt
USE (cUseFolder+"PTDET") IN 0 ALIAS ptdet
*USE "f:\whl\whdata\WHGENPK" IN 0 ALIAS whgenpk

SELECT pt
SET ORDER TO ACCTPT   && STR(ACCOUNTID,4)+SHIP_REF

SELECT xpt
SET ORDER TO TAG ship_ref
LOCATE

STORE RECCOUNT() TO lnCount
WAIT WINDOW AT 10,10 "Picktickets to load ...........  "+STR(lnCount) NOWAIT

load_me = .T.
lcWarehouse = "C"
SCAN
	WAIT "AT XPT RECORD "+ALLTRIM(STR(RECNO())) WINDOW NOWAIT NOCLEAR
	IF SEEK(STR(m.acct_num,4)+xpt.ship_ref,"PT","ACCTPT")
		LOOP
	ENDIF
	SELECT pt
	SET ORDER TO
	IF load_me = .T.
		SELECT xpt
		SCATTER MEMVAR MEMO
		nPTID = m.ptid

*!*			SELECT whgenpk		dy 12/26/17
*!*			LOCATE FOR gpk_pk="PT"
*!*			REPLACE gpk_currentnumber WITH gpk_currentnumber + 1
*!*			m.ptid = gpk_currentnumber

		m.ptid = sqlgenpk("pt","wh")

		INSERT INTO pt FROM MEMVAR

		m.ptqty = m.ptqty+1
		m.UploadCount = m.UploadCount  +1
		SELECT xptdet
		SCAN FOR ptid=xpt.ptid
			SCATTER MEMVAR MEMO
			m.ptid=pt.ptid

*!*				SELECT whgenpk		dy 12/26/17
*!*				LOCATE FOR gpk_pk="PTDET"
*!*				REPLACE gpk_currentnumber WITH gpk_currentnumber + 1
*!*				m.ptdetid = gpk_currentnumber

			m.ptdetid = sqlgenpk("ptdet","wh")

			INSERT INTO ptdet FROM MEMVAR
		ENDSCAN
	ENDIF
ENDSCAN

SELECT xpt
GOTO TOP
pickticket_num_start = xpt.ship_ref
GOTO BOTT
pickticket_num_end = xpt.ship_ref
LOCATE
SCAN
	cPTString = cPTString+CHR(13)+PADR(ALLT(xpt.ship_ref),21)
	cPTString = cPTString+PADR(ALLT(xpt.consignee),31)
	cPTString = cPTString+PADR(DTOC(xpt.START),11)
	cPTString = cPTString+PADR(DTOC(xpt.CANCEL),11)
	cPTString = cPTString+ALLT(STR(xpt.qty))
ENDSCAN

IF m.UploadCount > 0
	WAIT WINDOW AT 10,10 "Picktickets uploaded...........  "+STR(m.UploadCount) TIMEOUT 1
ELSE
	WAIT WINDOW AT 10,10 "No New Picktickets uploaded...........  " TIMEOUT 2
ENDIF

WAIT "940 Import Round Complete" WINDOW TIMEOUT 2
WAIT CLEAR

USE IN pt
USE IN ptdet
*USE IN whgenpk

currfile = xfile

DO CASE
	CASE coffice = "C"
		cOfficeGroup = "CA"
	CASE coffice = "N"
		cOfficeGroup = "NJ"
	CASE coffice = "M"
		cOfficeGroup = "FL"
	OTHERWISE
		cOfficeGroup = "CA"
ENDCASE

cFilenameOut = "fmi"+TTOC(DATETIME(),1)+".trn"
WAIT "" TIMEOUT 3

EmailcommentStr = EmailcommentStr+CHR(13)+cPTString

*************************************************************************************************
*!* E-Mail process
*************************************************************************************************
IF lEmail
		tsendto = "joe.bianchi@tollgroup.com"
		tcc     =  ""
	cMailLoc = IIF(coffice = "C","West",IIF(coffice = "N","New Jersey","Florida"))
	tsubject= "TGF "+cMailLoc+" "+cCustname+" Pickticket Upload:" +TTOC(DATETIME())
	tattach = ""
	tmessage = "Picktickets from "+cCustname+" (CO: "+cCoCode+")"+CHR(13)+"From File: "+cfilename+CHR(13)+EmailcommentStr
	tFrom ="TGF Warehouse Operations <warehouse-ops@fmiint.com>"
	tmessage = tmessage+CHR(13)+"LOADED INTO TEST TABLES"
	DO FORM m:\dev\frm\dartmail2 WITH tsendto,tFrom,tsubject,tcc,tattach,tmessage,"A"
ENDIF
*************************************************************************************************

RETURN