*!* m:\dev\prg\dodenim940_main.prg

PARAMETERS cOfficeIn
CLOSE DATA ALL

PUBLIC nAcctNum,cDelimiter,nRun_num,cTranslateOption,cWhichFile,cOffice,cCustname,cPropername,cStorenum,nUploadCount
PUBLIC cFilter,nRunID,lComplete,lTesting,m.UCCNUMBER,lEmail,lTestImport,emailcommentstr,cUseFolder,nFileSize
PUBLIC ptid,ptctr,ptqty,xfile,cfilename,UploadCount,pickticket_num_start,pickticket_num_end,nFileCount,units
PUBLIC cOldScreen,m.addby,m.adddt,m.addproc,tsendto,tcc,archivefile,lcarchivepath,lLoadSQL,cMod,cISA_Num
PUBLIC NormalExit,cTransfer
PUBLIC tsendtoerr,tccerr
PUBLIC cPTString
PUBLIC ARRAY a856(1)

lTesting = .f.
lTestImport = lTesting
lOverrideBusy = lTesting
NormalExit = .F.
lBrowfiles = lTesting
*lBrowfiles = .T.
*lOverrideBusy = .t.
lEmail = .T.

cPTString = ""
nUploadCount = 0


DO M:\DEV\PRG\_SETVARS WITH lTesting

_SCREEN.WINDOWSTATE= IIF(lTesting OR lOverrideBusy,2,1)

IF TYPE("cOfficeIn") = "L"
	cOffice = "N"
ELSE
	STORE cOfficeIn TO cOffice
ENDIF

STORE "" TO cOldScreen
STORE _SCREEN.CAPTION TO cOldScreen
cCustname = "YELLSTEEL"
cUseName = "YELL STEEL"
cPropername = PROPER(cUseName)
nAcctNum  = 6209
cMod = IIF(cOffice = "C","1","I")
gMasterOffice = cOffice
gOffice = cMod
lLoadSQL = .T.

STORE cPropername+" 940 Process" TO _SCREEN.CAPTION
CLEAR
WAIT WINDOW "Now setting up "+cPropername+" 940 process..." TIMEOUT 2

DO M:\DEV\PRG\createx856a


cfile = ""

dXdate1 = ""
dXxdate2 = DATE()
nFileCount = 0

nRun_num = 999
lnRunID = nRun_num

TRY
	ASSERT .F. MESSAGE "AT START OF "+cUseName+" 940 PROCESS"

	IF !lTesting
		SELECT 0
		USE F:\edirouting\FTPSETUP SHARED
		cTransfer = "940-YELLSTEELEDI"
		LOCATE FOR FTPSETUP.TRANSFER = cTransfer
		IF FTPSETUP.CHKBUSY = .T. AND !lOverrideBusy
			WAIT WINDOW "IN PROCESS...RETURNING" TIMEOUT 2
			NormalExit = .T.
			THROW
		ENDIF
		REPLACE CHKBUSY WITH .T.,trig_time WITH DATETIME() FOR FTPSETUP.TRANSFER = cTransfer
		USE IN FTPSETUP
	ENDIF

	SELECT 0
	USE F:\3pl\DATA\mailmaster ALIAS mm SHARED
	LOCATE FOR mm.edi_type = "940" AND mm.accountid = nAcctNum AND mm.office = cOffice
	IF FOUND()
*		STORE TRIM(mm.acctname) TO cAccountName
		STORE TRIM(mm.basepath) TO lcPath
		STORE TRIM(mm.archpath) TO lcarchivepath
		STORE TRIM(IIF(mm.use_alt,mm.sendtoalt,mm.sendto)) TO tsendto
		STORE TRIM(IIF(mm.use_alt,mm.ccalt,mm.cc)) TO tcc
*		STORE mm.testflag 	   TO lTest
		LOCATE
		LOCATE FOR mm.accountid = 9999 AND mm.office = "X"
		STORE TRIM(IIF(mm.use_alt,mm.sendtoalt,mm.sendto)) TO tsendtoerr
		STORE TRIM(IIF(mm.use_alt,mm.ccalt,mm.cc)) TO tccerr
		LOCATE
		LOCATE FOR mm.edi_type = "MISC" AND mm.taskname = "GENERAL"
		STORE TRIM(IIF(mm.use_alt,mm.sendtoalt,mm.sendto)) TO tsendtotest
		STORE TRIM(IIF(mm.use_alt,mm.ccalt,mm.cc)) TO tcctest
		USE IN mm
	ELSE
		WAIT WINDOW AT 10,10  "No parameters set for this acct# "+ALLTRIM(STR(nAcctNum))+"  ---> Office "+cOffice TIMEOUT 2
		USE IN mm
		THROW
	ENDIF

	IF lTesting
		SET STEP ON 
		lcPath = STRTRAN(UPPER(lcPath),"940IN","940TEST")
		tsendto = tsendtotest
		tcc = tcctest
	ENDIF

	IF lTesting
		cUseFolder = "F:\WHP\WHDATA\"
	ELSE
		xReturn = "XXX"
		DO M:\DEV\PRG\wf_alt WITH IIF(cOffice="N","I",cOffice),nAcctNum
		cUseFolder = UPPER(xReturn)
	ENDIF
	WAIT WINDOW cUseFolder TIMEOUT 2
	cFolder = IIF(cOffice = "C","-CA","-NJ")

	DO ("m:\dev\PRG\"+cCustname+"940edi_PROCESS")
	NormalExit = .T.
	WAIT WINDOW AT 10,10 "Normal Exit " TIMEOUT 2
	IF USED('ftpsetup')
		USE IN FTPSETUP
	ENDIF
	SELECT 0
	USE F:\edirouting\FTPSETUP SHARED
	REPLACE CHKBUSY WITH .F. FOR FTPSETUP.TRANSFER = cTransfer

CATCH TO oErr
	IF NormalExit = .F.
		ASSERT .F. MESSAGE "In Error Catch...DEBUG"
		tsubject = cUseName+" 940 EDI Upload Error ("+TRANSFORM(oErr.ERRORNO)+") at "+TTOC(DATETIME())
		tattach  = ""
		tsendto  = tsendtoerr
		tcc = tccerr
		tmessage = cUseName+" 940 Upload Error..... Please fix me........!"
		lcSourceMachine = SYS(0)
		lcSourceProgram = SYS(16)

		tmessage =tmessage+ "Try/CATCH Exeception Message:"+CHR(13)+;
			[  Error: ] + STR(oErr.ERRORNO) +CHR(13)+;
			[  LineNo: ] + STR(oErr.LINENO) +CHR(13)+;
			[  Message: ] + oErr.MESSAGE +CHR(13)+;
			[  Procedure: ] + oErr.PROCEDURE +CHR(13)+;
			[  Details: ] + oErr.DETAILS +CHR(13)+;
			[  StackLevel: ] + STR(oErr.STACKLEVEL) +CHR(13)+;
			[  LineContents: ] + oErr.LINECONTENTS+CHR(13)+;
			[  UserValue: ] + oErr.USERVALUE+CHR(13)+;
			[  Computer:  ] +lcSourceMachine+CHR(13)+;
			[  940 file:  ] +xfile+CHR(13)+;
			[  Program:   ] +lcSourceProgram

		tattach  = ""
		tfrom    ="TGF EDI Processing Center <edi-ops@fmiint.com>"
		DO FORM M:\DEV\frm\dartmail2 WITH tsendto,tfrom,tsubject,tcc,tattach,tmessage,"A"
	ENDIF
FINALLY
	CLOSE DATABASES ALL
ENDTRY

