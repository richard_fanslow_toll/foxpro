* report OT by glcode for a date range for Ken.

LOCAL loADPOTBYENTITY
loADPOTBYENTITY = CREATEOBJECT('ADPOTBYENTITY')
loADPOTBYENTITY.MAIN()

CLOSE DATABASES ALL
RETURN

#DEFINE LOGIT 1
#DEFINE WAITIT 2
#DEFINE NOWAITIT 4
#DEFINE SENDIT 8
#DEFINE CRLF CHR(13) + CHR(10)
#DEFINE RETURN_DATA_MANDATORY .F.
#DEFINE RETURN_DATA_NOT_MANDATORY .T.
#DEFINE CSVFORMAT 6
#DEFINE WRITE_ONLY_UNBUFFERED 11
#DEFINE COMMA ","

DEFINE CLASS ADPOTBYENTITY AS CUSTOM

	cProcessName = 'ADPOTBYENTITY'

	lTestMode = .T.  && .F. FOR NORMAL OPERATION, .T. TO SUPPRESS ALL BUT DEVELOPER EMAILS AND CHANGE INPUT TABLE, ETC.

	dToday = DATE()

	lAutoYield = .T.

	cStartTime = TTOC(DATETIME())

	* report properties
	lShowSalaries = .T.

	* connection properties
	nSQLHandle = 0

	* wait window properties
	nWaitWindowTimeout = 2
	lWaitWindowIsOn = .T.

	* logfile properties
	lLoggingIsOn = .T.
	cLogFile = 'F:\UTIL\ADPREPORTS\LOGFILES\ADP_OT_BY_GLCodeReport_log.txt'

	* INTERNAL email properties
	lSendInternalEmailIsOn = .T.
	cFrom = 'TGF Corporate <fmicorporate@fmiint.com>'
	cSendTo = 'mbennett@fmiint.com'
	cCC = ''
	cSubject = 'Driver Info Report for ' + DTOC(DATE())
	cAttach = ''
	cBodyText = ''

	cCOMPUTERNAME = ''
	cUSERNAME = ''


	FUNCTION INIT
		IF NOT DODEFAULT()
			RETURN .F.
		ENDIF
		WITH THIS
			*SET RESOURCE OFF
			CLOSE DATA
			SET CENTURY ON
			SET DATE AMERICAN
			SET HOURS TO 24
			SET ANSI ON
			SET TALK OFF
			SET DELETED ON
			SET CONSOLE OFF
			SET EXCLUSIVE OFF
			SET SAFETY OFF
			SET EXACT OFF
			SET STATUS BAR ON
			SET SYSMENU OFF
			SET ENGINEBEHAVIOR 70
			_VFP.AUTOYIELD = .lAutoYield
			.cCOMPUTERNAME = GETENV("COMPUTERNAME")
			.cUSERNAME = GETENV("USERNAME")
			IF .lTestMode THEN
				.cLogFile = 'F:\UTIL\ADPREPORTS\LOGFILES\ADP_OT_BY_GLCodeReport_log_TESTMODE.txt'
				.cSendTo = 'mbennett@fmiint.com'
				.cCC = ''
			ENDIF
			.lLoggingIsOn = .lLoggingIsOn AND NOT EMPTY(.cLogFile)
			IF .lLoggingIsOn THEN
				SET ALTERNATE TO (.cLogFile) ADDITIVE
				SET ALTERNATE ON
			ENDIF
		ENDWITH
	ENDFUNC


	FUNCTION DESTROY
		WITH THIS
			IF .lLoggingIsOn  THEN
				SET ALTERNATE OFF
				SET ALTERNATE TO
			ENDIF
		ENDWITH
		DODEFAULT()
	ENDFUNC


	FUNCTION MAIN
		WITH THIS
			LOCAL lnNumberOfErrors, lcSQL, loError, lcFiletoSaveAs, ldStartDate, lcStartDate, ldEndDate, lcEndDate 
			
			TRY
				lnNumberOfErrors = 0

				.TrackProgress("Driver Info Report process started....", LOGIT+SENDIT+NOWAITIT)
				.TrackProgress('COMPUTERNAME = ' + .cCOMPUTERNAME, LOGIT+SENDIT)
				.TrackProgress('USERNAME = ' + .cUSERNAME, LOGIT+SENDIT)
				.TrackProgress('PROJECT = ADPOTBYENTITY', LOGIT+SENDIT)
				IF .lTestMode THEN
					.TrackProgress('                        TEST MODE                        ', LOGIT+SENDIT)
				ENDIF
				
		

				* enter report date range.....				
				ldStartDate = {^2013-07-01}
				ldEndDate =   {^2013-07-31}
				
				* construct sql filters
				lcStartDate = "DATE'" + TRANSFORM(YEAR(ldStartDate)) + ;
					"-" + PADL(MONTH(ldStartDate),2,"0") + "-" + PADL(DAY(ldStartDate),2,"0") + "'" 

				lcEndDate = "DATE'" + TRANSFORM(YEAR(ldEndDate)) + ;
					"-" + PADL(MONTH(ldEndDate),2,"0") + "-" + PADL(DAY(ldEndDate),2,"0") + "'" 
	
				.cSubject = "OT by GLCode Report for " + TRANSFORM(ldStartDate) + " thru " + TRANSFORM(ldEndDate)				

				* get ss#s from ADP
				OPEN DATABASE F:\UTIL\ADPCONNECTION\CONNECT1

				.nSQLHandle = SQLCONNECT("PCPAYWIN","MBENNETT","VP737A")

				IF .nSQLHandle > 0 THEN

			SET TEXTMERGE ON

			TEXT TO	lcSQL NOSHOW

SELECT
<<"'" + TRANSFORM(ldStartDate) + " thru " + TRANSFORM(ldEndDate) + "'">> AS DATERANGE,
{fn LEFT(A.CHECKVIEWHOMEDEPT,2)} AS DIVISION,
{fn SUBSTRING(A.CHECKVIEWHOMEDEPT,3,4)} AS DEPT,
SUM({FN IFNULL(A.CHECKVIEWOTEARNING,0.00)}) AS OTPAY
FROM REPORTS.V_CHK_VW_INFO A
WHERE A.CHECKVIEWPAYDATE >= <<lcStartDate>> AND A.CHECKVIEWPAYDATE <= <<lcEndDate>>
GROUP BY {fn LEFT(A.CHECKVIEWHOMEDEPT,2)}, {fn SUBSTRING(A.CHECKVIEWHOMEDEPT,3,4)}
ORDER BY {fn SUBSTRING(A.CHECKVIEWHOMEDEPT,3,4)}

			ENDTEXT
			
			SET TEXTMERGE OFF


					IF .lTestMode THEN
						.TrackProgress('lcSQL =' + lcSQL, LOGIT+SENDIT)
					ENDIF

					IF USED('SQLCURSOR1') THEN
						USE IN SQLCURSOR1
					ENDIF
					IF USED('SQLCURSOR2') THEN
						USE IN SQLCURSOR2
					ENDIF
					IF USED('SQLCURSOR3') THEN
						USE IN SQLCURSOR3
					ENDIF
					
					USE F:\UTIL\ADPREPORTS\DATA\DEPTTRANS.DBF AGAIN IN 0 ALIAS DEPTRANS

					IF .ExecSQL(lcSQL, 'SQLCURSOR1', RETURN_DATA_MANDATORY) THEN


						lcFiletoSaveAs = "F:\UTIL\ADPREPORTS\REPORTS\OT_BY_GLCode_" + STRTRAN(DTOC(ldStartDate),"/","-") + "-" + STRTRAN(DTOC(ldEndDate),"/","-") + ".XLS"

						SELECT A.*, (B.GLACCTNO + "-0000-" + A.DIVISION) AS GLCODE ;
							FROM SQLCURSOR1 A ;
							LEFT OUTER JOIN DEPTRANS B ;
							ON B.DEPT = A.DEPT ;
							INTO CURSOR SQLCURSOR2 ;
							ORDER BY A.DEPT ;
							READWRITE
							
							
						SELECT DATERANGE, DEPT, GLCODE, SUM(OTPAY) AS OTPAY ;
							FROM SQLCURSOR2 ;
							INTO CURSOR SQLCURSOR3 ;
							GROUP BY 1, 2, 3 ;
							ORDER BY 1, 2, 3 ;
							READWRITE							
							
						
*!*				SELECT SQLCURSOR3 
*!*			BROW
					
						
						
						
						SELECT SQLCURSOR3 
						COPY TO (lcFiletoSaveAs) XL5						


						IF FILE(lcFiletoSaveAs) THEN
							* attach output file to email
							.cAttach = lcFiletoSaveAs
						ELSE
							.TrackProgress('ERROR: unable to email spreadsheet: ' + lcFiletoSaveAs, LOGIT+SENDIT+NOWAITIT)
						ENDIF
						
						CLOSE DATABASES ALL
					ENDIF  &&  .ExecSQL(lcSQL, 'SQLCURSOR1', RETURN_DATA_MANDATORY)

					.TrackProgress("Driver Info Report process ended normally.", LOGIT+SENDIT+NOWAITIT)
				ELSE
					* connection error
					.TrackProgress('Unable to connect to ADP Payroll System.', LOGIT+SENDIT)
				ENDIF   &&  .nSQLHandle > 0

			CATCH TO loError

				.TrackProgress('There was an error.',LOGIT+SENDIT)
				.TrackProgress('The Error # is: ' + TRANSFORM(loError.ERRORNO), LOGIT+SENDIT)
				.TrackProgress('The Error Message is: ' + TRANSFORM(loError.MESSAGE), LOGIT+SENDIT)
				.TrackProgress('The Error occurred in Program: ' + TRANSFORM(loError.PROCEDURE), LOGIT+SENDIT)
				.TrackProgress('The Error occurred at line #: ' + TRANSFORM(loError.LINENO), LOGIT+SENDIT)
				lnNumberOfErrors = lnNumberOfErrors + 1
				IF TYPE('oWorkbook') = "O" THEN
					oWorkbook.SAVE()
				ENDIF
				IF TYPE('oExcel') = "O" THEN
					oExcel.QUIT()
				ENDIF
				CLOSE DATABASES ALL

			ENDTRY

			CLOSE DATA
			WAIT CLEAR
			***************** INTERNAL email results ******************************
			.TrackProgress('About to send status email.',LOGIT)
			.TrackProgress('The logfile is: ' + .cLogFile, SENDIT)
			.TrackProgress("Driver Info Report process started: " + .cStartTime, LOGIT+SENDIT)
			.TrackProgress("Driver Info Report process finished: " + TTOC(DATETIME()), LOGIT+SENDIT)

			IF .lSendInternalEmailIsOn THEN
				DO FORM dartmail2 WITH .cSendTo,.cFrom,.cSubject,.cCC,.cAttach,.cBodyText,"A"
				.TrackProgress('Sent status email.',LOGIT)
			ELSE
				.TrackProgress('Did not send status email: THIS.lSendInternalEmailIsOn = FALSE.',LOGIT)
			ENDIF

		ENDWITH
		RETURN
	ENDFUNC && main


	FUNCTION ExecSQL
		LPARAMETERS tcSQL, tcCursorName, tlNoDataReturnedIsOkay
		LOCAL llRetval, lnResult
		WITH THIS
			* close target cursor if it's open
			IF USED(tcCursorName)
				USE IN (tcCursorName)
			ENDIF
			WAIT WINDOW "Fetching remote data; please wait..." NOWAIT
			lnResult = SQLEXEC(.nSQLHandle, tcSQL, tcCursorName)
			llRetval = ( lnResult > 0 )
			IF llRetval THEN
				* see if any data came back
				IF NOT tlNoDataReturnedIsOkay THEN
					IF (NOT USED(tcCursorName)) OR EOF(tcCursorName) THEN
						llRetval = .F.
						.TrackProgress('No data was returned by this query: ' +CRLF + tcSQL, LOGIT+SENDIT)
						.cSendTo = 'mbennett@fmiint.com'
					ENDIF
				ENDIF
			ELSE
				.TrackProgress("Result Code [" + TRANSFORM(lnResult) + "] returned by this query: " + tcSQL, LOGIT+SENDIT)
				.cSendTo = 'mbennett@fmiint.com'
			ENDIF
			WAIT CLEAR
			RETURN llRetval
		ENDWITH
	ENDFUNC


	PROCEDURE TrackProgress
		* do any combination of Wait Window, writing to logfile, and adding to body of email,
		* based on nFlags parameter.
		LPARAMETERS tcExpression, tnFlags
		WITH THIS
			IF BITAND(tnFlags,LOGIT) = LOGIT THEN
				IF .lLoggingIsOn THEN
					?
					? .cProcessName + "  " + TTOC(DATETIME()) + ": " + tcExpression
				ENDIF
			ENDIF
			IF .lWaitWindowIsOn THEN
				IF BITAND(tnFlags,NOWAITIT) = NOWAITIT THEN
					WAIT WINDOW tcExpression NOWAIT
				ENDIF
				IF BITAND(tnFlags,WAITIT) = WAITIT THEN
					WAIT WINDOW tcExpression TIMEOUT .nWaitWindowTimeout
				ENDIF
			ENDIF
			IF BITAND(tnFlags,SENDIT) = SENDIT THEN
				IF .lSendInternalEmailIsOn THEN
					.cBodyText = .cBodyText + tcExpression + CRLF + CRLF
				ENDIF
			ENDIF
		ENDWITH
	ENDPROC  &&  TrackProgress

ENDDEFINE

