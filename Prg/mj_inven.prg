parameter whichaccount
set exclusive off
set deleted on
set talk off
set safety off
*whichaccount = 6303

whichaccount = val(alltrim(transform(whichaccount)))

close data all

xsqlexec("select * from inven where mod='J'",,,"wh")

**to run report from snapshot files, comment out the USE statements above and uncomment out below - mvw 03/19/13
*!*	Use f:\whj\whdata\snapshot\soutship alias outship In 0
*!*	Use f:\whj\whdata\snapshot\soutdet alias outdet In 0
*!*	Use f:\whj\whdata\project In 0
*!*	Use f:\whj\whdata\snapshot\sinven alias inven In 0



**to run report from snapshot files, comment out the USE statements above and uncomment out below - mvw 03/19/13
*!*	Use M:\BAK\whbak\whdata-j\outship alias outship In 0
*!*	Use M:\BAK\whbak\whdata-j\outdet alias outdet In 0
*!*	Use M:\BAK\whbak\whdata-j\project In 0
*!*	Use M:\BAK\whbak\whdata-j\inven alias inven In 0


upcmastsql(6303)
index on style tag style
index on color tag color
index on id tag id

set deleted on

**************************************** begin ALL accounts TM 01/16/2014
if whichaccount = 9999
*********CA

	xsqlexec("select * from inven where mod='L'","invenc",,"wh")

	select units, style, color, id, pack, availqty, allocqty, holdqty, 0000000 as pickedqty, 0000000 as spqty,000000 overallqty, space(13) as upc,accountid,"     " as division , space(13) as upc2, 'CA' as whse ;
	from invenc where inlist(accountid,6303,6325,6304,6305,6320,6321,6322,6323,6324,6543,6364) and units into cursor inventoryc readwrite
*********NJ
	select units, style, color, id, pack, availqty, allocqty, holdqty, 0000000 as pickedqty, 0000000 as spqty,000000 overallqty, space(13) as upc,accountid,"     " as division , space(13) as upc2 , 'NJ' as whse ;
	from inven where inlist(accountid,6303,6325,6304,6305,6320,6321,6322,6323,6324,6543,6364) and units into cursor inventoryn readwrite

	select * from inventoryc union all select * from inventoryn into cursor inventory readwrite

	index on str(accountid,4)+transform(units)+style+color+id+pack+whse tag match
*SET STEP ON


* inprocess is a cursor of all outship records where they are not yet delivered and also excluding "notonwip"
*********NJ
	xsqlexec("select *, cast('NJ' as char(2)) as whse from outship where mod='J' " + ;
	"and inlist(accountid,6303,6325,6304,6305,6320,6321,6322,6323,6324,6543,6364) and del_date={} and notonwip=0 and qty#0 and pulled=1 " + ;
	"order by wo_num","inprocess",,"wh")

*********CA
	xsqlexec("select *, cast('CA' as char(2)) as whse from outship where mod='L' " + ;
	"and inlist(accountid,6303,6325,6304,6305,6320,6321,6322,6323,6324,6543,6364) and del_date={} and notonwip=0 and qty#0 and pulled=1 " + ;
	"order by wo_num","inprocessc",,"wh")

	select inprocessc
	scan
		scatter memvar memo
		insert into inprocess from memvar
	endscan

*********NJ
	xsqlexec("select outship.*, cast('NJ' as char(2)) as whse, units, style, color, id, pack, totqty from outship, outdet where outship.mod='J' " + ;
		"and outship.outshipid=outdet.outshipid and inlist(outship.accountid,6303,6325,6304,6305,6320,6321,6322,6323,6324,6543,6364) " + ;
		"and del_date={} and notonwip=0 and qty#0 and pulled=1","xdy",,"wh")

*		select *, sum(totqty) as pickedqty from xdy group by units, accountid, style, color, id, pack into cursor xunshippedn

	select units, outdet.accountid as accountid, style, color, id, pack, sum(totqty) as pickedqty, 'NJ' as whse ;
	from outdet, inprocessn where outdet.outshipid= inprocessn.outshipid ;
	group by units, outdet.accountid, style, color, id, pack ;
	into cursor xunshippedn

*********CA
	xsqlexec("select outship.*, cast('CA' as char(2)) as whse, units, style, color, id, pack, totqty from outship, outdet where outship.mod='L' " + ;
	"and outship.outshipid=outdet.outshipid and inlist(outship.accountid,6303,6325,6304,6305,6320,6321,6322,6323,6324,6543,6364) " + ;
	"and del_date={} and notonwip=0 and qty#0 and pulled=1","xdy",,"wh")

	select *, sum(totqty) as pickedqty from xdy group by units, accountid, style, color, id, pack into cursor xunshippedc

	select * from xunshippedn union all select * from xunshippedc into cursor xunshipped readwrite

	select xunshipped
	scan
		if seek(str(accountid,4)+transform(units)+style+color+id+pack+whse,"inventory","match")
			replace pickedqty with xunshipped.pickedqty in inventory
		else
			scatter memvar
			insert into inventory from memvar
		endif
	endscan
*set step on

* uncompleted special projects
*********NJ
	xsqlexec("select outdet.wo_num, outdet.accountid, units, style, color, id, pack, totqty " + ;
	"from outdet, project where outdet.mod='J' " + ;
	"and outdet.wo_num=project.wo_num and completeddt={} " + ;
	"and inlist(project.accountid,6303,6325,6304,6305,6320,6321,6322,6323,6324,6543,6364)","xdy",,"wh")

	select wo_num, accountid, units, style, color, id, pack, sum(totqty) as spqty, 'NJ' as whse ;
	from xdy group by 1,2,3,4,5,6,7 into cursor xspn

*********CA
	xsqlexec("select outdet.wo_num, outdet.accountid, units, style, color, id, pack, totqty " + ;
	"from outdet, project where outdet.mod='L' " + ;
	"and outdet.wo_num=project.wo_num and completeddt={} " + ;
	"and inlist(project.accountid,6303,6325,6304,6305,6320,6321,6322,6323,6324,6543,6364)","xdy",,"wh")

	select wo_num, accountid, units, style, color, id, pack, sum(totqty) as spqty, 'CA' as whse ;
	from xdy group by 1,2,3,4,5,6,7 into cursor xspc

	select * from xspn union all select * from xspc into cursor xsp readwrite

	select xsp
	scan
		if seek(str(accountid,4)+transform(units)+style+color+id+pack,"inventory","match")
			replace spqty with xsp.spqty in inventory
		else

		endif
	endscan



* copy and email inventory file

	select inventory
	delete for allocqty =0 and availqty=0 and pickedqty=0 and holdqty=0 and spqty=0

	select inventory
	scan &&for !Deleted()
		select upcmast
		do case
		case inlist(inventory.accountid,6364,6324,6320,6364,6304)  && the SPE account, same master as Retail
			locate for upcmast.style = inventory.style and inventory.color = upcmast.color and inventory.id = upcmast.id and accountid = 6304  && use 6304 for the SPE Account
		case  inlist(inventory.accountid,6325,6303,6305,2503,6543)
			locate for upcmast.style = inventory.style and inventory.color = upcmast.color and inventory.id = upcmast.id and accountid = 6303
		otherwise
			locate for upcmast.style = inventory.style and inventory.color = upcmast.color and inventory.id = upcmast.id and accountid = inventory.accountid
		endcase



		if found()
			replace inventory.upc with upcmast.upc in inventory
			if empty(upcmast.upc)
				alength = alines(aupc,upcmast.info,.t.,chr(13))
				cupc    = alltrim(segmentget(@aupc,"UPC",alength))
				replace inventory.upc2 with cupc in inventory
			endif
		else
			replace inventory.upc with "UNK"
		endif

************************begin division 12/3/2012 TM
		if empty( inventory.division)
			alength = alines(aupc,upcmast.info,.t.,chr(13))
			lcdiv   = alltrim(segmentget(@aupc,"DIV",alength))
			replace division with lcdiv in inventory
		else
			replace inventory.division with "UNK"
		endif
************************end division 12/3/2012 TM
	endscan

	select inventory
	scan
		replace pickedqty with pickedqty+allocqty && added 12/18/12 by TM to account for alloc from inven
		replace overallqty with availqty+holdqty+pickedqty+spqty
	endscan


*******************Added 7/9/15  TM  capture weekly transactions  START

*SET STEP ON
****************************begin capture I/B receipts  from previous Monday 12:01AM -  previous Saturday 11:59PM
	goffice='J'
	useca("inwolog","wh",,,"mod='"+goffice+"' and  accountid in (6303,6325,6543) and confirmdt>{"+dtoc(date()-90)+"}")
	useca("indet","wh",,,"mod='"+goffice+"' and  accountid in (6303,6325,6543) and date_rcvd>{"+dtoc(date()-90)+"}")

	xdate=date()-6
	xdate1=ctot(dtoc(xdate)+" 12:01:00 AM")
	xdate2=date()-1
	xdate3=ctot(dtoc(xdate2)+" 11:59:59 PM")
	select a.accountid, style,color,id,'NJ'as whse ,sum(totqty) as rcvqty from inwolog a, indet b where a.inwologid=b.inwologid and confirmdt >=xdate1  and confirmdt <=xdate3 and !zreturns group by 1,2,3,4 into cursor vrcvn
	use in indet
	use in inwolog

	goffice='L'
	useca("inwolog","wh",,,"mod='"+goffice+"' and  accountid in (6303,6325,6543) and confirmdt>{"+dtoc(date()-90)+"}")
	useca("indet","wh",,,"mod='"+goffice+"' and  accountid in (6303,6325,6543) and date_rcvd>{"+dtoc(date()-90)+"}")

	xdate=date()-6
	xdate1=ctot(dtoc(xdate)+" 12:01:00 AM")
	xdate2=date()-1
	xdate3=ctot(dtoc(xdate2)+" 11:59:59 PM")
	select a.accountid, style,color,id,'CA'as whse ,sum(totqty) as rcvqty from inwolog a, indet b where a.inwologid=b.inwologid and confirmdt >=xdate1 and confirmdt <=xdate3 and !zreturns and inlist(a.accountid,6303,6325) group by 1,2,3,4 into cursor vrcvc
	select * from vrcvn union all select * from vrcvc into cursor vrcv readwrite
	use in indet
	use in inwolog

	select * from inventory a full join vrcv b on a.accountid=b.accountid and a.style=b.style and a.color=b.color and a.id=b.id and a.whse=b.whse into cursor c1 readwrite

	replace style_a with style_b for isnull(style_a)
	replace color_a with color_b for isnull(color_a)
	replace id_a with id_b for isnull(id_a)
	replace pack with '1' for isnull(pack)
	replace availqty with 0 for isnull(availqty)
	replace allocqty with 0 for isnull(allocqty)
	replace holdqty with 0 for isnull(holdqty)
	replace pickedqty with 0 for isnull(pickedqty)
	replace spqty with 0 for isnull(spqty)
	replace overallqty with 0 for isnull(overallqty)
	replace accountid_a with accountid_b for isnull(accountid_a)
	replace whse_a with whse_b for isnull(whse_a)

	select units,style_a as style, color_a as color, id_a as id, pack,availqty,allocqty, holdqty,pickedqty,spqty,overallqty,upc,accountid_a as accountid,division,upc2,whse_a as whse,rcvqty from c1 into cursor c2 readwrite
	replace rcvqty with 0 for isnull(rcvqty)
****************************end capture I/B receipts
****************************begin capture shipments

	xsqlexec("select outship.accountid, style, color, id, cast('NJ' as char(2)) as whse, totqty from outship, outdet " + ;
	"where outship.outshipid=outdet.outshipid and bol_no#'NOTHING' and keyrec#'CANCEL'and cancelrts=0 and del_date>={"+dtoc(date()-7)+"}","xdy",,"wh")

	select *, sum(totqty) as shpdqty from xdy group by 1,2,3,4 into cursor vshpdn

	xsqlexec("select outship.accountid, style, color, id, cast('CA' as char(2)) as whse, totqty from outship, outdet " + ;
	"where outship.outshipid=outdet.outshipid and bol_no#'NOTHING' and keyrec#'CANCEL'and cancelrts=0 and del_date>={"+dtoc(date()-7)+"}","xdy",,"wh")

	select *, sum(totqty) as shpdqty from xdy group by 1,2,3,4 into cursor vshpdc

	select * from vshpdn union all select * from vshpdc into cursor vshpd readwrite

	select * from c2 a full join vshpd b on a.accountid=b.accountid and a.style=b.style and a.color=b.color and a.id=b.id and a.whse=b.whse into cursor c3 readwrite

	replace style_a with style_b for isnull(style_a)
	replace color_a with color_b for isnull(color_a)
	replace id_a with id_b for isnull(id_a)
	replace pack with '1' for isnull(pack)
	replace availqty with 0 for isnull(availqty)
	replace allocqty with 0 for isnull(allocqty)
	replace holdqty with 0 for isnull(holdqty)
	replace pickedqty with 0 for isnull(pickedqty)
	replace spqty with 0 for isnull(spqty)
	replace overallqty with 0 for isnull(overallqty)
	replace accountid_a with accountid_b for isnull(accountid_a)
	replace whse_a with whse_b for isnull(whse_a)

	select units,style_a as style, color_a as color, id_a as id, pack,availqty,allocqty, holdqty,pickedqty,spqty,overallqty,upc,accountid_a as accountid,division,upc2,whse_a as whse,rcvqty,shpdqty from c3 into cursor c4 readwrite
	replace rcvqty with 0 for isnull(rcvqty)
	replace shpdqty with 0 for isnull(shpdqty)

****************************end capture shipments
****************************begin capture adjustments

	goffice="J"
	xsqlexec("select * from adj where mod='J' and adjdt>={"+dtoc(date()-7)+"} and offset=0",,,"wh")
	select accountid,style,color,id,'NJ' as whse,sum(totqty) as adjqty from adj where comment !='PHYSICAL ADJUSTMENT'  and comment !='WAREHOUSE LOCATION'  and comment !='RETURN PUTAWAY' and adjtype !='LOCATION CHANGE' group by 1,2,3,4 into cursor vadjn readwrite

	goffice="L"
	xsqlexec("select * from adj where mod='L' and adjdt>={"+dtoc(date()-7)+"} and offset=0",,,"wh")
	select accountid,style,color,id,'CA' as whse,sum(totqty) as adjqty from adj where comment !='PHYSICAL ADJUSTMENT'  and comment !='WAREHOUSE LOCATION'  and comment !='RETURN PUTAWAY' and adjtype !='LOCATION CHANGE' group by 1,2,3,4 into cursor vadjc readwrite

	select * from vadjn union all select * from vadjc into cursor vadj readwrite

	select * from c4 a full join vadj b on a.accountid=b.accountid and a.style=b.style and a.color=b.color and a.id=b.id and a.whse=b.whse into cursor c5 readwrite

	replace style_a with style_b for isnull(style_a)
	replace color_a with color_b for isnull(color_a)
	replace id_a with id_b for isnull(id_a)
	replace pack with '1' for isnull(pack)
	replace availqty with 0 for isnull(availqty)
	replace allocqty with 0 for isnull(allocqty)
	replace holdqty with 0 for isnull(holdqty)
	replace pickedqty with 0 for isnull(pickedqty)
	replace spqty with 0 for isnull(spqty)
	replace overallqty with 0 for isnull(overallqty)
	replace accountid_a with accountid_b for isnull(accountid_a)
	replace whse_a with whse_b for isnull(whse_a)

	select units,style_a as style, color_a as color, id_a as id, pack,availqty,allocqty, holdqty,pickedqty,spqty,overallqty,upc,accountid_a as accountid,division,upc2,whse_a as whse,rcvqty,shpdqty,adjqty from c5 into cursor c6 readwrite
	replace rcvqty with 0 for isnull(rcvqty)
	replace shpdqty with 0 for isnull(shpdqty)
	replace adjqty with 0 for isnull(adjqty)

****************************end capture adjustments
****************************begin capture returns
	goffice='J'
	useca("inwolog","wh",,,"mod='"+goffice+"' and  accountid in (6303,6325,6543) and confirmdt>{"+dtoc(date()-90)+"}")
	useca("indet","wh",,,"mod='"+goffice+"' and  accountid in (6303,6325,6543) and date_rcvd>{"+dtoc(date()-90)+"}")

	xdate=date()-6
	xdate1=ctot(dtoc(xdate)+" 12:01:00 AM")
	xdate2=date()-1
	xdate3=ctot(dtoc(xdate2)+" 11:59:59 PM")
	select a.accountid, style,color,id,'NJ'as whse ,sum(totqty) as rtnqty from inwolog a, indet b where a.inwologid=b.inwologid  and confirmdt >=xdate1  and confirmdt <=xdate3 and zreturns group by 1,2,3,4 into cursor vrtnn
	use in indet
	use in inwolog
	goffice='L'
	useca("inwolog","wh",,,"mod='"+goffice+"' and  accountid in (6303,6325,6543) and confirmdt>{"+dtoc(date()-90)+"}")
	useca("indet","wh",,,"mod='"+goffice+"' and  accountid in (6303,6325,6543) and date_rcvd>{"+dtoc(date()-90)+"}")

	xdate=date()-6
	xdate1=ctot(dtoc(xdate)+" 12:01:00 AM")
	xdate2=date()-1
	xdate3=ctot(dtoc(xdate2)+" 11:59:59 PM")
	select a.accountid, style,color,id,'CA'as whse ,sum(totqty) as rtnqty from inwolog a, indet b where a.inwologid=b.inwologid  and confirmdt >=xdate1  and confirmdt <=xdate3 and zreturns and inlist(a.accountid,6303,6325) group by 1,2,3,4 into cursor vrtnc
	select * from vrtnn union all select * from vrtnc into cursor vrtn readwrite
	use in indet
	use in inwolog

	select * from c6 a full join vrtn b on a.accountid=b.accountid and a.style=b.style and a.color=b.color and a.id=b.id and a.whse=b.whse into cursor c7 readwrite

	replace style_a with style_b for isnull(style_a)
	replace color_a with color_b for isnull(color_a)
	replace id_a with id_b for isnull(id_a)
	replace pack with '1' for isnull(pack)
	replace availqty with 0 for isnull(availqty)
	replace allocqty with 0 for isnull(allocqty)
	replace holdqty with 0 for isnull(holdqty)
	replace pickedqty with 0 for isnull(pickedqty)
	replace spqty with 0 for isnull(spqty)
	replace overallqty with 0 for isnull(overallqty)
	replace accountid_a with accountid_b for isnull(accountid_a)
	replace whse_a with whse_b for isnull(whse_a)

	select units,style_a as style, color_a as color, id_a as id, pack,availqty,allocqty, holdqty,pickedqty,spqty,overallqty,upc,accountid_a as accountid,division,upc2,whse_a as whse,rcvqty,shpdqty,adjqty ,rtnqty from c7 into cursor c8 readwrite
	replace rcvqty with 0 for isnull(rcvqty)
	replace shpdqty with 0 for isnull(shpdqty)
	replace adjqty with 0 for isnull(adjqty)
	replace rtnqty with 0 for isnull(rtnqty)
****************************end capture returns


	select a.*,b.info,b.upc from c8 a left join upcmast b on a.style=b.style and a.color=b.color and a.id=b.id and b.accountid=6303 into cursor c9 readwrite
	replace all division with getmemodata("info","DIV") for isnull(division)
	replace upc_a with upc_b for isnull(upc_a)
	replace upc2 with '' for isnull(upc2)

	select units,style,  color,  id, pack,availqty,allocqty, holdqty,pickedqty,spqty,overallqty,upc_a as upc,accountid, 00000 as ax_whse, division,upc2, whse as toll_whse,rcvqty,shpdqty,adjqty ,rtnqty from c9 into cursor c10 readwrite
*!*	replace ax_whse WITH 1000 FOR accountid =6303 AND toll_whse='NJ'
*!*	replace ax_whse WITH 1200 FOR accountid =6304 AND toll_whse='NJ'
*!*	replace ax_whse WITH 1320 FOR accountid =6305 AND toll_whse='NJ'
*!*	replace ax_whse WITH 1310 FOR accountid =6320 AND toll_whse='NJ'
*!*	replace ax_whse WITH 1221 FOR accountid =6321 AND toll_whse='NJ'
*!*	replace ax_whse WITH 1222 FOR accountid =6322 AND toll_whse='NJ'
*!*	replace ax_whse WITH 1223 FOR accountid =6323 AND toll_whse='NJ'
*!*	replace ax_whse WITH 1250 FOR accountid =6324 AND toll_whse='NJ'
*!*	replace ax_whse WITH 1151 FOR accountid =6325 AND toll_whse='NJ'
*!*	replace ax_whse WITH 1400 FOR accountid =6364 AND toll_whse='NJ'
*!*	replace ax_whse WITH 1600 FOR accountid =6543 AND toll_whse='NJ'
*!*	replace ax_whse WITH 1001 FOR accountid =6303 AND toll_whse='CA'
*!*	replace ax_whse WITH 1152 FOR accountid =6325 AND toll_whse='CA'

	replace ax_whse with 10100 for accountid =6303 and toll_whse='NJ'
	replace ax_whse with 10180 for accountid =6325 and toll_whse='NJ'
	replace ax_whse with 10190 for accountid =6543 and toll_whse='NJ'
	replace ax_whse with 11100 for accountid =6303 and toll_whse='CA'
	replace ax_whse with 11180 for accountid =6325 and toll_whse='CA'
	replace ax_whse with 11190 for accountid =6543 and toll_whse='CA'
	set step on
	use in inventory
	select * from c10 into cursor inventory readwrite

*******************Added 7/9/15  TM  capture weekly transactions  END
**************************************** end ALL accounts TM 01/16/2014
else






**************************************** begin wholesale and damages TM 12/03/2012
	if whichaccount = 2503
		select units, style, color, id, pack, availqty, allocqty, holdqty, 0000000 as pickedqty, 0000000 as spqty,000000 overallqty, space(13) as upc,accountid,"     " as division , space(13) as upc2 ;
		from inven where inlist(accountid,6303,6325) and units into cursor inventory readwrite

		index on str(accountid,4)+transform(units)+style+color+id+pack tag match

* inprocess is a cursor of all outship records where they are not yet delivered and also excluding "notonwip"

		xsqlexec("select outship.*, cast('NJ' as char(2)) as whse, style, color, id, pack, totqty from outship, outdet where outship.mod='J' " + ;
			"and outship.outshipid=outdet.outshipid and inlist(outship.accountid,6303,6325) " + ;
			"and del_date={} and notonwip=0 and qty#0 and pulled=1","xdy",,"wh")

		select *, sum(totqty) as pickedqty from xdy group by units, accountid, style, color, id, pack into cursor xunshipped

		select xunshipped
		scan
			if seek(str(accountid,4)+transform(units)+style+color+id+pack,"inventory","match")
				replace pickedqty with xunshipped.pickedqty in inventory
			endif
		endscan

* uncompleted special projects
		xsqlexec("select outdet.wo_num, outdet.accountid, units, style, color, id, pack, totqty " + ;
			"from outdet, project where outdet.mod='J' " + ;
			"and outdet.wo_num=project.wo_num and completeddt={} " + ;
			"and inlist(project.accountid,6303,6325,6304,6305,6320,6321,6322,6323,6324,6543,6364)","xdy",,"wh")
		
		select xsp
		scan
			if seek(str(accountid,4)+transform(units)+style+color+id+pack,"inventory","match")
				replace spqty with xsp.spqty in inventory
			else
			endif
		endscan

* copy and email inventory file

		select inventory
		delete for allocqty=0 and availqty=0 and pickedqty=0 and holdqty=0 and spqty=0

		select inventory
		scan &&for !Deleted()
			select upcmast
			do case
			case inlist(whichaccount,6364,6324,6320,6304)  && the SPE account, same master as Retail
				locate for upcmast.style = inventory.style and inventory.color = upcmast.color and inventory.id = upcmast.id and accountid = 6304  && use 6304 for the SPE Account
			case  inlist(whichaccount,6325,6303,6305,2503,6543)
				locate for upcmast.style = inventory.style and inventory.color = upcmast.color and inventory.id = upcmast.id and accountid = 6303
			otherwise
				locate for upcmast.style = inventory.style and inventory.color = upcmast.color and inventory.id = upcmast.id and accountid =  inventory.accountid
			endcase



			if found()
				replace inventory.upc with upcmast.upc in inventory
				if empty(upcmast.upc)
					alength = alines(aupc,upcmast.info,.t.,chr(13))
					cupc    = alltrim(segmentget(@aupc,"UPC",alength))
					replace inventory.upc2 with cupc in inventory
				endif
			else
				replace inventory.upc with "UNK"
			endif

************************begin division 12/3/2012 TM
			if empty( inventory.division)
				alength = alines(aupc,upcmast.info,.t.,chr(13))
				lcdiv   = alltrim(segmentget(@aupc,"DIV",alength))
				replace division with lcdiv in inventory
			else
				replace inventory.division with "UNK"
			endif
************************end division 12/3/2012 TM
		endscan

		select inventory
		scan
			replace pickedqty with pickedqty+allocqty && added 12/18/12 by TM to account for alloc from inven
			replace overallqty with availqty+holdqty+pickedqty+spqty
		endscan

**************************************** end wholesale and damages TM 12/03/2012
	else
* get inventory
		select units, style, color, id, pack, availqty, allocqty, holdqty, 0000000 as pickedqty, ;
		0000000 as spqty,000000 overallqty, space(13) as upc,accountid ,"     " as division , space(13) as upc2 ;
		from inven where accountid = whichaccount and units into cursor inventory readwrite

		index on str(accountid,4)+transform(units)+style+color+id+pack tag match

* inprocess is a cursor of all outship records where they are not yet delivered and also excluding "notonwip"

		xsqlexec("select units, outship.accountid, style, color, id, pack, totqty " + ;
			"from outship, outdet where outship.outshipid=outdet.outshipid and outship.mod='J' " + ;
			"and outship.accountid="+transform(whichaccount)+" and del_date={} " + ;
			"and notonwip=0 and qty#0 and pulled=1","inprocess",,"wh")

		select units, accountid  as accountid, style, color, id, pack, sum(totqty) as pickedqty ;
			from inprocess group by units, accountid ,style, color, id, pack into cursor xunshipped
		
		select xunshipped
		scan
			if seek(str(accountid,4)+transform(units)+style+color+id+pack,"inventory","match")
				replace pickedqty with xunshipped.pickedqty in inventory
			else
			endif
		endscan

* uncompleted special projects		dy 3/25/17 done this code run if so needs to be changed similar to above
		xsqlexec("select * from Project where mod='L' and completeddt={} " + ;
		"and inlist(Project.accountid,6303,6325,6304,6305,6320,6321,6322,6323,6324,6543,6364)",,,"wh")
		select outdet.wo_num, outdet.accountid as accountid,units, style, color, id, pack, sum(totqty) as spqty ;
		from outdet, project ;
		where outdet.wo_num=project.wo_num ;
		and project.accountid= whichaccount ;
		and empty(completeddt) ;
		group by 1,2,3,4,5,6,7 ;
		into cursor xsp

		select xsp
		scan
			if seek(str(accountid,4)+transform(units)+style+color+id+pack,"inventory","match")
				replace spqty with xsp.spqty in inventory
			else
			endif
		endscan

* copy and email inventory file

		select inventory
		delete for availqty=0 and pickedqty=0 and holdqty=0 and spqty=0 and allocqty=0 &&allocqty added 2/22/2013 missing records when allo >0





		select inventory
		scan &&for !Deleted()
			select upcmast
			do case
			case inlist(whichaccount,6364,6324,6320,6304)  && the SPE account, same master as Retail
				locate for upcmast.style = inventory.style and inventory.color = upcmast.color and inventory.id = upcmast.id and accountid = 6303  && use 6304 for the SPE Account
			case  inlist(whichaccount,6325,6303,6305,2503,6543)
				locate for upcmast.style = inventory.style and inventory.color = upcmast.color and inventory.id = upcmast.id and accountid = 6303
			otherwise
				locate for upcmast.style = inventory.style and inventory.color = upcmast.color and inventory.id = upcmast.id and accountid = whichaccount
			endcase

			if found()
				replace inventory.upc with upcmast.upc in inventory
				if empty(upcmast.upc)
					alength = alines(aupc,upcmast.info,.t.,chr(13))
					cupc    = alltrim(segmentget(@aupc,"UPC",alength))
					replace inventory.upc2 with cupc in inventory
				endif
			else
				replace inventory.upc with "UNK"
			endif

************************begin division 12/3/2012 TM
			if empty( inventory.division)
				alength = alines(aupc,upcmast.info,.t.,chr(13))
				lcdiv    = alltrim(segmentget(@aupc,"DIV",alength))
				replace division with lcdiv in inventory
			else
				replace inventory.division with "UNK"
			endif
************************end division 12/3/2012 TM
		endscan

		select inventory
		scan
			replace pickedqty with pickedqty+allocqty  && added 12/18/12 by TM to account for alloc from inven
			replace overallqty with availqty+ holdqty+pickedqty+spqty
		endscan


		select inventory
		scan
			replace overallqty with availqty+ holdqty+pickedqty+spqty
		endscan
	endif   &&&&&&&&&TM
endif  &&&&&TM 011614

gunshot()
set step on 

do case
case whichaccount = 6303
	acctnam = "Wholesale"
case whichaccount = 6304
	acctnam = "Retail"
case whichaccount = 6320
	acctnam = "SPE"
case whichaccount = 6321
	acctnam = "UK"
case whichaccount = 6322
	acctnam = "FR"
case whichaccount = 6323
	acctnam = "IT"
case whichaccount = 6305
	acctnam = "ECOMM"
case whichaccount = 6324
	acctnam = "RETAIL_DMG_"
case whichaccount = 6325
	acctnam = "WHOLESALE_DMG_"
case whichaccount = 6364
	acctnam = "EMP_SALE_"
case whichaccount = 2503
	acctnam = "Wholesale_and_Whsl_DMG_"
case whichaccount = 6543
	acctnam = "PRESS_SAMPLES_"
case whichaccount = 9999
	acctnam = "ALL_ACCOUNTS_"
otherwise
	acctnam = "UNK"
endcase
if whichaccount= 9999

	export to "s:\marcjacobsdata\reports\inventorysyncfiles\"+acctnam+"_Inventory_"+ttoc(datetime(),1) fields style,color,id,pack,availqty,pickedqty,holdqty,spqty,overallqty, upc,accountid,division,ax_whse,toll_whse,rcvqty,shpdqty,adjqty,rtnqty type xls
else
	export to "s:\marcjacobsdata\reports\inventorysyncfiles\"+acctnam+"_Inventory_"+ttoc(datetime(),1) fields style,color,id,pack,availqty,pickedqty,holdqty,spqty,overallqty, upc,accountid,division type xls
endif
if whichaccount= 6303
	copy to s:\marcjacobsdata\reports\inventorysyncfiles\bi\wholesale_inventory.csv fields style,color,id,pack,availqty,pickedqty,holdqty,spqty,overallqty,upc,accountid,division  type csv
else
	if whichaccount= 6325
		copy to s:\marcjacobsdata\reports\inventorysyncfiles\bi\wholesale_dmg_inventory.csv fields style,color,id,pack,availqty,pickedqty,holdqty,spqty,overallqty,upc,accountid,division  type csv
	else
		if whichaccount= 2503
			copy to s:\marcjacobsdata\reports\inventorysyncfiles\bi\wholesale_and_whsl_dmg_inventory.csv fields style,color,id,pack,availqty,pickedqty,holdqty,spqty,overallqty,upc,accountid,division  type csv
		else
			if whichaccount= 9999
				copy to s:\marcjacobsdata\reports\inventorysyncfiles\bi\all_accounts_inventory.csv fields style,color,id,pack,availqty,pickedqty,holdqty,spqty,overallqty,upc,accountid,division,ax_whse,toll_whse  type csv
			else
			endif
		endif
	endif
endif

****************************
procedure segmentget
****************************

parameter thisarray,lckey,nlength

for i = 1 to nlength
	if i > nlength
		exit
	endif
	lnend= at("*",thisarray[i])
	if lnend > 0
		lcthiskey =trim(substr(thisarray[i],1,lnend-1))
		if occurs(lckey,lcthiskey)>0
			return substr(thisarray[i],lnend+1)
			i = 1
		endif
	endif
endfor

return ""
