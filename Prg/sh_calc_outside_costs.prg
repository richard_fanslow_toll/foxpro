* Extract Outside vendor cost from WORKDESC memo field and place in new field Outscost.
* Do only if

LOCAL lcLine, lnPos, lnOutscost

SET EXCLUSIVE OFF

USE f:\shop\shdata\ordhdr IN 0
*USE m:\dev\shdata\ordhdr AGAIN IN 0

SET MEMOWIDTH TO 1000

SELECT ordhdr
SCAN FOR YEAR(CRDATE) = 2004 AND NOT EMPTY(WORKDESC)
	lcLine = MLINE(WORKDESC,1)
	lnPos = RAT("$",lcLine)
	IF lnPos > 0 THEN
		lnOutscost = VAL(SUBSTR(lcLine,lnPos + 1))
		REPLACE Outscost WITH lnOutscost
	ENDIF
ENDSCAN


SELECT ordhdr
BROWSE FOR YEAR(CRDATE) = 2004 AND NOT EMPTY(WORKDESC)

USE IN ordhdr