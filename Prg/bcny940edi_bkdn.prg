*!* m:\dev\prg\bcny940edi_bkdn.prg
CLEAR
WAIT WINDOW "Now in BKDN Phase..." NOWAIT

lCheckStyle = .T.
lPick = .F.
lPrepack = .T.
m.units = .F.
STORE "" TO cUnitofMeas,m.linenum,cDesc,cInnerpack1,cInnerpack2,cInnerpack,cUnitstype,cUOM,cOrigWt,cUOW,cOrigVol,cUOV
STORE "" TO lcStyle,lcColor,lcSize,lcQty,lcUPC,lcSKU,lcDesc

goffice="2"
gmasteroffice="C"

useca("ackdata","wh")

SELECT x856
LOCATE
IF lTesting
*BROWSE
ENDIF
LOCATE FOR TRIM(x856.segment) = "W20"
lW20 = IIF(FOUND(),.T.,.F.)

LOCATE
COUNT FOR TRIM(segment) = "ST" TO nSegCnt
cSegCnt = ALLTRIM(STR(nSegCnt))


WAIT WINDOW "Now in 940 Breakdown" &message_timeout2
WAIT WINDOW "There are "+cSegCnt+" P/Ts in this file" &message_timeout2
SELECT x856
LOCATE

*!* Constants
m.acct_name = "BCNY"
m.acct_num = nAcctNum
m.careof = " "
m.sf_addr1 = "25 NEWBRIDGE ROAD"
m.sf_addr2 = "SUITE 405"
m.sf_csz = "HICKSVILLE, NY 11801"
m.printed = .F.
m.print_by = ""
m.printdate = DATE()
m.custsku = ""
lJCPMail = .T.
*!* End constants

*!*	IF !USED("uploaddet")
*!*		IF lTesting
*!*			USE F:\ediwhse\test\uploaddet IN 0 ALIAS uploaddet
*!*		ELSE
*!*			USE F:\ediwhse\DATA\uploaddet IN 0 SHARED ALIAS uploaddet
*!*		ENDIF
*!*	ENDIF

IF USED('bcnyxref')
	USE IN bcnyxref
ENDIF
USE "f:\3pl\data\consolidator-xref" IN 0 ALIAS bcnyxref

SELECT xpt
DELETE ALL
SELECT xptdet
DELETE ALL

STORE "" TO m.isa_num,m.shipins,m.color,m.id
STORE 0 TO ptctr,pt_total_eaches,pt_total_cartons,nCtnCount

SELECT x856
SET FILTER TO
GOTO TOP

WAIT WINDOW "NOW IN INITIAL HEADER BREAKDOWN" &message_timeout2

ptidctr    =0
ptdetidctr =0

nISA = 0  && To check for embedded FA envelopes.
lJCP = .F.

DO WHILE !EOF("x856")
	cSegment = TRIM(x856.segment)
	cCode = TRIM(x856.f1)

	IF TRIM(x856.segment) = "ISA"
		IF nISA = 0
			nISA = 1
			m.isa_num = ALLTRIM(x856.f13)
			cISA_Num = ALLTRIM(x856.f13)
			SELECT x856
			SKIP
			LOOP
		ENDIF
	ENDIF

	IF (TRIM(x856.segment) = "GS")
		cgs_num = ALLTRIM(x856.f6)
		lcCurrentGroupNum = ALLTRIM(x856.f6)
		SELECT x856
		SKIP
		LOOP
	ENDIF

	IF TRIM(x856.segment) = "ST" AND TRIM(x856.f1) = "940"
		IF !lTesting
			m.groupnum=lcCurrentGroupNum
			m.isanum=cisa_num
			m.transnum=x856.f2
			m.edicode="OW"
			m.accountid=6221
			m.loaddt=date()
			m.loadtime=datetime()
			m.filename=xfile
			insertinto("ackdata","wh",.t.)
		
		ELSE
		endif
		SELECT x856
		SKIP
		LOOP
	ENDIF


	IF (TRIM(x856.segment) = "W05")
	IF !lTesting
		REPLACE ACKDATA.ship_ref WITH ALLTRIM(x856.f2) IN ACKDATA
				ELSE
		endif
		lDoLineInsert = .F.
		ptdetctr = 0
		ptidctr  = ptidctr    +1
		cCustnotes = ""
		m.vendor_num = ""
		nCtnCount = 0
		nSHCount = 1
		nDelCount = 1
		nDetCnt = 0
		SELECT xpt
		APPEND BLANK
		REPLACE xpt.accountid WITH 6221
		REPLACE xpt.shipins WITH "FILENAME*"+cFilename IN xpt
		REPLACE xpt.qty WITH 0 IN xpt
		REPLACE xpt.origqty WITH 0 IN xpt
		STORE 0 TO pt_total_eaches
		STORE 0 TO pt_total_cartons
		ptctr = ptctr +1
		WAIT WINDOW AT 10,10 "Now processing PT# "+ ALLTRIM(x856.f2)+" Number "+CHR(13)+TRANSFORM(ptctr)+" of "+cSegCnt NOWAIT
		REPLACE xpt.ptid       WITH ptidctr IN xpt
		REPLACE xpt.ptdate     WITH DATE() IN xpt
		m.ship_ref = ALLTRIM(x856.f2)
		IF m.ship_ref = 'EJ102173L-A_F19RPF'
			SET STEP ON
		ENDIF
		lSplitPT = .F.
		m.cnee_ref = ALLTRIM(x856.f3)
		REPLACE xpt.ship_ref   WITH m.ship_ref IN xpt  && Pick Ticket
		REPLACE xpt.cnee_ref WITH m.cnee_ref IN xpt  && Pick Ticket
		m.pt = ALLTRIM(x856.f2)
		REPLACE xpt.shipins WITH xpt.shipins+CHR(13)+"RELEASENUM*"+ALLTRIM(x856.f5) IN xpt
		REPLACE xpt.shipins WITH xpt.shipins+CHR(13)+"W0504*"+ALLTRIM(x856.f4) IN xpt  && Added per Wei, to go back in the W0608 element, if present.
		SELECT x856
		SKIP 1
		LOOP
	ENDIF

	IF TRIM(x856.segment) = "N1" AND INLIST(TRIM(x856.f1),"ST") && Ship-to data
		lN2 = .F.
*!*			IF !lTesting
*!*				SELECT uploaddet
*!*				LOCATE
*!*				IF !EMPTY(M.isa_num)
*!*					APPEND BLANK
*!*					REPLACE uploaddet.office    WITH cOffice              IN uploaddet
*!*					REPLACE uploaddet.cust_name WITH UPPER(TRIM(x856.f2)) IN uploaddet
*!*					REPLACE uploaddet.acct_num  WITH m.acct_num           IN uploaddet
*!*					REPLACE uploaddet.DATE      WITH DATETIME()           IN uploaddet
*!*					REPLACE uploaddet.isa_num   WITH m.isa_num            IN uploaddet
*!*					REPLACE uploaddet.startpt   WITH xpt.ship_ref         IN uploaddet
*!*					REPLACE uploaddet.runid     WITH lnRunID              IN uploaddet
*!*				ENDIF
*!*			ENDIF

		cConsignee = UPPER(TRIM(x856.f2))  && sometimes the ship-to is a DC or Unit name So use the BT r BY for the Consignee
		m.st_name = UPPER(TRIM(x856.f2))

		REPLACE xpt.shipins WITH xpt.shipins+CHR(13)+"SHIPTO*"+cConsignee IN xpt
		REPLACE xpt.consignee WITH cConsignee IN xpt

		cDCNum = ALLTRIM(x856.f4)
		REPLACE xpt.shipins WITH xpt.shipins+CHR(13)+"STORENUM*"+cDCNum IN xpt && Full-length DC Number
		REPLACE xpt.dcnum WITH RIGHT(cDCNum,10) IN xpt

		cStorenum1 = ALLTRIM(cDCNum)
		nStoreNum = INT(VAL(cStorenum1))
		lCons = .F.
		IF ("BUY"$m.st_name AND "BABY"$m.st_name) OR ("BBB"$m.st_name) && BUY BUY BABY/BBB
			ASSERT .F. MESSAGE "At store number load...debug"
			lCons = IIF(SEEK(nStoreNum,'syncxref','storenum'),.T.,.F.)
		ENDIF

		DO WHILE LEFT(cStorenum1,1) = '0'
			cStorenum1 = ALLTRIM(SUBSTR(cStorenum1,2))
		ENDDO
		REPLACE xpt.storenum WITH INT(VAL(LEFT(cStorenum1,5))) IN xpt
		RELEASE cStorenum1

		SKIP 1 IN x856
		IF TRIM(x856.segment) = "N2"
			SKIP 1 IN x856
		ENDIF

		IF TRIM(x856.segment) = "N3"  && address info
			m.st_addr1 = UPPER(TRIM(x856.f1))
			m.st_addr2 = UPPER(TRIM(x856.f2))
			REPLACE xpt.address  WITH m.st_addr1 IN xpt
			REPLACE xpt.address2 WITH IIF(lN2,cConsignee,m.st_addr2) IN xpt
		ENDIF
		SKIP 1 IN x856
		IF TRIM(x856.segment) = "N4"  && address info
			IF !EMPTY(TRIM(x856.f3))
				m.zipbar = TRIM(x856.f3)
			ENDIF
			IF !EMPTY(TRIM(x856.f2))
				m.st_csz = UPPER(TRIM(x856.f1))+", "+UPPER(TRIM(x856.f2))+" "+TRIM(x856.f3)
			ELSE
				m.st_csz = UPPER(TRIM(x856.f1))
			ENDIF
			REPLACE xpt.csz WITH m.st_csz IN xpt
			cCountry = ALLT(x856.f4)
			REPLACE xpt.shipins WITH xpt.shipins+CHR(13)+"COUNTRY*"+cCountry IN xpt
		ENDIF
		SELECT x856
		SKIP
		LOOP
	ENDIF

	IF TRIM(x856.segment) = "N1" AND TRIM(x856.f1)="SF" AND INLIST(TRIM(x856.f3),"1","9")
		REPLACE xpt.duns WITH ALLTRIM(x856.f4) IN xpt
		REPLACE xpt.shipins WITH xpt.shipins+CHR(13)+"DUNSQUAL*"+ALLTRIM(x856.f3) IN xpt
		REPLACE xpt.shipins WITH xpt.shipins+CHR(13)+"DUNSNUMBER*"+ALLTRIM(x856.f4) IN xpt
		SELECT x856
		SKIP
		LOOP
	ENDIF

	IF TRIM(x856.segment) = "N1" AND INLIST(TRIM(x856.f1),"BT","BY") && look for the Consignee as the Shipto might be a DC name
		IF TRIM(x856.f1)= "BY"
			REPLACE xpt.storenum WITH VAL(RIGHT(ALLTRIM(x856.f4),5)) IN xpt
			REPLACE xpt.sforstore WITH RIGHT(ALLTRIM(x856.f4),10) IN xpt
			REPLACE xpt.shipins WITH xpt.shipins+CHR(13)+"SFSTORENUM*"+TRIM(x856.f4) IN xpt
			REPLACE xpt.shipins WITH xpt.shipins+CHR(13)+"BYNAME*"+TRIM(x856.f2) IN xpt
			REPLACE xpt.shipins WITH xpt.shipins+CHR(13)+"N1-BY*"+TRIM(x856.f4) IN xpt
		ENDIF
	ENDIF

	IF TRIM(x856.segment) = "N1" AND INLIST(TRIM(x856.f1),"Z7") && Ship-for data
		csfor_name = TRIM(x856.f2)
		csfor_name = STRTRAN(csfor_name," - ","-")
		csfor_name = STRTRAN(csfor_name,", ",",")
		csfor_name = STRTRAN(csfor_name,"#","")

		REPLACE xpt.shipfor WITH UPPER(csfor_name) IN xpt
		csforStoreNum = ALLTRIM(x856.f4)
		REPLACE xpt.sforstore WITH csforStoreNum IN xpt
		SKIP 1 IN x856
		IF TRIM(x856.segment) = "N3"  && address info
			m.sforaddr1 = UPPER(TRIM(x856.f1))
			m.sforaddr2 = UPPER(TRIM(x856.f2))
			REPLACE xpt.sforaddr1 WITH m.sforaddr1 IN xpt
			REPLACE xpt.sforaddr2 WITH m.sforaddr2 IN xpt
		ENDIF
		SKIP 1 IN x856
		IF TRIM(x856.segment) = "N4"  && address info
			IF !EMPTY(TRIM(x856.f2))
				m.sforcsz = UPPER(TRIM(x856.f1))+", "+UPPER(TRIM(x856.f2))+" "+TRIM(x856.f3)
			ELSE
				m.sforcsz = UPPER(TRIM(x856.f1))
			ENDIF
			REPLACE xpt.sforcsz WITH m.sforcsz IN xpt
			cCountry = ALLT(x856.f4)
			IF !EMPTY(cCountry)
				REPLACE xpt.shipins WITH xpt.shipins+CHR(13)+"SFCOUNTRY*"+cCountry IN xpt
			ENDIF
		ENDIF
		SELECT x856
		SKIP
		LOOP
	ENDIF

	IF TRIM(x856.segment) = "N9"
		DO CASE
			CASE TRIM(x856.f1) = "DP"
				m.dept = ALLT(x856.f2)
*				cFOB = ALLT(x856.f3)
				REPLACE xpt.dept WITH m.dept IN xpt  && Dept
				cDeptName = ALLT(x856.f3)
				REPLACE xpt.shipins WITH xpt.shipins+CHR(13)+"DEPT*"+m.dept IN xpt
				REPLACE xpt.shipins WITH xpt.shipins+CHR(13)+"DEPTNAME*"+cDeptName IN xpt
*				REPLACE xpt.shipins WITH xpt.shipins+CHR(13)+"FOB*"+cFOB IN xpt

			CASE TRIM(x856.f1) = "IA"
				REPLACE xpt.vendor_num WITH ALLT(x856.f2)
				REPLACE xpt.shipins WITH xpt.shipins+CHR(13)+"VENDORNUM*"+ALLT(x856.f2) IN xpt

			CASE TRIM(x856.f1) = "SN"
				REPLACE xpt.shipins WITH xpt.shipins+CHR(13)+"SNNUM*"+ALLT(x856.f2) IN xpt

			CASE TRIM(x856.f1) = "PK"
				REPLACE xpt.shipins WITH xpt.shipins+CHR(13)+"PKNUM*"+ALLT(x856.f2) IN xpt

			CASE TRIM(x856.f1) = "VN"
				REPLACE xpt.shipins WITH xpt.shipins+CHR(13)+"VNINFO*"+ALLT(x856.f2) IN xpt

			CASE TRIM(x856.f1) = "VI"
				REPLACE xpt.shipins WITH xpt.shipins+CHR(13)+"VINUM*"+ALLT(x856.f2) IN xpt

			CASE TRIM(x856.f1) = "TP"
				REPLACE xpt.shipins WITH xpt.shipins+CHR(13)+"TPINFO*"+ALLT(x856.f2) IN xpt

			CASE TRIM(x856.f1) = "SCA"
				REPLACE xpt.shipins WITH xpt.shipins+CHR(13)+"SCAINFO*"+ALLT(x856.f2) IN xpt

			CASE TRIM(x856.f1) = "DTM"
				REPLACE xpt.shipins WITH xpt.shipins+CHR(13)+"DTMINFO*"+ALLT(x856.f2) IN xpt

			CASE TRIM(x856.f1) = "ST"
				REPLACE xpt.shipins WITH xpt.shipins+CHR(13)+"STINFO*"+ALLT(x856.f2) IN xpt

			CASE TRIM(x856.f1) = "WH"
				REPLACE xpt.shipins WITH xpt.shipins+CHR(13)+"WHINFO*"+ALLT(x856.f2) IN xpt

			CASE TRIM(x856.f1) = "AT"
				REPLACE xpt.shipins WITH xpt.shipins+CHR(13)+"ATINFO*"+ALLT(x856.f2) IN xpt

			CASE TRIM(x856.f1) = "TI"
				REPLACE xpt.shipins WITH xpt.shipins+CHR(13)+"EDI-INFO*"+ALLT(x856.f2) IN xpt

			CASE TRIM(x856.f1) = "FOB"
				REPLACE xpt.shipins WITH xpt.shipins+CHR(13)+"FOB*"+ALLT(x856.f2) IN xpt
				REPLACE xpt.shipins WITH xpt.shipins+CHR(13)+"GROUPNO*"+ALLT(x856.f3) IN xpt

			CASE TRIM(x856.f1) = "14"
				REPLACE xpt.shipins WITH xpt.shipins+CHR(13)+"SERV_LVL*"+ALLT(x856.f2) IN xpt

			CASE TRIM(x856.f1) = "4F"
				REPLACE xpt.shipins WITH xpt.shipins+CHR(13)+"ACCTNUM*"+ALLT(x856.f2) IN xpt

*			CASE TRIM(x856.f1) = "SMU" And TRIM(x856.f2) = "Y"
*               REPLACE xpt.cnee_ref  WITH Alltrim(xpt.cnee_ref)+"-SMU" IN xpt

		ENDCASE
		SELECT x856
		SKIP
		LOOP
	ENDIF

	IF TRIM(x856.segment) = "G62" AND TRIM(x856.f1) == "37"  && Start date
		lcDate = ALLTRIM(x856.f2)
		lxDate = SUBSTR(lcDate,5,2)+"/"+SUBSTR(lcDate,7,2)+"/"+LEFT(lcDate,4)
		ldDate = CTOD(lxDate)
		REPLACE xpt.START WITH ldDate
		SELECT x856
		SKIP
		LOOP
	ENDIF

	IF TRIM(x856.segment) = "G62" AND TRIM(x856.f1) == "38"  && cancel date
		lcDate = ALLTRIM(x856.f2)
		lxDate = SUBSTR(lcDate,5,2)+"/"+SUBSTR(lcDate,7,2)+"/"+LEFT(lcDate,4)
		ldDate = CTOD(lxDate)
		REPLACE xpt.CANCEL WITH ldDate
		SELECT x856
		SKIP
		LOOP
	ENDIF

	IF TRIM(x856.segment) = "NTE"  && Warehouse handling notes
		IF TRIM(x856.f1) = "COM"
			REPLACE xpt.shipins WITH xpt.shipins+CHR(13)+"COMPANY*"+ALLTRIM(x856.f2) IN xpt
		ELSE
			m.sh = "SPINS"+ALLT(STR(nSHCount))+"*"+ALLTRIM(x856.f2)  && ADDED THE ENUMERATION OF THE NOTES 4/17/2008
			nSHCount = nSHCount + 1
			REPLACE xpt.shipins WITH xpt.shipins+CHR(13)+m.sh IN xpt
		ENDIF
		SELECT x856
		SKIP
		LOOP
	ENDIF

	IF TRIM(x856.segment) = "W66" && usually, ship_via info
		REPLACE xpt.shipins WITH xpt.shipins+CHR(13)+"PMT_TYPE*"+ALLTRIM(x856.f1) IN xpt
		IF !EMPTY(x856.f5)
			cShip_via = ALLT(x856.f5)
			cShip_via = IIF(lCons,LEFT(cShip_via,26)+"-CONS",cShip_via)  &&  Added 07.07.2014 per Maria R.
			REPLACE xpt.ship_via WITH cShip_via IN xpt
			m.sh = "SPINS"+ALLT(STR(nSHCount))+"*SHIP VIA: "+ALLTRIM(x856.f5)  &&
			nSHCount = nSHCount + 1
			REPLACE xpt.shipins WITH xpt.shipins+CHR(13)+m.sh
		ENDIF
		SELECT x856
		SKIP
		LOOP
	ENDIF

	IF TRIM(x856.segment) = "LX" && start of PT detail, stay here until this PT is complete
		WAIT "NOW IN DETAIL LOOP PROCESSING" WINDOW NOWAIT
		SELECT xptdet
		CALCULATE MAX(ptdetid) TO m.ptdetid
		m.ptdetid = m.ptdetid + 1
		m.printstuff = ""
		m.ptid = xpt.ptid
		m.accountid = xpt.accountid
		m.units = .F.
		SKIP 1 IN x856

		DO WHILE ALLTRIM(x856.segment) != "SE"
			DO CASE
				CASE TRIM(x856.segment) = "LX"
					m.ptdetid = m.ptdetid + 1
					SELECT xpt
					SCATTER MEMVAR MEMO BLANK FIELDS EXCEPT ptdetid,ptid,accountid

				CASE TRIM(x856.segment) = "W01" && Destination Quantity
					SELECT x856
					cUOM = ""
					cUOM = ALLTRIM(x856.f2)
					m.printstuff = "FILENAME*"+cFilename
					m.printstuff = m.printstuff+CHR(13)+"ORIGQTY*"+ALLTRIM(x856.f1)
					m.printstuff = m.printstuff+CHR(13)+"UOM*"+cUOM
					lcQty     = ALLTRIM(x856.f1)

					lcUPC = ""
					DO CASE
						CASE ALLTRIM(x856.f4) = "UP"
							lcUPC   = ALLTRIM(x856.f5)
						CASE ALLTRIM(x856.f6) = "UP"
							lcUPC   = ALLTRIM(x856.f7)
						CASE ALLTRIM(x856.f15) = "UP"
							lcUPC   = ALLTRIM(x856.f16)
					ENDCASE

					lcStyle = ""
					DO CASE
						CASE ALLTRIM(x856.f4) = "VN"
							lcStyle   = ALLTRIM(x856.f5)
						CASE ALLTRIM(x856.f6) = "VN"
							lcStyle   = ALLTRIM(x856.f7)
						CASE ALLTRIM(x856.f15) = "VN"
							lcStyle   = ALLTRIM(x856.f16)
					ENDCASE

					lcCustSKU = ""
					DO CASE
						CASE ALLTRIM(x856.f4) = "IN"
							lcCustSKU   = ALLTRIM(x856.f5)
						CASE ALLTRIM(x856.f6) = "IN"
							lcCustSKU   = ALLTRIM(x856.f7)
						CASE ALLTRIM(x856.f15) = "IN"
							lcCustSKU   = ALLTRIM(x856.f16)
						OTHERWISE
							lcCustSKU   = "UNK"
					ENDCASE
					m.units = IIF(cUOM = "EA",.T.,.F.)
					m.totqty = INT(VAL(x856.f1))

* 					SET STEP ON
					nRec1 = RECNO('x856')

					IF !lDoLineInsert&& Standard Prepacks
						INSERT INTO xptdet (ptid,ptdetid,accountid,ship_ref,STYLE,PACK,units,totqty,origqty,upc,custsku) ;
							VALUES (xpt.ptid,m.ptdetid,xpt.accountid,xpt.ship_ref,lcStyle,"1",.F.,VAL(lcQty),VAL(lcQty),lcUPC,lcCustSKU)
						REPLACE xptdet.units WITH m.units IN xptdet
						REPLACE xptdet.printstuff WITH m.printstuff IN xptdet
						SKIP 1 IN x856
						IF ALLT(x856.segment) = "N9" AND ALLT(x856.f1) # "V"
							detailprepackfill()
						ELSE
							SKIP-1 IN x856
						ENDIF
					ELSE
						lDoLineInsert = .F.
						SELECT xpt
						REPLACE xpt.ship_ref WITH ALLTRIM(xpt.ship_ref)+" ~U" IN xpt
						m.ptid = xpt.ptid+1
						SCATTER FIELDS EXCEPT ptid MEMO MEMVAR
						APPEND BLANK
						GATHER MEMVAR MEMO
						REPLACE xpt.ship_ref WITH STRTRAN(ALLTRIM(xpt.ship_ref),"~U","~C") IN xpt
						SELECT x856
						SKIP 1 IN x856
						INSERT INTO xptdet (ptid,ptdetid,accountid,ship_ref,STYLE,PACK,units,totqty,origqty,upc,custsku) ;
							VALUES (xpt.ptid,m.ptdetid,xpt.accountid,xpt.ship_ref,lcUPC,"1",.F.,VAL(lcQty),VAL(lcQty),lcUPC,lcCustSKU)
						REPLACE xptdet.units WITH .F. IN xptdet
						detailprepackfill()
						ptidctr = ptidctr+1
					ENDIF

					SELECT x856
				CASE ALLTRIM(x856.segment) = "N9" AND ALLTRIM(x856.f1) = "VSZ"
					REPLACE xptdet.printstuff WITH ALLT(xptdet.printstuff)+CHR(13)+"ORIGSIZE*"+ALLTRIM(x856.f2) IN xptdet

				CASE ALLTRIM(x856.segment) = "N9" AND ALLTRIM(x856.f1) = "VCL"
					REPLACE xptdet.printstuff WITH ALLT(xptdet.printstuff)+CHR(13)+"ORIGCOLOR*"+ALLTRIM(x856.f2) IN xptdet

			ENDCASE
			SKIP 1 IN x856
		ENDDO

		SELECT xptdet
	ENDIF

	SELECT x856
	IF !EOF()
		SKIP 1 IN x856
	ENDIF
ENDDO

tu("ackdata")

SELECT xpt
GOTO TOP

SCAN
	SELECT xptdet
	detqty =0

	SCAN FOR xptdet.ptid = xpt.ptid
		detqty = detqty + xptdet.totqty
	ENDSCAN
	REPLACE xpt.qty     WITH detqty IN xpt
	REPLACE xpt.origqty WITH detqty IN xpt
ENDSCAN

SELECT xptdet
DO m:\dev\prg\setuppercase940

IF lBrowfiles
	WAIT WINDOW "Now browsing XPT/XPTDET files" &message_timeout2
	SELECT xpt
	LOCATE
	BROWSE
	SELECT xptdet
	LOCATE
	BROWSE
	IF MESSAGEBOX("Do you wish to continue?",4+16+256,"CONTINUE")=7
		CANCEL
		EXIT
	ENDIF
ENDIF

WAIT cCustname+" Breakdown Round complete..." WINDOW &message_timeout2
IF lJCP
	DO jcpmail
ENDIF

SELECT x856
IF USED("PT")
	USE IN pt
ENDIF
IF USED("PTDET")
	USE IN ptdet
ENDIF
WAIT CLEAR
RETURN

**********************
PROCEDURE jcpmail
**********************
	IF lJCPMail
		SELECT 0
		USE F:\3pl\DATA\mailmaster ALIAS mm SHARED
		LOCATE FOR edi_type = "MISC" AND taskname = "GENERAL"
		tsendtojcp = ALLTRIM(IIF(mm.use_alt,mm.sendtoalt,mm.sendto))
		tccjcp = ALLTRIM(IIF(mm.use_alt,mm.ccalt,mm.cc))
		USE IN mm
		cMailLoc = IIF(cOffice = "C","San Pedro",IIF(cOffice = "X","Carson","New Jersey"))
		tsubject= cCustname+" JCPenney Pickticket Upload: " +TTOC(DATETIME())
		tattach = ""
		tmessage = "Uploading Consignee: JCPenney Picktickets for "+cCustname
		tmessage = tmessage+CHR(13)+"Filename: "+cFilename
		tmessage = tmessage+CHR(13)+"Check PT table for correct UOM and BT number stored"
		tFrom ="Toll EDI Operations <toll-edi-ops@tollgroup.com>"
		DO FORM m:\dev\frm\dartmail2 WITH tsendtojcp,tFrom,tsubject,tccjcp,tattach,tmessage,"A"
		lJCPMail = .F.
	ENDIF
ENDPROC

***************************
PROCEDURE detailprepackfill
***************************
	SELECT xptdet
	ptdetidctr = ptdetidctr +1
	REPLACE xptdet.units      WITH .F.        IN xptdet
	REPLACE xptdet.printstuff WITH m.printstuff IN xptdet

	DO WHILE !INLIST(x856.segment,"SE","LX")
		IF TRIM(x856.segment) = "N9" AND TRIM(x856.f1) = "UP"
			cSubUPC = TRIM(x856.f2)
			cSubQty = TRIM(x856.f3)
			cSubGroup = TRIM(x856.f7)
			lensg = LEN(cSubGroup)
			IF lensg > 0
				FOR zq = 1 TO 5
					zq1 = ALLT(STR(zq))
					nPos&zq1 = AT(">",cSubGroup,zq)
				ENDFOR
				RELEASE ALL LIKE zq*
				cStyleQual = SUBSTR(cSubGroup,1,2)
				cStyle = SUBSTR(cSubGroup,nPOS1+1,nPOS2-nPOS1-1)
				cColorQual = SUBSTR(cSubGroup,nPOS2+1,nPOS3-nPOS2-1)
				cColor = SUBSTR(cSubGroup,nPOS3+1,nPOS4-nPOS3-1)
				cSizeQual = SUBSTR(cSubGroup,nPOS4+1,nPOS5-nPOS4-1)
				cSize = SUBSTR(cSubGroup,nPOS5+1)
			ENDIF
			REPLACE xptdet.printstuff WITH xptdet.printstuff+CHR(13)+CHR(13)+"SUBUPC*"+cSubUPC IN xptdet
			REPLACE xptdet.printstuff WITH xptdet.printstuff+CHR(13)+"SUBQTY*"+cSubQty IN xptdet
			IF !EMPTY(cSubGroup)
				REPLACE xptdet.printstuff WITH xptdet.printstuff+CHR(13)+"STYQUAL*"+cStyleQual IN xptdet
				REPLACE xptdet.printstuff WITH xptdet.printstuff+CHR(13)+"STYLE*"+cStyle IN xptdet
				REPLACE xptdet.printstuff WITH xptdet.printstuff+CHR(13)+"COLQUAL*"+cColorQual IN xptdet
				REPLACE xptdet.printstuff WITH xptdet.printstuff+CHR(13)+"COLOR*"+cColor IN xptdet
				REPLACE xptdet.printstuff WITH xptdet.printstuff+CHR(13)+"SZQUAL*"+cSizeQual IN xptdet
				REPLACE xptdet.printstuff WITH xptdet.printstuff+CHR(13)+"SIZE*"+cSize IN xptdet
			ENDIF
		ENDIF
		SKIP 1 IN x856
	ENDDO
	SKIP -1 IN x856

ENDPROC
