parameters xucc

xucc=padr(xucc,20)

if !used("seafolly")
	use (wf(goffice)+"seafolly") in 0
endif

if !seek(padr(xucc,20),"seafolly","ucc")
	x3winmsg("UCC not found")
	return .f.
endif

if !seek("6379"+seafolly.ship_ref,"outship","acctpt")
	x3winmsg("UCC not found")
	return .f.
endif

xoutshipid=outship.outshipid

select * from outship where outshipid = xoutshipid into cursor xufoutship readwrite
select ship_ref from xufoutship into cursor results

select xufoutship

do case
case inlist(xufoutship.ship_via,"UPS","UNITED PARCEL","UPCO","U.P.S.")
	m.carrier="UPS"
case inlist(xufoutship.ship_via,"FEDEX","FED EX","FEDERAL EXPRESS")
	m.carrier="FDX"
otherwise
	x3winmsg("This PT is not marked as UPS or Fedex")
	return .f.
endcase

if " AB "$csz or " BC "$csz or " MB "$csz or " NB "$csz or " NF "$csz or " NS "$csz or " NT "$csz or " ON "$csz or " PE "$csz or " QC "$csz or " SK "$csz or " YK "$csz
	xcanada=.t.
else
	xcanada=.f.
endif

store "" to xbilling, xservice
ufbillsvc(m.carrier, xcanada, xufoutship.ship_via, xufoutship.accountid, xufoutship.shipins)
m.billing=xbilling
m.service=xservice
xfredmeyer=.f.

ufload(,,,,,,,xucc,,,.t.)
