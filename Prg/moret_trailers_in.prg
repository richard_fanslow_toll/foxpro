Close Databases ALL
SET STATUS bar ON
SET SAFETY OFF
SET ENGINEBEHAVIOR 70

Set Step On
CD F:\0-packinglist\moret-ca\trailers
IF ADIR(ary1,"*.xls") = 0
	WAIT WINDOW "No Moret Trailer files to process" TIMEOUT 2
	RETURN
ENDIF
len1 = ALEN(ary1,1)

FOR qq = 1 TO len1
	xfile = ALLTRIM(ary1[qq,1])
	cTrailerName = JUSTSTEM(xfile)
	SELECT 0
	USE F:\3pl\DATA\trailerin
	SELECT * FROM trailerin WHERE .F. INTO CURSOR trailout READWRITE
	USE IN trailerin

	CREATE CURSOR IN1 ( ;
		A  c(20),;
		B  c(20),;
		c  c(20),;
		d  c(30),;
		e  c(20),;
		F  c(50),;
		G  c(20),;
		H  c(20),;
		i  c(20))


	CREATE CURSOR holdstyle (vendor c(10), COLOR c(20), STYLE c(15), UOM c(3), uctn N(10), upoly N(10), ;
		totqty N(10), ctncount N(10), PACK c(5), prepack l)
	SELECT holdstyle

	SELECT 200  && Creates a defined workspace for loading Excel data
	IMPORT FROM [&xfile] TYPE XL8
	SELECT 200
	talias=ALIAS()
	SELECT &talias
	STORE 0 TO ww
	STORE "" TO ww1



	SCAN
		FOR ww = 97 TO 105  && vars a through i
			ww1 = CHR(ww)
			STORE "" TO m.&ww1
		ENDFOR

		SCATTER MEMVAR
		SELECT IN1
		APPEND BLANK
		FOR ww = 97 TO 105
			ww1 = CHR(ww)
			cWW1 =  (m.&ww1)
			IF TYPE('cWW1') = 'N'
				REPLACE IN1.&ww1 WITH ALLTRIM(STR(cWW1)) IN IN1
			ELSE
				REPLACE IN1.&ww1 WITH ALLTRIM(cWW1) IN IN1
			ENDIF
		ENDFOR
	ENDSCAN
	SELECT IN1
	LOCATE
	LOCATE FOR IN1.A = "STYLE-TOT"
	SKIP - 1
	cShipID = ALLTRIM(IN1.A)
	LOCATE
	nCtnTot = 0
	SUM INT(VAL(IN1.d)) TO nCtnTot FOR IN1.A="STYLE-TOT" && AND IN1.G = "EA"  && Prepacks only
	IF nCtnTot = 0
		WAIT WINDOW "No Prepacks in this file"
		CLOSE DATABASES ALL
		LOOP
	ENDIF

	cCtnTot = ALLTRIM(STR(nCtnTot))
	LOCATE
	SCAN FOR IN1.A = "STYLE-TOT"
		SKIP -1
		cStyleNum = ALLTRIM(IN1.B)
		SKIP 1
		REPLACE IN1.B WITH cStyleNum NEXT 1
		cStyleNum = ""
	ENDSCAN
	LOCATE
	
	INSERT INTO trailout (fielda,fieldb) VALUES ("COMPANY:","SBH")
	INSERT INTO trailout (fielda,fieldb) VALUES ("SHIPMENT ID:",cShipID)
	INSERT INTO trailout (fielda,fieldb) VALUES ("CONTAINER #",cTrailerName)
	INSERT INTO trailout (fielda,fieldb) VALUES ("FORWARDER:","MORET")
	INSERT INTO trailout (fielda,fieldb) VALUES ("VESSEL:","")
	INSERT INTO trailout (fielda,fieldb) VALUES ("COUNTRY:","USA")
	INSERT INTO trailout (fielda,fieldb) VALUES ("SHIP-DATE:",DTOC(DATE()))
	INSERT INTO trailout (fielda,fieldb) VALUES ("ETA-DATE:",DTOC(DATE()))
	INSERT INTO trailout (fielda,fieldb) VALUES ("WHSE CODE:","3")
	INSERT INTO trailout (fielda,fieldb) VALUES ("CARTONS:",cCtnTot)
	INSERT INTO trailout (fielda,fieldb,fieldc,fieldd,fielde,fieldf,fieldg,fieldh,fieldi) VALUES ("","","","","","","","","")
	INSERT INTO trailout (fielda,fieldb,fieldc,fieldd,fielde,fieldf,fieldg,fieldh,fieldi) ;
		VALUES ("VENDOR","CONTRACT","STYLE","QTY","UOM","PREPACK","U/CTN","U/POLY","CARTONS")
	cVendor = "UNK"
	cContract = "UNK"
	cUPOLY = "0"
	SELECT IN1
	SCAN FOR IN1.A = "STYLE-TOT"
		cStyle = ALLTRIM(IN1.B)
		cPrepack = ALLTRIM(IN1.F)
		cUOM = ALLTRIM(IN1.G)
		STORE IN1.d TO cQTY,cUCTN,cCartons
		INSERT INTO trailout (fielda,fieldb,fieldc,fieldd,fielde,fieldf,fieldg,fieldh,fieldi) ;
			VALUES (cVendor,cContract,cStyle,cQTY,cUOM,cPrepack,cUCTN,cUPOLY,cCartons)
	ENDSCAN
	SELECT trailout
	LOCATE
*	COPY TO ("m:\joeb\temp\trailer\HBETR"+TTOC(DATETIME(),1)) TYPE XL5
	COPY TO ("f:\0-packinglist\moret-ca\"+xfile) TYPE XL5
	IF FILE(xfile)
		DELETE FILE [&xfile]
	ENDIF

	USE IN trailout
	WAIT "" TIMEOUT 3
ENDFOR

WAIT WINDOW "Process complete..." TIMEOUT 2
CLOSE DATABASES ALL
DELETE FILE "F:\0-packinglist\moret-ca\trailers\*.dbf"
CLEAR ALL
RETURN
