**created 06/02/2015
**process to create 856 outbound files for TJX

PARAMETERS xwo_num
PUBLIC lTesting,NormalExit,lEmail,cErrMsg,lcBasepath,lcArchpath,lcHoldPath,tsendto,tcc,tfrom,tattach,lDoTJXFileOut,lTestOut,cMessage,lIsaFlag
PUBLIC tsendto,tsendtoerr,tcc,tccerr,cFilenameHold,cPrgName
PUBLIC cMod,cMBOL,cEDIType,lParcelType,cFilenameShort,cFilenameOut,cFilenameArch,lcPath,cWO_NumList,cISA_Num

STORE "" TO tsendto,tsendtoerr,tcc,tccerr
cOffice = "C"
cMod = "C"
cEDIType = "856"
lParcelType = .F.
cMBOL = ""

cMessage ="Early Error"
cPrgName = "TJX 856"

TRY
	lTesting = .F.
	lIsaFlag = lTesting
	NormalExit = .F.
	cErrMsg = ""
	lEmail = .T.
	lTestOut = .F.
	lDoTJXFileOut = .T.
	tfrom  = "TGF Corporate Communication Department <tgf-edi-ops@tollgroup.com>"
	tattach  = ""

	IF VARTYPE(xwo_num) # "N"
		DO ediupdate WITH "NON-NUMERIC WO#",.T.
		THROW
	ENDIF
	lnWonum = xwo_num
	cWO_Num = ALLTRIM(STR(xwo_num))
	cWO_NumList = cWO_Num
	cBOL = cWO_Num

*!* SET CUSTOMER CONSTANTS
	ccustname = "TJX"  && Customer Identifier
	cX12 = "004010"  && X12 Standards Set used
	cMailName = "TJX"
	nAcctNum = 6565
	IF USED("mm")
		USE IN mm
	ENDIF
	SELECT 0
	USE F:\3pl\DATA\mailmaster ALIAS mm SHARED

	IF lTesting
		SELECT mm
		LOCATE FOR mm.edi_type = "856" AND mm.office = "C" AND mm.accountid = 6565
		lUseAlt = mm.use_alt
		lcBasepath = IIF(lTestOut,"c:\tempfox\",mm.basepath)
		lcHoldPath = mm.holdpath
		lcArchpath = mm.archpath
		tsendto = IIF(lUseAlt,mm.sendtoalt,mm.sendto)
		tcc = IIF(lUseAlt,mm.ccalt,mm.cc)
		tsendtoerr = tsendto
		tccerr = tcc
		tsendtotest = tsendto
		tcctest = tcc
	ELSE
		LOCATE FOR mm.edi_type = "856" AND mm.office = "C" AND mm.accountid = 6565
		IF !FOUND()
			DO ediupdate WITH "MAILMASTER ACCT PROBLEM",.T.
			THROW
		ENDIF
		lcBasepath = IIF(lTestOut,"c:\tempfox\",mm.basepath)
		lcHoldPath = mm.holdpath
		lcArchpath = mm.archpath
		lUseAlt = mm.use_alt
		tsendto = IIF(lUseAlt,mm.sendtoalt,mm.sendto)
		tcc = IIF(lUseAlt,mm.ccalt,mm.cc)
		LOCATE
		LOCATE FOR mm.edi_type = "MISC" AND mm.taskname = "GENERAL"
		lUseAlt = mm.use_alt
		tsendtoerr = IIF(lUseAlt,mm.sendtoalt,mm.sendto)
		tccerr = IIF(lUseAlt,mm.ccalt,mm.cc)
		LOCATE
		LOCATE FOR mm.edi_type = "MISC" AND mm.taskname = "GENERAL"
		tsendtotest = IIF(lUseAlt,mm.sendtoalt,mm.sendto)
		tcctest = IIF(lUseAlt,mm.ccalt,mm.cc)
	ENDIF
	USE IN mm

	xoutdir="f:\ftpusers\tjmax\856out\"

	IF USED('detail')
		USE IN DETAIL
	ENDIF

	IF lTesting
		CLOSE DATA ALL
		USE F:\tjmax\DATA\tjxdetail.DBF IN 0 ALIAS DETAIL
	ELSE
		USE F:\wo\wodata\DETAIL IN 0
	ENDIF

	IF USED('wolog')
		USE IN wolog
	ENDIF
	USE F:\wo\wodata\wolog IN 0

	dt1     = TTOC(DATETIME(),1)
	dtmail  = TTOC(DATETIME())
	dt2     = DATETIME()
	cdate = DTOS(DATE())
	ctruncdate = RIGHT(cdate,6)
	ctime = LEFT(TIME(DATE()),2)+SUBSTR(TIME(DATE()),4,2)
	cfiledate = cdate+ctime
	cfd = "^"
	csegd = "~"
	nSegCtr = 0
	segterminator = "~" && Used at end of ISA segment
	gn856Ctr =0
	stNum = ""

	cString = ""
	nISA_Num = ""
	c_CntrlNum = ""

	lcPath = lcBasepath
	cFilenameHold = (ALLTRIM(lcHoldPath)+"TJX856"+dt1+".edi")
	cFilename = cFilenameHold
	cFilenameShort = JUSTFNAME(cFilenameHold)
	cFilenameArch = (ALLTRIM(lcArchpath)+cFilenameShort)
	cFilenameOut = (ALLTRIM(lcBasepath)+cFilenameShort)
	nFilenum = FCREATE(cFilenameHold)

	IF lTesting
		IF USED("edi_trigger")
			USE IN edi_trigger
		ENDIF
		USE F:\3pl\DATA\edi_trigger IN 0
	ENDIF

	xsendqual="ZZ"
	xsendid=PADR("TGFUSA",15," ")
	xrcvqual="12"
	xrcvid=PADR("5084758255",15," ")

	xfilestr=""

	STORE 0 TO shipmentlevelcount
	STORE 1 TO currentshipmentlevelcount
	STORE 0 TO equipmentlevelcount
	STORE 0 TO thisequiplevelcount
	STORE 0 TO orderlevelcount
	STORE 0 TO Thisorderlevelcount
	STORE 0 TO packlevelcount
	STORE 0 TO itemlevelcount
	STORE 0 TO hlctr
	STORE 1 TO hlsegcnt
	STORE 0 TO lnCTTCount
	totlincnt = 0
	lcInterctrlNum =""

	WAIT WINDOW "856 Processing for Work Order "+TRANSFORM(lnWonum)+"..." NOWAIT

	SELECT * FROM DETAIL WHERE wo_num = VAL(TRANSFORM(xwo_num)) INTO CURSOR tdetail READWRITE
	SELECT tdetail
	llreceived = .F.
	SCAN
		IF tdetail.rcv_qty > 0
			llreceived = .T.
		ENDIF
	ENDSCAN
	IF !llreceived
		DO ediupdate WITH "NO 856,Not Received ",.T.
		RETURN
	ENDIF

***************************************************************************
	WriteHeader()

	SELECT wolog
	=SEEK(lnWonum,"wolog","wo_num")

	lcseal=wolog.seal
	SELECT DETAIL
	=SEEK(lnWonum,"detail","wo_num")

	shipmentheader(TRANSFORM(lnWonum))

	STORE 3 TO segcnt

	STORE "" TO xerrormsg, xmemomsg &&may be updated in shipment_level() if error occurs
	xretrylater=.F.

************************************************************************************************88
	IF !equipment_level()
		IF !EMPTY(xerrormsg)
			tsubject = "TJX 856 Creation Error at "+TTOC(DATETIME())
			tmessage = xerrormsg
			DO FORM dartmail2 WITH tsendto,tfrom,tsubject,tcc,tattach,tmessage,"A"
			ediupdate(xerrormsg,.T.)
			THROW
		ENDIF
	ELSE

** OK wrote out the header and equipment now cycle through the POs

		xerror=.F.
		xemptyfile=.T.
		SELECT tdetail
		GOTO TOP
		SCAN

			order_level(tdetail.po_num)
		ENDSCAN

		STORE "CTT*"+ALLTRIM(STR(lnCTTCount,6,0))+segterminator TO cString
		DO cstringbreak
		segcnt=segcnt+1
		lcCtr = ALLTRIM(STR(totlincnt))
		lincnt = 1
		STORE 0 TO lnCTTCount
		lc856Ctr = ALLTRIM(STR(gn856Ctr))
		lcSegCtr = ALLTRIM(STR(segcnt))

		cString ="SE*"+lcSegCtr+"*"+stNum+PADL(lc856Ctr,4,"0")+segterminator
		DO cstringbreak

**process the next work order in the table
*!*	    summaryinfo = ;
*!*	    "-----------------------------------------------------------------------------"+Chr(13)+;
*!*	    '856 file name....'+"---> "+lc_cnum+Chr(13)+;
*!*	    '   created.......'+Dtoc(Date())+Chr(13)+;
*!*	    '   file.....: '+tfile+Chr(13)+;
*!*	    '-----------------------------------------------------------------------------'+Chr(13)+;
*!*	    'shipment level count.....'+Str(thisshipmentlevelcount)+Chr(13)+;
*!*	    '   order level count.....'+Str(thisorderlevelcount)+Chr(13)+;
*!*	    '    pack level count.....'+Str(thispacklevelcount)+Chr(13)+;
*!*	    '    item level count.....'+Str(thisitemlevelcount)+Chr(13)+;
*!*	    '-----------------------------------------------------------------------------'+Chr(13)

		lc856Ctr = ALLTRIM(STR(gn856Ctr))
		STORE "GE*"+lc856Ctr+"*"+stNum+segterminator TO cString
		DO cstringbreak

		STORE "IEA*1*"+lcInterctrlNum+segterminator TO cString
		DO cstringbreak

		FCLOSE(nFilenum)
		xtriggernwd=.T.
	ENDIF &&shipment_level()

	STORE "" TO workordersprocessed
	STORE 0 TO thisshipmentlevelcount,Thisorderlevelcount,thispacklevelcount,thisitemlevelcount

	DO ediupdate WITH "856 CREATED",.F.


	COPY FILE [&cFilenameHold] TO [&cFilenameArch]

	IF lDoTJXFileOut
		COPY FILE [&cFilenameHold] TO [&cFilenameOut]
	ENDIF
	IF FILE(cFilenameArch)
		DELETE FILE [&cFilenameHold]
	ENDIF

	IF lTesting
		tsubject = "TJX 856 *TEST* EDI from TGF as of "+dtmail
	ELSE
		tsubject = "TJX 856 EDI from TGF as of "+dtmail
	ENDIF
	tmessage = "TJX 856 EDI Info from TGF-CA, Truck WO# "+ALLTRIM(STR(xwo_num))+CHR(13)
	tmessage = tmessage +" has been created."+CHR(13)+"(Filename: "+cFilenameShort+")"

	IF lEmail
		DO FORM dartmail2 WITH tsendto,tfrom,tsubject,tcc,tattach,tmessage,"A"
	ENDIF

CATCH TO oErr
	IF NormalExit = .F.
		ASSERT .F. MESSAGE "In Error Catch"
		SET STEP ON
		tsendto = tsendtoerr
		tcc = tccerr
		tsubject = cMailName+" 856 Upload Error ("+TRANSFORM(oErr.ERRORNO)+") at "+TTOC(DATETIME())
		tattach  = ""
		tmessage = cMailName+" 856 Upload Error..... Please fix"
		tmessage = tmessage+CHR(13)+cMessage+CHR(13)+CHR(13)
		lcSourceMachine = SYS(0)
		lcSourceProgram = SYS(16)

		tmessage =tmessage+ "Try/CATCH Exeception Message:"+CHR(13)+;
			[  Error: ] + STR(oErr.ERRORNO) +CHR(13)+;
			[  LineNo: ] + STR(oErr.LINENO) +CHR(13)+;
			[  Message: ] + oErr.MESSAGE +CHR(13)+;
			[  Procedure: ] + oErr.PROCEDURE +CHR(13)+;
			[  Details: ] + oErr.DETAILS +CHR(13)+;
			[  StackLevel: ] + STR(oErr.STACKLEVEL) +CHR(13)+;
			[  LineContents: ] + oErr.LINECONTENTS+CHR(13)+;
			[  UserValue: ] + oErr.USERVALUE+CHR(13)+;
			[  Computer:  ] +lcSourceMachine+CHR(13)+;
			[  940 file:  ] +xFile+CHR(13)+;
			[  Program:   ] +lcSourceProgram

		tattach  = ""

		DO FORM m:\dev\FRM\dartmail2 WITH tsendto,tfrom,tsubject,tcc,tattach,tmessage,"A"

		asn_out_data()

		COPY FILE [&cFilenameHold] TO [&cFilenameArch]
		IF FILE(cFilenameArch)
			DELETE FILE [&cFilenameHold]
		ENDIF

	ELSE
		WAIT WINDOW AT 10,10 "Normal Exit " TIMEOUT 2
	ENDIF
FINALLY
	ON ERROR
	IF USED('wolog')
		USE IN wolog
	ENDIF
	IF USED('detail')
		USE IN DETAIL
	ENDIF

	IF lTesting
		CLOSE DATABASES ALL
	ENDIF
ENDTRY


********************************************************************************************************************
* E Q U I P M E N T   L E V E L
********************************************************************************************************************
PROC equipment_level

*Set Step On
	hlctr = hlctr +1
	equipmentlevelcount = equipmentlevelcount+1
	thisequiplevelcount = thisequiplevelcount+1
	currentequiplevelcount = hlctr
	lc_hlcount = ALLTRIM(getnum(hlctr))								&& overall HL counter
	lc_parentcount = ALLTRIM(getnum(currentshipmentlevelcount ))	&& the parent

	STORE "HL*"+lc_hlcount+"*"+lc_parentcount+"*"+"E"+segterminator TO cString
	DO cstringbreak
	segcnt=segcnt+1
	hlsegcnt=hlsegcnt+1

* count the line items for the CTT segment counter
	lnCTTCount = lnCTTCount +1

	SELECT tdetail
*Sum pl_qty To totctns
	SUM rcv_qty TO totctns

	STORE "TD1*CTN*"+ALLTRIM(TRANSFORM(totctns))+segterminator TO cString
	DO cstringbreak
	segcnt=segcnt+1

	SELECT tdetail
	GO TOP

	STORE "TD3**"+SUBSTR(tdetail.CONTAINER,1,4)+"*"+SUBSTR(tdetail.CONTAINER,5,7)+"******"+lcseal+segterminator TO cString
	DO cstringbreak
	segcnt=segcnt+1

	lcSaildate=getmemodata("tdetail.suppdata","SAILDATE")
	IF lcSaildate = "NA"
		lcSaildate = DTOS(DATE()-15)
	ENDIF
	STORE "DTM*370*"+ALLTRIM(lcSaildate)+segterminator TO cString
	DO cstringbreak
	segcnt=segcnt+1

	STORE "DTM*372*"+DTOS(tdetail.pickedup)+segterminator TO cString
	DO cstringbreak
	segcnt=segcnt+1

	STORE "DTM*019*"+DTOS(tdetail.stripped)+segterminator TO cString
	DO cstringbreak
	segcnt=segcnt+1

	STORE "N1*DS**92*"+ALLTRIM("996")+segterminator TO cString
	DO cstringbreak
	segcnt=segcnt+1

ENDPROC &&equipment level

********************************************************************************************************************
* O R D E R    L E V E L
********************************************************************************************************************
PROC order_level
	PARAMETER xpo_num

	hlctr = hlctr +1
	orderlevelcount = orderlevelcount+1
	Thisorderlevelcount = Thisorderlevelcount+1
	lc_hlcount = getnum(hlctr)								&& overall HL counter
	lc_parentcount = getnum(currentshipmentlevelcount)				&& the parent
	currentorderlevelcount = hlctr

	STORE "HL*"+ALLTRIM(lc_hlcount)+"*"+ALLTRIM(lc_parentcount)+"*O"+segterminator TO cString
	DO cstringbreak
********************************************************************************************************************************
	segcnt=segcnt+1
	hlsegcnt=hlsegcnt+1

** PRF
	STORE "PRF*"+ALLTRIM(tdetail.po_num)+segterminator TO cString
	DO cstringbreak
	segcnt=segcnt+1

	STORE "TD1*CTN*"+ALLTRIM(STR(tdetail.rcv_qty))+segterminator TO cString
	DO cstringbreak
	segcnt=segcnt+1

	xchain = getmemodata("suppdata","CHAIN")
	IF xchain = "NA"
		IF !EMPTY(tdetail.whse_loc)
			xchain = tdetail.whse_loc
		ELSE
			xchain = "28"
		ENDIF
	ELSE

	ENDIF

	STORE "REF*19*"+ALLTRIM(xchain)+segterminator TO cString
	DO cstringbreak
	segcnt=segcnt+1

ENDPROC &&order_level


********************************************************************************************************************
* S H I P M E N T     L E V E L
********************************************************************************************************************
PROC shipment_level
* reset all at each shipment level

	STORE 0 TO shipmentlevelcount
	STORE 0 TO equipmentlevelcount
	STORE 0 TO orderlevelcount
	STORE 0 TO packlevelcount
	STORE 0 TO itemlevelcount
	STORE 0 TO hlctr


	shipmentlevelcount = shipmentlevelcount +1
	thisshipmentlevelcount = thisshipmentlevelcount +1
	hlctr = hlctr +1
	currentshipmentlevelcount = hlctr
**currentshipmentlevelcount = shipmentlevelcount
	lc_hlcount = ALLTRIM(getnum(hlctr))
	STORE "HL*"+lc_hlcount+"**S"+segterminator TO cString

	xfilestr=xfilestr+cString
********************************************************************************************************************
	segcnt=segcnt+1
	hlsegcnt=hlsegcnt+1

**TD1 NEED TOTAL CTNS/WEIGHT/CUBE FOR TRIP

** TD5
&&&
	STORE "TD5*B*2*FMIX*M*FMI INC."+segterminator TO cString

	DO cstringbreak
	segcnt=segcnt+1

** TD3
&&&

	xsqlexec("select * from fxwolog where wo_num="+TRANSFORM(lnWonum),"xfxwolog",,"fx")

	DO CASE
		CASE RECCOUNT("xfxwolog")=0 AND EMPTY(tmfst.trailer)
**if not found in express and empty(manifest.trailer), error out - mvw 02/14/11
			xerrormsg="Error in trip "+TRANSFORM(lnWonum)+": Trailer number empty in manifest.dbf, file not created. Will retry file creation on next run."
			xmemomsg="**Trailer number empty in manifest.dbf, will retry**"+CHR(13)
			xretrylater=.T.
			RETURN .F.
		CASE RECCOUNT("xfxwolog")=0 AND LEN(ALLTRIM(tmfst.trailer))>6
**DO NOT send trips that have been loaded onto a ctr (vs a trailer), as per dan lewis - mvw 02/16/11
			xerrormsg="" &&left blank so no error email is sent
			xmemomsg="**Container used for transport (manifest.trailer), No file required**"+CHR(13)
			xretrylater=.F.
			RETURN .F.
		CASE RECCOUNT("xfxwolog")=0
**if not found in express, use the manifest trailer number (FMIUNK causes issues for jones) - mvw 02/14/11
*		lcTrailerNumber= "FMIUNK"
			lcTrailerNumber=STRTRAN(tmfst.trailer,"-")
		CASE RECCOUNT("xfxwolog")#0 AND EMPTY(xfxwolog.trailer)
			xerrormsg="Error in trip "+TRANSFORM(lnWonum)+": Trailer number empty in fxwolog.dbf, file not created. Will retry file creation on next run."
			xmemomsg="**Trailer number empty in fxwolog.dbf, will retry**"+CHR(13)
			xretrylater=.T.
			RETURN .F.
		OTHERWISE
			lcTrailerNumber= xfxwolog.trailer
	ENDCASE

	STORE "TD3*CN*FMIX*"+ALLTRIM(lcTrailerNumber)+"*******P5GP"+segterminator TO cString
	DO cstringbreak
	segcnt=segcnt+1

**changed to send lnwonum as local trips would always get a 0 in this element - mvw 08/03/11
*	Store "REF*BM*"+Alltrim(Str(fxwolog.WO_NUM))+"*FMI TRIP NUMBER"+segterminator To cstring  && FMI TRIP NO
	STORE "REF*BM*"+TRANSFORM(lnWonum)+"*FMI TRIP NUMBER"+segterminator TO cString  && FMI TRIP NO

	DO cstringbreak
	segcnt=segcnt+1

	STORE "REF*WS*NONE*DC"+segterminator   TO cString  && hardcoded as per direction from lognet
	DO cstringbreak
	segcnt=segcnt+1

	STORE "R4*R*UN*US LAX**US"+segterminator TO cString && hardcoded as per direction from lognet
	DO cstringbreak
	segcnt=segcnt+1


** DTM the date pickup appt date
** get the date out of FX wolog
* might need some work here on trips that are not express trips....................................................
* as trucking WOs do not have del appt and pu appt

	SET DATE TO ymd
	SET CENT ON

	xsqlexec("select * from fxwolog where wo_num="+TRANSFORM(lnWonum),"xfxwolog",,"fx")

	STORE {} TO ld_dateshipped, ld_delappt, ld_delivered
	IF RECCOUNT("xfxwolog")#0
		xsqlexec("select * from fxtrips where wo_num="+TRANSFORM(xfxwolog.wo_num)+" and deadhead=0","tripdates",,"fx")
		GOTO BOTTOM
		ld_dateshipped = pickedup
		ld_delappt     = delappt
		ld_delivered   = delivered
	ENDIF

	ld_dateshipped = IIF(EMPTY(ld_dateshipped),tmfst.stripped,ld_dateshipped)
	ld_delappt = IIF(EMPTY(ld_delappt),DATE(),ld_delappt)
	ld_delivered = IIF(EMPTY(ld_delivered),DATE(),ld_delivered)

	DO CASE
		CASE tmfst.stripped>ld_dateshipped
**just take the later date to pass lognet's checks - mvw 06/24/08
*!*			xerrormsg="Error in trip "+transform(tmfst.wo_num)+": Strip date (manifest.stripped) shows as after the delivered start date (fxtrips.pickedup), file not created."
*!*			return .f.
			ld_dateshipped=tmfst.stripped
		CASE ld_dateshipped>ld_delappt
**just take the later date to pass lognet's checks - mvw 06/24/08
*!*			xerrormsg="Error in trip "+transform(tmfst.wo_num)+": Delivery start date (fxtrips.pickedup) shows as after the estimated delivery date (fxtrips.delappt), file not created."
*!*			return .f.
			ld_delappt=ld_dateshipped
	ENDCASE

** N1--SHIP FROM
	STORE "N1*SF*FMI INC.*ZZ*FMIX"+segterminator TO cString
	DO cstringbreak
	segcnt=segcnt+1

	STORE "N3*350 WESTMOUNT DRIVE"+segterminator TO cString
	DO cstringbreak
	segcnt=segcnt+1

	STORE "N4*SAN PEDRO*CA*90731"+segterminator TO cString
	DO cstringbreak
	segcnt=segcnt+1

** N1--SHIP TO
&&&
	STORE "N1*ST*JONES APPAREL GROUP*92*"+ALLTRIM(tmfst.delloc)+segterminator TO cString
	DO cstringbreak
	segcnt=segcnt+1

	SELECT dellocs
	LOCATE FOR ALLTRIM(location) = ALLTRIM(tmfst.delloc)
	IF FOUND()
		lcAddr = IIF(!EMPTY(dellocs.address),dellocs.address,IIF(!EMPTY(dellocs.acct_name2),dellocs.acct_name2,"UNK"))
		lcCity = dellocs.city
		lcState = dellocs.state
		lcZip = dellocs.zip
	ELSE
		lcAddr = "UNK"
		lcCity = "UNK"
		lcState = "UNK"
		lcZip  = "UNK"
	ENDIF

	STORE "N3*"+ALLTRIM(lcAddr)+segterminator TO cString
	DO cstringbreak
	segcnt=segcnt+1

	STORE "N4*"+ALLTRIM(lcCity)+"*"+ALLTRIM(lcState)+"*"+ALLTRIM(lcZip)+segterminator TO cString
	DO cstringbreak
	segcnt=segcnt+1

	SET DATE TO MDY
	SET CENT OFF

ENDPROC &&shipment_level


********************************************************************************************************
PROCEDURE shipmentheader
	PARAMETERS tshipid

	gn856Ctr = gn856Ctr +1
	STORE "ST*856*"+stNum+PADL(ALLTRIM(STR(gn856Ctr)),4,"0")+segterminator TO cString
	DO cstringbreak

	tshipid = TRANSFORM(lnWonum)

	STORE "BSN*00*"+ALLTRIM(tshipid)+"*"+DTOS(DATE())+"*"+ctime+"*0001"+segterminator TO cString
	DO cstringbreak
	totlincnt = 0
	RETURN
ENDPROC &&shipmentheader

********************************************************************************************************
PROCEDURE WriteHeader
* get the next unique control number

	DO num_incr_isa

	stNum = ALLTRIM(c_CntrlNum)
	lc_856num =stNum
	lcInterctrlNum = PADL(ALLTRIM(STR(nISA_Num )),9,"0")
	cISA_Num = lcInterctrlNum
*  write out the 214 header

	STORE "ISA*00*          *00*          *"+xsendqual+"*"+xsendid+"*"+xrcvqual+"*"+xrcvid+"*"+ctruncdate+"*"+ctime+"*U*00401*"+lcInterctrlNum +"*0*P*>"+segterminator TO cString
	DO cstringbreak

	STORE "GS*SH*"+ALLTRIM(xsendid)+"*"+ALLTRIM(xrcvid)+"*"+ctruncdate+"*"+ctime+"*"+stNum+"*X*004010"+segterminator TO cString
	DO cstringbreak

ENDPROC

********************************************************************************************************
FUNCTION getnum
	PARAM plcount
	fidnum = IIF(BETWEEN(plcount,1,9),SPACE(11)+LTRIM(STR(plcount)),;
		IIF(BETWEEN(plcount,10,99),SPACE(10)+LTRIM(STR(plcount)),;
		IIF(BETWEEN(plcount,100,999),SPACE(9)+LTRIM(STR(plcount)),;
		IIF(BETWEEN(plcount,1000,9999),SPACE(8)+LTRIM(STR(plcount)),;
		IIF(BETWEEN(plcount,10000,99999),SPACE(7)+LTRIM(STR(plcount))," ")))))
	RETURN fidnum

****************************
PROCEDURE cstringbreak
****************************
	cLen = LEN(ALLTRIM(cString))
	IF !lTesting
		FWRITE(nFilenum,cString)
	ELSE
		FWRITE(nFilenum,cString)
	ENDIF
	RETURN
********************************************************************************************************
PROC fputstring
	PARAM filehandle,filedata
	FWRITE(filehandle,filedata)
ENDPROC


********************************************************************************************************
PROC CheckEmpty
	PARAMETER lcDataValue
	RETURN IIF(EMPTY(lcDataValue),"UNKNOWN",lcDataValue)
ENDPROC

****************************
PROCEDURE num_incr_isa
****************************
	IF !USED("serfile")
		USE F:\3pl\DATA\serial\tjx_serial ALIAS serfile IN 0
	ENDIF
	SELECT serfile
	IF lTesting AND lIsaFlag
		nOrigSeq = serfile.seqnum
		lIsaFlag = .F.
	ENDIF
	nISA_Num = serfile.seqnum
	c_CntrlNum = ALLTRIM(STR(serfile.seqnum))
	REPLACE serfile.seqnum WITH serfile.seqnum + 1 IN serfile
ENDPROC

****************************
PROCEDURE ediupdate
****************************
	PARAMETER cStatus,lIsError
	NormalExit = .T.

	IF !lTesting
		SELECT edi_trigger
		IF !lIsError
			REPLACE processed WITH .T.,proc945 WITH .T.,file945 WITH cFilename, ;
				fin_status WITH "856 CREATED",errorflag WITH .F.,when_proc WITH DATETIME() ;
				FOR edi_trigger.wo_num = xwo_num AND accountid = nAcctNum AND edi_type ="856"
		ELSE
			REPLACE processed WITH .T.,proc945 WITH .F.,file945 WITH "",;
				fin_status WITH cStatus,errorflag WITH .T. ;
				FOR edi_trigger.wo_num = xwo_num AND accountid = nAcctNum AND edi_type ="856"
		ENDIF
	ENDIF

	IF lIsError AND lEmail && AND cStatus # "SQL ERROR"
		tsubject = "856 Error in TJX At WO "+ALLTRIM(STR(xwo_num))
		tattach = " "
		tsendto = ALLTRIM(tsendtoerr+",pgaidis@fmiint.com")
		tcc = ALLTRIM(tccerr)
		tmessage = "856 Processing for WO# "+ALLTRIM(STR(xwo_num))+" produced this error: "+cStatus+CHR(13)+"...Check EDI_TRIGGER and re-run"
		DO FORM m:\dev\frm\dartmail2 WITH tsendto,tfrom,tsubject,tcc,tattach,tmessage,"A"
	ENDIF
ENDPROC