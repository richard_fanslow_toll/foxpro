lparameters xformname, xwo_num, xaccountid, xmessage, xdetails, xcontainer, xtrailer, xtruck, xqty, xtableupdate

if !glivedata
	return
endif

local m.formname, m.wo_num, m.accountid, m.message, m.details, m.qty, m.container, m.trailer, m.truck, m.zdate, m.zdatetime

if type("guserid")="C"
	xinfouserid=guserid
else
	xinfouserid="???"
endif

if type("goffice")="C"
	m.office=goffice
else
	m.office=""
endif

m.formname=iif(empty(xformname),"",upper(iif(xformname="frm",substr(xformname,4),xformname)))
m.wo_num=iif(empty(xwo_num),0,xwo_num)
m.accountid=iif(empty(xaccountid),0,xaccountid)
m.message=xmessage
m.details=iif(empty(xdetails),"",xdetails)
m.container=iif(empty(xcontainer),"",xcontainer)
m.chassis=""
m.trailer=iif(empty(xtrailer),"",xtrailer)
m.truck=iif(empty(xtruck),"",xtruck)
m.qty=iif(empty(xqty),0,xqty)
m.userid=xinfouserid
m.zdate=qdate()
m.zdatetime=qdate(.t.)
m.system=.f.
m.flag=.f.
m.infoid=x3genpk("info","wo")

if !used("info")
	email("Dyoung@fmiint.com","INFO: info not used",xmessage,,,,.t.,,,,,.t.,,.t.)
	return
endif

insert into info from memvar

**when called from a table trigger, insert is done after tableupdate - mvw 03/28/10
if xtableupdate
	tu("info")
endif
