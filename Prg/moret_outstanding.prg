Close Databases All
lTestMail = .t.
USE F:\3PL\data\trigger945accts IN 0
USE f:\whl\whdata\outship IN 0
WAIT WINDOW "Now selecting records WITH appointments" nowait
SELECT a.appt,a.accountid,b.name,a.wo_num,IIF(!EMPTY(a.bol_no),.t.,.f.) as bol_made,;
	a.scac,a.consignee ;
	FROM outship a,trigger945accts b ;
	WHERE EMPTY(a.del_date) ;
	AND BETWEEN(appt,DATE()-5,DATE()) ;
	AND a.accountid = b.accountid ;
	group BY 2,1,4 ;
	INTO CURSOR temp1 READWRITE

WAIT WINDOW "Now selecting records WITHOUT appointments" nowait
SELECT a.appt,a.accountid,b.name,a.wo_num,IIF(!EMPTY(a.bol_no),.t.,.f.) as bol_made,;
	a.scac,a.consignee ;
	FROM outship a,trigger945accts b ;
	WHERE EMPTY(a.del_date) ;
	AND emptynul(appt);
	AND a.accountid = b.accountid ;
	group BY 2,1,4 ;
	INTO CURSOR temp2
WAIT CLEAR 
SELECT temp1
APPEND blank
APPEND FROM DBF('temp2')
BROWSE

cOutfile = "f:\3pl\data\moret_pending.xls"
COPY TO &cOutfile TYPE xl5
USE IN temp1

	IF lTestmail
	tsendto = "joe.bianchi@tollgroup.com"
	tcc = ""
	ELSE
	tsendto = "tlarson@fmiint.com,lupe@moret.com"
	tcc = "pgaidis@fmiint.com,joe.bianchi@tollgroup.com"
	endif
	tsubject= "Pending Moret Shipments as of "+TTOC(DATETIME())
	tattach = cOutfile
	tmessage = "The attached Excel file contains all pending shipments."
	tmessage = tmessage +CHR(13) + "Includes all with appts first, then those without appts."
	tFrom ="TGF Warehouse Operations <warehouse-ops@fmiint.com>"
	DO FORM m:\dev\frm\dartmail2 WITH tsendto,tFrom,tsubject,tcc,tattach,tmessage,"A"

DELETE FILE &cOutfile
Close Databases All
return