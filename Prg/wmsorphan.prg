* delete orphaned records in WMS

utilsetup("WMSORPHAN")

guserid="WMSORPHAN"
gprocess=guserid
gdevelopment=.f.
xjobno=0

external array xdbf
 
xsqlexec("select * from whoffice",,,"wh")

select whoffice
scan for !test
	xoffice=whoffice.office
	goffice=xoffice
	xfolder=whoffice.folder+"\whdata\"

*	use (xfolder+"acctdet") in 0
*	use (xfolder+"acctgrp") in 0
*	use (xfolder+"apptcall") in 0
*	use (xfolder+"bl") in 0
*	use (xfolder+"bldet") in 0
*	use (xfolder+"blload") in 0
*	use (xfolder+"email") in 0
*	use (xfolder+"emaildet") in 0
*	use (xfolder+"inctn") in 0
*	if usesqlinwo()
*	else
*		use (xfolder+"indet") in 0
*		use (xfolder+"inloc") in 0
*		use (xfolder+"inwolog") in 0
*	endif
*	use (xfolder+"inven") in 0
*	use (xfolder+"invenloc") in 0
*	use (xfolder+"miscwo") in 0
*	use (xfolder+"miscdet") in 0
*	use (xfolder+"outcomp") in 0
*	use (xfolder+"package") in 0
*	use (xfolder+"phys") in 0
*	use (xfolder+"physppu") in 0
*	use (xfolder+"pl") in 0
*	use (xfolder+"plctn") in 0
*	use (xfolder+"project") in 0
*	use (xfolder+"projdet") in 0
*	use (xfolder+"pt") in 0
*	use (xfolder+"ptbak") in 0
*	use (xfolder+"ptdet") in 0
*	use (xfolder+"ptdetbak") in 0
*	use (xfolder+"pull") in 0
*	use (xfolder+"pulldet") in 0
*	use (xfolder+"rcv") in 0
*	use (xfolder+"rcvdet") in 0
*	use (xfolder+"rcvloc") in 0
*	use (xfolder+"routhead") in 0
*	use (xfolder+"routing") in 0
*	use (xfolder+"shipment") in 0
*	use (xfolder+"swcdet") in 0
*	use (xfolder+"swcloc") in 0
*	use (xfolder+"swcstyle") in 0
*	use (xfolder+"ufucc") in 0
*	use (xfolder+"ufintl") in 0

*	orphancheck("acctgrp","acctdet",.t.)
*	orphancheck("outship","apptcall",.t.)
*	orphancheck("bl","bldet",.t.)
*	orphancheck("bl","blload",.t.)
*	orphancheck("email","emaildet",.t.)
*	orphancheck("indet","inctn",.t.)
*	if usesqlinwo()
*	else
*		orphancheck("inwolog","indet",.t.)
*		orphancheck("indet","inloc",.t.)
*	endif
	
*	orphancheck("inven","invenloc",.t.)
*	orphancheck("miscwo","miscdet",.t.)
*	orphancheck("inwolog","pl",.t.)
*	orphancheck("package","ufucc",.t.)
*	orphancheck("phys","physppu",.t.)
*	orphancheck("pl","plctn",.t.)
*	orphancheck("project","projdet",.t.)
*	orphancheck("pt","ptdet",.t.)
*	orphancheck("ptbak","ptdetbak",.t.)
*	orphancheck("pull","pulldet",.t.)
*	orphancheck("rcv","rcvdet",.t.)
*	orphancheck("rcvdet","rcvloc",.t.)
*	orphancheck("routhead","routing",.t.)
*	orphancheck("outwolog","swcdet",.t.)
*	orphancheck("shipment","package",.t.)
*	orphancheck("shipment","ufintl",.t.)
*	orphancheck("shipment","ufucc",.t.)
*	orphancheck("swcdet","swcloc",.t.)
*	orphancheck("swcdet","swcstyle",.t.)

*	use in acctdet
*	use in acctgrp
*	use in apptcall
*	use in bl
*	use in bldet
*	use in blload
*	use in email
*	use in emaildet
*	use in inctn
*	if usesqlinwo()
*	else
*		use in inwolog
*		use in indet
*		use in inloc
*	endif
*	use in inven
*	use in invenloc
*	use in miscwo
*	use in miscdet
*	use in outcomp
*	use in package
*	use in phys
*	use in physppu
*	use in pl
*	use in plctn
*	use in pt
*	use in ptbak
*	use in ptdet
*	use in ptdetbak
*	use in pull
*	use in pulldet
*	use in rcv
*	use in rcvdet
*	use in rcvloc
*	use in routhead
*	use in routing
*	use in shipment
*	use in swcdet
*	use in swcloc
*	use in swcstyle
*	use in ufucc
*	use in ufintl
endscan

*

schedupdate()

_screen.Caption=gscreencaption
on error
