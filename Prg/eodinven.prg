**eodInven.prg
**
**daily snapshot of storage quantities (cartons & pallet positions) per account
**
parameters coffice
**cOffice = "1", "2", "L", "M", "5", "6" or "N"

if !set("default")="M:"
	set sysmenu off
	_screen.caption = "Daily Inven Snapshot"
	_screen.windowstate = 1
endif

xsqlexec("select * from whoffice",,,"wh")

xsqlexec("select * from account where inactive=0","account",,"qq")
index on accountid tag accountid
set order to


open database f:\wh\whall

xfilter="!test"
if !empty(coffice)
	xfilter=xfilter+" and office='"+coffice+"'"
endif

* clear previous data if rerunning  dy 12/21/15

useca("eodinven","wh",,,"eoddt={"+dtoc(date()-1)+"}")
delete all in eodinven
tu()

* loop

select whoffice
scan for &xfilter and !test
	wait window office nowait
	
	coffice=whoffice.office
	goffice=coffice
	cdb="f:\wh"+coffice+"\whdata\wh"
	open database &cdb
	cpnpfilter = "pnp"+whoffice.rateoffice
	gmasteroffice=whoffice.rateoffice

	xsqlexec("select * from inven where mod='"+goffice+"'",,,"wh")
	xsqlexec("select * from invenloc where mod='"+goffice+"'",,,"wh")
	index on invenid tag invenid
	set order to
	
	xsqlexec("select * from whseloc where office='"+gmasteroffice+"'","whseloc",,"wh")
	index on whseloc tag whseloc
	set order to
	
	**filter changed to ignore all "non-inventory" locations for CA - 12/15/05
	cFilter = "!InList(l.whseLoc, 'F', 'CUT', 'CYCLE', '???', 'FLOW', 'STATIC')"
	**remove all "F" locations as they are staged Floor locations
	cAddFilter = Iif(cOffice # "C", "", "and Not (whseLoc = '2' and (!InList(Right(Trim(whseLoc),1),'A','B','C','D','E') or ;
			!InList(Substr(whseLoc,2,1),'A','B','C','D','E')))")

	select l.*, ;
		sum(l.locqty) as cartons, ;
		sum(l.locqty*i.ctncube) as cuft, ;
		0000000000 as skucnt, ;
		0000000000 as pnpskucnt, ;
		0000000000 as unitqty, ;
		0000000000 as unpicked, ;
		0000000000 as unshipped, ;
		0000000 as unitlocs, ;
		0000000 as origpallets ;
		from invenloc l ;
		left join inven i on i.invenid = l.invenid ;
		where l.locqty > 0 and !l.units and &cfilter and !isnull(i.ctncube) &caddfilter ;
		group by l.accountid, l.whseloc ;
	  into cursor csreodinven readwrite
	
	if inlist(coffice,"C","1","2","5","6","7")
		**remove all "F" locations as they are staged Floor locations
		delete for whseloc = "2" and (!inlist(right(trim(whseloc),1),"A","B","C","D","E") or ;
			!inlist(substr(whseloc,2,1),"A","B","C","D","E"))
	endif

	select accountid, ;
		cnt(1) as pallets, ;
		sum(cartons) as cartons, ;
		sum(cuft) as cuft, ;
		skucnt, ;
		pnpskucnt, ;
		unitqty, ;
		unpicked, ;
		unshipped, ;
		unitlocs, ;
		origpallets ;
		from csreodinven ;
		group by accountid ;
	  into cursor csreodinven readwrite

	**because pnp works differently in FL (no cartons associated with the units), need to get unit/sku cnts differently

	if coffice = "M"
		select l.*, ;
			i.units as invunits ;
			from invenloc l ;
			left join inven i on i.invenid = l.invenid ;
			where l.locqty # 0 and iif(l.units, .t., &cfilter) and !isnull(i.ctncube) ;
		  into cursor xtemp
	else
		select l.*, ;
			i.units as invunits ;
			from invenloc l ;
			left join inven i on i.invenid = l.invenid ;
			left join account a on a.accountid = i.accountid ;
			where l.locqty # 0 and iif(a.&cpnpfilter, l.units, !l.units and &cfilter &caddfilter) and !isnull(i.ctncube) ;
		  into cursor xtemp
	endif
	
	select *, .f. as pnp, ;
		sum(iif(invunits, locqty, 0)) as unitqty, ;
		sum(locqty) as totqty ;
		from xtemp ;
		group by accountid,style,color,id,pack ;
	  into cursor csreodunits readwrite

	scan
		upcmastsql(csreodunits.accountid,csreodunits.style,csreodunits.color,csreodunits.id,,"pnp=1")
		if found()
			replace pnp with .t. in csreodunits 
		endif
	endscan

	select accountid, ;
		cnt(1) as skucnt, ;
		sum(iif(pnp,1,0)) as pnpskucnt, ;
		sum(unitqty) as unitqty ;
		from csreodunits ;
		group by accountid ;
		where totqty > 0 ;
	  into cursor csreodunits

	insert into csreodinven (accountid, skucnt, pnpskucnt, unitqty) select * from csreodunits

	select * ;
		from xtemp ;
		group by whseloc ;
		where units and !inlist(whseloc,'RACK','CYCLE','PSTAT','PFLOW','???','PP','NSTAT','ASTAT','BULK','XRACK') ;
	  into cursor csreodunits
	select accountid, ;
		cnt(1) as unitlocs ;
		from csreodunits ;
		group by accountid ;
	  into cursor csreodunits

	insert into csreodinven (accountid, unitlocs) select * from csreodunits

	**need to also take into acct pulled/unshipped freight - mvw 06/01/09
	**needed to separate the pulled/unshipped to 2 separate columns, pulled/unpicked & picked/unshipped - mvw 09/29/09

	xsqlexec("select outship.accountid, picked, totqty from outship, outdet " + ;
		"where outship.outshipid=outdet.outshipid and outship.mod='"+goffice+"' " + ;
		"and del_date={} and pulled=1 and units=1 and notonwip=0","xdytemp",,"wh")

	select accountid, sum(iif(emptynul(picked),totqty,0)) as unpicked, sum(iif(!empty(picked),totqty,0)) as unshipped ;
		from xdytemp group by accountid into cursor xout
	
	insert into csreodinven (accountid, unpicked, unshipped) select * from xout
	*****

	do case
	**for MJ (Mod J), need to add pallet positions for all non-shelf (start with "2S", "2V") and 
	**	non-garment-on-hanger (start with "2H") locs, as everything is kept in units - mvw 02/01/12
	case coffice="J"
		select csreodinven
		replace all pallets with 0

		select * from invenloc where !inlist(whseloc,"2S","2V","2H") and locqty>0 group by accountid, whseloc into cursor xtemp
		select accountid, cnt(1) as pallets from xtemp group by accountid into cursor xtemp
		insert into csreodinven (accountid, pallets) select * from xtemp
		use in xtemp

	**for Ariat, need to add decking (unit pallet locs) to pallet count - mvw 07/07/15
	case coffice="K"
		select i.*, loctype from invenloc i left join whseloc w on w.whseloc=i.whseloc where i.accountid=6532 and i.locqty>0 group by i.whseloc into cursor xtemp
		count to xpallets for loctype='DECKING'
		insert into csreodinven (accountid, pallets) values (6532,xpallets)
		use in xtemp

	**for Cache (Mod I), need to calculate pallet count differently. For the pushback racks (each location is 4 deep), 
	**	we need to check inloc to determine how many pallet id's exist in a given location - mvw 02/01/12  removed dy 1/15/17
*!*		case coffice="I"
*!*			select csreodinven
*!*			replace all pallets with 0 for accountid = 6179

*!*			select 001 as pallets, i.*, w.deep from invenloc i left join whseloc w on w.whseloc=i.whseloc ;
*!*				where i.accountid = 6179 and !i.units and locqty>0  group by i.accountid, i.whseloc into cursor xpallet readwrite
*!*			scan for deep>1
*!*				select * from inloc where accountid=6179 and whseloc=xpallet.whseloc and remainqty>0 ;
*!*					group by palletid into cursor xtemp
*!*				replace pallets with iif(reccount("xtemp")>0,reccount("xtemp"),1) in xpallet
*!*				use in xtemp
*!*			endscan

*!*			select accountid, sum(pallets) as pallets from xpallet group by accountid into cursor xpallet
*!*			insert into csreodinven (accountid, pallets) select * from xpallet
*!*			use in xpallet

	**for Moret (mod E), need to calculate pallet count using below code to take into acct pushback locations, etc... also 
	**  need to take into acct staged freight as well. will populate pallets field for only 1 moret acct - mvw 09/26/12
	**changed to break out by account, see code below - mvw 10/11/12
	case coffice="7"
		select csreodinven
		**keep track of original pallet count type in "origpallets" in case need that count as well - mvw 09/26/12
		replace all origpallets with pallets, pallets with 0

		xsqlexec("select * from adj where mod='"+goffice+"' and units=0 and remainqty>0",,,"wh")

		xsqlexec("select whseloc, palletid, inloc.remainqty, indet.accountid from indet, inloc " + ;
			"where indet.indetid=inloc.indetid and indet.mod='"+goffice+"' and units=0 and inloc.remainqty>0","xtemp",,"wh")

		select whseloc, palletid, remainqty, accountid from xtemp ;
		union select whseloc, palletid, remainqty, accountid from adj order by whseloc into cursor xtemp
			
		select whseloc, padr(palletid,10) as palletid, sum(remainqty) as qty, space(11) as loctype, accountid from xtemp group by 1,2 into cursor xrpt readwrite
			
		* get staged pallets

		xsqlexec("select outwologid, swcdetid, swcnum, swcover, bol_no, pt from swcdet where mod='"+coffice+"' and wo_date>{"+dtoc(date()-270)+"}",,,"wh")
		xsqlexec("select swcdetid, locqty, stageloc, palletnum, palletid from swcloc where mod='"+coffice+"' and wo_date>{"+dtoc(date()-270)+"}",,,"wh")

		xsqlexec("select * from outwolog where mod='"+coffice+"' and wo_date>{"+dtoc(date()-60)+"}",,,"wh")
		index on outwologid tag outwologid
		set order to
		
		select stageloc as whseloc, locqty as qty, swcnum, swcover, bol_no, outwolog.accountid, pt, ;
			swcnum+trans(palletnum,"@L 999") as palletid, palletnum ;
			from outwolog, swcdet, swcloc ;
			where outwolog.outwologid=swcdet.outwologid and swcdet.swcdetid=swcloc.swcdetid ;
			and outwolog.wo_date>date()-60 ;
			into cursor xrpt2 readwrite

		scan for !empty(bol_no)
			if xsqlexec("select * from bl where mod='"+coffice+"' and bol_no='"+xrpt2.bol_no+"'",,,"wh") # 0
				if xsqlexec("select * from blload where mod='"+coffice+"' and swcnum='"+xrpt2.swcnum+"' and palletnum="+transform(xrpt2.palletnum),,,"wh") # 0
					if blload.completed
						delete in xrpt2
					endif
				endif
			endif
		endscan

		select xrpt2
		scan for !empty(bol_no)
			if xsqlexec("select * from outship where mod='"+goffice+"' and bol_no='"+xrpt2.bol_no+"' " + ;
				"and (del_date#{} or notonwip=1)",,,"wh") # 0
				
				delete in xrpt2
			endif
		endscan
 
		select xrpt2
		scan
			if xsqlexec("select * from outship where mod='"+goffice+"' " + ;
				"and accountid="+transform(xrpt2.accountid)+" and ship_ref='"+left(xrpt2.pt,20)+"'",,,"wh") # 0
				
				if (!emptynul(outship.del_date) or outship.notonwip)
					delete in xrpt2
				endif
			else
				if "/"$xrpt2.pt and xsqlexec("select * from outship where mod='"+goffice+"' " + ;
					"and accountid="+transform(xrpt2.accountid)+" and ship_ref='"+padr(left(xrpt2.pt,at("/",xrpt2.pt)-1),20)+"'",,,"wh") # 0

					if (!emptynul(outship.del_date) or outship.notonwip)
						delete in xrpt2
					endif
				else
					if "-"$xrpt2.pt and xsqlexec("select * from outship where mod='"+goffice+"' " + ;
						"and accountid="+transform(xrpt2.accountid)+" and ship_ref='"+padr(left(xrpt2.pt,at("-",xrpt2.pt)-1),20)+"'",,,"wh") # 0
					
						if (!emptynul(outship.del_date) or outship.notonwip)
							delete in xrpt2
						endif
					endif
				endif
			endif
		endscan

		select xrpt2
		scan 
			if xsqlexec("select * from outship where mod='"+goffice+"' and swcnum='"+xrpt2.swcnum+"' " + ;
				"and swcover='"+xrpt2.swcover+"' and del_date#{}",,,"wh") # 0
				
				delete in xrpt2
			endif
		endscan
		
		select xrpt2
		scan 
			scatter memvar
			insert into xrpt from memvar
		endscan

		* remove multiple blank pallet IDs

		select xrpt
		scan for empty(palletid)
			xrecno=recno()
			xwhseloc=whseloc
			xqty=qty
			
			select xrpt
			locate for whseloc=xwhseloc and !empty(palletid)
			if found()
				replace qty with xrpt.qty+xqty in xrpt
				go (xrecno) in xrpt
				delete in xrpt
			else
				go (xrecno) in xrpt
			endif
		endscan

		* fill in loctype

		select xrpt
		scan 
			if seek(xrpt.whseloc,"whseloc","whseloc")
				replace loctype with whseloc.loctype in xrpt
			endif
		endscan

		* determine # of pallets based on location type 

		select whseloc, 999 as numpall, loctype, accountid from xrpt ;
			where loctype#"NOT PALLET" group by 1 into cursor xloc readwrite

		scan 
			do case
			case loctype="2 PUSHBACK"
				select xrpt
				count to aa for whseloc=xloc.whseloc
				replace numpall with min(2,aa) in xloc
			case loctype="4 PUSHBACK"
				select xrpt
				count to aa for whseloc=xloc.whseloc
				replace numpall with min(4,aa) in xloc
			case loctype="FLOOR"
				select xrpt
				count to aa for whseloc=xloc.whseloc
				replace numpall with aa in xloc
			otherwise
				replace numpall with 1 in xloc
			endcase
		endscan

		**changed to break out by account - mvw 10/11/12
*!*			select xloc
*!*			sum numpall to xpallets

*!*			**only place pallet count in for 1 moret acct
*!*			xaccountid=val(left(gmoretacctlist,at(",",gmoretacctlist)-1))
*!*			insert into csreodinven (accountid, pallets) values (xaccountid, xpallets)

		select accountid, sum(numpall) as pallets from xloc group by accountid into cursor xpallet

		insert into csreodinven (accountid, pallets) select * from xpallet
		use in xpallet

		use in xtemp
		use in xloc
		use in xrpt
		use in xrpt2
	endcase

	select date()-1 as eoddt, ;
		accountid, ;
		sum(pallets) as pallets, ;
		sum(cartons) as cartons, ;
		sum(cuft) as cuft, ;
		sum(skucnt) as skucnt, ;
		sum(pnpskucnt) as pnpskucnt, ;
		sum(unitqty) as unitqty, ;
		sum(unpicked) as unpicked, ;
		sum(unshipped) as unshipped, ;
		sum(unitlocs) as unitlocs, ;
		sum(origpallets) as origpallets ;
		from csreodinven ;
		group by accountid ;
	  into cursor csreodinven

	select eodinven
	scatter memvar memo blank
	
	m.office=gmasteroffice
	m.mod=coffice
	
	select csreodinven		
	scan
		scatter memvar
		m.units=m.unitqty
		m.eodinvenid=sqlgenpk("eodinven","wh")
		insert into eodinven from memvar
	endscan

	tu("eodinven")

	set database to wh
	close data
	
	if used("inven")
		use in inven
	endif
	if used("invenloc")
		use in invenloc
	endif
	if used("outwolog")
		use in outwolog
	endif
	if used("outship")
		use in outship
	endif
	if used("outdet")
		use in outdet
	endif
	if used("indet")
		use in indet
	endif
	if used("inloc")
		use in inloc
	endif
	if used("adj")
		use in adj
	endif
	if used("swcdet")
		use in swcloc
	endif
	if used("swcdet")
		use in swcdet
	endif
	if used("bl")
		use in bl
	endif
	if used("blload")
		use in blload
	endif
endscan

close databases all

gunshot()