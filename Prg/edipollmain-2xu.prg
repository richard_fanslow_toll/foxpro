*runack("EDIPOLLER_2XU	")

CLOSE DATABASES ALL
CLEAR ALL
RELEASE ALL
cPath = ["M:\DEV\PRG; M:\DEV\PROJ"]
SET PATH TO &cPath ADDITIVE

WITH _SCREEN
	.AUTOCENTER = .T.
	.WINDOWSTATE = 0
	.BORDERSTYLE = 1
	.WIDTH = 260
	.HEIGHT = 130
	.TOP = 850
	.LEFT = 660
	.CLOSABLE = .F.
	.MAXBUTTON = .F.
	.MINBUTTON = .F.
	.SCROLLBARS = 0
	.CAPTION = "2XU POLLER - EDI"
ENDWITH
TRY
	DO m:\dev\prg\_setvars
	PUBLIC lSkipError
	lSkipError =  .F.
	SET STATUS BAR OFF
	SET SYSMENU OFF
	ON ESCAPE CANCEL
	ON ERROR THROW
	SELECT 0
	USE F:\3pl\DATA\edipollertrip SHARED ALIAS e2xu
	REPLACE e2xu._2xu WITH .F.
	USE IN e2xu

	SELECT 0
	USE F:\3pl\DATA\last945time SHARED ALIAS _2xulast
	REPLACE _2xulast.checkflag WITH .T. FOR poller = "2XU" IN _2xulast
	REPLACE _2xulast.tries WITH 0 FOR poller = "2XU" IN _2xulast
	USE IN _2xulast
	ASSERT .F. MESSAGE "Going into poller run"
	DO FORM m:\dev\frm\edi_jobhandler_2xu

CATCH TO oErr
	ASSERT .F. MESSAGE "AT CATCH SECTION"
	IF !lSkipError
		tfrom    ="TGF EDI Processing Center <tgf-edi-ops@fmiint.com>"
		tsubject = "2XU Outbound EDI Poller Error ("+TRANSFORM(oErr.ERRORNO)+") at "+TTOC(DATETIME())
		tattach  = ""
		SELECT 0
		USE F:\3pl\DATA\mailmaster ALIAS mm
		LOCATE
		LOCATE FOR mm.edi_type = 'MISC' AND mm.taskname = "GENERAL"
		tsendto = IIF(mm.use_alt,sendtoalt,sendto)
		tcc = IIF(mm.use_alt,ccalt,cc)
		USE IN mm

		tmessage = "2XU 945 Poller Major Error..... Please fix me........!"
		lcSourceMachine = SYS(0)

		tmessage =tmessage+ "Try/CATCH Exeception Message:"+CHR(13)+;
			[  Error: ] + STR(oErr.ERRORNO) +CHR(13)+;
			[  LineNo: ] + STR(oErr.LINENO) +CHR(13)+;
			[  Message: ] + oErr.MESSAGE +CHR(13)+;
			[  Procedure: ] + oErr.PROCEDURE +CHR(13)+;
			[  Details: ] + oErr.DETAILS +CHR(13)+;
			[  StackLevel: ] + STR(oErr.STACKLEVEL) +CHR(13)+;
			[  LineContents: ] + oErr.LINECONTENTS+CHR(13)+;
			[  UserValue: ] + oErr.USERVALUE+CHR(13)+;
			[  Computer:  ] +lcSourceMachine+CHR(13)
		DO FORM m:\dev\FRM\dartmail2 WITH tsendto,tfrom,tsubject,tcc,tattach,tmessage,"A"
	ENDIF
	SELECT 0
	USE F:\3pl\DATA\edipollertrip SHARED ALIAS e2xu
	REPLACE e2xu._2xu WITH .T. IN e2xu
FINALLY
	CLOSE DATABASES ALL
	SET STATUS BAR ON
	ON ERROR
	SET LIBRARY TO 
ENDTRY
