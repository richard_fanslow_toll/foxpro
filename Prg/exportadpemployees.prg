* export new employees from ADP into Foxpro

runack("EAE")

* create EXE in F:\HR\

LOCAL loExportNewADPEmployees
*!*	IF NOT INLIST(UPPER(ALLTRIM(GETENV("COMPUTERNAME"))),"MBENNETT","HR1") THEN
*!*		=MESSAGEBOX("The Employee Export must be run from Lucille's PC!",0+16,"Export FoxPro Employees")
*!*		RETURN
*!*	ENDIF
loExportNewADPEmployees = CREATEOBJECT('ExportNewADPEmployees')
loExportNewADPEmployees.MAIN()
CLOSE DATABASES ALL
RETURN

#DEFINE LOGIT 1
#DEFINE WAITIT 2
#DEFINE NOWAITIT 4
#DEFINE SENDIT 8
#DEFINE CRLF CHR(13) + CHR(10)
#DEFINE RETURN_DATA_MANDATORY .F.
#DEFINE RETURN_DATA_NOT_MANDATORY .T.
#DEFINE CSVFORMAT 6
#DEFINE WRITE_ONLY_UNBUFFERED 11
#DEFINE COMMA ","
#DEFINE EXPRESSDIVISION 2
#DEFINE EXPRESSDEPT 655

DEFINE CLASS ExportNewADPEmployees AS CUSTOM

	cProcessName = 'ExportNewADPEmployees'

	lTestMode = .F.  && .F. FOR NORMAL OPERATION, .T. TO SUPPRESS ALL BUT DEVELOPER EMAILS AND CHANGE INPUT TABLE, ETC.

	lAutoYield = .T.

	cStartTime = TTOC(DATETIME())

	* Data Table properties
	cFoxProEmployeeTable = "F:\HR\HRDATA\EMPLOYEE"

	* email properties
	cExportedList = ''

	* connection properties
	nSQLHandle = 0

*!*		* file properties
*!*		nFileHandle = 0
*!*		cHeaderLine = ;
*!*			'Co Code,' + ;
*!*			'File #,' + ;
*!*			'Social Security Number,' + ;
*!*			'Employee Last Name,' + ;
*!*			'Employee First Name,' + ;
*!*			'Address Line 1,' + ;
*!*			'Address Line 2,' + ;
*!*			'City,' + ;
*!*			'State Postal Code,' + ;
*!*			'Zip Code,' + ;
*!*			'Gender,' + ;
*!*			'Home Department,' + ;
*!*			'Rate Type,' + ;
*!*			'Rate 1 Amount,' + ;
*!*			'Hire Status,Hire Date,' + ;
*!*			'Termination Date,' + ;
*!*			'Birth Date'

	* wait window properties
	nWaitWindowTimeout = 2
	lWaitWindowIsOn = .T.

	* logfile properties
	lLoggingIsOn = .T.
	cLogFile = 'F:\UTIL\ADPEDI\Logfiles\ExportADPEmployees_log.txt'

	* INTERNAL email properties
	lSendInternalEmailIsOn = .T.
	cFrom = 'FMI Corporate <fmicorporate@fmiint.com>'
	*cSendTo = 'mbennett@fmiint.com'
	cSendTo = 'lwaldrip@fmiint.com, Marie.Freiberger@tollgroup.com, Lauren.Klaver@tollgroup.com, mbennett@fmiint.com'
	cCC = ''
	cSubject = 'EXPORT NEW ADP EMPLOYEES TO FOXPRO, Results for: ' + TTOC(DATETIME())
	cAttach = ''
	cBodyText = ''
	
	* Express properties
	cExpressSendTo = 'don.kistner@tollgroup.com, michael.divirgilio@tollgroup.com, ' + ;
		'cecilia.beard@tollgroup.com, charvey@fmiint.com, fran.castro@tollgroup.com'
	cExpressCC = 'lucille.waldrip@tollgroup.com, Marie.Freiberger@tollgroup.com, Lauren.Klaver@tollgroup.com, mbennett@fmiint.com'
	cExpressHireList = ''
	cExpressSubject = 'New Express Employees'
	*

	cCOMPUTERNAME = ''
	cUSERNAME = ''

	FUNCTION INIT
		IF NOT DODEFAULT()
			RETURN .F.
		ENDIF
		WITH THIS
			*SET RESOURCE OFF
			CLOSE DATA
			SET CENTURY OFF
			SET DATE AMERICAN
			SET HOURS TO 24
			SET ANSI ON
			SET TALK OFF
			SET DELETED ON
			SET CONSOLE OFF
			SET EXCLUSIVE OFF
			SET SAFETY OFF
			SET EXACT OFF
			SET STATUS BAR OFF
			SET SYSMENU OFF
			*SET ENGINEBEHAVIOR 70
			SET MULTILOCKS ON
			_VFP.AUTOYIELD = .lAutoYield
			.cCOMPUTERNAME = GETENV("COMPUTERNAME")
			.cUSERNAME = GETENV("USERNAME")
			IF .lTestMode THEN
				.cSendTo = 'mbennett@fmiint.com'
				.cExpressSendTo = 'mbennett@fmiint.com'
				.cExpressCC = ''
				.cLogFile = 'F:\UTIL\ADPEDI\Logfiles\ExportADPEmployees_log_TESTMODE.txt'
				*.cFoxProEmployeeTable = "F:\UTIL\ADPEDI\TESTDATA\EMPLOYEE"
			ENDIF
			.lLoggingIsOn = .lLoggingIsOn AND NOT EMPTY(.cLogFile)
			IF .lLoggingIsOn THEN
				SET ALTERNATE TO (.cLogFile) ADDITIVE
				SET ALTERNATE ON
			ENDIF
			SET PROCEDURE TO VALIDATIONS ADDITIVE
		ENDWITH
	ENDFUNC


	FUNCTION DESTROY
		WITH THIS
			IF .lLoggingIsOn  THEN
				SET ALTERNATE OFF
				SET ALTERNATE TO
			ENDIF
		ENDWITH
		DODEFAULT()
	ENDFUNC


	FUNCTION MAIN
		WITH THIS
			LOCAL lnNumberOfErrors, lcSQL, loError, lcTopBodyText, ldBlankDate, ltBlankDateTime, llValid
			LOCAL ldHireDate, ldTempHireDate, ldNewDate, lcImport, lcPhone, lcOldCompany, lcExpressID
			TRY
				lnNumberOfErrors = 0
				lcTopBodyText = ''
				ldBlankDate = CTOD('')
				ltBlankDateTime = DTOT(ldBlankDate)

				.TrackProgress('', LOGIT)
				.TrackProgress('', LOGIT)
				.TrackProgress('=========================================================', LOGIT+SENDIT)
				.TrackProgress('EXPORT NEW ADP EMPLOYEES process started....', LOGIT+SENDIT+NOWAITIT)
				.TrackProgress('COMPUTERNAME = ' + .cCOMPUTERNAME, LOGIT+SENDIT)
				.TrackProgress('USERNAME = ' + .cUSERNAME, LOGIT+SENDIT)
				.TrackProgress('=========================================================', LOGIT+SENDIT)
				IF .lTestMode THEN
					.TrackProgress('', LOGIT)
					.TrackProgress('=========================================================', LOGIT+SENDIT)
					.TrackProgress('                        TEST MODE                        ', LOGIT+SENDIT)
					.TrackProgress('=========================================================', LOGIT+SENDIT)
				ENDIF
				.TrackProgress('', LOGIT)
				.TrackProgress('=========================================================', LOGIT+SENDIT)
				.TrackProgress('EMPLOYEE TABLE = ' + .cFoxProEmployeeTable, LOGIT+SENDIT)
				.TrackProgress('=========================================================', LOGIT+SENDIT)
				.TrackProgress('', LOGIT)

				* get ss#s from ADP
				OPEN DATABASE F:\UTIL\ADPCONNECTION\CONNECT1

				.nSQLHandle = SQLCONNECT("PCPAYWIN","MBENNETT","VP737A")
				
				lcWhere = " WHERE STATUS = 'A' "  && standard case
				*lcWhere = " WHERE FILE# = 3750 "    && use this to force export for a terminated ee


				IF .nSQLHandle > 0 THEN

					lcSQL = ;
						"SELECT " + ;
						" {fn IFNULL(AREACODEPHONE#,' ')} AS PHONE, " + ;
						" BIRTHDATE AS DOB, " + ;
						" {fn IFNULL(CITY,' ')} AS CITY, " + ;
						" {fn IFNULL(COMPANYCODE,' ')} AS ADP_COMP, " + ;
						" {fn IFNULL(EEOCJOBCLASS,' ')} AS CLASSIF, " + ;
						" {fn IFNULL(CUSTAREA3,' ')} AS WORKSITE, " + ;
						" {fn IFNULL(FILE#,0000)} AS EMP_NUM, " + ;
						" {fn IFNULL(FIRSTNAME,' ')} AS FIRST, " + ;
						" {fn IFNULL(LASTNAME,' ')} AS LAST, " + ;
						" {fn IFNULL(GENDER,' ')} AS SEXTYPE, " + ;
						" HIREDATE AS HIRE_DATE, " + ;
						" HIREDATE AS VHIRE_DATE, " + ;
						" {fn IFNULL(HOMEDEPARTMENT,'??????')}  AS HOMEDEPARTMENT, " + ;
						" {fn IFNULL(NAME,' ')} AS EMPLOYEE, " + ;
						" {fn IFNULL(RATE1AMT,0.00)} AS SALARY, " + ;
						" SOCIALSECURITY# AS SS_NUM, " + ;
						" {fn IFNULL(STATE,' ')} AS STATE, " + ;
						" {fn IFNULL(STREETLINE1,' ')} AS STREETLINE1, " + ;
						" {fn IFNULL(STREETLINE2,' ')} AS STREETLINE2, " + ;
						" {fn IFNULL(ASSIGNEDSHIFT,' ')} AS SHIFT, " + ;
						" {fn IFNULL(ZIPCODE,' ')} AS ZIP " + ;
						" FROM REPORTS.V_EMPLOYEE " + ;
						lcWhere + ;
						" AND COMPANYCODE IN ('E87','E88','E89') " + ;
						" ORDER BY FILE# "

					IF USED('SQLCURSOR1') THEN
						USE IN SQLCURSOR1
					ENDIF

					IF .ExecSQL(lcSQL, 'SQLCURSOR1', RETURN_DATA_MANDATORY) THEN


*!*	SELECT SQLCURSOR1
*!*	BROWSE
*!*	X = 2*.F.

						* format ss#, etc.
						* TTOD(DOB, HIRE_DATE, VHIRE_DATE)
						* DIVISION, DEPT from HOMEDEPARTMENT
						* ADDRESS = ALLTRIM(STREETLINE1) + ALLTRIM(STREETLINE2)

						IF USED('SQLCURSOR2') THEN
							USE IN SQLCURSOR2
						ENDIF
						IF USED('SQLCURSOR3') THEN
							USE IN SQLCURSOR3
						ENDIF

						.TrackProgress('Formatting ADP data....', LOGIT+SENDIT+NOWAITIT)
						SELECT ;
							INT(EMP_NUM) AS EMP_NUM, ;
							TTOD(NVL(DOB,ltBlankDateTime)) AS DOB, ;
							TTOD(NVL(HIRE_DATE,ltBlankDateTime)) AS HIRE_DATE, ;
							TTOD(NVL(VHIRE_DATE,ltBlankDateTime)) AS VHIRE_DATE, ;
							INT(VAL(LEFT(HOMEDEPARTMENT,2))) AS DIVISION, ;
							INT(VAL(SUBSTR(HOMEDEPARTMENT,3))) AS DEPT, ;
							(LEFT(SS_NUM,3) + "-" + SUBSTR(SS_NUM,4,2) + "-" + SUBSTR(SS_NUM,6)) AS SS_NUM, ;
							IIF(INLIST(ADP_COMP,"E88","E89"),0.00,SALARY) AS SALARY, ;
							UPPER((ALLTRIM(STREETLINE1) + ALLTRIM(STREETLINE2))) AS ADDRESS, ;
							VAL(CLASSIF) AS CLASSIF, ;
							(STRTRAN(PHONE," ","") + " ") AS PHONE, ;
							UPPER(CITY) AS CITY, ;
							LEFT(UPPER(ALLTRIM(ADP_COMP)),3) AS ADP_COMP, ;
							WORKSITE, ;
							UPPER(FIRST) AS FIRST, ;
							UPPER(LAST) AS LAST, ;
							SEXTYPE, ;
							UPPER(EMPLOYEE) AS EMPLOYEE, ;
							STATE, ;
							ZIP, ;
							SHIFT, ;
							ldBlankDate AS TOBEINSUR, ;
							ldBlankDate AS LIFE, ;
							ldBlankDate AS elig401K, ;
							.T. AS ACTIVE, ;
							.T. AS VALID, ;
							.F. AS checked ;
							FROM SQLCURSOR1 ;
							INTO CURSOR SQLCURSOR2 ;
							READWRITE
							
							
						* CONVERT NEW COMPANYCODES LIKE E87, E88, E89 BACK TO SXI ZXU ETC.
						SELECT SQLCURSOR2 
						SCAN
							lcOldCompany = GetOldADPCompany(SQLCURSOR2.ADP_COMP)
							IF SQLCURSOR2.ADP_COMP <> lcOldCompany THEN
								REPLACE SQLCURSOR2.ADP_COMP WITH lcOldCompany
							ENDIF
						ENDSCAN

						.TrackProgress('Identifying Active ADP employees not Active in FoxPro....', LOGIT+SENDIT+NOWAITIT)
						* Open employee table
						IF USED('EMPLOYEE') THEN
							USE IN EMPLOYEE
						ENDIF

						USE (.cFoxProEmployeeTable) AGAIN IN 0 ALIAS EMPLOYEE

						* get sub selection where file# not in FoxPro
						SELECT * ;
							FROM SQLCURSOR2 ;
							INTO CURSOR SQLCURSOR3 ;
							WHERE EMP_NUM NOT IN (SELECT EMP_NUM FROM EMPLOYEE WHERE ACTIVE) ;
							READWRITE

						IF NOT USED('SQLCURSOR3') OR EOF('SQLCURSOR3') THEN
							.TrackProgress('There were no new ADP employees to import!', LOGIT+SENDIT+NOWAITIT)
						ELSE
							* list the ADP employees not in FoxPro
							.TrackProgress('', LOGIT+SENDIT)
							.TrackProgress('Active ADP Employees not Active in FoxPro: ', LOGIT+SENDIT)
							SELECT SQLCURSOR3
							SCAN
								.TrackProgress('     File#: ' + TRANSFORM(SQLCURSOR3.EMP_NUM) + ',  Name: ' + ALLTRIM(SQLCURSOR3.EMPLOYEE) + ;
								',  Division: ' + PADL(SQLCURSOR3.DIVISION,2,'0'), LOGIT+SENDIT)
							ENDSCAN
							GOTO TOP
							.TrackProgress('', LOGIT+SENDIT)

							* display grid letting user confirm imports
							* needed because there might be employees terminated
							* in FoxPro but still active (in error) in ADP,
							* and we don't want to just automatically import them
							* back into FoxPro.

							* format phone #s so they show properly in Employee form, which has inputmask of (999)999-9999
							SELECT SQLCURSOR3
							lcPhone = SQLCURSOR3.PHONE
							SCAN
								IF ALLTRIM(SQLCURSOR3.PHONE) = "()" THEN
									lcPhone	= ""
								ELSE
									* phone from adp should now be in format (999)9999999; need to insert the "-"
									lcPhone	= LEFT(SQLCURSOR3.PHONE,8) + "-" + SUBSTR(SQLCURSOR3.PHONE,9,4)
								ENDIF
								REPLACE SQLCURSOR3.PHONE WITH 	lcPhone
							ENDSCAN
							
*!*							IF .lTestMode then
*!*								SELECT SQLCURSOR3
*!*								brow
*!*							endif

							SELECT SQLCURSOR3
							GOTO TOP
							*!*								BROWSE FIELDS EMPLOYEE, PHONE

							* this form allows user to edit the "checked" field in SQLCURSOR3
							DO FORM CONFIRMEXPORTS

							* get sub selection where user checked IMPORT on form
							SELECT * ;
								FROM SQLCURSOR3 ;
								INTO CURSOR SQLCURSOR4 ;
								WHERE checked ;
								READWRITE

							IF NOT USED('SQLCURSOR4') OR EOF('SQLCURSOR4') THEN
								.TrackProgress('No new ADP employees were selected for import!', LOGIT+SENDIT+NOWAITIT)
							ELSE
								* proceed

								.TrackProgress('Calculating Insurance and 401k dates....', LOGIT+SENDIT+NOWAITIT)
								* do other field calcs as when Hire_date entered in employee.scx
								IF NOT USED('HR401K') THEN
									USE F:\HR\HRDATA\HR401K IN 0 ALIAS HR401K
								ENDIF
								SELECT SQLCURSOR4
								SCAN
									ldHireDate = SQLCURSOR4.HIRE_DATE
									ldTempHireDate = SQLCURSOR4.HIRE_DATE

									* new date is the first of the month after they have worked 4 full months
									IF DAY(SQLCURSOR4.HIRE_DATE) = 1 THEN
										* just add 4 months to hire date
										ldNewDate = GOMONTH(SQLCURSOR4.HIRE_DATE,4)
									ELSE
										* go to the first of the next month
										ldNewDate = GOMONTH(SQLCURSOR4.HIRE_DATE,1)
										ldNewDate = ldNewDate - DAY(ldNewDate) + 1
										* then add 4 months
										ldNewDate = GOMONTH(ldNewDate,4)
									ENDIF

									SELECT HR401K
									GOTO TOP
									LOCATE FOR BETWEEN(ldHireDate,low,HIGH)
									IF FOUND() THEN
										REPLACE SQLCURSOR4.elig401K WITH HR401K.eligable
									ELSE
										.TrackProgress('WARNING: 401k Elig. Date not found for employee # ' + TRANSFORM(SQLCURSOR4.EMP_NUM), LOGIT+SENDIT)
									ENDIF
									
									SELECT HR401K
									LOCATE
									
									REPLACE SQLCURSOR4.TOBEINSUR WITH ldNewDate, SQLCURSOR4.LIFE WITH ldNewDate
								ENDSCAN

								* validate records to be processed, logging errors
								SELECT SQLCURSOR4
								SCAN
									llValid = .T.
									IF EMPTY(SQLCURSOR4.EMPLOYEE) THEN
										.TrackProgress('Missing Name for employee # ' + TRANSFORM(SQLCURSOR4.EMP_NUM), LOGIT+SENDIT)
										llValid = .F.
									ENDIF
									IF EMPTY(SQLCURSOR4.EMP_NUM) THEN
										.TrackProgress('Missing File# for employee: ' + SQLCURSOR4.EMPLOYEE, LOGIT+SENDIT)
										llValid = .F.
									ENDIF
									IF EMPTY(SQLCURSOR4.DOB) THEN
										.TrackProgress('Missing DOB for employee: ' + SQLCURSOR4.EMPLOYEE, LOGIT+SENDIT)
										llValid = .F.
									ENDIF
									IF EMPTY(SQLCURSOR4.HIRE_DATE) THEN
										.TrackProgress('Missing HIRE_DATE for employee: ' + SQLCURSOR4.EMPLOYEE, LOGIT+SENDIT)
										llValid = .F.
									ENDIF
									IF NOT IsValidNumericDivision(SQLCURSOR4.DIVISION) THEN
										.TrackProgress('Invalid DIVISION [' + TRANSFORM(SQLCURSOR4.DIVISION) + '] for employee: ' + SQLCURSOR4.EMPLOYEE, LOGIT+SENDIT)
										llValid = .F.
									ENDIF
									IF EMPTY(SQLCURSOR4.DEPT) THEN
										.TrackProgress('Missing DEPT for employee: ' + SQLCURSOR4.EMPLOYEE, LOGIT+SENDIT)
										llValid = .F.
									ENDIF
									IF EMPTY(SQLCURSOR4.SS_NUM) THEN
										.TrackProgress('Missing SS_NUM for employee: ' + SQLCURSOR4.EMPLOYEE, LOGIT+SENDIT)
										llValid = .F.
									ENDIF
									IF EMPTY(SQLCURSOR4.CLASSIF) THEN
										.TrackProgress('Missing CLASSIF (EEOCJOBCLASS in ADP) for employee: ' + SQLCURSOR4.EMPLOYEE, LOGIT+SENDIT)
										llValid = .F.
									ENDIF
									IF EMPTY(SQLCURSOR4.WORKSITE) THEN
										.TrackProgress('Missing WORKSITE (CUSTOM FIELD 3 in ADP) for employee: ' + SQLCURSOR4.EMPLOYEE, LOGIT+SENDIT)
										llValid = .F.
									ENDIF
									IF EMPTY(SQLCURSOR4.ADP_COMP) THEN
										.TrackProgress('Missing ADP_COMP for employee: ' + SQLCURSOR4.EMPLOYEE, LOGIT+SENDIT)
										llValid = .F.
									ENDIF
									IF NOT llValid THEN
										REPLACE SQLCURSOR4.VALID WITH .F.
									ENDIF
								ENDSCAN
								
								* insert validated into employee table, LOGGING INSERTS
								.TrackProgress('', LOGIT+SENDIT)
								SELECT SQLCURSOR4
								SCAN FOR VALID
									SCATTER MEMVAR
									
*!*										* company X for express drivers
*!*										*IF ( SQLCURSOR4.DIVISION = 2 ) AND ( SQLCURSOR4.DEPT = 655 ) THEN
*!*										IF (SQLCURSOR4.DIVISION = EXPRESSDIVISION) AND (SQLCURSOR4.DEPT = EXPRESSDEPT) THEN
*!*											m.company = "X"
*!*										ELSE
*!*											m.company = " "
*!*										ENDIF

									* populating more of the old companies per April 11/22/10 MB
									DO CASE
										CASE (INLIST(SQLCURSOR4.DIVISION,1,60,75)) 
											m.company = "F"
										CASE (SQLCURSOR4.DIVISION = 2) 
											company = "X"
										CASE (SQLCURSOR4.DIVISION = 3) 
											m.company = "T"
										CASE (INLIST(SQLCURSOR4.DIVISION,14,4,7)) 
											m.company = "I"
										CASE (SQLCURSOR4.DIVISION = 6) 
											m.company = "J"
										CASE (INLIST(SQLCURSOR4.DIVISION,5,52,53,54,55,56,57,58)) 
											m.company = "W"
										CASE (INLIST(SQLCURSOR4.DIVISION,50)) 
											m.company = "L"
										OTHERWISE
											m.company = " "
									ENDCASE
									
									m.addproc = "EAE"
									m.adddt = DATETIME()
									m.addby = UPPER(.cUSERNAME)
									m.employeeid = genpk("employee","hr")
									
									*IF NOT .lTestMode THEN
										INSERT INTO EMPLOYEE FROM MEMVAR
									*ENDIF
									
									lcImport = 'Imported File#: ' +  TRANSFORM(SQLCURSOR4.EMP_NUM) + ',  Name: ' + ALLTRIM(SQLCURSOR4.EMPLOYEE)
									.TrackProgress(lcImport, LOGIT+SENDIT)
									.cExportedList = .cExportedList + lcImport + CRLF
									
									* build express hire list
									IF (SQLCURSOR4.DIVISION = EXPRESSDIVISION) AND (SQLCURSOR4.DEPT = EXPRESSDEPT) THEN
										lcExpressID = "On " + TRANSFORM(SQLCURSOR4.HIRE_DATE) + " we welcome our new employee " + ;
											PROPER( ALLTRIM(SQLCURSOR4.FIRST) + " " + ALLTRIM(SQLCURSOR4.LAST) ) + ;
											" in division 02, department 0655."
										.cExpressHireList = .cExpressHireList + lcExpressID + CRLF + CRLF
									ENDIF
									
								ENDSCAN
							ENDIF   &&  NOT USED('SQLCURSOR4') OR EOF('SQLCURSOR4')
						ENDIF   &&  NOT USED('SQLCURSOR3') OR EOF('SQLCURSOR3')

						CLOSE DATABASES ALL
					ENDIF  &&  .ExecSQL(lcSQL, 'SQLCURSOR1', RETURN_DATA_MANDATORY)

					.TrackProgress('', LOGIT+SENDIT)
					.TrackProgress('EXPORT NEW ADP EMPLOYEES process ended normally.', LOGIT+SENDIT+NOWAITIT)

					IF EMPTY(.cExportedList) THEN
						lcTopBodyText = "No new employees were exported. No action is required."
					ELSE
						lcTopBodyText = "The following new employees were exported: " + CRLF + CRLF + .cExportedList
					ENDIF

				ELSE
					* connection error
					.TrackProgress('Unable to connect to ADP Payroll System.', LOGIT+SENDIT)
				ENDIF   &&  .nSQLHandle > 0

			CATCH TO loError

				.TrackProgress('There was an error.',LOGIT+SENDIT)
				.TrackProgress('The Error # is: ' + TRANSFORM(loError.ErrorNo), LOGIT+SENDIT)
				.TrackProgress('The Error Message is: ' + TRANSFORM(loError.MESSAGE), LOGIT+SENDIT)
				.TrackProgress('The Error occurred in Program: ' + TRANSFORM(loError.PROCEDURE), LOGIT+SENDIT)
				.TrackProgress('The Error occurred at line #: ' + TRANSFORM(loError.LINENO), LOGIT+SENDIT)
				lnNumberOfErrors = lnNumberOfErrors + 1
				CLOSE DATA

			ENDTRY

			CLOSE DATA
			WAIT CLEAR
			***************** INTERNAL email results ******************************
			.TrackProgress('About to send status email.',LOGIT)
			.TrackProgress('The logfile is: ' + .cLogFile, SENDIT)
			.TrackProgress('EXPORT NEW ADP EMPLOYEES process started: ' + .cStartTime, LOGIT+SENDIT)
			.TrackProgress('EXPORT NEW ADP EMPLOYEES process finished: ' + TTOC(DATETIME()), LOGIT+SENDIT)

			* put some text at top of body
			.cBodyText = lcTopBodyText + CRLF + CRLF + ;
				"(Report log follows)" + ;
				CRLF + CRLF + .cBodyText

			IF .lSendInternalEmailIsOn THEN
				* try to trap error from not having dartmail dll's registered on user's pc...
				TRY
					DO FORM M:\DEV\FRM\dartmail2 WITH .cSendTo,.cFrom,.cSubject,.cCC,.cAttach,.cBodyText,"A"
					.TrackProgress('Sent status email.',LOGIT)

					IF NOT EMPTY(.cExpressHireList) then
						.cBodyText = CRLF + .cExpressHireList + CRLF + CRLF + ;
							"(Report log follows)" + ;
							CRLF + CRLF + .cBodyText
						.cSubject = .cExpressSubject
						DO FORM dartmail2 WITH .cExpressSendTo,.cFrom,.cSubject,.cExpressCC,.cAttach,.cBodyText,"A"
						.TrackProgress('Sent status email.',LOGIT)
					ENDIF

				CATCH TO loError
					=MESSAGEBOX('There was an error sending the status email. Please contact Mark Bennett.',0+64,"Export FoxPro Employees")
					.TrackProgress('There was an error sending the status email.',LOGIT)
					.TrackProgress('The Error # is: ' + TRANSFORM(loError.ErrorNo), LOGIT)
					.TrackProgress('The Error Message is: ' + TRANSFORM(loError.MESSAGE), LOGIT)
					.TrackProgress('The Error occurred in Program: ' + TRANSFORM(loError.PROCEDURE), LOGIT)
					.TrackProgress('The Error occurred at line #: ' + TRANSFORM(loError.LINENO), LOGIT)
					lnNumberOfErrors = lnNumberOfErrors + 1
					CLOSE DATA
				ENDTRY
			ELSE
				.TrackProgress('Did not send status email: THIS.lSendInternalEmailIsOn = FALSE.',LOGIT)
			ENDIF

		ENDWITH
		RETURN
	ENDFUNC && main


	FUNCTION ExecSQL
		LPARAMETERS tcSQL, tcCursorName, tlNoDataReturnedIsOkay
		LOCAL llRetval, lnResult
		WITH THIS
			* close target cursor if it's open
			IF USED(tcCursorName)
				USE IN (tcCursorName)
			ENDIF
			WAIT WINDOW "Fetching remote data; please wait..." NOWAIT
			lnResult = sqlexec(.nSQLHandle, tcSQL, tcCursorName)
			llRetval = ( lnResult > 0 )
			IF llRetval THEN
				* see if any data came back
				IF NOT tlNoDataReturnedIsOkay THEN
					IF (NOT USED(tcCursorName)) OR EOF(tcCursorName) THEN
						llRetval = .F.
						.TrackProgress('No data was returned by this query: ' +CRLF + tcSQL, LOGIT+SENDIT)
					ENDIF
				ENDIF
			ELSE
				.TrackProgress("Result Code [" + TRANSFORM(lnResult) + "] returned by this query: " + tcSQL, LOGIT+SENDIT)
			ENDIF
			WAIT CLEAR
			RETURN llRetval
		ENDWITH
	ENDFUNC


	PROCEDURE TrackProgress
		* do any combination of Wait Window, writing to logfile, and adding to body of email,
		* based on nFlags parameter.
		LPARAMETERS tcExpression, tnFlags
		WITH THIS
			IF BITAND(tnFlags,LOGIT) = LOGIT THEN
				IF .lLoggingIsOn THEN
					?
					? .cProcessName + "  " + TTOC(DATETIME()) + ": " + tcExpression
				ENDIF
			ENDIF
			IF .lWaitWindowIsOn THEN
				IF BITAND(tnFlags,NOWAITIT) = NOWAITIT THEN
					WAIT WINDOW tcExpression NOWAIT
				ENDIF
				IF BITAND(tnFlags,WAITIT) = WAITIT THEN
					WAIT WINDOW tcExpression TIMEOUT .nWaitWindowTimeout
				ENDIF
			ENDIF
			IF BITAND(tnFlags,SENDIT) = SENDIT THEN
				IF .lSendInternalEmailIsOn THEN
					.cBodyText = .cBodyText + tcExpression + CRLF + CRLF
				ENDIF
			ENDIF
		ENDWITH
	ENDPROC  &&  TrackProgress

ENDDEFINE

