DO M:\DEV\PRG\_SETVARS WITH .T.
PUBLIC cfilename,cfileoutname,NormalExit
SET STEP ON 
TRY
	lTesting = .F.
	lOverrideBusy = .F.
	NormalExit = .F.

	IF !lTesting
		IF USED('ftpsetup')
		USE IN ftpsetup
		endif
		SELECT 0
		USE F:\edirouting\ftpsetup SHARED
		cTransfer = "MARCJACOBS-MOVE"
		LOCATE FOR ftpsetup.transfer = cTransfer
		IF ftpsetup.chkbusy = .T. AND !lOverrideBusy
			WAIT WINDOW "File is processing...exiting" TIMEOUT 2
			NormalExit = .T.
			THROW
		ENDIF
		REPLACE chkbusy WITH .T. FOR ftpsetup.transfer = cTransfer
		USE IN ftpsetup
	ENDIF

	cDirIn = "F:\FTPUSERS\MJ_Wholesale\IN\"
	CD &cDirIn
	cCustname = "MARC JACOBS"

	_SCREEN.WINDOWSTATE=1
	_SCREEN.CAPTION="Marc Jacobs File Move Process"

	nFound = ADIR(ary1,'*.*')
	IF nFound = 0
		CLOSE DATABASES ALL
		WAIT WINDOW "No NJ files found to process...exiting" TIMEOUT 2
		NormalExit = .t.
		throw
	ENDIF

	len1 = ALEN(ary1,1)

	FOR i = 1 TO len1
		ASSERT .F. MESSAGE "In move loop"
		cfilename = ALLT(ary1[i,1])
		WAIT WINDOW "Processing "+cfilename TIMEOUT 1

		xfile = (cDirIn+cfilename)
		cString = FILETOSTR(xfile)

		DO CASE
		SET STEP ON 
			CASE "GS*FA"$cString  && 997 files
				cDirOut = ("F:\FTPUSERS\MJ_Wholesale\WMS_Outbound\997in\")

			CASE "GS*PO"$cString  && 943 files
				cDirOut = ("F:\FTPUSERS\MJ_Wholesale\WMS_Inbound\943IN\")
				
			CASE "GS*AR"$cString  && 943 files
				cDirOut = ("F:\FTPUSERS\MJ_Wholesale\WMS_Inbound\943IN\")
				
			CASE "GS*SC"$cString  && 832 Stylemaster files
				cDirOut = ("F:\FTPUSERS\MJ_Wholesale\WMS_Inbound\Stylemaster\")

			CASE "GS*AN"$cString  && 180/RMA files
				cDirOut = ("F:\FTPUSERS\MJ_Wholesale\WMS_Outbound\180IN\")

			CASE "CMDTL_"$cfilename
				cDirOut = ("F:\FTPUSERS\MJ_Wholesale\WMS_Inbound\credit_memo\")

			OTHERWISE  && 940 files
				cDirOut = ("F:\FTPUSERS\MJ_Wholesale\WMS_Outbound\940IN\")
		ENDCASE

		WAIT WINDOW "File will be moved to folder: "+cDirOut NOWAIT
		cfileoutname = (cDirOut+cfilename)
		SET STEP ON 
		COPY FILE &xfile TO &cfileoutname
		IF FILE(cfileoutname)
			IF FILE(xfile)
				DELETE FILE [&xfile]
			ENDIF
		ENDIF
	ENDFOR

	WAIT WINDOW "Marc Jacobs file move process complete" TIMEOUT 1
	SELECT 0
	USE F:\edirouting\ftpsetup SHARED
	If !Used("ftpsetup")
      USE F:\edirouting\ftpsetup SHARED In 0 
    ENDIF
	cTransfer = "MARCJACOBS-MOVE"
	REPLACE chkbusy WITH .F. FOR ftpsetup.transfer = cTransfer
	CLOSE DATABASES ALL
	WAIT CLEAR
	NormalExit = .T.

CATCH TO oErr
	IF !NormalExit
		ASSERT .F. MESSAGE "At CATCH..."
		SET STEP ON

		tsubject = cCustname+" File Move Error ("+TRANSFORM(oErr.ERRORNO)+") at "+TTOC(DATETIME())
		tattach  = ""
		SELECT 0
		USE F:\3pl\DATA\mailmaster ALIAS mm
		LOCATE FOR mm.edi_type = "MISC" AND mm.taskname = "MJPLERROR"
		tsendto = IIF(mm.use_alt,TRIM(mm.sendtoalt),TRIM(mm.sendto))
		tcc = IIF(mm.use_alt,TRIM(mm.ccalt),TRIM(mm.cc))
		USE IN mm
		tmessage = cCustname+" File Move Error..... Please fix me........!"
		lcSourceMachine = SYS(0)
		lcSourceProgram = SYS(16)

		tmessage =tmessage+ "Try/CATCH Exeception Message:"+CHR(13)+;
			[  Message: ] + oErr.MESSAGE +CHR(13)+;
			[  Procedure: ] + oErr.PROCEDURE +CHR(13)+;
			[  LineNo: ] + STR(oErr.LINENO) +CHR(13)+;
			[  Error: ] + STR(oErr.ERRORNO) +CHR(13)+;
			[  Details: ] + oErr.DETAILS +CHR(13)+;
			[  StackLevel: ] + STR(oErr.STACKLEVEL) +CHR(13)+;
			[  LineContents: ] + oErr.LINECONTENTS+CHR(13)+;
			[  UserValue: ] + oErr.USERVALUE+CHR(13)+;
			[  Computer:  ] +lcSourceMachine+CHR(13)+;
			[  940 file:  ] +xfile+CHR(13)+;
			[  Program:   ] +lcSourceProgram

		tattach  = ""
    Do sendmail_au With tsendto,"noreply@tollgroup.com",tsubject,tcc,tattach,tmessage,""
  ELSE
		WAIT WINDOW AT 10,10 "Normal Exit " TIMEOUT 2
	ENDIF
FINALLY
	CLOSE DATABASES ALL
ENDTRY
