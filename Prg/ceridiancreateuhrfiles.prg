* Create UHR .csv files for Ceridian
*
*
* Kronos HR status codes:
#DEFINE KACTIVE '-10131'
#DEFINE KTERMINATED '-10133'
#DEFINE KLEAVE '-10132'
#DEFINE EXPRESSHOMEDEPT '020655'


LOCAL loCeridianUHRProcess
loCeridianUHRProcess = CREATEOBJECT('CeridianUHRProcess')
loCeridianUHRProcess.MAIN()
CLOSE DATABASES ALL
RETURN

#DEFINE LOGIT 1
#DEFINE WAITIT 2
#DEFINE NOWAITIT 4
#DEFINE SENDIT 8
#DEFINE CRLF CHR(13) + CHR(10)
#DEFINE CARRIAGE_RETURN CHR(13)
#DEFINE LINE_FEED CHR(10)
#DEFINE RETURN_DATA_MANDATORY .F.
#DEFINE RETURN_DATA_NOT_MANDATORY .T.

DEFINE CLASS CeridianUHRProcess AS CUSTOM

	cProcessName = 'CeridianUHRProcess'

	lTestMode = .F.  && .F. FOR NORMAL OPERATION, .T. TO SUPPRESS ALL BUT DEVELOPER EMAILS AND CHANGE INPUT TABLE, ETC.

	lAutoYield = .T.

	dToday = DATE()

	cStartTime = TTOC(DATETIME())

	* output file properties
	nCurrentOutputCSVFileHandle = -1
	cOutputCSVHeaderLine1 = 'CO CODE,FILE #,SOCIAL SECURITY NUMBER,EMPLOYEE LAST NAME,EMPLOYEE FIRST NAME,ADDRESS LINE 1,ADDRESS LINE 2,CITY,STATE POSTAL CODE,'
	cOutputCSVHeaderLine2 = 'ZIP CODE,GENDER,HOME DEPARTMENT,RATE TYPE,RATE 1 AMOUNT,HIRE STATUS,HIRE DATE,BIRTH DATE,WORKED STATE TAX CODE,SUI/SDI TAX JURISDICTION CODE'
	*cOutputCSVHeaderLine2 = 'ZIP CODE,GENDER,HOME DEPARTMENT,RATE TYPE,RATE 1 AMOUNT,HIRE STATUS,HIRE DATE,BIRTH DATE,WORKED STATE TAX CODE'
	cFileDate = STRTRAN(DTOC(DATE()),"/","_")

	* processing properties
	nNumberOfOutputFiles = 0
	cTopEmailText = ""
	cErrorsText = ""
	cOutputFolder = 'F:\UTIL\CERIDIAN\UHRDOCS\'
	* connection properties
	nSQLHandle = 0

	* wait window properties
	nWaitWindowTimeout = 2
	lWaitWindowIsOn = .T.

	* logfile properties
	lLoggingIsOn = .T.
	cLogFile = 'F:\UTIL\CERIDIAN\Logfiles\CeridianUHRProcess_log.txt'

	* INTERNAL email properties
	lSendInternalEmailIsOn = .F.
	cFrom = 'TGF Corporate <fmicorporate@fmiint.com>'
	cSendTo = 'mark.bennett@tollgroup.com'
	cCC = ''
	cSubject = 'Ceridian UHR Export for: ' + TTOC(DATETIME())
	cAttach = ''
	cBodyText = ''


	* other properties
	cCOMPUTERNAME = ''
	cUSERNAME = ''

	FUNCTION INIT
		IF NOT DODEFAULT()
			RETURN .F.
		ENDIF
		WITH THIS
			CLEAR
			*SET RESOURCE OFF
			CLOSE DATA
			SET CENTURY ON
			SET DATE AMERICAN
			SET DECIMALS TO 3
			SET HOURS TO 24
			SET ANSI ON
			SET TALK OFF
			SET DELETED ON
			SET CONSOLE OFF
			SET EXCLUSIVE OFF
			SET SAFETY OFF
			SET EXACT OFF
			SET STATUS BAR ON
			SET SYSMENU OFF
			SET ENGINEBEHAVIOR 70
			_VFP.AUTOYIELD = .lAutoYield
			.cCOMPUTERNAME = GETENV("COMPUTERNAME")
			.cUSERNAME = GETENV("USERNAME")
			IF .lTestMode THEN
				.cSendTo = 'Mark Bennett <mbennett@fmiint.com>'
				.cCC = ''
				.cLogFile = 'F:\UTIL\ADPEDI\LOGFILES\CeridianUHRProcess_log_TESTMODE.txt'
			ENDIF
			.lLoggingIsOn = .lLoggingIsOn AND NOT EMPTY(.cLogFile)
			IF .lLoggingIsOn THEN
				SET ALTERNATE TO (.cLogFile) ADDITIVE
				SET ALTERNATE ON
			ENDIF
			* re-init date properties after all SETTINGS
			.dToday = DATE()
		ENDWITH
	ENDFUNC


	FUNCTION DESTROY
		WITH THIS
			IF .lLoggingIsOn  THEN
				SET ALTERNATE OFF
				SET ALTERNATE TO
			ENDIF
		ENDWITH
		DODEFAULT()
	ENDFUNC


	FUNCTION MAIN
		WITH THIS
			LOCAL lnNumberOfErrors, loError, lcSQL, ldToday, ldToday, lcOutputFilename, lcOutputLine
			LOCAL lcFirstName, lnSpacePos, lcMI, lnBytesWritten, lcOutputCSVHeaderLine1, lcOutputCSVHeaderLine2, lcOutputCSVHeaderLine3

			TRY
				lnNumberOfErrors = 0

				*.TrackProgress('==================================================================================================================', LOGIT+SENDIT)
				.TrackProgress('Ceridian UHR Export process started....', LOGIT+SENDIT+NOWAITIT)
				.TrackProgress('COMPUTERNAME = ' + .cCOMPUTERNAME, LOGIT+SENDIT)
				.TrackProgress('USERNAME = ' + .cUSERNAME, LOGIT+SENDIT)
				.TrackProgress('Project = CERIDIANCREATEUHRFILES', LOGIT)
				IF .lTestMode THEN
					.TrackProgress('                        TEST MODE                        ', LOGIT+SENDIT)
				ENDIF

				SET PROCEDURE TO VALIDATIONS ADDITIVE

				ldToday = .dToday
				ldOneMonthAgo = GOMONTH(ldToday,-1)

				*.GetAllFoxProData()



				*.GetEmployeeHistory()



				***********************************************************
				***************   PREVIOUS BASE FILE
				lcRootFileName = "PreviousBaseEmployees"
				lcOutputFilename = .cOutputFolder + lcRootFileName + ".CSV"
				WAIT WINDOW NOWAIT "Creating " + lcOutputFilename + "..."

				***********************************************************

				*!*					SET TEXTMERGE ON
				*!*					TEXT TO	lcSQL NOSHOW
				*!*	SELECT
				*!*	"ADP   " AS SOURCE,
				*!*	A.COMPANYCODE AS ADP_COMP,
				*!*	{fn LEFT(A.HOMEDEPARTMENT,2)} AS DIVISION,
				*!*	{fn SUBSTRING(A.HOMEDEPARTMENT,3,4)} AS DEPARTMENT,
				*!*	A.LASTNAME,
				*!*	A.FIRSTNAME,
				*!*	' ' AS MI,
				*!*	A.FILE# AS CLOCK,
				*!*	A.HIREDATE AS HIREDATEDT,
				*!*	A.HIREDATE AS ORIGHIREDT,
				*!*	A.TERMINATIONDATE AS TERMDATEDT,
				*!*	A.CUSTAREA3 AS WORKSITE,
				*!*	A.GENDER,
				*!*	A.STREETLINE1 AS STREET1,
				*!*	{FN IFNULL(A.STREETLINE2,' ')} AS STREET2,
				*!*	A.CITY,
				*!*	A.STATE,
				*!*	'USA' AS COUNTRY,
				*!*	' ' AS SUFFIX,
				*!*	' ' AS EMPNUMBER,
				*!*	' ' AS RATECODE,
				*!*	' ' AS PAYFREQ,
				*!*	' ' AS PAYTYPE,
				*!*	A.STATUS,
				*!*	' ' AS TMPSINDATE,
				*!*	A.ZIPCODE,
				*!*	A.BIRTHDATE AS BDATEDT,
				*!*	{FN IFNULL(A.RATE1AMT,0.00)} AS RATE1AMT,
				*!*	A.SOCIALSECURITY# AS SS_NUM
				*!*	FROM REPORTS.V_EMPLOYEE A
				*!*	WHERE A.COMPANYCODE IN ('E87','E88','E89')
				*!*	ORDER BY A.NAME
				*!*					ENDTEXT
				*!*					SET TEXTMERGE OFF

				SET TEXTMERGE ON
				TEXT TO	lcSQL NOSHOW
SELECT
DISTINCT
COMPANYCODE AS PSID,
SOCIALSECURITY# AS SS_NUM,
NAME,
HIREDATE
FROM REPORTS.V_EMPLOYEE
				ENDTEXT
				SET TEXTMERGE OFF

				IF .lTestMode THEN
					.TrackProgress('', LOGIT+SENDIT)
					.TrackProgress('lcSQL =' + lcSQL, LOGIT+SENDIT)
					.TrackProgress('', LOGIT+SENDIT)
				ENDIF

				.TrackProgress('Connecting to ADP System....', LOGIT+SENDIT)

				.nSQLHandle = SQLCONNECT("PCPAYWIN","MBENNETT","VP737A")

				IF .nSQLHandle > 0 THEN

					IF USED('CURSSNPRE') THEN
						USE IN CURSSNPRE
					ENDIF
					IF USED('CURSSN') THEN
						USE IN CURSSN
					ENDIF
					IF USED('CURADP') THEN
						USE IN CURADP
					ENDIF
					IF USED('CURADPPRE') THEN
						USE IN CURADPPRE
					ENDIF

					IF .ExecSQL(lcSQL, 'CURSSNPRE', RETURN_DATA_MANDATORY) THEN



						SELECT ;
							LEFT(SS_NUM,3) + "-" + SUBSTR(SS_NUM,4,2) + "-" + RIGHT(SS_NUM,4) AS SSN, ;
							PSID, ;
							SS_NUM, ;
							NAME, ;
							HIREDATE ;
							FROM CURSSNPRE ;
							INTO CURSOR CURSSN ;
							ORDER BY 1, 2


						*!*							SELECT CURSSN
						*!*							BROWSE
						*!*							THROW

						* extract Fox Data which is not in ADP Master Control (i.e., the SSNs are not in CURSSN)

						SELECT ;
							A.RECNUM, ;
							LEFT(A.SOURCE,6) AS SOURCE, ;
							A.ADP_COMP, ;
							PADL(A.DIVISION,2,'0') AS DIVISION, ;
							PADL(A.DEPT,4,'0') AS DEPARTMENT, ;
							A.LAST AS LASTNAME, ;
							A.FIRST AS FIRSTNAME, ;
							' ' AS MI, ;
							A.EMP_NUM AS CLOCK, ;
							DTOT(A.HIRE_DATE) AS HIREDATEDT, ;
							DTOT({}) AS ORIGHIREDT, ;
							DTOT(A.TERM_DATE) AS TERMDATEDT, ;
							A.WORKSITE, ;
							A.SEXTYPE AS GENDER, ;
							A.ADDRESS AS STREET1, ;
							' ' AS STREET2, ;
							A.CITY, ;
							A.STATE, ;
							A.PART_TIME, ;
							A.TERM_NOTE, ;
							'USA' AS COUNTRY, ;
							'    ' AS SUFFIX, ;
							' ' AS EMPNUMBER, ;
							' ' AS RATECODE, ;
							' ' AS PAYFREQ, ;
							' ' AS PAYTYPE, ;
							IIF(A.ACTIVE,"A","T") AS STATUS, ;
							' ' AS TMPSINDATE, ;
							A.ZIP AS ZIPCODE, ;
							DTOT(A.DOB) AS BDATEDT, ;
							A.SALARY AS RATE1AMT, ;
							A.SALARY, ;
							A.PHONE, ;
							A.SS_NUM ;
							FROM F:\UTIL\CERIDIAN\DATA\ALLFOXEE A ;
							INTO CURSOR CURADPPRE ;
							WHERE A.SS_NUM NOT IN (SELECT SSN FROM CURSSN) ;
							AND A.LASTDATE IN (SELECT MAX(LASTDATE) FROM F:\UTIL\CERIDIAN\DATA\ALLFOXEE WHERE SS_NUM = A.SS_NUM ) ;
							ORDER BY A.SS_NUM ;
							READWRITE

						*!*	AND NOT A.ACTIVE ;
						*!*	AND NOT EMPTY(A.TERM_DATE) ;
						*!*	AND A.TERM_DATE IN (SELECT MAX(TERM_DATE) FROM F:\UTIL\CERIDIAN\DATA\ALLFOXEE WHERE SS_NUM = A.SS_NUM) ;


						*!*							SELECT CURADP
						*!*							SCAN FOR SOURCE = "ADP   "
						*!*
						*!*								* PROCESS MI'S
						*!*								lcFirstName = ALLTRIM(CURADP.FIRSTNAME)
						*!*								lnSpacePos = AT(' ',lcFirstName)
						*!*								IF lnSpacePos > 0 THEN
						*!*									* replace First name with everything to the left of the space
						*!*									lcNewFirstName = LEFT(lcFirstName, lnSpacePos - 1)
						*!*									REPLACE CURADP.FIRSTNAME WITH lcNewFirstName IN CURADP

						*!*									* replace MI with 1st char of everything to the right of the space
						*!*									lcMI = UPPER(LEFT(ALLTRIM(SUBSTR(lcFirstName, lnSpacePos)),1))
						*!*									IF NOT EMPTY(lcMI) THEN
						*!*										REPLACE CURADP.MI WITH lcMI IN CURADP
						*!*									ENDIF
						*!*								ENDIF
						*!*
						*!*								* DETERMINE RATE CODE, PAY FREQ, PAY TYPE codes
						*!*								DO CASE
						*!*									CASE ADP_COMP = 'E87'
						*!*										REPLACE CURADP.RATECODE WITH '4', CURADP.PAYFREQ WITH 'W', CURADP.PAYTYPE WITH '1' IN CURADP
						*!*									CASE ADP_COMP = 'E88'
						*!*										REPLACE CURADP.RATECODE WITH '2', CURADP.PAYFREQ WITH 'B', CURADP.PAYTYPE WITH '6'  IN CURADP
						*!*									CASE ADP_COMP = 'E89'
						*!*										REPLACE CURADP.RATECODE WITH 'E', CURADP.PAYFREQ WITH 'B', CURADP.PAYTYPE WITH '6'  IN CURADP
						*!*								ENDCASE
						*!*
						*!*								* INACTIVE STATUS
						*!*								IF CURADP.STATUS = 'L' THEN
						*!*									REPLACE CURADP.STATUS WITH 'I' IN CURADP
						*!*								ENDIF
						*!*
						*!*								* HANDLE NULL TERM DATE
						*!*								IF ISNULL(TERMDATEDT) THEN
						*!*									REPLACE CURADP.TERMDATEDT WITH CTOT('') IN CURADP
						*!*								ENDIF
						*!*
						*!*							ENDSCAN

						*!*							SELECT CURADPPRE
						*!*							BROWSE
						*!*							*THROW

						SELECT A.* ;
							FROM CURADPPRE A ;
							INTO CURSOR CURADP ;
							WHERE A.RECNUM IN (SELECT MAX(RECNUM) FROM CURADPPRE WHERE SS_NUM = A.SS_NUM ) ;
							ORDER BY A.SS_NUM ;
							READWRITE

						*!*							SELECT CURADP
						*!*							BROWSE
						*!*							THROW


						SELECT CURADP
						SCAN FOR SOURCE = "FOXPRO"

							* PROCESS MI'S
							lcFirstName = ALLTRIM(CURADP.FIRSTNAME)
							lnSpacePos = AT(' ',lcFirstName)
							IF lnSpacePos > 0 THEN
								* replace First name with everything to the left of the space
								lcNewFirstName = LEFT(lcFirstName, lnSpacePos - 1)
								REPLACE CURADP.FIRSTNAME WITH lcNewFirstName IN CURADP

								* replace MI with 1st char of everything to the right of the space
								lcMI = UPPER(LEFT(ALLTRIM(SUBSTR(lcFirstName, lnSpacePos)),1))
								IF NOT EMPTY(lcMI) THEN
									REPLACE CURADP.MI WITH lcMI IN CURADP
								ENDIF
							ENDIF

							* DETERMINE RATE CODE, PAY FREQ, PAY TYPE codes
							DO CASE
								CASE INLIST(ADP_COMP ,'SXI','E87')
									REPLACE CURADP.RATECODE WITH '4', CURADP.PAYFREQ WITH 'W', CURADP.PAYTYPE WITH '1' IN CURADP
								CASE INLIST(ADP_COMP ,'ZXU','E88')
									REPLACE CURADP.RATECODE WITH '2', CURADP.PAYFREQ WITH 'B', CURADP.PAYTYPE WITH '6'  IN CURADP
								CASE INLIST(ADP_COMP ,'AXA','E89')
									REPLACE CURADP.RATECODE WITH 'E', CURADP.PAYFREQ WITH 'B', CURADP.PAYTYPE WITH '6'  IN CURADP
							ENDCASE

							* INACTIVE STATUS
							IF CURADP.STATUS = 'L' THEN
								REPLACE CURADP.STATUS WITH 'I' IN CURADP
							ENDIF

							* HANDLE NULL TERM DATE
							IF ISNULL(TERMDATEDT) THEN
								REPLACE CURADP.TERMDATEDT WITH CTOT('') IN CURADP
							ENDIF

							* GET ORIG HIRE DATE
							IF USED('CURORIG') THEN
								USE IN CURORIG
							ENDIF
							SELECT MIN(HIRE_DATE) AS ORIG_HIRE FROM F:\UTIL\CERIDIAN\DATA\ALLFOXEE INTO CURSOR CURORIG WHERE SS_NUM = CURADP.SS_NUM
							REPLACE CURADP.ORIGHIREDT WITH DTOT(CURORIG.ORIG_HIRE) IN CURADP

						ENDSCAN



						* NOW MAKE STATUS = ACTIVE, MAKE HIREDATE = ORIGHIRE, BLANK TERMDATE
						SELECT CURADP
						SCAN
							REPLACE CURADP.HIREDATEDT WITH CURADP.ORIGHIREDT IN CURADP &&, CURADP.STATUS WITH "A", CURADP.TERMDATEDT WITH DTOT({}) IN CURADP
						ENDSCAN

						*!*							SELECT CURADP
						*!*							BROWSE
						*!*							THROW


						SELECT ;
							TTOD(BDATEDT) AS BIRTHDATE, ;
							TTOD(HIREDATEDT) AS HIREDATE, ;
							TTOD(ORIGHIREDT) AS ORIGHIRE, ;
							TTOD(TERMDATEDT) AS TERMDATE, * ;
							FROM CURADP ;
							INTO CURSOR CURADP2 ;
							ORDER BY SS_NUM

						.nCurrentOutputCSVFileHandle = FCREATE(lcOutputFilename)
						IF .nCurrentOutputCSVFileHandle < 0 THEN
							.TrackProgress('***** Error on FCREATE of: ' + lcOutputFilename, LOGIT+SENDIT)
							THROW
						ELSE
							.TrackProgress('FCREATEed: ' + lcOutputFilename, LOGIT+SENDIT)
							.nNumberOfOutputFiles = .nNumberOfOutputFiles + 1
						ENDIF

						lcOutputCSVHeaderLine1 = 'SSN,HireDate,TermDate,Street1,Street2,City,State,Zip,Country,FirstName,MiddleName,LastName,Suffix,PSID,Clock,EmpNumber,'
						lcOutputCSVHeaderLine2 = 'Pay Division,Pay Department,BirthDate,Gender,OrigHireDate,Pay Rate,Rate Code,Pay Freq,Status,Temp SIN Expiration Date,Pay Type,#'

						lnBytesWritten = FWRITE(.nCurrentOutputCSVFileHandle,lcOutputCSVHeaderLine1 + lcOutputCSVHeaderLine2 + CRLF)
						.TrackProgress('FWROTE # of bytes: ' + TRANSFORM(lnBytesWritten), LOGIT+SENDIT)

						SELECT CURADP2
						SCAN

							lcOutputLine = ;
								.CleanUpField(CURADP2.SS_NUM) + "," + ;
								.CleanUpField(CURADP2.HIREDATE) + "," + ;
								.GetBlankDate() + "," + ;
								.CleanUpField(CURADP2.STREET1) + "," + ;
								.CleanUpField(CURADP2.STREET2) + "," + ;
								.CleanUpField(CURADP2.CITY) + "," + ;
								.CleanUpField(CURADP2.STATE) + "," + ;
								.CleanUpField(CURADP2.ZIPCODE) + "," + ;
								.CleanUpField(CURADP2.COUNTRY) + "," + ;
								.CleanUpField(CURADP2.FIRSTNAME) + "," + ;
								.CleanUpField(CURADP2.MI) + "," + ;
								.CleanUpField(CURADP2.LASTNAME) + "," + ;
								.CleanUpField(CURADP2.SUFFIX) + "," + ;
								.CleanUpField(CURADP2.ADP_COMP) + "," + ;
								.CleanUpField(CURADP2.CLOCK) + "," + ;
								.CleanUpField(CURADP2.EMPNUMBER) + "," + ;
								.CleanUpField(PADL(CURADP2.DIVISION,2,'0')) + "," + ;
								.CleanUpField(PADL(CURADP2.DEPARTMENT,4,'0')) + "," + ;
								.CleanUpField(CURADP2.BIRTHDATE) + "," + ;
								.CleanUpField(CURADP2.GENDER) + "," + ;
								.CleanUpField(CURADP2.ORIGHIRE) + "," + ;
								.CleanUpField(CURADP2.RATE1AMT) + "," + ;
								.CleanUpField(CURADP2.RATECODE) + "," + ;
								.CleanUpField(CURADP2.PAYFREQ) + "," + ;
								"A" + "," + ;
								.CleanUpField(CURADP2.TMPSINDATE) + "," + ;
								.CleanUpField(CURADP2.PAYTYPE) + "," + ;
								"#"

							=FPUTS(.nCurrentOutputCSVFileHandle,lcOutputLine)

						ENDSCAN

						= FCLOSE(.nCurrentOutputCSVFileHandle)



						***********************************************************
						***************   PERSONAL FILE
						lcRootFileName = "Personal"
						lcOutputFilename = .cOutputFolder + lcRootFileName + ".CSV"
						WAIT WINDOW NOWAIT "Creating " + lcOutputFilename + "..."
						***********************************************************


						IF USED('CURPERSONAL') THEN
							USE IN CURPERSONAL
						ENDIF
						IF USED('CURDATE') THEN
							USE IN CURDATE
						ENDIF

						SELECT {} AS EpDateBeg, * ;
							FROM CURADP2 ;
							INTO CURSOR CURPERSONAL ;
							ORDER BY SS_NUM READWRITE

						SELECT CURPERSONAL
						SCAN
							SELECT MAX(HIRE_DATE) AS EpDateBeg ;
								FROM F:\UTIL\CERIDIAN\DATA\ALLFOXEE ;
								INTO CURSOR CURDATE ;
								WHERE SS_NUM = CURPERSONAL.SS_NUM

							REPLACE CURPERSONAL.EpDateBeg WITH CURDATE.EpDateBeg IN CURPERSONAL
						ENDSCAN

						.nCurrentOutputCSVFileHandle = FCREATE(lcOutputFilename)
						IF .nCurrentOutputCSVFileHandle < 0 THEN
							.TrackProgress('***** Error on FCREATE of: ' + lcOutputFilename, LOGIT+SENDIT)
							THROW
						ELSE
							.TrackProgress('FCREATEed: ' + lcOutputFilename, LOGIT+SENDIT)
							.nNumberOfOutputFiles = .nNumberOfOutputFiles + 1
						ENDIF

						lcOutputCSVHeaderLine1 = 'SSN,EpDateBeg,EpDateEnd,EpHomePhone,EpEthnicOld,EpMarital,EpPrefix,EpEmail,EpDisabled,EpVeteran,EpAtlPhone,EpSmoker,EpCellPhone,EpPagerPhone,EpFaxPhone,'
						lcOutputCSVHeaderLine2 = 'EpModemPhone,EpDateBorn,EpEthnic,Race1,Race2,Race3,Race4,Race5,EpStreet1,EpStreet2,EpCity,EpState,EpZip,EpCountry,EpFirstName,EpMiddleName,EpLastName,EpSuffix,EpSex,EpNickname,EpLangPref,EpVisibleMinority,EpAboriginal,#'

						lnBytesWritten = FWRITE(.nCurrentOutputCSVFileHandle,lcOutputCSVHeaderLine1 + lcOutputCSVHeaderLine2 + CRLF)
						.TrackProgress('FWROTE # of bytes: ' + TRANSFORM(lnBytesWritten), LOGIT+SENDIT)

						SELECT CURPERSONAL
						SCAN

							lcOutputLine = ;
								.CleanUpField(CURPERSONAL.SS_NUM) + "," + ;
								.CleanUpField(CURPERSONAL.EpDateBeg) + "," + ;
								.GetBlankDate() + "," + ;
								.CleanUpField(CURPERSONAL.PHONE) + ;
								",,,,,,,,,,,,," + ;
								.GetBlankDate() + "," + ;
								",,,,,," + ;
								.CleanUpField(CURPERSONAL.STREET1) + "," + ;
								.CleanUpField(CURPERSONAL.STREET2) + "," + ;
								.CleanUpField(CURPERSONAL.CITY) + "," + ;
								.CleanUpField(CURPERSONAL.STATE) + "," + ;
								.CleanUpField(CURPERSONAL.ZIPCODE) + "," + ;
								.CleanUpField(CURPERSONAL.COUNTRY) + "," + ;
								.CleanUpField(CURPERSONAL.FIRSTNAME) + "," + ;
								.CleanUpField(CURPERSONAL.MI) + "," + ;
								.CleanUpField(CURPERSONAL.LASTNAME) + "," + ;
								.CleanUpField(CURPERSONAL.SUFFIX) + "," + ;
								.CleanUpField(CURPERSONAL.GENDER) + "," + ;
								",,,," + ;
								"#"


							=FPUTS(.nCurrentOutputCSVFileHandle,lcOutputLine)

						ENDSCAN

						= FCLOSE(.nCurrentOutputCSVFileHandle)



						***********************************************************
						***************   JOBS FILE
						lcRootFileName = "Job"
						lcOutputFilename = .cOutputFolder + lcRootFileName + ".CSV"
						WAIT WINDOW NOWAIT "Creating " + lcOutputFilename + "..."
						***********************************************************

						IF USED('CURJOBPRE') THEN
							USE IN CURJOBPRE
						ENDIF
						IF USED('CURJOB') THEN
							USE IN CURJOB
						ENDIF

						SELECT ;
							A.*, ;
							' ' AS REGION, ;
							' ' AS ROOM, ;
							B.JOB AS TITLE, ;
							100 AS PCTTIME, ;
							'NEW HIRE' AS WHYSTART, ;
							' ' AS WHYTERM, ;
							' ' AS JOBCODE, ;
							' ' AS WORKPHONE, ;
							' ' AS EXT, ;
							' ' AS SUPERVSR, ;
							' ' AS FAX, ;
							' ' AS MODEM, ;
							1 AS FTE ;
							FROM CURPERSONAL A ;
							LEFT OUTER JOIN F:\UTIL\CERIDIAN\DATA\JOBS B ;
							ON B.DEPT = A.DEPARTMENT ;
							INTO CURSOR CURJOBPRE ;
							ORDER BY A.SS_NUM, A.EpDateBeg ;
							READWRITE

						*!*							SELECT CURJOBPRE
						*!*							BROW
						*!*
						*!*								WHERE A.SS_NUM NOT IN (SELECT SSN FROM CURSSN) ;
						*!*

						.nCurrentOutputCSVFileHandle = FCREATE(lcOutputFilename)
						IF .nCurrentOutputCSVFileHandle < 0 THEN
							.TrackProgress('***** Error on FCREATE of: ' + lcOutputFilename, LOGIT+SENDIT)
							THROW
						ELSE
							.TrackProgress('FCREATEed: ' + lcOutputFilename, LOGIT+SENDIT)
							.nNumberOfOutputFiles = .nNumberOfOutputFiles + 1
						ENDIF

						lcOutputCSVHeaderLine1 = 'SSN,EjDateBeg,EjDateEnd,EjCompany,EjDivision,EjRegion,EjLocation,EjDepartment,EjRoom,EjTitle,EjPctTime,EjWhyStart,EjWhyTerm,'
						lcOutputCSVHeaderLine2 = 'EjJobCode,EjWorkPhone,EjExt,EjSupervisorFlxIDeb,EjFaxPhone,EjModemPhone,EjFTE,#'

						lnBytesWritten = FWRITE(.nCurrentOutputCSVFileHandle,lcOutputCSVHeaderLine1 + lcOutputCSVHeaderLine2 + CRLF)
						.TrackProgress('FWROTE # of bytes: ' + TRANSFORM(lnBytesWritten), LOGIT+SENDIT)


						*!*						SELECT CURJOBPRE
						*!*						BROWSE
						*!*						THROW

						SELECT CURJOBPRE
						SCAN

							lcOutputLine = ;
								.CleanUpField(CURJOBPRE.SS_NUM) + "," + ;
								.CleanUpField(CURJOBPRE.EpDateBeg) + "," + ;
								.GetBlankDate() + "," + ;
								.CleanUpField(CURJOBPRE.ADP_COMP) + "," + ;
								.CleanUpField(PADL(CURJOBPRE.DIVISION,2,'0')) + "," + ;
								.CleanUpField(CURJOBPRE.REGION) + "," + ;
								.CleanUpField(CURJOBPRE.WORKSITE) + "," + ;
								.CleanUpField(PADL(CURJOBPRE.DEPARTMENT,4,'0')) + "," + ;
								.CleanUpField(CURJOBPRE.ROOM) + "," + ;
								.CleanUpField(IIF(ISNULL(CURJOBPRE.TITLE),' ',CURJOBPRE.TITLE)) + "," + ;
								.CleanUpField(CURJOBPRE.PCTTIME) + "," + ;
								.CleanUpField(CURJOBPRE.WHYSTART) + "," + ;
								.CleanUpField(CURJOBPRE.WHYTERM) + "," + ;
								.CleanUpField(CURJOBPRE.JOBCODE) + "," + ;
								.CleanUpField(CURJOBPRE.WORKPHONE) + "," + ;
								.CleanUpField(CURJOBPRE.EXT) + "," + ;
								.CleanUpField(CURJOBPRE.SUPERVSR) + "," + ;
								.CleanUpField(CURJOBPRE.FAX) + "," + ;
								.CleanUpField(CURJOBPRE.MODEM) + "," + ;
								.CleanUpField(CURJOBPRE.FTE) + "," + ;
								"#"

							=FPUTS(.nCurrentOutputCSVFileHandle,lcOutputLine)

						ENDSCAN

						= FCLOSE(.nCurrentOutputCSVFileHandle)



						***********************************************************
						***************   EMPLOYMENT FILE
						lcRootFileName = "Employment"
						lcOutputFilename = .cOutputFolder + lcRootFileName + ".CSV"
						WAIT WINDOW NOWAIT "Creating " + lcOutputFilename + "..."
						***********************************************************
						*!*

						.nCurrentOutputCSVFileHandle = FCREATE(lcOutputFilename)
						IF .nCurrentOutputCSVFileHandle < 0 THEN
							.TrackProgress('***** Error on FCREATE of: ' + lcOutputFilename, LOGIT+SENDIT)
							THROW
						ELSE
							.TrackProgress('FCREATEed: ' + lcOutputFilename, LOGIT+SENDIT)
							.nNumberOfOutputFiles = .nNumberOfOutputFiles + 1
						ENDIF

						lcOutputCSVHeaderLine1 = 'SSN,EeDateBeg,EeDateEnd,EeDateOriginalHire,EeDateLastHire,EeDateSeniorityCalc,EeDateBenefitCalc,EeTermDate,EeTermWhy,EeHireBack,EeI9Status,'
						lcOutputCSVHeaderLine2 = 'EeDateI9Expire,EeAlienNumber,EeEmpNumber,EeEmpNumber2,EeEmpNumber3,EeSecurityClearance,EeFoundSource,EeOwnPct,EeHighComp,EeOfficer,EeCategory,'
						lcOutputCSVHeaderLine3 = 'EeDateStatus,EeStatus,EeWorkCompElg,EeFindCost,EeReferName,EeHolidayCal,EeShares,EeStockID,EeKeyEmp,EeTempSINExpirationDate,EeCitizenship,#'

						lnBytesWritten = FWRITE(.nCurrentOutputCSVFileHandle,lcOutputCSVHeaderLine1 + lcOutputCSVHeaderLine2 + lcOutputCSVHeaderLine3 + CRLF)
						.TrackProgress('FWROTE # of bytes: ' + TRANSFORM(lnBytesWritten), LOGIT+SENDIT)

						IF USED('CUREMPPRE') THEN
							USE IN CUREMPPRE
						ENDIF
						IF USED('CUREMP') THEN
							USE IN CUREMP
						ENDIF

						SELECT ;
							A.*, ;
							A.EpDateBeg AS STATDATE ;
							FROM CURPERSONAL A ;
							INTO CURSOR CUREMPPRE ;
							ORDER BY A.SS_NUM, A.EpDateBeg ;
							READWRITE

						SELECT CUREMPPRE
						SCAN
							IF CUREMPPRE.STATUS = 'T' AND (NOT EMPTY(CUREMPPRE.TERMDATE)) THEN
								REPLACE CUREMPPRE.STATDATE WITH (CUREMPPRE.TERMDATE + 1) IN CUREMPPRE
							ENDIF
						ENDSCAN

						SELECT CUREMPPRE
						SCAN

							lcOutputLine = ;
								.CleanUpField(CUREMPPRE.SS_NUM) + "," + ;
								.CleanUpField(CUREMPPRE.STATDATE) + "," + ;
								.GetBlankDate() + "," + ;
								.CleanUpField(CUREMPPRE.ORIGHIRE) + "," + ;
								.CleanUpField(CUREMPPRE.EpDateBeg) + "," + ;
								.GetBlankDate() + "," + ;
								.GetBlankDate() + "," + ;
								.CleanUpField(IIF(CUREMPPRE.STATUS = 'T',CUREMPPRE.TERMDATE,{})) + "," + ;
								.CleanUpField(IIF(CUREMPPRE.STATUS = 'T',ALLTRIM(CUREMPPRE.TERM_NOTE),' ')) + "," + ;  && EeTermWhy
							"," + ;  && EeHireBack
							"," + ;  && EeI9Status
							.GetBlankDate() + ;  && EeDateI9Expire
							"," + ;  && EeAlienNumber
							"," + ;  && EeEmpNumber
							"," + ;  && EeEmpNumber
							"," + ;  && EeEmpNumber2
							"," + ;  && EeEmpNumber3
							"," + ;  && EeSecurityClearance
							"," + ;  && EeFoundSource
							"," + ;  && EeOwnPct
							IIF(INLIST(CUREMPPRE.ADP_COMP,'AXA','E89'),'Y','N') + "," + ;  && EeHighComp
							IIF(INLIST(CUREMPPRE.ADP_COMP,'AXA','E89'),'Y','N') + "," + ;  && EeOfficer
							IIF(CUREMPPRE.PART_TIME,'Part Time','Full Time') + "," + ;  && EeCategory
							"," + ;  && EeDateStatus
							IIF(CUREMPPRE.STATUS = 'A','Active',IIF(CUREMPPRE.STATUS = 'I','On Leave','Terminated')) + "," + ;  && EeStatus
							"," + ;  && EeWorkCompElg
							"," + ;  && EeFindCost
							"," + ;  && EeReferName
							"," + ;  && EeHolidayCal
							"," + ;  && EeShares
							"," + ;  && EeStockID
							"," + ;  && EeKeyEmp
							"," + ;  && EeTempSINExpirationDate
							"," + ;  && EeCitizenship
							"#"

							=FPUTS(.nCurrentOutputCSVFileHandle,lcOutputLine)

						ENDSCAN

						= FCLOSE(.nCurrentOutputCSVFileHandle)





						***********************************************************
						***************   COMPENSATION FILE
						lcRootFileName = "Compensation"
						lcOutputFilename = .cOutputFolder + lcRootFileName + ".CSV"
						WAIT WINDOW NOWAIT "Creating " + lcOutputFilename + "..."
						***********************************************************
						*!*

						.nCurrentOutputCSVFileHandle = FCREATE(lcOutputFilename)
						IF .nCurrentOutputCSVFileHandle < 0 THEN
							.TrackProgress('***** Error on FCREATE of: ' + lcOutputFilename, LOGIT+SENDIT)
							THROW
						ELSE
							.TrackProgress('FCREATEed: ' + lcOutputFilename, LOGIT+SENDIT)
							.nNumberOfOutputFiles = .nNumberOfOutputFiles + 1
						ENDIF

						lcOutputCSVHeaderLine1 = 'SSN,EmDateBeg,EmDateEnd,EmKind,EmPayGrade,EmPayStep,EmPayCycle,EmPayType,EmHourlyRate,EmAnnual,EmHoursPerPay,'
						lcOutputCSVHeaderLine2 = 'EmShiftID,EmReason,EmIncrAmount,'
						lcOutputCSVHeaderLine3 = '#'

						lnBytesWritten = FWRITE(.nCurrentOutputCSVFileHandle,lcOutputCSVHeaderLine1 + lcOutputCSVHeaderLine2 + lcOutputCSVHeaderLine3 + CRLF)
						.TrackProgress('FWROTE # of bytes: ' + TRANSFORM(lnBytesWritten), LOGIT+SENDIT)

						IF USED('CURCOMP') THEN
							USE IN CURCOMP
						ENDIF

						SELECT ;
							A.* ;
							FROM CURPERSONAL A ;
							INTO CURSOR CURCOMP ;
							ORDER BY A.SS_NUM, A.EpDateBeg ;
							READWRITE

						SELECT CURCOMP
						SCAN

							lcOutputLine = ;
								.CleanUpField(CURCOMP.SS_NUM) + "," + ;
								.CleanUpField(CURCOMP.EpDateBeg) + "," + ;
								.GetBlankDate() + "," + ;  && EmDateEnd
								"," + ;  &&  EmKind
								"," + ;  &&  EmPayGrade
								"," + ;  &&  EmPayStep
								IIF(INLIST(CURCOMP.ADP_COMP,'SXI','E87'),'W','B') + "," + ; && EmPayCycle
								IIF(INLIST(CURCOMP.ADP_COMP,'SXI','E87'),'H','S') + "," + ; && EmPayType
								.CleanUpField(IIF(INLIST(CURCOMP.ADP_COMP,'SXI','E87'),CURCOMP.SALARY,' ')) + "," + ; && EmHourlyRate
								.CleanUpField(0.00) + "," + ;  && EmAnnual
								.CleanUpField(IIF(INLIST(CURCOMP.ADP_COMP,'SXI','E87'),40,80)) + "," + ; && EmHoursPerPay
								"," + ;  &&  EmShiftID
								"," + ;  &&  EmReason
								"," + ;  &&  EmIncrAmount
								"#"

							=FPUTS(.nCurrentOutputCSVFileHandle,lcOutputLine)

						ENDSCAN

						= FCLOSE(.nCurrentOutputCSVFileHandle)







						=SQLDISCONNECT(.nSQLHandle)

					ENDIF  &&  .ExecSQL(lcSQL, 'CURADP', RETURN_DATA_MANDATORY)

				ELSE
					* connection error
					.TrackProgress('Unable to connect to ADP Payroll System.', LOGIT+SENDIT)
				ENDIF  &&  .nSQLHandle > 0

				.TrackProgress('<< Ceridian UHR Export process ended normally >>', LOGIT+SENDIT)



			CATCH TO loError

				.TrackProgress('There was an error.',LOGIT+SENDIT)
				.TrackProgress('The Error # is: ' + TRANSFORM(loError.ERRORNO), LOGIT+SENDIT)
				.TrackProgress('The Error Message is: ' + TRANSFORM(loError.MESSAGE), LOGIT+SENDIT)
				.TrackProgress('The Error occurred in Program: ' + TRANSFORM(loError.PROCEDURE), LOGIT+SENDIT)
				.TrackProgress('The Error occurred at line #: ' + TRANSFORM(loError.LINENO), LOGIT+SENDIT)
				lnNumberOfErrors = lnNumberOfErrors + 1
				IF TYPE('oExcel') = 'O' AND NOT ISNULL(oExcel) THEN
					oExcel.QUIT()
				ENDIF
				CLOSE DATA
				CLOSE ALL

			ENDTRY

			CLOSE DATA
			WAIT CLEAR
			***************** INTERNAL email results ******************************
			.TrackProgress('About to send status email.',LOGIT)
			.TrackProgress('==================================================================================================================', SENDIT)
			.TrackProgress('The logfile is: ' + .cLogFile, SENDIT)
			.TrackProgress('Ceridian UHR Export process started: ' + .cStartTime, LOGIT+SENDIT)
			.TrackProgress('Ceridian UHR Export process finished: ' + TTOC(DATETIME()), LOGIT+SENDIT)

			IF .lSendInternalEmailIsOn THEN
				* try to trap error from not having dartmail dll's registered on user's pc...
				TRY
					DO FORM dartmail2 WITH .cSendTo,.cFrom,.cSubject,.cCC,.cAttach,.cBodyText,"A"
					.TrackProgress('Sent status email.',LOGIT)

				CATCH TO loError
					=MESSAGEBOX('There was an error sending the status email. Please contact Mark Bennett.',0+64,"Ceridian UHR Export Report")
					.TrackProgress('There was an error sending the status email.',LOGIT)
					.TrackProgress('The Error # is: ' + TRANSFORM(loError.ERRORNO), LOGIT)
					.TrackProgress('The Error Message is: ' + TRANSFORM(loError.MESSAGE), LOGIT)
					.TrackProgress('The Error occurred in Program: ' + TRANSFORM(loError.PROCEDURE), LOGIT)
					.TrackProgress('The Error occurred at line #: ' + TRANSFORM(loError.LINENO), LOGIT)
					lnNumberOfErrors = lnNumberOfErrors + 1
					CLOSE DATA
				ENDTRY
			ELSE
				.TrackProgress('Did not send status email: THIS.lSendInternalEmailIsOn = FALSE.',LOGIT)
			ENDIF

		ENDWITH
		RETURN
	ENDFUNC && main



	PROCEDURE GetAllFoxProData
		WITH THIS

			WAIT WINDOW NOWAIT "Getting old FoxPro employee data...."

			CLOSE DATABASES ALL

			IF FILE('F:\UTIL\CERIDIAN\DATA\ALLFOXEE.DBF') THEN
				DELETE FILE F:\UTIL\CERIDIAN\DATA\ALLFOXEE.DBF
			ENDIF
			IF FILE('F:\UTIL\CERIDIAN\DATA\ALLFOXEE.FPT') THEN
				DELETE FILE F:\UTIL\CERIDIAN\DATA\ALLFOXEE.FPT
			ENDIF
			IF FILE('F:\UTIL\CERIDIAN\DATA\ALLFOXEE.CDX') THEN
				DELETE FILE F:\UTIL\CERIDIAN\DATA\ALLFOXEE.CDX
			ENDIF


			IF FILE('F:\UTIL\CERIDIAN\DATA\EMP1.DBF') THEN
				DELETE FILE F:\UTIL\CERIDIAN\DATA\EMP1.DBF
			ENDIF
			IF FILE('F:\UTIL\CERIDIAN\DATA\EMP2.DBF') THEN
				DELETE FILE F:\UTIL\CERIDIAN\DATA\EMP2.DBF
			ENDIF
			IF FILE('F:\UTIL\CERIDIAN\DATA\EMP3.DBF') THEN
				DELETE FILE F:\UTIL\CERIDIAN\DATA\EMP3.DBF
			ENDIF
			IF FILE('F:\UTIL\CERIDIAN\DATA\EMP4.DBF') THEN
				DELETE FILE F:\UTIL\CERIDIAN\DATA\EMP4.DBF
			ENDIF
			IF FILE('F:\UTIL\CERIDIAN\DATA\EMP5.DBF') THEN
				DELETE FILE F:\UTIL\CERIDIAN\DATA\EMP5.DBF
			ENDIF
			IF FILE('F:\UTIL\CERIDIAN\DATA\EMP6.DBF') THEN
				DELETE FILE F:\UTIL\CERIDIAN\DATA\EMP6.DBF
			ENDIF

			IF FILE('F:\UTIL\CERIDIAN\DATA\EMP1.FPT') THEN
				DELETE FILE F:\UTIL\CERIDIAN\DATA\EMP1.FPT
			ENDIF
			IF FILE('F:\UTIL\CERIDIAN\DATA\EMP2.FPT') THEN
				DELETE FILE F:\UTIL\CERIDIAN\DATA\EMP2.FPT
			ENDIF
			IF FILE('F:\UTIL\CERIDIAN\DATA\EMP3.FPT') THEN
				DELETE FILE F:\UTIL\CERIDIAN\DATA\EMP3.FPT
			ENDIF
			IF FILE('F:\UTIL\CERIDIAN\DATA\EMP4.FPT') THEN
				DELETE FILE F:\UTIL\CERIDIAN\DATA\EMP4.FPT
			ENDIF
			IF FILE('F:\UTIL\CERIDIAN\DATA\EMP5.FPT') THEN
				DELETE FILE F:\UTIL\CERIDIAN\DATA\EMP5.FPT
			ENDIF
			IF FILE('F:\UTIL\CERIDIAN\DATA\EMP6.FPT') THEN
				DELETE FILE F:\UTIL\CERIDIAN\DATA\EMP6.FPT
			ENDIF

			SELECT "FOXPRO1" AS SOURCE, * FROM F:\HR\HRDATA\EMPLOYEE INTO TABLE F:\UTIL\CERIDIAN\DATA\EMP1
			SELECT "FOXPRO2" AS SOURCE, * FROM F:\HR\HRDATA\TERMINAT INTO TABLE F:\UTIL\CERIDIAN\DATA\EMP2

			* note: x: must be mapped to \\dc3\STORAGE Share\storage\
			SELECT "FOXPRO3" AS SOURCE, * FROM X:\HR\EMPLOYEE INTO TABLE F:\UTIL\CERIDIAN\DATA\EMP3
			SELECT "FOXPRO4" AS SOURCE, * FROM X:\HR\TERMINAT INTO TABLE F:\UTIL\CERIDIAN\DATA\EMP4

			SELECT "FOXPRO5" AS SOURCE, * FROM "X:\REMOVED FROM F - HR\HR\HRDATA\EMPLOYEE" INTO TABLE F:\UTIL\CERIDIAN\DATA\EMP5
			SELECT "FOXPRO6" AS SOURCE, * FROM "X:\REMOVED FROM F - HR\HR\HRDATA\TERMINAT" INTO TABLE F:\UTIL\CERIDIAN\DATA\EMP6

			CLOSE DATABASES ALL


			USE F:\UTIL\CERIDIAN\DATA\EMP1
			APPEND FROM F:\UTIL\CERIDIAN\DATA\EMP2
			APPEND FROM F:\UTIL\CERIDIAN\DATA\EMP3
			APPEND FROM F:\UTIL\CERIDIAN\DATA\EMP4
			APPEND FROM F:\UTIL\CERIDIAN\DATA\EMP5
			APPEND FROM F:\UTIL\CERIDIAN\DATA\EMP6

			CLOSE DATABASES ALL

			SELECT {} AS LASTDATE, 0000000000 AS RECNUM, * FROM F:\UTIL\CERIDIAN\DATA\EMP1 INTO TABLE F:\UTIL\CERIDIAN\DATA\ALLFOXEE ORDER BY SS_NUM, HIRE_DATE
			CLOSE DATABASES ALL

			*!*				USE F:\UTIL\CERIDIAN\DATA\ALLFOXEE
			*!*				BROWSE

			CLOSE DATABASES ALL

			USE F:\UTIL\CERIDIAN\DATA\ALLFOXEE EXCLUSIVE
			INDEX ON HIRE_DATE TAG HIRE
			INDEX ON TERM_DATE TAG TERM
			INDEX ON LASTDATE TAG LAST
			INDEX ON RECNUM TAG RECNUM

			CLOSE DATABASES ALL

			USE F:\UTIL\CERIDIAN\DATA\ALLFOXEE ALIAS ALLFOXEE
			SELECT ALLFOXEE
			SCAN
				IF ISNULL(ALLFOXEE.TERM_DATE) THEN
					REPLACE ALLFOXEE.LASTDATE WITH ALLFOXEE.HIRE_DATE IN ALLFOXEE
				ELSE
					REPLACE ALLFOXEE.LASTDATE WITH IIF(ALLFOXEE.HIRE_DATE >= ALLFOXEE.TERM_DATE, ALLFOXEE.HIRE_DATE, ALLFOXEE.TERM_DATE) IN ALLFOXEE
				ENDIF
				REPLACE ALLFOXEE.RECNUM WITH RECNO() IN ALLFOXEE
			ENDSCAN

			*!*			USE F:\HR\HRDATA\EMPLOYEE.DBF
			*!*		BROWSE FOR EMPLOYEE = lcName

			*!*		USE F:\HR\HRDATA\TERMINAT.DBF
			*!*		BROWSE FOR EMPLOYEE = lcName

			*!*		USE X:\HR\EMPLOYEE
			*!*		BROWSE FOR EMPLOYEE = lcName

			*!*		USE X:\HR\TERMINAT
			*!*		BROWSE FOR EMPLOYEE = lcName

			*!*		USE "X:\REMOVED FROM F - HR\HR\HRDATA\EMPLOYEE"
			*!*		BROWSE FOR EMPLOYEE = LCNAME
			*!*
			*!*		USE "X:\REMOVED FROM F - HR\HR\HRDATA\TERMINAT"

			CLOSE DATABASES ALL

			WAIT CLEAR

		ENDWITH
		RETURN
	ENDPROC  && GetAllFoxProData



	PROCEDURE GetEmployeeHistory
		WITH THIS

		ENDWITH
		RETURN
	ENDPROC  && GetEmployeeHistory


	*!*		FUNCTION CheckField
	*!*			LPARAMETERS tcField
	*!*			* ASSUMES CURMASTER IS OPEN AND SELECTED
	*!*			LOCAL lcEmployeeIdentifier, llRetVal
	*!*			llRetVal = .T.
	*!*			IF EMPTY(&tcField) THEN
	*!*				lcEmployeeIdentifier = ALLTRIM(CURMASTER.ADP_COMP) + " " + TRANSFORM(CURMASTER.FILE_NUM) + " :  " + ALLTRIM(CURMASTER.LASTNAME) + ", " + ALLTRIM(CURMASTER.FIRSTNAME)
	*!*				.cErrorsText = .cErrorsText + 'ERROR: empty [' + tcField + '] for ' + lcEmployeeIdentifier + CRLF
	*!*				llRetVal = .F.
	*!*			ENDIF
	*!*			RETURN llRetVal
	*!*		ENDFUNC


	*!*		PROCEDURE ProcessKronosOutputFiles
	*!*			LPARAMETERS tcADPCOMP
	*!*			WITH THIS
	*!*				LOCAL lcOutputFilename, lcOutputLine, lcTargetArchivedFilename, llWroteOutputHeader, lcEmployeeIdentifier

	*!*				llWroteOutputHeader = .F.
	*!*				.nCurrentOutputCSVFileHandle = -1

	*!*				SELECT CURMASTER
	*!*				SCAN FOR (ADP_COMP == tcADPCOMP) AND (ISGOOD = 'Y')

	*!*					IF NOT llWroteOutputHeader THEN
	*!*						.CreateOutputCSVFile(tcADPCOMP)
	*!*						llWroteOutputHeader = .WriteOutputHeader()
	*!*					ENDIF

	*!*					lcEmployeeIdentifier = tcADPCOMP + " " + TRANSFORM(CURMASTER.FILE_NUM) + " :  " + ;
	*!*						ALLTRIM(CURMASTER.LASTNAME) + ", " + ALLTRIM(CURMASTER.FIRSTNAME) + CRLF

	*!*					.cNewHireList = .cNewHireList + lcEmployeeIdentifier

	*!*					*!*	* build express hire list
	*!*					*!*	IF CURMASTER.HOMEDEPT == EXPRESSHOMEDEPT THEN
	*!*					*!*		.cExpressHireList = .cExpressHireList + lcEmployeeIdentifier
	*!*					*!*	ENDIF

	*!*					* Write Detail Line

	*!*					lcOutputLine = ;
	*!*						GetNewADPCompany(.CleanUpField(CURMASTER.ADP_COMP)) + "," + ;
	*!*						.CleanUpField(CURMASTER.FILE_NUM) + "," + ;
	*!*						.CleanUpField(CURMASTER.SS_NUM) + "," + ;
	*!*						.CleanUpField(CURMASTER.LASTNAME) + "," + ;
	*!*						.CleanUpField(CURMASTER.FIRSTNAME) + "," + ;
	*!*						.CleanUpField(CURMASTER.STREET1) + "," + ;
	*!*						.CleanUpField(CURMASTER.STREET2) + "," + ;
	*!*						.CleanUpField(CURMASTER.CITY) + "," + ;
	*!*						.CleanUpField(CURMASTER.STATE) + "," + ;
	*!*						.CleanUpField(CURMASTER.ZIPCODE) + "," + ;
	*!*						.CleanUpField(CURMASTER.GENDER) + "," + ;
	*!*						.CleanUpField(CURMASTER.HOMEDEPT) + "," + ;
	*!*						.CleanUpField(CURMASTER.RATETYPE) + "," + ;
	*!*						.CleanUpField(ALLTRIM(STR(CURMASTER.RATE1AMT,9,2))) + "," + ;
	*!*						.CleanUpField(CURMASTER.STATUS) + "," + ;
	*!*						.CleanUpField(TRANSFORM(CURMASTER.HIREDATE)) + "," + ;
	*!*						.CleanUpField(TRANSFORM(CURMASTER.DOB)) + "," + ;
	*!*						.CleanUpField(CURMASTER.WORKSTATE) + "," + ;
	*!*						.CleanUpField(CURMASTER.SUISDI)

	*!*					=FPUTS(.nCurrentOutputCSVFileHandle,lcOutputLine)

	*!*					*** removed tax jurisdiction per Lucille 5/8/08 MB because they were not always right
	*!*					*** .CleanUpField(lcSUISDITAXJURISCD)

	*!*				ENDSCAN

	*!*				* close output file
	*!*				.CloseOutputCSVFile()

	*!*				* copy output file (renamed so it is unique) to TARGETARCHIVED folder on F:
	*!*				lcOutputFilename = .cADPPath + "PR" + GetNewADPCompany(tcADPCOMP) + "EMP.CSV"
	*!*				lcTargetArchivedFilename = .cArchiveTargetCSVPath + "PR" + GetNewADPCompany(tcADPCOMP) + "EMP_" + .cFileDate + ".CSV"
	*!*				IF FILE(lcOutputFilename) THEN
	*!*					COPY FILE (lcOutputFilename) TO (lcTargetArchivedFilename)
	*!*				ENDIF

	*!*				.TrackProgress('=========================================================', LOGIT+SENDIT)
	*!*			ENDWITH
	*!*		ENDPROC


	FUNCTION CleanUpField
		* prepare field for inclusion in the string to be written with FPUTS().
		* especially critical to remove any commas, since that creates bogus column in CSV output file.
		LPARAMETERS tuValue
		LOCAL lcRetVal
		lcRetVal = STRTRAN(ALLTRIM(TRANSFORM(tuValue)),",","")
		RETURN lcRetVal
	ENDFUNC


	*!*		FUNCTION CreateOutputCSVFile
	*!*			LPARAMETERS tcADPCOMP
	*!*			WITH THIS
	*!*				LOCAL lcOutputFilename, llRetVal
	*!*				lcOutputFilename = .cADPPath + "PR" + GetNewADPCompany(tcADPCOMP) + "EMP.CSV"
	*!*				llRetVal = .T.
	*!*				.nCurrentOutputCSVFileHandle = FCREATE(lcOutputFilename)
	*!*				IF .nCurrentOutputCSVFileHandle < 0 THEN
	*!*					.TrackProgress('***** Error on FCREATE of: ' + lcOutputFilename, LOGIT+SENDIT)
	*!*					llRetVal = .F.
	*!*				ELSE
	*!*					.TrackProgress('FCREATEed: ' + lcOutputFilename, LOGIT+SENDIT)
	*!*					.nNumberOfOutputFiles = .nNumberOfOutputFiles + 1
	*!*					DO CASE
	*!*						CASE tcADPCOMP = "AXA"
	*!*							.lOutputAXAFile = .T.
	*!*						CASE tcADPCOMP = "SXI"
	*!*							.lOutputSXIFile = .T.
	*!*						CASE tcADPCOMP = "ZXU"
	*!*							.lOutputZXUFile = .T.
	*!*						OTHERWISE
	*!*							* nothing
	*!*					ENDCASE
	*!*				ENDIF
	*!*				.TrackProgress('nCurrentOutputCSVFileHandle = ' + TRANSFORM(.nCurrentOutputCSVFileHandle), LOGIT+SENDIT)
	*!*			ENDWITH
	*!*			RETURN llRetVal
	*!*		ENDFUNC


	*!*		FUNCTION WriteOutputHeader
	*!*			WITH THIS
	*!*				LOCAL llRetVal, lnBytesWritten
	*!*				* note: no CRLF on first part of header line
	*!*				llRetVal = .T.
	*!*				lnBytesWritten = FWRITE(.nCurrentOutputCSVFileHandle,.cOutputCSVHeaderLine1)
	*!*				.TrackProgress('FWROTE # of bytes: ' + TRANSFORM(lnBytesWritten), LOGIT+SENDIT)
	*!*				lnBytesWritten = FWRITE(.nCurrentOutputCSVFileHandle,.cOutputCSVHeaderLine2 + CRLF)
	*!*				.TrackProgress('FWROTE # of bytes: ' + TRANSFORM(lnBytesWritten), LOGIT+SENDIT)
	*!*			ENDWITH
	*!*			RETURN llRetVal
	*!*		ENDFUNC
	
	FUNCTION GetBlankDate
		RETURN TRANSFORM({})
		*RETURN ''	
	ENDFUNC


	FUNCTION CloseOutputCSVFile
		WITH THIS
			LOCAL llRetVal
			llRetVal = .T.
			IF .nCurrentOutputCSVFileHandle > -1 THEN
				llRetVal = FCLOSE(.nCurrentOutputCSVFileHandle)
			ENDIF
		ENDWITH
		RETURN llRetVal
	ENDFUNC


	FUNCTION ExecSQL
		LPARAMETERS tcSQL, tcCursorName, tlNoDataReturnedIsOkay
		LOCAL llRetVal, lnResult
		WITH THIS
			* close target cursor if it's open
			IF USED(tcCursorName)
				USE IN (tcCursorName)
			ENDIF
			WAIT WINDOW "Fetching remote data; please wait..." NOWAIT
			lnResult = SQLEXEC(.nSQLHandle, tcSQL, tcCursorName)
			llRetVal = ( lnResult > 0 )
			IF llRetVal THEN
				* see if any data came back
				IF NOT tlNoDataReturnedIsOkay THEN
					IF (NOT USED(tcCursorName)) OR EOF(tcCursorName) THEN
						llRetVal = .F.
						.TrackProgress('No data was returned by this query: ' +CRLF + tcSQL, LOGIT+SENDIT)
					ENDIF
				ENDIF
			ELSE
				.TrackProgress("Result Code [" + TRANSFORM(lnResult) + "] returned by this query: " + tcSQL, LOGIT+SENDIT)
			ENDIF
			WAIT CLEAR
			RETURN llRetVal
		ENDWITH
	ENDFUNC


	PROCEDURE TrackProgress
		* do any combination of Wait Window, writing to logfile, and adding to body of email,
		* based on nFlags parameter.
		LPARAMETERS tcExpression, tnFlags
		WITH THIS
			IF BITAND(tnFlags,LOGIT) = LOGIT THEN
				IF .lLoggingIsOn THEN
					?
					? .cProcessName + "  " + TTOC(DATETIME()) + ": " + tcExpression
				ENDIF
			ENDIF
			IF .lWaitWindowIsOn THEN
				IF BITAND(tnFlags,NOWAITIT) = NOWAITIT THEN
					WAIT WINDOW tcExpression NOWAIT
				ENDIF
				IF BITAND(tnFlags,WAITIT) = WAITIT THEN
					WAIT WINDOW tcExpression TIMEOUT .nWaitWindowTimeout
				ENDIF
			ENDIF
			IF BITAND(tnFlags,SENDIT) = SENDIT THEN
				IF .lSendInternalEmailIsOn THEN
					.cBodyText = .cBodyText + tcExpression + CRLF + CRLF
				ENDIF
			ENDIF
		ENDWITH
	ENDPROC  &&  TrackProgress

ENDDEFINE
