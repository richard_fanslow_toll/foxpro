**********************************************************************************
* Nautica 856 bkdn
* This takes the 856 and creates records in f:\nautica\data\ndetail for each PO
* and then also creates barcode records in f:\nautica\data\nbcodes for each carton
***********************************************************************************
* The 856 is structured as
* Shipment
*   Order
*     Pack
*       Item
*     Pack
*       Item
*     Pack
*       Item
*
***********************************************************************************

runack("NAUTICA856IN")

SET EXCLUSIVE OFF
SET SAFETY OFF
CLOSE DATABASES ALL

WAIT WINDOW AT 10,10  "Now setting up Nautica 856 upload............" TIMEOUT 1
SET tablevalidate TO 0
SET SAFETY OFF
SET TALK OFF
SET EngineBehavior 70
SET ESCAPE ON
SET SAFETY OFF
SET MULTILOCKS ON
SET DELETED ON
SET EXCLUSIVE OFF

ON ESCAPE cancel

DO m:\dev\prg\createx856

PUBLIC lProcessOK,lTesting

lTesting = .T.
lcDELIMITER = "*"
lcTranslateOption ="NAUTICA"

delimchar = lcDELIMITER
lcTranOpt= lcTranslateOption

lcpath="f:\nautica\sample856\"
lcarchivepath="f:\nautica\sample856\archive\"

CD &lcpath

lnNum = ADIR(tarray,"f:\nautica\sample856\*.*")

IF lnNum = 0
	WAIT WINDOW AT 10,10 "    No Nautica 856's to import.............." TIMEOUT 2
	RETURN
ELSE
	SELECT 0
	IF lTesting
		USE F:\NAUTICA\DATA\ndetail EXCL
		ZAP
	ENDIF
	USE F:\NAUTICA\DATA\ndetail ALIAS ndetail SHARED
	SELECT 0
	IF lTesting
		USE F:\NAUTICA\DATA\nbcodes EXCL
		ZAP
	ENDIF
	USE F:\NAUTICA\DATA\nbcodes ALIAS nbcodes SHARED
	USE f:\transload\tldata\barcodes IN 0 ALIAS barcodes
	USE f:\vf\data\dc_locs IN 0 ALIAS dc_locs shared
ENDIF

FOR thisfile = 1  TO lnNum
	Archivename = TTOC(DATETIME(),1)
	xfile = lcpath+tarray[thisfile,1]+"."
	archivefile = lcarchivepath+Archivename+tarray[thisfile,1]
	WAIT WINDOW "Importing file: "+xfile NOWAIT 
	IF FILE(xfile)
* load the file into the 856 array
		DO m:\dev\prg\loadedifile WITH xfile,delimchar,lcTranOpt,"NAUTICA"
		lProcessOK = .F.
		DO m:\dev\prg\856bkdn WITH xfile
		IF lProcessOK AND !lTesting
			COPY FILE &xfile TO &archivefile
			IF FILE(archivefile)
				DELETE FILE &xfile
			ENDIF
		ENDIF
	ENDIF
NEXT thisfile

SELECT nbcodes
SET STEP ON 
UPDATE nbcodes SET nbcodes.inwo = barcodes.wo_num WHERE nbcodes.barcode = barcodes.barcodeno
BROWSE
SELECT ndetail
BROWSE
CLOSE DATABASES ALL
RETURN

*************************************************************************************************
