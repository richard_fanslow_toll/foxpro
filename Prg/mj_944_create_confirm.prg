** This script will identify if "Send Receipt" was ticked after confirming the I/B 943 WO
** Send Receipt creates the edi944 file.
utilsetup("MJ_944_CREATE_CONFIRM")
set exclusive off
set deleted on
set talk off
set safety off   &&Allows for overwrite of existing files

goffice='J'
useca("inwolog","wh",,,"mod='"+goffice+"' and  accountid in (6303,6325,6543) and confirmdt>{"+dtoc(date()-90)+"}")

select wo_num as wo, acct_ref as shipment_number, confirmdt as date_confirmed;
	from  inwolog where inlist(accountid ,6303,6325,6543) and !(emptynul(confirmdt)) and reference !='CANCEL' and confirmsent = .f.  and "FILE943" $ comments;
	into cursor temp1

if reccount() > 0
	export 	to  s:\marcjacobsdata\temp\mj_943_wo_missing_send_receipt type xls

	tsendto = "tmarg@fmiint.com,laudy.minyety@tollgroup.com,kristen.floeck@tollgroup.com"
	tattach = "S:\MarcJacobsData\TEMP\mj_943_WO_missing_send_receipt.xls"
	tcc =""
	tfrom ="Toll EDI System Operations <fmi-transload-ops@fmiint.com>"
	tmessage = "NJ 943 WO Send Receipt not Ticked "+ttoc(datetime())+"           from mj_944_create_confirm.prg"
	tsubject = "NJ 943 WO Send Receipt not Ticked"
	do form m:\dev\frm\dartmail2 with tsendto,tfrom,tsubject, tcc,tattach,tmessage  ,"A"
else
	wait window at 10,10 "No  data to report.........." timeout 2
	tsendto = "tmarg@fmiint.com"
	tattach = ""
	tcc =""
	tfrom ="Toll EDI System Operations <fmi-transload-ops@fmiint.com>"
	tmessage = "NO NJ 943 WO Send Receipt not Ticked: "+ttoc(datetime())+"           from mj_944_create_confirm.prg"
	tsubject = "NO NJ 943 WO Send Receipt not Ticked executed without issue"
	do form m:\dev\frm\dartmail2 with tsendto,tfrom,tsubject, tcc,tattach,tmessage  ,"A"
endif

******************************** check for 944 in edi_trigger  NJ

if !used("edi_trigger")
	use f:\3pl\data\edi_trigger shared in 0
endif

select wo_num as wo, acct_ref as shipment_number, confirmdt as date_confirmed;
	from  inwolog where inlist(accountid ,6303,6325,6543) and confirmdt > date()-30  and reference !='CANCEL' and confirmsent = .t.  and "FILE943" $ comments;
	into cursor temp2
select * from temp2 a left join edi_trigger b on a.wo=b.wo_num into cursor c3 readwrite
select * from c3 where emptynul(when_proc) into cursor c4 readwrite
if reccount() > 0
	export to  s:\marcjacobsdata\temp\mj_missing_944 type xls

	tsendto = "tmarg@fmiint.com,laudy.minyety@tollgroup.com,kristen.floeck@tollgroup.com,todd.margolin@tollgroup.com"
	tattach = "S:\MarcJacobsData\TEMP\mj_missing_944.xls"
	tcc =""
	tfrom ="Toll EDI System Operations <fmi-transload-ops@fmiint.com>"
	tmessage = "MJ NJ 944 issue "+ttoc(datetime())+"           from mj_944_create_confirm.prg"
	tsubject = "MJ NJ 944 issue"
	do form m:\dev\frm\dartmail2 with tsendto,tfrom,tsubject, tcc,tattach,tmessage  ,"A"
else
	wait window at 10,10 "No  data to report.........." timeout 2
	tsendto = "tmarg@fmiint.com"
	tattach = ""
	tcc =""
	tfrom ="Toll EDI System Operations <fmi-transload-ops@fmiint.com>"
	tmessage = "NO MJ NJ 944 issue: "+ttoc(datetime())+"           from mj_944_create_confirm.prg"
	tsubject = "NO MJ NJ 944 issue"
	do form m:\dev\frm\dartmail2 with tsendto,tfrom,tsubject, tcc,tattach,tmessage  ,"A"
endif




************** ML

goffice='L'

useca("inwolog","wh",,,"mod='"+goffice+"' and  accountid in (6303,6325,6543) and confirmdt>{"+dtoc(date()-90)+"}",,"inwologm")

select wo_num as wo, acct_ref as shipment_number, confirmdt as date_confirmed;
	from  inwologm where inlist(accountid ,6303,6325,6543) and !(emptynul(confirmdt)) and reference !='CANCEL' and confirmsent = .f.  and "FILE943" $ comments;
	into cursor temp3
if reccount() > 0
	export 	to   s:\marcjacobsdata\temp\ml_mj_943_wo_missing_send_receipt type xls
	tsendto = "tmarg@fmiint.com,laudy.minyety@tollgroup.com,ana.hernandez@tollgroup.com,todd.margolin@tollgroup.com"
	tattach = "S:\MarcJacobsData\TEMP\ML_mj_943_WO_missing_send_receipt.xls"
	tcc =""
	tfrom ="Toll EDI System Operations <fmi-transload-ops@fmiint.com>"
	tmessage = "ML 943 WO Send Receipt not Ticked "+ttoc(datetime())+"           from mj_944_create_confirm.prg"
	tsubject = "ML 943 WO Send Receipt not Ticked"
	do form m:\dev\frm\dartmail2 with tsendto,tfrom,tsubject, tcc,tattach,tmessage  ,"A"
else
	wait window at 10,10 "No  data to report.........." timeout 2
	tsendto = "tmarg@fmiint.com"
	tattach = ""
	tcc =""
	tfrom ="Toll EDI System Operations <fmi-transload-ops@fmiint.com>"
	tmessage = "NO ML 943 WO Send Receipt not Ticked: "+ttoc(datetime())+"           from mj_944_create_confirm.prg"
	tsubject = "NO ML 943 WO Send Receipt not Ticked executed without issue"
	do form m:\dev\frm\dartmail2 with tsendto,tfrom,tsubject, tcc,tattach,tmessage  ,"A"
endif
******************************** check for 944 in edi_trigger  ML

if !used("edi_trigger")
	use f:\3pl\data\edi_trigger shared in 0
endif

select wo_num as wo, acct_ref as shipment_number, confirmdt as date_confirmed;
	from  inwologm where inlist(accountid ,6303,6325,6543) and confirmdt > date()-30  and reference !='CANCEL' and confirmsent = .t.  and "FILE943" $ comments;
	into cursor temp4
select * from temp4 a left join edi_trigger b on a.wo=b.wo_num into cursor c5 readwrite
select * from c5 where emptynul(when_proc) into cursor c6 readwrite
if reccount() > 0
	export to  s:\marcjacobsdata\temp\mj_missing_944 type xls

	tsendto = "tmarg@fmiint.com,laudy.minyety@tollgroup.com,kristen.floeck@tollgroup.com,todd.margolin@tollgroup.com,ana.hernandez@tollgroup.com"
	tattach = "S:\MarcJacobsData\TEMP\mj_missing_944.xls"
	tcc =""
	tfrom ="Toll EDI System Operations <fmi-transload-ops@fmiint.com>"
	tmessage = "MJ ML 944 issue "+ttoc(datetime())+"           from mj_944_create_confirm.prg"
	tsubject = "MJ ML 944 issue"
	do form m:\dev\frm\dartmail2 with tsendto,tfrom,tsubject, tcc,tattach,tmessage  ,"A"
else
	wait window at 10,10 "No  data to report.........." timeout 2
	tsendto = "tmarg@fmiint.com"
	tattach = ""
	tcc =""
	tfrom ="Toll EDI System Operations <fmi-transload-ops@fmiint.com>"
	tmessage = "NO MJ ML 944 issue: "+ttoc(datetime())+"           from mj_944_create_confirm.prg"
	tsubject = "NO MJ ML 944 issue"
	do form m:\dev\frm\dartmail2 with tsendto,tfrom,tsubject, tcc,tattach,tmessage  ,"A"
endif


wait window at 10,10 "all done...." timeout 2

schedupdate()
_screen.caption=gscreencaption
on error


