**domtoicon.prg
**
**Purpose: Create xml files for drayage shipments and domestic outbounds to be sent to Icon
**
**
xoutdir="f:\ftpusers\toll\domxml\"

utilsetup("EDIEXPORT")

wait "Processing Domestic Files To Icon... Step 1" window nowait noclear

set date ymd

use f:\3pl\data\edi_trigger in 0

**seems to be a new issue where edi_trigger is not being opened?? - mvw 10/24/16
if !used("edi_trigger")
	return
endif


use f:\wo\wodata\wolog in 0
use f:\wo\wodata\detail in 0
use f:\wo\wodata\manifest in 0
use f:\sysdata\qqdata\unloccode in 0

xsqlexec("select * from dellocs",,,"wo")

wait "Processing Domestic Files To Icon... Step 2" window nowait noclear

**added the running of 214P triggers which send "OUTGATE" events to cargowise - mvw 10/06/16
**added 214C (work order creation) & 214A (available) triggers as well - mvw 10/24/16
select edi_trigger
count to xfilecnt for inlist(accountid,&gjonesacctlist) and inlist(edi_type,"214P","214C","214A","214D") and !processed
xfilenum=0

wait "Processing Domestic Files To Icon... Step 3" window nowait noclear

if xfilecnt>0
	wait "Processing Domestic OUTGATE Files To Icon..." window nowait noclear

	**for use in dropping new OUTGATE XML files - need to map drive below - mvw 10/05/16
	**map drive to cargowise
	xerror=.f.
	try
		wshnetwork = createobject("wscript.network")

		**use drive Q
		xdrive="Q:"
		xupdateprofile=.t.
		xconnect=.t.
		if directory(xdrive)
			**if drive letter already mapped, make sre its mapped to correct place
			xdrives = wshnetwork.EnumNetworkDrives
			xcnt=xdrives.count
			for xcounter = 0 to xcnt step 2
				if xdrives.item(xcounter)="Q:"
					if lower(xdrives.item(xcounter+1))="\\10.250.240.193\eadapterglb"
						xconnect=.f.
					endif
					exit
				endif
			endfor

			if xconnect
				**if drive letter exists already, disconnect
				xforce=.t.
				wshnetwork.removenetworkdrive(xdrive,xforce,xupdateprofile)
			endif
		endif

		if xconnect
			wshnetwork.mapnetworkdrive(xdrive,"\\10.250.240.193\eAdapterGLB",xupdateprofile,"tollgroup\scsd","TxUJ2yQS")
		endif
	catch
		xerror=.t.
	endtry

	if xerror or !directory(xdrive)
		xerror=.t.
		xmsg="OUTGATE XML Error: Cannot map a drive to Cargowise drop folder... Process cancelled"
		xfrom="TGFSYSTEM"
		do form dartmail2 With "mwinter@fmiint.com",xfrom,xmsg," ","",xmsg,"A"
	else
		scan for inlist(accountid,&gjonesacctlist) and inlist(edi_type,"214P","214C","214A","214D") and !processed
			xfilenum=xfilenum+1
			wait "Processing 214 (Outgate, WO Creation, Available & Delivered Events) to Cargowise Trigger "+transform(xfilenum)+" of "+transform(xfilecnt)+"..." window nowait noclear

			xoutfile=""
			xerrormsg=""
			=cargowise_xml_outgate(edi_trigger.wo_num,edi_trigger.edi_type)

			**copy file to cargowise mapped drive
			if empty(xerrormsg)
				copy file &xoutfile to Q:\*.*
			endif

			replace processed with .t., when_proc with datetime(), file212 with iif(!empty(xoutfile),xoutfile,""), ;
				fin_status with iif(!empty(xerrormsg),xerrormsg,"FILE CREATED") in edi_trigger

			if empty(xerrormsg)
				xsubject="OUTGATE XML File Sent For "+alltrim(wolog.container)
				xmsg="File: "+xoutfile
				xfrom="TGFSYSTEM"
				do form dartmail2 With "mwinter@fmiint.com",xfrom,xsubject," ","",xmsg,"A"
			endif
		endscan
	endif
endif

wait "Processing Domestic Files To Icon... OUTGATE Processing Complete" window nowait noclear

xfilecreated=.f.

select edi_trigger
count to xfilecnt for edi_type="XMLD" and !processed
xfilenum=0

**XMLD = "XML Icon Domestic"
scan for edi_type="XMLD" and !processed
	xfilenum=xfilenum+1
	wait "Processing Domestic To Icon trigger "+transform(xfilenum)+" of "+transform(xfilecnt)+"..." window nowait noclear

	xfilestr=""

	do case
	**drayage
	case seek(edi_trigger.wo_num,"detail","wo_num")
		**need wolog.pickedup as pudt as detail.pickedup doesnt seem to always get populated - mvw 03/11/15
		select w.size, w.mblnum, w.pickedup as pudt, d.*, sum(d.pl_qty) as totqty, sum(d.totalkgs) as totkgs, sum(d.cbm) AS totcbm ;
			from wolog w left join detail d on d.wo_num=w.wo_num ;
			where w.wo_num=edi_trigger.wo_num group by hawb into cursor xdetail

		xoutfile=""
		scan for upper(hawb)="TGF"
			do case
			case type="O"
				xfilestr=filetostr("f:\ftpusers\toll\template-drayocean.xml")
			case type="A"
				xfilestr=filetostr("f:\ftpusers\toll\template-drayair.xml")
			otherwise
				replace processed with .t., when_proc with datetime() in edi_trigger
				loop
			endcase

			xpol=iif(office="N","USNYC","USLGB")
			xcarrier="FMIT"
			xsendagent="FMI"

			**date sample "2011.09.22 00:00:00"

			***GENERATETIME***
			xfilestr=strtran(xfilestr,"***GENERATETIME***",strtran(ttoc(datetime()),"/","."))

			***DESTINATION*** (same as POL?)
			xfilestr=strtran(xfilestr,"***DESTINATION***",xpol)

			***TOTALKGS***
			xfilestr=strtran(xfilestr,"***TOTALKGS***",transform(totkgs))

			***TOTALCBM***
			xfilestr=strtran(xfilestr,"***TOTALCBM***",transform(totcbm))

			***TOTALPIECES***
			xfilestr=strtran(xfilestr,"***TOTALQTY***",transform(totqty))

			***ETD*** (Est time departure) pickup date
			xfilestr=strtran(xfilestr,"***ETD***",iif(empty(pudt),"",strtran(ttoc(pudt),"/",".")))

			***ATD*** (Actual time departure) pickup date
			xfilestr=strtran(xfilestr,"***ATD***",iif(empty(pudt),"",strtran(ttoc(pudt),"/",".")))

			***ETA*** (Est time arrival) pickup date
			xfilestr=strtran(xfilestr,"***ETA***",iif(empty(pudt),"",strtran(ttoc(pudt),"/",".")))

			***ATA*** (Actual time arrival) pickup date
			xfilestr=strtran(xfilestr,"***ATA***",iif(empty(pudt),"",strtran(ttoc(pudt),"/",".")))

			***PICKUPDT*** pickup date
			xfilestr=strtran(xfilestr,"***PICKUPDT***",iif(empty(pudt),"",strtran(ttoc(pudt),"/",".")))

			***DELIVERDT*** same as pickup date
			xfilestr=strtran(xfilestr,"***DELIVERDT***",iif(empty(pudt),"",strtran(ttoc(pudt),"/",".")))

			***TRIG_TIME*** triggered time should be the actual event time, now sent in header_date20 - mvw 03/11/15
			**use pickedup date instead of trigger time as the may back date a confirmation - mvw 03/12/15
*			xfilestr=strtran(xfilestr,"***TRIG_TIME***",iif(empty(edi_trigger.trig_time),"",strtran(ttoc(edi_trigger.trig_time),"/",".")))
			xfilestr=strtran(xfilestr,"***TRIG_TIME***",iif(empty(pudt),"",strtran(ttoc(pudt),"/",".")))

			***POL*** (port of loading, ie USNYC)
			xfilestr=strtran(xfilestr,"***POL***",xpol)

			***SENDAGENT*** (sending agent)
			xfilestr=strtran(xfilestr,"***SENDAGENT***",xsendagent)

			**type dependant fields (Air vs Ocean)
			do case
			case type="O"
				***CONTAINERNO***
				xfilestr=strtran(xfilestr,"***CONTAINERNO***",alltrim(container))

				***CONTAINERTYPE***
				xctrsize="UNKNOWN"
				store 0 to x20cnt, x40cnt &&total numbers of 20' and 40' ctrs
				do case
				case inlist(size,"20' ","20'R")
					xctrsize="20GP"
					x20cnt=1
				case size="20'HNG"
					xctrsize="20GOH"
					x20cnt=1
				case inlist(size,"40' ","40'R")
					xctrsize="40GP"
					x40cnt=1
				case size="40'HC"
					xctrsize="40HC"
					x40cnt=1
				case size="40'HNG"
					xctrsize="40GOH"
					x40cnt=1
				case size="45' "
					xctrsize="45GP"
				case size="45'HC"
					xctrsize="45HC"
				case size="48' "
					xctrsize="UNKNOWN"
				case size="53' "
					xctrsize="53GP"
				endcase
				
				xfilestr=strtran(xfilestr,"***CONTAINERTYPE***",xctrsize)

				**mblnum now sent in MAWBOBL element - mvw 03/11/15
				xfilestr=strtran(xfilestr,"***MASTERBL***",alltrim(mblnum))

				***20CNT***
				xfilestr=strtran(xfilestr,"***20CNT***",transform(x20cnt))
				***40CNT***
				xfilestr=strtran(xfilestr,"***40CNT***",transform(x40cnt))

			case type="A"
				***AWB***
				xfilestr=strtran(xfilestr,"***AWB***",alltrim(awb))
			endcase


			***HAWB***
			**strip the "TGFH" prefix from the hawb (sent in house_number, shipperbookno & hawbhbl elements) - mvw 03/11/15
*			xfilestr=strtran(xfilestr,"***HAWB***",alltrim(hawb))
			xfilestr=strtran(xfilestr,"***HAWB***",alltrim(strtran(hawb,"TGFH","")))
			
			***CARRIER***
			xfilestr=strtran(xfilestr,"***CARRIER***",alltrim(xcarrier))

			***CNEECODE***
			xfilestr=strtran(xfilestr,"***CNEECODE***",alltrim(delloc))

			select dellocs
			locate for accountid=xdetail.accountid and location=xdetail.delloc

			***CNEENAME***
			xfilestr=strtran(xfilestr,"***CNEENAME***",alltrim(iif(empty(label),acct_name,label)))
			***CNEESTREET***
			xfilestr=strtran(xfilestr,"***CNEESTREET***",alltrim(iif(empty(address),acct_name2,address)))
			***CNEECITY***
			xfilestr=strtran(xfilestr,"***CNEECITY***",alltrim(city))
			***CNEESTATE***
			xfilestr=strtran(xfilestr,"***CNEESTATE***",alltrim(state))

			select xdetail

			***JOBCREATED*** datetime()
			xfilestr=strtran(xfilestr,"***JOBCREATED***",strtran(ttoc(datetime()),"/","."))
			***JOBUPDATED*** datetime()
			xfilestr=strtran(xfilestr,"***JOBUPDATED***",strtran(ttoc(datetime()),"/","."))

			**CREATE FILE
			do while .t.
				xoutfile="iex_shipment_"+strtran(strtran(strtran(ttoc(datetime()),":","")," ",""),"/")+".xml"

				xarchiveoutfile=xoutdir+"archive\"+xoutfile
				if !file(xarchiveoutfile)
					exit
				endif
			enddo

			xoutfile=xoutdir+xoutfile
			strtofile(xfilestr,xoutfile)
			copy file &xoutfile to &xarchiveoutfile

			xfilecreated=.t.
		endscan

		replace processed with .t., when_proc with datetime(), file212 with iif(!empty(xoutfile),xoutfile,""), ;
			fin_status with iif(!empty(xoutfile),"FILE CREATED","NO FILE REQUIRED") in edi_trigger

	**domestic outbounds (trips)
	case seek(edi_trigger.wo_num,"manifest","wo_num")
		select *, sum(pl_qty) as totqty, sum(totalkgs) as totkgs, sum(cbm) AS totcbm ;
			from manifest where wo_num=edi_trigger.wo_num group by orig_wo,hawb into cursor xmfst

		xoutfile=""
		scan for upper(hawb)="TGF"
			**we now need separate files for air and ocean as we now send the awb and ctr # for link 
			**  into icon... previously we had sent hawb in <SHIPPERBOOKNO> as the link but that was an
			**  issue in ICON, it would overwrite all previous data for that hawb - mvw 11/12/12
			*xfilestr=filetostr("f:\ftpusers\toll\template-domout.xml")
			do case
			case type="O"
				xfilestr=filetostr("f:\ftpusers\toll\template-outocean.xml")
			case type="A"
				xfilestr=filetostr("f:\ftpusers\toll\template-outair.xml")
			otherwise
				replace processed with .t., when_proc with datetime() in edi_trigger
				loop
			endcase

			xpol=iif(office="N","USCSF","USSPQ")
			xsendagent="FMI"
			xdestagent=""

			**change xdest to delloc city below if found
			xdest=xpol
			store "" to xcneename,xcneest,xcneecity,xcneest

			select dellocs
			locate for accountid=xmfst.accountid and location=xmfst.delloc
			**if not found, mark trigger as error and move on - mvw 01/14/15
			if !found()
				replace errorflag with .t., processed with .t., fin_status with "DELLOC not found" in edi_trigger
				loop
			else
				xcneename=alltrim(iif(empty(label),acct_name,label))
				xcneeaddr=alltrim(iif(empty(address),acct_name2,address))
				xcneecity=alltrim(city)
				xcneest=alltrim(state)

				select unloccode
				locate for country="US" and name=upper(dellocs.city)
				if found()
					xdest="US"+alltrim(unloccode.unloccode)
				endif
			endif

			select xmfst

			**date sample "2011.09.22 00:00:00"

			***GENERATETIME***
			xfilestr=strtran(xfilestr,"***GENERATETIME***",strtran(ttoc(datetime()),"/","."))

			***DESTINATION***
			xfilestr=strtran(xfilestr,"***DESTINATION***",xdest)

			***TOTALKGS***
			xfilestr=strtran(xfilestr,"***TOTALKGS***",transform(totkgs))

			***TOTALCBM***
			xfilestr=strtran(xfilestr,"***TOTALCBM***",transform(totcbm))

			***TOTALPIECES***
			xfilestr=strtran(xfilestr,"***TOTALQTY***",transform(totqty))

			***ETD*** (Est time departure) delivered date
			xfilestr=strtran(xfilestr,"***ETD***",strtran(ttoc(iif(emptynul(delivered),ttod(edi_trigger.trig_time),delivered)),"/","."))

			***ATD*** (Actual time departure) delivered date
			xfilestr=strtran(xfilestr,"***ATD***",strtran(ttoc(iif(emptynul(delivered),ttod(edi_trigger.trig_time),delivered)),"/","."))

			***ETA*** (Est time arrival) delivered date
			xfilestr=strtran(xfilestr,"***ETA***",strtran(ttoc(iif(emptynul(delivered),ttod(edi_trigger.trig_time),delivered)),"/","."))

			***ATA*** (Actual time arrival) delivered date
			xfilestr=strtran(xfilestr,"***ATA***",strtran(ttoc(iif(emptynul(delivered),ttod(edi_trigger.trig_time),delivered)),"/","."))

			***POL*** (port of loading, ie USNYC)
			xfilestr=strtran(xfilestr,"***POL***",xpol)

			***SENDAGENT*** (sending agent)
			xfilestr=strtran(xfilestr,"***SENDAGENT***",xsendagent)

			***DESTAGENT*** (destination agent)
			xfilestr=strtran(xfilestr,"***DESTAGENT***",xdestagent)

			***TRAILERNO***
			xfilestr=strtran(xfilestr,"***TRAILERNO***",alltrim(trailer))

			**type dependant fields (Air vs Ocean)
			do case
			case type="O"
				***CONTAINERNO***
				xfilestr=strtran(xfilestr,"***CONTAINERNO***",alltrim(container))

				***CONTAINERTYPE***
				xctrsize="UNKNOWN"
				store 0 to x20cnt, x40cnt &&total numbers of 20' and 40' ctrs
				do case
				case inlist(size,"20' ","20'R")
					xctrsize="20GP"
					x20cnt=1
				case size="20'HNG"
					xctrsize="20GOH"
					x20cnt=1
				case inlist(size,"40' ","40'R")
					xctrsize="40GP"
					x40cnt=1
				case size="40'HC"
					xctrsize="40HC"
					x40cnt=1
				case size="40'HNG"
					xctrsize="40GOH"
					x40cnt=1
				case size="45' "
					xctrsize="45GP"
				case size="45'HC"
					xctrsize="45HC"
				case size="48' "
					xctrsize="UNKNOWN"
				case inlist(size,"53' ","53'D")
					xctrsize="53GP"
				endcase
				
				xfilestr=strtran(xfilestr,"***CONTAINERTYPE***",xctrsize)

				***20CNT***
				xfilestr=strtran(xfilestr,"***20CNT***",transform(x20cnt))
				***40CNT***
				xfilestr=strtran(xfilestr,"***40CNT***",transform(x40cnt))

			case type="A"
				***AWB***
				xfilestr=strtran(xfilestr,"***AWB***",alltrim(awb))
			endcase

			***HAWB***
			xfilestr=strtran(xfilestr,"***HAWB***",alltrim(hawb))

			**updated to place manifest.wo_num+"-"+hawb in <SHIPPERBOOKNO>, as per Bill Lillicrap - mvw 01/12/12
			***TRIPNO***
			xfilestr=strtran(xfilestr,"***TRIPNO***",transform(wo_num)+"-"+alltrim(hawb))
			
			***CARRIER***
			xfilestr=strtran(xfilestr,"***CARRIER***",alltrim(carrier))

			***CNEECODE***
			xfilestr=strtran(xfilestr,"***CNEECODE***",alltrim(delloc))

			***CNEENAME***
			xfilestr=strtran(xfilestr,"***CNEENAME***",xcneename)
			***CNEESTREET***
			xfilestr=strtran(xfilestr,"***CNEESTREET***",xcneeaddr)
			***CNEECITY***
			xfilestr=strtran(xfilestr,"***CNEECITY***",xcneecity)
			***CNEESTATE***
			xfilestr=strtran(xfilestr,"***CNEESTATE***",xcneest)

			***JOBCREATED*** datetime()
			xfilestr=strtran(xfilestr,"***JOBCREATED***",strtran(ttoc(datetime()),"/","."))
			***JOBUPDATED*** datetime()
			xfilestr=strtran(xfilestr,"***JOBUPDATED***",strtran(ttoc(datetime()),"/","."))

			**CREATE FILE
			do while .t.
				xoutfile="iex_shipment_"+strtran(strtran(strtran(ttoc(datetime()),":","")," ",""),"/")+".xml"

				xarchiveoutfile=xoutdir+"archive\"+xoutfile
				if !file(xarchiveoutfile)
					exit
				endif
			enddo

			xoutfile=xoutdir+xoutfile
			strtofile(xfilestr,xoutfile)
			copy file &xoutfile to &xarchiveoutfile

			xfilecreated=.t.
		endscan

		replace processed with .t., when_proc with datetime(), file212 with iif(!empty(xoutfile),xoutfile,""), ;
			fin_status with iif(!empty(xoutfile),"FILE CREATED","NO FILE REQUIRED") in edi_trigger

	otherwise
		replace processed with .t., when_proc with datetime() in edi_trigger
	endcase
endscan

if xfilecreated
	use f:\edirouting\ftpjobs in 0
	insert into ftpjobs (jobname, userid, jobtime) values ("DOM-XML-OUT","EDIEXPORT",datetime())
	use in ftpjobs
endif

set date american

if used('wolog')
	use in wolog
endif
if used('detail')
	use in detail
endif
if used('manifest')
	use in manifest
endif
if used('dellocs')
	use in dellocs
endif
if used('unloccode')
	use in unloccode
endif
if used('edi_trigger')
	use in edi_trigger
endif

wait clear
