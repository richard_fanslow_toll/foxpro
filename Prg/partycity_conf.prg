**created 06/02/2015
**process to create Party City conformation file.
parameters xwo_num

if !inlist(type("xwo_num"),"N")
	xwo_num = "710099"
endif

lnwonum = val(xwo_num)

close data all

do m:\dev\prg\_setvars with .t.
public ltesting,nfilenum
ltesting = .t.

ctime = ttoc(datetime(),1)
cconfirmdate = substr(dtoc(date(),1),3)
cconfirmtime = alltrim(substr(ctime,9))
nfilenum = 0

cfilename = ("confirmation"+ctime+".txt")
lcoutfile = "f:\ftpusers\partycity\confirmations\"+cfilename
lcarchivefile = "f:\ftpusers\partycity\confirmations\archive\"+cfilename
nfilenum = fcreate(lcoutfile)

create cursor temp1 (field1 c(200))

if usesqlctnucc()
	xsqlexec("select * from ctnucc where container='BMOU4125055'",,,"wh")
else
	cd f:\wo\wodata\
	select 0
	open database f:\wo\wodata\wo.dbc
	use wo!ctnucc
	locate
endif

crectype = "C"
scan for container = "BMOU4125055"
	corigin = "P2 " &&PADR(ALLTRIM(ctnucc.origin),3)
	cseal = padr(alltrim(ctnucc.reference),15," ")
	cpro = padr(alltrim(ctnucc.serialno),20," ")  && this was the PRONum that came oin on the ASN in file...........
	cpro = padr(alltrim(xwo_num),20," ")          && now we are using the trucking work order
	cponum = padr(alltrim(ctnucc.ponum),25)
	clicense = substr(alltrim(ctnucc.ucc),11,9)  && RIGHT(ALLTRIM(ctnucc.ucc),9)
	cstring = ""
	cstring = crectype+corigin+cseal+clicense+cponum+cpro+cconfirmdate+cconfirmtime
	fputs(nfilenum,cstring)
*	INSERT INTO temp1 FROM MEMVAR
*	m.field1 = ""
endscan
if ltesting
	set step on
endif
=fclose(nfilenum)
if file(lcoutfile)
	copy file [&lcOutFile] to [&lcArchiveFile]
endif
modify file  [&lcOutFile]

if !ltesting
	delete file [&lcOutFile]
endif

close data all
