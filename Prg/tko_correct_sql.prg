*!*	This program will check SQL Labels by WO for presence of data by WO, account 5865
*!*	It will then crosscheck Cartons for qty.
*!*	If zero, Labels will auto-transfer. If mismatch Lbls to Ctns, will notify by mail.

utilsetup("TKO_CORRECT_SQL")

RELEASE ALL
CLOSE DATABASES ALL
PUBLIC cSQLPass,nWO_Num,nAcctNum,cCustName,lTesting,cxMessage,lNormalExit,cSrvr,cWhseLoc,cMsg
PUBLIC nLbl,cLbl,nCtn,cCtn,cWO,nLblUnits,cLblUnits,nCtnUnits,cCtnUnits,cWOMove,lDoMail,tfrom
PUBLIC tsendto,tcc
DO m:\dev\prg\_setvars WITH .T.
ON ERROR DEBUG

lTesting = .F.

* nWO_Num = 5041083  && This will change to a data loop selection

cCustName = "TKO"
nAcctNum = 5865
lcUserid = "MIS_USER"
lNormalExit = .F.
lDoMail = .T.
tfrom ="TGF WMS Operations <transload-ops@fmiint.com>"
STORE "" TO cxMessage,cLbl,cCtn,cWO,cWOMove
STORE 0 TO nWO_Num,nLbl,nCtn

TRY
	ASSERT .F. MESSAGE "At start of process"
	FOR j = 1 TO 2
		IF j = 1
			cOffice = "C"
			gmasteroffice = "C"
			goffice = "2"
			cSrvr = "tgfnjsql01"
			cWhseLoc = "SP"
		ELSE
			cOffice = "N"
			gmasteroffice = "N"
			goffice = "I"
			cSrvr = "tgfnjsql01"
			cWhseLoc = "NJ"
		ENDIF

		IF USED('outship')
			USE IN outship
		ENDIF

		xsqlexec("select * from outship where accountid = 5865 and office = '"+cOffice+"' and appt = {"+DTOC(DATE())+"}",,,"wh")
		
		IF j > 1
			cxMessage = cxMessage+CHR(13)
		ENDIF
		SELECT 0
		SELECT wo_num ;
			FROM outship ;
			WHERE accountid = 5865 ;
			AND appt = DATE() ;
			GROUP BY 1 ;
			INTO CURSOR tempout READWRITE
		IF lTesting
			BROWSE
*			ASSERT .f. MESSAGE "In record select...debug"
		ENDIF

		LOCATE
		nWORecs = RECCOUNT()
		IF nWORecs = 0
			cStr1 = "There are no "+cWhseLoc+" TKO Prepack shipments today"
			cxMessage = IIF(EMPTY(cxMessage),cStr1,cxMessage+CHR(13)+cStr1)
			WAIT WINDOW cxMessage TIMEOUT 2
			IF j = 1
				LOOP
			ELSE
				EXIT
			ENDIF
		ENDIF
		INDEX ON wo_num TAG wo_num
		USE IN outship

		SELECT 0
		USE F:\wh\sqlpassword
		cSQLPass = ALLTRIM(sqlpassword.PASSWORD)
		USE IN sqlpassword

		lcDSNLess="driver=SQL Server;server=&csrvr;uid=SA;pwd=&cSQLPass;DATABASE=PICKPACK"
		nHandle=SQLSTRINGCONNECT(lcDSNLess,.T.)
		SQLSETPROP(nHandle,"DispLogin",3)

		WAIT WINDOW "Now processing "+cCustName+" "+cWhseLoc+" SQL view...please wait" NOWAIT
		IF nHandle=0 && bailout
			WAIT WINDOW "Could not make SQL connection" TIMEOUT 3
			cxMessage = "NO SQL CONNECT"
			THROW
		ENDIF

		SELECT tempout
		SCAN
			nWO_Num = tempout.wo_num
			cWO   = ALLTRIM(STR(nWO_Num))

			FOR sqlloops = 1 TO 2
				if usesql()
					IF sqlloops = 1
						xsqlexec("select * from labels where wo_num="+cwo+" order by ship_ref, outshipid, ucc","lblresults",,"pickpack")
					ELSE
						xsqlexec("select * from cartons where wo_num="+cwo+" order by ship_ref, outshipid, ucc","ctnresults",,"pickpack")
					ENDIF
				else
					IF sqlloops = 1
						cFileOutName = "lblresults"
						lcQ1=[SELECT * FROM dbo.labels Labels]
						lcQ2 = [ WHERE Labels.wo_num = ]
						lcQ3 = " &cWo "
						lcQ6 = [order by Labels.ship_ref,Labels.outshipid,Labels.ucc]
						lcsql = lcQ1+lcQ2+lcQ3+lcQ6
					ELSE
						cFileOutName = "ctnresults"
						lcQ1 = [SELECT * FROM dbo.cartons Cartons]
						lcQ2 = [ WHERE Cartons.wo_num = ]
						lcQ3 = " &cWo "
						lcQ6 = [order by Cartons.ship_ref,Cartons.outshipid,Cartons.ucc]
						lcsql = lcQ1+lcQ2+lcQ3+lcQ6
					ENDIF

					llSuccess=SQLEXEC(nHandle,lcsql,cFileOutName)

					IF llSuccess=0  && no records 0 indicates no records and 1 indicates records found
						ASSERT .F. MESSAGE "At NO RECORDS statement in SQLConnect"
						WAIT WINDOW "No "+cCustName+" "+cWhseLoc+" records found" TIMEOUT 2
						cxMessage = "NO SQL CONNECT, WO "+ALLTRIM(STR(nWO_Num))
						THROW
					endif
				endif
			ENDFOR

*			ASSERT .F. MESSAGE "At DEBUG point"
			SELECT ctnresults
			SUM totqty TO nCtnUnits
			cCtnUnits = ALLTRIM(STR(nCtnUnits))
			SELECT DISTINCT ucc FROM ctnresults INTO CURSOR gen1
			COUNT TO nCtn
			cCtn = ALLTRIM(STR(nCtn))

			SELECT lblresults
			SUM totqty TO nLblUnits
			cLblUnits = ALLTRIM(STR(nLblUnits))
			SELECT DISTINCT ucc FROM lblresults INTO CURSOR gen1
			COUNT TO nLbl
			cLbl = ALLTRIM(STR(nLbl))
			cMsgStr = "Total LABELS UCC/Ctns = "+cLbl+CHR(13)+;
				"Total CARTONS UCC/Ctns = "+cCtn+CHR(13)+CHR(13)+;
				"Total LABELS Units = "+cLblUnits+CHR(13)+;
				"Total CARTONS Units = "+cCtnUnits
			WAIT WINDOW cMsgStr TIMEOUT 1
			USE IN gen1

			IF nCtn > 0
				ASSERT .F. MESSAGE "At Cartons > 0"
				IF nLbl # nCtn
					cxMessage = cxMessage+CHR(13)+"There is a "+cWhseLoc+" Qty mismatch."
					cxMessage = cxMessage+CHR(13)+"Labels: "+cLbl+", Cartons: "+cCtn+" (WO: "+cWO+")."
					cxMessage = cxMessage+CHR(13)+"Check and if needed, run 'Finish Packing' in ppdebug."+CHR(13)
					WAIT WINDOW cxMessage TIMEOUT 2
*					THROW
				ELSE
					WAIT WINDOW "Both Qtys match correctly for WO# "+cWO TIMEOUT 2
				ENDIF
			ELSE
				IF nLbl = 0
					cxMessage = cxMessage+CHR(13)+"There are zero records in SQL Ctns AND SQL Labels."
					cxMessage = cxMessage+CHR(13)+"No cartonization done, WO# "+cWO+CHR(13)
*					THROW
				ELSE
					cMsgStr = "SQL Labels = "+cLbl+", Cartons = 0"
					cMsgStr = cMsgStr+CHR(13)+"Will now move Label data to Cartons."
					WAIT WINDOW cMsgStr TIMEOUT 2
					ASSERT .F. MESSAGE cMsgStr
					cWOMove = IIF(EMPTY(cWOMove),cWO,cWOMove+", "+cWO)
					moveresults()
				ENDIF
			ENDIF
		ENDSCAN

		SQLCANCEL(nHandle)
		SQLDISCONNECT(0)
		WAIT WINDOW cWhseLoc+" TKO WOs processed..." TIMEOUT 2
		ASSERT .F.
		IF !EMPTY(cWOMove)
			cStr1 = "SQL Data successfully moved for WO#(s)"+cWOMove
			cxMessage = IIF(EMPTY(cxMessage),cStr1,cxMessage+CHR(13)+cStr1)
		ELSE
			cStr1 = cWhseLoc+" WOs were already correct, no update."
			cxMessage = IIF(EMPTY(cxMessage),cStr1,cxMessage+CHR(13)+cStr1)
		ENDIF
	ENDFOR

	IF !EMPTY(cxMessage)
		mailrecips()
		tattach = ""
		tsubject = "Timed Notice: TKO SQL Filler PRG"
		tmessage = cxMessage
		DO FORM m:\dev\FRM\dartmail2 ;
			WITH tsendto,tfrom,tsubject,tcc,tattach,tmessage,"A"
	ENDIF

	WAIT WINDOW "All TKO WOs processed...exiting" TIMEOUT 2
	lNormalExit = .T.

CATCH TO oErr
	tfrom    ="TGF WMS Pick Pack System Operations <transload-ops@fmiint.com>"
	tattach  = ""
	SELECT 0

	IF !lNormalExit
		ASSERT .F. MESSAGE "In CATCH error...debug"
		mailrecips()
		tattach = ""
		tsubject = "TKO SQL Checking: Error ("+TRANSFORM(oErr.ERRORNO)+") at "+TTOC(DATETIME())
		tmessage = "TKO SQL Checking: Error "
		IF EMPTY(cxMessage)
			tmessage = tmessage+ "Try/CATCH Exeception Message:"+CHR(13)+;
				[  Error: ] + STR(oErr.ERRORNO) +CHR(13)+;
				[  Procedure: ] + oErr.PROCEDURE +CHR(13)+;
				[  Message: ] + oErr.MESSAGE +CHR(13)+;
				[  LineNo: ] + STR(oErr.LINENO) +CHR(13)+;
				[  Details: ] + oErr.DETAILS +CHR(13)+;
				[  LineContents: ] + oErr.LINECONTENTS+CHR(13)+;
				[  UserValue: ] + oErr.USERVALUE+CHR(13)
		ENDIF
		tmessage = tmessage+CHR(13)+CHR(13)+cxMessage
		IF lDoMail
			DO FORM m:\dev\FRM\dartmail2 ;
				WITH tsendto,tfrom,tsubject,tcc,tattach,tmessage,"A"
		ENDIF
	ENDIF
FINALLY
	ON ERROR
	CLOSE DATABASES ALL
ENDTRY

schedupdate()


*************************************************************************
PROCEDURE moveresults
*************************************************************************
* Updates the carton table with label data

	lcQuery = "XXX"

	SELECT lblresults
	lnRecs = RECCOUNT()
	LOCATE
	ii=1

	SCAN
		SCATTER MEMVAR

		ioutdet   = ALLTRIM(STR(m.outdetid))
		iOutship  = ALLTRIM(STR(m.outshipid))
		IF ISNULL(m.ucc)
			m.ucc = "N/A"
		ENDIF

		cUCC      = [']+ALLTRIM(m.ucc)+[']
		IF ISNULL(m.scc214)
			m.scc214 = "N/A"
		ENDIF

		cScc214   = [']+ALLTRIM(m.scc214)+[']
		cpt       = [']+ALLTRIM(m.ship_ref)+[']
		IF ISNULL(m.name)
			m.name = "N/A"
		ENDIF

		m.name = STRTRAN(m.name,['],"")
		cName     = [']+ALLTRIM(m.name)+[']
		IF ISNULL(m.consignee)
			m.consignee = "N/A"
		ENDIF

		m.consignee = STRTRAN(m.consignee,['],"")
		cConsign  = [']+ALLTRIM(m.consignee)+[']
		icust     = ALLTRIM(STR(m.accountid))
		cStyle    = [']+SUBSTR(ALLTRIM(m.style),1,20)+[']
		IF ISNULL(m.color)
			m.color = " "
		ENDIF

		cColor    = [']+ALLTRIM(m.color)+[']
		IF ISNULL(m.id)
			m.id = " "
		ENDIF

		cID       = [']+ALLTRIM(m.id)+[']
		cPack     = [']+ALLTRIM(m.pack)+[']
		cUpc      = [']+ALLTRIM(m.upc)+[']
		cCarton   = [']+ALLTRIM(m.cartonnum)+[']
		cUser     = [']+ALLTRIM(lcUserid)+[']

		IF ISNULL(m.whseloc)
			m.whseloc= "N/A"
		ENDIF

		cWhseLoc  = [']+ALLTRIM(m.whseloc)+[']
		IF ISNULL(m.zone)
			m.zone="N/A"
		ENDIF

		cZone     = [']+ALLTRIM(m.zone)+[']
		nWO       = ALLTRIM(STR(m.wo_num))
		nQty      = ALLTRIM(STR(m.qty))
		nTotQty   = ALLTRIM(STR(m.totqty))
		cRunID    = ALLTRIM(STR(m.runid))

	    cGoffice  = [']+goffice+[']
    	cGmasteroffice  = [']+gmasteroffice+[']
		ccacctnum = ['TKO']

		nCartonid = ALLTRIM(STR(RECNO()))

		lcQ = [insert into cartons ]
		lcQ1 = [(outdetid,outshipid,userid,name,consignee,accountid,wo_num,ship_ref,style,color,id,upc,pack,totqty,qty,ucc,cartonnum,scc214,whseloc,zone,office,runid,mod,cacctnum)]

		lcQ2 = [ values;
       (&ioutdet,;
        &ioutship,;
        &cUser,;
        &cName,;
        &cConsign,;
        &icust,;
        &nWO,;
        &cPT,;
        &cStyle,;
        &cColor,]

		lcQ3=[&cID,;
        &cUPC,;
        &cPack,;
        &ntotqty,;
        &nqty,;
        &cUCC,;
        &cCarton,;
        &cScc214,;
        &cWhseloc,;
        &cZone,;
        &cGmasteroffice,;
        &cRunID,;
        &cgoffice,;
        &ccacctnum)]

		lcQuery = lcQ+lcQ1+lcQ2+lcQ3
		WAIT WINDOW AT 10,10 "Loading record #"+ ALLTRIM(TRANSFORM(ii)) +" out of "+ALLTRIM(TRANSFORM(lnRecs))+" records............  "+ALLTRIM(STR((ii/lnRecs)*100,4,2))+"% done...." NOWAIT
		ii=ii+1

		if usesql()
			xsqlexec(lcquery,,,"pickpack")
		else
			nOK = SQLEXEC(nHandle,lcQuery,"temp")

			IF nOK != 1
				WAIT WINDOW AT 6,2 "SQL Error.."+CHR(13)+" Call MIS"
				cxMessage = "There was an error connecting"
				THROW
			endif
		endif
	ENDSCAN
ENDPROC

**********************
PROCEDURE mailrecips
**********************
	SELECT 0
	USE F:\3pl\DATA\mailmaster ALIAS mm
	LOCATE FOR mm.edi_type = "MISC" AND mm.taskname = "GENERAL"
	tsendto = IIF(mm.use_alt,sendtoalt,sendto)
	tcc =IIF(mm.use_alt,ccalt,cc)
	USE IN mm
ENDPROC

***************************************************************
*!* Stored pieces of the Cartons select statement are below *!*
*	lcQ4 = [ AND  Cartons.ucc <> ]
*	lcQ5 = " 'CUTS' "
*	lcsql = lcQ1+lcQ2+lcQ3+lcQ4+lcQ5+lcQ6
***************************************************************
