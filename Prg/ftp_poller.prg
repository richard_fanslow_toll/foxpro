runack("FTP_POLLER")

DO m:\dev\prg\ftpsetup
SET SYSMENU OFF
SET TALK OFF
SET STATUS BAR OFF 
SET EXCLUSIVE OFF

WITH _SCREEN
	.TOP = 70
	.LEFT = 450
	.WINDOWSTATE = 0
	.BORDERSTYLE = 1
	.WIDTH = 450
	.HEIGHT = 230
	.CLOSABLE = .T.
	.MAXBUTTON = .F.
	.SCROLLBARS = 0
	.CAPTION = "TGF FTP Poller"
ENDWITH

PUBLIC lnRec1
SET DELETED ON 
lcRec1 = 0

*On error DO errorhand() with error(),message(),program(),lineno()
do form M:\DEV\FRM\ftphandler

*!*	********************************************************************************
*!*	procedure errorhand
*!*	parameter merror,mess,mprog,mlineno

*!*	wait window at 10,10 "Program Error number "+alltrim(str(merror))+chr(13)+;
*!*	                     "Error message        "+mess+chr(13)+;
*!*	                     "Line number          "+alltrim(str(mlineno))+chr(13)+;  
*!*	                     "Program              "+mprog      
*!*	                     
*!*	********************************************************************************
                     