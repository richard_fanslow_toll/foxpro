*****  file should be .csv  style,desc, UOM   column b, d ,r
utilsetup("SYNCLAIRE_832")
Close Data All
zuserid=Upper(Getenv("USERNAME"))
lcPath = 'F:\FTPUSERS\Synclaire\Stylemaster\'
lcArchivePath = 'F:\FTPUSERS\Synclaire\Stylemaster\Archived\'
useca("upcmast","wh",,,"select * from upcmast where  accountid in (6521)")
*********************************start xlsx processing
RecCtr=0
len1 = Adir(ary1,lcPath+"*.xlsx")
Delete File F:\FTPUSERS\Synclaire\Stylemaster\*.png
Delete File F:\FTPUSERS\Synclaire\Stylemaster\*.pdf
SET STEP ON 
If len1 = 0
	Wait Window "No files found...exiting" Timeout 2
	Close Data All
	schedupdate()
	_Screen.Caption=gscreencaption
	On Error
	Return
Endif
For i = 1 To len1
	cFilename = Alltrim(ary1[i,1])
	xfile = lcPath+cFilename
	cArchiveFile = (lcArchivePath+cFilename)
	cLoadFile = (lcPath+cFilename)
	Copy File [&cLoadFile] To [&cArchiveFile]
	
*******************************************  ALMA file
*!*	Create Cursor t1 (aa c(20), STYLE C(50), cc c(20), dESC c(50), ee C(20), ff c(20),gg c(20), hh C(20), ii c(20),jj c(20), kk C(20), ll c(20),mm c(20), nn C(20), oo c(20),pp c(20),qq c(20),xtype C(4))	
*!*		tfile=Alltrim(ary1[i,1])
*!*		tfilepath=lcPath+Alltrim(ary1[i,1])
*!*		do excel_to_csv with cfilename, .t.
*!*		
*!*		cFilenamein = lcPath+cFilename
*!*		cFilenameout = lcPath+JUSTSTEM(cFilename)+".csv"	
*!*		SET STEP ON 
*!*		Appe From "&cFilenameout" Type Deli
*!*		Delete File "&tfilepath"
*!*		Delete File "&cFilenameout"
*!*		Delete  From t1 Where Style='SKU'
*!*		DELETE FROM t1 where EMPTY(style)

Create Cursor t1 (aa c(20), Desc C(50), cc c(20), dd c(20), Style C(20), ff c(20),gg c(20),xtype C(4))
	tfile=Alltrim(ary1[i,1])
	tfilepath=lcPath+Alltrim(ary1[i,1])
	do excel_to_csv with cfilename, .t.
	
	cFilenamein = lcPath+cFilename
	cFilenameout = lcPath+JUSTSTEM(cFilename)+".csv"	
	SET STEP ON 
	Appe From "&cFilenameout" Type Deli
	Delete File "&tfilepath"
	Delete File "&cFilenameout"
	Delete  From t1 Where Style='SKU'
	DELETE FROM t1 where EMPTY(style)

	Select upcmast
	Scatter Memvar Memo Blank
	Select t1
	?Datetime()
	Scan
		Scatter Memvar Memo
*		m.adddt=Datetime()
		m.updatedt=Datetime()
		m.addby=zuserid
		m.updateby=zuserid
		m.addproc='SYNC832'
		m.updproc='SYNC832'
		If !Empty(Style)
			Select upcmast
			Locate For Alltrim(Style)=Alltrim(t1.Style)
			If !Found()
				m.upcmastid=sqlgenpk("upcmast","wh")
				Insert Into upcmast From Memvar
				Replace Descrip           With  t1.Desc   In upcmast
				Replace updatedt        With Datetime()   In upcmast
				Replace adddt        With Datetime()   In upcmast
				Replace upc     With t1.Style  In upcmast
				Replace updproc With 'SYNC832' In upcmast
				Replace accountid With 6521
				If t1.xtype='E'
					Replace pnp With .T.
				Else
					Replace pnp With .F.
				Endif
				Release M.upcmastid
			Else
				Replace Descrip           With  t1.Desc   In upcmast
				Replace updatedt        With Datetime()   In upcmast
				Replace upc     With t1.Style  In upcmast
				Replace updproc With 'SYNC832' In upcmast
				If t1.xtype='E'
					Replace pnp With .T.
				Else
					Replace pnp With .F.
				Endif
			Endif
		Else

		Endif

	ENDSCAN
	?Datetime()
	tu("upcmast")

ENDFOR

*********************************start xls processing
RecCtr=0
len1 = Adir(ary1,lcPath+"*.xls")
SET STEP ON 
If len1 = 0
	Wait Window "No files found...exiting" Timeout 2
	Close Data All
	schedupdate()
	_Screen.Caption=gscreencaption
	On Error
	Return
Endif
For i = 1 To len1
	cFilename = Alltrim(ary1[i,1])
	xfile = lcPath+cFilename
	cArchiveFile = (lcArchivePath+cFilename)
	cLoadFile = (lcPath+cFilename)
	Copy File [&cLoadFile] To [&cArchiveFile]
	
Create Cursor t1 (aa c(20), Desc C(50), cc c(20), dd c(20), Style C(20), ff c(20),gg c(20),xtype C(4))
	tfile=Alltrim(ary1[i,1])
	tfilepath=lcPath+Alltrim(ary1[i,1])
	do excel_to_csv with cfilename, .t.
	
	cFilenamein = lcPath+cFilename
	cFilenameout = lcPath+JUSTSTEM(cFilename)+".csv"	
	SET STEP ON 
	Appe From "&cFilenameout" Type Deli
	Delete File "&tfilepath"
	Delete File "&cFilenameout"
	Delete  From t1 Where Style='SKU'
	DELETE FROM t1 where EMPTY(style)

	Select upcmast
	Scatter Memvar Memo Blank
	Select t1
	?Datetime()
	Scan
		Scatter Memvar Memo
*		m.adddt=Datetime()
		m.updatedt=Datetime()
		m.addby=zuserid
		m.updateby=zuserid
		m.addproc='SYNC832'
		m.updproc='SYNC832'
		If !Empty(Style)
			Select upcmast
			Locate For Alltrim(Style)=Alltrim(t1.Style)
			If !Found()
				m.upcmastid=sqlgenpk("upcmast","wh")
				Insert Into upcmast From Memvar
				Replace Descrip           With  t1.Desc   In upcmast
				Replace updatedt        With Datetime()   In upcmast
				Replace adddt        With Datetime()   In upcmast
				Replace upc     With t1.Style  In upcmast
				Replace updproc With 'SYNC832' In upcmast
				Replace accountid With 6521
				If t1.xtype='E'
					Replace pnp With .T.
				Else
					Replace pnp With .F.
				Endif
				Release M.upcmastid
			Else
				Replace Descrip           With  t1.Desc   In upcmast
				Replace updatedt        With Datetime()   In upcmast
				Replace upc     With t1.Style  In upcmast
				Replace updproc With 'SYNC832' In upcmast
				If t1.xtype='E'
					Replace pnp With .T.
				Else
					Replace pnp With .F.
				Endif
			Endif
		Else

		Endif

	ENDSCAN
	?Datetime()
	tu("upcmast")

ENDFOR


*********************************start csv processing
RecCtr=0
len1 = Adir(ary1,lcPath+"*.csv")
SET STEP ON 
If len1 = 0
	Wait Window "No files found...exiting" Timeout 2
	Close Data All
	schedupdate()
	_Screen.Caption=gscreencaption
	On Error
	Return
Endif
For i = 1 To len1
	cFilename = Alltrim(ary1[i,1])
	xfile = lcPath+cFilename
	cArchiveFile = (lcArchivePath+cFilename)
	cLoadFile = (lcPath+cFilename)
	Copy File [&cLoadFile] To [&cArchiveFile]

Create Cursor t1 (Style C(20), Desc C(50),xtype C(4))
	tfile=Alltrim(ary1[i,1])
	tfilepath=lcPath+Alltrim(ary1[i,1])
	cFilenamein = lcPath+cFilename
	cFilenameout = lcPath+JUSTSTEM(cFilename)+".csv"	
	SET STEP ON 
	Appe From "&cFilenameout" Type Deli
	Delete File "&tfilepath"
	Delete File "&cFilenameout"
	Delete  From t1 Where Style='SKU'
	DELETE FROM t1 where EMPTY(style)


	Select upcmast
	Scatter Memvar Memo Blank
	Select t1
	?Datetime()
	Scan
		Scatter Memvar Memo
*		m.adddt=Datetime()
		m.updatedt=Datetime()
		m.addby=zuserid
		m.updateby=zuserid
		m.addproc='SYNC832'
		m.updproc='SYNC832'
		If !Empty(Style)
			Select upcmast
			Locate For Alltrim(Style)=Alltrim(t1.Style)
			If !Found()
				m.upcmastid=sqlgenpk("upcmast","wh")
				Insert Into upcmast From Memvar
				Replace Descrip           With  t1.Desc   In upcmast
				Replace updatedt        With Datetime()   In upcmast
				Replace adddt        With Datetime()   In upcmast
				Replace upc     With t1.Style  In upcmast
				Replace updproc With 'SYNC832' In upcmast
				Replace accountid With 6521
				If t1.xtype='E'
					Replace pnp With .T.
				Else
					Replace pnp With .F.
				Endif
				Release M.upcmastid
			Else
				Replace Descrip           With  t1.Desc   In upcmast
				Replace updatedt        With Datetime()   In upcmast
				Replace upc     With t1.Style  In upcmast
				Replace updproc With 'SYNC832' In upcmast
				If t1.xtype='E'
					Replace pnp With .T.
				Else
					Replace pnp With .F.
				Endif
			Endif
		Else

		Endif

	ENDSCAN
	?Datetime()
	tu("upcmast")

ENDFOR




Close Databases All

































