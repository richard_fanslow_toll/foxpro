Public fieldlist,lcreport_title

whichuser = "PAULG"

reportfor = "TKO"

Do case
 Case reportfor = "MORET"
  acctlist = "6262,6261,6088,5448,5453,5452,5741,5742,5743,5447,5451,5449,6255"
  lcReport_title ="ModernShoe/HighlineUnited 945 Status Report"

 Case reportfor = "MODERNSHOE/HIGHLINE UNITED"
  acctlist = "ModernShoe/HighlineUnited 945 Status Report"
  lcReport_title ="ModernShoe/HighlineUnited 945 Status Report"

 Case reportfor = "NANJING"
  acctlist = acctlist = "4610,4694"
  lcReport_title ="Nanjing 945 Status Report"

 Case reportfor = "TKO"
  acctlist = "5865,5836"
  lcReport_title ="TKO 945 Status Report"
  
EndCase   

Set Exclusive Off
Set Status off
Set Talk off
Set Safety Off
Close Data All
Set Deleted On
Set EngineBehavior 70
Wait Window At 10,10 "Parsing the FTP logs... Please wait......." Nowait
numdays = 5

Do parse_all_ftp_logs
Close Data All

*Use F:\wh7\whdata\outship In 0 Alias outship_sp

Use F:\wh2\whdata\outship In 0 Alias outship_sp

Use F:\whm\whdata\outship In 0 Alias outship_fl

cEDIFolder = "F:\3PL\DATA\"
Use (cEDIFolder+"edi_trigger") In 0 Alias edi_trigger Shared

Use h:\fox\ftplog In 0

Wait Window At 10,10 "Loading in the outship records from SP..... Please wait......." Nowait

SELECT wo_num AS FMIWorkOrder,consignee,ship_ref as pt,accountid,Space(10) acctname,wo_date AS workorderdate,"CA" AS office,bol_no AS BOL,scac,START,CANCEL,del_date,appt,SPACE(32) AS ShipmentFile,00000 as printorder,;
 SPACE(15) AS EDIStatus,SPACE(15) AS UA_Status,SPACE(20) AS Xfertimetxt,{  /  /       :  :  } AS trigtime,{  /  /       :  :  } AS proctime, {  /  /       :  :  } AS xfertime,SPACE(8) AS hour_delay, ;
 0000 as bol_pts, 0000 as edi_pts, Space(10) as ptcounterror ;
 FROM outship_sp WHERE Inlist(accountid,&acctlist) AND (appt = DATE() OR EMPTYnul(del_date) OR del_date >= DATE()-numdays) INTO CURSOR bols READWRITE group by bol_no order by cancel

SELECT bols
Goto top
DELETE FOR workorderdate < Date() -10
Goto top
Delete For Empty(bol)
Goto top
Delete For bol = "NOTHING SHIPPED"

Wait Window At 10,10 "Loading in the outship records from SP where Nothing Shipped..... Please wait......." Nowait

SELECT wo_num AS FMIWorkOrder,consignee,ship_ref as pt,accountid,Space(10) acctname,wo_date AS workorderdate,"CA" AS office,bol_no AS BOL,scac,START,CANCEL,del_date,appt,SPACE(32) AS ShipmentFile,00000 as printorder,;
 SPACE(15) AS EDIStatus,SPACE(15) AS UA_Status,SPACE(20) AS Xfertimetxt,{  /  /       :  :  } AS trigtime,{  /  /       :  :  } AS proctime, {  /  /       :  :  } AS xfertime,SPACE(8) AS hour_delay, ;
 0000 as bol_pts, 0000 as edi_pts, Space(10) as ptcounterror ;
 FROM outship_sp WHERE Inlist(accountid,&acctlist) AND (appt = DATE() OR EMPTYnul(del_date) OR del_date >= DATE()-numdays) and (BOL="NOTHING SHIPPED") INTO CURSOR bols_ns READWRITE order by cancel

SELECT bols_ns
Goto top
DELETE FOR workorderdate < Date() -10
replace all printorder with 9999

SELECT wo_num AS FMIWorkOrder,consignee,ship_ref as pt,accountid,Space(10) acctname,wo_date AS workorderdate,"FL" AS office,bol_no AS BOL,scac,START,CANCEL,del_date,appt,SPACE(32) AS ShipmentFile,00000 as printorder,;
 SPACE(15) AS EDIStatus,SPACE(15) AS UA_Status,SPACE(20) AS Xfertimetxt,{  /  /       :  :  } AS trigtime,{  /  /       :  :  } AS proctime, {  /  /       :  :  } AS xfertime,SPACE(8) AS hour_delay, ;
 0000 as bol_pts, 0000 as edi_pts, Space(10) as ptcounterror ;
 FROM outship_fl WHERE Inlist(accountid,&acctlist) AND (appt = DATE() OR EMPTYnul(del_date) OR del_date >= DATE()-numdays) INTO CURSOR bols_fl READWRITE group by bol_no order by cancel

SELECT bols_fl
Goto top
DELETE FOR workorderdate < Date() -10
Goto top
Delete For Empty(bol)

SELECT bols_ns
SCAN
  SELECT bols_ns
  SCATTER MEMVAR
  SELECT bols
  APPEND BLANK
  GATHER MEMVAR
ENDSCAN

SELECT bols_fl
SCAN
  SELECT bols_fl
  SCATTER MEMVAR
  SELECT bols
  APPEND BLANK
  GATHER MEMVAR
ENDSCAN
  
Wait Window At 10,10 "Updating the EDI Trigger information..... Please wait......." Nowait
Select bols
Goto Top
Scan
  If !Empty(bols.BOL) and printorder !=9999
    Select edi_trigger
    Set Order To BOL
    Seek bols.BOL
    If Found()
      Replace bols.ShipmentFile With Justfname(file945)
      Replace bols.proctime With when_proc
      Replace bols.trigtime With trig_time
      Replace bols.EDIStatus With fin_status
      Count for bol = bols.bol to num_pts
      replace bols.edi_pts with num_pts in bols
    Endif
  Endif
Endscan

Wait Window At 10,10 "Updating the NS EDI Trigger information..... Please wait......." Nowait
Select bols
Goto Top
Scan
  If !Empty(bols.BOL) and printorder =9999
    Select edi_trigger
    Set Order To acct_pt
    Seek Alltrim(Str(bols.accountid))+bols.pt
    If Found()
      Replace bols.ShipmentFile With bols.pt
      Replace bols.proctime With when_proc
      Replace bols.trigtime With trig_time
      Replace bols.EDIStatus With fin_status
*      Count for bol = bols.bol to num_pts
      replace bols.edi_pts with 1 in bols
    Endif
  Endif
Endscan


Select bols
Scan
  If bols.office = "CA"
    Select outship_sp
    Count for outship_sp.bol_no = bols.bol to num_pts
     replace bols.bol_pts with num_pts in bols
  EndIf 
  If bols.office = "FL"
    Select outship_fl
    Count for outship_fl.bol_no = bols.bolo to num_pts
     replace bols.bol_pts with num_pts in bols
  EndIf 

EndScan 


Select bols
Goto top
Scan
  Do case
    Case accountid= 6262
      replace acctname with "Bozkee"
    Case accountid= 6261
      replace acctname with "CaribJoe"
    Case accountid= 6088
      replace acctname with "Danskin"
    Case accountid= 5448
      replace acctname with "Everlast"
    Case accountid= 5453
      replace acctname with "HBest"
    Case accountid= 5452
      replace acctname with "HPD"
    Case accountid= 5741
      replace acctname with "JMI Kid"
    Case accountid= 5742
      replace acctname with "JMI Lad"
    Case accountid= 5743
      replace acctname with "JMI Men"
    Case accountid= 5447
      replace acctname with "Jockey"
    Case accountid= 5451
      replace acctname with "SBH"
    Case accountid= 5449
      replace acctname with "Scanty"
    Case accountid= 6255
      replace acctname with "Spaldin"
    Case accountid= 4677
      replace acctname with "Courtds"
    Case accountid=4610
      replace acctname with "Nan-P&P"
    Case accountid= 4694
      replace acctname with "Nan-Pre"
    Case accountid=5865
      replace acctname with "TKO-P&P"
    Case accountid= 5836
      replace acctname with "TKO-Pre"
    Case accountid=5910
      replace acctname with "HL-P&P"
    Case accountid=5864 
      replace acctname with "HL-Pre"
    Case accountid=4752
      replace acctname with "MSh-P&P"
    Case accountid=5318 
      replace acctname with "MSh-Pre"
    Otherwise
      replace acctname with "UNK"
  EndCase 

endscan

Scan
  Select ftplog
  Locate For Upper(Alltrim(bols.ShipmentFile))$Upper(ftplog.filename)
  If Found()
    Replace bols.UA_Status With "PickedUp" In bols
    Replace bols.Xfertimetxt With ftplog.xfertime In bols
  Else
    Replace bols.UA_Status With "NO" In bols
  Endif
Endscan

Select bols
Goto Top
Scan
  Replace xfertime With txttotime(Xfertimetxt)
  Replace hour_delay With Str((xfertime-trigtime)/3600,5,2)
Endscan

Select bols
Goto Top

Select * from bols order by del_date,appt descending into cursor bols2 readwrite
Select bols2
i=99999
Scan
  replace printorder with i
  i=i-1
  If edi_pts != bol_pts
    replace ptcounterror with alltrim(Str(bol_pts))+[,]+alltrim(Str(edi_pts))
  Else
    replace ptcounterror with alltrim(Str(bol_pts))+[,]+alltrim(Str(edi_pts))
  EndIf 
EndScan 

Select bols2 
Index on printorder tag porder ascending
Set Order To porder

rptok = .F.


Do reporttopdf With "Moret945report_RevB","h:\fox\Moret945Report",rptok

Export To c:\tempfox\Moret_945_Status.Xls Type Xls

Wait Window At 10,10 "Sending the Email..... Please wait......." Timeout 1

* email for those with the attachment
tsendto = "pgaidis@fmiint.com"
tcc = ""
tsubject = "Updated Moret 945 Status"
tmessage = "See attached report file" + Chr(13) + Chr(13) + " For content or distribution changes contact "+Chr(13)+" Paul Gaidis 732-750-9000 x143"+Chr(13)+Chr(13)
tmessage = tmessage

tattach  = "h:\fox\Moret945Report.pdf"
tfrom    ="TGF EDI Processing Center <transload-ops@fmiint.com>"
Do Form m:\dev\FRM\dartmail2 With tsendto,tfrom,tsubject,tcc,tattach,tmessage,"A"
