**Custom Order Cycle Time KPI for Marc Jacobs

**Time from the lastest of start date & order drop date till the freight was staged
**  - Based on max(outShip.start-3bus. days, outShip.ptDate, outship.wo_date) vs. outShip.staged
**  - Keep track of how many of the staged were automatically entered by the system (outShip.manstage)
**  
**  - No lead time: wos that have a greater ptDate than start-3 (3 bus days)
**  -   Also, do not need to compare wo_date in for the acceptedDt, just max(start-3, ptDate)

cAcctFilter = Iif(Empty(cWhseAcct), "", "o."+cWhseAcct)

store "" to xfile1, xfile2, xfile3
csubject="MJ Order Cycle Time - "+transform(dstartdt)+" - "+transform(denddt)
xrptcnt=3
if xrpttype="WIP"
	csubject="MJ Undelivered WIP Report"
	xrptcnt=1
endif

**run and send all 3 reports together (Allocated to staged, Staged to Shipped, Allocated to shipped)
**only run once for undelievered wip
for xrpt = 1 to xrptcnt
	do case
	case xrpttype="WIP"
		xrptname="wip"
	case xrpt=1
		xrptname="oct - allocated to staged"
	case xrpt=2
		xrptname="oct - allocated to shipped"
	case xrpt=3
		xrptname="oct - staged to shipped"
	endcase

	Wait window "Gathering data for "+xrptname+" ("+transform(dstartdt)+" - "+transform(denddt)+") Spreadsheet..." nowait noclear

	xdatefilter="Between(o.staged, dStartDt, dEndDt) and !empty(o.staged)"
	xdate1="acceptedDt"
	xdate2="o.staged"
	xdesc="Staged Date Vs. Allocation Date"
	do case
	case inlist(lower(xrptname),"oct - allocated to shipped","oct - staged to shipped")
		xdatefilter="Between(o.del_date, dStartDt, dEndDt) and !empty(o.del_date)"
		xdate1=iif(lower(xrptname)="oct - staged to shipped","staged",xdate1)
		xdate2="o.del_date"
		xdesc=iif(lower(xrptname)="oct - allocated to shipped","Shipped Date Vs. Allocation Date","Shipped Date Vs. Staged Date")
	case lower(xrptname)="wip"
		**for undelivered wip, get all undelieverd pts and date range is staged (if not staged yet date()) - allocation
		xdatefilter="empty(o.del_date)"
		xdate2="iif(empty(o.staged),date(),o.staged)"
	endcase
set step on 
	**bc alloc2ship and stage2ship are the same query, first pull data into csrdetail1 and then create csrdetail (readwrite) from it 
	** and manipulate data in csrdetail as needed... that way only have to run query for csrdetail1 once for both reports - mvw 08/14/13
	**addded outwolog into query to removed all outwolog.unbilled=.t. - mvw 09/11/13
	if xrpt#3
		xsqlexec("select outwologid, unbilled from outwolog where wo_date>{"+dtoc(date()-240)+"}",,,"wh")
		index on outwologid tag outwologid
		set order to
		xsqlexec("select outwologid, outshipid, wo_num, wo_date, accountId, staged, ship_ref, manStage, consignee, start, cancel, " + ;
			"ptDate, del_date, ctnqty, sp, notonwip from outship where wo_date>{"+dtoc(date()-240)+"}",,,"wh")
		index on outshipid tag outshipid
		index on accountid tag accountid
		set order to
		xsqlexec("select outshipid, totqty, units, style, color, id from outdet where wo_date>{"+dtoc(date()-240)+"}",,,"wh")
		index on outshipid tag outshipid
		set order to
	
		Select o.wo_num, o.wo_date, o.accountId, a.acctName, &xdate2 as rangeDt, o.ship_ref, o.manStage, o.consignee, ;
				o.start, o.start as oldstart, o.cancel, o.ptDate, o.ptDate as acceptedDt, o.staged, o.del_date, d.totqty as unitqty, o.ctnqty, 00 as dateDiff, ;
				style, color, id, space(10) as div, w.unbilled ;
			from outship o ;
				left join outdet d on d.outshipid=o.outshipid ;
				left join outwolog w on w.outwologid=o.outwologid ;
				left join account a on a.accountId = o.accountId ;
			where &cAcctFilter &xdatefilter and (!empty(o.ptdate) or !empty(o.start)) and !o.sp and !o.notonwip and d.units and !w.unbilled ;
			order by a.acctName, rangedt, o.wo_num ;
		  into cursor csrdetail1
	endif

	select * from csrdetail1 into cursor csrdetail readwrite

	**look up division in upcmast
	scan
		upcmastsql(csrdetail.accountid,csrdetail.style,csrdetail.color,csrdetail.id)
		
		if !found()
			replace div with "NA" in csrdetail
		else
			select upcmast
			xdiv=getmemodata("info","DIV")
			xdiv=iif(xdiv#"NA",xdiv,getmemodata("info","VENDCODE"))
			replace div with xdiv in csrdetail
		endif
	endscan

	locate

	**NOTE: if you are updating entire cursor in kpichgdate, must be at the top of the cursor as it does a do while !eof()
	=kpichgdate("start", -3, .t.) &&change start date to start - 3 bus. days

	**added for staged to shipped, if empty staged set staged to del_date - mvw 10/09/12
	replace staged with del_date for emptynul(staged)

	cComment = " - Allocation Date is the latest of the w/o creation date, "+;
		"order drop date & start date less 3 bus. days."

	**acceptedDt is the latest of ptDate, wo_date and (start - 3 bus. days)  - start changed back 3 days above
	replace all accepteddt with iif(ptdate>start, iif(ptdate>wo_date, ptdate, wo_date), ;
		iif(start>wo_date, start, wo_date))
	locate

	**calculate date differential into dateDiff - make necessary adjustments for weekends & FMI holidays
	**changed to use rangedt field for wip report addition
**	DO kpicalcdiff with xdate1, xdate2, "dateDiff"
	DO kpicalcdiff with xdate1, "rangedt", "dateDiff"

	Locate
	If Eof()
		if !xnoalert
			strMsg = "No data found."
			=MessageBox(strMsg, 48, xrptname)
		endif
	Else
		Wait window "Creating "+xrptname+" ("+transform(dstartdt)+" - "+transform(denddt)+") Spreadsheet..." nowait noclear

		oWorkbook=""
		cTitle="Order Cycle Time"+right(xrptname,len(xrptname)-3)
		if lower(xrptname)="wip"
			cTitle=csubject
		endif

		xcommentcol="'Manually entered staged dates: '+Alltrim(Str(totManStage))+'/'+Alltrim(Str(totCnt))+' = '+Alltrim(Str((totManStage/totCnt)*100,5,1))+'%'"
		xcommentcol=iif(lower(xrptname)="oct - allocated to shipped","''",xcommentcol)

		DO xlsTimeGenericMJ with "", "rangeDt", iif(xunits,"unitqty","ctnQty"), "sum(Iif(manStage, "+iif(xunits,"unitqty","ctnQty")+", 0)) as totManStage", cTitle, ;
			xdesc, cComment, " - Totals are calculated in "+iif(xunits,"units","cartons")+".", ;
			xcommentcol, .t., .t.

		if ldetail
			nworksheet = 2
			store tempfox+substr(sys(2015), 3, 10)+".xls" to ctmpfile
			Select *, Iif(manStage, "Yes", "No") as manual from csrDetail order by acctname, div, staged into cursor csrDet
			Copy to &cTmpFile fields acctName, div, wo_num, wo_date, ship_ref, consignee, unitqty, ctnQty, acceptedDt, cancel, staged, del_date, dateDiff, manual type xl5

			DO xlsTimeGenDetail with cTmpFile, nWorkSheet, "L"

			oWorkbook.worksheets[nWorksheet].Range("A"+Alltrim(Str(Reccount("csrDet")+5))).font.color = Rgb(255,0,0) &&red
			oWorkbook.worksheets[nWorksheet].Range("A"+Alltrim(Str(Reccount("csrDet")+5))).Value = ;
				"Note: 'Manual' column indicates a manually entered stage date."

			nworksheet = nworksheet+1
			use in csrdet
		endif

		oWorkbook.Worksheets[1].range("A1").activate()

		oworkbook.CheckCompatibility = .f.
		oExcel.DisplayAlerts = .f.

		oWorkbook.Save()
	EndIf
endfor

use in outship
use in outdet
if used("upcmast")
	use in upcmast
endif
use in csrdetail1
use in csrdetail
if used("csrtotals")
  use in csrtotals
endif

**copy file to LAN - mvw 08/14/13
if !empty(xfile1) &&empty if no data - 04/04/14 mvw
	copy file "&xfile1" to S:\MarcJacobsData\Reports\KPI\*.*
	cemailfile=xfile1
	if xrptcnt>1
		cemailfile=xfile1+","+xfile2+","+xfile3
		copy file "&xfile2" to S:\MarcJacobsData\Reports\KPI\*.*
		copy file "&xfile3" to S:\MarcJacobsData\Reports\KPI\*.*
	endif
endif

wait clear

return


*********************************************************************************
**PROCEDURES
*********************************************************************************


Procedure xlsTimeGenericMJ
**create worksheet from generic "time" excel sheet
Parameters cFilter, cDateField, cCntField, cAddFields, cRptName, cRptDesc, cAddComment, cAddComment2, cCommentCol, lNotes, lAcctTotals
**lCtnCnt: if true, indicates that totals should be tot # ctns as opposed to tot # of recs in csrDetail
**cAddFields: additional fields needed in csrTotals (with leading ",")

	xxlsfile=iif(type("xxlsfile")="U","kpiTimeGenericMJ.XLS",xxlsfile)

	do case
	case xrpt=1
		store tempfox+strtran(transform(date()),"/")+"_"+iif(type("xrpttype")#"C","",xrpttype+"_")+"alloc2stage.xls" to cfile
		xfile1=cfile
	case xrpt=2
		store tempfox+strtran(transform(date()),"/")+"_"+iif(type("xrpttype")#"C","",xrpttype+"_")+"alloc2ship.xls" to cfile
		xfile2=cfile
	case xrpt=3
		store tempfox+strtran(transform(date()),"/")+"_"+iif(type("xrpttype")#"C","",xrpttype+"_")+"stage2ship.xls" to cfile
		xfile3=cfile
	endcase
	**copying of an included file does not work, need to use StrToFile(FilToStr())
	=StrtoFile(FileToStr(xxlsfile), cfile)

	conerror = on("error")
	oworkbook = oexcel.workbooks.open(cfile)

	oworkbook.worksheets[1].range("A2").value = cRptName+" ("+Dtoc(dStartDt)+" - "+Dtoc(dEndDt)+"): "+;
		cAcctName+" ("+Iif(cOffice="C","SP",Iif(cOffice="L","ML",Iif(cOffice="M", "FL", "NJ")))+")"
	oworkbook.worksheets[1].range("C3").value = cRptDesc

	**cusror csrTotals is created within xlsTimeGenSummary()
	DO xlsTimeGenSummaryMJ with cFilter, cDateField, cCntField, cAddFields, cAddComment, cAddComment2, cCommentCol, lNotes, "dateDiff", "5", lAcctTotals

	If lDetail
		oWorkSheet = oworkbook.worksheets.add(,oworkbook.worksheets[1])
		oWorkSheet.name = "Detail"
	EndIf

	oWorkbook.Worksheets[1].activate()

Return &&End xlsTimeGeneric



Procedure xlsTimeGenSummaryMJ
Parameters cFilter, cDateField, cCntField, cAddFields, cAddComment, cAddComment2, cCommentCol, lNotes, cDiffField, cStartRow, lAcctTotals
**cDiffField: specifies "dateDiff" field name - 2nd summary band (ie. edi timeliness) use datDiff2
**cStartRow: specifies start row - 2nd summary band (ie. edi timeliness) must be below the 1st (character)

	xxlsfile=iif(type("xxlsfile")="U","kpiTimeGeneric.XLS",xxlsfile)

	corder = "acctname, div"
	cAcctFlds = "accountid, acctname,"
	cgroupby = "accountid, div"
	cAddFields=iif(empty(cAddFields),"",", "+cAddFields)

	Select &cAcctFlds (week(&cDateField)-week(dstartdt))+1 as week, Month(&cDateField) as month, Year(&cDateField) as year, &cDateField as date, ;
		   sum(&cCntField) as totCnt, sum(Iif(&cDiffField<=0, &cCntField, 0)) as tot0, ;
		   sum(Iif(&cDiffField=1, &cCntField, 0)) as tot1, sum(Iif(&cDiffField=2, &cCntField, 0)) as tot2, ;
		   sum(Iif(&cDiffField=3, &cCntField, 0)) as tot3, sum(Iif(&cDiffField=4, &cCntField, 0)) as tot4, ;
		   sum(Iif(between(&cDiffField,5,6), &cCntField, 0)) as tot5, sum(Iif(between(&cDiffField,7,9), &cCntField, 0)) as tot6, ;
		   sum(Iif(between(&cDiffField,10,14), &cCntField, 0)) as tot7, sum(Iif(between(&cDiffField,15,19), &cCntField, 0)) as tot8, ;
		   sum(Iif(&cDiffField>=20, &cCntField, 0)) as tot9, div ;
		   &cAddFields ;
		from csrDetail group by &cgroupby order by &corder ;
		&cFilter ;
	  into cursor csrtotals

	Store &cStartRow to nRow, nStartRow
	Scan
		cRow = Alltrim(Str(nRow))
		oworkbook.worksheets[1].range("A"+cRow) = alltrim(acctname)+" / "+alltrim(div)
		oworkbook.worksheets[1].range("B"+cRow) = csrTotals.totCnt
		oworkbook.worksheets[1].range("C"+cRow) = csrTotals.tot0
		oworkbook.worksheets[1].range("D"+cRow) = "=C"+cRow+"/B"+cRow
		oworkbook.worksheets[1].range("E"+cRow) = csrTotals.tot1
		oworkbook.worksheets[1].range("F"+cRow) = "=E"+cRow+"/B"+cRow
		oworkbook.worksheets[1].range("G"+cRow) = csrTotals.tot2
		oworkbook.worksheets[1].range("H"+cRow) = "=G"+cRow+"/B"+cRow
		oworkbook.worksheets[1].range("I"+cRow) = csrTotals.tot3
		oworkbook.worksheets[1].range("J"+cRow) = "=I"+cRow+"/B"+cRow
		oworkbook.worksheets[1].range("K"+cRow) = csrTotals.tot4
		oworkbook.worksheets[1].range("L"+cRow) = "=K"+cRow+"/B"+cRow
		oworkbook.worksheets[1].range("M"+cRow) = csrTotals.tot5
		oworkbook.worksheets[1].range("N"+cRow) = "=M"+cRow+"/B"+cRow
		oworkbook.worksheets[1].range("O"+cRow) = csrTotals.tot6
		oworkbook.worksheets[1].range("P"+cRow) = "=O"+cRow+"/B"+cRow
		oworkbook.worksheets[1].range("Q"+cRow) = csrTotals.tot7
		oworkbook.worksheets[1].range("R"+cRow) = "=Q"+cRow+"/B"+cRow
		oworkbook.worksheets[1].range("S"+cRow) = csrTotals.tot8
		oworkbook.worksheets[1].range("T"+cRow) = "=S"+cRow+"/B"+cRow
		oworkbook.worksheets[1].range("U"+cRow) = csrTotals.tot9
		oworkbook.worksheets[1].range("V"+cRow) = "=U"+cRow+"/B"+cRow
		oworkbook.worksheets[1].range("W"+cRow) = &cCommentCol

		oworkbook.worksheets[1].range(Chr(97)+cRow).borders(7).linestyle = 1
		For nCol = 97 to 119 &&a-w
		  oworkbook.worksheets[1].range(Chr(nCol)+cRow).borders(10).linestyle = Iif(!InList(nCol,99,101,103,105,107,109,111,113,115,117), 1, 0)
		  oworkbook.worksheets[1].range(Chr(nCol)+cRow).borders(9).linestyle = Iif(Recno()=Reccount(), 1, 0)
		EndFor

		nRow=nRow+1
	EndScan

	If Reccount() > 1
		nRow = Reccount()+nStartRow

		cRow = Alltrim(Str(nRow))
		oworkbook.worksheets[1].range(Chr(97)+cRow).borders(7).linestyle = 1
		For i = 97 to 119 &&a-w
		  oworkbook.worksheets[1].range(Chr(i)+cRow).borders(8).linestyle = 1
		  oworkbook.worksheets[1].range(Chr(i)+cRow).borders(9).linestyle = 1
		  oworkbook.worksheets[1].range(Chr(i)+cRow).borders(10).linestyle = Iif(!InList(i,99,101,103,105,107,109,111,113,115,117), 1, 0)
		EndFor

		nRow=nRow+1
		cRow = Alltrim(Str(nRow))
		oworkbook.worksheets[1].range("A"+cRow) = "Total"
		oworkbook.worksheets[1].range("B"+cRow) = "=sum(B"+Alltrim(Str(nStartRow))+":B"+Alltrim(Str(Reccount()+nStartRow-1))+")"
		oworkbook.worksheets[1].range("C"+cRow) = "=sum(C"+Alltrim(Str(nStartRow))+":C"+Alltrim(Str(Reccount()+nStartRow-1))+")"
		oworkbook.worksheets[1].range("D"+cRow) = "=C"+cRow+"/B"+cRow
		oworkbook.worksheets[1].range("E"+cRow) = "=sum(E"+Alltrim(Str(nStartRow))+":E"+Alltrim(Str(Reccount()+nStartRow-1))+")"
		oworkbook.worksheets[1].range("F"+cRow) = "=E"+cRow+"/B"+cRow
		oworkbook.worksheets[1].range("G"+cRow) = "=sum(G"+Alltrim(Str(nStartRow))+":G"+Alltrim(Str(Reccount()+nStartRow-1))+")"
		oworkbook.worksheets[1].range("H"+cRow) = "=G"+cRow+"/B"+cRow
		oworkbook.worksheets[1].range("I"+cRow) = "=sum(I"+Alltrim(Str(nStartRow))+":I"+Alltrim(Str(Reccount()+nStartRow-1))+")"
		oworkbook.worksheets[1].range("J"+cRow) = "=I"+cRow+"/B"+cRow
		oworkbook.worksheets[1].range("K"+cRow) = "=sum(K"+Alltrim(Str(nStartRow))+":K"+Alltrim(Str(Reccount()+nStartRow-1))+")"
		oworkbook.worksheets[1].range("L"+cRow) = "=K"+cRow+"/B"+cRow
		oworkbook.worksheets[1].range("M"+cRow) = "=sum(M"+Alltrim(Str(nStartRow))+":M"+Alltrim(Str(Reccount()+nStartRow-1))+")"
		oworkbook.worksheets[1].range("N"+cRow) = "=M"+cRow+"/B"+cRow
		oworkbook.worksheets[1].range("O"+cRow) = "=sum(O"+Alltrim(Str(nStartRow))+":O"+Alltrim(Str(Reccount()+nStartRow-1))+")"
		oworkbook.worksheets[1].range("P"+cRow) = "=O"+cRow+"/B"+cRow
		oworkbook.worksheets[1].range("Q"+cRow) = "=sum(Q"+Alltrim(Str(nStartRow))+":Q"+Alltrim(Str(Reccount()+nStartRow-1))+")"
		oworkbook.worksheets[1].range("R"+cRow) = "=Q"+cRow+"/B"+cRow
		oworkbook.worksheets[1].range("S"+cRow) = "=sum(S"+Alltrim(Str(nStartRow))+":S"+Alltrim(Str(Reccount()+nStartRow-1))+")"
		oworkbook.worksheets[1].range("T"+cRow) = "=S"+cRow+"/B"+cRow
		oworkbook.worksheets[1].range("U"+cRow) = "=sum(U"+Alltrim(Str(nStartRow))+":U"+Alltrim(Str(Reccount()+nStartRow-1))+")"
		oworkbook.worksheets[1].range("V"+cRow) = "=U"+cRow+"/B"+cRow

		oWorkbook.worksheets[1].Range("B"+cRow+":"+"V"+cRow).Interior.Color = RGB(255,255,128)

		oworkbook.worksheets[1].range(Chr(97)+cRow).borders(7).linestyle = 1
		For i = 97 to 119 &&a-w
		  oworkbook.worksheets[1].range(Chr(i)+cRow).borders(8).linestyle = 1
		  oworkbook.worksheets[1].range(Chr(i)+cRow).borders(9).linestyle = 1
		  oworkbook.worksheets[1].range(Chr(i)+cRow).borders(10).linestyle = Iif(!InList(i,99,101,103,105,107,109,111,113,115,117), 1, 0)
		EndFor
	EndIf

	If lNotes
		oworkbook.worksheets[1].range("A"+Alltrim(Str(Reccount()+nStartRow+7)),"A"+Alltrim(Str(Reccount()+nStartRow+10))).font.color = Rgb(255,0,0) &&red
		oworkbook.worksheets[1].range("A"+Alltrim(Str(Reccount()+nStartRow+7))) = "Notes:"
		oworkbook.worksheets[1].range("A"+Alltrim(Str(Reccount()+nStartRow+8))) = " - Totals exclude weekends & holidays."
		oworkbook.worksheets[1].range("A"+Alltrim(Str(Reccount()+nStartRow+9))) = cAddComment
		oworkbook.worksheets[1].range("A"+Alltrim(Str(Reccount()+nStartRow+10))) = cAddComment2
	EndIf

Return &&End xlsTimeGenSummary
