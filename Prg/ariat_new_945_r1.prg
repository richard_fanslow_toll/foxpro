*!* ARIAT 945 (Whse. Shipping Advice) SCANPACK Creation Program
*!* Creation Date: 08.12.2014 by Joe (Derived from Ariat945_create program)
*!* The current version contains changes to provide for long tracking number insertion (has a backup prg copy, dated 20170608 for restoration if needed)

PARAMETERS cBOL,cPickticket

*WAIT WINDOW "Now at start of Ariat Scanpack 945 process" TIMEOUT 10
*SET STEP ON
PUBLIC ARRAY thisarray(1)
PUBLIC c_CntrlNum,c_GrpCntrlNum,cReturned,cWO_Num,cWO_NumStr,nWO_Num1,lTesting,lTestInput,lAmazon,lUsePackageData,nDecrementQty,lcPath,lcArchivePath
PUBLIC lEmail,dDateTimeCal,nFilenum,lScanpack,nWO_Xcheck,lDoTotqtyCheck,cTrkNumber,cErrMsg,cCustPrefix,nTotCtnWt,nLoops,nODetQty,lDoCompare,cShip_ref
PUBLIC cCity,cState,cZip,nOriFgSeq,nOrigGrpSeq,tsendto,tcc,tsendtoerr,tccerr,cProgname,cISA_Num,cChkDate,lAddCharges,cLinenumSD,nSQLQty,m.linenum,lDoScan,cPRONum
PUBLIC nLength,nWidth,nDepth,nWO_Num,lDoAriatFilesOut,tsendtotest,tcctest,nAcctNum1,nAcctNum2,lUseBOL,cTrknumberDet,lTestProd,cMatchUCC,lMasterSubdetailInBOL
PUBLIC cMod,cMBOL,cFilenameShort,cFilenameOut,cFilenameArch,lParcelType,cWO_NumList,lcPath,cEDIType,cISA_Num,lDoSQL,lLoadSQLBL,lDoBTCharges
PUBLIC nFilenum
WAIT WINDOW "" TIMEOUT 1

cOffice = "K"
cMod = cOffice
cMBOL = ""
lParcelType = .F.
cEDIType = "945"
gMasteroffice= cOffice
gOffice = cOffice
lDoSQL = .T.
lLoadSQLBL = .T.
nCharge =0.0
m.linenum = ""

cErrMsg = "EARLY ERROR"

SELECT 0
USE F:\wh\sqlpassword
cSQLPass = ALLTRIM(sqlpassword.PASSWORD)
USE IN sqlpassword

*!*	OPEN DATABASE m:\dev\wh
*!*	set database to wh
*!*	scacenter()

* to do testing, set the next 2 options to .t. and hard code in the bol later down in the code

lTesting   = .f. && Set to .t. for full testing
lTestInput =  .F.  &&  Set to .t. to use test input files only!
lTestProd = .F. && Allows testing using triggers and pre-delivered production data
lDoCompare = !lTestInput && Cross-checks SQL totals in Labels vs Cartons

lDoAriatFilesOut = .T.  && Default = .t.
lDoScan = !lTesting
lEmail = .T.  && Default = .t.
lTestMail = IIF(lTesting OR lTestInput,.T.,.F.) && Sends mail to Joe only
*lTestMail = .t.

*set step On
DO m:\dev\prg\_setvars WITH lTesting

cProgname = "ariat945_create"
lUseBOL = .F.
lUsePackageData = .T.

IF lTesting
	CLOSE DATABASES ALL
ENDIF

CLEAR
lDoTotqtyCheck = .T.  && In general, this should not be changed.
lPick = .F.
lPrepack = .F.
lJCP = .F.
lIsError = .F.
lDoCatch = .T.
cShip_ref = ""
lCloseOutput = .T.
nFilenum = 0
nWO_Num = 0
ccustname = ""
STORE "" TO cWO_NumStr,cWO_NumList
cTrknumberDet = ""
cString = ""
lAddCharges = .F.

WAIT WINDOW "At the start of ARIAT 945 (all types) process..." NOWAIT
*!* Note: For Wal-mart testing, be sure that there is a BOL and DEL_DATE filled in on the test OUTSHIP table,
*!* and that it matches the test data below

TRY
	IF lTesting
		CLOSE DATABASES ALL
		DO m:\dev\prg\lookups
		IF !USED('edi_trigger')
			cEDIFolder = "F:\3PL\DATA\"
			USE (cEDIFolder+"edi_trigger") IN 0 ALIAS edi_trigger
		ENDIF

*!* TEST DATA AREA*
		cBOL =  "92612979381558250029"
		cPickticket = '0085540352'
		cTime = DATETIME()
	ENDIF

	lFederated = .F.
	nAcctNum = 6532
	nAcctNum1 = 16532
	nAcctNum2 = 26532
	nOrigSeq = 0
	nOrigGrpSeq = 0
	cPPName = "Ariat"
	tfrom = "Toll EDI Operations <toll-edi-ops@tollgroup.com>"
	tmessage=""
	tcc = ""
	lDoBOLGEN = .F.
	lSamples = .F.
	cBOL1 = ""
	lISAFlag = .T.
	lSTFlag = .T.
	nLoadid = 1
	lSQLMail = .F.
	lFedEx = .F.
	cUPC = ""

	CREATE CURSOR temp945 (accountid i,isa_num c(10),st_num c(10),wo_num i,bol_no c(20),ship_ref c(20),filename c(50),filedate T)
	cBOL=TRIM(cBOL)
	xReturn = "XXX"
	IF lTestInput
		cUseFolder = "F:\WHP\WHDATA\"
	ELSE
		DO m:\dev\prg\wf_alt WITH cOffice,nAcctNum
		cUseFolder = UPPER(xReturn)
	ENDIF
	cRetMsg = "X"

*********************
*!* Don't change these flags until testing is complete!!!
*!*		lTestMail = .T.
*!*		lDoAriatFilesOut = .F.
*********************

	STORE "LB" TO cWeightUnit
	STORE "CF" TO cVolUnit
	cfd = "*"
	csegd = "~"
	nSegCtr = 0
	cterminator = ">" && Used at end of ISA segment

	WAIT WINDOW "Now preparing tables and variables" NOWAIT NOCLEAR

*!* SET CUSTOMER CONSTANTS
	ccustname = "ARIAT"  && Customer Identifier
	cX12 = "004010"  && X12 Standards Set used
	cMailName = "Ariat"
	SELECT 0
	USE F:\3pl\DATA\mailmaster ALIAS mm
	LOCATE FOR mm.edi_type = "945" AND mm.office = cOffice AND mm.accountid = nAcctNum
	tsendto = IIF(mm.use_alt,mm.sendtoalt,mm.sendto)
	tcc = IIF(mm.use_alt,mm.ccalt,mm.cc)
	lcPath = ALLT(mm.basepath)
	lcArchivePath = ALLT(mm.archpath)
	lcholdpath = ALLT(mm.holdpath)
	LOCATE
	LOCATE FOR mm.edi_type = 'MISC' AND mm.taskname = "GENERAL"
	tsendtoerr = IIF(mm.use_alt,mm.sendtoalt,mm.sendto)
	tccerr = IIF(mm.use_alt,mm.ccalt,mm.cc)
	tsendtotest = IIF(mm.use_alt,mm.sendtoalt,mm.sendto)
	tcctest = IIF(mm.use_alt,mm.ccalt,mm.cc)
	USE IN mm
	tcc = IIF(lTesting,"",tcc)

*!* SET OTHER CONSTANTS
	cCustLoc =  "KY"
	cfmiWarehouse = ""
	cCustPrefix = "945k"
	cDivision = "Kentucky"
	cFolder = "WHK"
	cSF_Addr1  = "7501 WINSTEAD DRIVE"
	cSF_CSZ    = "LOUISVILLE"+cfd+"KY"+cfd+"40258"
	cCustFolder = UPPER(ccustname)

	lNonEDI = .F.  && Assumes not a Non-EDI 945
	STORE "" TO lcKey
	STORE 0 TO alength,nLength
	cTime = DATETIME()
	cDelTime = SUBSTR(TTOC(cTime,1),9,4)
	cCarrierType = "M" && Default as "Motor Freight"
	dapptnum = ""

*!* SET OTHER CONSTANTS
	IF INLIST(cOffice,'C','1','2')
		dDateTimeCal = (DATETIME()-(3*3600))
	ELSE
		dDateTimeCal = DATETIME()
	ENDIF
	dt1 = TTOC(dDateTimeCal,1)
	dtmail = TTOC(dDateTimeCal)
	dt2 = dDateTimeCal
	cString = ""

	csendqual = "ZZ"
	csendid = "TGFPROD940"  && this will go to TGFPROD940 when in production
	csendidlong = PADR(csendid,15," ")
*!* Receiver variables are at the ISA loop (noted 10.01.2010)

	cdate = DTOS(DATE())
	cTruncDate = RIGHT(cdate,6)
	cTruncTime = SUBSTR(TTOC(dDateTimeCal,1),9,4)
	cfiledate = cdate+cTruncTime
	cOrig = "J"
	nSTCount = 0
	cStyle = ""
	cPTString = ""
	nTotCtnWt = 0
	nTotCtnVol = 0
	nTotCtnCount = 0
	nUnitSum = 0
	cTargetStyle = ""

	DO m:\dev\prg\swc_cutctns_gen WITH cBOL

** PAD ID Codes
	dDateTimeCal = DATETIME()
	dt1 = TTOC(dDateTimeCal,1)
	cFilenameHold = (lcholdpath+cCustPrefix+dt1+".txt")
	cFilenameShort = JUSTFNAME(cFilenameHold)
	lcPath = IIF(lTesting,"f:\ftpusers\ariat-test\945-staging\","f:\ftpusers\ariat\OUT\")

	IF lTesting &&And lTestInput
		cFilenameOut = (lcPath+cFilenameShort)  && Ariat's direct 945OUT folder
	ELSE
		cFilenameOut = (lcPath+cFilenameShort)
		cFilenameArch = (lcArchivePath+cFilenameShort)
	ENDIF

	nFilenum = FCREATE(cFilenameHold)
*	ASSERT .F. MESSAGE "At Ariat SQL scan"
	cSQL=IIF(lTestInput,"SQL4","SQL5")

	lcDSNLess="driver=SQL Server;server=&cSQL;uid=SA;pwd=&cSQLPass;DATABASE=PICKPACK"
	nHandle=SQLSTRINGCONNECT(m.lcDSNLess,.T.)
	SQLSETPROP(nHandle,"DispLogin",3)
	WAIT WINDOW "Now processing ARIAT PT SQL select...please wait" NOWAIT
	IF nHandle=0 && bailout
		WAIT WINDOW "Could not make SQL connection" TIMEOUT 3
		cErrMsg = "NO SQL CONNECT"
		DO ediupdate WITH cErrMsg,.T.
		THROW
	ENDIF

	IF !lTestInput
		IF USED('OUTWOLOG')
			USE IN outwolog
		ENDIF
	ENDIF

	IF !USED('shipcode')
		xsqlexec("select * from shipcode where .t.",,,"wh")
	ENDIF

	IF !USED('ariat_carriers')
		xsqlexec("select * from ariat_carriers",,,"wh")
		INDEX ON scac TAG scac
		INDEX ON NAME TAG NAME
		INDEX ON vendor TAG vendor
	ENDIF

	IF USED('ackdata')
		USE IN ackdata
	ENDIF

	useca("ackdata","wh")

	IF usesqlctn()
	ELSE
		IF !USED('CTNUCC')
			USE (cUseFolder+"CTNUCC") IN 0 ALIAS ctnucc
		ENDIF
	ENDIF

	IF USED('OUTSHIP')
		USE IN outship
	ENDIF

	csq1 = [select * from outship where accountid = ]+TRANSFORM(nAcctNum)+[ and office = ']+cOffice+[' and bol_no = ']+cBOL+[']
	xsqlexec(csq1,,,"wh")
	IF RECCOUNT() = 0
		cErrMsg = "MISS BOL "+cBOL
		DO ediupdate WITH cErrMsg,.T.
		THROW
	ENDIF
	INDEX ON bol_no TAG bol_no

	SET STEP ON 
	SELECT outship
	IF VARTYPE(cPickticket) = "C"
		cPickticket = PADR(ALLTRIM(cPickticket),20)
		IF SEEK(cPickticket,"outship","ship_ref")
			cSCAC = ALLTRIM(outship.scac)
			USE ("F:\3pl\DATA\parcel_carriers") IN 0 ORDER TAG scac
			IF SEEK(cSCAC,"parcel_carriers","scac")
				lParcelType = .T.
			ENDIF
			USE IN parcel_carriers
		ENDIF
	ENDIF
	
	LOCATE FOR outship.bol_no = cBOL AND 	IIF(lParcelType,outship.ship_ref = cPickticket,accountid = nAcctNum)
	lParcelType = IIF(ALLTRIM(outship.keyrec) == ALLTRIM(outship.bol_no),.F.,lParcelType)

	nWO_Num = outship.wo_num
	xxlcPT  =outship.ship_ref

	lScanpack = IIF("SCANPACK"$outship.shipins,.T.,.F.)
	lDoScan = IIF(lScanpack,.T.,.F.)

	IF !lTesting
*		ASSERT .f. MESSAGE "At WO Hold section"
		IF USED('ariat_wohold')
			USE IN ariat_wohold
		ENDIF
		SELECT 0
		USE F:\3pl\DATA\ariat_wohold
		nWO_numOld = ariat_wohold.wo_num
		IF nWO_Num = nWO_numOld
			lDoSQL = .F.
		ELSE
			REPLACE wo_num WITH nWO_Num IN ariat_wohold
		ENDIF
	ENDIF
	lDoSQL = .T.
	IF outship.carrcode = "UPSGCC"  && UPSGCC indicates that this is a Consolidated to Canada, so we need to load in the tracking numbers
*		WAIT WINDOW "" TIMEOUT 1
		RUN /N F:\UTIL\ARIAT\ARIAT_TRK_UCC_IMPORT.EXE
*		WAIT WINDOW "" TIMEOUT 1

*******************************************************************************************
		SELECT ship_ref FROM outship WHERE bol_no = cBOL INTO CURSOR temppts
		SELECT temppts
		lcErrorStr = ""
		llExportError = .F.
*		SET STEP ON
		SCAN
			ctemp = "VART"
			lcSQL= [SELECT distinct ucc from cartons where ship_ref = ']+temppts.ship_ref+[' and accountid = 6532]
			llSuccess=SQLEXEC(nHandle,lcSQL,ctemp)
			IF llSuccess=0  && no records 0 indicates no records and 1 indicates records found
				cErrMsg = "NO SQL CONNECT AT WORLDEASE CHECK "
				DO ediupdate WITH cErrMsg,.T.
				THROW
			ENDIF

			IF usesqlctn()
				IF xsqlexec("select * from ctnucc where mod='K' and ucc='"+VART.ucc+"'",,,"wh") = 0
					lcErrorStr = lcErrorStr +CHR(13)+"no data for delivery: "+temppts.ship_ref+"looking for ucc "+ VART.ucc
					llExportError=.T.
				ELSE
					IF EMPTY(ctnucc.REFERENCE) OR EMPTY(ctnucc.serialno) && ctnucc needs the small pakg charge--> ctnucc.reference and the trknumber --> ctnucc.serialno
						lcErrorStr = lcErrorStr +CHR(13)+temppts.ship_ref+"  UCC: "+ucc+"  Trk#: "+ctnucc.serialno+"  PkgChg: "+ctnucc.REFERENCE
						llExportError=.T.
					ENDIF
				ENDIF
			ELSE
				IF !SEEK(VART.ucc,"ctnucc","ucc")
					lcErrorStr = lcErrorStr +CHR(13)+"No data for Delivery: "+temppts.ship_ref+"looking for UCC "+ VART.ucc
					llExportError=.T.
				ELSE
					IF EMPTY(ctnucc.REFERENCE) OR EMPTY(ctnucc.serialno) && ctnucc needs the small pakg charge--> ctnucc.reference and the trknumber --> ctnucc.serialno
						lcErrorStr = lcErrorStr +CHR(13)+temppts.ship_ref+"  UCC: "+ucc+"  Trk#: "+ctnucc.serialno+"  PkgChg: "+ctnucc.REFERENCE
						llExportError=.T.
					ENDIF
				ENDIF
			ENDIF
		ENDSCAN

		IF llExportError
			tsubject = "ARIAT Worldease Export Error: "+TRIM(cBOL)
			tsendto = "cheri.foster@tollgroup.com,tony.sutherland@tollgroup.com,michael.sanders@tollgroup.com"
			tcc = "pgaidis@fmiint.com,joe.bianchi@tollgroup.com,tmarg@fmiint.com"
			tmessage = "Worldease Export data issues for following deliveries:"+CHR(13)+lcErrorStr
			tattach =""
			DO FORM m:\dev\frm\dartmail2 WITH tsendto,tfrom,tsubject,tcc,tattach,tmessage,"A"
			cErrMsg = "ERR-WORLDEASE CHK-UCC "+VART.ucc
			DO ediupdate WITH cErrMsg,.T.
			THROW
		ENDIF
	ENDIF
*******************************************************************************************
	IF USED('OUTDET')
		USE IN outdet
	ENDIF

	selectoutdet()
	SELECT outdet
	INDEX ON outdetid TAG outdetid

	IF USED('BL')
		USE IN bl
	ENDIF
	IF lLoadSQLBL
		csq1 = [select * from BL where bol_no = ']+cBOL+[']
		xsqlexec(csq1,,,"wh")
		SELECT bl
		INDEX ON bol_no TAG bol_no
	ELSE
		USE (cUseFolder+"BL") IN 0 ALIAS bl
	ENDIF

	IF lDoScan
		WAIT WINDOW "This is a SCANPACK Account..."+CHR(13)+"Must confirm No LABELS in SQL" NOWAIT

		SELECT wo_num,outshipid,shipins ;
			FROM outship ;
			WHERE bol_no = PADR(cBOL,20) ;
			AND accountid = nAcctNum ;
			AND IIF(lParcelType,outship.ship_ref = cPickticket,accountid = nAcctNum) ;
			INTO CURSOR tempsr
		SELECT tempsr
		LOCATE
		WAIT WINDOW "There are "+ALLTRIM(STR(RECCOUNT()))+" outshipid's to check" NOWAIT

		SELECT tempsr
		lCleanSQL = .T.

		SCAN FOR "SCANPACK"$shipins  && only verify the actual scanpacked pts, pts can be mixed on a BOL
			STORE ALLTRIM(STR(tempsr.outshipid)) TO cOSID
			ctemp = "VART"
			lcQ1 = [SELECT * FROM dbo.labels Labels]
			lcQ2 = [ WHERE Labels.outshipid = ]
			lcQ3 = " &cOSID "
			lcQ4 = [ AND Labels.accountid = ]
			lcQ5 = "6532"
			lcSQL = lcQ1+lcQ2+lcQ3+lcQ4+lcQ5
			llSuccess=SQLEXEC(nHandle,lcSQL,ctemp)
			IF llSuccess=0  && no records 0 indicates no records and 1 indicates records found
				cErrMsg = "NO SQL CONNECT"
				DO ediupdate WITH cErrMsg,.T.
				THROW
			ENDIF
			SELECT VART
			IF RECCOUNT() = 0
				WAIT WINDOW "No "+ccustname+" records found in LABELS for OSID "+cOSID+" OK...proceeding" NOWAIT
			ELSE
				MESSAGEBOX("RECORDS EXIST IN SQL LABELS"+CHR(13)+"FOR OSID "+cOSID+CHR(13)+"CAN'T PROCEED",16,"SQL LABEL RECORD TRAP",4000)
				lCleanSQL = .F.
				ASSERT .F. MESSAGE "SQL Labels records found"
				cErrMsg = "RECS FOUND IN SQL LABELS"
				DO ediupdate WITH cErrMsg,.T.
				THROW
			ENDIF
		ENDSCAN
		WAIT WINDOW "All Outshipids within this BOL are scanned...proceeding" NOWAIT
		SQLCANCEL(nHandle)
		USE IN VART
	ENDIF

	SELECT outship
	cbolx = PADR(cBOL,20)

	IF SEEK(cbolx,'outship','bol_no')
		lSYMS = IIF("SYMS"$outship.consignee,.T.,.F.)
		lAmazon = IIF("AMAZON"$outship.consignee OR "GOLDEN STATE"$outship.consignee,.T.,.F.)
		lAriatEurope = IIF("ARIAT"$outship.consignee AND "EUROPE"$outship.consignee,.T.,.F.)
		lDoCompare = IIF("SCANPACK"$outship.shipins,.F.,lDoCompare)
	ELSE
		WAIT WINDOW "BOL not found in OUTSHIP" TIMEOUT 2
		cErrMsg = "BOL NOT FOUND"
		DO ediupdate WITH cErrMsg,.T.
		THROW
	ENDIF

	IF USED('SHIPMENT')
		USE IN shipment
	ENDIF

	SET STEP ON 
	csq1 = [select * from shipment where accountid = ]+TRANSFORM(nAcctNum)+[ and pickticket =']+xxlcPT+[']
	xsqlexec(csq1,,,"wh")
	INDEX ON shipmentid TAG shipmentid
	INDEX ON pickticket TAG pickticket

	IF USED('PACKAGE')
		USE IN package
	ENDIF

	csq1 = [select * from package where accountid = ]+TRANSFORM(nAcctNum)+[ and shipmentid =]+TRANSFORM(shipment.shipmentid)
	xsqlexec(csq1,,,"wh")
	INDEX ON shipmentid TAG shipmentid
	INDEX ON ucc TAG ucc
	SET ORDER TO TAG shipmentid

	SELECT shipment
	SET RELATION TO shipmentid INTO package

&& Added code blocks to the following to overcome BOL vs. WO selection issues, 12.23.2005

	WAIT WINDOW "Skipping Labels/Cartons Comparison..." NOWAIT

	IF lDoSQL  && If using the same WO# for multiple parcel shipments, this saves a lot of time.
		SELECT outship
		LOCATE
		IF USED("sqlpt")
			USE IN sqlpt
		ENDIF
		DELETE FILE "F:\3pl\DATA\sqlpt.dbf"
		SELECT bol_no,ship_ref,outshipid ;
			FROM outship ;
			WHERE bol_no = PADR(cBOL,20) ;
			AND accountid = nAcctNum ;
			AND IIF(lParcelType,outship.ship_ref = cPickticket,outship.accountid = nAcctNum) ;
			GROUP BY 1,2 ;
			ORDER BY 1,2 ;
			INTO DBF F:\3pl\DATA\sqlpt
		USE IN sqlpt
		USE F:\3pl\DATA\sqlpt IN 0 ALIAS sqlpt

		cRetMsg = ""

		DO m:\dev\prg\sqlconnectariat_bol  WITH nAcctNum,ccustname,cPPName,.T.,cOffice
		IF cRetMsg<>"OK"
			cErrMsg = cRetMsg
			DO ediupdate WITH cErrMsg,.T.
			THROW
		ENDIF
	ENDIF

	SELECT variatpp
	LOCATE
************************************************************************
* here lets do a pre-check on all the PTs fo a QTY check
************************************************************************
	llAllQtysOK = .T.
	cErrMsg ="Pick Tickets with Sql.totqty vs Outdet.totqty errors on shipment: "+cBOL+CHR(13)+REPLICATE("-",86)+CHR(13)
	SELECT ship_ref,outshipid,shipins,wo_num FROM outship WHERE outship.bol_no= cBOL AND IIF(lParcelType,outship.ship_ref = cPickticket,outship.accountid = nAcctNum) INTO CURSOR outshipids
	SELECT outshipids
	SCAN
		SELECT outdet
		SET ORDER TO
		SUM totqty TO nUnitTot1 FOR  units AND outdet.outshipid = outshipids.outshipid
		SELECT variatpp
		IF lTesting
*			BROWSE
		ENDIF

		SUM totqty TO nUnitTot2 FOR variatpp.outshipid = outshipids.outshipid
		IF nUnitTot1<>nUnitTot2
			SET STEP ON
			llPType="UNK"
			DO CASE
			CASE "PICKPACK"$outshipids.shipins
				llPType = "PICKPACK"
			CASE "SCANPACK"$outshipids.shipins
				llPType = "SCANPACK"
			CASE "PREPACK"$outshipids.shipins
				llPType = "PREPACK "
			ENDCASE
			SELECT variatpp
			LOCATE FOR variatpp.outshipid = outshipids.outshipid
			cErrMsg = cErrMsg+"Pick Ticket: "+ALLTRIM(outshipids.ship_ref)+" -("+llPType+")  Outdet totqty = "+PADR(ALLTRIM(TRANSFORM(nUnitTot1)),6," ")+"  SQL Cartons TotQty:"+PADR(ALLTRIM(TRANSFORM(nUnitTot2)),6," ")+CHR(13)
			llAllQtysOK = .F.
		ENDIF
	ENDSCAN

	IF llAllQtysOK = .F.

		IF lEmail
			SELECT 0
			USE F:\3pl\DATA\mailmaster ALIAS mm

			IF cOffice = "K"
				LOCATE FOR mm.edi_type = "MISC" AND mm.taskname = "SHORTERROR"
			ELSE
				LOCATE FOR mm.edi_type = "MISC" AND mm.taskname = "GRPERROR"
			ENDIF
			tsendto = IIF(mm.use_alt,mm.sendtoalt,mm.sendto)
			tcc = IIF(mm.use_alt,mm.ccalt,mm.cc)
			USE IN mm
			tsubject ="Ariat Outdet/SQL Qty Errors on BOL/Trk#: "+cBOL+" at "+TTOC(DATETIME())
			tattach=""
			tmessage = cErrMsg
			DO FORM m:\dev\frm\dartmail2 WITH tsendto,tfrom,tsubject,tcc,tattach,tmessage,"A"
		ENDIF

		m.bol_no = cBOL
		m.ship_ref = ALLTRIM(outship.ship_ref)
		m.addproc = "ARIAT940"
		csq1 = [select * from info where bol_no = ']+cBOL+[' and ship_ref = ']+m.ship_ref+[' and addproc = ']+m.addproc+[']
		xsqlexec(csq1,"par1",,"wh")
		LOCATE
		IF EOF("par1")
			USE IN par1
			useca("info","wh")
			m.accountid = nAcctNum
			m.office = cOffice
			m.mod = cMod
			m.cnee_ref = ALLTRIM(outship.cnee_ref)
			m.userid ="JOEB"
			m.addby = "JOEB"
			m.adddt = DATETIME()
			m.zdate = DATE()
			m.zdatetime = DATETIME()
			m.message = "SQL Qty shortage - Record(s) corrected"
			insertinto("info","wh",.T.)
			tu("info")

			cErrMsg= "SQL/OTDET QTY ERR"
			DO ediupdate WITH cErrMsg,.T.
			THROW
		ENDIF
		RELEASE csq1
		IF USED("par1")
			USE IN par1
		ENDIF

*   Set Step On
	ENDIF

*************************************************************************

	IF lTesting
*	BROWSE TIMEOUT 30
	ENDIF

	IF USED("scacs")
		USE IN scacs
	ENDIF
	USE ("F:\3pl\DATA\parcel_carriers") IN 0 ALIAS scacs ORDER TAG scac

	USE F:\wh\sqlpassword IN 0
	gsqlpassword = ALLTRIM(sqlpassword.PASSWORD)
	USE IN sqlpassword

	cServer = "SQL5"
	SQLSETPROP(0,'DispLogin',3)
	SQLSETPROP(0,"dispwarnings",.F.)

	lcDSNLess="driver=SQL Server;server=&cServer;uid=SA;pwd=&gsqlpassword;DATABASE=PICKPACK"
	nHandle=SQLSTRINGCONNECT(m.lcDSNLess,.T.)
	IF nHandle<=0 && bailout
		WAIT WINDOW AT 6,2 "No Data Connection.."+CHR(13)+" Call MIS...."
		lcQuery = [Could not make connection to sp3 SQL server]+CHR(13)+"   "+lcDSNLess
		THROW
		RETURN
	ENDIF

	SELECT outship
	LOCATE
	WAIT CLEAR
	WAIT WINDOW "Now creating Header information" NOWAIT NOCLEAR


*********************************************************************************************************************
* here we check to see if the BOL has multiple pick tickets, if so we need to send a 945 file per pick ticket
* even in the case of a trucking BOL with many picktickets
*********************************************************************************************************************
*  Select ship_ref From outship Where bol_no = cBol Into Cursor xPTs
*  Select xPts
*  Scan

*!* HEADER LEVEL EDI DATA
	IF !lTesting
		DO num_incr_isa
	ELSE
		c_CntrlNum = "1"
	ENDIF

	cISA_Num = PADL(c_CntrlNum,9,"0")
	nISA_Num = INT(VAL(cISA_Num))
	cISACode = IIF(lTesting OR lTestInput,"T","P")
	cISACode = "P"

	SELECT outship
	LOCATE FOR outship.bol_no = cBOL AND IIF(lParcelType,outship.ship_ref = cPickticket,outship.accountid = nAcctNum)
	IF FOUND()
		alength = ALINES(apt,outship.shipins,.T.,CHR(13))
		cxShipID = segmentget(@apt,"SHIPID945",alength)
	ENDIF

	crecqual = "01"
	crecid = "789995313"
	crecidlong = PADR(crecid,15," ")

**************************************************************************8

****************** START WRITING OUT THE 945 FILE ****************************

	STORE "ISA"+cfd+"00"+cfd+"          "+cfd+"00"+cfd+"          "+cfd+csendqual+cfd+csendidlong+cfd+;
		crecqual+cfd+crecidlong+cfd+cTruncDate+cfd+cTruncTime+cfd+"U"+cfd+"00401"+cfd+;
		cISA_Num+cfd+"0"+cfd+cISACode+cfd+cterminator+csegd TO cString
	DO cstringbreak

	STORE "GS"+cfd+"SW"+cfd+csendid+cfd+crecid+cfd+cdate+cfd+cTruncTime+cfd+c_CntrlNum+;
		cfd+"X"+cfd+cX12+csegd TO cString
	DO cstringbreak

	SELECT outship
*	SET ORDER TO TAG wo_num
	LOCATE

	IF !lTesting
		IF EMPTY(cBOL)  && May be empty if triggered from PT screen, or FedEx shipment
			WAIT CLEAR
			WAIT WINDOW "MISSING Bill of Lading Number!" TIMEOUT 2
			cErrMsg = "BOL# EMPTY"
			DO ediupdate WITH cErrMsg,.T.
			THROW
		ELSE
			IF !SEEK(PADR(TRIM(cBOL),20),"outship","bol_no")
				WAIT CLEAR
				WAIT WINDOW "Invalid Bill of Lading Number - Not Found!" TIMEOUT 2
				cErrMsg = "BOL# NOT FOUND"
				DO ediupdate WITH cErrMsg,.T.
				THROW
			ENDIF
		ENDIF
	ENDIF

*************************************************************************
*1	PTIDSOURCE/OUTSHIP RECORD MAIN LOOP
*	Loops through individual OUTSHIP lines for BOL#
*************************************************************************
	WAIT CLEAR
	WAIT WINDOW "Now creating BOL#-based information..." NOWAIT NOCLEAR

	SELECT outship
	SET ORDER TO
	IF lTesting OR lTestProd
		COUNT TO N FOR outship.bol_no = PADR(cBOL,20)
	ELSE
		COUNT TO N FOR outship.bol_no = PADR(cBOL,20) AND !EMPTYnul(del_date)
	ENDIF
	IF N=0
		cErrMsg = "INCOMP BOL"
		DO ediupdate WITH cErrMsg,.T.
		THROW
	ENDIF

	LOCATE
	cMissDel = ""

	IF lTesting OR lTestProd
		IF cBOL = "EMPTY"
			oscanstr = "outship.bol_no = PADR(cBOL,20)"
		ELSE
			oscanstr = "outship.bol_no = PADR(cBOL,20) and ctnqty>0 and IIF(lParcelType,outship.ship_ref = cPickticket,outship.accountid = nAcctNum)"
		ENDIF
	ELSE
		oscanstr = "outship.bol_no = PADR(cBOL,20) AND !EMPTYnul(del_date) and ctnqty>0 and IIF(lParcelType,outship.ship_ref = cPickticket,outship.accountid = nAcctNum)"
	ENDIF


	SELECT 0
	SELECT * FROM "f:\3pl\data\ackdatashell" WHERE .F. INTO CURSOR tempack READWRITE

	SELECT outship
	nWO_Xcheck = 0
	DIMENSION aryamz(1)

	SCAN FOR &oscanstr &&outship.ship_ref = xPTs.ship_ref
		cShip_ref = ALLTRIM(outship.ship_ref)
		lScanpack = IIF("SCANPACK"$outship.shipins,.T.,.F.)
*!*			alength = ALINES(apt,outship.shipins,.T.,CHR(13))
*!*			cShipmentID = segmentget(@apt,"LONGTRKNUM",alength)
*!*			IF EMPTY(cShipmentID)
*!*				cShipmentID = TRIM(cBOL)
*!*			ENDIF


		SCATTER FIELDS wo_num,scac,ship_via MEMVAR
		IF EMPTY(outship.scac)
			m.scac = segmentget(@apt,"SHIPSCAC",alength)
			REPLACE outship.scac WITH m.scac IN outship NEXT 1
			IF lTestInput AND DATE() = {^2014-12-01}
				m.scac = "UPSS"
			ENDIF
		ENDIF

		lConsolFreight = .F.
		lWorldEaseFreight = .F.

		SELECT scacs
		IF INLIST(PADR(ALLTRIM(outship.carrcode),10),"UPSGCC","UPSFCO","UPSFFF","UPSFTP","UPSF") OR cBOL = '1Z89RA010303768895'
			IF outship.carrcode = "UPSGCC"
				lWorldEaseFreight = .T.
				IF usesqlctn()
					xsqlexec("select * from ctnucc where mod='K' and ship_ref='"+outship.ship_ref+"'",,,"wh")
				ELSE
					SELECT ctnucc
					LOCATE FOR ctnucc.ship_ref = outship.ship_ref
				ENDIF

				IF FOUND()
					lcWorldEaseFreightCharge = ALLTRIM(ctnucc.REFERENCE)  && this is the charge from Worldease
				ELSE
*					SET STEP ON
					lcWorldEaseFreightCharge = "UNK"
					tsubject = "ARIAT Worldease Export Error: "+TRIM(cBOL)
					tsendto = "cheri.foster@tollgroup.com,tony.sutherland@tollgroup.com,michael.sanders@tollgroup.com"
					tcc = "pgaidis@fmiint.com,joe.bianchi@tollgroup.com,tmarg@fmiint.com"
					tmessage = "Worldease Export data issues for following delivery:"+CHR(13)+ALLTRIM(outship.ship_ref)+CHR(13)+" Can't find this delivery in the Worldease data...."
					tattach =""
					DO FORM m:\dev\frm\dartmail2 WITH tsendto,tfrom,tsubject,tcc,tattach,tmessage,"A"
					cErrMsg = "WORLEASE DATA PT LOOKUP ERROR"
					DO ediupdate WITH cErrMsg,.T.
					THROW
				ENDIF
* Select =Seek(tuccs.ucc,"ctnucc","ship_ref")
*!*	        If Found("ctnucc")
*!*	          lcWorldEaseFreightCharge = Alltrim(ctnucc.Reference)
*!*	        Else
*          lcWorldEaseFreightCharge = "UNK"
*!*	        Endif
			ENDIF
			WAIT WINDOW "This is a UPS CONSOLIDATION shipment, running as type = 'Freight'" NOWAIT
			lConsolFreight = .T.
			lParcelType = .F.
		ELSE
			IF !EMPTY(TRIM(outship.scac))
				STORE TRIM(outship.scac) TO m.scac
				m.scac = IIF(m.scac = "FEDZ","FDEN",m.scac)
				m.scac = IIF(m.scac = "FDXG","FDEG",m.scac)
				m.scac = IIF(m.scac = "UPWZ","UPSN",m.scac)
				SELECT shipcode
				LOCATE FOR scaccode = m.scac

				IF FOUND()
**added UPSGCC filter to exclude that carrcode from UPS processing, will be processed manually - no data in shipment/package - mvw 05/27/14
					IF INLIST(m.scac,"UPSN","USPS")
						IF INLIST(cShip_ref,"0083853250")  && Put in PT # for those parcel shipments processed w/o labels, etc.
							lParcelType = .F.
							lFedEx = .F.
						ELSE
							IF outship.carrcode = "XXXXXUPSGCC"
*								SET STEP ON
								cErrMsg = "NO 945 NEEDED - UPSGCC"
								DO ediupdate WITH cErrMsg,.F.
								THROW

							ELSE
								lParcelType = .T.
								lFedEx = .F.
							ENDIF
						ENDIF
					ENDIF
					IF INLIST(m.scac,"FDEN","FDEG","FXSP")
						IF outship.carrcode ="FDXIFE"
							lFedEx   = .F.
							lParcelType = .F.
						ELSE
							lFedEx   = .T.
							lParcelType = .T.
						ENDIF
					ENDIF
				ENDIF
				lParcelType = IIF(ALLTRIM(outship.keyrec) == ALLTRIM(outship.bol_no),.F.,lParcelType)

				SELECT outship

				IF lParcelType
					cCarrierType = "U" && Parcel as UPS or FedEx
				ENDIF
			ELSE
				WAIT CLEAR
				cErrMsg = "MISSING SCAC"
				DO ediupdate WITH cErrMsg,.T.
				THROW
			ENDIF
		ENDIF

		lUsePackageData = IIF(EMPTY(outship.amazon) AND lParcelType,.T.,.F.)
		cShip_ref = ALLTRIM(outship.ship_ref)
		cCarrCode = ALLTRIM(outship.carrcode)
		cCarrCode = IIF(cCarrCode = "FDXGCO","FDXGC",cCarrCode)
		IF lParcelType
			SELECT shipcode
			LOCATE FOR shipmode = cCarrCode
			IF !FOUND()
				cErrMsg = "BAD CARRCODE: "+cCarrCode
				DO ediupdate WITH cErrMsg,.T.
				THROW
			ELSE
				lAddCharges = shipcode.frton945
			ENDIF
		ENDIF

		SELECT outship
		IF lParcelType
			cPROUPS = cBOL
		ENDIF
*		ASSERT .F. MESSAGE "Debug here!"
		SELECT outship
		SCATTER MEMVAR MEMO
		IF " OV"$m.ship_ref OR "!!"$m.ship_ref
			LOOP
		ENDIF

		nWO_Num = outship.wo_num
		cWO_Num = ALLTRIM(STR(nWO_Num))
		lBloom = IIF(outship.consignee = "BLOOMINGDALE",.T.,.F.)

		IF !(cWO_Num$cWO_NumStr)
			cWO_NumStr = IIF(EMPTY(cWO_NumStr),cWO_Num,cWO_NumStr+","+cWO_Num)
		ENDIF
		IF !(cWO_Num$cWO_NumList)
			cWO_NumList = IIF(EMPTY(cWO_NumList),cWO_Num,cWO_NumList+CHR(13)+cWO_Num)
		ENDIF

		IF lTestInput
			lPick = .T.
		ELSE
			xsqlexec("select * from outwolog where wo_num  = "+TRANSFORM(m.wo_num),,,"wh")
			lPick = IIF(outwolog.picknpack,.T.,.F.)
			lPrepack = IIF(lPick,.F.,.T.)
		ENDIF

		nTotCtnCount = m.qty

		cPO_Num = LEFT(ALLTRIM(m.cnee_ref),22)
		IF EMPTY(cPO_Num)
			cPO_Num = "NA"
		ENDIF

*!* Added this code to trap miscounts in OUTDET Units

		IF lDoTotqtyCheck && lPrepack AND
			SELECT outdet
			SET ORDER TO
			SUM totqty TO nUnitTot1 FOR  units AND outdet.outshipid = outship.outshipid
			IF lPrepack
				SUM (VAL(PACK)*totqty) TO nUnitTot2 FOR !units AND outdet.outshipid = outship.outshipid
				IF nUnitTot1<>nUnitTot2
					SET STEP ON
					cErrMsg = "OUTDET INTERNAL TOTQTY ERR"
					=FCLOSE(nFilenum)
					DO ediupdate WITH cErrMsg,.T.
					THROW
				ENDIF
			ENDIF
			SELECT variatpp
			SUM totqty TO nUnitTot2 FOR variatpp.outshipid = outship.outshipid
			IF nUnitTot1<>nUnitTot2
				SET STEP ON
				SELECT variatpp
				LOCATE FOR variatpp.outshipid = outship.outshipid
				cErrMsg = "SQL/OTDET QTY ERR: PT "+ALLTRIM(variatpp.ship_ref)
				DO ediupdate WITH cErrMsg,.T.
				=FCLOSE(nFilenum)
				THROW
			ENDIF
		ENDIF
		SELECT outdet
		SET ORDER TO && outdetid
		LOCATE

*!* End code addition

		IF (("PENNEY"$UPPER(outship.consignee)) OR ("KMART"$UPPER(outship.consignee)) OR ("K-MART"$UPPER(outship.consignee))) AND !lParcelType
			lApptFlag = .T.
		ELSE
			lApptFlag = .F.
		ENDIF

		IF lTestInput
			ddel_date = DATE()
			dapptnum = "99999"
			dapptdate = DATE()
		ELSE
			ddel_date = outship.del_date
			IF EMPTY(ddel_date)
				IF lTesting OR lTestProd
					ddel_date = DATE()
				ELSE
					cMissDel = IIF(EMPTY(cMissDel),"The following PTs had no Delivery Dates:"+CHR(13)+TRIM(cShip_ref),cMissDel+CHR(13)+TRIM(cShip_ref))
				ENDIF
			ENDIF
			dapptnum = ALLTRIM(outship.appt_num)
			IF EMPTY(dapptnum) AND lTestProd
				dapptnum = STR(INT(RAND(SECONDS())*(10^7)),7)
			ENDIF

			IF EMPTY(dapptnum) AND (("PENNEY"$outship.consignee) OR ("KMART"$outship.consignee) OR ("K-MART"$outship.consignee))  && Penney/KMart Appt Number check
				IF !lApptFlag
					dapptnum = ""
				ELSE
					cErrMsg = "EMPTY APPT #"
					DO ediupdate WITH cErrMsg,.T.
					THROW
				ENDIF
			ENDIF

			dapptdate = outship.appt
			IF EMPTY(dapptdate)
				dapptdate = IIF(lTestProd,DATE(),outship.del_date)
			ENDIF
		ENDIF

		alength = ALINES(apt,outship.shipins,.T.,CHR(13))
		IF !lAriatEurope
			IF lAmazon
				cPRONum = ALLTRIM(outship.keyrec)
			ELSE
				cPRONum = ALLT(outship.appt_num)
				IF EMPTY(cPRONum) AND !lParcelType
					cPRONum = ALLT(outship.keyrec)
					IF EMPTY(cPRONum) AND !lParcelType
						cPRONum = "NONE"
					ENDIF
				ENDIF
			ENDIF
		ELSE
			cPRONum = "OCEAN"
		ENDIF

		nOutshipid = m.outshipid
		cPTString = IIF(EMPTY(cPTString),m.consignee+" "+cShip_ref,cPTString+CHR(13)+m.consignee+" "+cShip_ref)

		m.CSZ = TRIM(m.CSZ)
		IF EMPTY(ALLT(STRTRAN(M.CSZ,",","")))
			WAIT CLEAR
			WAIT WINDOW "No SHIP-TO City/State/ZIP info...exiting" TIMEOUT 2
			cErrMsg = "NO CSZ INFO"
			DO ediupdate WITH cErrMsg,.T.
			THROW
		ENDIF
		IF !(", "$m.CSZ)
			m.CSZ = ALLTRIM(STRTRAN(m.CSZ,",",", "))
		ENDIF
		m.CSZ = ALLTRIM(STRTRAN(m.CSZ,"  "," "))
		cCity = ALLT(LEFT(TRIM(m.CSZ),AT(",",m.CSZ)-1))
		cStateZip = ALLT(SUBSTR(TRIM(m.CSZ),AT(",",m.CSZ)+1))

		cState = ALLT(LEFT(cStateZip,2))
		cOrigState = segmentget(@apt,"ORIGSTATE",alength)
		cState = IIF(EMPTY(cOrigState),cState,cOrigState)

		cZip = ALLT(SUBSTR(cStateZip,3))

		lNoSForAddress = .F.
		IF (EMPTY(STRTRAN(ALLTRIM(outship.SForCSZ),",","")) OR EMPTY(ALLTRIM(outship.sforaddr1)))
			BLANK FIELDS outship.SForCSZ NEXT 1 IN outship
			m.SForCSZ = ""
			lNoSForAddress = .T.
		ENDIF

		IF !lTesting
			DO num_incr_st
		ELSE
			c_GrpCntrlNum  = "1"
		ENDIF
		WAIT CLEAR
		WAIT WINDOW "Now creating Line Item information" NOWAIT NOCLEAR

		INSERT INTO temp945 (accountid,isa_num,st_num,wo_num,bol_no,ship_ref,filename,filedate) ;
			VALUES (m.accountid,c_CntrlNum,c_GrpCntrlNum,m.wo_num,m.bol_no,m.ship_ref,cFilenameShort,dt2)
******** added 1/9/17 TMARG to capture 997 data

		m.groupnum=c_CntrlNum
		m.isanum=cISA_Num
		m.transnum=PADL(c_GrpCntrlNum,9,"0")
		m.edicode="SW"
		m.accountid=m.accountid
		m.loaddt=DATE()
		m.loadtime=DATETIME()
		m.filename=cFilenameShort
		m.ship_ref=m.ship_ref
		insertinto("ackdata","wh",.T.)
		tu("ackdata")

		STORE "ST"+cfd+"945"+cfd+PADL(c_GrpCntrlNum,9,"0")+csegd TO cString
		DO cstringbreak
		nSTCount = nSTCount + 1
		nSegCtr = 1
		cIntAcct = ""
		INSERT INTO tempack FROM MEMVAR


		STORE "W06"+cfd+"N"+cfd+cShip_ref+cfd+cdate+cfd+TRIM(cShip_ref)+cfd+TRIM(cWO_Num)+cfd+cPO_Num+csegd TO cString
		DO cstringbreak
		nSegCtr = nSegCtr + 1

		nCtnNumber = 1  && Seed carton sequence count

		cStoreNum = ALLTRIM(m.dcnum)

		IF EMPTY(cStoreNum)
			STORE "N1"+cfd+"ST"+cfd+ALLTRIM(m.consignee)+csegd TO cString
		ELSE
			STORE "N1"+cfd+"ST"+cfd+ALLTRIM(m.consignee)+cfd+"92"+cfd+cStoreNum+csegd TO cString
		ENDIF
		DO cstringbreak
		nSegCtr = nSegCtr + 1

		IF !EMPTY(m.sforstore)
			STORE "N1"+cfd+"BY"+cfd+ALLTRIM(m.shipfor)+cfd+"91"+cfd+TRIM(m.sforstore)+csegd TO cString
			DO cstringbreak
			nSegCtr = nSegCtr + 1
		ENDIF

		STORE "N1"+cfd+"SF"+cfd+"TGF"+cfd+"91"+cfd+"01"+csegd TO cString
		DO cstringbreak
		nSegCtr = nSegCtr + 1

		STORE "N3"+cfd+TRIM(cSF_Addr1)+csegd TO cString
		DO cstringbreak
		nSegCtr = nSegCtr + 1

		STORE "N4"+cfd+cSF_CSZ+cfd+"USA"+csegd TO cString
		DO cstringbreak
		nSegCtr = nSegCtr + 1

		IF lParcelType
			SET STEP ON
			IF cBOL # "EMPTY"
				lUsePackageData = .F.
				IF !lAmazon
					IF lTesting AND lTestInput
						nCharge = 25.70
					ELSE
						cUPSTrack = ""
						nStrSeek = PADR(ALLT(cShip_ref),20)
						SELECT shipment
						LOCATE FOR shipment.pickticket = nStrSeek AND INLIST(shipment.accountid,nAcctNum,nAcctNum1,nAcctNum2,9999)
						IF !FOUND()
*						SET STEP ON
							cErrMsg = "NO FIND IN SHIPMENT, PT "+ALLTRIM(cShip_ref)
							DO ediupdate WITH cErrMsg,.T.
							THROW
						ELSE
							SUM shipment.charge TO nCharge ;
								FOR shipment.pickticket = nStrSeek ;
								AND INLIST(shipment.accountid,nAcctNum,nAcctNum1,nAcctNum2,9999)
						ENDIF

						lUsePackageData = .T.
						SELECT package
						SELECT trknumber,ucc AS ucc,pkgweight,LENGTH,WIDTH,depth ;
							FROM package ;
							WHERE .F. ;
							INTO CURSOR PACKAGEDATA READW
						SELECT shipment
						LOCATE
						ns1 = 1
						ns = TRANSFORM(ns1)
						SCAN FOR shipment.pickticket = nStrSeek AND INLIST(shipment.accountid,nAcctNum,nAcctNum1,nAcctNum2,9999)
							SELECT trknumber,ucc AS ucc,pkgweight,LENGTH,WIDTH,depth ;
								FROM package ;
								WHERE package.shipmentid = shipment.shipmentid ;
								INTO CURSOR PACKAGEDATA&ns
							SELECT PACKAGEDATA
							APPEND FROM DBF('PACKAGEDATA'+ns)
							USE IN PACKAGEDATA&ns
						ENDSCAN
					ENDIF
				ELSE
					lenary1 = MEMLINES(outship.amazon)
					IF lenary1 < 1
						IF EMPTY(cBOL)
							cErrMsg = "EMPTY 'AMAZON' FIELD"
							DO ediupdate WITH cErrMsg,.T.
							THROW
						ELSE
							IF cBOL = "AMZ"
								nStrSeek = PADR(ALLT(cShip_ref),20)
								SELECT shipment
								LOCATE FOR shipment.pickticket = nStrSeek AND INLIST(shipment.accountid,nAcctNum,nAcctNum1,nAcctNum2,9999)
								IF !FOUND()
									SET STEP ON
									cErrMsg = "MISS PT IN SHIPMENT "+cShip_ref
									DO ediupdate WITH cErrMsg,.T.
									THROW
								ELSE
									SUM shipment.charge FOR shipment.pickticket = nStrSeek AND INLIST(shipment.accountid,nAcctNum,nAcctNum1,nAcctNum2,9999) TO nCharge
								ENDIF

								lUsePackageData = .T.
								lUseBOL = .F.
								SELECT package
								LOCATE FOR package.shipmentid = shipment.shipmentid
								IF !FOUND()
									SET STEP ON
									cErrMsg = "NO MATCH IN PACKAGE "+cShip_ref
									DO ediupdate WITH cErrMsg,.T.
									THROW
								ENDIF

								SELECT trknumber,ucc ;
									FROM package ;
									WHERE .F. ;
									INTO CURSOR PACKAGEDATA READW
								SELECT shipment
								LOCATE
								ns1 = 1
								ns = TRANSFORM(ns1)
								nCharge= 0.0
								SCAN FOR shipment.pickticket = nStrSeek AND INLIST(shipment.accountid,nAcctNum,nAcctNum1,nAcctNum2,9999)
									SELECT trknumber,ucc ;
										FROM package ;
										WHERE package.shipmentid = shipment.shipmentid ;
										INTO CURSOR PACKAGEDATA&ns
									SELECT PACKAGEDATA
									APPEND FROM DBF('PACKAGEDATA'+ns)
									USE IN PACKAGEDATA&ns
								ENDSCAN
							ELSE
								lUsePackageData = .F.
								lUseBOL = .T.
								cTrkNumber =  cBOL
							ENDIF
						ENDIF
					ELSE
						ALINES(aryamz,outship.amazon)
						lUseBOL = .T.
						cTrkNumber =  cBOL
					ENDIF
				ENDIF
			ELSE
				CREATE CURSOR PACKAGEDATA (trknumber c(20),ucc c(20))
				nCharge = 20.5
				FOR wwq = 1 TO outship.ctnqty
					cUCC = "00008102000005"+PADL(RIGHT(ALLT(STR(RAND(SECONDS())*250000)),6),6,"0")
					IF lFedEx
						cTrkNumber = "977712345"+PADL(RIGHT(ALLT(STR(RAND(SECONDS())*250000)),6),6,"0")
					ELSE
						cTrkNumber = "1ZX123A12356"+PADL(RIGHT(ALLT(STR(RAND(SECONDS())*250000)),6),6,"0")
					ENDIF
					INSERT INTO PACKAGEDATA (trknumber,ucc) VALUES (cTrkNumber,cUCC)
				ENDFOR
			ENDIF
		ENDIF
*!*			IF cBOL = "EMPTY"
*!*				lUseBOL = .T.
*!*				cTrkNumber =  cBOL
*!*			ENDIF

		cCharge = ALLTRIM(STR(nCharge*100,10))

		IF lUseBOL
			cTrkNumber =  cBOL
		ENDIF

		IF lAmazon AND lParcelType AND EMPTY(cTrkNumber)
			cErrMsg = "EMPTY AMZ Track# "+cShip_ref
			DO ediupdate WITH cErrMsg,.T.
			THROW
		ENDIF

		IF lTesting
*			SET STEP ON
		ENDIF

		cBOLFill = IIF((lParcelType AND !lAmazon),cBOL,cPRONum)  && on a ParcelType the cBol would be the tracking number; changed to PRO# for all Amazon shipments
		cBOLFill = IIF(lConsolFreight,cBOL,cBOLFill)  && if lConsolFreight is .T. then its a consolidate to Canada,
&& N9*BM would be the LTL BOL and the individual ctns would be UPS tracking number from Worldease import
		ASSERT .F.
		IF EMPTY(cBOLFill)
		ELSE
			STORE "N9"+cfd+"BM"+cfd+cBOLFill+csegd TO cString  && on a regular or domestic small pkg shipment both N9*BM and N9*CN are the small pkg tracking number
			DO cstringbreak
			nSegCtr = nSegCtr + 1
		ENDIF

		STORE "N9"+cfd+"CN"+cfd+cBOL+csegd TO cString  && Added 05.09.2014, per Daniel at Ariat.
		DO cstringbreak                                && On small pkg this is a  tracking number
		nSegCtr = nSegCtr + 1

		cSalesOrder = segmentget(@apt,"SALESORDER",alength)
		STORE "N9"+cfd+"CO"+cfd+cSalesOrder+csegd TO cString
		DO cstringbreak
		nSegCtr = nSegCtr + 1

		cBillto = segmentget(@apt,"BILLTONAME",alength)  && Added to pull Dillard or Amazon from bill-to info
		cBilltocode = segmentget(@apt,"BILLTOCODE",alength)  && Added to
		lDoBTCharges = IIF(INLIST(cBilltocode,"20004210","20005422"),.F.,.T.)

		llDillards = .F.
		llARN= .F.

		IF "DILLARD"$outship.consignee OR "DILLARD"$UPPER(cBillto)
			llDillards = .T.
			STORE "N9"+cfd+"LO"+cfd+IIF(lAmazon,cPRONum,ALLT(outship.appt_num))+csegd TO cString  && Changed to use PRO# if Amazon type shipment.
			DO cstringbreak
			nSegCtr = nSegCtr + 1
		ENDIF

		IF ("AMAZON"$outship.consignee OR "AMAZON"$UPPER(cBillto)) AND !("MARKETPLACE"$UPPER(cBillto))
			llARN= .T.
			cAmazonRoutCode = ALLT(outship.appt_num)
			cAmazonRoutCode = IIF(EMPTY(cAmazonRoutCode),ALLTRIM(outship.keyrec),cAmazonRoutCode)
			llAllNumeric=.T.
			FOR lndigctr = 1 TO LEN(cAmazonRoutCode)
				IF !ISDIGIT(SUBSTR(cAmazonRoutCode,lndigctr,1))
					llAllNumeric = .F.
				ENDIF
			NEXT
			IF (EMPTY(cAmazonRoutCode) OR LEN(cAmazonRoutCode)!= 10 OR !llAllNumeric)
				SET STEP ON
				cErrMsg = "EMPTY/BAD AMZ ARN Number "+cShip_ref
				DO ediupdate WITH cErrMsg,.T.
				THROW
			ENDIF
			STORE "N9"+cfd+"LO"+cfd+ALLT(cAmazonRoutCode)+csegd TO cString
			DO cstringbreak
			nSegCtr = nSegCtr + 1
		ENDIF

		IF [DICK'S SPORTING GOODS]$outship.consignee
			llCSCode= .T.
			lcCSCode = ALLT(outship.appt_num)
			IF SUBSTR(lcCSCode,1,2)!="CS"
				llCSCode= .F.
			ENDIF
			IF (EMPTY(lcCSCode) OR LEN(lcCSCode) #10 OR !llCSCode) AND lcCSCode # "NOLOADNUMBER"
				cErrMsg = "EMPTY/BAD DICKS Routing Code "+cShip_ref
				DO ediupdate WITH cErrMsg,.T.
				THROW
			ENDIF
			STORE "N9"+cfd+"LO"+cfd+ALLT(lcCSCode)+csegd TO cString
			DO cstringbreak
			nSegCtr = nSegCtr + 1
		ENDIF

		IF llDillards = .F. AND llARN= .F. AND !EMPTY(outship.appt_num)
			STORE "N9"+cfd+"LO"+cfd+ALLT(outship.appt_num)+csegd TO cString
			DO cstringbreak
			nSegCtr = nSegCtr + 1
		ENDIF

		STORE "G62"+cfd+"11"+cfd+TRIM(DTOS(ddel_date))+cfd+"D"+cfd+cDelTime+csegd TO cString  && Ship date/time
		DO cstringbreak
		nSegCtr = nSegCtr + 1

		llSmallPkg = .F.
*		SET STEP ON
		SELECT shipcode
		LOCATE FOR scaccode = outship.scac
		IF FOUND()
			llSmallPkg = .T.
		ELSE
			cbolx = PADR(cBOL,20)
			IF SEEK(cbolx,'bl','bol_no')
				DO CASE
				CASE bl.terms = "1"
					lcterms = "PP"
				CASE bl.terms = "2"
					lcterms = "CC"
				CASE bl.terms = "3"
					lcterms = "TP"
				OTHERWISE
					lcterms = "PP"
				ENDCASE
			ELSE
				lcterms = "PP"
			ENDIF
		ENDIF
*SET STEP ON
		IF EMPTY(m.carrcode) AND !llSmallPkg
*		SET STEP ON
			SELECT ariat_carriers
			LOCATE FOR ALLTRIM(ariat_carriers.scac) = ALLTRIM(outship.scac) AND ALLTRIM(ariat_carriers.terms)= ALLTRIM(lcterms)
			IF FOUND()
				m.carrcode = ariat_carriers.vendor
			ELSE
				LOCATE FOR ALLTRIM(ariat_carriers.scac) = ALLTRIM(outship.scac)
				IF FOUND()
					m.carrcode = ariat_carriers.vendor
				ELSE
					cErrMsg = "Ariat SCAC is not valid....->> "+outship.scac+"....at BOL...."+cBOL+"  at PT "+cShip_ref
					DO ediupdate WITH cErrMsg,.T.
					THROW
				ENDIF
			ENDIF
		ENDIF

		STORE "W27"+cfd+cCarrierType+cfd+TRIM(m.scac)+cfd+TRIM(m.carrcode)+csegd TO cString  && Changed per Paul, 05.05.2014
		DO cstringbreak
		nSegCtr = nSegCtr + 1


*    Set Step On

		IF lWorldEaseFreight = .T. AND !EMPTY(lcWorldEaseFreightCharge)
			STORE "G72"+cfd+"504"+cfd+"06"+REPLICATE(cfd,6)+ALLT(lcWorldEaseFreightCharge)+csegd TO cString
			DO cstringbreak
			nSegCtr = nSegCtr + 1
		ENDIF

		IF lParcelType AND lWorldEaseFreight = .F. AND lDoBTCharges
			IF lAddCharges
				IF INLIST(ALLTRIM(m.carrcode),"UPSG","UPSGR")  && special 15% discount from the Malvern published rates, PG 03/06/2015
					cCharge = STR(ROUND(VAL(ALLTRIM(cCharge))*0.85,0),6,0)
				ENDIF
				STORE "G72"+cfd+"504"+cfd+"06"+REPLICATE(cfd,6)+ALLT(cCharge)+csegd TO cString
				DO cstringbreak
				nSegCtr = nSegCtr + 1
			ENDIF
		ENDIF

*************************************************************************
*	DETAIL LOOP - MATCHING SHIP_REF FIELD (FROM OUTSHIP)
*************************************************************************
		WAIT CLEAR
		WAIT WINDOW "Now creating Detail information "+cShip_ref NOWAIT NOCLEAR


		CREATE CURSOR upcdata (;
			ship_ref CHAR(20),;
			outshipid INT,;
			outdetid INT,;
			upc CHAR(12),;
			STYLE CHAR(20),;
			COLOR CHAR(10),;
			mainline CHAR(15),;
			origqty  N(6),;
			linenum  CHAR(15),;
			lineqty N(6),;
			remainqty N(6))

		lcPT = cShip_ref

**  query by pick ticket

		IF lTesting
			SELECT ucc,upc,STYLE,COLOR,PACK,SUM(totqty) AS pqty FROM variatpp WHERE ship_ref=lcPT AND ucc # 'CUTS' AND accountid = 6532  GROUP BY ucc,upc,STYLE,COLOR,PACK INTO CURSOR tempdata
		ELSE
			lcQuery  = [select ucc,upc,style,color,pack,sum(totqty) as pqty from dbo.cartons where ship_ref=']+lcPT+[' and ucc != 'CUTS' and accountid = 6532  group by ucc,upc,style,color,pack]
			IF SQLEXEC(nHandle,lcQuery,"tempdata")#1
				WAIT WINDOW AT 6,2 "Error Cartons SQL query.........." TIMEOUT 1
				SQLDISCONNECT(nHandle)
			ENDIF
		ENDIF

		SELECT tempdata
		IF lTesting
*		BROWSE
		ENDIF
		SELECT DISTINCT ucc FROM tempdata INTO CURSOR tuccs

		SELECT outship
		IF SEEK("6532"+PADR(ALLTRIM(lcPT),20," "),"outship","acctpt")

			thisoutshipid = outship.outshipid

			SELECT STYLE,COLOR,upc,printstuff,outshipid,outdetid,units,linenum,SUM(origqty) AS mlqty FROM outdet WHERE  units AND outdet.outshipid = thisoutshipid GROUP BY STYLE,COLOR,upc  INTO CURSOR toutdet
*Set Step On

			SELECT toutdet
			SCAN FOR toutdet.outshipid = thisoutshipid AND units
&& OK insert the first main line data record
				INSERT INTO upcdata (ship_ref,outshipid,outdetid,upc,STYLE,COLOR,mainline,origqty) VALUES (lcPT,thisoutshipid,toutdet.outdetid,toutdet.upc,toutdet.STYLE,toutdet.COLOR,toutdet.linenum,toutdet.mlqty)
				thismainline = toutdet.linenum

				ALINES(a, printstuff)
				llSubline = .F.
				lnSubQty=0

				FOR i=1 TO ALEN(a)
					IF a[i] = "SUBLINE"
						llSubline = .T.
						lnLineStarts =ATC("*",a[i])
						thisline=  SUBSTR(a[i],lnLineStarts+1)
						i=i+1
						lnQtyStarts =ATC("*",a[i])
						thisqty=  SUBSTR(a[i],lnQtyStarts+1)
						lnSubQty = lnSubQty +VAL(thisqty)
						INSERT INTO upcdata (ship_ref,outshipid,outdetid,upc,STYLE,COLOR,linenum,lineqty) VALUES (lcPT,thisoutshipid,toutdet.outdetid,toutdet.upc,toutdet.STYLE,toutdet.COLOR,thisline,VAL(thisqty))
					ENDIF
				NEXT i

				IF llSubline
					mainlineqty = toutdet.mlqty - lnSubQty
					INSERT INTO upcdata (ship_ref,outshipid,outdetid,upc,STYLE,COLOR,linenum,lineqty) VALUES (lcPT,thisoutshipid,toutdet.outdetid,toutdet.upc,outdet.STYLE,toutdet.COLOR,toutdet.linenum,mainlineqty)
				ELSE
					INSERT INTO upcdata (ship_ref,outshipid,outdetid,upc,STYLE,COLOR,linenum,lineqty) VALUES (lcPT,thisoutshipid,toutdet.outdetid,toutdet.upc,outdet.STYLE,toutdet.COLOR,toutdet.linenum,toutdet.mlqty)
				ENDIF
			ENDSCAN
		ELSE

			RETURN

		ENDIF

*** now the UPCdata cursor has the linenum and qty data, we scan through UCCs a =and simply decrement according to the SQL Carton/detail/totqty value
		SELECT upcdata
		REPLACE ALL remainqty WITH lineqty FOR lineqty > 0
		IF lTesting
*		BROWSE
		ENDIF


		IF lTesting
*			SET STEP ON
*			lcQuery  = [select ucc,upc,style,color,pack,sum(totqty) as pqty from dbo.labels where ship_ref=']+lcPT+[' and ucc != 'CUTS' and accountid = 6532  group by ucc,upc,style,color,pack]
			SELECT ucc,upc,STYLE,COLOR,PACK,SUM(totqty) AS pqty FROM variatpp WHERE ship_ref=lcPT AND ucc # 'CUTS' AND accountid = 6532  GROUP BY ucc,upc,STYLE,COLOR,PACK INTO CURSOR tempdata
		ELSE
			lcQuery  = [select ucc,upc,style,color,pack,sum(totqty) as pqty from dbo.cartons where ship_ref=']+lcPT+[' and ucc != 'CUTS' and accountid = 6532  group by ucc,upc,style,color,pack]
			IF SQLEXEC(nHandle,lcQuery,"tempdata")#1
				WAIT WINDOW AT 6,2 "Error Cartons SQL query.........." TIMEOUT 1
				SQLDISCONNECT(nHandle)
			ENDIF
		ENDIF

		SELECT tempdata
		SELECT DISTINCT ucc FROM tempdata WHERE ucc != 'CUTS' INTO CURSOR tuccs  && no CUTS ever, these are scanpack shorts
		IF lTesting
			LOCATE
*			BROWSE
		ENDIF

		lxctr =0

		SELECT tuccs
		SCAN

			SELECT DISTINCT upc FROM tempdata WHERE ucc = tuccs.ucc INTO CURSOR tupcs

			SELECT tupcs
			SCAN

				lxctr = lxctr +1

				SELECT tempdata
				SCAN FOR tempdata.ucc = tuccs.ucc AND tempdata.upc = tupcs.upc

					thisUPCQty = tempdata.pqty

					DO WHILE thisUPCQty !=0 && here we will completely ignoe any UPC value where totqty =0, no worry about "NOQTY" type cartons

						SELECT upcdata
						LOCATE FOR ALLTRIM(upcdata.upc) = ALLTRIM(tupcs.upc) AND remainqty >0
						IF FOUND()
							DO CASE
							CASE upcdata.remainqty >= thisUPCQty
								ThisPackQty = thisUPCQty
								REPLACE upcdata.remainqty WITH upcdata.remainqty - thisUPCQty
								thisUPCQty = 0
								thisLinenum = upcdata.linenum
							CASE upcdata.remainqty < thisUPCQty  && looks like this line is now depleted
								thisUPCQty = thisUPCQty - upcdata.remainqty
								ThisPackQty = upcdata.remainqty
								REPLACE upcdata.remainqty WITH 0
								thisLinenum = upcdata.linenum
							ENDCASE
						ELSE
							SET STEP ON
							cErrMsg = "ODET/CTNS QY MTCH AT "+cShip_ref+" and UPC "+ALLTRIM(tupcs.upc)
							DO ediupdate WITH cErrMsg,.T.
							THROW
						ENDIF

						STORE "LX"+cfd+ALLTRIM(STR(lxctr))+csegd TO cString   && Carton Seq. # (up to Carton total)
						DO cstringbreak
						nSegCtr = nSegCtr + 1

						STORE "MAN"+cfd+"GM"+cfd+tuccs.ucc+csegd TO cString
						DO cstringbreak
						nSegCtr = nSegCtr + 1

						STORE "W12"+cfd+"CL"+cfd+cfd+ALLTRIM(STR(ThisPackQty))+cfd+cfd+"EA"+cfd+ALLTRIM(tempdata.upc)+REPLICATE(cfd,4)+"5.0"+cfd+"G"+cfd+"L"+csegd TO cString
						DO cstringbreak
						nSegCtr = nSegCtr + 1
						nUnitSum = nUnitSum  +ThisPackQty  && just added this

						STORE "N9"+cfd+"LI"+cfd+ALLTRIM(thisLinenum)+csegd TO cString
						DO cstringbreak
						nSegCtr = nSegCtr + 1

						DO CASE
						CASE lParcelType AND lWorldEaseFreight = .F.
							IF SEEK(tuccs.ucc,"package","ucc")
								lcTrknumber = ALLTRIM(package.trknumber)
								lcTrknumber = IIF(EMPTY(lcTrknumber),cBOLFill,lcTrknumber)
							ELSE
								lcTrknumber = "UNK"
							ENDIF

						CASE lWorldEaseFreight = .T.
							IF usesqlctn()
								IF xsqlexec("select * from ctnucc where mod='K' and ucc='"+tuccs.ucc+"'",,,"wh") # 0
									lcTrknumber = ALLTRIM(ctnucc.serialno)
									lcTrknumber = IIF(EMPTY(lcTrknumber),cBOLFill,lcTrknumber)
								ELSE
									lcTrknumber = "UNK"
								ENDIF
							ELSE
								IF SEEK(tuccs.ucc,"ctnucc","ucc")
									lcTrknumber = ALLTRIM(ctnucc.serialno)
									lcTrknumber = IIF(EMPTY(lcTrknumber),cBOLFill,lcTrknumber)
								ELSE
									lcTrknumber = "UNK"
								ENDIF
							ENDIF

						OTHERWISE
							lcTrknumber = cBOLFill
						ENDCASE

*!*							IF lParcelType And lWorldeaseFreight = .f.
*!*								=SEEK(tuccs.ucc,"package","ucc")
*!*								IF FOUND("package")
*!*									lcTrknumber = ALLTRIM(package.trknumber)
*!*									lcTrknumber = IIF(EMPTY(lcTrknumber),cBOLFill,lcTrknumber)
*!*								ELSE
*!*									lcTrknumber = "UNK"
*!*								ENDIF
*!*							ELSE
*!*								lcTrknumber = cBOLFill
*!*							ENDIF

						STORE "N9"+cfd+"TR"+cfd+ALLTRIM(lcTrknumber)+csegd TO cString
						DO cstringbreak
						nSegCtr = nSegCtr + 1

					ENDDO  && do each UPC/line number

				ENDSCAN  && tempdata.ucc = tuccs.ucc And tempdata.upc = tupcs.upc, scan through the UPCs in this cartons

			ENDSCAN  && scan through the UCC cartons on this pick ticket

		ENDSCAN  && toutdet.outshipid = thisoutshipid And units, scan throuh this pick ticket


&& now we need to scan through the UPC/line detail cursor  an look for any UPC/line num
*    If lTesting
		lxctr = lxctr +1
		SELECT upcdata
		SCAN FOR upcdata.lineqty=upcdata.remainqty AND !EMPTY(upcdata.lineqty)
			STORE "LX"+cfd+ALLTRIM(STR(lxctr))+csegd TO cString   && Carton Seq. # (up to Carton total)
			DO cstringbreak
			nSegCtr = nSegCtr + 1

			STORE "MAN"+cfd+"GM"+cfd+"NOQTY"+csegd TO cString
			DO cstringbreak
			nSegCtr = nSegCtr + 1

			STORE "W12"+cfd+"CL"+cfd+cfd+"0"+cfd+cfd+"EA"+cfd+ALLTRIM(upcdata.upc)+REPLICATE(cfd,4)+"0.0"+cfd+"G"+cfd+"L"+csegd TO cString
			DO cstringbreak
			nSegCtr = nSegCtr + 1

			STORE "N9"+cfd+"LI"+cfd+ALLTRIM(upcdata.linenum)+csegd TO cString
			DO cstringbreak
			nSegCtr = nSegCtr + 1

			STORE "N9"+cfd+"TR"+cfd+"NONE"+csegd TO cString
			DO cstringbreak
			nSegCtr = nSegCtr + 1
			lxctr = lxctr +1
		ENDSCAN
*   Endif
** scan for

*************************************************************************
*2^	END DETAIL MATCHING SHIP_REF LOOP
*************************************************************************
		WAIT CLEAR
		WAIT WINDOW "Now creating Section Closure information" NOWAIT NOCLEAR
		IF lTesting
		ENDIF

		IF lParcelType AND lTesting
			nTotCtnWt = IIF(EMPTY(nTotCtnWt) OR ISNULL(nTotCtnWt),5,nTotCtnWt)
			STORE "W03"+cfd+ALLTRIM(STR(nUnitSum))+cfd+ALLTRIM(STR(nTotCtnWt))+cfd+;
				cWeightUnit+cfd+ALLTRIM(STR(CEILING(nTotCtnVol)))+cfd+cVolUnit+csegd TO cString
		ELSE-
*			nTotCtnWt = IIF(ISNULL(nTotCtnWt) AND csendid = "TGFTEST",25,nTotCtnWt)
			nTotCtnWt = INT(outship.weight) && Added on 05.21.2014 per Paul to replace ^
			STORE "W03"+cfd+ALLTRIM(STR(nUnitSum))+cfd+ALLTRIM(STR(nTotCtnWt))+cfd+;
				cWeightUnit+csegd TO cString
		ENDIF
		FPUTS(nFilenum,cString)

		nTotCtnWt = 0
		nTotCtnVol = 0
		nTotCtnCount = 0
		nUnitSum = 0
		STORE  "SE"+cfd+ALLTRIM(STR(nSegCtr+2))+cfd+PADL(c_GrpCntrlNum,9,"0")+csegd TO cString
		FPUTS(nFilenum,cString)

		SELECT outship
		WAIT CLEAR
	ENDSCAN


*************************************************************************
*1^	END OUTSHIP MAIN LOOP
*************************************************************************

	DO close945
	=FCLOSE(nFilenum)
	SQLDISCONNECT(nHandle)
	SELECT tempack
	IF lTesting
		SET STEP ON
		LOCATE
		BROW
	ENDIF
**************commented out 1/9/17 TMARG
*!*		SELECT ackdata
*!*		APPEND FROM DBF('tempack')
	USE IN tempack
	USE IN ackdata


*	SET STEP ON
	IF !lTesting
		SELECT edi_trigger
		DO ediupdate WITH "945 CREATED",.F.
		SELECT edi_trigger
		LOCATE

		IF !USED("ftpedilog")
			SELECT 0
			USE F:\edirouting\ftpedilog ALIAS ftpedilog
			INSERT INTO ftpedilog (TRANSFER,ftpdate,filename,acct_name,TYPE) VALUES ("945-"+ccustname+"-"+cCustLoc,dt2,cFilenameHold,UPPER(ccustname),"945")
			USE IN ftpedilog
		ENDIF
	ENDIF

	WAIT CLEAR
	WAIT WINDOW cCustFolder+" 945 Process complete..." NOWAIT

*!* Create eMail confirmation message
	IF lTestMail
		SELECT 0
		USE F:\3pl\DATA\mailmaster ALIAS mm
		LOCATE FOR mm.edi_type = "MISC" AND mm.taskname = "GENERAL"
		tsendto = IIF(mm.use_alt,mm.sendtoalt,mm.sendto)
		tcc = "" && Iif(mm.use_alt,mm.ccalt,mm.cc)
		USE IN mm
	ENDIF

	tsubject = cMailName+IIF(lScanpack," (S-P)","")+" 945 EDI File from TGF as of "+dtmail+" ("+cCustLoc+")"+IIF(lTesting," *TEST*","")
	tattach = " "
	tmessage = "(S-P) 945 EDI Info from TGF, file "+cFilenameShort+CHR(13)
	tmessage = tmessage + "Division "+cDivision+", BOL# "+TRIM(cBOL)+CHR(13)
	tmessage = tmessage + "Work Orders: "+cWO_NumStr+","+CHR(13)
	tmessage = tmessage + "containing these picktickets:"+CHR(13)+CHR(13)
	tmessage = tmessage + cPTString + CHR(13)+CHR(13)
	tmessage = tmessage +"has been created and will be transmitted ASAP."+CHR(13)+CHR(13)
	IF !EMPTY(cMissDel)
		tmessage = tmessage+CHR(13)+CHR(13)+cMissDel+CHR(13)+CHR(13)
	ENDIF
	tmessage = tmessage + "If you have any questions, please eMail or call 732-750-9000 x217."
	IF lEmail
		DO FORM m:\dev\frm\dartmail2 WITH tsendto,tfrom,tsubject,tcc,tattach,tmessage,"A"
	ENDIF

	WAIT CLEAR
	WAIT WINDOW cMailName+" 945 EDI File output complete" AT 20,60 NOWAIT

*!* Transfers files to correct output folders
	IF !lTesting
		COPY FILE [&cFilenameHold] TO [&cFilenameArch]
	ENDIF
	IF !lTesting
		IF lDoAriatFilesOut
			COPY FILE [&cFilenameHold] TO [&cFilenameOut]
			DELETE FILE [&cFilenameHold]
			SELECT temp945
			COPY TO "f:\3pl\data\temp945a.dbf"
			SELECT 0
			USE "f:\3pl\data\temp945a.dbf" ALIAS temp945a
			SELECT 0
			USE "f:\3pl\data\pts_sent945.dbf" ALIAS pts_sent945
			APPEND FROM "f:\3pl\data\temp945a.dbf"
			USE IN pts_sent945
			USE IN temp945a
			DELETE FILE "f:\3pl\data\temp945a.dbf"
		ENDIF

		asn_out_data()
	ENDIF

	RELEASE ALL LIKE c_CntrlNum,c_GrpCntrlNum

	USE IN temp945

CATCH TO oErr
	SET STEP ON
	IF lDoCatch
		WAIT CLEAR
*		ASSERT .F. MESSAGE "In Error CATCH"
		IF cErrMsg != "EARLY ERROR"

*		lEmail = .F.
			DO ediupdate WITH IIF(EMPTY(cErrMsg),"ERRHAND ERROR",cErrMsg),.T.

		ENDIF
		tsubject = ccustname+" Error ("+TRANSFORM(oErr.ERRORNO)+") at "+TTOC(DATETIME())
		tattach  = ""
		IF lTesting
			tsendto  = tsendtotest
			tcc = tcctest
		ELSE
			tsendto  = tsendtoerr
			tcc = tccerr
		ENDIF

		tmessage = ccustname+" Error processing "+CHR(13)

		tmessage =tmessage+ "Try/CATCH Exeception Message:"+CHR(13)+;
			[  Error: ] + STR(oErr.ERRORNO) +CHR(13)+;
			[  LineNo: ] + STR(oErr.LINENO) +CHR(13)+;
			[  Message: ] + oErr.MESSAGE +CHR(13)+;
			[  Procedure: ] + oErr.PROCEDURE +CHR(13)+;
			[  Details: ] + oErr.DETAILS +CHR(13)+;
			[  StackLevel: ] + STR(oErr.STACKLEVEL) +CHR(13)+;
			[  LineContents: ] + oErr.LINECONTENTS+CHR(13)+;
			[  UserValue: ] + oErr.USERVALUE

		tsubject = "945 EDI Poller Error at "+TTOC(DATETIME())
		tattach  = ""
		tcc=""
		DO FORM m:\dev\frm\dartmail2 WITH tsendto,tfrom,tsubject,tcc,tattach,tmessage,"A"
		FCLOSE(nFilenum)
		SQLDISCONNECT(nHandle)
	ENDIF

FINALLY
	IF USED('OUTSHIP')
		USE IN outship
	ENDIF
	IF USED('OUTDET')
		USE IN outdet
	ENDIF
	IF USED('SHIPMENT')
		USE IN shipment
	ENDIF
	IF USED('PACKAGE')
		USE IN package
	ENDIF
	IF USED('SERFILE')
		USE IN serfile
	ENDIF
	IF USED('OUTWOLOG')
		USE IN outwolog
	ENDIF

	WAIT CLEAR

ENDTRY

*** END OF CODE BODY



****************************
PROCEDURE close945
****************************
** Footer Creation

STORE  "GE"+cfd+ALLTRIM(STR(nSTCount))+cfd+c_CntrlNum+csegd TO cString
FPUTS(nFilenum,cString)

STORE  "IEA"+cfd+"1"+cfd+PADL(c_CntrlNum,9,"0")+csegd TO cString
FPUTS(nFilenum,cString)

RETURN

****************************
PROCEDURE num_incr_isa
****************************

IF !USED("serfile")
	USE ("f:\3pl\data\serial\"+ccustname+"945_serial") IN 0 ALIAS serfile
ENDIF
SELECT serfile
IF lTesting AND lISAFlag
	nOrigSeq = serfile.seqnum
	lISAFlag = .F.
ENDIF
nISA_Num = serfile.seqnum
c_CntrlNum = ALLTRIM(STR(serfile.seqnum))
REPLACE serfile.seqnum WITH serfile.seqnum + 1 IN serfile
SELECT outship
RETURN

****************************
PROCEDURE num_incr_st
****************************

IF !USED("serfile")
	USE ("f:\3pl\data\serial\"+ccustname+"945_serial") IN 0 ALIAS serfile
ENDIF
SELECT serfile
IF lTesting AND lSTFlag
	nOrigGrpSeq = serfile.grpseqnum
	lSTFlag = .F.
ENDIF
c_GrpCntrlNum = ALLTRIM(STR(serfile.grpseqnum))
REPLACE serfile.grpseqnum WITH serfile.grpseqnum + 1 IN serfile
SELECT outship
ENDPROC

****************************
PROCEDURE segmentget
****************************

PARAMETER thisarray,lcKey,nLength

FOR i = 1 TO nLength
	IF i > nLength
		EXIT
	ENDIF
	lnEnd= AT("*",thisarray[i])
	IF lnEnd > 0
		lcThisKey =TRIM(SUBSTR(thisarray[i],1,lnEnd-1))
		IF OCCURS(lcKey,lcThisKey)>0
			RETURN SUBSTR(thisarray[i],lnEnd+1)
			i = 1
		ENDIF
	ENDIF
ENDFOR

RETURN ""

****************************
PROCEDURE ediupdate
****************************
PARAMETER cStatus,lIsError
*	SET STEP ON
lDoCatch = .F.

IF !lTesting
	SELECT edi_trigger
	nRec = RECNO()
	LOCATE
	IF cStatus = "NO 945 NEEDED - UPSGCC"  && Manually processed, per Mike Winter
		REPLACE processed WITH .T.,proc945 WITH .T.,file945 WITH cFilenameHold,isa_num WITH cISA_Num,;
			fin_status WITH cStatus,errorflag WITH .F.,when_proc WITH DATETIME() ;
			FOR edi_trigger.bol = cBOL AND accountid = nAcctNum
	ELSE
		IF !lIsError
			REPLACE processed WITH .T.,proc945 WITH .T.,file945 WITH cFilenameHold,isa_num WITH cISA_Num,;
				fin_status WITH "945 CREATED",errorflag WITH .F.,when_proc WITH DATETIME() ;
				FOR edi_trigger.bol = cBOL AND accountid = nAcctNum  AND IIF(lParcelType,edi_trigger.ship_ref = cPickticket,edi_trigger.accountid = nAcctNum)
		ELSE
			REPLACE processed WITH .T.,proc945 WITH .F.,file945 WITH cFilenameHold,;
				fin_status WITH cStatus,errorflag WITH .T. ;
				FOR edi_trigger.bol = cBOL AND accountid = nAcctNum  AND IIF(lParcelType,edi_trigger.ship_ref = cPickticket,edi_trigger.accountid = nAcctNum)
			IF lCloseOutput
				=FCLOSE(nFilenum)
				ERASE &cFilenameHold
			ENDIF
		ENDIF
	ENDIF
ENDIF

IF lIsError AND lEmail AND cStatus<>"SQL ERROR"
	tsubject = "945 Error in ARIAT BOL "+TRIM(cBOL)+"(At PT "+cShip_ref+")"
	tattach = " "
	tsendto = tsendtoerr
	tcc = tccerr
	tmessage = "945 Processing for BOL# "+cBOL+", WO# "+cWO_Num+" produced this error: (Office: "+cOffice+"): "+cStatus+CHR(13)+;
		IIF(cStatus = "BAD BOL#, PT #  ","Please add tracking number to Amazon field or email to Joe","Check EDI_TRIGGER and re-run")
	IF "TOTQTY ERR"$cStatus
		tmessage = tmessage + CHR(13) + "At OUTSHIPID: "+ALLTRIM(STR(m.outshipid))
	ENDIF
	DO FORM m:\dev\frm\dartmail2 WITH tsendto,tfrom,tsubject,tcc,tattach,tmessage,"A"
ENDIF

IF USED('outship')
	USE IN outship
ENDIF
IF USED('outdet')
	USE IN outdet
ENDIF

IF USED('shipcode')
	USE IN shipcode
ENDIF

IF USED('serfile')
	SELECT serfile
	IF lTesting
		REPLACE serfile.seqnum WITH nOrigSeq
		REPLACE serfile.grpseqnum WITH nOrigGrpSeq
	ENDIF
	USE IN serfile
ENDIF
IF USED('scacs')
	USE IN scacs
ENDIF
IF USED('mm')
	USE IN mm
ENDIF
IF USED('tempx')
	USE IN tempx
ENDIF
IF USED('vbug')
	USE IN VBUG
ENDIF
IF !lTesting
	SELECT edi_trigger
	LOCATE
ENDIF

ENDPROC

****************************
PROCEDURE cstringbreak
****************************
cLen = LEN(ALLTRIM(cString))
IF ISNULL(cString)
	ASSERT .F. MESSAGE "At cStringBreak procedure"
ENDIF
FPUTS(nFilenum,cString)
ENDPROC


****************************
PROCEDURE cszbreak
****************************
cCSZ = ALLT(m.CSZ)

FOR ii = 5 TO 2 STEP -1
	cCSZ = STRTRAN(cCSZ,SPACE(ii),SPACE(1))
ENDFOR
cCSZ = STRTRAN(cCSZ,",","")
len1 = LEN(ALLT(cCSZ))
nSpaces = OCCURS(" ",cCSZ)

IF nSpaces<2
	cErrMsg = "BAD CSZ INFO"
	DO ediupdate WITH cErrMsg,.T.
	THROW
ENDIF

*	ASSERT .F. MESSAGE "In CSZ Breakout"
FOR ii = nSpaces TO 1 STEP -1
	ii1 = ALLT(STR(ii))
	DO CASE
	CASE ii = nSpaces
		nPOS = AT(" ",cCSZ,ii)
		cZip = ALLT(SUBSTR(cCSZ,nPOS))
		nEndState = nPOS
	CASE ii = (nSpaces - 1)
		nPOS = AT(" ",cCSZ,ii)
		cState = ALLT(SUBSTR(cCSZ,nPOS,nEndState-nPOS))
		IF nSpaces = 2
			cCity = ALLT(LEFT(cCSZ,nPOS))
			EXIT
		ENDIF
	OTHERWISE
		nPOS = AT(" ",cCSZ,ii)
		cCity = ALLT(LEFT(cCSZ,nPOS))
*				WAIT WINDOW "CITY: "+cCity TIMEOUT 2
	ENDCASE
ENDFOR
ENDPROC

*******************************************************************************************************
*!*	PROCEDURE scacenter
*!*	USE wh.scacenter ALIAS scacenter
*!*	replace scacenterdt WITH DATETIME(),scacenterby WITH IIF(TYPE("guserid")="C" AND !EMPTY(guserid),guserid,"???")
*!*	endproc
