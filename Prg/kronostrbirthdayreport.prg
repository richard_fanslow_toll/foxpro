* List Birthday Hours taken by each Time Reviewer's employees since 1 year ago.

*** BY TIMEREVIEWER, and email to each just his people ****

runack("KRONOSTRBIRTHDAYREPORT")

LOCAL loKronosTRBirthdayReport, lcTRNum, llSpecial

llSpecial = .F.

loKronosTRBirthdayReport = CREATEOBJECT('KronosTRBirthdayReport')
loKronosTRBirthdayReport.SetSpecial()
USE F:\UTIL\KRONOS\DATA\TIMERVWR.DBF AGAIN IN 0 ALIAS RVWRLIST
SELECT RVWRLIST

IF llSpecial THEN
	SCAN FOR SPECIAL AND LACTIVE AND LBDAY
		loKronosTRBirthdayReport.MAIN( RVWRLIST.TIMERVWR, RVWRLIST.NAME, RVWRLIST.EMAIL, RVWRLIST.CC )
	ENDSCAN
ELSE
	SCAN FOR LACTIVE AND LBDAY
		loKronosTRBirthdayReport.MAIN( RVWRLIST.TIMERVWR, RVWRLIST.NAME, RVWRLIST.EMAIL, RVWRLIST.CC )
	ENDSCAN
ENDIF && llSpecial

CLOSE DATABASES ALL
RETURN

#DEFINE LOGIT 1
#DEFINE WAITIT 2
#DEFINE NOWAITIT 4
#DEFINE SENDIT 8
#DEFINE CRLF CHR(13) + CHR(10)
#DEFINE RETURN_DATA_MANDATORY .F.
#DEFINE RETURN_DATA_NOT_MANDATORY .T.

DEFINE CLASS KronosTRBirthdayReport AS CUSTOM

	cProcessName = 'KronosTRBirthdayReport'

	lSpecial = .F.

	lTestMode = .F.  && .F. FOR NORMAL OPERATION, .T. TO SUPPRESS ALL BUT DEVELOPER EMAILS AND CHANGE INPUT TABLE, ETC.

	lAutoYield = .T.

	cStartTime = TTOC(DATETIME())

	* Excel properties
	oExcel = NULL

	* connection properties
	nSQLHandle = 0

	* file properties
	nFileHandle = 0

	* table properties
	cFoxProTimeReviewerTable = "F:\UTIL\KRONOS\DATA\TIMERVWR"

	* date properties
	dtNow = DATETIME()
	dToday = DATE()
	cToday = DTOC(DATE())

	* wait window properties
	nWaitWindowTimeout = 2
	lWaitWindowIsOn = .T.

	* logfile properties
	lLoggingIsOn = .T.
	cLogFile = 'F:\UTIL\KRONOS\Logfiles\KronosTRBirthdayReport_log.txt'

	* INTERNAL email properties
	lSendInternalEmailIsOn = .T.
	cFrom = 'FMI Corporate <fmicorporate@fmiint.com>'
	cSendTo = 'Mark Bennett <mbennett@fmiint.com>'
	*cSendTo = 'Lucille Waldrip <lwaldrip@fmiint.com>, Lori Guiliano <lguiliano@fmiint.com>, Mark Bennett <mbennett@fmiint.com>'
	*cCC = 'Mark Bennett <mbennett@fmiint.com>, Lucille Waldrip <lwaldrip@fmiint.com>'
	cCC = 'Mark Bennett <mbennett@fmiint.com>'
	cSubject = 'KRONOS TIME REVIEWER BIRTHDAYS REPORT for: ' + TTOC(DATETIME())
	cAttach = ''
	cBodyText = ''

	cCOMPUTERNAME = ''
	cUSERNAME = ''

	FUNCTION INIT
		IF NOT DODEFAULT()
			RETURN .F.
		ENDIF
		WITH THIS
			LOCAL loError
			CLEAR
			*SET RESOURCE OFF
			CLOSE DATA
			SET CENTURY ON
			SET DATE YMD
			SET HOURS TO 24
			SET ANSI ON
			SET TALK OFF
			SET DELETED ON
			SET CONSOLE OFF
			SET EXCLUSIVE OFF
			SET SAFETY OFF
			SET EXACT OFF
			SET STATUS BAR ON
			SET SYSMENU OFF
			SET ENGINEBEHAVIOR 70
			_VFP.AUTOYIELD = .lAutoYield
			.cCOMPUTERNAME = GETENV("COMPUTERNAME")
			.cUSERNAME = GETENV("USERNAME")
			IF .lTestMode THEN
				.cSendTo = 'Mark Bennett <mbennett@fmiint.com>'
				.cLogFile = 'F:\UTIL\KRONOS\Logfiles\KronosTRBirthdayReport_log_TESTMODE.txt'
			ENDIF
			.lLoggingIsOn = .lLoggingIsOn AND NOT EMPTY(.cLogFile)
			IF .lLoggingIsOn THEN
				SET ALTERNATE TO (.cLogFile) ADDITIVE
				SET ALTERNATE ON
			ENDIF
			* re-init date properties after all SETTINGS
			.dtNow = DATETIME()
			.dToday = DATE()
			.cToday = DTOC(DATE())
			.cAttach = ''

			WAIT WINDOW NOWAIT "Opening Excel..."
			TRY
				.oExcel = CREATEOBJECT("excel.application")
				.oExcel.VISIBLE = .F.
			CATCH
				.oExcel = NULL
			ENDTRY
		ENDWITH
	ENDFUNC


	FUNCTION DESTROY
		WITH THIS
			IF .lLoggingIsOn  THEN
				SET ALTERNATE OFF
				SET ALTERNATE TO
			ENDIF
			IF TYPE('.oExcel') = 'O' AND NOT ISNULL(.oExcel) THEN
				.oExcel.QUIT()
			ENDIF
		ENDWITH
		DODEFAULT()
	ENDFUNC


	FUNCTION MAIN

		LPARAMETERS tcTRNumber, tcTRName, tcTReMail, tcCC

		LOCAL lnNumberOfErrors, lcSQL, lcSQL2, loError, lcTopBodyText, llValid
		LOCAL lcXLFileName, ldStartDate, ldEndDate, lcStartDate, lcEndDate, ldDate
		LOCAL lcSQLStartDate, lcSQLEndDate, lcXLFileName, lcSpecialtext
		LOCAL oWorkbook, oWorksheet, lnRow, lcRow, lnStartRow, lcStartRow, lnEndRow, lnPP, lcPP, lnNN, lcNN, lcJJ
		LOCAL lcColsToProcess, lnColumn, lcBoldColumn, i, j, lcBoldRow, luCellValue, lcTargetDirectory, ldPayDate, lcFiletoSaveAs
		LOCAL lcFileDate, lcDivision, lcRateType, lcTitle, llSaveAgain, lcPayCode
		LOCAL lcTRName, lcTReMail, lcTRNumber, lcCC, lcTestCC, lcFoxProTimeReviewerTable

		WITH THIS

			TRY
				lnNumberOfErrors = 0
				lcTopBodyText = ''
				lcTRNumber = tcTRNumber
				lcTRName = ALLTRIM(tcTRName)
				lcTReMail = ALLTRIM(tcTReMail)
				lcCC = ALLTRIM(tcCC)

				* repeat some setup
				SET CONSOLE OFF
				SET TALK OFF
				.cCC = 'Mark Bennett <mbennett@fmiint.com>, Lucille Waldrip <lwaldrip@fmiint.com>'
				.cStartTime = TTOC(DATETIME())
				.dtNow = DATETIME()
				.cAttach = ''

				IF .lTestMode THEN
					.cSendTo = 'Mark Bennett <mbennett@fmiint.com>'
					IF NOT EMPTY(lcCC) THEN
						lcTestCC = lcCC + ", " + .cCC
					ELSE
						lcTestCC = .cCC
					ENDIF
					.cCC = ""
				ELSE
					.cSendTo = lcTReMail
					IF NOT EMPTY(lcCC) THEN
						.cCC = lcCC + ", " + .cCC
					ENDIF
				ENDIF

				.cBodyText = ""

				*.TrackProgress('==================================================================================================================', LOGIT+SENDIT)
				.TrackProgress('KRONOS TIME REVIEWER BIRTHDAYS REPORT process started....', LOGIT+SENDIT+NOWAITIT)
				.TrackProgress('COMPUTERNAME = ' + .cCOMPUTERNAME, LOGIT+SENDIT)
				.TrackProgress('USERNAME = ' + .cUSERNAME, LOGIT+SENDIT)
				.TrackProgress('==================================================================================================================', LOGIT+SENDIT)
				IF .lTestMode THEN
					.TrackProgress('', LOGIT)
					.TrackProgress('==================================================================================================================', LOGIT+SENDIT)
					.TrackProgress('                        TEST MODE                        ', LOGIT+SENDIT)
					.TrackProgress('Time Reviewer # = ' + lcTRNumber, LOGIT+SENDIT)
					.TrackProgress('Time Reviewer Name = ' + lcTRName, LOGIT+SENDIT)
					.TrackProgress('cSendTo would be: ' + lcTReMail, LOGIT+SENDIT)
					.TrackProgress('cCC would be: ' + lcTestCC, LOGIT+SENDIT)
					.TrackProgress('==================================================================================================================', LOGIT+SENDIT)
				ENDIF
				.TrackProgress('', LOGIT)

				* report on period from yesterday through 1 year before yesterday
				ldDate = .dToday
				ldEndDate = ldDate - 1
				ldStartDate= GOMONTH(ldEndDate,-12)


				SET DATE AMERICAN

				lcStartDate = STRTRAN(DTOC(ldStartDate),"/","-")
				lcEndDate = STRTRAN(DTOC(ldEndDate),"/","-")

				SET DATE YMD

				*	 create "YYYYMMDD" safe date format for use in SQL query
				* NOTE: for SQL we are going to use < EndDate ( not <= ), so we are going to add 1 to that date here for querying purposes.
				lcSQLStartDate = STRTRAN(DTOC(ldStartDate),"/","")
				lcSQLEndDate = STRTRAN(DTOC(ldEndDate + 1),"/","")

				.cSubject = 'KRONOS TIME REVIEWER BIRTHDAYS REPORT for ' + lcTRName + ': ' + lcStartDate + " to " + lcEndDate

				IF USED('CURWTKHOURS') THEN
					USE IN CURWTKHOURS
				ENDIF

				* open FoxPro Time Reviewer table
				IF USED('TIMERVWRTABLE') THEN
					USE IN TIMERVWRTABLE
				ENDIF
				lcFoxProTimeReviewerTable = .cFoxProTimeReviewerTable

				USE (lcFoxProTimeReviewerTable) AGAIN IN 0 ALIAS TIMERVWRTABLE


				* main SQL
				lcSQL = ;
					" SELECT " + ;
					" D.LABORLEV2NM AS DIVISION, " + ;
					" D.LABORLEV3NM AS DEPT, " + ;
					" D.LABORLEV7NM AS TIMERVWR, " + ;
					" C.FULLNM AS EMPLOYEE, " + ;
					" A.APPLYDTM AS DATE, " + ;
					" E.NAME AS PAYCODEDESC, " + ;
					" E.ABBREVIATIONCHAR AS PAYCODE, " + ;
					" C.PERSONNUM AS FILE_NUM, " + ;
					" C.PERSONID, " + ;
					" (A.DURATIONSECSQTY / 3600.00) AS HOURS " + ;
					" FROM WFCTOTAL A " + ;
					" JOIN WTKEMPLOYEE B " + ;
					" ON B.EMPLOYEEID = A.EMPLOYEEID " + ;
					" JOIN PERSON C " + ;
					" ON C.PERSONID = B.PERSONID " + ;
					" JOIN LABORACCT D " + ;
					" ON D.LABORACCTID = A.LABORACCTID " + ;
					" JOIN PAYCODE E " + ;
					" ON E.PAYCODEID = A.PAYCODEID " + ;
					" WHERE (A.APPLYDTM >= '" + lcSQLStartDate + "')" + ;
					" AND (A.APPLYDTM < '" + lcSQLEndDate + "')" + ;
					" AND (D.LABORLEV1NM = 'SXI') " + ;
					" AND (D.LABORLEV7NM = " + lcTRNumber + ")" + ;
					" AND (E.ABBREVIATIONCHAR IN ('BDY','BAD','PPB')) " + ;
					" ORDER BY C.FULLNM, A.APPLYDTM, E.NAME "


				IF .lTestMode THEN
					.TrackProgress('', LOGIT+SENDIT)
					.TrackProgress('lcSQL =' + lcSQL, LOGIT+SENDIT)
				ENDIF

				.TrackProgress('Connecting to WTK System....', LOGIT+SENDIT+NOWAITIT)

				* get ss#s from ADP
				OPEN DATABASE F:\UTIL\KRONOS\KRONOSEDI

				*.nSQLHandle = SQLCONNECT("KRONOSEDI","tkcsowner","tkcsowner")
				.nSQLHandle = SQLCONNECT("WTK_ON_PAYROLL2","sa","kr0n1tes")

				IF USED('CURWTKHOURS') THEN
					USE IN CURWTKHOURS
				ENDIF

				IF .nSQLHandle > 0 THEN

					*IF .ExecSQL(lcSQL, 'CURWTKHOURS', RETURN_DATA_MANDATORY) THEN
					IF .ExecSQL(lcSQL, 'CURWTKHOURS', RETURN_DATA_NOT_MANDATORY) THEN

						IF USED('CURWTKHOURS') AND (NOT EOF('CURWTKHOURS')) THEN

							WAIT WINDOW NOWAIT "Preparing data..."

							SELECT CURWTKHOURS
							GOTO TOP
							IF EOF() THEN
								.TrackProgress("There was no Birthday data to report!", LOGIT+SENDIT)
							ELSE

								lcRateType = "HOURLY"

								*!*								WAIT WINDOW NOWAIT "Opening Excel..."
								*!*								oExcel = CREATEOBJECT("excel.application")

								oWorkbook = .oExcel.workbooks.OPEN("F:\UTIL\KRONOS\TEMPLATES\KronosTRBirthdayReport.XLS")

								***********************************************************************************************************************
								***********************************************************************************************************************
								WAIT WINDOW NOWAIT "Looking for target directory..."
								* see if target directory exists, and, if not, create it
								lcFileDate = PADL(MONTH(ldDate),2,"0") + "-"  + PADL(DAY(ldDate),2,"0") + "-" + PADL(YEAR(ldDate),4,"0")

								lcTargetDirectory = "F:\UTIL\KRONOS\TRBIRTHDAYREPORTS\" + lcFileDate + "\"

								* create directory if it doesn't exist
								IF NOT DIRECTORY(lcTargetDirectory) THEN
									MKDIR (lcTargetDirectory)
									WAIT WINDOW TIMEOUT 1 "Made Target Directory " + lcTargetDirectory
								ENDIF

								* if target directory exists, save there
								llSaveAgain = .F.
								IF DIRECTORY(lcTargetDirectory) THEN
									lcFiletoSaveAs = lcTargetDirectory + lcFileDate + " Kronos "  + lcRateType + " for " + lcTRName
									lcXLFileName = lcFiletoSaveAs + ".XLS"
									IF FILE(lcXLFileName) THEN
										DELETE FILE (lcXLFileName)
									ENDIF
									oWorkbook.SAVEAS(lcFiletoSaveAs)
									* set flag to save again after sheet is populated
									llSaveAgain = .T.
								ENDIF
								***********************************************************************************************************************
								***********************************************************************************************************************

								lnRow = 3
								lnStartRow = lnRow + 1
								lcStartRow = ALLTRIM(STR(lnStartRow))

								oWorksheet = oWorkbook.Worksheets[1]
								oWorksheet.RANGE("A" + lcStartRow,"T1000").clearcontents()

								oWorksheet.RANGE("A" + lcStartRow,"T1000").FONT.SIZE = 12
								oWorksheet.RANGE("A" + lcStartRow,"T1000").FONT.NAME = "Arial"
								oWorksheet.RANGE("A" + lcStartRow,"T1000").FONT.bold = .T.

								lcTitle = "Kronos Birthday Report for Period:" + lcStartDate + " - " + lcEndDate
								oWorksheet.RANGE("A1").VALUE = lcTitle
								oWorksheet.RANGE("A2").VALUE = "EEs for: " + lcTRName

								* main scan/processing
								SELECT CURWTKHOURS
								SCAN

									lnRow = lnRow + 1
									lcRow = LTRIM(STR(lnRow))
									oWorksheet.RANGE("A" + lcRow).VALUE = ALLTRIM(CURWTKHOURS.EMPLOYEE)
									oWorksheet.RANGE("B" + lcRow).VALUE = CURWTKHOURS.FILE_NUM
									oWorksheet.RANGE("C" + lcRow).VALUE = TTOD(CURWTKHOURS.DATE)
									oWorksheet.RANGE("D" + lcRow).VALUE = ALLTRIM(CURWTKHOURS.PAYCODEDESC)
									oWorksheet.RANGE("E" + lcRow).VALUE = CURWTKHOURS.HOURS

									oWorksheet.RANGE("A" + lcRow,"E" + lcRow).Interior.ColorIndex = IIF(((lnRow % 2) = 1),15,0)

								ENDSCAN

								lnEndRow = lnRow
								lcEndRow = ALLTRIM(STR(lnEndRow))

*!*									***********************************
*!*									* 01/20/04 MB - do all the bolding/clearing of numeric columns here in one nested loop,
*!*									* to eliminate a lot of extraneous code above.
*!*									lcColsToProcess = "EFGHIJKLMNOPQRSTU"
*!*									FOR i = 1 TO LEN(lcColsToProcess)
*!*										lcBoldColumn = SUBSTR(lcColsToProcess,i,1)
*!*										FOR j = lnStartRow TO lnEndRow
*!*											lcBoldRow = ALLTRIM(STR(j))
*!*											luCellValue = oWorksheet.RANGE(lcBoldColumn + lcBoldRow).VALUE
*!*											IF TYPE([luCellValue])= "N" THEN
*!*												IF luCellValue > 0.0 THEN
*!*													oWorksheet.RANGE(lcBoldColumn + lcBoldRow).FONT.bold = .T.
*!*												ELSE
*!*													oWorksheet.RANGE(lcBoldColumn + lcBoldRow).VALUE = ""
*!*												ENDIF
*!*											ENDIF
*!*										ENDFOR  && j
*!*									ENDFOR  && i
*!*									***********************************

								*!*								lcJJ = lcRow


								lnRow = lnRow + 1
								lcRow = LTRIM(STR(lnRow))
								*oWorksheet.RANGE("A" + lcStartRow,"T" + lcRow).SELECT
								oWorksheet.PageSetup.PrintArea = "$A$"  + lcStartRow + ":$E$" + lcRow

								* save again
								IF llSaveAgain THEN
									oWorkbook.SAVE()
								ENDIF
								oWorkbook.CLOSE()

								*******************************************************************************************
								*******************************************************************************************

								lcTopBodyText = ;
									"Time Reviewer, " + CRLF + CRLF + ;
									"FYI, the attached spreadsheet report shows the Birthdays taken by your employees in the last year."

								*!*									CRLF + CRLF

								.cBodyText = lcTopBodyText + CRLF + CRLF + ;
									"==================================================================================================================" + ;
									CRLF + "<report log follows>" + ;
									CRLF + CRLF + .cBodyText

								*.oExcel.VISIBLE = .T.

								*.oExcel.QUIT()

							ENDIF  && EOF() CUREMPLOYEES

							IF FILE(lcXLFileName) THEN
								.cAttach = lcXLFileName
								.TrackProgress('Created Birthday Report File : ' + lcXLFileName, LOGIT+SENDIT)
								.TrackProgress('KRONOS TIME REVIEWER BIRTHDAYS REPORT process ended normally.', LOGIT+SENDIT+NOWAITIT)
							ELSE
								.TrackProgress('!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!', LOGIT+SENDIT)
								.TrackProgress("ERROR: There was a problem Creating Birthday Report File : " + lcXLFileName, LOGIT+SENDIT)
								.TrackProgress('!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!', LOGIT+SENDIT)
							ENDIF

						ELSE

							.cAttach = ''
							.TrackProgress("We didn't find any Birthdays taken by your employees in the last year!", LOGIT+SENDIT)

						ENDIF && USED('CURWTKHOURS') AND (NOT EOF('CURWTKHOURS')) THEN

					ENDIF  &&  .ExecSQL(lcSQL, 'CURWTKEMPLOYEE', RETURN_DATA_MANDATORY)


					IF USED('CURWTKHOURS') THEN
						USE IN CURWTKHOURS
					ENDIF

					SQLDISCONNECT(.nSQLHandle)

					*CLOSE DATABASES ALL
				ELSE
					* connection error
					.TrackProgress('Unable to connect to WTK System.', LOGIT+SENDIT)
				ENDIF   &&  .nSQLHandle > 0
			CATCH TO loError

				.TrackProgress('There was an error.',LOGIT+SENDIT)
				.TrackProgress('The Error # is: ' + TRANSFORM(loError.ErrorNo), LOGIT+SENDIT)
				.TrackProgress('The Error Message is: ' + TRANSFORM(loError.MESSAGE), LOGIT+SENDIT)
				.TrackProgress('The Error occurred in Program: ' + TRANSFORM(loError.PROCEDURE), LOGIT+SENDIT)
				.TrackProgress('The Error occurred at line #: ' + TRANSFORM(loError.LINENO), LOGIT+SENDIT)
				lnNumberOfErrors = lnNumberOfErrors + 1
				IF TYPE('.oExcel') = 'O' AND NOT ISNULL(.oExcel) THEN
					.oExcel.VISIBLE = .T.
				ENDIF
				IF .nSQLHandle > 0 THEN
					SQLDISCONNECT(.nSQLHandle)
				ENDIF
				*CLOSE DATA
				*CLOSE ALL

			ENDTRY

			*CLOSE DATA
			WAIT CLEAR
			***************** INTERNAL email results ******************************
			.TrackProgress('About to send status email.',LOGIT)
			.TrackProgress('==================================================================================================================', SENDIT)
			.TrackProgress('The logfile is: ' + .cLogFile, SENDIT)
			.TrackProgress('KRONOS TIME REVIEWER BIRTHDAYS REPORT process started: ' + .cStartTime, LOGIT+SENDIT)
			.TrackProgress('KRONOS TIME REVIEWER BIRTHDAYS REPORT process finished: ' + TTOC(DATETIME()), LOGIT+SENDIT)

			IF .lSendInternalEmailIsOn THEN
				* try to trap error from not having dartmail dll's registered on user's pc...
				TRY
					DO FORM dartmail2 WITH .cSendTo,.cFrom,.cSubject,.cCC,.cAttach,.cBodyText,"A"
					.TrackProgress('Sent status email.',LOGIT)
				CATCH TO loError
					=MESSAGEBOX('There was an error sending the status email. Please contact Mark Bennett.',0+64,"KRONOS TIME REVIEWER BIRTHDAYS REPORT")
					.TrackProgress('There was an error sending the status email.',LOGIT)
					.TrackProgress('The Error # is: ' + TRANSFORM(loError.ErrorNo), LOGIT)
					.TrackProgress('The Error Message is: ' + TRANSFORM(loError.MESSAGE), LOGIT)
					.TrackProgress('The Error occurred in Program: ' + TRANSFORM(loError.PROCEDURE), LOGIT)
					.TrackProgress('The Error occurred at line #: ' + TRANSFORM(loError.LINENO), LOGIT)
					lnNumberOfErrors = lnNumberOfErrors + 1
					CLOSE DATA
				ENDTRY
			ELSE
				.TrackProgress('Did not send status email: THIS.lSendInternalEmailIsOn = FALSE.',LOGIT)
			ENDIF

		ENDWITH
		RETURN
	ENDFUNC && main


	FUNCTION ExecSQL
		LPARAMETERS tcSQL, tcCursorName, tlNoDataReturnedIsOkay
		LOCAL llRetval, lnResult
		WITH THIS
			* close target cursor if it's open
			IF USED(tcCursorName)
				USE IN (tcCursorName)
			ENDIF
			WAIT WINDOW "Fetching remote data; please wait..." NOWAIT
			lnResult = sqlexec(.nSQLHandle, tcSQL, tcCursorName)
			llRetval = ( lnResult > 0 )
			IF llRetval THEN
				* see if any data came back
				IF NOT tlNoDataReturnedIsOkay THEN
					IF (NOT USED(tcCursorName)) OR EOF(tcCursorName) THEN
						llRetval = .F.
						.TrackProgress('No data was returned by this query: ' +CRLF + tcSQL, LOGIT+SENDIT)
					ENDIF
				ENDIF
			ELSE
				.TrackProgress("Result Code [" + TRANSFORM(lnResult) + "] returned by this query: " + tcSQL, LOGIT+SENDIT)
			ENDIF
			WAIT CLEAR
			RETURN llRetval
		ENDWITH
	ENDFUNC


	FUNCTION SetSpecial
		THIS.lSpecial = .T.
	ENDFUNC


	PROCEDURE TrackProgress
		* do any combination of Wait Window, writing to logfile, and adding to body of email,
		* based on nFlags parameter.
		LPARAMETERS tcExpression, tnFlags
		WITH THIS
			IF BITAND(tnFlags,LOGIT) = LOGIT THEN
				IF .lLoggingIsOn THEN
					?
					? .cProcessName + "  " + TTOC(DATETIME()) + ": " + tcExpression
				ENDIF
			ENDIF
			IF .lWaitWindowIsOn THEN
				IF BITAND(tnFlags,NOWAITIT) = NOWAITIT THEN
					WAIT WINDOW tcExpression NOWAIT
				ENDIF
				IF BITAND(tnFlags,WAITIT) = WAITIT THEN
					WAIT WINDOW tcExpression TIMEOUT .nWaitWindowTimeout
				ENDIF
			ENDIF
			IF BITAND(tnFlags,SENDIT) = SENDIT THEN
				IF .lSendInternalEmailIsOn THEN
					.cBodyText = .cBodyText + tcExpression + CRLF + CRLF
				ENDIF
			ENDIF
		ENDWITH
	ENDPROC  &&  TrackProgress

ENDDEFINE

