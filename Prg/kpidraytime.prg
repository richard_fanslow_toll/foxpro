**Drayage Time KPI

**Time from container availibilty to container pickup
**  - Full ocean containers Only
**  - Times based on availLog.avail Vs. wolog.pickedUp
**
**Comments/Issues:
**  - if no availLog record found with a valid "avail" date, wo_date used in its stead
**  - if empty(pickedUp), wo_date used in its stead
Wait window "Gathering data for Drayage Time Spreadsheet..." nowait noclear

USE f:\wo\wodata\wolog In 0

*dstartdt={1/1/18}
*denddt={1/20/18}
*xaccountid=6638
*xtrkoffice="N"
*goffice="N"

*!*		Select w.*, w.accountid as acctNumFld, acctname as acctNameFld, Max(called) as lastCall, ;
*!*			   called, iif(empty(pickedup),wo_date,pickedup) as pudt, Iif(IsNull(avail), wo_date, Iif(Empty(avail), called, avail)) as avail ;
*!*			from wolog w ;
*!*				left join availlog a on w.wo_num = a.wo_num ;
*!*			group by w.wo_num, a.called, a.avail, a.remarks ;
*!*			order by w.acctname, w.wo_date ;
*!*			where &cTrkAcct ;
*!*				Iif(Empty(w.pickedUp), Between(w.wo_date, dStartDt, dEndDt), Between(w.pickedUp, dStartDt, dEndDt)) and ;
*!*				w.office = xtrkoffice and type # "D" ;
*!*		  into cursor csrDetail

**in order to get away from the SLOW "Iif(Empty(w.pickedUp), Between(w.wo_date, dStartDt, dEndDt), Between(w.pickedUp, dStartDt, dEndDt))" 
**  filetr, grab everything and delete excess - mvw 04/19/12
set step on

if empty(xaccountid)
	xacctfilter=".t."
else
	xacctfilter="accountid="+transform(xaccountid)
endif

xsqlexec("select wo_num, called, avail, remarks from availlog where "+xacctfilter+" and office='"+goffice+"'",,,"wo")
index on wo_num tag wo_num
set order to

Select w.*, w.accountid as acctNumFld, acctname as acctNameFld, Max(called) as lastCall, ;
	   called, iif(emptynul(pickedup),wo_date,pickedup) as pudt, Iif((emptynul(avail) and emptynul(called)), wo_date, Iif(Emptynul(avail), called, avail)) as avail ;
	from wolog w ;
		left join availlog a on w.wo_num = a.wo_num ;
	group by w.wo_num, a.called, a.avail, a.remarks ;
	order by w.acctname, w.wo_date ;
	where &xacctfilter and (Between(w.wo_date, dStartDt, dEndDt) or Between(w.pickedup, dStartDt, dEndDt)) and ;
		w.office = xtrkoffice and type # "D" ;
  into cursor xtemp readwrite

delete for !emptynul(pickedup) and !between(pickedup, dStartDt, dEndDt)


**availType: 0=avail, 1=wo_date, 2=last call date
cLocFilter = Iif(!Empty(cAcctNum), "", " and a.&cLocFld")
Select c.*, Iif(IsNull(avail), 1, Iif(Empty(avail), 2, 0)) as availType, 00 as DateDiff ;
	from xtemp c ;
		left join account a on a.accountid = c.accountid ;
	group by c.wo_num order by c.acctNameFld, c.wo_date ;
	where (IsNull(called) or called = lastCall) &cLocFilter ;
  into cursor csrDetail readWrite

use in xtemp

**calculate date differential into dateDiff - make necessary adjustments for weekends & FMI holidays
DO kpicalcdiff with "avail", "pudt", "dateDiff"

Locate
If Eof()
	strMsg = "No data found."
	=MessageBox(strMsg, 48, "Drayage Time")
Else
	Wait window "Creating Drayage Time Spreadsheet..." nowait noclear

	oWorkbook=""
	DO xlsTimeGeneric with "where (!Empty(pudt) or !emptynul(wo_date))", "pudt", "1", "", "Drayage Times", "Picked Up Date Vs. Availablilty", ;
		" - Date range entered reflects container P/U dates.", " - Totals exclude wos. with empty picked up dates.", "''", .t., .t.

	If lDetail
		Select csrTotals
		nWorksheet = 2
		Scan
			STORE tempfox+SUBSTR(SYS(2015), 3, 10)+".xls" TO cTmpFile
			Select *, Iif(type="A", awb, container) as field1 from csrDetail ;
				where Year(pudt) = csrTotals.year and Month(pudt) = csrTotals.month into cursor csrDet
			Copy to &cTmpFile fields acctNameFld, wo_num, type, field1, wo_date, avail, pudt, dateDiff type xl5

			DO xlsTimeGenDetail with cTmpFile, nWorkSheet, "G"

			With oWorkbook.worksheets[nWorksheet]
			  Select csrDet
			  Scan
				cRow = Alltrim(Str(Recno()+1))
				.Range("A"+cRow+":"+"G"+cRow).Interior.Color = Iif(Empty(pudt), RGB(255,255,128), Rgb(255,255,255))
				.Range("E"+cRow).Interior.Color = Iif(availType=0, .Range("E"+cRow).Interior.Color, ;
					Iif(availType=1, Rgb(255,0,0), Rgb(0,0,255)))
			  EndScan
			
			  .Range("A"+Alltrim(Str(Reccount("csrDet")+5))+":A"+Alltrim(Str(Reccount("csrDet")+5))).font.color = Rgb(255,0,0) &&red
			  .Range("A"+Alltrim(Str(Reccount("csrDet")+5))).Value = "Notes:"
			  .Range("A"+Alltrim(Str(Reccount("csrDet")+6))).Value = " - All available dates highlighted RED are actually w/o dates."
			  .Range("A"+Alltrim(Str(Reccount("csrDet")+7))).Value = " - All available dates highlighted BLUE are actually last call dates."
			  .Range("A"+Alltrim(Str(Reccount("csrDet")+8))).Value = ;
					" - All records highlighted YELLOW contain empty P/U dates and therefore were not used in the calculations."
			EndWith

			nWorksheet = nWorksheet+1
			use in csrdet
		EndScan
	EndIf

	oWorkbook.Worksheets[1].range("A1").activate()
	oWorkbook.Save()
EndIf

use in wolog
use in availlog
use in csrdetail
if used("csrtotals")
  use in csrtotals
endif