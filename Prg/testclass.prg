LOCAL loFilesInUse, lcServer, lcDir, lcUser
lcServer = "App1n2"
*lcDir = "D:\FMISYS\LNT\"
lcDir = "D:\FMISYS\LNT\LNTBARCODE.DBF"
lcUser = ""

* clear curFilesInUse
IF USED('curFilesInUse')
	USE IN curFilesInUse
ENDIF

CREATE CURSOR curFilesInUse ;
	( cFileName c(50), ;
	nNumLocks i, ;
	cUser c(20), ;
	cPermissions c(50), ;
	nNetInfoID i )

SET PROCEDURE TO F:\Util\FilesInUse\clsFilesInUse.prg ADDITIVE
loFilesInUse = CREATEOBJECT('clsFilesInUse')

* put file info into cursor
loFilesInUse.FindFilesInUse(lcServer, lcDir, lcUser)

SELECT curFilesInUse

BROWSE

USE IN curFilesInUse


