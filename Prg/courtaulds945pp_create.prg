*!* COURTAULDS 945 PickPack (Whse. Shipping Advice) Creation Program
*!* Creation Date: 11.18.2005 by Joe

PARAMETERS nWO_Num,cBOL,cShip_ref,cOffice,cTime,lJCP,lPick

PUBLIC ARRAY thisarray(1)
PUBLIC c_CntrlNum,c_GrpCntrlNum,cReturned,cWO_Num,cWO_NumStr,nWO_Num1,lTesting,lDoCatch,cISA_Num,nOrigSeq,nOrigGrpSeq,gOffice
PUBLIC lCourtMail,lDoCourtfilesOut,dDateTimeCal,nFilenum,tsendto,tcc,tsendtoerr,tccerr,lPSU,cProgname,nAcctNum,cErrMsg,cCourtMailName,tfrom,lOverflow
PUBLIC cMod,cMBOL,cFilenameShort,cFilenameOut,cFilenameArch,lParcelType,cWO_NumList,lcPath,lcArchivePath,lcHoldPath,cEDIType,cISA_Num,lTestMail

lTesting   = .F.  && Set to .t. for testing
lTestinput = .F.  &&  Set to .t. for test input files only!

DO m:\dev\prg\_setvars WITH lTesting
cProgname = "courtaulds945pp_create"
cCourtMailName = "Courtaulds"

WAIT WINDOW "At start of Courtaulds 945 process" NOWAIT

ON ESCAPE CANCEL
lIsError = .F.
lDoCatch = .T.
lCloseOutput = .T.
lPick = .T.
lPrepack = .F.
nFilenum = 0
lOverflow = .f.
cMBOL = ""
STORE "" TO cShip_ref,ccustname,cWO_NumStr,cWO_NumList,tattach
*WAIT "" TIMEOUT 10
WAIT WINDOW "At the start of COURTAULDS 945 STD. PICKPACK process..." TIMEOUT 2
*!* Note: For Wal-mart testing, be sure that there is a BOL and DEL_DATE filled in on the test OUTSHIP table,
*!* and that it matches the test data below

TRY
	lFederated = .F.
	nAcctNum = 4677
	cOffice = "M"
	gOffice = cOffice
	cMod = cOffice
	cEDIType = "945"
	lParcelType = .F.

	cPPName = "Courtaulds"
	tfrom = "TGF EDI Operations <toll-edi-ops@tollgroup.com>"
	tmessage=""
	tcc = ""
	lUPS = .F.
	lPSU = .F.
	lDoBOLGEN = .F.
	lSamples = .F.
	cBOL1 = ""
	lISAFlag = .T.
	lSTFlag = .T.
	nLoadid = 1
	lSQLMail = .F.
	cErrMsg = ""

	IF lTesting OR lTestinput && AND DATE()<>{^2006-10-20}
		CLOSE DATABASES ALL
		DO m:\dev\prg\lookups
		IF !USED('edi_trigger')
			cEDIFolder = "F:\3PL\DATA\"
			USE (cEDIFolder+"edi_trigger") IN 0 ALIAS edi_trigger
		ENDIF
*!* TEST DATA AREA
		cBOL = "04907304677382660"
		cTime = DATETIME()
		lFedEx = .F.
	ENDIF
	CREATE CURSOR tempcourt945 (accountid i,isa_num c(10),st_num c(10),wo_num i,bol_no c(20),ship_ref c(20),filename c(50),filedate T)
	cBOL=TRIM(cBOL)

	IF lTesting
		SET STEP ON
	ENDIF

	SELECT 0
	USE F:\3pl\DATA\mailmaster ALIAS mm
	LOCATE FOR mm.edi_type = "945" AND mm.office= cOffice AND mm.accountid = nAcctNum
	tsendto = IIF(mm.use_alt,mm.sendtoalt,mm.sendto)
	tcc = IIF(mm.use_alt,mm.ccalt,mm.cc)
	lcPath = ALLTRIM(mm.basepath)
	lcArchivePath = ALLTRIM(mm.archpath)
	lcHoldPath = ALLTRIM(mm.holdpath)
	LOCATE
	LOCATE FOR mm.edi_type = 'MISC' AND mm.taskname = 'GENERAL'
	tsendtoerr = IIF(mm.use_alt,mm.sendtoalt,mm.sendto)
	tccerr = IIF(mm.use_alt,mm.ccalt,mm.cc)
	tsendtotest = IIF(mm.use_alt,mm.sendtoalt,mm.sendto)
	tcctest = IIF(mm.use_alt,mm.ccalt,mm.cc)
	USE IN mm

	cRetMsg = "X"
	IF USED('outship')
		USE IN outship
	ENDIF

	csq1 = [select * from outship where accountid = ]+TRANSFORM(nAcctNum)+[ and office = ']+cOffice+[' and bol_no = ']+cBOL+[']
	xsqlexec(csq1,,,"wh")
	IF RECCOUNT() = 0
		cErrMsg = "MISS BOL "+cBOL
		DO ediupdate WITH cErrMsg,.T.
		THROW
	ENDIF
	DELETE FOR EMPTY(del_date) OR ISNULL(del_date)
	INDEX ON bol_no TAG bol_no
	INDEX ON wo_num TAG wo_num
	LOCATE

	SELECT outship
	IF !SEEK(cBOL,'outship','bol_no')
		WAIT WINDOW "BOL# "+cBOL+" not found in OUTSHIP!"
		cErrMsg = "BOL NOT IN OUTSHIP"
		DO ediupdate WITH cErrMsg,.T.
		THROW
	ELSE
		nWO_Num = outship.wo_num
		cShip_ref = ALLTRIM(outship.ship_ref)
	ENDIF

	IF !lTesting AND !lTestinput
		DO m:\dev\prg\sqldata-COMPARE WITH "",cBOL,nWO_Num,cOffice,nAcctNum,cPPName

		IF cRetMsg<>"OK"
			SET STEP ON
			lCloseOutput = .T.
			cWO_Num = ALLTRIM(STR(nWO_Num1))
			cErrMsg = "SQL ERROR"
			DO ediupdate WITH cErrMsg,.T.
			THROW
		ENDIF
	ENDIF

	IF LEFT(cBOL,2) = "1Z" OR LEFT(cBOL,3)="PSU"
		lUPS = .T.
	ENDIF

	cWO_Num = ALLTRIM(STR(nWO_Num))

	lCourtMail = .T.
	lTestMail = lTesting && Sends mail to Joe only
	lDoCourtfilesOut = .T. && If true, copies output to FTP folder (default = .t.)
	lStandalone = lTesting

*lTestMail = .t.
	STORE "LB" TO cWeightUnit

	WAIT WINDOW "Now preparing tables and variables" NOWAIT NOCLEAR

*!* SET CUSTOMER CONSTANTS
	ccustname = "COURTAULDS"  && Customer Identifier
	cX12 = "004050"  && X12 Standards Set used
*SET STEP ON

*!* SET OTHER CONSTANTS
	DO CASE
	CASE cOffice = "M"
		cCustLoc =  "FL"
		cfmiWarehouse = ""
		cCustPrefix = "945"+"f"
		cDivision = "Florida"
		cFolder = "WHM"
		cSF_Addr1  = "11400 NW 32ND AVE"
		cSF_CSZ    = "MIAMI+FL+33167"

	CASE cOffice = "N"
		cCustLoc =  "NJ"
		cfmiWarehouse = ""
		cCustPrefix = "945"+"j"
		cDivision = "New Jersey"
		cFolder = "WHN"
		cSF_Addr1  = "800 FEDERAL BLVD"
		cSF_CSZ    =  "CARTERET+NJ+07008"

	OTHERWISE
		cCustLoc =  "CA"
		cfmiWarehouse = ""
		cCustPrefix = "945"+"c"
		cDivision = "California"
		cFolder = "WHC"
		cSF_Addr1  = "450 WESTMONT DRIVE"
		cSF_CSZ    = "SAN PEDRO+CA+90731"
	ENDCASE
	cCustFolder = UPPER(ccustname)
	lNonEDI = .F.  && Assumes not a Non-EDI 945
	STORE "" TO lcKey
	STORE 0 TO alength,nLength
	cDelTime = SUBSTR(TTOC(cTime,1),9,4)
	cCarrierType = "M" && Default as "Motor Freight"
	dapptnum = ""

*!* SET OTHER CONSTANTS
	IF INLIST(cOffice,'C','1','2')
		dDateTimeCal = (DATETIME()-(3*3600))
	ELSE
		dDateTimeCal = DATETIME()
	ENDIF
	dt1 = TTOC(dDateTimeCal,1)
	dtmail = TTOC(dDateTimeCal)
	dt2 = dDateTimeCal
	cString = ""

	csendqual = "ZZ"
	csendid = "FMIX"
	crecqual = "14"
	crecid = "5013546000747"

	cfd = "+"
	csegd = "'"
	nSegCtr = 0
	cterminator = ">" && Used at end of ISA segment
	cdate = DTOS(DATE())
	cTruncDate = RIGHT(cdate,6)
	cTruncTime = SUBSTR(TTOC(dDateTimeCal),9,4)
	cfiledate = cdate+cTruncTime
	cOrig = "J"
	nSTCount = 0
	cStyle = ""
	cPTString = ""
	nTotCtnWt = 0
	nTotCtnCount = 0
	nUnitSum = 0
	cTargetStyle = ""

** PAD ID Codes
	csendidlong = PADR(csendid,15," ")
	crecidlong = PADR(crecid,15," ")

	lcPath = IIF(lTesting,("F:\FTPUSERS\Courtaulds\945OUT\test\"),lcPath)
	cFilenameHold = (lcHoldPath+cCustPrefix+dt1+".txt")
	cFilenameShort = JUSTFNAME(cFilenameHold)
	cFilenameOut = (lcPath+cFilenameShort)
	cFilenameArch = (lcArchivePath+cFilenameShort)
	nFilenum = FCREATE(cFilenameHold)

	IF USED('OUTWOLOG')
		USE IN outwolog
	ENDIF

	IF USED('shipment')
		USE IN shipment
	ENDIF

	IF USED('PACKAGE')
		USE IN package
	ENDIF

	dDate = DATE()-5
	csq1 = [select * from shipment where right(rtrim(str(accountid)),4) = ]+TRANSFORM(nAcctNum)+[ and shipdate >= {]+DTOC(dDate)+[}]
	xsqlexec(csq1,,,"wh")
	INDEX ON shipmentid TAG shipmentid
	INDEX ON pickticket TAG pickticket

	SELECT shipment
	LOCATE
	IF RECCOUNT("shipment") > 0
		xjfilter="shipmentid in ("
		SCAN
			nShipmentId = shipment.shipmentid
			xsqlexec("select * from package where shipmentid="+TRANSFORM(nShipmentId),,,"wh")

			IF RECCOUNT("package")>0
				xjfilter=xjfilter+TRANSFORM(shipmentid)+","
			ELSE
				xjfilter="1=0"
			ENDIF
		ENDSCAN
		xjfilter=LEFT(xjfilter,LEN(xjfilter)-1)+")"

		xsqlexec("select * from package where "+xjfilter,,,"wh")
	ELSE
		xsqlexec("select * from package where .f.",,,"wh")
	ENDIF
	SELECT package
	INDEX ON shipmentid TAG shipmentid
	SET ORDER TO TAG shipmentid

	SELECT shipment
	LOCATE
	SET RELATION TO shipmentid INTO package

	IF SEEK(cBOL,'outship','bol_no')
		IF ("COURTAULDS"$outship.consignee) OR ("STEINBEST"$outship.consignee) OR ("WRIGHT"$UPPER(outship.consignee))
			lSamples = .T.
		ENDIF

		IF ("SYMS"$outship.consignee)
			lSYMS = .T.
		ELSE
			lSYMS = .F.
		ENDIF
	ELSE
		WAIT WINDOW "BOL not found in OUTSHIP" TIMEOUT 2
		cErrMsg = "BOL NOT FOUND"
		DO ediupdate WITH cErrMsg,.T.
		THROW
	ENDIF

	IF !lSYMS AND !lSamples
		SELECT dcnum FROM outship WHERE outship.bol_no = cBOL GROUP BY 1 INTO CURSOR tempx
		LOCATE
		IF EOF()
			cErrMsg = "EMPTY DC/STORE#"
			DO ediupdate WITH cErrMsg,.T.
			THROW
		ENDIF
		IF EMPTY(tempx.dcnum)
			USE IN tempx
			SELECT storenum FROM outship WHERE outship.bol_no = cBOL GROUP BY 1 INTO CURSOR tempx
			IF EOF()
				cErrMsg = "EMPTY DC/STORE#"
				DO ediupdate WITH cErrMsg,.T.
				THROW
			ENDIF

			IF EMPTY(tempx.storenum)
				cErrMsg = "EMPTY DC/STORE#"
				DO ediupdate WITH cErrMsg,.T.
				THROW
			ELSE
				IF RECCOUNT('tempx')  > 1 AND cBOL<>"04907314677854548"
					cErrMsg = "MULTI DC NUMS"
					DO ediupdate WITH cErrMsg,.T.
					THROW
				ENDIF
			ENDIF
		ENDIF
		IF USED('tempx')
			USE IN tempx
		ENDIF
	ENDIF

	IF USED('OUTDET')
		USE IN outdet
	ENDIF

	selectoutdet()
	SELECT outdet
	INDEX ON outdetid TAG outdetid

&& Added code blocks to the following to overcome BOL vs. WO selection issues, 12.23.2005
	SELECT outship
	LOCATE
	IF USED("sqlwocourt")
		USE IN sqlwocourt
	ENDIF
	DELETE FILE "F:\3pl\DATA\sqlwocourt.dbf"
	SELECT bol_no,wo_num ;
		FROM outship WHERE bol_no = cBOL ;
		GROUP BY 1,2 ;
		ORDER BY 1,2 ;
		INTO DBF F:\3pl\DATA\sqlwocourt
	USE IN sqlwocourt
	USE F:\3pl\DATA\sqlwocourt IN 0 ALIAS sqlwo

	cRetMsg = ""

	DO m:\dev\prg\sqlconnect_bol  WITH nAcctNum,ccustname,cPPName,.T.,cOffice

	IF cRetMsg<>"OK"
		cErrMsg = cRetMsg
		DO ediupdate WITH cErrMsg,.T.
		THROW
	ENDIF

	IF lTesting OR lTestinput
		SELECT vcourtauldspp
		LOCATE
*		BROWSE FOR ship_ref = '498884-002852'
	ENDIF

*	ASSERT .F. MESSAGE "Run ID Check"
	SELECT wo_num FROM vcourtauldspp GROUP BY 1 INTO CURSOR temprunid1
	SELECT runid FROM vcourtauldspp GROUP BY 1 INTO CURSOR temprunid2
	SELECT temprunid1
	STORE RECCOUNT() TO nWORecs
	SELECT temprunid2
	STORE RECCOUNT() TO nRIDRecs

	IF nWORecs#nRIDRecs
		cErrMsg = "MULTI RUNIDS"
		DO ediupdate WITH cErrMsg,.T.
		THROW
	ENDIF
	USE IN temprunid1
	USE IN temprunid2

	IF !USED("scacs")
		xsqlexec("select * from scac","scacs",,"wh")
		INDEX ON scac TAG scac
	ENDIF

	SELECT outship
	LOCATE
	WAIT CLEAR
	WAIT WINDOW "Now creating Header information" NOWAIT NOCLEAR

*!* HEADER LEVEL EDI DATA
	DO num_incr_isa

	cISA_Num = PADL(c_CntrlNum,9,"0")
	nISA_Num = INT(VAL(cISA_Num))

	IF lTesting
		cISACode = "T"
	ELSE
		cISACode = "P"
	ENDIF

	STORE "ISA"+cfd+"00"+cfd+"          "+cfd+"00"+cfd+"          "+cfd+csendqual+cfd+csendidlong+cfd+;
		crecqual+cfd+crecidlong+cfd+cTruncDate+cfd+cTruncTime+cfd+"U"+cfd+"00401"+cfd+;
		cISA_Num+cfd+"0"+cfd+cISACode+cfd+cterminator+csegd TO cString
	DO cstringbreak

	STORE "GS"+cfd+"SW"+cfd+csendid+cfd+crecid+cfd+cdate+cfd+cTruncTime+cfd+c_CntrlNum+;
		cfd+"X"+cfd+cX12+csegd TO cString
	DO cstringbreak

	SELECT outship
*	SET ORDER TO TAG wo_num
	LOCATE

	IF !SEEK(nWO_Num)
		WAIT CLEAR
		WAIT WINDOW "Invalid Work Order Number - Not Found in OUTSHIP!" TIMEOUT 2
		cErrMsg = "WO NOT FOUND"
		DO ediupdate WITH cErrMsg,.T.
		THROW
	ENDIF

	IF !lTesting
		IF EMPTY(cBOL)  && May be empty if triggered from PT screen, or FedEx shipment
			WAIT CLEAR
			WAIT WINDOW "MISSING Bill of Lading Number!" TIMEOUT 2
			cErrMsg = "BOL# EMPTY"
			DO ediupdate WITH cErrMsg,.T.
			THROW
		ELSE
			IF !SEEK(PADR(TRIM(cBOL),20),"outship","bol_no")
				WAIT CLEAR
				WAIT WINDOW "Invalid Bill of Lading Number - Not Found!" TIMEOUT 2
				cErrMsg = "BOL# NOT FOUND"
				DO ediupdate WITH cErrMsg,.T.
				THROW
			ENDIF
		ENDIF
	ENDIF

*************************************************************************
*1	PTIDSOURCE/OUTSHIP RECORD MAIN LOOP
*	Loops through individual OUTSHIP lines for BOL#
*************************************************************************
	WAIT CLEAR
*!*		IF lUPS
*!*			WAIT WINDOW "Now creating WO#-based information..." NOWAIT NOCLEAR
*!*		ELSE
	WAIT WINDOW "Now creating BOL#-based information..." NOWAIT NOCLEAR

	SELECT outship
	SET ORDER TO
	COUNT TO N FOR outship.bol_no = cBOL AND !EMPTY(del_date)
	IF N=0
		cErrMsg = "INCOMPL. BOL: Del_Date"
		DO ediupdate WITH cErrMsg,.T.
		THROW
	ENDIF

	LOCATE
	cMissDel = ""

	oscanstr = "outship.bol_no = cBOL AND !EMPTY(outship.del_date) and outship.qty>0"
	SELECT outship

	SCAN FOR &oscanstr
		SELECT outship
		SCATTER MEMVAR MEMO
		lMacy = IIF("MACY"$m.consignee,.T.,.F.)
		lD2S = IIF("D2S"$m.consignee,.T.,.F.)
		cWO_Num = ALLTRIM(STR(m.wo_num))
		IF !(cWO_Num$cWO_NumStr)
			cWO_NumStr = IIF(EMPTY(cWO_NumStr),cWO_Num,cWO_NumStr+","+cWO_Num)
		ENDIF
		IF !(cWO_Num$cWO_NumList)
			cWO_NumList = IIF(EMPTY(cWO_NumList),cWO_Num,cWO_NumList+CHR(13)+cWO_Num)
		ENDIF
		IF !lTestinput
			csq1 = [select * from outwolog where accountid = ]+TRANSFORM(nAcctNum)+[ and wo_num = ]+TRANSFORM(outship.wo_num)
			xsqlexec(csq1,,,"wh")
			IF RECCOUNT() = 0
				cErrMsg = "MISS OUTWOLOG REC FOR WO# "+cWO_Num
				DO ediupdate WITH cErrMsg,.T.
				THROW
			ENDIF
			SELECT outwolog
			INDEX ON wo_num TAG wo_num

			IF SEEK(m.wo_num,"outwolog","wo_num")
				lPick = IIF(outwolog.picknpack,.T.,.F.)
				lPrepack = IIF(lPick,.F.,.T.)
			ENDIF
			USE IN outwolog
		ENDIF

		nTotCtnCount = m.qty
		cPO_Num = ALLTRIM(m.cnee_ref)
		cShip_ref = ALLTRIM(m.ship_ref)

		IF LEFT(cBOL,3)="PSU" AND "MACY"$outship.consignee
			lPSU = .T.
		ENDIF

*!* Added this code to trap miscounts in OUTDET Units
		IF lPrepack
			SELECT outdet
			SET ORDER TO
			SUM totqty TO nUnitTot1 FOR  units AND outdet.outshipid = outship.outshipid
			SUM (VAL(PACK)*totqty) TO nUnitTot2 FOR !units AND outdet.outshipid = outship.outshipid
			IF nUnitTot1<>nUnitTot2
				WAIT WINDOW "TOTQTY ERROR AT PT "+cShip_ref TIMEOUT 5
				cErrMsg = "OUTDET TOTQTY ERR"
				DO ediupdate WITH cErrMsg,.T.
				THROW
			ENDIF
			SELECT vcourtauldspp
			SUM totqty TO nUnitTot2 FOR vcourtauldspp.outshipid = outship.outshipid
			IF nUnitTot1<>nUnitTot2
				cErrMsg = "SQL TOTQTY ERR"
				DO ediupdate WITH cErrMsg,.T.
				THROW
			ENDIF
		ENDIF
		SELECT outdet
		SET ORDER TO outdetid
		LOCATE

*!* End code addition

		IF (("PENNEY"$UPPER(outship.consignee)) OR ("KMART"$UPPER(outship.consignee)) OR ("K-MART"$UPPER(outship.consignee)))
			lApptFlag = .T.
		ELSE
			lApptFlag = .F.
		ENDIF
		IF lTestinput
			ddel_date = DATE()
			dapptnum = "99999"
			dapptdate = DATE()
		ELSE
			ddel_date = outship.del_date
			IF EMPTY(ddel_date)
				cMissDel = IIF(EMPTY(cMissDel),"The following PTs had no Delivery Dates:"+CHR(13)+TRIM(cShip_ref),cMissDel+CHR(13)+TRIM(cShip_ref))
			ENDIF
			dapptnum = ALLTRIM(outship.appt_num)

			IF !lSYMS
				IF EMPTY(dapptnum) && Penney/KMart Appt Number check
					IF !lApptFlag OR lUPS
						dapptnum = ""
					ELSE
						cErrMsg = "EMPTY APPT#, WO# "+cWO_Num
						DO ediupdate WITH cErrMsg,.T.
						THROW
					ENDIF
				ENDIF
			ELSE
				dapptnum = "99999"
			ENDIF
			dapptdate = outship.appt

			IF EMPTY(dapptdate)
				dapptdate = outship.del_date
			ENDIF
		ENDIF

		IF !lTesting
			IF lUPS
				cBOL = TRIM(outship.bol_no)
			ELSE
				nWO_Num = outship.wo_num
				cWO_Num = ALLTRIM(STR(nWO_Num))
			ENDIF
		ENDIF

		IF ALLTRIM(outship.SForCSZ) = ","
			BLANK FIELDS outship.SForCSZ NEXT 1 IN outship
		ENDIF

		alength = ALINES(apt,outship.shipins,.T.,CHR(13))

		cTRNum = ""
		cPRONum = ""

		IF (("WALMART"$outship.consignee) OR ("WAL-MART"$outship.consignee) OR ("WAL MART"$outship.consignee))
			IF LEFT(outship.keyrec,2) = "PR"
				STORE TRIM(keyrec) TO cPRONum
			ENDIF
			IF LEFT(outship.keyrec,2) = "TR"
				STORE TRIM(keyrec) TO cTRNum
			ENDIF
		ENDIF

		IF cBOL = "99911111111111111"
			cShip_ref = ALLTRIM(STRTRAN(cShip_ref,"OVER",""))
		ENDIF

		nOutshipid = m.outshipid
*!*			IF lUPS
*!*				cPTString = IIF(EMPTY(cPTString),m.consignee+" "+cBOL+" "+cShip_ref,cPTString+CHR(13)+m.consignee+" "+cBOL+" "+cShip_ref)
*!*			ELSE
		cPTString = IIF(EMPTY(cPTString),m.consignee+" "+cShip_ref,cPTString+CHR(13)+m.consignee+" "+cShip_ref)
*!*			ENDIF
		m.CSZ = TRIM(m.CSZ)

		IF nWO_Num <> 258419
			IF EMPTY(M.CSZ)
				WAIT CLEAR
				WAIT WINDOW "No SHIP-TO City/State/ZIP info...exiting" TIMEOUT 2
				cErrMsg = "BAD ADDRESS INFO"
				DO ediupdate WITH cErrMsg,.T.
				THROW
			ENDIF
			m.CSZ = ALLTRIM(STRTRAN(m.CSZ,"  "," "))
			IF !","$m.CSZ
				SET STEP ON
				cErrMsg = "NO COMMA CSZ: "+cShip_ref
				DO ediupdate WITH cErrMsg,.T.
				THROW
			ENDIF
			nSpaces = OCCURS(" ",ALLTRIM(m.CSZ))
			IF nSpaces = 0
				WAIT WINDOW "Ship-to CSZ Segment Error" TIMEOUT 3
				cErrMsg = "SHIP-TO CSZ: "+cShip_ref
				DO ediupdate WITH cErrMsg,.T.
				THROW
			ELSE
				nCommaPos = AT(",",m.CSZ)
				nLastSpace = AT(" ",m.CSZ,nSpaces)
				IF ISALPHA(SUBSTR(TRIM(m.CSZ),AT(" ",m.CSZ,nSpaces-1)+1,2))
					cCity = LEFT(TRIM(m.CSZ),AT(",",m.CSZ)-1)
					cState = SUBSTR(m.CSZ,nCommaPos+2,2)
					cZip = TRIM(SUBSTR(m.CSZ,nLastSpace+1))
				ELSE
					WAIT CLEAR
					WAIT WINDOW "NOT ALPHA: "+SUBSTR(TRIM(m.CSZ),AT(" ",m.CSZ,nSpaces-1)+1,2) TIMEOUT 3
					cCity = LEFT(TRIM(m.CSZ),AT(",",m.CSZ)-1)
					cState = SUBSTR(TRIM(m.CSZ),AT(" ",m.CSZ,nSpaces-2)+1,2)
					cZip = TRIM(SUBSTR(m.CSZ,nLastSpace+1))
				ENDIF
			ENDIF

			STORE "" TO cSForCity,cSForState,cSForZip
			IF !lFederated
				IF EMPTY(M.SForCSZ)
				ELSE
					m.SForCSZ = ALLTRIM(STRTRAN(m.SForCSZ,"  "," "))
					nSpaces = OCCURS(" ",ALLTRIM(m.SForCSZ))
					IF nSpaces = 0
						m.SForCSZ = STRTRAN(m.SForCSZ,",","")
						cSForCity = ALLTRIM(m.SForCSZ)
						cSForState = ""
						cSForZip = ""
					ELSE
						nCommaPos = AT(",",m.SForCSZ)
						nLastSpace = AT(" ",m.SForCSZ,nSpaces)
						nMinusSpaces = IIF(nSpaces=1,0,1)
						IF ISALPHA(SUBSTR(TRIM(m.SForCSZ),AT(" ",TRIM(m.SForCSZ),nSpaces-nMinusSpaces)+1,2))
							cSForCity = LEFT(TRIM(m.SForCSZ),AT(",",m.SForCSZ)-1)
							cSForState = SUBSTR(m.SForCSZ,nCommaPos+2,2)
							cSForZip = TRIM(RIGHT(TRIM(m.SForCSZ),5))
							IF ISALPHA(cSForZip)
								cSForZip = ""
							ENDIF
						ELSE
							WAIT CLEAR
							WAIT WINDOW "NOT ALPHA: "+SUBSTR(TRIM(m.SForCSZ),AT(" ",m.SForCSZ,nSpaces-1)+1,2) TIMEOUT 3
							cSForCity = LEFT(TRIM(m.SForCSZ),AT(",",m.SForCSZ)-1)
							cSForState = SUBSTR(TRIM(m.SForCSZ),AT(" ",m.SForCSZ,nSpaces-2)+1,2)
							cSForZip = TRIM(RIGHT(TRIM(m.SForCSZ),5))
							IF ISALPHA(cSForZip)
								cSForZip = ""
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ELSE
				cStoreName = segmentget(@apt,"STORENAME",alength)
			ENDIF
		ELSE
			STORE "" TO cCity,cState,cZip
		ENDIF

		DO num_incr_st
		WAIT CLEAR
		WAIT WINDOW "Now creating Line Item information" NOWAIT NOCLEAR

		INSERT INTO tempcourt945 (accountid,isa_num,st_num,wo_num,bol_no,ship_ref,filename,filedate) ;
			VALUES (m.accountid,c_CntrlNum,c_GrpCntrlNum,m.wo_num,m.bol_no,m.ship_ref,cFilenameShort,dt2)

		STORE "ST"+cfd+"945"+cfd+PADL(c_GrpCntrlNum,9,"0")+csegd TO cString
		DO cstringbreak
		nSTCount = nSTCount + 1
		nSegCtr = 1


		IF lJCP AND lUPS
			STORE "W06"+cfd+"N"+cfd+cShip_ref+cfd+cdate+cfd+"0"+cfd+TRIM(cWO_Num)+cfd+cPO_Num+csegd TO cString
		ELSE
			STORE "W06"+cfd+"N"+cfd+cShip_ref+cfd+cdate+cfd+TRIM(cBOL)+cfd+TRIM(cWO_Num)+cfd+cPO_Num+csegd TO cString
		ENDIF
		DO cstringbreak
		nSegCtr = nSegCtr + 1
		nCtnNumber = 1  && Seed carton sequence count
*	nLineNum = 1

		IF !lSYMS AND !lSamples
			cStoreNum = ALLTRIM(m.dcnum)
			IF EMPTY(TRIM(cStoreNum))
				IF (!EMPTY(m.storenum) AND m.storenum<>0)
					IF LEN(ALLTRIM(STR(m.storenum))) < 5
						cStoreNum = PADL(ALLTRIM(STR(m.storenum)),4,"0")
					ELSE
						cStoreNum = ALLTRIM(STR(m.storenum))
					ENDIF
				ENDIF
				IF EMPTY(cStoreNum)
					cStoreNum = segmentget(@apt,"STORENUM",alength)
					IF EMPTY(cStoreNum)
						cErrMsg = "MISS DCNUM"
						DO ediupdate WITH cErrMsg,.T.
						THROW
					ENDIF
				ENDIF
			ENDIF
		ELSE
			cStoreNum = ""
		ENDIF

		STORE "N1"+cfd+"SF"+cfd+"TGF INC."+cfd+"91"+cfd+"01"+csegd TO cString
		DO cstringbreak
		nSegCtr = nSegCtr + 1

		IF !lFederated
			STORE "N3"+cfd+TRIM(cSF_Addr1)+csegd TO cString
			DO cstringbreak
			nSegCtr = nSegCtr + 1

			STORE "N4"+cfd+cSF_CSZ+cfd+"USA"+csegd TO cString
			DO cstringbreak
			nSegCtr = nSegCtr + 1
		ENDIF

		IF lSYMS OR lSamples
			STORE "N1"+cfd+"ST"+cfd+ALLTRIM(m.consignee)+cfd+"92"+csegd TO cString
		ELSE
			STORE "N1"+cfd+"ST"+cfd+ALLTRIM(m.consignee)+cfd+"92"+cfd+cStoreNum+csegd TO cString
		ENDIF
		DO cstringbreak
		nSegCtr = nSegCtr + 1

		STORE "N3"+cfd+TRIM(outship.address)+csegd TO cString
		DO cstringbreak
		nSegCtr = nSegCtr + 1

		STORE "N4"+cfd+cCity+cfd+cState+cfd+cZip+cfd+"USA"+csegd TO cString
		DO cstringbreak
		nSegCtr = nSegCtr + 1

		IF !EMPTY(m.sforstore)
			STORE "N1"+cfd+"BY"+cfd+ALLTRIM(m.shipfor)+cfd+"91"+cfd+TRIM(m.sforstore)+csegd TO cString
			DO cstringbreak
			nSegCtr = nSegCtr + 1

			STORE "N3"+cfd+TRIM(m.sforaddr1)+csegd TO cString
			DO cstringbreak
			nSegCtr = nSegCtr + 1

			IF !EMPTY(cSForState)
				STORE "N4"+cfd+cSForCity+cfd+cSForState+cfd+cSForZip+cfd+"USA"+csegd TO cString
			ELSE
				STORE "N4"+cfd+cSForCity+csegd TO cString
			ENDIF
			DO cstringbreak
			nSegCtr = nSegCtr + 1
		ENDIF

		STORE "N9"+cfd+"MN"+cfd+TRIM(cWO_Num)+csegd TO cString
		DO cstringbreak
		nSegCtr = nSegCtr + 1

		STORE "N9"+cfd+"DP"+cfd+TRIM(outship.dept)+csegd TO cString
		DO cstringbreak
		nSegCtr = nSegCtr + 1

		STORE "N9"+cfd+"PUA"+cfd+dapptnum+csegd TO cString
		DO cstringbreak
		nSegCtr = nSegCtr + 1

		IF !EMPTY(cPRONum)
			STORE "N9"+cfd+"RE"+cfd+cPRONum+csegd TO cString
			DO cstringbreak
			nSegCtr = nSegCtr + 1
		ENDIF

*!* For JCPenney orders only
		IF cacctnum = "JCP" OR ("PENNEY"$UPPER(m.consignee)) AND !lUPS
			IF lTesting AND EMPTY(m.batch_num)
				cLoadID = PADR(ALLTRIM(STR(nLoadid)),6,"0")
				nLoadid = nLoadid + 1
			ELSE
				cLoadID = ALLTRIM(m.batch_num)
				IF EMPTY(cLoadID)
					cLoadID = ALLTRIM(m.appt_num)
					IF EMPTY(cLoadID)
						cErrMsg = "MISS LOADID"
						DO ediupdate WITH cErrMsg,.T.
						THROW
					ENDIF
				ENDIF
			ENDIF
			STORE "N9"+cfd+"P8"+cfd+TRIM(cLoadID)+csegd TO cString
			DO cstringbreak
			nSegCtr = nSegCtr + 1
		ENDIF

		STORE "G62"+cfd+"11"+cfd+TRIM(DTOS(ddel_date))+cfd+"D"+cfd+cDelTime+csegd TO cString  && Ship date/time
		DO cstringbreak
		nSegCtr = nSegCtr + 1

		SELECT scacs
		IF lTesting AND (EMPTY(m.scac) OR EMPTY(m.ship_via))
			STORE "TEST" TO m.scac
			STORE "TEST SHIPPER" TO m.ship_via
		ELSE
			IF !EMPTY(TRIM(outship.scac))
				STORE ALLT(outship.scac) TO m.scac
				lFedEx = .F.
				SELECT scacs
				IF SEEK(m.scac,"scacs","scac")
					IF ("FEDERAL EXPRESS"$NAME) OR ("FEDEX"$NAME)
						lFedEx = .T.
					ENDIF
					SELECT outship

					IF lUPS OR lFedEx
						cCarrierType = "U" && Parcel as UPS or FedEx
					ENDIF
				ELSE
					WAIT CLEAR
					cErrMsg = "MISSING SCAC"
					DO ediupdate WITH cErrMsg,.T.
					THROW
				ENDIF
			ENDIF
		ENDIF

		SELECT outship
		IF !lUPS
			IF USED("parcel_carriers")
			USE IN parcel_carriers
			endif
			USE F:\3pl\DATA\parcel_carriers IN 0 ALIAS parcel_carriers ORDER TAG scac
			IF SEEK(m.scac,"parcel_carriers","scac")
				IF parcel_carriers.FedEx = .T.
					cCarrierType = "U"
				ENDIF
			ENDIF
			USE IN parcel_carriers
		ELSE
			IF lD2S
				m.scac = "UPSL"
				m.ship_via = "UPS COLLECT GROUND"
			ENDIF
		ENDIF

		IF !EMPTY(cTRNum)
			STORE "W27"+cfd+cCarrierType+cfd+TRIM(m.scac)+cfd+TRIM(m.ship_via)+REPLICATE(cfd,2)+"TL"+;
				REPLICATE(cfd,2)+cTRNum+csegd TO cString
		ELSE
			STORE "W27"+cfd+cCarrierType+cfd+TRIM(m.scac)+cfd+TRIM(m.ship_via)+csegd TO cString
		ENDIF
		DO cstringbreak
		nSegCtr = nSegCtr + 1

		IF lUPS
			SELECT shipment
			IF SEEK(cShip_ref,"shipment","pickticket")
				SELECT package
				SUM package.pkgcharge TO nFreight FOR package.shipmentid=shipment.shipmentid
				cFreight = STRTRAN(ALLTRIM(STR(nFreight,10,2)),".","")
			ENDIF
*!*				STORE "G72"+cfd+"516"+cfd+"15"+REPLICATE(cfd,6)+ALLT(cFreight)+csegd TO cString
*!*				DO cstringbreak
*!*				nSegCtr = nSegCtr + 1
		ENDIF

		IF lPSU
			ASSERT .F. MESSAGE "IN PSU tracking number select...DEBUG!"
			SELECT shipment
			IF SEEK(cShip_ref,"shipment","pickticket")
				SELECT trknumber ;
					FROM package ;
					WHERE package.shipmentid = shipment.shipmentid ;
					INTO CURSOR tracknum
				SELECT tracknum
				nTrk = RECCOUNT()
				LOCATE
			ELSE
				WAIT WINDOW "Pickticket "+cShip_ref+" does not exist in SHIPMENT" TIMEOUT 2
				cErrMsg = "MISS PT "+cShip_ref+" IN SHPMT"
				DO ediupdate WITH cErrMsg,.T.
				THROW
			ENDIF
			SELECT DISTINCT ucc ;
				FROM vcourtauldspp ;
				WHERE vcourtauldspp.ship_ref = cShip_ref ;
				INTO CURSOR tempv
			SELECT tempv
			nVcnt = RECCOUNT()
			USE IN tempv
			IF nVcnt # nTrk
				cErrMsg = "SQL>UPS CTN COUNT OFF"
				DO ediupdate WITH cErrMsg,.T.
				THROW
			ENDIF
			RELEASE vcnt
		ENDIF


*************************************************************************
*2	DETAIL LOOP - MATCHING SHIP_REF FIELD (FROM OUTSHIP)
*************************************************************************
		WAIT CLEAR
		WAIT WINDOW "Now creating Detail information "+cShip_ref NOWAIT NOCLEAR
		SELECT vcourtauldspp
		IF nWO_Num = 230001
*		ASSERT .f.
			INDEX ON outdetid TAG outdetid
			SELECT outdet
			SET ORDER TO
			SET RELATION TO outdetid INTO vcourtauldspp
		ELSE
			SELECT outdet
			SET ORDER TO TAG outdetid
			SELECT vcourtauldspp
			SET RELATION TO outdetid INTO outdet
		ENDIF


		LOCATE
		LOCATE FOR ship_ref = TRIM(cShip_ref) AND outshipid = nOutshipid
		IF !FOUND()
			IF !lTesting
				WAIT WINDOW "PT "+cShip_ref+" NOT FOUND in vcourtauldspp...ABORTING" TIMEOUT 2
				IF !lTesting
					lSQLMail = .T.
				ENDIF
				cErrMsg = "MISS PT-SQL: "+cShip_ref
				DO ediupdate WITH cErrMsg,.T.
				THROW
			ELSE
				LOOP
			ENDIF
		ENDIF

*ASSERT .F. MESSAGE "At outdet line scan"
		scanstr = "vcourtauldspp.ship_ref = cShip_ref and vcourtauldspp.outshipid = nOutshipid"

		SELECT vcourtauldspp
		LOCATE
		LOCATE FOR &scanstr

		cCartonNum= "XXX"
		lDoPALSegment = .T.
		lDoManSegment = .T.
		DO WHILE &scanstr

			alength = ALINES(apt,outdet.printstuff,.T.,CHR(13))
			lSkipBack = .T.

			IF vcourtauldspp.totqty = 0
				SKIP 1 IN vcourtauldspp
				LOOP
			ENDIF

			IF TRIM(vcourtauldspp.ucc) <> cCartonNum
				STORE TRIM(vcourtauldspp.ucc) TO cCartonNum
			ENDIF

			STORE "LX"+cfd+ALLTRIM(STR(nCtnNumber))+csegd TO cString   && Carton Seq. # (up to Carton total)
			DO cstringbreak
			nSegCtr = nSegCtr + 1

			DO WHILE vcourtauldspp.ucc  = cCartonNum
*				ASSERT .f. MESSAGE "In carton number scan"
				IF vcourtauldspp.totqty = 0
					SKIP 1 IN vcourtauldspp
					LOOP
				ENDIF

				cUCCNumber = vcourtauldspp.ucc
				IF EMPTY(cUCCNumber) OR ISNULL(cUCCNumber)
					WAIT CLEAR
					WAIT WINDOW "Empty UCC Number in vcourtauldspp "+cShip_ref TIMEOUT 2
					lSQLMail = .T.
					cErrMsg = "EMPTY UCC#, PT "+cShip_ref
					DO ediupdate WITH cErrMsg,.T.
					THROW
				ENDIF
				cUCCNumber = TRIM(STRTRAN(TRIM(cUCCNumber)," ",""))

				IF lDoManSegment
					lDoManSegment = .F.
					STORE "MAN"+cfd+"GM"+cfd+TRIM(cUCCNumber)+csegd TO cString  && SCC-14 Number (Wal-mart spec)
					DO cstringbreak
					nSegCtr = nSegCtr + 1
					nShipDetQty = outdet.totqty
*					nShipDetQty = INT(VAL(outdet.pack))
*					nUnitSum = nUnitSum + nShipDetQty

					IF (EMPTY(outdet.ctnwt) OR outdet.ctnwt = 0) AND outdet.totqty > 0
						IF lTestinput
							cCtnWt = "5"
						ELSE
							cCtnWt = ALLTRIM(STR(CEILING(INT(outship.weight/outship.ctnqty))))
						ENDIF
						IF EMPTY(cCtnWt) OR INT(VAL(cCtnWt)) = 0
							cErrMsg = "WT ERR IN PT: "+cShip_ref
							DO ediupdate WITH cErrMsg,.T.
							THROW
						ENDIF
						nTotCtnWt = nTotCtnWt + INT(VAL(cCtnWt))
					ELSE
						STORE ALLTRIM(STR(outdet.ctnwt)) TO cCtnWt
						nTotCtnWt = nTotCtnWt + outdet.ctnwt
					ENDIF
				ENDIF

				cColor = TRIM(outdet.COLOR)
				cSize = TRIM(outdet.ID)
				cUPC = TRIM(outdet.upc)

				IF (ISNULL(cUPC) OR EMPTY(cUPC))
					cUPC = TRIM(vcourtauldspp.upc)
				ENDIF

				cStyle = TRIM(outdet.STYLE)
				cItemNum = TRIM(outdet.custsku)
				cLineNum = TRIM(outdet.linenum)
				IF EMPTY(cLineNum)
					cErrMsg = "EMPTY LINE #(s)"
					DO ediupdate WITH cErrMsg,.T.
					THROW
				ENDIF

				nShipDetQty = vcourtauldspp.totqty
				IF ISNULL(nShipDetQty)
					nShipDetQty = outdet.totqty
				ENDIF
				nUnitSum = nUnitSum + nShipDetQty

				nOrigDetQty = vcourtauldspp.qty
				IF ISNULL(nOrigDetQty)
					nOrigDetQty = nShipDetQty
				ENDIF

				IF lDoPALSegment
					STORE "PAL"+REPLICATE(cfd,11)+cCtnWt+cfd+cWeightUnit+csegd TO cString
					DO cstringbreak
					nSegCtr = nSegCtr + 1

					IF lPSU AND lMacy
						SELECT tracknum
						cTrkNumber = ALLT(tracknum.trknumber)
						SKIP 1 IN tracknum
						STORE "N9"+cfd+"BM"+cfd+cTrkNumber+csegd TO cString
						DO cstringbreak
						nSegCtr = nSegCtr + 1
					ENDIF

					lDoPALSegment = .F.
				ENDIF

*!* Changed the following to utilize original 940 unit codes from Printstuff field
				cUnitCode = TRIM(segmentget(@apt,"UNITCODE",alength))
				IF EMPTY(cUnitCode)
					cUnitCode = "EA"
				ENDIF

				IF nOrigDetQty = nShipDetQty
					nShipStat = "CL"
				ELSE
					nShipStat = "PR"
				ENDIF

				STORE "W12"+cfd+nShipStat+cfd+ALLTRIM(STR(nOrigDetQty))+cfd+ALLTRIM(STR(nShipDetQty))+cfd+;
					ALLTRIM(STR(nOrigDetQty-nShipDetQty))+cfd+cUnitCode+cfd+cfd+"VA"+cfd+cStyle+csegd TO cString
				DO cstringbreak
				nSegCtr = nSegCtr + 1

				STORE "N9"+cfd+"BV"+cfd+cLineNum+csegd TO cString
				DO cstringbreak
				nSegCtr = nSegCtr + 1

				SKIP 1 IN vcourtauldspp
			ENDDO
			nCtnNumber = nCtnNumber+1
			lSkipBack = .T.
			lDoManSegment = .T.
			lDoPALSegment = .T.
		ENDDO

*************************************************************************
*2^	END DETAIL MATCHING SHIP_REF LOOP
*************************************************************************
		WAIT CLEAR
		WAIT WINDOW "Now creating Section Closure information" NOWAIT NOCLEAR
		STORE "W03"+cfd+ALLTRIM(STR(nUnitSum))+cfd+ALLTRIM(STR(nTotCtnWt))+cfd+;
			cWeightUnit+csegd TO cString   && Units sum, Weight sum, carton count

		nTotCtnWt = 0
		nTotCtnCount = 0
		nUnitSum = 0
		FPUTS(nFilenum,cString)

		STORE  "SE"+cfd+ALLTRIM(STR(nSegCtr+2))+cfd+PADL(c_GrpCntrlNum,9,"0")+csegd TO cString
		FPUTS(nFilenum,cString)

		SELECT outship
		WAIT CLEAR
	ENDSCAN


*************************************************************************
*1^	END OUTSHIP MAIN LOOP
*************************************************************************

	DO close945
	=FCLOSE(nFilenum)

	IF !("W06"$cString)
		cTestString = FILETOSTR(cFilenameHold)
		IF !("W06"$cTestString)  && Indicates an empty/junk file
			DELETE FILE &cFilenameHold
			SELECT edi_trigger
			cErrMsg = "FILE EMPTY"
			tsendtoempty = tsendtotest
			tccempty = tcctest
			tsubject = ccustname+" Empty 945 File at "+TTOC(DATETIME())
			tmessage = "File "+cFilenameHold+" was created empty and deleted before sending."+CHR(13)+"Check edi poller process if this message repeats."
			tattach  = ""
			tcc=""
			tfrom    = "TGF EDI 945 Poller Operations <toll-edi-ops@tollgroup.com>"
			DO FORM m:\dev\frm\dartmail2 WITH tsendtoempty,tfrom,tsubject,tccempty,tattach,tmessage,"A"

			lDoCatch = .F.
			THROW
		ENDIF
	ENDIF


	IF !lTesting
		SELECT edi_trigger
		DO ediupdate WITH "945 CREATED",.F.
		SELECT edi_trigger
		LOCATE

		IF !USED("ftpedilog")
			SELECT 0
			USE F:\edirouting\ftpedilog ALIAS ftpedilog
			INSERT INTO ftpedilog (TRANSFER,ftpdate,filename,acct_name,TYPE) VALUES ("945-"+ccustname+"-"+cCustLoc,dt2,cFilenameHold,UPPER(ccustname),"945")
			USE IN ftpedilog
		ENDIF
	ENDIF

	WAIT CLEAR
	WAIT WINDOW cCustFolder+" 945 Process complete..." TIMEOUT 1

*!* Create eMail confirmation message
	IF lTestMail
		tsendto = tsendtotest
		tcc = tcctest
	ENDIF

	tsubject = cCourtMailName+" 945 EDI File from TGF as of "+dtmail+" ("+cCustLoc+")"
	tattach = " "
	tmessage = "945 EDI Info from TGF, file "+cFilenameShort+CHR(13)
	tmessage = tmessage + "Division "+cDivision+", BOL# "+TRIM(cBOL)+CHR(13)
	tmessage = tmessage + "For Work Orders: "+cWO_NumStr+","+CHR(13)
	tmessage = tmessage + "containing these picktickets:"+CHR(13)
	tmessage = tmessage + cPTString + CHR(13)
	tmessage = tmessage +"has been created and will be transmitted ASAP."+CHR(13)+CHR(13)
	IF !EMPTY(cMissDel)
		tmessage = tmessage+CHR(13)+CHR(13)+cMissDel+CHR(13)+CHR(13)
	ENDIF
	tmessage = tmessage + "If you have any questions, please eMail or call 732-750-9000 x217."
	IF lCourtMail
		DO FORM m:\dev\frm\dartmail2 WITH tsendto,tfrom,tsubject,tcc,tattach,tmessage,"A"
	ENDIF

	cFilenameArchive = UPPER("f:\ftpusers\"+cCustFolder+"\945OUT\945ARCHIVE\"+cCustPrefix+dt1+".txt")

	RELEASE ALL LIKE c_CntrlNum,c_GrpCntrlNum
	WAIT CLEAR
	WAIT WINDOW cCourtMailName+" 945 EDI File output complete" TIMEOUT 1

*!* Transfers files to correct output folders
	ASSERT .F. MESSAGE "At FilesOut Statement...debug"

	COPY FILE [&cFilenameHold] TO [&cFilenameArch]
	IF lDoCourtfilesOut
		COPY FILE [&cFilenameHold] TO [&cFilenameOut]
		DELETE FILE [&cFilenameHold]
		SELECT tempcourt945
		COPY TO "f:\3pl\data\tempcourt945a.dbf"
		USE IN tempcourt945
		SELECT 0
		USE "f:\3pl\data\tempcourt945a.dbf" ALIAS tempcourt945a
		SELECT 0
		USE "f:\3pl\data\pts_sent945.dbf" ALIAS pts_sent945
		APPEND FROM "f:\3pl\data\tempcourt945a.dbf"
		USE IN pts_sent945
		USE IN tempcourt945a
		DELETE FILE "f:\3pl\data\tempcourt945a.dbf"
	ENDIF

	IF !lTesting
		*!* asn_out_data()()
	ENDIF

CATCH TO oErr
	IF lDoCatch
		SET STEP ON
		tsendto  = tsendtoerr
		tcc = tccerr
		IF EMPTY(cErrMsg)
			tsubject = cCourtMailName+" Error ("+TRANSFORM(oErr.ERRORNO)+") at "+TTOC(DATETIME())
			tattach  = ""
			tmessage = ccustname+" Error processing "+CHR(13)

			tmessage =tmessage+ "Try/CATCH Exeception Message:"+CHR(13)+;
				[  Error: ] + STR(oErr.ERRORNO) +CHR(13)+;
				[  LineNo: ] + STR(oErr.LINENO) +CHR(13)+;
				[  Message: ] + oErr.MESSAGE +CHR(13)+;
				[  Procedure: ] + oErr.PROCEDURE +CHR(13)+;
				[  Details: ] + oErr.DETAILS +CHR(13)+;
				[  StackLevel: ] + STR(oErr.STACKLEVEL) +CHR(13)+;
				[  LineContents: ] + oErr.LINECONTENTS+CHR(13)+;
				[  UserValue: ] + oErr.USERVALUE
			tmessage =tmessage+CHR(13)+cProgname
		ELSE
			tsubject = ccustname+" 945 Error at "+TTOC(DATETIME())
			tmessage = "The following ERROR occurred during processing:"+CHR(13)+cErrMsg
			IF "TOTQTY ERR"$cErrMsg
				tmessage = tmessage + CHR(13) + "At OUTSHIPID: "+ALLTRIM(STR(m.outshipid))
			ENDIF
			tattach  = ""
			tcc=""
			tfrom    = "TGF EDI 945 Poller Operations <toll-edi-ops@tollgroup.com>"
			DO FORM m:\dev\frm\dartmail2 WITH tsendto,tfrom,tsubject,tcc,tattach,tmessage,"A"
		ENDIF
	ENDIF

ENDTRY

*** END OF CODE BODY

****************************
PROCEDURE close945
****************************
** Footer Creation

STORE  "GE"+cfd+ALLTRIM(STR(nSTCount))+cfd+c_CntrlNum+csegd TO cString
FPUTS(nFilenum,cString)

STORE  "IEA"+cfd+"1"+cfd+PADL(c_CntrlNum,9,"0")+csegd TO cString
FPUTS(nFilenum,cString)

RETURN

****************************
PROCEDURE num_incr_isa
****************************

IF !USED("serfile")
	USE ("f:\3pl\data\serial\"+ccustname+"945_serial") IN 0 ALIAS serfile
ENDIF
SELECT serfile
IF lTesting AND lISAFlag
	nOrigSeq = serfile.seqnum
	lISAFlag = .F.
ENDIF
nISA_Num = serfile.seqnum
c_CntrlNum = ALLTRIM(STR(serfile.seqnum))
REPLACE serfile.seqnum WITH serfile.seqnum + 1 IN serfile
SELECT outship
RETURN

****************************
PROCEDURE num_incr_st
****************************

IF !USED("serfile")
	USE ("f:\3pl\data\serial\"+ccustname+""945_serial") IN 0 ALIAS serfile
ENDIF
SELECT serfile
IF lTesting AND lSTFlag
	nOrigGrpSeq = serfile.grpseqnum
	lSTFlag = .F.
ENDIF
c_GrpCntrlNum = ALLTRIM(STR(serfile.grpseqnum))
REPLACE serfile.grpseqnum WITH serfile.grpseqnum + 1 IN serfile
SELECT outship
RETURN


****************************
PROCEDURE segmentget
****************************

PARAMETER thisarray,lcKey,nLength

FOR i = 1 TO nLength
	IF i > nLength
		EXIT
	ENDIF
	lnEnd= AT("*",thisarray[i])
	IF lnEnd > 0
		lcThisKey =TRIM(SUBSTR(thisarray[i],1,lnEnd-1))
		IF OCCURS(lcKey,lcThisKey)>0
			RETURN SUBSTR(thisarray[i],lnEnd+1)
			i = 1
		ENDIF
	ENDIF
ENDFOR

RETURN ""

****************************
PROCEDURE ediupdate
****************************
PARAMETER cStatus,lIsError
lDoCatch = lIsError

IF !lTesting
	SELECT edi_trigger
	nRec = RECNO()
*		DO m:\dev\prg\edistatusmove WITH cBOL
	LOCATE
	IF !lIsError
		REPLACE edi_trigger.processed WITH .T.,edi_trigger.proc945 WITH .T.,edi_trigger.file945 WITH cFilenameHold,;
			edi_trigger.isa_num WITH cISA_Num,;
			edi_trigger.fin_status WITH "945 CREATED",edi_trigger.errorflag WITH .F.,edi_trigger.when_proc WITH DATETIME() ;
			FOR edi_trigger.bol = cBOL AND edi_trigger.accountid = nAcctNum
	ELSE
		REPLACE edi_trigger.processed WITH .T.,edi_trigger.proc945 WITH .F.,edi_trigger.file945 WITH "",;
			edi_trigger.fin_status WITH cStatus,edi_trigger.errorflag WITH .T. ;
			FOR edi_trigger.bol = cBOL AND edi_trigger.accountid = nAcctNum
		IF lCloseOutput
			=FCLOSE(nFilenum)
			ERASE &cFilenameHold
		ENDIF
	ENDIF
ENDIF

IF USED('outship')
	USE IN outship
ENDIF
IF USED('outdet')
	USE IN outdet
ENDIF

IF USED('serfile')
	SELECT serfile
	IF lTesting
*!*				REPLACE serfile.seqnum WITH nOrigSeq
*!*				REPLACE serfile.grpseqnum WITH nOrigGrpSeq
	ENDIF
	USE IN serfile
ENDIF
IF USED('scacs')
	USE IN scacs
ENDIF
IF USED('mm')
	USE IN mm
ENDIF
IF USED('tempx')
	USE IN tempx
ENDIF
IF USED('shipment')
	USE IN shipment
ENDIF
IF USED('package')
	USE IN package
ENDIF
IF !lTesting
	SELECT edi_trigger
	LOCATE
ENDIF
IF lIsError
ENDIF
RETURN

****************************
PROCEDURE cstringbreak
****************************
cLen = LEN(ALLTRIM(cString))
TRY
	FPUTS(nFilenum,cString)
CATCH
ENDTRY
RETURN