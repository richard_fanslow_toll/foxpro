* This program identifies when a a "A" whseloc was last pulled
utilsetup("MJ_KPI_ONHAND")
set exclusive off
set deleted on
set talk off
set safety off   &&Allows for overwrite of existing files


**********************************START KPI RETURNS
**********************************START NJ
_screen.windowstate = 1
close databases all

goffice="J"

xsqlexec("select outship.accountid, style, color, id, totqty as pulledqty from outship, outdet where outship.mod='J' " + ;
	"and outship.outshipid=outdet.outshipid and cancelrts=0 and pulleddt#{} and del_date={} and bol_no#'NOTH' " + ;
	"and outship.accountid#6453 and notonwip=0","p1a",,"wh")

xsqlexec("select * from inven where mod='J'",,,"wh")
xsqlexec("select * from invenloc where mod='J'",,,"wh")
goffice="J"
useca("indet","wh",,,"mod='"+goffice+"' and  accountid in (6303,6325,6543) and date_rcvd>{"+dtoc(date()-90)+"}")

upcmastsql(6303)
select accountid,style,color,id,totqty,holdqty,allocqty,availqty, space(3) as div from inven where totqty+holdqty+allocqty!=0 and accountid !=6453 into cursor t1 readwrite
select accountid,style,color,id,sum(pulledqty) as pulledqty from p1a group by 1,2,3,4 into cursor p1 readwrite
select * from t1 a full join p1 b on a.accountid=b.accountid and a.style=b.style and a.color=b.color and a.id=b.id into cursor p2 readwrite
replace accountid_a with accountid_b for isnull(accountid_a)
replace style_a with style_b for isnull(style_a)
replace color_a with color_b for isnull(color_a)
replace id_a with id_b for isnull(id_a)
replace totqty with 0 for isnull(totqty)
replace holdqty with 0 for isnull(holdqty)
replace allocqty with 0 for isnull(allocqty)
replace availqty with 0 for isnull(availqty)
replace pulledqty with 0 for isnull(pulledqty)
select accountid_a as accountid, style_a as style, color_a as color,id_a as id,totqty+pulledqty as totqty,holdqty,allocqty,pulledqty, availqty,div from p2 into cursor p3 readwrite


select a.*, info,upc from p3 a left join upcmast b on a.style=b.style and a.color=b.color and a.id=b.id and b.accountid=6303 into cursor c1 readwrite
replace all div with getmemodata("info","DIV")
select * from c1 into cursor n1 readwrite

xsqlexec("select * from outship where mod='J' and inlist(accountid,6303,6325,6543) and wo_date>{"+dtoc(date()-120)+"}",,,"wh")
xsqlexec("select * from outdet where mod='J' and inlist(accountid,6303,6325,6543) and wo_date>{"+dtoc(date()-120)+"}",,,"wh")

select a.*,outshipid from n1 a left join outdet b on a.accountid=b.accountid and a.style=b.style and a.color=b.color and a.id=b.id and !inlist(a.accountid,6453) into cursor n2 readwrite
select a.*, del_date from n2 a left join outship b on a.outshipid=b.outshipid into cursor n3 readwrite

select accountid,style,color,id,totqty,holdqty,allocqty, pulledqty, availqty,div,upc,max(del_date) from n3 group by accountid,style,color,id,totqty,holdqty,allocqty,pulledqty, availqty,div,upc into cursor n4 readwrite
select space(6) as whse,a.*,b.max_del_date as last_shpd from c1 a left join n4 b on a.accountid=b.accountid and a.style=b.style and a.color=b.color and a.id=b.id into cursor n5 readwrite
select  whse,a.accountid,a.style,a.color,a.id,a.totqty,holdqty,allocqty, pulledqty, availqty,div,upc ,last_shpd, max(date_rcvd) as date_rcvd  from n5 a left join indet b on a.accountid=b.accountid and a.style=b.style;
	and a.color=b.color and a.id=b.id group by whse, a.accountid,a.style,a.color,a.id,a.totqty,holdqty,allocqty,pulledqty,availqty,div,upc,last_shpd into cursor n6 readwrite

***Select Space(6) As whse,a.*,date_rcvd From n5 a Left Join indet b On a.accountid=b.accountid And a.Style=b.Style And a.Color=b.Color And a.Id=b.Id Group By WHSE, a.accountid,a.Style,a.Color,a.Id,a.totqty,holdqty,allocqty,availqty,div,upc,last_shpd Into Cursor n6 Readwrite
replace all whse with '10100' for accountid=6303 in n6
replace all whse with '10180' for accountid=6325 in n6
replace all whse with '10190' for accountid=6543 in n6
*******added TMARG 7/6/16 to capture QC quantity
****remove quantity in QC location from 6303 and into 10160
select accountid,style,color,id,locqty as qcqty from invenloc where 'QC' $whseloc and locqty!=0 into cursor qc1 readwrite
if reccount() > 0
	select a.*,qcqty from n6 a full join qc1 b on a.accountid=b.accountid and a.style=b.style and a.color=b.color and a.id=b.id into cursor n7 readwrite
	replace qcqty with 0 for isnull(qcqty) in n7
	select whse, accountid, style,color,id,totqty-qcqty as totqty,holdqty-qcqty as holdqty, allocqty,pulledqty,availqty,div,upc,last_shpd,date_rcvd,qcqty from n7 into cursor n8 readwrite
	replace qcqty with 0 for !isnull(qcqty) in n8

	select space(6) as whse, accountid,style,color,id,qcqty as totqty,qcqty as holdqty,0000 as allocqty,0000 as pulledqty, 0000 as availqty, space(3) as div,space(14) as upc,date() as last_shpd, date() as date_rcvd,qcqty from qc1 into cursor qc2 readwrite
	replace last_shpd with {  /  /    } for !isnull(last_shpd)
	replace date_rcvd with {  /  /    } for !isnull(date_rcvd)
	replace all whse with '10100' for accountid=6303 in qc2
	replace all whse with '10180' for accountid=6325 in qc2
	replace all whse with '10190' for accountid=6543 in qc2
	select a.*,b.upc as upc2, info from qc2 a left join upcmast b on a.style=b.style and a.color=b.color and a.id=b.id into cursor qc3 readwrite
	replace upc with upc2 for empty(upc)
	replace all div with getmemodata("info","DIV")
	select whse, accountid,style,color,id,totqty,holdqty, allocqty, pulledqty, availqty, div, upc, last_shpd,  date_rcvd,qcqty from qc3 into cursor qc4 readwrite
	select * from n8 union all select * from qc4 into cursor n6a readwrite


else
	select *, 00000 as qcqty from n6 into cursor n6a readwrite
endif
select whse,space(6) as ax_whse, style,color,id,totqty,holdqty,allocqty,pulledqty,availqty,div,upc,last_shpd,date_rcvd,qcqty from n6a into cursor n6b readwrite
replace ax_whse with '10160' for qcqty!=0 in n6b
replace ax_whse with '10100' for qcqty=0 and whse='10100' in n6b
replace ax_whse with '10180' for qcqty=0 and whse='10180' in n6b
replace ax_whse with '10190' for qcqty=0 and whse='10190' in n6b
*SET STEP ON 
use in outship
use in outdet
use in inven
use in invenloc
use in indet
*****************************ML

goffice="L"

xsqlexec("select * from inven where mod='L'",,,"wh")
xsqlexec("select * from invenloc where mod='L'",,,"wh")

*SET STEP ON 
useca("indet","wh",,,"mod='L' and  accountid in (6303,6325,6543) and date_rcvd>{"+dtoc(date()-90)+"}")

select accountid,style,color,id,totqty,holdqty,allocqty, availqty, space(3) as div from inven where totqty+holdqty+allocqty!=0 and inlist(accountid,6303,6325,6543) into cursor t2 readwrite

	xsqlexec("select outship.accountid, style, color, id, totqty as pulledqty from outship, outdet where outship.mod='L' " + ;
		"and outship.outshipid=outdet.outshipid and cancelrts=0 and pulleddt#{} and del_date={} and bol_no#'NOTH' " + ;
		"and outship.accountid#6453 and notonwip=0","q1a",,"wh")

select accountid,style,color,id,sum(pulledqty) as pulledqty from q1a group by 1,2,3,4 into cursor q1 readwrite
select * from t2 a full join q1 b on a.accountid=b.accountid and a.style=b.style and a.color=b.color and a.id=b.id into cursor q2 readwrite
replace accountid_a with accountid_b for isnull(accountid_a)
replace style_a with style_b for isnull(style_a)
replace color_a with color_b for isnull(color_a)
replace id_a with id_b for isnull(id_a)
replace totqty with 0 for isnull(totqty)
replace holdqty with 0 for isnull(holdqty)
replace allocqty with 0 for isnull(allocqty)
replace availqty with 0 for isnull(availqty)
replace pulledqty with 0 for isnull(pulledqty)
select accountid_a as accountid, style_a as style, color_a as color,id_a as id,totqty+pulledqty as totqty,holdqty,allocqty,pulledqty, availqty,div from q2 into cursor q3 readwrite

select a.*, info,upc from q3 a left join upcmast b on a.style=b.style and a.color=b.color and a.id=b.id and b.accountid=6303 into cursor c2 readwrite
replace all div with getmemodata("info","DIV")
select * from c2  into cursor m1 readwrite

xsqlexec("select * from outship where mod='L' and inlist(accountid,6303,6325,6543) and wo_date>{"+dtoc(date()-120)+"}",,,"wh")
xsqlexec("select * from outdet where mod='L' and inlist(accountid,6303,6325,6543) and wo_date>{"+dtoc(date()-120)+"}",,,"wh")

select a.*,outshipid from m1 a left join outdet b on a.accountid=b.accountid and a.style=b.style and a.color=b.color and a.id=b.id and inlist(a.accountid,6303,6325,6543) into cursor m2 readwrite
select a.*, del_date from m2 a left join outship b on a.outshipid=b.outshipid into cursor m3 readwrite

select accountid,style,color,id,totqty,holdqty,allocqty,  pulledqty, availqty,div,upc,max(del_date) as last_shpd from m3 group by accountid,style,color,id,totqty,holdqty,allocqty,pulledqty, availqty,div,upc into cursor m4 readwrite
select a.*, last_shpd from c2 a left join m4 b on a.accountid=b.accountid and a.style=b.style and a.color=b.color and a.id=b.id into cursor m5 readwrite
select space(6) as whse,a.*,max(date_rcvd) as date_rcvd  from m5 a left join indet b on a.accountid=b.accountid and a.style=b.style and a.color=b.color and a.id=b.id group by whse, a.accountid,a.style,a.color,a.id,a.totqty,holdqty,allocqty,availqty,div,upc,last_shpd into cursor m6 readwrite
*SET STEP ON 

***Select Space(6) As whse,a.*,date_rcvd From m5 a Left Join indet b On a.accountid=b.accountid And a.Style=b.Style And a.Color=b.Color And a.Id=b.Id Group By a.accountid,a.Style,a.Color,a.Id,a.totqty,holdqty,allocqty,availqty,div,upc,last_shpd Into Cursor m6 Readwrite
replace all whse with '11100' for accountid=6303 in m6
replace all whse with '11180' for accountid=6325 in m6
replace all whse with '11190' for accountid=6543 in m6
*******added TMARG 7/6/16 to capture QC quantity
*!*	select accountid,style,color,id,locqty as qcqty from invenloc where 'QC' $whseloc and locqty!=0 into cursor qc2 readwrite
*!*	if reccount() > 0
*!*		select a.*,qcqty from m6 a full join qc2 b on a.accountid=b.accountid and a.style=b.style and a.color=b.color and a.id=b.id into cursor m7 readwrite
*!*		replace qcqty with 0 for isnull(qcqty)
*!*		select whse, accountid, style,color,id,totqty,holdqty-qcqty as holdqty, allocqty,pulledqty,availqty,div,upc,last_shpd,date_rcvd,qcqty from m7 into cursor m6a readwrite
*!*	else
*!*		select *, 00000 as qcqty from m6 into cursor m6a readwrite
*!*	ENDIF

set step on

****remove quantity in QC location from 6303 and into 11160
select accountid,style,color,id,locqty as qcqty from invenloc where 'QC' $whseloc and locqty!=0 into cursor qc1m readwrite
if reccount() > 0
	select a.*,qcqty from m6 a full join qc1m b on a.accountid=b.accountid and a.style=b.style and a.color=b.color and a.id=b.id into cursor m7 readwrite
	replace qcqty with 0 for isnull(qcqty) in m7
	select whse, accountid, style,color,id,totqty-qcqty as totqty,holdqty-qcqty as holdqty, allocqty,pulledqty,availqty,div,upc,last_shpd,date_rcvd,qcqty from m7 into cursor m8 readwrite
	replace qcqty with 0 for !isnull(qcqty) in m8

	select space(6) as whse, accountid,style,color,id,qcqty as totqty,qcqty as holdqty,0000 as allocqty,0000 as pulledqty, 0000 as availqty, space(3) as div,space(14) as upc,date() as last_shpd, date() as date_rcvd,qcqty from qc1m into cursor qc2m readwrite
	replace last_shpd with {  /  /    } for !isnull(last_shpd) in qc2m
	replace date_rcvd with {  /  /    } for !isnull(date_rcvd) in qc2m
	replace all whse with '11100' for accountid=6303 in qc2m
	replace all whse with '11180' for accountid=6325 in qc2m
	replace all whse with '11190' for accountid=6543 in qc2m
	select a.*,b.upc as upc2, info from qc2m a left join upcmast b on a.style=b.style and a.color=b.color and a.id=b.id into cursor qc3m readwrite
	replace upc with upc2 for empty(upc) in qc3m
	replace all div with getmemodata("info","DIV")  in qc3m
	select whse, accountid,style,color,id,totqty,holdqty, allocqty, pulledqty, availqty, div, upc, last_shpd,  date_rcvd,qcqty from qc3m into cursor qc4m readwrite
	select * from m8 union all select * from qc4m into cursor m6a readwrite
else
	select whse, accountid,style,color,id,totqty,holdqty, allocqty, pulledqty, availqty, div, upc, last_shpd,  date_rcvd, 00000 as qcqty from m6 into cursor m6a readwrite
endif
select whse,space(6) as ax_whse, style,color,id,totqty,holdqty,allocqty,pulledqty,availqty,div,upc,last_shpd,date_rcvd,qcqty from m6a into cursor m6b readwrite
replace ax_whse with '11160' for qcqty!=0 in m6b
replace ax_whse with '11100' for qcqty=0 and whse='11100' in m6b
replace ax_whse with '11180' for qcqty=0 and whse='11180' in m6b
replace ax_whse with '11190' for qcqty=0 and whse='11190' in m6b

select * from n6b union all select * from m6b into cursor final readwrite
select final

*copy to s:\marcjacobsdata\reports\mjdashboard\mjkpionhand type csv
copy to F:\FTPUSERS\MJ_Wholesale\OUT\reports\mjkpionhand type csv

close data all

schedupdate()
_screen.caption=gscreencaption
on error