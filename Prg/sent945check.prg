utilsetup("SENT945CHECK")

PUBLIC cDelim,cArchDirStem,cArchDir,len1,lTesting,nDaysBack,dToday
DO m:\dev\prg\_setvars WITH .T.
DO m:\dev\prg\lookups
SET HOURS TO 24

lTesting = .f.
lBrowFiles = lTesting
*lBrowFiles = .t.

_SCREEN.WINDOWSTATE = IIF(lTesting,2,1)
_SCREEN.CAPTION= "Sent 945 Check Process"

USE F:\3pl\DATA\edi_trigger IN 0 ALIAS edi_trigger ORDER TAG acct_pt

IF FILE('f:\3pl\data\temp945check.dbf')
	DELETE FILE "F:\3pl\DATA\temp945check.dbf"
ENDIF

IF lTesting
	SET STEP ON
ENDIF

cArchDirStem = "F:\FTPUSERS\"
dToday = DATE()
nDaysBack = IIF(DOW(dToday)=2,5,3) && How far to look back in OUTSHIP records

SELECT 0
USE F:\3pl\DATA\trigger945accts.DBF ALIAS accts ORDER TAG GROUP

SCAN FOR ACTIVE AND accts.GROUP = "MORET"
	IF USED('outship')
		USE IN outship
	ENDIF
*	RELEASE ALL LIKE ary1
	len1 = 0
	nAcctNum = accts.accountid

	cName = ALLTRIM(accts.NAME)
	cOffice = ALLTRIM(accts.office)
	cWhse = ALLTRIM(accts.whse)
	cGroup = ALLTRIM(accts.GROUP)

	WAIT WINDOW "Now Processing 945 trail for "+cName+" Acct: "+ALLTRIM(STR(nAcctNum)) TIMEOUT 1

	WAIT WINDOW "Now Selecting OUTSHIP records for "+cName+", WHSE "+cWhse NOWAIT
	xReturn = "XXX"
	DO m:\dev\prg\wf_alt WITH cOffice,nAcctNum
	
	cUseFolder = UPPER(xReturn)
	IF EMPTY(cUseFolder)
		WAIT WINDOW "Use Folder is empty" TIMEOUT 2
		ASSERT .F. MESSAGE "In empty UseFolder"
		schedupdate()
		CLOSE DATABASES ALL
		CANCEL
		RETURN
	ENDIF

	xsqlexec("select * from outship where mod = '"+cWhse+"' and del_date >= {"+DTOC(dToday - nDaysBack)+"}",,,"wh")

	SELECT cOffice AS office,accountid,consignee,del_date,bol_no,;
		LEFT(ALLTRIM(ship_ref),6) AS ship_ref,ctnqty,qty ;
		FROM outship ;
		WHERE del_date >= (dToday-nDaysBack) ;
		AND !EMPTYnul(del_date) ;
		AND accountid = nAcctNum ;
		AND delenterdt<(DATETIME()-1200) ;
		AND !"OV"$ship_ref ;
		INTO CURSOR outdeliv READWRITE
	IF lBrowFiles
		BROWSE
	ENDIF

	DO CASE
		CASE INLIST(cOffice,"I","N")
			cArchDir = (cArchDirStem+"Moret-NJ\945out\945archive")
		CASE cOffice = "M"
			cArchDir = (cArchDirStem+"Moret-FL\945out\945archive")
		CASE INLIST(cOffice,"E","C","2")
			cArchDir = (cArchDirStem+"Moret-SP\945out\945archive")
		OTHERWISE
			cArchDir = (cArchDirStem+"Moret-ML\945out\945archive")
	ENDCASE

	IF lBrowFiles
		WAIT WINDOW cArchDir TIMEOUT 2
	ENDIF

	SELECT outdeliv
	SCAN
		IF "~C"$outdeliv.ship_ref
			REPLACE outdeliv.ship_ref WITH ;
				ALLTRIM(STRTRAN(outdeliv.ship_ref,"~C",""))
		ENDIF
		IF "~U"$outdeliv.ship_ref
			REPLACE outdeliv.ship_ref WITH ;
				ALLTRIM(STRTRAN(outdeliv.ship_ref,"~U",""))
		ENDIF
	ENDSCAN

	SELECT outdeliv
	CD &cArchDir
	CREATE CURSOR temp1 (ship_ref c(20), filename c(50))
	WAIT CLEAR

	moret945()

ENDSCAN

SELECT temp945check
LOCATE

IF lTesting
	BROW
	ASSERT .F. MESSAGE "At deletion scan process"
ENDIF

IF !EOF()
	LOCATE
	DELETE FOR (INLIST(accountid,&gMoretAcctList) OR INLIST(accountid,&gMoretAcctList2)) ;
		AND (qty = 0 OR ctnqty=0)
	WAIT "" TIMEOUT 2
ENDIF

LOCATE
IF !EOF()
	ASSERT .F. MESSAGE "Scanning Check file"
	SCAN
		cStringck = STR(temp945check.accountid,4)+PADR(ALLTRIM(temp945check.ship_ref),20)
		SELECT edi_trigger
		IF SEEK(cStringck,'edi_trigger','acct_pt')
			DO CASE
				CASE EMPTY(when_proc) AND !processed
					DELETE NEXT 1 IN temp945check
				CASE when_proc < DATETIME()-300 AND processed=.T. AND errorflag = .F.
					DELETE NEXT 1 IN temp945check
				CASE "NO 945"$UPPER(fin_status) AND processed=.T. AND errorflag = .F.
					DELETE NEXT 1 IN temp945check
			ENDCASE
		ENDIF
	ENDSCAN
	IF lTesting
		BROWSE
	ENDIF
	LOCATE
	IF !EOF()
		ASSERT .F.
		WAIT WINDOW "Records are missing...mailing in process" TIMEOUT 2
		IF !lTesting
			domail()
		ENDIF
	ELSE
		WAIT WINDOW "No records unsent...exiting" TIMEOUT 2
	ENDIF
ENDIF

schedupdate()

CLOSE DATABASES ALL
SET HOURS TO 12
WAIT CLEAR
RELEASE  cDelim,cArchDirStem,cArchDir,len1,lTesting,nDaysBack,dToday
RETURN

***************************
*!* END OF MAIN CODE BODY *
***************************

**********************
PROCEDURE moret945
**********************

	nDaysBack945 = IIF(DOW(dToday)=2,7,5)

	WAIT WINDOW "Now Selecting 945 File records for "+cName NOWAIT
	IF ADIR(ary1,"*.*") = 0
		RETURN
	ENDIF
	=ASORT(ary1)
	len1 = ALEN(ary1,1)
	FOR j = 1 TO len1
		dDate = ary1[j,3]
		IF dDate < (dToday-nDaysBack945)
			LOOP
		ENDIF
		cFilename = ALLTRIM(ary1[j,1])
		STORE cFilename TO m.filename
		CREATE CURSOR tempx (field1 c(200))
		APPEND FROM [&cFilename] TYPE SDF
		SELECT tempx
		cMoYr = ALLTRIM(SUBSTR(DTOS(DATE()),5))
		LOCATE FOR LEFT(ALLTRIM(field1),2) = "OH"
		IF found()
			lOH = .T.
			cScanStr = "LEFT(ALLTRIM(field1),2) = 'OH'"
		ELSE
			lOH = .F.
			cScanStr = "left(ALLTRIM(field1),3) = 'W06'"
		ENDIF

		SCAN FOR &cScanStr
			IF lOH
				m.ship_ref=SUBSTR(ALLTRIM(field1),3,6)
			ELSE
				m.ship_ref = SUBSTR(ALLTRIM(field1),6,6)
			ENDIF
			INSERT INTO temp1 FROM MEMVAR
		ENDSCAN
		IF USED('tempx')
			USE IN tempx
		ENDIF
	ENDFOR
	SELECT temp1

	SELECT a.*,b.filename,.F. AS trigrec ;
		FROM outdeliv a LEFT JOIN temp1 b ;
		ON a.ship_ref = b.ship_ref ;
		INTO CURSOR temp2

	IF !FILE('f:\3pl\data\temp945check.dbf')
		COPY STRUCTURE TO F:\3pl\DATA\temp945check
	ENDIF

	SELECT * FROM temp2 WHERE ISNULL(filename) INTO CURSOR temp3
	IF !USED('temp945check')
		SELECT 0
		USE F:\3pl\DATA\temp945check
	ENDIF
	SELECT temp945check
	APPEND FROM DBF('temp3')
	USE IN temp2
	USE IN temp3
	WAIT CLEAR

ENDPROC

**********************
PROCEDURE main945
**********************

	nDaysBack945 = IIF(DOW(dToday)=2,7,5)

	WAIT WINDOW "Now Selecting 945 File records for "+cName NOWAIT
	IF ADIR(ary1,"*.*") = 0
		RETURN
	ENDIF
	=ASORT(ary1,3,-1,1)
	len1 = ALEN(ary1,1)
	FOR j = 1 TO len1
		cFilename = LOWER(ALLTRIM(ary1[j,1]))
		dDate = ary1[j,3]
		IF dDate < (dToday-nDaysBack945)
			LOOP
		ENDIF

		IF LEFT(cFilename,3) = "997"
			LOOP
		ENDIF
		STORE cFilename TO m.filename
		DO m:\joeb\prg\createx856a
		SELECT x856
		APPEND FROM [&cFilename] TYPE DELIMITED WITH CHARACTER &cDelim
		SCAN FOR ALLTRIM(x856.segment) = "W06"
			m.ship_ref=ALLTRIM(x856.f2)
			INSERT INTO temp1 FROM MEMVAR
		ENDSCAN
	ENDFOR
	SELECT temp1

	SELECT a.*,b.filename,.F. AS trigrec ;
		FROM outdeliv a LEFT JOIN temp1 b ;
		ON a.ship_ref == b.ship_ref ;
		INTO CURSOR temp2

	IF !FILE('f:\3pl\data\temp945check.dbf')
		COPY STRUCTURE TO F:\3pl\DATA\temp945check
	ENDIF

	SELECT * FROM temp2 WHERE ISNULL(filename) INTO CURSOR temp3
	IF !USED('temp945check')
		SELECT 0
		USE F:\3pl\DATA\temp945check
	ENDIF
	SELECT temp945check
	APPEND FROM DBF('temp3')
	USE IN temp2
	USE IN temp3
	WAIT CLEAR
ENDPROC

*********************
PROCEDURE domail
*********************

	SELECT temp945check
	cOutfile = "f:\3pl\data\deliv_no945.xls"
	COPY TO &cOutfile TYPE XL5
	USE IN temp945check
	DELETE FILE "f:\3pl\data\temp945check.dbf"
	tFrom ="TGF WMS Operations <transload-ops@fmiint.com>"
	SELECT 0
	USE F:\3pl\DATA\mailmaster ALIAS mm
	LOCATE FOR mm.edi_type = "MISC" AND mm.taskname = "JOETEST"
	tsendto = IIF(mm.use_alt,mm.sendtoalt,mm.sendto)
	tcc = IIF(mm.use_alt,mm.ccalt,mm.cc)
	USE IN mm

	tsubject = "Delivered Shipments - No 945s"
	tattach = cOutfile
	tmessage = "(Program: Sent945check.exe)"+CHR(13)
	tmessage = tmessage + CHR(10) + "The attached Excel file contains delivered PTs for which no 945 was found."
	tmessage = tmessage + CHR(10) + "Please review ASAP."

	DO FORM m:\dev\frm\dartmail2 WITH tsendto,tFrom,tsubject,tcc,tattach,tmessage,"A"
	DELETE FILE &cOutfile
ENDPROC
