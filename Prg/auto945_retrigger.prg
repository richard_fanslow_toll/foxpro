utilsetup("AUTO_RETRIGGER")

_setvars()
CLOSE DATA ALL
_SCREEN.WINDOWSTATE=1
USE F:\3pl\DATA\edi_trigger.DBF ALIAS edi_trigger SHARED
DOskip = .F.

*!* Conditional statement added 09/04/07, Joe
*!* Will retrigger ALL error 945s every hour on the 20s between 5-11PM,
*!* but only Nanjing 'del_date' timing errors otherwise

DO CASE
	CASE BETWEEN(HOUR(DATETIME() ),17,23) AND BETWEEN(MINUTE(DATETIME()),15,30)
		REPLACE edi_trigger.processed WITH .F. ;
			FOR edi_trigger.errorflag ;
			AND DOskip = .F. ;
			AND !INLIST(edi_trigger.accountid,6293,6521) ;
			AND edi_trigger.edi_type = "945" ;
			AND TTOD(edi_trigger.trig_time)>(DATE()-1) IN edi_trigger
	CASE TIME()>"08:15:00"
		REPLACE edi_trigger.processed WITH .F. ;
			FOR edi_trigger.errorflag ;
			AND DOskip = .F. ;
			AND edi_trigger.edi_type = "945" ;
			AND ((INLIST(edi_trigger.accountid,4610,4694,4677) ;
			AND INLIST(edi_trigger.fin_status,"EMPTY DELDATE","EMPTY DEL_DATE","INCOMP BOL")) ;
			OR (edi_trigger.accountid = 5383 AND edi_trigger.fin_status # "SQL ERROR")) IN edi_trigger
	OTHERWISE
		REPLACE edi_trigger.processed WITH .F. ;
			FOR edi_trigger.errorflag ;
			AND DOskip = .F. ;
			AND INLIST(edi_trigger.fin_status,"EMPTY DELDATE","EMPTY DEL_DATE","INCOMP BOL") IN edi_trigger
ENDCASE
*			AND INLIST(edi_trigger.accountid,4610,4694,4677,6521) 
USE IN edi_trigger

SELECT 0
USE F:\edirouting\ftpsetup.DBF ALIAS ftpsetup
REPLACE ftpsetup.chkbusy WITH .F. FOR transfer # "TRIG-" AND chkbusy
IF !BETWEEN(TIME(),"00:00:01","08:00:00")
	REPLACE ftpsetup.folderchk WITH .T. FOR transfer = "TRIG-" AND !folderchk
	REPLACE ftpsetup.chkbusy WITH .F. FOR transfer = "TRIG-" AND chkbusy
ENDIF
CLOSE DATA ALL
schedupdate()
