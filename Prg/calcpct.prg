lparameter xnum1, xnum2, xwhole

do case
case xnum1=0 or xnum2=0 or (xnum1<0 and xnum2>0) or (xnum1>0 and xnum2<0)
	return 0
case !xwhole
	return max(min(round((xnum2-xnum1)/xnum1*100,2),999.99),-999.99)
otherwise
	return max(min(round((xnum2-xnum1)/xnum1*100,0),999),-99)
endcase