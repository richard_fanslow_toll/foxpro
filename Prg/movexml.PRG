utilsetup("MOVEXML")

DO m:\dev\prg\_setvars

lcPath = "c:\amsxml\"
lcLocalPath ="c:\amsxml\sent\"
lcArchivePath = "f:\efreight\amsxml\"

lnNum = ADIR(tarray,"c:\amsxml\*.xml")

IF lnNum =0
	WAIT WINDOW AT 10,10 " No AMS/XML files to upload !! " TIMEOUT 2
	schedupdate()
	RETURN
Else
	WAIT WINDOW AT 10,10 " There are "+Alltrim(Transform(lnNum))+" files to move over........" TIMEOUT 2
EndIf 

IF lnNum >= 1
	FOR thisfile = 1  TO lnNum
		xfile = lcPath+ALLTRIM(tarray[thisfile,1])
		archivefile = lcArchivePath+ALLTRIM(tarray[thisfile,1])
		localfile =  lcLocalPath+ALLTRIM(tarray[thisfile,1])
		WAIT WINDOW AT 10,10 "Checking file: "+xfile TIMEOUT 1
		COPY FILE &xfile TO &localfile
		COPY FILE &xfile TO &archivefile
		IF FILE(archivefile)
			DELETE FILE &xfile
		ENDIF
	NEXT
ENDIF

*!*	* all  of the XML files are now F:\EFREIGHT\AMSXML\

*!*	DO m:\dev\prg\efreight_extract_xml.prg

*!*	* extract the BOL XLS data from all XML files and place the result
*!*	* in F:\EFREIGHT\AMSXML\ (and archive them, too), then email XLS to a list
*!*	* the FTP job will transfer all files to Lognet

Set Exclusive off
WAIT WINDOW AT 10,10 " Inserting a an FTP job.............." TIMEOUT 2
INSERT INTO F:\edirouting\ftpjobs (jobname,userid,jobtime) VALUES ("EFREIGHT-AMSXFER","MOVEXML",Datetime())
WAIT WINDOW AT 10,10 "FTP Job Inserted: " TIMEOUT 2

schedupdate()
