parameter lcoffice,acct_num, xtesting

utilsetup("NANJINGINVENTORY")

*try

	if vartype(acct_num)="L"
		acct_num = "4610"
	endif

*Set Step On
	close databases all
	close tables all
	set enginebehavior 70
	set exclusive off
	set safety off
	set deleted on
	set asserts off
	set tablevalidate to 0
	tfrom = "TGF Warehouse Operations <warehouse-ops@fmiint.com>"
	tattach = " "

*!*	if !xtesting and set("default")#"M"
*!*		_screen.Caption="SENDINVENTORY"
*!*		gsystemmodule="SENDINVENTORY"
*!*		guserid="???"
*!*		on error do fmerror with error(), program(), lineno(1), sys(16),, .t.
*!*	endif

	acct_num = val(acct_num)

	if file("c:\tempfox\fo2.Dbf")
		delete file c:\tempfox\fo2.dbf
	endif


	for lnoffice = 2 to 2
*!*		SET NOTIFY ON
*!*		SET TALK ON
*!*		SET STATUS ON

		do case
		case lnoffice = 1
			lcoffice = "N"
			goffice="I"
		case lnoffice = 2
			lcoffice = "Y"
			goffice="Y"
		endcase

		lcwhpath = wf(lcoffice,acct_num)
		store .f. to testing
		tcc = ""

		do case

		case acct_num = 4610
			acctlist = "4610,4694"
			lcfilename = "NANJING_"+lcoffice+"_"+ttoc(datetime(),1)+".846"
			lcfilename2 = "NANJING_"+lcoffice+"_inproc_"+ttoc(datetime(),1)+".846"
			tsendto = 'pgaidis@fmiint.com'
		endcase


		lcdate = dtos(date())
		lctruncdate = right(lcdate,6)
		lctime = left(time(date()),2)+substr(time(date()),4,2)
		lcfiledate = lcdate+lctime
		lcqtyrec = "1"
		lcorig = "J"

* removed the "!SP" in the query to take into account the freight for SP that will be shipped

		xsqlexec("select * from outdet where 1=0","tempdet",,"wh")
		
		xsqlexec("select * from outship where mod='"+goffice+"' and inlist(accountid,"+acctlist+") " + ;
			"and del_date+{} and notonwip=0 and qty#0 and ctnqty#0 order by wo_num","inprocess",,"wh")

		xsqlexec("select * from project where office='"+lcoffice+"' and inlist(accountid,"+acctlist+")",,,"wh")
		index on wo_num tag wo_num
		select * from project into cursor spwos readwrite

		set talk off
		set notify off
		set status off

		assert .f.
		select inprocess  && inprocess is a cursor of all outship records where they are not yet delivered and also excluding "notonwip"
* Delete For sp=.T.  && dont need any outship records that are related to special projects  / removed 1/21 we need to account for SP that are to be shipped
		goto top

		if inlist(acct_num,&acctlist)
			select spwos
			scan
				select inprocess
				wait window at 10,10 "Looking for WO "+transform(spwos.wo_num) nowait
				locate for inprocess.wo_num = spwos.wo_num
				if found()
					select inprocess
					delete for inprocess.wo_num = spwos.wo_num
				endif
			endscan
		endif

		select inprocess
		goto top
		lnrec= reccount()
		i=1
		scan
			wait window at 10,10 "Working on "+str(i)+"  of "+str(lnrec) nowait

			xsqlexec("select * from outdet where outdet.outshipid="+transform(inprocess.outshipid),"ttindet",,"wh")

			select ttindet
			scan
				select ttindet
				scatter memvar
				select tempdet
				append blank
				gather memvar
			endscan
			i=i+1
		endscan

		select tempdet

		select *,0000 as availqty, 000000000 as pickedqty,00000000 as outwo from tempdet group by style,color,id,pack into cursor items readwrite

		select items
		scan
			select tempdet
			sum totqty for tempdet.style = items.style and tempdet.color=items.color and tempdet.pack=items.pack to lntotqty
			replace items.pickedqty with lntotqty in items

* added the totqty set to 0 ..... this was tested by PFG 2/23/2006 only effects items that are no longer in inventory but still
* on some WIP... or in outdet records
			replace items.totqty with 0 in items

		endscan

		xsqlexec("select * from inven where mod='"+goffice+"'",,,"wh")
		select * ,0000000 as pickedqty,0000000 as spqty from inven where inlist(accountid,&acctlist) and totqty >0 into cursor inventory readwrite

*!*	 If lnOffice = 1
*!*	    SELECT * ,0000000 AS PickedQty,0000000 AS SPQty FROM m:\temp\njinven.dbf WHERE INLIST(accountid,&AcctList) AND totqty >0 INTO CURSOR inventory READWRITE
*!*	 EndIf
*!*
*!*	 If lnOffice = 2
*!*	   SELECT * ,0000000 AS PickedQty,0000000 AS SPQty FROM m:\temp\inven.dbf WHERE INLIST(accountid,&AcctList) AND totqty >0 INTO CURSOR inventory READWRITE
*!*	 endif

		select inventory
		index on style+color+id+pack tag upc
		set order to upc

		delete for availqty =0 and holdqty=0
		select inventory
		goto top

		select items
		select * from items into array itemsarray

		select inventory

		select items
		scan
			scatter memvar
			m.style = items.style
			m.color = items.color
			m.id    = items.id
			m.pack  = items.pack
			m.pickedqty = items.pickedqty
			select inventory
			seek m.style+m.color+m.id+m.pack
			if !found()
				append blank
				gather memvar
			else
				replace pickedqty with m.pickedqty in inventory
			endif
		endscan

		if inlist(acct_num,&acctlist)  && now add in the special project inventory that is not yet delivered
			xsqlexec("select outdet.wo_num, style, color, id, pack, totqty from outdet, project " + ;
			"where outdet.wo_num=project.wo_num and completeddt={}","xdy",,"wh")

			select wo_num, style, color, id, pack, sum(totqty) as spqty from xdy group by 1,2,3,4,5 into cursor xrpt readwrite

			select xrpt
			scan
				if !seek(xrpt.wo_num,"project","wo_num")
					select xrpt
					delete
				endif
			endscan

			select xrpt
			scan
				select xrpt
				scatter memvar
				m.style = xrpt.style
				m.color = xrpt.color
				m.id    = xrpt.id
				m.pack  = xrpt.pack
				m.spqty = xrpt.spqty
				select inventory
				seek m.style+m.color+m.id+m.pack
				if !found()
					append blank
					gather memvar
				else
					replace xrpt.spqty with xrpt.spqty + m.spqty in inventory
				endif
			endscan
		endif

		select inventory

		delete for accountid = 4694 and units
		goto top

		delete for accountid = 4610 and !units

		if lnoffice = 2
			select space(10) as nanjingacct, accountid,units,style,color,id,pack,totqty,holdqty,allocqty,availqty,pickedqty,00000000 as spqty,00000 as totalqty, space(8) as office,space(20) as oldstyle,;
			space(25) as oldcolor,space(10) as oldsize,space(25) as desc, space(25) as custsku, space(10) as invtype ;
			from inventory  into cursor finaloutput2 readwrite

			select finaloutput2
			replace all office with "    C   "
			replace all invtype with "PREPACK" for units = .f.
			replace all invtype with "UNITPICK" for units = .t.
			replace all totalqty with holdqty+availqty+pickedqty
		endif

		if lnoffice = 1
			select space(10) as nanjingacct, accountid,units,style,color,id,pack,totqty,holdqty,allocqty,availqty,pickedqty,00000000 as spqty,00000 as totalqty,space(8) as office,space(20) as oldstyle,;
			space(25) as oldcolor,space(10) as oldsize,space(25) as desc, space(25) as custsku, space(10) as invtype ;
			from inventory into cursor finaloutput1 readwrite

			select finaloutput1
			replace all office with "   N    "
			replace all invtype with "PREPACK" for units = .f.
			replace all invtype with "UNITPICK" for units = .t.
			replace all totalqty with holdqty+availqty+pickedqty
		endif

		use in project

		if used("inven")
			use in inven
		endif

	next && lnoffice

*	select finaloutput2
*	copy to c:\tempfox\fo2.dbf

*	select finaloutput1
*	append from c:\tempfox\fo2.dbf


	select finaloutput2
	replace all nanjingacct with "1"
	goto top
	replace all nanjingacct with "10" for inlist(substr(style,1,2),"MD","GL","TO","BL")

	goto top

	use f:\nanjing\nanjingstylemaster in 0

	select finaloutput2
	goto top
	scan
		select nanjingstylemaster
		locate for alltrim(newstyle) = alltrim(finaloutput2.style)
		if found()
			select finaloutput2
			replace oldstyle with nanjingstylemaster.style
			replace oldcolor with nanjingstylemaster.color
			replace oldsize  with nanjingstylemaster.size
			replace desc     with nanjingstylemaster.desc
			replace custsku  with nanjingstylemaster.vendorsku
		endif

	endscan




*******************************************************************************************************************************************************

	do case

	case acct_num = 4610
		xlsfilename = justfname(lcfilename)
		xlsfilename= "f:\ftpusers\NANJING\846out\"+xlsfilename+".xls"
*Export to &xlsfilename fields style,color,id,pack,availqty,pickedqty type xls
		assert .f. message "At copy to XLS for pickpack"

		copy fields nanjingacct,office,style,color,id,pack,availqty,pickedqty,holdqty,spqty,totalqty,custsku,desc,invtype ;
		to "f:\ftpusers\NANJING\846out\NANJING.xls" type xls

		tmessage = "Attached is the inventory file for Nanjing goods.............."+chr(13)+chr(13)+;
		"This email is generated via an automated process"+chr(13)+"For changes in content or distribution please contact: Paul Gaidis @ 732-750-9000x143 "
		tsubject= "Nanjing LA & NJ Inventory Sent: " +ttoc(datetime())

	endcase

	tsendto = 'Dyoung@fmiint.com'
	*tsendto = 'jason@nanjingusa.com,cary@nanjingusa.com'
* Changed to distrobution list per ticket from Jason Vickery. -CL 3/13/18
	tsendto = "WH-Inventory@nanjingusa.com"

	tattach ="f:\ftpusers\NANJING\846out\NANJING.xls"

	if !testing
		do form m:\dev\frm\dartmail2 with tsendto,tfrom,tsubject,tcc,tattach,tmessage,"A"
	endif

*!*	catch to oerr
*!*		set step on
*!*		pterror = .t.
*!*		tsubject = "846 Inventory Update Error ("+transform(oerr.errorno)+")at "+ttoc(datetime())
*!*		tattach  = ""
*!*		tsendto  = "pgaidis@fmiint.com,joe.bianchi@tollgroup.com"
*!*		tfrom    ="TGF WMS EDI System Operations <transload-ops@fmiint.com>"
*!*		tcc =""
*!*		tmessage = "Error processing an 846 Update "+chr(13)
*!*		tmessage =tmessage+ "Try/CATCH Exeception Message:"+chr(13)+;
*!*		[  Error: ] + str(oerr.errorno) +chr(13)+;
*!*		[  LineNo: ] + str(oerr.lineno) +chr(13)+;
*!*		[  Message: ] + oerr.message +chr(13)+;
*!*		[  Procedure: ] + oerr.procedure +chr(13)+;
*!*		[  Details: ] + oerr.details +chr(13)+;
*!*		[  StackLevel: ] + str(oerr.stacklevel) +chr(13)+;
*!*		[  LineContents: ] + oerr.linecontents+chr(13)+;
*!*		[  UserValue: ] + oerr.uservalue+chr(13)
*!*		do form dartmail2 with tsendto,tfrom,tsubject,tcc,tattach,tmessage,"A"

*!*	endtry

schedupdate()