*!* Cross-checks CROSSDOCK ASN transmissions

CLOSE DATABASES ALL
CLEAR
DO m:\dev\prg\_setvars
PUBLIC cErrList,cErrHdr,cEDIFile,cMissBOL,cFoundList,dDate

STORE "BOLs with Errorflag:" TO cErrHdr
cErrList = ""
cEDIFile = ""
cMissBOL = ""
cFoundList = ""
dDate = DATE()

SELECT 0
CREATE CURSOR intake1 (field1 c(254))

SELECT 0
CREATE CURSOR output1 (field1 c(100))

SELECT 0
CREATE CURSOR ASN_list (filename c(50))

IF !USED('edi_trigger')
	cEDIFolder = "F:\3PL\DATA\"
	USE (cEDIFolder+"edi_trigger") IN 0 ALIAS edi_trigger SHARED
ENDIF
SELECT edi_trigger
SET FILTER TO accountid = 5687

xReturn = "XXX"
DO m:\dev\prg\wf_alt WITH "C",5687
cUseFolder = UPPER(xReturn)

USE (cUseFolder+"OUTSHIP") IN 0 ALIAS outship

SELECT bol_no ;
	FROM outship ;
	WHERE del_date = dDate ;
	AND accountid = 5687 ;
	GROUP BY 1 ;
	INTO CURSOR temp1

SELECT temp1
LOCATE
BROWSE

SCAN
	cBOL = ALLTRIM(temp1.bol_no)
	SELECT edi_trigger
	IF SEEK(cBOL)
		IF errorflag
			cErrList = IIF(EMPTY(cErrList),cBOL,cErrList+CHR(13)+cBOL)
		ELSE
			INSERT INTO ASN_list (filename) VALUES (JUSTFNAME(edi_trigger.file945))
		ENDIF
	ELSE
		WAIT WINDOW "BOL "+cBOL+" not found in EDI_TRIGGER"
		cMissBOL = IIF(EMPTY(cMissBOL),cBOL,cMissBOL+CHR(13)+cBOL)
	ENDIF
ENDSCAN

SELECT ASN_list
LOCATE
IF !EOF()
*	BROWSE TIMEOUT 5
ELSE
	closeout()
	RETURN
ENDIF
?IIF(EMPTY(cErrList),"NO ERRORS",cErrList)
*WAIT "" TIMEOUT 1
?IIF(EMPTY(cMissBOL),"NO EMPTY BOLs",cMissBOL)
WAIT "" TIMEOUT 1
CLEAR

*ASSERT .F.

CD F:\FTPlogs
ADIR(ary1,"*.txt")
len1 = ALEN(ary1,1)
FOR qq = 1 TO len1
	IF ary1[qq,3] = DATE()
		cFTPFname = ALLTRIM(ary1[qq,1])
		EXIT
	ENDIF
ENDFOR

WAIT WINDOW "Most recent FTP File: "+cFTPFname TIMEOUT 1

STORE ("F:\FTPLogs\"+cFTPFname) TO cFTPFile
SELECT intake1
APPEND FROM &cFTPFile TYPE SDF
LOCATE
*BROWSE TIMEOUT 5
CLEAR

SELECT ASN_list
SCAN
	cFile945 = ALLTRIM(ASN_list.filename)
	SELECT intake1
	LOCATE FOR cFile945$intake1.field1
	IF FOUND()
		cFoundStr = cFile945+" - ASN OK"
	ELSE
		cFoundStr = cFile945+" - NO ASN RETRIEVED"
	ENDIF
	INSERT INTO output1 (field1) VALUES (cFoundStr)
ENDSCAN

SELECT output1
LOCATE
BROWSE
closeout()
RETURN

******************
PROCEDURE closeout
******************
	CLOSE DATABASES ALL
	RELEASE ary1
ENDPROC
