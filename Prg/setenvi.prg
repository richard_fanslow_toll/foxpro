set enginebehavior 70

set bell off
set cent off
set century to 19 rollover 80
set cpdialog off
set date to american
set delete on
set escape off
set exclusive off
set heading off
set help off
set intensity on
set notify off
set safety off
set status off
set status bar off
set talk off
set typeahead to 20
set unique off

set multilocks on
