 **This script will copy tables for weekly inventory sync
utilsetup("MJ_INV_SYNC_TABLE_COPY")
Set Exclusive Off
Set Deleted On
Set Talk Off
Set Safety Off 
WITH _SCREEN	
	.AUTOCENTER = .T.
	.WINDOWSTATE = 0
	.BORDERSTYLE = 1
	.WIDTH = 320
	.HEIGHT = 210
	.TOP = 290
	.LEFT = 110
	.CLOSABLE = .T.
	.MAXBUTTON = .t.
	.MINBUTTON = .T.
	.SCROLLBARS = 0
	.CAPTION = "MJ_INV_SYNC_TABLE_COPY"
ENDWITH	


*!*	If !Used("inwolog")
*!*	  use f:\whj\whdata\inwolog In 0
*!*	ENDIF
*!*	COPY TO F:\whj\whdata\snapshot\inwolog.dbf
*!*	USE IN inwolog

*!*	If !Used("pl")
*!*	  use f:\whj\whdata\pl In 0
*!*	ENDIF
*!*	COPY TO F:\whj\whdata\snapshot\pl.dbf
*!*	USE IN pl

*!*	If !Used("indet")
*!*	  use f:\whj\whdata\indet In 0
*!*	ENDIF
*!*	COPY TO F:\whj\whdata\snapshot\indet.dbf
*!*	USE IN indet

*!*	If !Used("outship")
*!*	  use f:\whj\whdata\outship In 0
*!*	ENDIF
*!*	COPY TO F:\whj\whdata\snapshot\outship.dbf
*!*	USE IN outship

*!*	If !Used("outdet")
*!*	  use f:\whj\whdata\outdet In 0
*!*	ENDIF
*!*	COPY TO F:\whj\whdata\snapshot\outdet.dbf
*!*	USE IN outdet

goffice="J"

xsqlexec("select * from inven where mod='"+goffice+"'",,,"wh")
COPY TO F:\whj\whdata\snapshot\sinven.dbf
USE IN inven

xsqlexec("select * from invenloc where mod='"+goffice+"'",,,"wh")
COPY TO F:\whj\whdata\snapshot\sinvenloc.dbf
USE IN invenloc

xsqlexec("select * from cmtrans",,,"wh")

COPY TO F:\whj\whdata\snapshot\scmtrans.dbf
USE IN cmtrans

If !Used("mj_ra_reconcile")
  use f:\whj\whdata\mj_ra_reconcile IN 0
ENDIF
COPY TO F:\whj\whdata\snapshot\smj_ra_reconcile.dbf
USE IN mj_ra_reconcile

* turned off dy 6/14/17

*!*	If !Used("outship")
*!*	  use f:\whj\whdata\outship In 0
*!*	ENDIF
*!*	COPY TO F:\whj\whdata\snapshot\soutship.dbf
*!*	USE IN outship

*!*	If !Used("outdet")
*!*	  use f:\whj\whdata\outdet In 0
*!*	ENDIF
*!*	COPY TO F:\whj\whdata\snapshot\soutdet.dbf
*!*	USE IN outdet

*!*	If !Used("adj")
*!*	  use f:\whj\whdata\adj In 0
*!*	ENDIF
*!*	COPY TO F:\whj\whdata\snapshot\adj.dbf
*!*	USE IN adj


  tsendto = "tmarg@fmiint.com"
  tattach = "" 
  tcc =""
  tFrom ="Toll EDI System Operations <fmi-transload-ops@fmiint.com>"
  tmessage = "MJ inventory sync table copy complete  "+Ttoc(Datetime())
  tSubject = "MJ inventory sync table copy complete"

  Do Form m:\dev\frm\dartmail2 With tsendto,tFrom,tSubject, tcc,tattach,tmessage  ,"A"   




Close Data All
schedupdate()
_Screen.Caption=gscreencaption