Parameter whichlabel, xprinter, xstartdt, xlastcarton
************************************************************************************

if !empty(xprinter)
	lcPtr=xprinter
endif
xstartdt=iif(empty(xstartdt),{},xstartdt)

** not sure if we need there but it wont hurt
!net Use lpt1 /Delete
!net Use lpt1 &lcPtr

** point to a wave label record of maybe a query to a cursor xdata

lcStr =""
lcStr2=""

lcquery ="select * From cartons  Where ucc = '"+Alltrim(xdata.uccnumber)+Alltrim(xdata.chkdigit)+"'"
xsqlexec(lcQuery,"xctns",,"pickpack_sql5")
*useca("cartons","PICKPACK_SQL5",,,lcQuery)
Select upc,qty,totqty,.f. as isShort From xctns Into Cursor xctns readwrite
llShort = .f.

Select xctns
Scan
 If totqty != qty
   replace isShort with .t.
 endif    
 Endscan

Select xdata
Scan

  If Inlist(whichlabel,"UCC","BOTH")
    lcStr = Filetostr("f:\wh\zpltemplates\billabong_generic_zpl.txt")
    lcStr = Strtran(lcStr,"<SHIPFROM>",    Alltrim(xdata.acct_name))
    lcStr = Strtran(lcStr,"<SHIPFROMADDR>",Alltrim(xdata.sfromaddr1))
    lcStr = Strtran(lcStr,"<SHIPFROMCSZ>", Alltrim(xdata.sfromcsz))
    lcStr = Strtran(lcStr,"<SHIPTO>",      Alltrim(xdata.shipto))
    lcStr = Strtran(lcStr,"<SHIPTOADDR1>", Alltrim(xdata.staddr1))
    If!Empty(xdata.staddr2)
      lcStr = Strtran(lcStr,"<SHIPTOADDR2>", Alltrim(xdata.staddr2))
    Else
      lcStr = Strtran(lcStr,"<SHIPTOADDR2>", "")
    Endif
    lcStr = Strtran(lcStr,"<SHIPTOCSZ>",   Alltrim(xdata.stcsz))
    lcStr = Strtran(lcStr,"(420) 12345",   "(420) "+Alltrim(xdata.stzip))
    lcStr = Strtran(lcStr,"42012345",      "420"+Substr(Alltrim(xdata.stzip),1,5))
    lcStr = Strtran(lcStr,"<PICKTICKET>",  " PT: "+Alltrim(xdata.pt))
    lcStr = Strtran(lcStr,"<PONUM>",       " PO: "+Alltrim(xdata.cust_po))
    lcStr = Strtran(lcStr,"<XXXXSHIPVIAXXXX>",     " " )
    lcStr = Strtran(lcStr,"<CARTONOF>",    Alltrim(xdata.cartonof))
    lcStr = Strtran(lcStr,"<WONUM>",       "WO#:"+Alltrim(Transform(xdata.wo_num)))
    lcStr = Strtran(lcStr,"80000510200000218128",Alltrim(xdata.uccnumber))
    lcStr = Strtran(lcStr,"<UCCNUMBER>","("+Substr(xdata.uccnumber,1,2)+") "+Substr(xdata.uccnumber,3,1)+" "+Substr(xdata.uccnumber,4,7)+" "+Substr(xdata.uccnumber,12)+" "+Alltrim(chkdigit))
  Endif

** ^FT53,1198^BCN,294,N,N,Y^FD>;><80000510200000218128^FS

  If Inlist(whichlabel,"DETAIL","BOTH")

    lcStr2 = Filetostr("f:\wh\zpltemplates\billabong_detail_zpl_REV2.txt")

    If xstartdt > Date()
      lcStr2= Strtran(lcStr2,"<SHIPDATE>",   "   "+Transform(Month(xstartdt))+"/"+Padl(Transform(Day(xstartdt)),2,"0"))
    Else
      lcStr2= Strtran(lcStr2,"<SHIPDATE>",   "   ")
    endif

    If xlastcarton = .T. 
      lcStr2= Strtran(lcStr2,"<PACKINGLISTENCLOSED>",   "PACKING LIST ENCLOSED")
    Else
      lcStr2= Strtran(lcStr2,"<PACKINGLISTENCLOSED>",   "   ")
    endif

    lcStr2 = Strtran(lcStr2,"<CARTONOF>" ,  Alltrim(xdata.cartonof) )
    lcStr2 = Strtran(lcStr2,"<WONUM>",     "WO#:"+Alltrim(Transform(xdata.wo_num)))
    lcStr2 = Strtran(lcStr2,"<PICKTICKET>"," PT: "+Alltrim(xdata.pt))
    lcStr2 = Strtran(lcStr2,"<SWC>","SWC:"+Alltrim(xdata.swc))
    lcStr2 = Strtran(lcStr2,"<XXXXXXSHIPVIAXXXXXX>",   Alltrim(xdata.ship_via))

    lcStr2 = Strtran(lcStr2,"<RUNID>",  "SWC:"+Alltrim(Transform(xdata.runid)))
    lcStr2 = Strtran(lcStr2,"<PRINTDATE>",  " PRINTED: "+Alltrim(Ttoc(Datetime())))
    lcStr2 = Strtran(lcStr2,"80000510200000218128",Alltrim(xdata.uccnumber))
    lcStr2 = Strtran(lcStr2,"<UCCNUMBER>","("+Substr(xdata.uccnumber,1,2)+") "+Substr(xdata.uccnumber,3,1)+" "+Substr(xdata.uccnumber,4,7)+" "+Substr(xdata.uccnumber,12)+" "+Alltrim(chkdigit))

    lcStr2= Strtran(lcStr2,"<H1>",   Alltrim(xdata.h1))

   Do writeNline With "<N1>",  Alltrim(xdata.n1)
   Do writeNline With "<N2>",  Alltrim(xdata.n2)
   Do writeNline With "<N3>",  Alltrim(xdata.n3)
   Do writeNline With "<N4>",  Alltrim(xdata.n4)
   Do writeNline With "<N5>",  Alltrim(xdata.n5)
   Do writeNline With "<N6>",  Alltrim(xdata.n6)
   Do writeNline With "<N7>",  Alltrim(xdata.n7)
   Do writeNline With "<N8>",  Alltrim(xdata.n8)
   Do writeNline With "<N9>",  Alltrim(xdata.n9)
   Do writeNline With "<N10>",  Alltrim(xdata.n10)
   Do writeNline With "<N11>",  Alltrim(xdata.n11)
   Do writeNline With "<N12>",  Alltrim(xdata.n12)
   Do writeNline With "<N13>",  Alltrim(xdata.n13)
   Do writeNline With "<N14>",  Alltrim(xdata.n14)
   Do writeNline With "<N15>",  Alltrim(xdata.n15)
   Do writeNline With "<N16>",  Alltrim(xdata.n16)
   Do writeNline With "<N17>",  Alltrim(xdata.n17)
   Do writeNline With "<N18>",  Alltrim(xdata.n18)
   Do writeNline With "<N19>",  Alltrim(xdata.n19)
   Do writeNline With "<N20>",  Alltrim(xdata.n20)
   Do writeNline With "<N21>",  Alltrim(xdata.n21)
   Do writeNline With "<N22>",  Alltrim(xdata.n22)
   Do writeNline With "<N23>",  Alltrim(xdata.n23)
   Do writeNline With "<N24>",  Alltrim(xdata.n24)
   Do writeNline With "<N25>",  Alltrim(xdata.n25)
   Do writeNline With "<N26>",  Alltrim(xdata.n26)
 Endif

  lcOutStr = lcStr+lcStr2

  Strtofile(lcOutStr,"h:\fox\temp.txt")
  lcfile = "h:\fox\temp.txt"
  !Type &lcfile > lpt1
Endscan

Use In xdata
***********************************************************************************
** ok replace the last couple of characters in the line data if this upc was shorted
*************************************************************************************8
Procedure WriteNline
  Parameters mask,linedata
   
  If Empty(linedata)
    lcStr2= Strtran(lcStr2,mask, "")
    return 
  Endif 
 
  Select xctns
  Scan
    If Alltrim(xctns.upc)$linedata And xctns.qty!= xctns.totqty
      linedata = Substr(linedata,1,Len(Alltrim(linedata))-3)
      linedata = linedata+Padl(Transform(xctns.totqty),3," ")
      lcStr2 = Strtran(lcStr2,mask,linedata)
      return
    endif  
  Endscan 

  lcStr2 = Strtran(lcStr2,mask,linedata)

Endproc  
**************************************************************************************