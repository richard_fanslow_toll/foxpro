* amazon_315_xml by MB, based on anf_214.xml by PG
*
* Create xml 315 status edi files for either AVAIL or OUTGATE
*
* called from timer in edi_jobhandler_aux-edi.scx, which polls F:\3PL\DATA\EDI_TRIGGER.DBF
*
* note: file created will be copied to ICON outgate folder by scheduled task OUTGATE_TO_ICON
*
* Amazon accountid = 6703

LPARAMETERS tnwo_num, tccode

LOCAL lltestmode, lcoutputfolder, lccontainer, lcbol, lceventtime, lcdatefmt, lcheader, lcfilename, lccentury


lltestmode = .F.
*llTestMode = .T.

lcfilename = ''

IF lltestmode THEN
	lcoutputfolder = 'F:\FTPUSERS\AMAZON\TESTTGF315\'
ELSE
	lcoutputfolder = 'F:\FTPUSERS\AMAZON\TGF315\'
ENDIF

* mark in edi_trigger as processed so this proc is not called repeatedly if it errors out
*replace edi_trigger.processed with .t. in edi_trigger

SET STEP ON

IF !USED("wolog")
	USE F:\wo\wodata\wolog IN 0 SHARED
ENDIF

*** tcCode should be "OUTGATE" or "AVAIL"
DO CASE
	CASE tccode = "OUTGATE"
		IF !USED("tdaily")
			USE F:\wo\wodata\tdaily IN 0 SHARED
		ENDIF
	CASE tccode = "AVAIL"
ENDCASE

SELECT wolog
LOCATE FOR tnwo_num = wolog.wo_num
IF !FOUND()

ELSE
	lccontainer = wolog.CONTAINER
	*lcBol = wolog.brokref  && per Bill L., this is house bol # - he wants Master BOL #. 10/25/2017 MB
	lcbol = wolog.mblnum
ENDIF

*SELECT tdaily

lceventtime = DATETIME()

DO CASE
	CASE tccode = "OUTGATE"
		SELECT tdaily
		LOCATE FOR wo_num = tnwo_num AND action = "PU"
		IF FOUND()
			lceventtime = tdaily.confirmdt
		ELSE
			lceventtime = DATETIME()
		ENDIF
	CASE tccode = "AVAIL"
		xsqlexec("select avail, remarks from availlog where wo_num="+TRANSFORM(tnwo_num)+" and avail#{}",,,"wo")
		LOCATE FOR 'AVAILABLE' $ UPPER(remarks)
		IF FOUND()
			lceventtime = availlog.AVAIL
		ELSE
			lceventtime = DATETIME()
		ENDIF
ENDCASE

lcdatefmt = SET('DATE')
lccentury = SET('CENTURY')

SET DATE ymd
SET CENTURY ON

lceventtime = STRTRAN(TTOC(lceventtime ),"/","-")
lceventtime = STRTRAN(lceventtime," ","T")
lceventtime = STRTRAN(lceventtime,"TPM","")
lceventtime = ALLTRIM(STRTRAN(lceventtime,"TAM",""))

lcheader = FILETOSTR("F:\AMAZON\DATA\AMAZON_315_TEMPLATE.TXT")

lcheader = STRTRAN(lcheader,"<EVENTTIME>",lceventtime)
lcheader = STRTRAN(lcheader,"<CONTAINER>",ALLTRIM(lccontainer))
lcheader = STRTRAN(lcheader,"<BOLNUMBER>",ALLTRIM(lcbol))

**<CONTAINERRETURNTIME>

DO CASE
	CASE tccode = "OUTGATE"
		lcheader = STRTRAN(lcheader,"<EVENTTYPE>","GOU")
		lcheader = STRTRAN(lcheader,"<JOBCONTAINERTYPE>","JobContainer.JC_FCLWharfGateOut")
		lcheader = STRTRAN(lcheader,"<CONTAINERRETURNTIME>",lceventtime)
		lceventcode="GOU"
	CASE tccode = "AVAIL"
		lcheader = STRTRAN(lcheader,"<EVENTTYPE>","CAV")
		lcheader = STRTRAN(lcheader,"<JOBCONTAINERTYPE>","JobContainer.JC_FCLAvailable")
		lcheader = STRTRAN(lcheader,"<CONTAINERRETURNTIME>",lceventtime )
		lceventcode="CAV"

ENDCASE

lcfilestamp = TTOC(DATETIME())
lcfilestamp = STRTRAN(lcfilestamp,"/","")
lcfilestamp = STRTRAN(lcfilestamp,":","")
lcfilestamp = STRTRAN(lcfilestamp," ","")

&&TGFSCS_MRKU0065216_GOU_20160922112543.xml

lcfilename = lcoutputfolder + "TGFSCS_"+ALLTRIM(lccontainer)+"_"+ALLTRIM(tccode)+"_"+lcfilestamp+".xml"

STRTOFILE(lcheader,lcfilename)

SET DATE (lcdatefmt)
SET CENTURY &lccentury

RETURN lcfilename
