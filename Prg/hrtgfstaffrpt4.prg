* HRTGFSTAFFRPT4.PRG
* produce FTE report or do daily FTE export to Qlikview, depending on passed parameter.
* rewritten to base headcount on Kronos calendar month data
* instead of ADP pay date data.

**************************************************************************************************************************
**  new version 8/19/2013 which uses f:\util\adpreports\data\depttrans.dbf to look up TGF Dept Descriptions by dept code,
** instead of using the custom fields in the HR employee table.
** Per Ken's request (he gave me the translation table). 8/2013
** tcType = 'R' for this
**************************************************************************************************************************

* Build EXE as F:\UTIL\HR\TGFSTAFFRPT\HRTGFSTAFFRPT.exe
LPARAMETERS tcType
LOCAL loTGFStaffingReport, ldToday, i, lcType
PRIVATE oExcel, oWorkbook

runack("HRTGFSTAFFRPT")

utilsetup("HRTGFSTAFFRPT")

IF EMPTY( tcType ) THEN
	lcType = "Q"
ELSE
	lcType = UPPER(ALLTRIM( tcType ))
ENDIF

IF NOT INLIST( lcType,"Q","K","R") THEN
	WAIT WINDOW TIMEOUT 60 "Invalid Type parameter!!"
	RETURN
ENDIF

* this is needed for anything we do...
oExcel = CREATEOBJECT("excel.application")
oExcel.displayalerts = .F.
oExcel.VISIBLE = .F.
oWorkbook = .F.

loTGFStaffingReport = CREATEOBJECT('TGFStaffingReport')

DO CASE
	CASE lcType = "Q"
		* mode Q = do 2 daily exports - 1 tab each - to Qlikview
		* one is MTD current month; second is entire prior month (if we are in first week of a month)

		ldToday = DATE()
		*ldToday = {^2012-07-02}

		IF INLIST(DAY(ldToday),1,2,3,4,5,6,7) THEN
			* process prior month
			loTGFStaffingReport.MAIN('ALL', ldToday, lcType)
		ENDIF

		* always process current month (i.e. MTD)
		ldToday = GOMONTH(ldToday,1)
		loTGFStaffingReport.MAIN('ALL', ldToday, lcType)

	CASE lcType = "K"
		* mode K (for Ken Kausner) = run his FTE report - 3 tabs - against prior month
		*ldToday = DATE()  && this will cause Main() to process prior month
		*!*	********************************************
		*!*	* ACTIVATE TO FORCE DIFFERENT MONTH
	ldToday = {^2014-08-01}
		*!*	********************************************
		loTGFStaffingReport.MAIN('FF', ldToday, lcType)
		loTGFStaffingReport.MAIN('SCS', ldToday, lcType)
		loTGFStaffingReport.MAIN('ADMIN', ldToday, lcType)
		loTGFStaffingReport.MAIN('ALL', ldToday, lcType)

	CASE lcType = "R"
		ldToday = DATE()  && this will cause Main() to process prior month
		*!*	********************************************
		*!*	* ACTIVATE TO FORCE DIFFERENT MONTH
		*!*	ldToday = {^2013-08-01}
		*!*	********************************************
		loTGFStaffingReport.MAIN('ALL', ldToday, lcType)

	OTHERWISE
		* nothing
ENDCASE

schedupdate()

CLOSE DATABASES ALL
RETURN


*!*	********************************************
*!*	* ACTIVATE TO FORCE DIFFERENT MONTHS
*!*	ldToday = {^2012-07-01}
*!*	********************************************

* DON'T CHANGE THE ORDER OF THESE: 'FF' MUST BE FIRST, 'ALL' MUST BE LAST!
*loTGFStaffingReport.MAIN('FF', ldToday)
*loTGFStaffingReport.MAIN('SCS', ldToday)
*loTGFStaffingReport.MAIN('ALL', ldToday)


*!*	** activate below for multiple months...
*!*	ldToday = {^2010-01-01}
*!*	FOR i = 1 TO 30
*!*		ldToday = GOMONTH(ldToday,1)
*!*		loTGFStaffingReport.MAIN('ALL', ldToday)
*!*	NEXT



#DEFINE LOGIT 1
#DEFINE WAITIT 2
#DEFINE NOWAITIT 4
#DEFINE SENDIT 8
#DEFINE CRLF CHR(13) + CHR(10)
#DEFINE RETURN_DATA_MANDATORY .F.
#DEFINE RETURN_DATA_NOT_MANDATORY .T.
#DEFINE CSVFORMAT 6
#DEFINE WRITE_ONLY_UNBUFFERED 11
#DEFINE COMMA ","
#DEFINE xlEdgeBottom 9
#DEFINE xlContinuous 1
#DEFINE xlMedium -4138
#DEFINE xlFONT_RED -16776961

DEFINE CLASS TGFStaffingReport AS CUSTOM

	cProcessName = 'TGFStaffingReport'

	lTestMode = .T.  && .F. FOR NORMAL OPERATION, .T. TO SUPPRESS ALL BUT DEVELOPER EMAILS AND CHANGE INPUT TABLE, ETC.
	lDoBIExport = .T.

	dToday = DATE()

	lAutoYield = .T.

	cStartTime = TTOC(DATETIME())

	* report properties
	cBadData = ''
	*nFTEHours_Hourly = 0
	*nFTEHours_Salaried = 0

	*nFTEHours = 173.33
	nFTEHours = 160.00  && Per Ken Kausner 3/4/13 MB

	nMilesToHoursFactor = 0.0167  && 1 / 60
	nOOMileage = 0.0
	nOOHeadCnt = 0
	nTempMarkUpFactor = 1.275

	* FILENAME PROPERTIES
	cDetailFileForKen = ''

	* table properties
	cDeptlookupTable = 'F:\UTIL\ADPREPORTS\DATA\DEPTTRANS.DBF'

	*** usually set these to .F.
	lShowHours = .F.
	lSpecialAllWhere = .F.

	* connection properties
	nSQLHandle = 0

	* wait window properties
	nWaitWindowTimeout = 2
	lWaitWindowIsOn = .T.

	* logfile properties
	lLoggingIsOn = .T.
	cLogFile = 'F:\UTIL\HR\TGFSTAFFRPT\LOGFILES\TGFSTAFF_Report_log.txt'

	* INTERNAL email properties
	lSendInternalEmailIsOn = .T.
	cFrom = 'Mark.Bennett@Tollgroup.com'
	cSendTo = 'mbennett@fmiint.com, Mark.Bennett@Tollgroup.com'
	cCC = ''
	cSubject = 'FTE Info to Qlikview for ' + DTOC(DATE())
	cAttach = ''
	cBodyText = ''

	cCOMPUTERNAME = ''
	cUSERNAME = ''


	FUNCTION INIT
		IF NOT DODEFAULT()
			RETURN .F.
		ENDIF
		WITH THIS
			*SET RESOURCE OFF
			CLOSE DATA
			SET CENTURY ON
			SET DATE AMERICAN
			SET HOURS TO 24
			SET ANSI ON
			SET TALK OFF
			SET DELETED ON
			SET CONSOLE OFF
			SET EXCLUSIVE OFF
			SET SAFETY OFF
			SET EXACT OFF
			SET STATUS BAR ON
			SET SYSMENU OFF
			SET ENGINEBEHAVIOR 70
			_VFP.AUTOYIELD = .lAutoYield
			.cCOMPUTERNAME = GETENV("COMPUTERNAME")
			.cUSERNAME = GETENV("USERNAME")
			IF .lTestMode THEN
				.cLogFile = 'F:\UTIL\HR\TGFSTAFFRPT\LOGFILES\TGFSTAFF_Report_log_TESTMODE.txt'
				.cSendTo = 'mbennett@fmiint.com'
			ENDIF
			.lLoggingIsOn = .lLoggingIsOn AND NOT EMPTY(.cLogFile)
			IF .lLoggingIsOn THEN
				SET ALTERNATE TO (.cLogFile) ADDITIVE
				SET ALTERNATE ON
			ENDIF
			SET PROCEDURE TO VALIDATIONS ADDITIVE
		ENDWITH
	ENDFUNC


	FUNCTION DESTROY
		WITH THIS
			IF .lLoggingIsOn  THEN
				SET ALTERNATE OFF
				SET ALTERNATE TO
			ENDIF
		ENDWITH
		DODEFAULT()
	ENDFUNC


	FUNCTION MAIN
		LPARAMETERS tcMode, tdToday, tcType
		WITH THIS
			LOCAL lnNumberOfErrors, lcSQL, lcSQL2, lcSQL3, lcSQL4, lcSQL5, lcSQL6, loError, ldToday, lnFTEHours
			LOCAL lcFiletoSaveAs, lcSpreadsheetTemplate, lcGrandTotalRow, lcWhere
			LOCAL lnDay, ldFromDate, ldToDate, lcSQLFromDate, lcSQLToDate, lcADPFromDate, lcADPToDate
			LOCAL lcSQLKronos, lcSuffix, lcDivWhere, lcDateSuffix
			LOCAL i, oWorksheet, lnStartRow, lcStartRow, lnRow, lcRow, lnTempMarkUpFactor
			LOCAL lnMgmtStartRow, lcMgmtStartRow, lnMgmtEndRow, lcMgmtEndRow, lnPreCasualRow, lcKronosDivWhere
			LOCAL lnNumHourlyPaydates, lnNumSalariedPaydates, lcBIFileName, lcSQLKronosHourly, lcSQLKronosTMP
			LOCAL lcFileName, lcDept, lcHCFTEEntityDetailFile, lcHCFTEEntitySummaryFile, lcHCFTEEntityMultDeptsFile

			TRY
				lnNumberOfErrors = 0

				IF NOT USED('DEPTLOOKUPTABLE') THEN
					USE (.cDeptlookupTable) IN 0 ALIAS DEPTLOOKUPTABLE
				ENDIF

				lnFTEHours = .nFTEHours

				lnTempMarkUpFactor = .nTempMarkUpFactor

				.cBodyText = ''
				.cBadData = ''

				.dToday = tdToday
				ldToday = tdToday
				lnDay = DAY(tdToday)

				.TrackProgress("FTE Info to Qlikview process started....", LOGIT+SENDIT+NOWAITIT)
				.TrackProgress('COMPUTERNAME = ' + .cCOMPUTERNAME, LOGIT+SENDIT)
				.TrackProgress('USERNAME = ' + .cUSERNAME, LOGIT+SENDIT)
				.TrackProgress('tcMode = ' + tcMode, LOGIT+SENDIT)
				.TrackProgress('tcType = ' + tcType, LOGIT+SENDIT)
				.TrackProgress('tdToday = ' + TRANSFORM(tdToday), LOGIT+SENDIT)
				.TrackProgress('PROJECT = HRTGFSTAFFRPT', LOGIT+SENDIT)
				IF .lTestMode THEN
					.TrackProgress('                        TEST MODE                        ', LOGIT+SENDIT)
				ENDIF

				DO CASE
					CASE tcType = "Q"
						lDoBIExport = .T.
					CASE tcType = "K"
						lDoBIExport = .F.
					CASE tcType = "R"
						lDoBIExport = .F.
					OTHERWISE
						* nothing
				ENDCASE

				lnStartRow = 3
				lcStartRow = ALLTRIM(STR(lnStartRow))

				DO CASE
					CASE tcMode = "FF"
						lcDivWhere = "AND {fn LEFT(CHECKVIEWHOMEDEPT,2)} IN ('20','21','22','23','24','25','26','27','28','29','30','31','32','35','36','37','38','39','71','72','73','91') "
						lcKronosDivWhere =            "AND D.LABORLEV2NM IN ('20','21','22','23','24','25','26','27','28','29','30','31','32','35','36','37','38','39','71','72','73','91') "
						lcSuffix = "_FF"
					CASE tcMode = "SCS"
						*lcDivWhere = "AND {fn LEFT(CHECKVIEWHOMEDEPT,2)} NOT IN ('20','21','22','23','24','25','26','27','28','29','30','31','35','36','37','38','39','71','72','73','91','90','92') "
						*lcKronosDivWhere =            "AND D.LABORLEV2NM NOT IN ('20','21','22','23','24','25','26','27','28','29','30','31','35','36','37','38','39','71','72','73','91','90','92') "
						lcDivWhere = "AND {fn LEFT(CHECKVIEWHOMEDEPT,2)} IN ('01','02','03','04','05','06','07','14','49','50','52','53','54','55','56','57','58','59','60','61','62') "
						lcKronosDivWhere =            "AND D.LABORLEV2NM IN ('01','02','03','04','05','06','07','14','49','50','52','53','54','55','56','57','58','59','60','61','62') "
						lcSuffix = "_NON_FF"
					CASE tcMode = "ALL"
						lcDivWhere = ""
						lcKronosDivWhere = ""
						*!*							IF .lSpecialAllWhere THEN
						*!*								lcDivWhere = "AND {fn LEFT(CHECKVIEWHOMEDEPT,2)} IN ('21','36','73') "
						*!*								lcKronosDivWhere = "AND D.LABORLEV2NM IN ('21','36','73') "
						*!*							ENDIF
						lcSuffix = "_ALL"
					CASE tcMode = "ADMIN"
						lcDivWhere = "AND {fn LEFT(CHECKVIEWHOMEDEPT,2)} IN ('90','92') "
						lcKronosDivWhere =            "AND D.LABORLEV2NM IN ('90','92') "
						lcSuffix = "_ADMIN"
					OTHERWISE
						THROW
				ENDCASE

				.TrackProgress('lcSuffix = ' + lcSuffix, LOGIT+SENDIT)
				.TrackProgress('lcDivWhere = ' + lcDivWhere, LOGIT+SENDIT)

				ldToDate = ldToday - lnDay  && this is last day of prior month
				ldFromDate = ldToDate + 1  && this is 1st day of current month
				ldFromDate = GOMONTH(ldFromDate,-1)  && this is 1st day of prior month

				lcDateSuffix = "_" + DTOS(ldFromDate) + "-" + DTOS(ldToDate)
				lcSuffix = lcSuffix + lcDateSuffix
				.cSubject = 'FTE Info to Qlikview for ' + DTOC(ldFromDate) + " - " + DTOC(ldToDate)


				lcADPFromDate = "DATE'" + TRANSFORM(YEAR(ldFromDate)) + "-" + PADL(MONTH(ldFromDate),2,'0') + "-" + PADL(DAY(ldFromDate),2,'0') + "'"
				lcADPToDate = "DATE'" + TRANSFORM(YEAR(ldToDate)) + "-" + PADL(MONTH(ldToDate),2,'0') + "-" + PADL(DAY(ldToDate),2,'0') + "'"

				lcSpreadsheetTemplate = 'F:\UTIL\HR\TGFSTAFFRPT\TEMPLATES\TGFSTAFFING_TEMPLATE4.XLS'
				lcFiletoSaveAs = 'F:\UTIL\HR\TGFSTAFFRPT\REPORTS\TGFSTAFFING' + lcDateSuffix + "_" + tcType + '.XLS'
				.TrackProgress('Creating spreadsheet: ' + lcFiletoSaveAs, LOGIT+SENDIT+NOWAITIT)


				* this block must only run once, when depends on mode...
				DO CASE
					CASE tcType = "Q"
						IF tcMode = "ALL" THEN
							* delete output file if it already exists...
							IF FILE(lcFiletoSaveAs) THEN
								DELETE FILE (lcFiletoSaveAs)
							ENDIF
							oWorkbook = oExcel.workbooks.OPEN(lcSpreadsheetTemplate)
							oWorkbook.SAVEAS(lcFiletoSaveAs)
						ENDIF
					CASE tcType = "K"
						.cSubject = 'FTE Staffing for ' + DTOC(ldFromDate) + " - " + DTOC(ldToDate)
						.cSendTo = 'Ken.Kausner@Tollgroup.com'
						.cCC = 'Mark.Bennett@Tollgroup.com'
						IF tcMode = "FF" THEN
							* delete output file if it already exists...
							IF FILE(lcFiletoSaveAs) THEN
								DELETE FILE (lcFiletoSaveAs)
							ENDIF
							oWorkbook = oExcel.workbooks.OPEN(lcSpreadsheetTemplate)
							oWorkbook.SAVEAS(lcFiletoSaveAs)
						ENDIF
					CASE tcType = "R"
						.cSubject = 'H/C & FTE by Entity Report for ' + DTOC(ldFromDate) + " - " + DTOC(ldToDate)
						.cSendTo = 'Ken.Kausner@Tollgroup.com'
						.cCC = 'Mark.Bennett@Tollgroup.com'
						* delete output file if it already exists...s
						IF FILE(lcFiletoSaveAs) THEN
							DELETE FILE (lcFiletoSaveAs)
						ENDIF
						oWorkbook = oExcel.workbooks.OPEN(lcSpreadsheetTemplate)
						oWorkbook.SAVEAS(lcFiletoSaveAs)
					OTHERWISE
						* nothing
				ENDCASE

				IF .lTestMode THEN
					.cSendTo = 'Mark.Bennett@Tollgroup.com'
					.cCC = ''
				ENDIF

				* determine current worksheet
				DO CASE
					CASE tcMode = "FF"
						oWorksheet = oWorkbook.Worksheets[1]
					CASE tcMode = "SCS"
						oWorksheet = oWorkbook.Worksheets[2]
					CASE tcMode = "ADMIN"
						oWorksheet = oWorkbook.Worksheets[3]
					CASE tcMode = "ALL"
						oWorksheet = oWorkbook.Worksheets[4]
				ENDCASE


				IF USED('CURHOURS') THEN
					USE IN CURHOURS
				ENDIF
				IF USED('CURHOURS2') THEN
					USE IN CURHOURS2
				ENDIF
				IF USED('CURHOURSPRE') THEN
					USE IN CURHOURSPRE
				ENDIF


				***************************************************

				* GATHER HOURLY & TEMP INFO FROM KRONOS

				.nSQLHandle = SQLCONNECT("WTK2","sa","kr0n1tes")

				IF .nSQLHandle > 0 THEN

					lcSQLFromDate = .GetSQLStartDateTimeString( ldFromDate )
					lcSQLToDate = .GetSQLEndDateTimeString( ldToDate )


					***** NOTE: we need to exclude 020655 from below, because they have no actual worked hours, so only Vac/sick etc.d will
					***** show up, understating their hours and fte. Then we must add them to the Salaried query later on (basically treating them as salaried).

					SET TEXTMERGE ON
					TEXT TO	lcSQLKronosHourly NOSHOW
SELECT
A.APPLYDTM,
D.LABORLEV5NM AS AGENCYNUM,
D.LABORLEV1NM AS ADP_COMP,
D.LABORLEV2NM AS DIVISION,
D.LABORLEV3NM AS DEPT,
C.FULLNM AS NAME,
C.PERSONNUM AS FILE_NUM,
E.NAME AS PAYCODEDESC,
(CASE WHEN E.NAME NOT IN ('Regular Hours','Overtime Hours' ,'Doubltime Hours') THEN (A.DURATIONSECSQTY / 3600.00) ELSE 0.000 END) AS CODEHOURS,
(CASE WHEN E.NAME = 'Regular Hours' THEN (A.DURATIONSECSQTY / 3600.00) ELSE 0.000 END) AS REGHOURS,
(CASE WHEN E.NAME IN ('Overtime Hours' ,'Doubltime Hours') THEN (A.DURATIONSECSQTY / 3600.00) ELSE 0.000 END) AS OTHOURS,
(A.DURATIONSECSQTY / 3600.00) AS TOTHOURS,
WAGEAMT
FROM WFCTOTAL A
JOIN WTKEMPLOYEE B
ON B.EMPLOYEEID = A.EMPLOYEEID
JOIN PERSON C
ON C.PERSONID = B.PERSONID
JOIN LABORACCT D
ON D.LABORACCTID = A.LABORACCTID
JOIN PAYCODE E
ON E.PAYCODEID = A.PAYCODEID
WHERE (A.APPLYDTM >= <<lcSQLFromDate>>)
AND (A.APPLYDTM <= <<lcSQLToDate>>)
AND (D.LABORLEV1NM IN ('SXI','TMP'))
AND E.NAME NOT IN ('FMLA','Unpaid Leave','Unpaid Hours','Unpaid Suspended','Voluntary Time Off')
AND C.PERSONNUM <> '999'
AND NOT ((D.LABORLEV2NM = '02') AND (D.LABORLEV3NM = '0655'))
<<lcKronosDivWhere>>
ORDER BY 1,2,3,4,6
					ENDTEXT
					SET TEXTMERGE OFF

					IF .lTestMode THEN
						.TrackProgress('lcSQLKronosHourly = ' + lcSQLKronosHourly, LOGIT+SENDIT)
					ENDIF

					IF .ExecSQL(lcSQLKronosHourly, 'CURHOURSPRE', RETURN_DATA_MANDATORY) THEN



						*!*	SELECT CURHOURSPRE
						*!*	BROWSE
						*!*	THROW


						DO CASE

							CASE INLIST(tcType,"K","Q","R")
								* WE WANT TO DELETE ANY DIV 59 DATA AFTER 6/30/2013 - Because Underarmour took over Rialto on 7/1/2013 but kept using our clocks for a few weeks.
								SELECT CURHOURSPRE
								SCAN
									IF (TTOD(APPLYDTM) > {^2013-06-30}) AND (DIVISION = '59') THEN
										DELETE
									ENDIF
								ENDSCAN

							OTHERWISE
								* nothing
						ENDCASE


						* ROLL UP HOURS INFO
						SELECT ALLTRIM(ADP_COMP) AS ADP_COMP, ;
							NAME, ;
							INT(VAL(FILE_NUM)) AS FILE_NUM, ;
							ALLTRIM(DIVISION) + ALLTRIM(DEPT) AS HOMEDEPT, ;
							ldFromDate AS PAYDATE, ;
							SUM(CODEHOURS) AS CODEHOURS, ;
							SUM(REGHOURS) AS REGHOURS, ;
							SUM(OTHOURS) AS OTHOURS, ;
							SUM(TOTHOURS) AS TOTHOURS, ;
							SUM(WAGEAMT) AS WAGEAMT, ;
							0.00 AS FTE, ;
							1.00 AS HEADCNT, ;
							lnFTEHours AS EXPFTE, ;
							'U' AS GENDER, ;
							'U' AS MGMTTYPE, ;
							'U' AS TIMETYPE, ;
							'U  ' AS TGFDEPT, ;
							'                         ' AS DEPTDESC, ;
							'    ' AS GLACCTNO, ;
							00000000.00 AS ANNSALARY ;
							FROM CURHOURSPRE ;
							INTO CURSOR CURHOURS ;
							GROUP BY 1, 2, 3, 4 ;
							ORDER BY 1, 2, 3, 4 ;
							READWRITE

						* CONVERT KRONOS ADP COMPS TO ADP ADP COMPS
						SELECT CURHOURS
						SCAN
							REPLACE CURHOURS.ADP_COMP WITH GetNewADPCompany( ADP_COMP ) IN CURHOURS
						ENDSCAN

						*!*							* ZERO OUT WAGEAMT FOR SXI BECAUSE IT'S NOT ACCURATE
						*!*							SELECT CURHOURS
						*!*							SCAN FOR ADP_COMP = 'E87'
						*!*								REPLACE WAGEAMT WITH 0.0 IN CURHOURS
						*!*							ENDSCAN

						* POPULATE MISSING TEMP INFO
						SELECT CURHOURS
						SCAN FOR ADP_COMP = 'TMP'

							*!*								REPLACE CURHOURS.GENDER WITH 'U', ;
							*!*									CURHOURS.MGMTTYPE WITH 'B', ;
							*!*									CURHOURS.TIMETYPE WITH 'C', ;
							*!*									CURHOURS.TGFDEPT WITH 'TP' ;
							*!*									IN CURHOURS

							REPLACE CURHOURS.GENDER WITH 'U', ;
								CURHOURS.TIMETYPE WITH 'C' ;
								IN CURHOURS

						ENDSCAN

					ENDIF  && .ExecSQL

					=SQLDISCONNECT(.nSQLHandle)

				ENDIF && .nSQLHandle > 0 THEN




				***************************************************

				* now connect to ADP...

				.nSQLHandle = SQLCONNECT("PCPAYWIN","MBENNETT","VP737A")

				IF .nSQLHandle > 0 THEN

					lcWhere = ""

					SET TEXTMERGE ON
					TEXT TO	lcSQL NOSHOW
SELECT
STATUS,
COMPANYCODE AS ADP_COMP,
NAME,
FILE# AS FILE_NUM,
GENDER,
{fn ifnull({fn SUBSTRING(CUSTAREA1,1,1)},'U') AS MGMTTYPE,
{fn ifnull({fn SUBSTRING(CUSTAREA1,2,2)},'U ') AS TGFDEPT,
{fn ifnull({fn SUBSTRING(CUSTAREA2,1,1)},'F') AS TIMETYPE,
{FN IFNULL(ANNUALSALARY,00000000.00)} AS ANNSALARY
FROM REPORTS.V_EMPLOYEE
WHERE COMPANYCODE IN ('E87','E88','E89')
ORDER BY 1, 2, 3
					ENDTEXT
					SET TEXTMERGE OFF




					*!*						SET TEXTMERGE ON
					*!*						TEXT TO	lcSQL2 NOSHOW
					*!*	SELECT
					*!*	COMPANYCODE AS ADP_COMP,
					*!*	FILE# AS FILE_NUM,
					*!*	NAME,
					*!*	CHECKVIEWHOMEDEPT AS HOMEDEPT,
					*!*	CHECKVIEWPAYDATE AS PAYDATE,
					*!*	{FN IFNULL(SUM(CHECKVIEWHOURSAMT),0.00)} AS CODEHOURS,
					*!*	{FN IFNULL(MAX(CHECKVIEWREGHOURS),0.00)} AS REGHOURS,
					*!*	{FN IFNULL(MAX(CHECKVIEWOTHOURS),0.00)} AS OTHOURS
					*!*	FROM REPORTS.V_CHK_VW_HOURS
					*!*	WHERE CHECKVIEWPAYDATE >= <<lcADPFromDate>>
					*!*	AND CHECKVIEWPAYDATE <= <<lcADPToDate>>
					*!*	<<lcDivWhere>>
					*!*	GROUP BY COMPANYCODE, FILE#, NAME, CHECKVIEWHOMEDEPT, CHECKVIEWPAYDATE
					*!*	ORDER BY 1,2,4
					*!*						ENDTEXT
					*!*						SET TEXTMERGE OFF



*!*						SET TEXTMERGE ON
*!*						TEXT TO	lcSQL3 NOSHOW
*!*	SELECT
*!*	DISTINCT
*!*	COMPANYCODE AS ADP_COMP,
*!*	SOCIALSECURITY# AS SS_NUM,
*!*	CHECKVIEWPAYDATE AS PAYDATE
*!*	FROM REPORTS.V_CHK_VW_INFO
*!*	WHERE CHECKVIEWPAYDATE >= <<lcADPFromDate>>
*!*	AND CHECKVIEWPAYDATE <= <<lcADPToDate>>
*!*	ORDER BY 1,2,3
*!*						ENDTEXT
*!*						SET TEXTMERGE OFF

					*!*	CHECKVIEWHOMEDEPT AS HOMEDEPT,

					* BELOW GETS SALARIED + 020655 HOURS FROM ADP
					* added checkviewgrosspaya > 0.00 to exclude w-2 check runs that inflate the # of people working
					SET TEXTMERGE ON
					TEXT TO	lcSQL4 NOSHOW
SELECT
DISTINCT
COMPANYCODE AS ADP_COMP,
FILE# AS FILE_NUM,
NAME,
CHECKVIEWHOMEDEPT AS HOMEDEPT,
1.00 AS HEADCNT,
0.00 AS CODEHOURS,
160.00 AS REGHOURS,
0.00 AS OTHOURS,
160.00 AS TOTHOURS,
'U' AS GENDER,
'U' AS MGMTTYPE,
'U' AS TIMETYPE,
'U  ' AS TGFDEPT
FROM REPORTS.V_CHK_VW_INFO
WHERE ((COMPANYCODE <> 'E87') OR (CHECKVIEWHOMEDEPT = '020655'))
AND CHECKVIEWPAYDATE >= <<lcADPFromDate>>
AND CHECKVIEWPAYDATE <= <<lcADPToDate>>
AND {FN IFNULL(CHECKVIEWGROSSPAYA,0.00)} > 0.00
<<lcDivWhere>>
ORDER BY FILE#
					ENDTEXT
					SET TEXTMERGE OFF

*!*						SET TEXTMERGE ON
*!*						TEXT TO	lcSQL5 NOSHOW
*!*	SELECT
*!*	FILE# AS FILE_NUM,
*!*	CHECKVIEWHOMEDEPT AS HOMEDEPT,
*!*	MAX(CHECKVIEWPAYDATE) AS LASTPAY
*!*	FROM REPORTS.V_CHK_VW_INFO
*!*	WHERE ((COMPANYCODE <> 'E87') OR (CHECKVIEWHOMEDEPT = '020655'))
*!*	AND CHECKVIEWPAYDATE >= <<lcADPFromDate>>
*!*	AND CHECKVIEWPAYDATE <= <<lcADPToDate>>
*!*	AND {FN IFNULL(CHECKVIEWGROSSPAYA,0.00)} > 0.00
*!*	<<lcDivWhere>>
*!*	GROUP BY FILE#, CHECKVIEWHOMEDEPT
*!*	ORDER BY FILE#, CHECKVIEWHOMEDEPT
*!*						ENDTEXT
*!*						SET TEXTMERGE OFF

*!*	* for this sql, include paydate and do not aggregate yet. we will use paydate to determine which homedept to use for the month
*!*						SET TEXTMERGE ON
*!*						TEXT TO	lcSQL6 NOSHOW
*!*	SELECT
*!*	COMPANYCODE AS ADP_COMP,
*!*	FILE# AS FILE_NUM,
*!*	NAME,
*!*	CHECKVIEWPAYDATE AS PAYDATE,
*!*	CHECKVIEWHOMEDEPT AS HOMEDEPT,
*!*	1.00 AS HEADCNT,
*!*	0.00 AS CODEHOURS,
*!*	160.00 AS REGHOURS,
*!*	0.00 AS OTHOURS,
*!*	160.00 AS TOTHOURS,
*!*	'U' AS GENDER,
*!*	'U' AS MGMTTYPE,
*!*	'U' AS TIMETYPE,
*!*	'U  ' AS TGFDEPT
*!*	FROM REPORTS.V_CHK_VW_INFO
*!*	WHERE ((COMPANYCODE <> 'E87') OR (CHECKVIEWHOMEDEPT = '020655'))
*!*	AND CHECKVIEWPAYDATE >= <<lcADPFromDate>>
*!*	AND CHECKVIEWPAYDATE <= <<lcADPToDate>>
*!*	AND {FN IFNULL(CHECKVIEWGROSSPAYA,0.00)} > 0.00
*!*	<<lcDivWhere>>
*!*	ORDER BY FILE#
*!*						ENDTEXT
*!*						SET TEXTMERGE OFF

					IF .lTestMode THEN
						.TrackProgress('                        TEST MODE                        ', LOGIT+SENDIT)
						.TrackProgress('lcSQL =' + lcSQL, LOGIT+SENDIT)
						*.TrackProgress('lcSQL2 =' + lcSQL2, LOGIT+SENDIT)
						*.TrackProgress('lcSQL3 =' + lcSQL3, LOGIT+SENDIT)
						.TrackProgress('lcSQL4 =' + lcSQL4, LOGIT+SENDIT)
						*.TrackProgress('lcSQL5 =' + lcSQL5, LOGIT+SENDIT)
						*.TrackProgress('lcSQL6 =' + lcSQL6, LOGIT+SENDIT)
					ENDIF

					IF USED('CURHEADCOUNT') THEN
						USE IN CURHEADCOUNT
					ENDIF
					IF USED('CURCUSTOM') THEN
						USE IN CURCUSTOM
					ENDIF
					IF USED('CURDIVLIST') THEN
						USE IN CURDIVLIST
					ENDIF
					IF USED('CURSALPRE') THEN
						USE IN CURSALPRE
					ENDIF
					IF USED('CURSALPRE2') THEN
						USE IN CURSALPRE2
					ENDIF
					IF USED('CURPAYDATESPRE') THEN
						USE IN CURPAYDATESPRE
					ENDIF
					IF USED('CURPAYDATES') THEN
						USE IN CURPAYDATES
					ENDIF
					IF USED('CURWHITE') THEN
						USE IN CURWHITE
					ENDIF
					IF USED('CURBLUE') THEN
						USE IN CURBLUE
					ENDIF
					IF USED('CURCNTTYPE') THEN
						USE IN CURCNTTYPE
					ENDIF
					IF USED('CURCNTGENDER') THEN
						USE IN CURCNTGENDER
					ENDIF
					IF USED('CURTEMPSPRE') THEN
						USE IN CURTEMPSPRE
					ENDIF
					IF USED('CURTEMPS') THEN
						USE IN CURTEMPS
					ENDIF
					IF USED('CURTMPHEADCNT') THEN
						USE IN CURTMPHEADCNT
					ENDIF
					IF USED('CURSALDATES') THEN
						USE IN CURSALDATES
					ENDIF
					IF USED('CURSALDETAIL') THEN
						USE IN CURSALDETAIL
					ENDIF
					IF USED('CURSALDETAIL2') THEN
						USE IN CURSALDETAIL2
					ENDIF

					IF .ExecSQL(lcSQL, 'CURCUSTOM', RETURN_DATA_MANDATORY) AND ;
							.ExecSQL(lcSQL4, 'CURSALPRE', RETURN_DATA_MANDATORY) THEN

						*!*								.ExecSQL(lcSQL2, 'CURHOURSPRE', RETURN_DATA_MANDATORY) AND ;

*!*	*!*	IF .LTESTMODE THEN
*!*	*!*		SELECT CURSALPRE
*!*	*!*		BROWSE
*!*	*!*	ENDIF
						

						* WE WANT TO DELETE ANY DIV 59 DATA AFTER 6/30/2013 - Because Underarmour took over Rialto on 7/1/2013 but kept using our clocks for a few weeks.
						IF ldFromDate > {^2013-06-30} THEN
							DO CASE

								CASE INLIST(tcType,"K","Q","R")
									SELECT CURSALPRE
									SCAN
										IF LEFT(HOMEDEPT,2) = '59' THEN
											DELETE
										ENDIF
									ENDSCAN

								OTHERWISE
									* nothing
							ENDCASE
						ENDIF


						* We need to adjust FTE in cursalpre for those who appear more than once
						IF USED('CURDUPES') THEN
							USE IN CURDUPES
						ENDIF

						SELECT FILE_NUM, COUNT(*) AS HEADCNT ;
							FROM CURSALPRE ;
							INTO CURSOR CURDUPES ;
							GROUP BY 1

						IF USED('CURDUPES') AND NOT EOF('CURDUPES') THEN
							SELECT CURDUPES
							SCAN FOR HEADCNT > 1
								SELECT CURSALPRE
								SCAN FOR FILE_NUM == CURDUPES.FILE_NUM
									REPLACE CURSALPRE.REGHOURS WITH ( CURSALPRE.REGHOURS / CURDUPES.HEADCNT ), ;
										CURSALPRE.TOTHOURS WITH ( CURSALPRE.TOTHOURS / CURDUPES.HEADCNT ) ;
										IN CURSALPRE
								ENDSCAN
							ENDSCAN
						ENDIF

						IF USED('CURDUPES') THEN
							USE IN CURDUPES
						ENDIF

						*!*	SELECT CURSALPRE
						*!*	BROWSE


						* Add salaried info to CURHOURS, which has only hourly in it right now...
						SELECT CURSALPRE
						SCAN
							SCATTER MEMVAR
							m.PAYDATE = ldFromDate
							m.EXPFTE = lnFTEHours
							m.ANNSALARY = 00000000.00
							INSERT INTO CURHOURS FROM MEMVAR
						ENDSCAN

						*!*						ENDIF && .ExecSQL(lcSQL, 'CURCUSTOM', RETURN_DATA_MANDATORY)

						* POPULATE MISSING INFO FROM OTHER CURSOR AND ALSO CALC FTE

						*!*	IF .LTESTMODE THEN
						*!*		SELECT CURHOURS
						*!*		BROWSE
						*!*	ENDIF

						*SET STEP ON




						*!*
						*!*							SELECT CURHOURS
						*!*							SCAN FOR ADP_COMP <> 'TMP'

						*!*								SELECT CURCUSTOM
						*!*								LOCATE FOR ;
						*!*									ALLTRIM(ADP_COMP) = ALLTRIM(CURHOURS.ADP_COMP) ;
						*!*									AND FILE_NUM = CURHOURS.FILE_NUM ;
						*!*									AND ALLTRIM(STATUS) <> 'T'

						*!*								IF FOUND() THEN
						*!*									REPLACE CURHOURS.TIMETYPE WITH ALLTRIM(CURCUSTOM.TIMETYPE), ;
						*!*										CURHOURS.MGMTTYPE WITH ALLTRIM(CURCUSTOM.MGMTTYPE), ;
						*!*										CURHOURS.GENDER WITH ALLTRIM(CURCUSTOM.GENDER), ;
						*!*										CURHOURS.NAME WITH ALLTRIM(CURCUSTOM.NAME), ;
						*!*										CURHOURS.TGFDEPT WITH ALLTRIM(CURCUSTOM.TGFDEPT), ;
						*!*										CURHOURS.ANNSALARY WITH CURCUSTOM.ANNSALARY ;
						*!*										IN CURHOURS

						*!*									* for full-time / part-time flag, if it's not a P, make it an F
						*!*									IF NOT UPPER(ALLTRIM(CURCUSTOM.TIMETYPE)) == "P" THEN
						*!*										REPLACE CURHOURS.TIMETYPE WITH "F" IN CURHOURS
						*!*									ENDIF

						*!*								ELSE
						*!*									* active status not found, try terminated in case they are recent term
						*!*									SELECT CURCUSTOM
						*!*									LOCATE FOR ;
						*!*										ALLTRIM(ADP_COMP) = ALLTRIM(CURHOURS.ADP_COMP) ;
						*!*										AND FILE_NUM = CURHOURS.FILE_NUM ;
						*!*										AND ALLTRIM(STATUS) = 'T'

						*!*									IF FOUND() THEN
						*!*										REPLACE CURHOURS.TIMETYPE WITH ALLTRIM(CURCUSTOM.TIMETYPE), ;
						*!*											CURHOURS.MGMTTYPE WITH ALLTRIM(CURCUSTOM.MGMTTYPE), ;
						*!*											CURHOURS.GENDER WITH ALLTRIM(CURCUSTOM.GENDER), ;
						*!*											CURHOURS.NAME WITH ALLTRIM(CURCUSTOM.NAME), ;
						*!*											CURHOURS.TGFDEPT WITH ALLTRIM(CURCUSTOM.TGFDEPT), ;
						*!*											CURHOURS.ANNSALARY WITH CURCUSTOM.ANNSALARY ;
						*!*											IN CURHOURS

						*!*										* for full-time / part-time flag, if it's not a P, make it an F
						*!*										IF NOT UPPER(ALLTRIM(CURCUSTOM.TIMETYPE)) == "P" THEN
						*!*											REPLACE CURHOURS.TIMETYPE WITH "F" IN CURHOURS
						*!*										ENDIF

						*!*									ELSE
						*!*										* just search by file#, ignore status and companycode

						*!*										SELECT CURCUSTOM
						*!*										LOCATE FOR FILE_NUM = CURHOURS.FILE_NUM

						*!*										IF FOUND() THEN
						*!*											REPLACE CURHOURS.TIMETYPE WITH ALLTRIM(CURCUSTOM.TIMETYPE), ;
						*!*												CURHOURS.MGMTTYPE WITH ALLTRIM(CURCUSTOM.MGMTTYPE), ;
						*!*												CURHOURS.GENDER WITH ALLTRIM(CURCUSTOM.GENDER), ;
						*!*												CURHOURS.NAME WITH ALLTRIM(CURCUSTOM.NAME), ;
						*!*												CURHOURS.TGFDEPT WITH ALLTRIM(CURCUSTOM.TGFDEPT), ;
						*!*												CURHOURS.ANNSALARY WITH CURCUSTOM.ANNSALARY ;
						*!*												IN CURHOURS

						*!*											* for full-time / part-time flag, if it's not a P, make it an F
						*!*											IF NOT UPPER(ALLTRIM(CURCUSTOM.TIMETYPE)) == "P" THEN
						*!*												REPLACE CURHOURS.TIMETYPE WITH "F" IN CURHOURS
						*!*											ENDIF

						*!*										ELSE
						*!*											* nothing can be done
						*!*											SELECT CURCUSTOM
						*!*											LOCATE
						*!*										ENDIF

						*!*									ENDIF

						*!*								ENDIF

						*!*							ENDSCAN


						SELECT CURHOURS
						SCAN FOR ADP_COMP <> 'TMP'

							SELECT CURCUSTOM
							LOCATE FOR ;
								ALLTRIM(ADP_COMP) = ALLTRIM(CURHOURS.ADP_COMP) ;
								AND FILE_NUM = CURHOURS.FILE_NUM ;
								AND ALLTRIM(STATUS) <> 'T'

							IF FOUND() THEN
								REPLACE CURHOURS.TIMETYPE WITH ALLTRIM(CURCUSTOM.TIMETYPE), ;
									CURHOURS.GENDER WITH ALLTRIM(CURCUSTOM.GENDER), ;
									CURHOURS.NAME WITH ALLTRIM(CURCUSTOM.NAME), ;
									CURHOURS.ANNSALARY WITH CURCUSTOM.ANNSALARY ;
									IN CURHOURS

								* for full-time / part-time flag, if it's not a P, make it an F
								IF NOT UPPER(ALLTRIM(CURCUSTOM.TIMETYPE)) == "P" THEN
									REPLACE CURHOURS.TIMETYPE WITH "F" IN CURHOURS
								ENDIF

							ELSE
								* active status not found, try terminated in case they are recent term
								SELECT CURCUSTOM
								LOCATE FOR ;
									ALLTRIM(ADP_COMP) = ALLTRIM(CURHOURS.ADP_COMP) ;
									AND FILE_NUM = CURHOURS.FILE_NUM ;
									AND ALLTRIM(STATUS) = 'T'

								IF FOUND() THEN
									REPLACE CURHOURS.TIMETYPE WITH ALLTRIM(CURCUSTOM.TIMETYPE), ;
										CURHOURS.GENDER WITH ALLTRIM(CURCUSTOM.GENDER), ;
										CURHOURS.NAME WITH ALLTRIM(CURCUSTOM.NAME), ;
										CURHOURS.ANNSALARY WITH CURCUSTOM.ANNSALARY ;
										IN CURHOURS

									* for full-time / part-time flag, if it's not a P, make it an F
									IF NOT UPPER(ALLTRIM(CURCUSTOM.TIMETYPE)) == "P" THEN
										REPLACE CURHOURS.TIMETYPE WITH "F" IN CURHOURS
									ENDIF

								ELSE
									* just search by file#, ignore status and companycode

									SELECT CURCUSTOM
									LOCATE FOR FILE_NUM = CURHOURS.FILE_NUM

									IF FOUND() THEN
										REPLACE CURHOURS.TIMETYPE WITH ALLTRIM(CURCUSTOM.TIMETYPE), ;
											CURHOURS.GENDER WITH ALLTRIM(CURCUSTOM.GENDER), ;
											CURHOURS.NAME WITH ALLTRIM(CURCUSTOM.NAME), ;
											CURHOURS.ANNSALARY WITH CURCUSTOM.ANNSALARY ;
											IN CURHOURS

										* for full-time / part-time flag, if it's not a P, make it an F
										IF NOT UPPER(ALLTRIM(CURCUSTOM.TIMETYPE)) == "P" THEN
											REPLACE CURHOURS.TIMETYPE WITH "F" IN CURHOURS
										ENDIF

									ELSE
										* nothing can be done
										SELECT CURCUSTOM
										LOCATE
									ENDIF

								ENDIF

							ENDIF

						ENDSCAN

						*!*		SELECT CURHOURS
						*!*		BROW
						*!*	SET STEP ON
						*****************************************************************
						* populate tgf depts from the new dept translation table
						SELECT CURHOURS
						SCAN
							lcDept = RIGHT(ALLTRIM(CURHOURS.HOMEDEPT),4)
							SELECT DEPTLOOKUPTABLE
							LOCATE FOR DEPT = lcDept
							IF FOUND() THEN
								REPLACE CURHOURS.MGMTTYPE WITH DEPTLOOKUPTABLE.COLLARTYPE, ;
									CURHOURS.TGFDEPT WITH DEPTLOOKUPTABLE.HCFTECODE, ;
									CURHOURS.GLACCTNO WITH DEPTLOOKUPTABLE.GLACCTNO, ;
									CURHOURS.DEPTDESC WITH DEPTLOOKUPTABLE.HCFTEDESC ;
									IN CURHOURS
							ENDIF
						ENDSCAN
						*****************************************************************

						*!*	IF (tcMode = "ALL") THEN

						*!*		SELECT CURHOURS
						*!*		BROWSE
						*!*
						*!*	ENDIF

						* CALC FTE
						SELECT CURHOURS
						SCAN
							*!*	DO CASE
							*!*		CASE CURHOURS.HOMEDEPT = '020655'
							*!*			* EMPLOYEE IS OTR DRIVER -- calc FTE
							*!*			REPLACE CURHOURS.FTE WITH ( CURHOURS.TOTHOURS / lnFTEHours ) IN CURHOURS
							*!*		CASE (CURHOURS.ADP_COMP = 'E87') AND (NOT CURHOURS.HOMEDEPT = '020655')
							*!*			* EMPLOYEE IS HOURLY, AND NOT AN OTR DRIVER -- calc FTE
							*!*			REPLACE CURHOURS.FTE WITH ( CURHOURS.TOTHOURS / lnFTEHours ) IN CURHOURS
							*!*		CASE INLIST(CURHOURS.ADP_COMP,'E88','E89')
							*!*			* EMPLOYEE IS SALARIED -- calc FTE
							*!*			REPLACE CURHOURS.FTE WITH ( CURHOURS.TOTHOURS / lnFTEHours ) IN CURHOURS
							*!*		CASE INLIST(CURHOURS.ADP_COMP,'TMP')
							*!*			* TEMP -- calc FTE
							*!*			REPLACE CURHOURS.FTE WITH ( CURHOURS.TOTHOURS / lnFTEHours ) IN CURHOURS
							*!*		OTHERWISE
							*!*			* NOTHING
							*!*	ENDCASE

							* NOW THEY ARE ALL TREATED THE SAME 07/03/2012 MB
							REPLACE CURHOURS.FTE WITH ( CURHOURS.TOTHOURS / lnFTEHours ) IN CURHOURS

							* populate wageamt = 1/12th of annual salary for salAried emps, per Ken K. 11/12/12 MB
							IF INLIST(CURHOURS.ADP_COMP,'E88','E89') THEN
								REPLACE CURHOURS.WAGEAMT WITH (CURHOURS.ANNSALARY / 12)
							ENDIF

						ENDSCAN


						*!*							IF (tcMode = "ALL") THEN
						*!*								* get OO headcounts and mileage if possible
						*!*								.GetOwnerOpData( ldFromDate, ldToDate )
						*!*							ENDIF


						*!*							* add dept descriptions
						*!*							SELECT A.*, IIF(ISNULL(B.DESC),'U',B.DESC) AS DEPTDESC ;
						*!*								FROM CURHOURS A ;
						*!*								LEFT OUTER JOIN ;
						*!*								F:\UTIL\ADPREPORTS\DATA\TGFDEPTS B ;
						*!*								ON B.CODE = ALLTRIM(A.TGFDEPT) ;
						*!*								INTO CURSOR CURHOURS2 ;
						*!*								ORDER BY ADP_COMP, NAME ;
						*!*								READWRITE

						SELECT * ;
							FROM CURHOURS ;
							INTO CURSOR CURHOURS2 ;
							ORDER BY ADP_COMP, NAME ;
							READWRITE

						* apply temp markup factor
						SELECT CURHOURS2
						SCAN FOR ADP_COMP = 'TMP'
							REPLACE CURHOURS2.WAGEAMT WITH ( lnTempMarkUpFactor * CURHOURS2.WAGEAMT )
						ENDSCAN


						IF (tcMode = "ALL") THEN

							*SELECT CURHOURS2
							*BROW

							* track some detail info for people with bad/missing data
							SELECT CURHOURS2
							SCAN FOR ('U' $ MGMTTYPE) OR ('U' $ TGFDEPT) OR (EMPTY(DEPTDESC))
								.cBadData = .cBadData + ALLTRIM(CURHOURS2.ADP_COMP) + ', ' + ALLTRIM(CURHOURS2.NAME) + ', ' + RIGHT(ALLTRIM(CURHOURS2.HOMEDEPT),4) + ', ' + TRANSFORM(CURHOURS2.FILE_NUM) + ;
									+ ', ' + CURHOURS2.MGMTTYPE + ', ' + CURHOURS2.TGFDEPT+ ', ' + CURHOURS2.DEPTDESC + CRLF
							ENDSCAN

						ENDIF



						IF tcType = "R" THEN
							* generate hc/fte entity report for Ken and Bob
							IF USED('CURHCFTEPRE') THEN
								USE IN CURHCFTEPRE
							ENDIF
							IF USED('CURHCFTE') THEN
								USE IN CURHCFTE
							ENDIF
							IF USED('CURMULTDEPTSPRE') THEN
								USE IN CURMULTDEPTSPRE
							ENDIF
							IF USED('CURMULTDEPTS') THEN
								USE IN CURMULTDEPTS
							ENDIF
							
							lcHCFTEEntityDetailFile  = "F:\UTIL\HR\TGFSTAFFRPT\REPORTS\HC_FTE_ENTITY_DETAIL" + lcSuffix + ".XLS"
							lcHCFTEEntitySummaryFile = "F:\UTIL\HR\TGFSTAFFRPT\REPORTS\HC_FTE_ENTITY_SUMMARY" + lcSuffix + ".XLS"
							lcHCFTEEntityMultDeptsFile = "F:\UTIL\HR\TGFSTAFFRPT\REPORTS\HC_FTE_ENTITY_MULTDEPTS" + lcSuffix + ".XLS"

							* this file may not always be created, so delete any old ones if they are there...
							IF FILE(lcHCFTEEntityMultDeptsFile) THEN
								DELETE FILE (lcHCFTEEntityMultDeptsFile)
							ENDIF

							SELECT ;
								ADP_COMP, ;
								NAME, ;
								FILE_NUM, ;
								LEFT(ALLTRIM(HOMEDEPT),2) AS DIVISION, ;
								RIGHT(ALLTRIM(HOMEDEPT),4) AS DEPT, ;
								GLACCTNO, ;
								(GLACCTNO + "-0000-" + LEFT(ALLTRIM(HOMEDEPT),2)) AS GLCODE, ;
								HEADCNT, ;
								FTE ;
								FROM CURHOURS2 ;
								INTO CURSOR CURHCFTEPRE ;
								WHERE ALLTRIM(ADP_COMP) <> 'TMP' ;
								ORDER BY 1, 2 ;
								READWRITE
								
							SELECT ;
								NAME, ;
								FILE_NUM, ;
								COUNT(*) AS COUNT ;
								FROM CURHCFTEPRE ;
								INTO CURSOR CURMULTDEPTSPRE ;
								GROUP BY NAME, FILE_NUM ;
								ORDER BY NAME, FILE_NUM ;
								HAVING COUNT > 1
								
							IF USED('CURMULTDEPTSPRE') AND NOT EOF('CURMULTDEPTSPRE') THEN
							
								SELECT * ;
									FROM CURHCFTEPRE ;
									INTO CURSOR CURMULTDEPTS ;
									WHERE FILE_NUM IN (SELECT FILE_NUM FROM CURMULTDEPTSPRE) ;
									ORDER BY NAME
								
								SELECT CURMULTDEPTS 
								COPY TO (lcHCFTEEntityMultDeptsFile) XL5
								
							ENDIF
							
								
								

							*!*				SELECT CURHCFTEPRE
							*!*				BROWSE
							SELECT CURHCFTEPRE
							COPY TO (lcHCFTEEntityDetailFile) XL5

							SELECT ;
								DEPT, ;
								GLCODE, ;
								SUM(FTE) AS FTE, ;
								SUM(HEADCNT) AS HEADCNT ;
								FROM CURHCFTEPRE ;
								INTO CURSOR CURHCFTE ;
								GROUP BY 1, 2 ;
								ORDER BY 1, 2 ;
								READWRITE

							*!*				SELECT CURHCFTE
							*!*				BROWSE

							SELECT CURHCFTE
							COPY TO (lcHCFTEEntitySummaryFile) XL5


						ENDIF  && tcType = "R"

						SELECT CURHOURS2
						lcFileName = "F:\UTIL\HR\TGFSTAFFRPT\REPORTS\DETAIL\tgfhours" + lcSuffix + ".XLS"
						*COPY TO ( "F:\UTIL\HR\TGFSTAFFRPT\REPORTS\DETAIL\tgfhours" + lcSuffix + ".XLS" ) XL5
						COPY TO ( lcFileName ) XL5

						IF LEFT( lcSuffix, 4 ) == "_ALL" THEN
							* we just copied the detail file which Ken now wants - keep track of the filename for later
							.cDetailFileForKen = lcFileName
						ENDIF

						*SELECT CURHOURS2
						*COPY TO ( "F:\UTIL\HR\TGFSTAFFRPT\REPORTS\DETAIL\tgfhours" + lcSuffix )


						*******************************************************************************************************
						*******************************************************************************************************
						IF (tcMode = "ALL") AND .lDoBIExport THEN

							* create data for Qlikview

							*.CreateBIData()
							*!*		SELECT CURHOURSPRE3
							*!*		BROW
							* NOTE: the copying and renaming below is needed to make the 1st tab of the spreadsheet have the name "sheet1".
							* Because VFP will give that tab the same name as the spreadhseet filename via COPY TO ... XL5 command. MB 05/11/2012.

							IF .lTestMode THEN
								lcBIFileName = "F:\UTIL\HR\TGFSTAFFRPT\REPORTS\BIDATA\ADPFTE" + lcSuffix + ".XLS"
								SELECT CURHOURS2
								COPY TO F:\UTIL\HR\TGFSTAFFRPT\REPORTS\BIDATA\Sheet1.XLS XL5
								IF FILE( lcBIFileName ) THEN
									DELETE FILE ( lcBIFileName )
								ENDIF
								RENAME F:\UTIL\HR\TGFSTAFFRPT\REPORTS\BIDATA\Sheet1.XLS TO ( lcBIFileName )
								.TrackProgress('Copied ' + lcSuffix + ' xls file to TEST Qlikview Folder!', LOGIT+SENDIT)
							ELSE
								lcBIFileName = "S:\QLIKVIEW\ADP\ADPFTE" + lcSuffix + ".XLS"
								SELECT CURHOURS2
								COPY TO S:\QLIKVIEW\ADP\Sheet1.XLS XL5
								IF FILE( lcBIFileName ) THEN
									DELETE FILE ( lcBIFileName )
								ENDIF
								RENAME S:\QLIKVIEW\ADP\Sheet1.XLS TO ( lcBIFileName )
								.TrackProgress('Copied ' + lcSuffix + ' xls file to Qlikview Folder!', LOGIT+SENDIT)
							ENDIF

							*SELECT CURHOURS2
							*COPY TO ( "F:\UTIL\HR\TGFSTAFFRPT\REPORTS\BIDATA\ADPFTE" + lcSuffix )
						ENDIF  &&  (tcMode = "ALL") AND .lDoBIExport
						*******************************************************************************************************
						*******************************************************************************************************

						*!*							* summarize for White Collar
						*!*							SELECT "WHITE COLLAR" AS MGMTTYPE, ;
						*!*								TGFDEPT, ;
						*!*								DEPTDESC, ;
						*!*								SUM(FTE) AS FTE, ;
						*!*								SUM(HEADCNT) AS HEADCNT, ;
						*!*								SUM(TOTHOURS) AS TOTHOURS ;
						*!*								FROM CURHOURS2 ;
						*!*								INTO CURSOR CURWHITE ;
						*!*								WHERE CURHOURS2.MGMTTYPE = "W" ;
						*!*								GROUP BY 1, 2, 3 ;
						*!*								ORDER BY 1, 3

						* RECORD DISTINCT DIVISIONS - FOR DEBUG PURPOSES
						SELECT DISTINCT LEFT(HOMEDEPT,2) AS DIV ;
							FROM CURHOURS2 ;
							INTO CURSOR CURDIVLIST ;
							ORDER BY 1

						SELECT CURDIVLIST
						COPY TO ( "F:\UTIL\HR\TGFSTAFFRPT\REPORTS\DETAIL\DIV_LIST" + lcSuffix + ".XLS" ) XL5

						* RECORD DISTINCT INFO - FOR DEBUG PURPOSES
						SELECT A.NAME, A.FILE_NUM, A.HOMEDEPT  ;
							FROM CURHOURS2 A ;
							INTO CURSOR CURHEADCOUNT ;
							WHERE EXISTS ;
							(SELECT NAME FROM CURHOURS2 WHERE NAME = A.NAME GROUP BY NAME HAVING COUNT(*) > 1) ;
							ORDER BY A.NAME

						SELECT CURHEADCOUNT
						COPY TO ( "F:\UTIL\HR\TGFSTAFFRPT\REPORTS\DETAIL\HEADCOUNT" + lcSuffix + ".XLS" ) XL5


						* summarize for White Collar
						SELECT "WHITE COLLAR" AS MGMTTYPE, ;
							TGFDEPT, ;
							DEPTDESC, ;
							SUM(FTE) AS FTE, ;
							COUNT(DISTINCT FILE_NUM) AS HEADCNT, ;
							0000 AS ADJHDCNT, ;
							SUM(TOTHOURS) AS TOTHOURS ;
							FROM CURHOURS2 ;
							INTO CURSOR CURWHITE ;
							WHERE CURHOURS2.MGMTTYPE = "W" ;
							GROUP BY 1, 2, 3 ;
							ORDER BY 1, 3
							
							
*!*							SELECT CURWHITE
*!*							BROWSE
*!*							
*!*							SELECT CURWHITE
*!*							SCAN
*!*								IF USED('CURWADJHDCNT') THEN
*!*									USE IN CURWADJHDCNT
*!*								ENDIF
*!*								
*!*								SELECT COUNT(DISTINCT FILE_NUM) AS ADJHDCNT, ;
*!*									FROM CURHOURS2 ;
*!*									INTO CURSOR CURWADJHDCNT ;
*!*									WHERE CURHOURS2.MGMTTYPE = "W" ;
*!*									AND CURHOURS2.TGFDEPT = CURWHITE.TGFDEPT ;
*!*									AND 
*!*							
*!*							ENDSCAN
*!*							
*!*							SELECT CURWHITE
*!*							BROWSE
						
							

						SELECT CURWHITE
						COPY TO ( "F:\UTIL\HR\TGFSTAFFRPT\REPORTS\DETAIL\tgfwhitecollar" + lcSuffix + ".XLS" ) XL5

						* summarize for Blue Collar
						*!*							SELECT "BLUE COLLAR" AS MGMTTYPE, ;
						*!*								TGFDEPT, ;
						*!*								DEPTDESC, ;
						*!*								SUM(FTE) AS FTE, ;
						*!*								SUM(HEADCNT) AS HEADCNT, ;
						*!*								SUM(TOTHOURS) AS TOTHOURS ;
						*!*								FROM CURHOURS2 ;
						*!*								INTO CURSOR CURBLUE ;
						*!*								WHERE CURHOURS2.MGMTTYPE <> "W" ;
						*!*								GROUP BY 1, 2, 3 ;
						*!*								ORDER BY 1, 3

						SELECT "BLUE COLLAR" AS MGMTTYPE, ;
							TGFDEPT, ;
							DEPTDESC, ;
							SUM(FTE) AS FTE, ;
							COUNT(DISTINCT FILE_NUM) AS HEADCNT, ;
							SUM(TOTHOURS) AS TOTHOURS ;
							FROM CURHOURS2 ;
							INTO CURSOR CURBLUE ;
							WHERE CURHOURS2.MGMTTYPE <> "W" ;
							GROUP BY 1, 2, 3 ;
							ORDER BY 1, 3

						SELECT CURBLUE
						COPY TO ( "F:\UTIL\HR\TGFSTAFFRPT\REPORTS\DETAIL\tgfblueecollar" + lcSuffix + ".XLS" ) XL5

						* summarize for part-time / full-ime
						*!*							SELECT TIMETYPE, ;
						*!*								SUM(FTE) AS FTE, ;
						*!*								SUM(HEADCNT) AS HEADCNT ;
						*!*								FROM CURHOURS2 ;
						*!*								INTO CURSOR CURCNTTYPE ;
						*!*								GROUP BY 1 ;
						*!*								ORDER BY 1

						SELECT TIMETYPE, ;
							SUM(FTE) AS FTE, ;
							COUNT(DISTINCT FILE_NUM) AS HEADCNT ;
							FROM CURHOURS2 ;
							INTO CURSOR CURCNTTYPE ;
							GROUP BY 1 ;
							ORDER BY 1

						SELECT CURCNTTYPE
						COPY TO ( "F:\UTIL\HR\TGFSTAFFRPT\REPORTS\DETAIL\tgftimetype" + lcSuffix + ".XLS" ) XL5


						* summarize BY GENDER
						*!*							SELECT GENDER, ;
						*!*								SUM(FTE) AS FTE, ;
						*!*								SUM(HEADCNT) AS HEADCNT ;
						*!*								FROM CURHOURS2 ;
						*!*								INTO CURSOR CURGENDER ;
						*!*								GROUP BY 1 ;
						*!*								ORDER BY 1

						SELECT GENDER, ;
							SUM(FTE) AS FTE, ;
							COUNT(DISTINCT FILE_NUM) AS HEADCNT ;
							FROM CURHOURS2 ;
							INTO CURSOR CURGENDER ;
							GROUP BY 1 ;
							ORDER BY 1

						SELECT CURGENDER
						COPY TO ( "F:\UTIL\HR\TGFSTAFFRPT\REPORTS\DETAIL\tgfgender"  + lcSuffix + ".XLS" ) XL5

						=SQLDISCONNECT(.nSQLHandle)


						**********************************************************************************
						**********************************************************************************
						**********************************************************************************

						* populate the spreadsheet

						oWorksheet.RANGE("C1").VALUE = 'for Date Range: ' + DTOC(ldFromDate) + " - " + DTOC(ldToDate)

						lnRow = lnStartRow
						lcRow = ALLTRIM(STR(lnRow))
						lnMgmtStartRow = lnRow + 1
						lcMgmtStartRow = ALLTRIM(STR(lnMgmtStartRow))
						oWorksheet.RANGE("A"+lcRow).VALUE = "Mgmt. Type"
						oWorksheet.RANGE("B"+lcRow).VALUE = "TGF Dept."
						oWorksheet.RANGE("C"+lcRow).VALUE = "Dept. Description"
						oWorksheet.RANGE("D"+lcRow).VALUE = "FTE"
						oWorksheet.RANGE("E"+lcRow).VALUE = "Headcount"
						IF .lShowHours THEN
							oWorksheet.RANGE("F"+lcRow).VALUE = "Tot Hours"
						ENDIF
						oWorksheet.RANGE("A"+lcRow+":F"+lcRow).FONT.BOLD = .T.

						SELECT CURWHITE
						SCAN
							lnRow = lnRow + 1
							lcRow = ALLTRIM(STR(lnRow))
							oWorksheet.RANGE("A"+lcRow).VALUE = CURWHITE.MGMTTYPE
							oWorksheet.RANGE("B"+lcRow).VALUE = CURWHITE.TGFDEPT
							oWorksheet.RANGE("C"+lcRow).VALUE = CURWHITE.DEPTDESC
							oWorksheet.RANGE("D"+lcRow).VALUE = CURWHITE.FTE
							oWorksheet.RANGE("E"+lcRow).VALUE = CURWHITE.HEADCNT
							IF .lShowHours THEN
								oWorksheet.RANGE("F"+lcRow).VALUE = CURWHITE.TOTHOURS
							ENDIF
						ENDSCAN

						SELECT CURBLUE
						SCAN
							lnRow = lnRow + 1
							lcRow = ALLTRIM(STR(lnRow))
							oWorksheet.RANGE("A"+lcRow).VALUE = CURBLUE.MGMTTYPE
							oWorksheet.RANGE("B"+lcRow).VALUE = CURBLUE.TGFDEPT
							oWorksheet.RANGE("C"+lcRow).VALUE = CURBLUE.DEPTDESC
							oWorksheet.RANGE("D"+lcRow).VALUE = CURBLUE.FTE
							oWorksheet.RANGE("E"+lcRow).VALUE = CURBLUE.HEADCNT
							IF .lShowHours THEN
								oWorksheet.RANGE("F"+lcRow).VALUE = CURBLUE.TOTHOURS
							ENDIF
						ENDSCAN

						lnMgmtEndRow = lnRow
						lcMgmtEndRow = ALLTRIM(STR(lnMgmtEndRow))

						lnRow = lnRow + 1
						lcRow = ALLTRIM(STR(lnRow))

						* Ken wants totals for the Mgmt Type section...
						oWorksheet.RANGE("D" + lcRow).VALUE = "=SUM(D" + lcMgmtStartRow + ":D" + lcMgmtEndRow + ")"
						oWorksheet.RANGE("E" + lcRow).VALUE = "=SUM(E" + lcMgmtStartRow + ":E" + lcMgmtEndRow + ")"

						* UNDERLINE cell columns to be totaled...
						oWorksheet.RANGE("D" + lcMgmtEndRow + ":E" + lcMgmtEndRow).BORDERS(xlEdgeBottom).LineStyle = xlContinuous
						oWorksheet.RANGE("D" + lcMgmtEndRow + ":E" + lcMgmtEndRow).BORDERS(xlEdgeBottom).Weight = xlMedium

						lnRow = lnRow + 2
						lcRow = ALLTRIM(STR(lnRow))
						oWorksheet.RANGE("A"+lcRow).VALUE = "Emp. Type"
						oWorksheet.RANGE("B"+lcRow).VALUE = "FTE"
						oWorksheet.RANGE("C"+lcRow).VALUE = "Headcount"
						oWorksheet.RANGE("A"+lcRow+":C"+lcRow).FONT.BOLD = .T.

						SELECT CURCNTTYPE
						SCAN
							lnRow = lnRow + 1
							lcRow = ALLTRIM(STR(lnRow))
							oWorksheet.RANGE("A"+lcRow).VALUE = .GetTimeTypeDesc( CURCNTTYPE.TIMETYPE )  && IIF(CURCNTTYPE.TIMETYPE="F","Full Time","Part Time")
							oWorksheet.RANGE("B"+lcRow).VALUE = CURCNTTYPE.FTE
							oWorksheet.RANGE("C"+lcRow).VALUE = CURCNTTYPE.HEADCNT
						ENDSCAN

						lnPreCasualRow = lnRow  && save row for filling in temp data later

						* final roll up
						*!*							SELECT ;
						*!*								"Casual" AS TYPE, ;
						*!*								SUM(TOTHOURS) AS TOTHOURS, ;
						*!*								SUM(FTE) AS FTE, ;
						*!*								SUM(HEADCNT) AS HEADCNT ;
						*!*								FROM CURHOURS2 ;
						*!*								INTO CURSOR CURTMPHEADCNT ;
						*!*								WHERE ADP_COMP = 'TMP'

						SELECT ;
							"Casual" AS TYPE, ;
							SUM(TOTHOURS) AS TOTHOURS, ;
							SUM(FTE) AS FTE, ;
							COUNT(DISTINCT FILE_NUM) AS HEADCNT ;
							FROM CURHOURS2 ;
							INTO CURSOR CURTMPHEADCNT ;
							WHERE ADP_COMP = 'TMP'

						SELECT CURTMPHEADCNT
						COPY TO ("F:\UTIL\HR\TGFSTAFFRPT\REPORTS\DETAIL\TGFTEMPS"  + lcSuffix + ".XLS") XL5

						lnRow = lnRow + 2
						lcRow = ALLTRIM(STR(lnRow))
						oWorksheet.RANGE("A"+lcRow).VALUE = "Gender"
						oWorksheet.RANGE("B"+lcRow).VALUE = "FTE"
						oWorksheet.RANGE("C"+lcRow).VALUE = "Headcount"
						oWorksheet.RANGE("A"+lcRow+":C"+lcRow).FONT.BOLD = .T.

						SELECT CURGENDER
						SCAN
							lnRow = lnRow + 1
							lcRow = ALLTRIM(STR(lnRow))
							oWorksheet.RANGE("A"+lcRow).VALUE = CURGENDER.GENDER
							oWorksheet.RANGE("B"+lcRow).VALUE = CURGENDER.FTE
							oWorksheet.RANGE("C"+lcRow).VALUE = CURGENDER.HEADCNT
						ENDSCAN

						*CLOSE DATABASES ALL
					ENDIF  &&  .ExecSQL(lcSQL, 'SQLCURSOR1', RETURN_DATA_MANDATORY)
				ELSE
					* connection error
					.TrackProgress('Unable to connect to ADP Payroll System.', LOGIT+SENDIT)
				ENDIF   &&  .nSQLHandle > 0

				.TrackProgress("FTE Info to Qlikview process ended normally.", LOGIT+SENDIT+NOWAITIT)


			CATCH TO loError

				.TrackProgress('There was an error.',LOGIT+SENDIT)
				.TrackProgress('The Error # is: ' + TRANSFORM(loError.ERRORNO), LOGIT+SENDIT)
				.TrackProgress('The Error Message is: ' + TRANSFORM(loError.MESSAGE), LOGIT+SENDIT)
				.TrackProgress('The Error occurred in Program: ' + TRANSFORM(loError.PROCEDURE), LOGIT+SENDIT)
				.TrackProgress('The Error occurred at line #: ' + TRANSFORM(loError.LINENO), LOGIT+SENDIT)
				lnNumberOfErrors = lnNumberOfErrors + 1

				IF (tcMode = "ALL") THEN
					IF TYPE('oWorkbook') = "O" THEN
						oWorkbook.SAVE()
					ENDIF
					IF TYPE('oExcel') = "O" THEN
						oExcel.QUIT()
					ENDIF
					*CLOSE DATABASES ALL
				ENDIF

			ENDTRY

			*CLOSE DATA
			WAIT CLEAR
			***************** INTERNAL email results ******************************

			* this block must get done only once, at the end of the process...
			IF (tcMode = "ALL") THEN

				IF (lnNumberOfErrors = 0) THEN
					* SAVE AND QUIT EXCEL
					oWorkbook.SAVE()
					oExcel.QUIT()

					DO CASE

						CASE tcType ="K"
							IF FILE(lcFiletoSaveAs) THEN
								.cAttach = lcFiletoSaveAs

								IF FILE(.cDetailFileForKen) THEN
									* add the detail file per Ken 12/10/12 MB
									.cAttach = .cAttach + "," + .cDetailFileForKen
								ELSE
									.TrackProgress("ERROR attaching " + .cDetailFileForKen, LOGIT+SENDIT+NOWAITIT)
								ENDIF

								.cBodyText = "See attached report." + CRLF + CRLF + "<<REPORT LOG FOLLOWS>>" + CRLF + .cBodyText
							ELSE
								.TrackProgress("ERROR attaching " + lcFiletoSaveAs, LOGIT+SENDIT+NOWAITIT)
							ENDIF

						CASE tcType ="R"
							IF FILE(lcHCFTEEntitySummaryFile) THEN
								.cAttach = lcHCFTEEntitySummaryFile

								IF FILE(lcHCFTEEntityDetailFile) THEN
									.cAttach = .cAttach + "," + lcHCFTEEntityDetailFile

									IF FILE(lcHCFTEEntityMultDeptsFile) THEN
									
										.cAttach = .cAttach + "," + lcHCFTEEntityMultDeptsFile
										
										.cBodyText = "See attached reports." + CRLF + CRLF + ;
											"WARNING: some people were in more than 1 div/dept in the time period. " + ;
											"This will cause total summary headcount by entity to be greater than overall total headcount. " + ;
											"See attached MULTDEPTS spreadsheet for a list of these people." + CRLF + CRLF + "<<REPORT LOG FOLLOWS>>" + CRLF + .cBodyText
										
									ELSE
										.cBodyText = "See attached reports." + CRLF + CRLF + "<<REPORT LOG FOLLOWS>>" + CRLF + .cBodyText
									ENDIF
									
								ELSE
									.TrackProgress("ERROR attaching " + lcHCFTEEntityDetailFile, LOGIT+SENDIT+NOWAITIT)
								ENDIF

								*.cBodyText = "See attached reports." + CRLF + CRLF + "<<REPORT LOG FOLLOWS>>" + CRLF + .cBodyText
								
							ELSE
								.TrackProgress("ERROR attaching " + lcHCFTEEntitySummaryFile, LOGIT+SENDIT+NOWAITIT)
							ENDIF

						OTHERWISE
							* NOTHING
					ENDCASE

					IF NOT EMPTY(.cBadData) THEN
						.cBadData = 'WARNING: the following people had bad or missing data elements:' + CRLF + CRLF + ;
							'ADP_COMP, NAME, DEPT, FILE_NUM, MGMTTYPE, TGFDEPT, DEPTDESC' + CRLF + .cBadData
						.cBodyText = .cBadData + CRLF + .cBodyText
					ENDIF
					
				ENDIF  &&  (lnNumberOfErrors = 0)

				.TrackProgress('About to send status email.',LOGIT)
				.TrackProgress('The logfile is: ' + .cLogFile, SENDIT)
				.TrackProgress("FTE Info process started: " + .cStartTime, LOGIT+SENDIT)
				.TrackProgress("FTE Info process finished: " + TTOC(DATETIME()), LOGIT+SENDIT)

				IF .lSendInternalEmailIsOn THEN
					DO FORM dartmail2 WITH .cSendTo,.cFrom,.cSubject,.cCC,.cAttach,.cBodyText,"A"
					.TrackProgress('Sent status email.',LOGIT)
				ELSE
					.TrackProgress('Did not send status email: THIS.lSendInternalEmailIsOn = FALSE.',LOGIT)
				ENDIF && tcMode = "ALL"
			ENDIF

		ENDWITH
		RETURN
	ENDFUNC && main


	FUNCTION GetTimeTypeDesc
		LPARAMETERS tcTimeType
		LOCAL lcRetVal
		lcRetVal = "Unknown"
		DO CASE
			CASE tcTimeType = "F"
				lcRetVal = "Full Time"
			CASE tcTimeType = "P"
				lcRetVal = "Part Time"
			CASE tcTimeType = "C"
				lcRetVal = "Casual"
			CASE tcTimeType = "O"
				lcRetVal = "Contract"
		ENDCASE

		RETURN lcRetVal
	ENDFUNC  &&  GetTimeTypeDesc


	*!*		PROCEDURE GetOwnerOpData
	*!*			LPARAMETERS tdStartDate, tdEndDate
	*!*
	*!*			* Get count of West, East, Express OwnerOps - code stolen from M:\DARREN\OO.PRG :)
	*!*
	*!*			LOCAL ldStartDate, ldEndDate, lnMileage
	*!*
	*!*			ldStartDate = tdStartDate
	*!*			ldEndDate = tdEndDate
	*!*
	*!*			lnMileage = 0.0
	*!*
	*!*			USE F:\ownerop\oodata\odrivers IN 0
	*!*			USE F:\wo\wodata\tdaily IN 0
	*!*			USE F:\EXPRESS\fxdata\fxtrips IN 0

	*!*			CREATE CURSOR xrpt (DRIVER c(30), ss_num c(11), ltn N(3), ltc N(3), ex N(3))
	*!*			INDEX ON ss_num TAG ss_num
	*!*			INDEX ON DRIVER TAG DRIVER

	*!*			SELECT tdaily
	*!*			SCAN FOR BETWEEN(dispatched,ldStartDate,ldEndDate) AND !EMPTY(dis_driver)
	*!*				SELECT odrivers
	*!*				LOCATE FOR ACTIVE AND PADR(LEFT(TRIM(FIRST)+" "+LAST,30),30)=tdaily.dis_driver
	*!*				IF FOUND()
	*!*					IF !SEEK(odrivers.ss_num,"xrpt","ss_num")
	*!*						INSERT INTO xrpt (DRIVER, ss_num) VALUES (odrivers.DRIVER, odrivers.ss_num)
	*!*					ENDIF
	*!*					xoffice=tdaily.office
	*!*					REPLACE lt&xoffice WITH xrpt.lt&xoffice+1 IN xrpt
	*!*				ENDIF
	*!*			ENDSCAN

	*!*			SELECT fxtrips
	*!*			SCAN FOR BETWEEN(delivered,ldStartDate,ldEndDate) AND dvrcompany="O" AND !deadhead
	*!*				FOR i=1 TO 2
	*!*					ii=TRANSFORM(i)
	*!*					IF !EMPTY(dvr&ii.ss)
	*!*						IF SEEK(dvr&ii.ss,"odrivers","ss_num")
	*!*
	*!*							lnMileage = lnMileage + fxtrips.traveled
	*!*
	*!*							IF !SEEK(odrivers.ss_num,"xrpt","ss_num")
	*!*								INSERT INTO xrpt (DRIVER, ss_num) VALUES (odrivers.DRIVER, odrivers.ss_num)
	*!*							ENDIF
	*!*							REPLACE ex WITH xrpt.ex+1 IN xrpt
	*!*						ENDIF
	*!*					ENDIF
	*!*				NEXT
	*!*			ENDSCAN

	*!*			*SELECT xrpt
	*!*			*GO TOP
	*!*			*BROWSE
	*!*
	*!*			*!*	SELECT COUNT(DISTINCT ss_num) AS HEADCOUNT ;
	*!*			*!*		FROM xrpt ;
	*!*			*!*		INTO CURSOR CUREASTOO ;
	*!*			*!*		WHERE ltn > 0

	*!*			*!*	SELECT CUREASTOO
	*!*			*!*	GOTO TOP
	*!*			*!*	m.ECSTOOPS = CUREASTOO.HEADCOUNT

	*!*			*!*	SELECT COUNT(DISTINCT ss_num) AS HEADCOUNT ;
	*!*			*!*		FROM xrpt ;
	*!*			*!*		INTO CURSOR CURWESTOO ;
	*!*			*!*		WHERE ltc > 0

	*!*			*!*	SELECT CURWESTOO
	*!*			*!*	GOTO TOP
	*!*			*!*	m.WCSTOOPS = CURWESTOO.HEADCOUNT

	*!*			*!*	SELECT COUNT(DISTINCT ss_num) AS HEADCOUNT ;
	*!*			*!*		FROM xrpt ;
	*!*			*!*		INTO CURSOR CURXPROO ;
	*!*			*!*		WHERE ex > 0

	*!*			*!*	SELECT CURXPROO
	*!*			*!*	GOTO TOP
	*!*			*!*	m.XPRSOOPS = CURXPROO.HEADCOUNT
	*!*
	*!*
	*!*			.nOOMileage = lnMileage
	*!*			.nOOHeadCnt = RECCOUNT( 'xrpt' )

	*!*	SET STEP ON

	*!*			USE IN odrivers
	*!*			USE IN tdaily
	*!*			USE IN fxtrips
	*!*
	*!*			RETURN
	*!*		ENDPROC


	FUNCTION ExecSQL
		LPARAMETERS tcSQL, tcCursorName, tlNoDataReturnedIsOkay
		LOCAL llRetval, lnResult
		WITH THIS
			* close target cursor if it's open
			IF USED(tcCursorName)
				USE IN (tcCursorName)
			ENDIF
			WAIT WINDOW "Fetching remote data; please wait..." NOWAIT
			lnResult = SQLEXEC(.nSQLHandle, tcSQL, tcCursorName)
			llRetval = ( lnResult > 0 )
			IF llRetval THEN
				* see if any data came back
				IF NOT tlNoDataReturnedIsOkay THEN
					IF (NOT USED(tcCursorName)) OR EOF(tcCursorName) THEN
						llRetval = .F.
						.TrackProgress('No data was returned by this query: ' +CRLF + tcSQL, LOGIT+SENDIT)
						.cSendTo = 'Mark Bennett <mbennett@fmiint.com>'
					ENDIF
				ENDIF
			ELSE
				.TrackProgress("Result Code [" + TRANSFORM(lnResult) + "] returned by this query: " + tcSQL, LOGIT+SENDIT)
				.cSendTo = 'Mark Bennett <mbennett@fmiint.com>'
			ENDIF
			WAIT CLEAR
			RETURN llRetval
		ENDWITH
	ENDFUNC


	PROCEDURE TrackProgress
		* do any combination of Wait Window, writing to logfile, and adding to body of email,
		* based on nFlags parameter.
		LPARAMETERS tcExpression, tnFlags
		WITH THIS
			IF BITAND(tnFlags,LOGIT) = LOGIT THEN
				IF .lLoggingIsOn THEN
					?
					? .cProcessName + "  " + TTOC(DATETIME()) + ": " + tcExpression
				ENDIF
			ENDIF
			IF .lWaitWindowIsOn THEN
				IF BITAND(tnFlags,NOWAITIT) = NOWAITIT THEN
					WAIT WINDOW tcExpression NOWAIT
				ENDIF
				IF BITAND(tnFlags,WAITIT) = WAITIT THEN
					WAIT WINDOW tcExpression TIMEOUT .nWaitWindowTimeout
				ENDIF
			ENDIF
			IF BITAND(tnFlags,SENDIT) = SENDIT THEN
				IF .lSendInternalEmailIsOn THEN
					.cBodyText = .cBodyText + tcExpression + CRLF + CRLF
				ENDIF
			ENDIF
		ENDWITH
	ENDPROC  &&  TrackProgress


	FUNCTION GetSQLStartDateTimeString
		* returns a timestamp string for use in SQL queries
		LPARAMETERS tdDate
		LOCAL lcSetDate, lcSQLDateTimeString
		lcSetDate = SET('DATE')
		SET DATE YMD
		lcSQLDateTimeString   = "{ts '" + TRANSFORM(YEAR(tdDate)) + "-" + PADL(MONTH(tdDate),2,"0") + "-" + PADL(DAY(tdDate),2,"0") + " 00:00:00.0'}"
		SET DATE &lcSetDate.
		RETURN lcSQLDateTimeString
	ENDFUNC


	FUNCTION GetSQLEndDateTimeString
		* returns a timestamp string for use in SQL queries
		LPARAMETERS tdDate
		LOCAL lcSetDate, lcSQLDateTimeString
		lcSetDate = SET('DATE')
		SET DATE YMD
		lcSQLDateTimeString = "{ts '" + TRANSFORM(YEAR(tdDate)) + "-" + PADL(MONTH(tdDate),2,"0") + "-" + PADL(DAY(tdDate),2,"0") + " 23:59:59.9'}"
		SET DATE &lcSetDate.
		RETURN lcSQLDateTimeString
	ENDFUNC


ENDDEFINE
