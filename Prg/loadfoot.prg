

*******************************************************************************************************
* Process Footstar pm data to produce a bill.
*
*******************************************************************************************************
LPARAMETERS tcMode

LOCAL loFootstarProc

_screen.Caption = "Loading Footstar Bill Data..."

SET PROCEDURE TO F:\FOOTSTAR\PRG\FOOTCLASS

loFootstarProc = CREATEOBJECT('FOOTSTAR')

IF NOT EMPTY(tcMode) AND UPPER(ALLTRIM(tcMode)) = "AUTO" THEN
	loFootstarProc.SetAutoMode()
ENDIF

loFootstarProc.Load()

RETURN
