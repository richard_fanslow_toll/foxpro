*!* This is a generic form import program for CR8REC transfer sheets, etc.

*SET STEP ON
IF lTesting
	lTestMail = .T.
ENDIF

SELECT xinwolog
SET ORDER TO inwologid
LOCATE
STORE RECCOUNT() TO lnCount
WAIT WINDOW AT 10,10 "Container lines to load ...........  "+STR(lnCount) NOWAIT TIMEOUT 1

nUploadcount = 0
cRecs = ALLTRIM(STR(lnCount))

*ASSERT .F. MESSAGE "At XINWOLOG Import loop...debug"
cCtrString = PADR("WO NUM",10)+PADR("CONTAINER",15)+PADR("CTNS",6)+"PRS"+CHR(13)

*SET STEP ON
SCAN  && Currently in XINWOLOG table
	cContainer = ALLT(xinwolog.CONTAINER)
	cCtnQty = ALLT(STR(xinwolog.plinqty))
	WAIT "AT XINWOLOG RECORD "+ALLTRIM(STR(RECNO()))+" OF "+cRecs WINDOW NOWAIT NOCLEAR
*	SELECT inwolog

	csq1 = [select * from inwolog where container = ']+PADR(cContainer,11)+[' and accountid = ]+TRANSFORM(nAcctNum)
	xsqlexec(csq1,"tempinwo",,"wh")
	SELECT tempinwo
	LOCATE
	IF RECCOUNT()>0
		WAIT WINDOW "Container "+cContainer+" already exists in INWOLOG...looping" TIMEOUT 6
		DELETE FILE [&cFullName]
		RETURN
	ENDIF

	DO CASE
	CASE cOffice = "X"
		lcWarehouse = "CR1"
	CASE cOffice = "N"
		lcWarehouse = "NJ1"
	CASE cOffice = "M"
		lcWarehouse = "FL1"
	OTHERWISE
		lcWarehouse = "LA1"
	ENDCASE

	SELECT xinwolog
	SCATTER MEMVAR MEMO
	SELECT inwolog
	SET ORDER TO
	m.accountid = nAcctNum
	nwo_num = dygenpk("wonum",cWhseMod)
	m.wo_num = nwo_num
	cWO_Num = ALLT(STR(m.wo_num))

	m.office = cOffice
	m.mod = cMod
	insertinto("inwolog","wh",.T.)

	SELECT inwolog
	nUploadcount = nUploadcount + 1

	SELECT xpl
	SUM totqty TO nGTUnits FOR xpl.units = .T.
	cGTUnits = ALLTRIM(STR(nGTUnits))
	cCtrString = cCtrString+CHR(13)+PADR(cWO_Num,10)+PADR(cContainer,15)+PADR(cCtnQty,6)+cGTUnits  && ,cUnitQty)

	SCAN FOR inwologid = xinwolog.inwologid
		SCATTER MEMVAR MEMO
		m.inwologid = inwolog.inwologid
		m.wo_num = nwo_num
		m.wo_date = DATE()
		m.date_rcvd = DATE()
		m.confirmdt = {  /  /    }
		m.acctname = IIF(nAcctNum = 6221,"BCNY INTERNATIONAL, INC.","SYNCLAIRE BRANDS")
		m.accountid = nAcctNum
		m.office = cOffice
		m.mod = cMod
		insertinto("pl","wh",.T.)
	ENDSCAN
ENDSCAN

SELECT inwolog
STORE "" TO xtruckwonum, xtruckseal
DO "m:\dev\prg\whtruckwonum.prg" WITH inwolog.wo_num, inwolog.REFERENCE, inwolog.CONTAINER, inwolog.accountid
REPLACE inwolog.truckwonum WITH xtruckwonum, inwolog.seal WITH xtruckseal IN inwolog

SELECT inwolog
SCATTER MEMVAR MEMO BLANK
IF !lDoSQL
	SELECT pl
	SCATTER MEMVAR MEMO BLANK
ENDIF

IF lDoSQL
	tu("inwolog")
	tu("pl")
ENDIF

WAIT CLEAR

WAIT WINDOW AT 10,10 "Container records loaded...........  "+STR(nUploadcount) TIMEOUT 2

WAIT CLEAR
IF nUploadcount > 0
*	SET STEP ON
	tsubject= "Inbound WO(s) created for "+cCustname

	IF lTesting
		tsubject= tsubject+" (TEST) at "+TTOC(DATETIME())
	ELSE
		tsubject = tsubject+ICASE(cOffice = "C"," (SP)",cOffice = "X"," (CR)",cOffice = "M"," (FL)",cOffice = "I"," (NJ)","(UNK) ")+"at "+TTOC(DATETIME())
	ENDIF
	tattach = ""
	tmessage = cCtrString
	tmessage = tmessage + CHR(13) + CHR(13) + "From generic file: "+cFilename
	IF lTesting
		tmessage = tmessage + CHR(13) + "Data is in F:\WHP\WHDATA"
	ENDIF
	tFrom ="Toll WMS EDI Operations <toll-edi-ops@tollgroup.com>"
	IF lDoMail
		DO FORM m:\dev\frm\dartmail2 WITH tsendto,tFrom,tsubject,tcc,tattach,tmessage,"A"
	ENDIF
	cCtrString = ""
	WAIT cCustname+" Inbound Import Process Complete for file "+cFilename WINDOW TIMEOUT 2
ENDIF

RETURN
