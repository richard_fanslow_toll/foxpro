**kpi.prg
**
**Key Performance Indicators
**mvw 04/12/05
**
**Files open on calling: sysData.account, main.offices, csrOffices
Parameters xrptname, cAcctNum, cAcctName, cOffice, dStartDt, dEndDt, lDetail, cEmailList, cCCList, ;
		   cBuilding, cMod, xnotinvenacct
**cAcctNum: acct no. (char)
**cBuilding: "SP1", "SP2" or .F. (if ommitted)
**removed the MOD - no longer used and complex code including acctgrp, etc. - mvw 12/22/15


**xnoalert: do not display messageboxes for auto run processes - mvw 03/03/14
xnoalert=iif(type("xnoalert")="U", .f., xnoalert)

cWhseAcct = Iif(Empty(cAcctNum), "", "accountId = "+cAcctNum+" and ")
cTrkAcct = Iif(Empty(cAcctNum), "", "accountid = "+cAcctNum+" and ")

if !inlist(lower(xrptname),"inventory accuracy","kpioctmj","inbound edi","outbound edi","drayage time","daily core objectives")
	do case
	case (inlist(val(cAcctNum),&gmoretacctlist) or inlist(val(cAcctNum),&gmoretacctlist2)) and messagebox("Include all Moret accounts?",4+16,"Moret")=6
		cWhseAcct = "(inlist(accountId,"+gmoretacctlist+") or inlist(accountId,"+gmoretacctlist2+")) and "
		cTrkAcct = "(inlist(accountId,"+gmoretacctlist+") or inlist(accountId,"+gmoretacctlist2+")) and "
		cacctname = "ALL MORET ACCOUNTS"
	case inlist(val(cAcctNum),&gmjacctlist) and messagebox("Include all MJ accounts?",4+16,"MJ")=6
		cWhseAcct = "inlist(accountId,"+gmjacctlist+") and "
		cTrkAcct = "inlist(accountId,"+gmjacctlist+") and "
		cacctname = "ALL MJ ACCOUNTS"
	endcase
endif

cLocFld = Iif(cOffice = "N", "njInven", Iif(cOffice = "M", "flInven", Iif(cOffice = "K", "kyInven", Iif(cOffice = "O", "onInven", Iif(cOffice = "X", "xxInven", Iif(cOffice = "Y", "yyInven", Iif(cOffice = "Z", "zzInven", "caInven")))))))
xtrkoffice=iif(inlist(coffice,"L","R"),"C",coffice)

store "" to cFile, cEmailFile, cSubject &&prefdefine, values given in some kpi prgs


**Comments/Questions:
**  - need to update whWonum in main.wolog in crt_wo.scx for trucking wos. created after whse wo (check for ctrPrfx+left(container,6) and wo_date within 90 days?)
**		- make sure darren has a plan for the update of inWolog.truckwonum for wos. that are created after the trucking wo
**		- all reports are done on the assumption that whWonum is updated accordingly!
**		- may need to "backload" all whWonums prior to field creation
**  - need to update availDt in wh.inwolog when update pickedUp for trucking wos.
**		- if a whse wo is created for multiple trucking wos, the pickedUp date of one isnt't necessarily the same as the others????!!
**		- make sure darren's has a plan for the update of availDt for wos. not picked up by FMI
**  - may need to keep as its own exe b/c databases span data sessions?? need to open and close the different 
**		wh databases... may cause a problem from within wh system
**

oWorkbook = ""

Wait window "Opening necessary databases..." nowait noclear

public oexcel
oExcel = CreateObject("Excel.Application")

if !xnotinvenacct and xinvenrpt
	xdatabase=coffice
	xoffice=coffice

	DO case
	  Case inlist(cOffice,"N","C")
		do case
		  case !empty(cbuilding)
			cwhdir="f:\wh"+cbuilding+"\whdata\"
			xoffice=cbuilding
			open database &cwhdir.wh

		  case !empty(cmod)
			**removed cmod code, do not believe this is used any longer - mvw 12/22/15
			messagebox("Code for MOD lookup removed! Please contact MIS",16,"Invalid Pararmeter")
			return .f.
*!*				lfound=.f.
*!*				for i=1 to 5 &&scan thru 5 wh dirs
*!*					if inlist(i,3,4)	&& dy 6/29/09
*!*						loop
*!*					endif
*!*					cpath="f:\wh"+transform(i)+"\whdata\wh"
*!*					open database &cpath
*!*					select 0
*!*					use wh!acctgrp
*!*					locate for upper(groupname) = upper(cmod)
*!*					if found()
*!*						lfound=.t.
*!*						use in acctgrp
*!*						exit
*!*					endif
*!*					set database to wh
*!*					close databases
*!*				endfor

*!*				if !lfound
*!*					messagebox("MOD not found",16,"Error")
*!*					return
*!*				endif

		  case !empty(cacctnum)
			cwhdir=wf(coffice, val(cacctnum), .f.)
			xoffice=whbldg(coffice, val(cacctnum))
			open database &cwhdir.wh
		endcase

	  Case cOffice = "K"
		goffice=""
		xoffice=""
		do case
		case !empty(cbuilding)
			cwhdir="f:\wh"+cbuilding+"\whdata\"
			xoffice=cbuilding
			goffice=xoffice
			open database &cwhdir.wh
		case !empty(cacctnum)
			cwhdir=wf(coffice, val(cacctnum), .f.)
			xoffice=whbldg(coffice, val(cacctnum))
			goffice=xoffice
			open database &cwhdir.wh
		otherwise
			open database f:\whk\whdata\wh
			goffice="K"
		endcase

	  otherwise
		cwhdir="f:\wh"+coffice+"\whdata\"
		open database &cwhdir.wh
	EndCase
endif

cPnpFilter = "pnp"+iif(empty(coffice),"N",alltrim(coffice))

do case
case lower(xrptname)="inventory accuracy"
	**Inventory Accuracy
	=kpiInvenAcc()

case lower(xrptname)="oct"
	**Order Cycle Time
	=kpioct()

case lower(xrptname)="kpioctmj"
	**Custom Order Cycle Time Reports for Marc Jacobs
	=kpioctmj()

case lower(xrptname)="fill rate"
	**Fill Rate
	=kpiFillRate()

case lower(xrptname)="inbound edi"
	**Inbound EDI Timeliness
	=kpiEdiTime("Inbound", "944")

case lower(xrptname)="outbound edi"
	**Outbound EDI Timeliness
	=kpiEdiTime("Outbound", "945")

case lower(xrptname)="drayage time"
	**Drayage Time
	=kpiDrayTime()

case lower(xrptname)="yard to unload"
	**Yard to Unload Time
	=kpiytu()

case lower(xrptname)="yard to stripped"
	**Yard to Stripped
	=kpiyts()

case lower(xrptname)="daily core objectives"
	**Core Objectives
	=kpicoreobj()
*	=kpicoreobjsql()
endcase

**close the database
if dbused("wh")
	set database to wh
	close databases
endif

If Type("oWorkbook") = "O"
	If Empty(cEmailList)
		oExcel.visible=.t.
	Else
		oExcel.quit &&release the file before attempting to send

		if lower(xrptname)="daily core objectives" and cbuilding="I" and used("kpilog")
			replace step13 with .t. in kpilog
		endif

		**send the report
		xsendto = cEmailList
		xcc = cCCList
		cAttach = cEmailFile
		xfrom ="TGF WMS Operations <fmicorporate@fmiint.com>"
		cMessage = cSubject

		if lower(xrptname)="daily core objectives" and cbuilding="I" and used("kpilog")
			xdata="cTo = "+cEmailList+chr(13)+;
				   "cCc = "+cCCList+chr(13)+;
				   "cAttach = "+cEmailFile+chr(13)+;
				   "cMessage = "+cSubject+chr(13)
			replace step14 with .t., memo with xdata in kpilog
		endif

*		DO FORM dartmail2 WITH xsendto,xfrom,cSubject,xcc,cAttach,cMessage,"A"
		xsendto=strtran(xsendto,",",";") &&change to semicolon for emil.prg
		xcc=strtran(xcc,",",";") &&change to semicolon for emil.prg

*!*			**using email() we need to break out multiple attachments into array - mvw 08/26/13
*!*			if occurs(",",cattach)>0
*!*				dimension xarray[occurs(",",cattach)+1]

*!*				for i = 1 to occurs(",",cattach)+1
*!*					do case
*!*					case i=1
*!*						xarray[i]=left(cattach,at(",",cattach,1)-1)
*!*					case occurs(",",cattach)=i-1
*!*						xarray[i]=right(cattach,len(cattach)-at(",",cattach,i-1))
*!*					otherwise
*!*						xarray[i]=substr(cattach,at(",",cattach,i-1)+1,at(",",cattach,i)-(at(",",cattach,i-1)+1))
*!*					endcase
*!*				endfor

*!*				do email with "mike.winter@tollgroup.com",csubject,cmessage,xarray,.t.,"",.t.,,,,,.f.,xfrom
*!*			else
*!*				email("mike.winter@tollgroup.com",csubject,cmessage,cattach,.f.,"",.t.,,,,,.f.,xfrom)
*!*			endif

		**using emailx() we need to break out multiple attachments in xattach1, xattach2,...,xattach6 - mvw 08/26/13
		xattach1=cattach
		store "" to xattach2, xattach3, xattach4, xattach5, xattach6

		if occurs(",",cattach)>0
			for i = 1 to 6
				xvar="xattach"+transform(i)
				do case
				case i=1
					&xvar=left(cattach,at(",",cattach,1)-1)
				case occurs(",",cattach)=i-1
					&xvar=right(cattach,len(cattach)-at(",",cattach,i-1))
					exit
				otherwise
					&xvar=substr(cattach,at(",",cattach,i-1)+1,at(",",cattach,i)-(at(",",cattach,i-1)+1))
				endcase
			endfor
		endif

		**changed to emailx() bc of issues with toll email addrs not receiving emails - mvw 08/22/13
		emailx(xsendto,xcc,csubject,cmessage,.f.,xattach1,xattach2,xattach3,xattach4,xattach5,xattach6)

		if lower(xrptname)="daily core objectives" and cbuilding="I" and used("kpilog")
			replace step15 with .t. in kpilog
		endif
	endif
endif

release oworkbook, oexcel
