lparameters xform, xsaving

do case
case !wheditcheck(xform,xsaving,"OBBL","B/L")
	return .f.
case !wheditcheck(xform,xsaving,"OBIN","Inbound")
	return .f.
case !wheditcheck(xform,xsaving,"OBOUT","Outbound")
	return .f.
endcase


****
procedure wheditcheck

lparameters xform, xsaving, xcheckform, xdesc

if xform#xcheckform
	xdy = ascan(oforms.iaforminstances,xcheckform,1)
	if xdy#0
		xdy = asubscript(oforms.iaforminstances,xdy,1)
		osource = oforms.iaforminstances[xdy,1]
		if osource.getpprop("icMode")#"DEFAULT"
			if xsaving
				x3winmsg("You can't save changes on this screen until you cancel your changes on the "+xdesc+" screen.")
			else	
				x3winmsg("You won't be able to save changes on this screen unless you cancel your changes on the "+xdesc+" screen.")
			endif
*			email("dyoung@fmiint.com","INFO: User "+trim(guserid)+" in "+goffice+" trying to "+iif(xsaving,"save ","edit ")+xform+" while changing "+xcheckform,,,,,.t.,,,,,.t.)
			return .f.
		endif
	endif
endif
