use f:\wo\wodata\manifest in 0
use f:\wo\wodata\daily in 0
use f:\wo\wodata\tdaily in 0

if !empty(cponum)
	xfilter="str(accountid,4)+po_num=str(xaccountid,4)+cponum and complete"
else
	xfilter="accountid=xaccountid and between(wo_date,xstartdt,xenddt) and complete"
endif

select wo_num from manifest where &xfilter into cursor csrwonums

select max(wo_num) as maxwo, min(wo_num) as minwo from csrwonums into cursor xtemp
xsqlexec("select * from fxtrips where wo_num>="+transform(minwo)+" and wo_num<="+transform(maxwo)+" and accountid="+transform(xaccountid),"fxtrips",,"fx")
use in xtemp

select mfst.*, ;
	iif(isnull(daily.dispatched), tdaily.dispatched, daily.dispatched) as "dispDt", ;
	fxtrips.dispatched as "fxDispDt" ;
	from csrwonums ;
	left join manifest mfst on csrwonums.wo_num = mfst.wo_num ;
	left join daily  on daily.wo_num = csrwonums.wo_num ;
	left join tdaily on tdaily.wo_num = csrwonums.wo_num ;
	left join fxtrips on fxtrips.wo_num = csrwonums.wo_num ;
	; &&group by b/c may have multiple recs per w/o in disp.daily
	group by mfst.wo_num, mfst.orig_wo, mfst.hawb, mfst.po_num, mfst.style, mfst.delloc ;
	into cursor csrmfst

use in csrwonums


set step on 

insert into pdf_file (unique_id) values (reports.unique_id)
assert .f.

if eof() &&need something to distinguish from "poller down"
	select pdf_file
	replace memo with "NO DATA"
else
	public oexcel
	oexcel = createobject("Excel.Application")
	oexcel.displayalerts = .f.

	cfile = tempfox+alltrim(str(reports.unique_id))+".xls"
	copy file f:\webrpt\frttrack.xls to (cfile)

	oworkbook = oexcel.workbooks.open(cfile)
	oexcel.activesheet.range("A2").value = "Account: "+alltrim(acctname)

	nrownum = 4
	select csrmfst
	do while !eof()
		cpolist = ""
		ntotqty = 0

		ddate = {} &&will remain empty if !COMPLETE
		do case
		case !isnull(dispdt)
			ddate = dispdt
		case !isnull(fxdispdt)
			ddate = fxdispdt
		case complete
			ddate = wo_date
		endcase

		do case
		case delloc = "SHEPARDS" &&shepardsville
			ntransit = 7 &&7 days
		otherwise &&greensboro, swedesboro, etc??
			ntransit = 8 &&8 days
		endcase

		do insertrow with nrownum, trailer, wo_num, carrier, delloc, "", eventcode, "", ddate, ddate+ntransit

		nwonum = wo_num
		do while wo_num = nwonum
			cpolist = iif(empty(cpolist), "", cpolist+",")+alltrim(po_num)
			ntotqty = ntotqty + quantity
			skip
		enddo

		crownum = alltrim(str(nrownum, 5, 0))
		oexcel.activesheet.range("E"+crownum).value = cpolist
		oexcel.activesheet.range("G"+crownum).value = ntotqty

		nrownum = nrownum+1
	enddo

	oworkbook.save()
	oworkbook.close()
	oexcel.quit()
	release oworkbook, oexcel

	select pdf_file
	**in order to avoid "VFPOLEDB" "provider ran out of memory" errors for large files (>2mb) no longer move file 
	**  thru linked server, just copy file directly to \\iis2\apps\CargoTrackingWeb\reports\ - mvw 03/08/13
	*append memo memo from &cfile overwrite
	copy file "&cfile" to \\iis2\apps\CargoTrackingWeb\reports\*.*
	replace memo with "FILE COPIED TO REPORT FOLDER" in pdf_file
endif

use in csrmfst
use in manifest
use in daily
use in tdaily
use in fxtrips


*******************
procedure insertrow

parameters nrow, ;
cfielda, cfieldb, cfieldc, cfieldd, cfielde, cfieldf, ;
cfieldg, cfieldh, cfieldi

with oexcel.activesheet
	crownum = alltrim(str(nrow, 5, 0))
	.range("A"+crownum).value = iif(!empty(cfielda), cfielda, "")
	.range("B"+crownum).value = iif(!empty(cfieldb), cfieldb, "")
	.range("C"+crownum).value = iif(!empty(cfieldc), cfieldc, "")
	.range("D"+crownum).value = iif(!empty(cfieldd), cfieldd, "")
	.range("E"+crownum).value = iif(!empty(cfielde), cfielde, "")
	.range("F"+crownum).value = iif(!empty(cfieldf), cfieldf, "")
	.range("G"+crownum).value = iif(!empty(cfieldg), cfieldg, "")
	.range("H"+crownum).value = iif(!empty(cfieldh), cfieldh, "")
	.range("I"+crownum).value = iif(!empty(cfieldi), cfieldi, "")
endwith
