* Create spreadsheet & email of Hours summary for Salaried Employees for the CURRENT or PRIOR pay period.
* To be run each Friday.
LOCAL loKronosTRReportSALARIED, llSpecial

llSpecial = .T.

IF NOT llSpecial THEN
	utilsetup("KRONOSTRREPORT-SALARIED")
ENDIF

loKronosTRReportSALARIED = CREATEOBJECT('KronosTRReportSALARIED')
USE F:\UTIL\KRONOS\DATA\TIMERVWR.DBF AGAIN IN 0 ALIAS RVWRLIST


IF llSpecial THEN
	loKronosTRReportSALARIED.SetSpecial()
	SELECT RVWRLIST
	SCAN FOR SPECIAL AND LACTIVE AND (NOT LHOURLY)
		loKronosTRReportSALARIED.MAIN( RVWRLIST.TIMERVWR, RVWRLIST.NAME, RVWRLIST.EMAIL, RVWRLIST.CC )
	ENDSCAN
	SCAN FOR SPECIAL AND (NOT LHOURLY) AND (ALLTRIM(TIMERVWR) == 'ALL')
		loKronosTRReportSALARIED.MAIN( RVWRLIST.TIMERVWR, RVWRLIST.NAME, RVWRLIST.EMAIL, RVWRLIST.CC )
	ENDSCAN
ELSE
	* normal case
	SELECT RVWRLIST
	SCAN FOR LACTIVE AND (NOT LHOURLY)
		loKronosTRReportSALARIED.MAIN( RVWRLIST.TIMERVWR, RVWRLIST.NAME, RVWRLIST.EMAIL, RVWRLIST.CC )
	ENDSCAN
	SCAN FOR (NOT LHOURLY) AND (ALLTRIM(TIMERVWR) == 'ALL')
		loKronosTRReportSALARIED.MAIN( RVWRLIST.TIMERVWR, RVWRLIST.NAME, RVWRLIST.EMAIL, RVWRLIST.CC )
	ENDSCAN
ENDIF && llSpecial

IF NOT llSpecial THEN
	schedupdate()
ENDIF

CLOSE DATABASES ALL
RETURN

#DEFINE LOGIT 1
#DEFINE WAITIT 2
#DEFINE NOWAITIT 4
#DEFINE SENDIT 8
#DEFINE CRLF CHR(13) + CHR(10)
#DEFINE RETURN_DATA_MANDATORY .F.
#DEFINE RETURN_DATA_NOT_MANDATORY .T.

DEFINE CLASS KronosTRReportSALARIED AS CUSTOM

	cProcessName = 'KronosTRReportSALARIED'

	* processing properties
	lSpecial = .F.
	lTestMode = .F.  && .F. FOR NORMAL OPERATION, .T. TO SUPPRESS ALL BUT DEVELOPER EMAILS AND CHANGE INPUT TABLE, ETC.
	lCurrentWeek = .F.  && .T. to report on current week (default); .F. to report on prior week
	cWhichWeek = ''
	lIncludeAllTimeReviewers = .F.
	cTimeReviewerList = ''
	
	lAutoYield = .T.

	cStartTime = TTOC(DATETIME())

	* Excel properties
	oExcel = NULL

	* connection properties
	nSQLHandle = 0

	* file properties
	nFileHandle = 0

	* table properties
	cFoxProTimeReviewerTable = "F:\UTIL\KRONOS\DATA\TIMERVWR"

	* date properties
	dtNow = DATETIME()
	
	dToday = DATE()
	
	cToday = DTOC(DATE())

	* wait window properties
	nWaitWindowTimeout = 2
	lWaitWindowIsOn = .T.

	* logfile properties
	lLoggingIsOn = .T.
	cLogFile = 'F:\UTIL\KRONOS\Logfiles\KronosTRReportSALARIED_log.txt'

	* INTERNAL email properties
	lSendInternalEmailIsOn = .T.
	cFrom = 'FMI Corporate <fmicorporate@fmiint.com>'
	cSendTo = 'mbennett@fmiint.com'
	cCC = 'MarieDesaye@fmiint.com, mbennett@fmiint.com'  && lucille.waldrip@tollgroup.com, 
	cSubject = 'Kronos Salaried Attendance Report for: ' + TTOC(DATETIME())
	cAttach = ''
	cBodyText = ''

	cCOMPUTERNAME = ''
	cUSERNAME = ''

	FUNCTION INIT
		IF NOT DODEFAULT()
			RETURN .F.
		ENDIF
		WITH THIS
			LOCAL loError
			CLEAR
			*SET RESOURCE OFF
			CLOSE DATA
			SET CENTURY ON
			SET DATE YMD
			SET HOURS TO 24
			SET ANSI ON
			SET TALK OFF
			SET DELETED ON
			SET CONSOLE OFF
			SET EXCLUSIVE OFF
			SET SAFETY OFF
			SET EXACT OFF
			SET STATUS BAR ON
			SET SYSMENU OFF
			SET ENGINEBEHAVIOR 70
			_VFP.AUTOYIELD = .lAutoYield
			.cCOMPUTERNAME = GETENV("COMPUTERNAME")
			.cUSERNAME = GETENV("USERNAME")
			IF .lTestMode THEN
				.cSendTo = 'mbennett@fmiint.com'
				.cLogFile = 'F:\UTIL\KRONOS\Logfiles\KronosTRReportSALARIED_log_TESTMODE.txt'
			ENDIF
			.lLoggingIsOn = .lLoggingIsOn AND NOT EMPTY(.cLogFile)
			IF .lLoggingIsOn THEN
				SET ALTERNATE TO (.cLogFile) ADDITIVE
				SET ALTERNATE ON
			ENDIF
			* re-init date properties after all SETTINGS
			.dtNow = DATETIME()
			.dToday = DATE()
			.cToday = DTOC(DATE())
			.cAttach = ''

			WAIT WINDOW NOWAIT "Opening Excel..."
			TRY
				.oExcel = CREATEOBJECT("excel.application")
				.oExcel.VISIBLE = .F.
			CATCH
				.oExcel = NULL
			ENDTRY
		ENDWITH
	ENDFUNC


	FUNCTION DESTROY
		WITH THIS
			IF .lLoggingIsOn  THEN
				SET ALTERNATE OFF
				SET ALTERNATE TO
			ENDIF
			IF TYPE('.oExcel') = 'O' AND NOT ISNULL(.oExcel) THEN
				.oExcel.QUIT()
			ENDIF
		ENDWITH
		DODEFAULT()
	ENDFUNC


	FUNCTION MAIN

		LPARAMETERS tcTRNumber, tcTRName, tcTReMail, tcCC

		LOCAL lnNumberOfErrors, lcSQL, lcSQL2, lcSQL3, lcSQL4, loError, lcTopBodyText, llValid
		LOCAL lcXLFileName, ldStartDate, ldEndDate, lcStartDate, lcEndDate, ldDate
		LOCAL lcSQLStartDate, lcSQLEndDate, lcXLFileName, lcSpecialtext
		LOCAL oWorkbook, oWorksheet, lnRow, lcRow, lnStartRow, lcStartRow, lnEndRow, lnPP, lcPP, lnNN, lcNN, lcJJ
		LOCAL lcColsToProcess, lnColumn, lcBoldColumn, i, j, lcBoldRow, luCellValue, lcTargetDirectory, lcFiletoSaveAs
		LOCAL lcFileDate, lcDivision, lcRateType, lcTitle, llSaveAgain, lcPayCode
		LOCAL lcTRName, lcTReMail, lcTRNumber, lcCC, lcTestCC, lcFoxProTimeReviewerTable &&, lcMissingApprovals
		LOCAL ldCurrentDate, lnDayCount, lcTRWhere

		WITH THIS

			TRY
				lnNumberOfErrors = 0
				lcTopBodyText = ''
				lcTRNumber = tcTRNumber
				lcTRName = ALLTRIM(tcTRName)
				lcTReMail = ALLTRIM(tcTReMail)
				lcCC = ALLTRIM(tcCC)
								
				* repeat some setup
				SET CONSOLE OFF
				SET TALK OFF
				.cSendTo = 'mbennett@fmiint.com'
				.cCC = 'MarieDesaye@fmiint.com, mbennett@fmiint.com'  && lucille.waldrip@tollgroup.com, 
				.cStartTime = TTOC(DATETIME())
				.dtNow = DATETIME()
				.cAttach = ''

				IF .lTestMode THEN
					.cSendTo = 'mbennett@fmiint.com'
					IF NOT EMPTY(lcCC) THEN
						lcTestCC = lcCC + ", " + .cCC
					ELSE
						lcTestCC = .cCC
					ENDIF
					.cCC = ""
				ELSE
					.cSendTo = lcTReMail
					IF NOT EMPTY(lcCC) THEN
						.cCC = lcCC + ", " + .cCC
					ENDIF
				ENDIF

				.cBodyText = ""

				*.TrackProgress('==================================================================================================================', LOGIT+SENDIT)
				.TrackProgress('Kronos Salaried Attendance Report process started....', LOGIT+SENDIT+NOWAITIT)
				.TrackProgress('COMPUTERNAME = ' + .cCOMPUTERNAME, LOGIT+SENDIT)
				.TrackProgress('USERNAME = ' + .cUSERNAME, LOGIT+SENDIT)
				.TrackProgress('==================================================================================================================', LOGIT+SENDIT)
				IF .lTestMode THEN
					.TrackProgress('', LOGIT)
					.TrackProgress('==================================================================================================================', LOGIT+SENDIT)
					.TrackProgress('                        TEST MODE                        ', LOGIT+SENDIT)
					.TrackProgress('Time Reviewer # = ' + lcTRNumber, LOGIT+SENDIT)
					.TrackProgress('Time Reviewer Name = ' + lcTRName, LOGIT+SENDIT)
					.TrackProgress('cSendTo would be: ' + lcTReMail, LOGIT+SENDIT)
					.TrackProgress('cCC would be: ' + lcTestCC, LOGIT+SENDIT)
					.TrackProgress('==================================================================================================================', LOGIT+SENDIT)
				ENDIF
				.TrackProgress('', LOGIT)

				* calculate pay period start/end dates.
				ldDate = .dToday
				
				* to force re-running older weeks
				*ldDate = {^2013-10-25}



*!*	***************************************************
*!*	* force later date for testing unapproved warnings
*!*	IF .lTestMode THEN
*!*		ldDate = .dToday + 7
*!*	ENDIF
*!*	***************************************************
				
				IF .lCurrentWeek THEN
					* report on current week.
					* We go back until we hit Sunday, which becomes the start date.
					DO WHILE DOW(ldDate,1) <> 1
						ldDate = ldDate - 1
					ENDDO
					ldStartDate = ldDate
					ldEndDate = ldStartDate + 6
					.cWhichWeek = "Current"
				ELSE				
					* report on prior week.
					* We go back from today until we hit a Saturday (DOW = 7) , which is the end day of the previous pay period.
					* then we subtract 6 days from that to get the prior Sunday (DOW = 1), which is the start day of the previous week.
					DO WHILE DOW(ldDate,1) <> 7
						ldDate = ldDate - 1
					ENDDO
					ldEndDate = ldDate
					ldStartDate = ldDate - 6				
					.cWhichWeek = "Prior"
				ENDIF

				SET DATE AMERICAN

				lcStartDate = STRTRAN(DTOC(ldStartDate),"/","-")
				lcEndDate = STRTRAN(DTOC(ldEndDate),"/","-")

				SET DATE YMD

				*	 create "YYYYMMDD" safe date format for use in SQL query
				* NOTE: for SQL we are going to use < EndDate ( not <= ), so we are going to add 1 to that date here for querying purposes.
				lcSQLStartDate = STRTRAN(DTOC(ldStartDate),"/","")
				lcSQLEndDate = STRTRAN(DTOC(ldEndDate + 1),"/","")

				.cSubject = 'Salaried Attendance Report for ' + lcTRName + ': ' + lcStartDate + " to " + lcEndDate + " (" + .cWhichWeek + " Week)"

				IF USED('CURWTKHOURS') THEN
					USE IN CURWTKHOURS
				ENDIF

*!*					IF USED('CURAPPROVALS') THEN
*!*						USE IN CURAPPROVALS
*!*					ENDIF
				
				IF USED('CURALLTREMPS') THEN
					USE IN CURALLTREMPS
				ENDIF
				
				IF USED('CURDAILYPRE') THEN
					USE IN CURDAILYPRE
				ENDIF
				
				IF USED('CURDAILY') THEN
					USE IN CURDAILY
				ENDIF

				IF USED('CURDISTINCTEMPS') THEN
					USE IN CURDISTINCTEMPS
				ENDIF
				

*!*					IF USED('CURMISSINGAPPROVALS') THEN
*!*						USE IN CURMISSINGAPPROVALS
*!*					ENDIF


				* open FoxPro Time Reviewer table
				IF USED('TIMERVWRTABLE') THEN
					USE IN TIMERVWRTABLE
				ENDIF
				lcFoxProTimeReviewerTable = .cFoxProTimeReviewerTable

				USE (lcFoxProTimeReviewerTable) AGAIN IN 0 ALIAS TIMERVWRTABLE
				
				IF UPPER(ALLTRIM(lcTRNumber)) == 'ALL' THEN
					.lIncludeAllTimeReviewers = .T.
					.TrackProgress('.lIncludeAllTimeReviewers = .T.', LOGIT+SENDIT)
				ELSE
					.lIncludeAllTimeReviewers = .F.
					.TrackProgress('.lIncludeAllTimeReviewers = .F.', LOGIT+SENDIT)
				ENDIF

				IF .lIncludeAllTimeReviewers THEN
					* special report for Lucille will include employees for all valid Time Reviewers
					.cTimeReviewerList = .GetActiveSalariedTimeReviewerList()
					lcTRWhere1 = " AND (D.LABORLEV7NM IN (" + .cTimeReviewerList + "))"
					lcTRWhere2 = " AND (HOMELABORLEVELNM7 IN (" + .cTimeReviewerList + "))"
				ELSE
					* standard case, employees for single time reviewer
					lcTRWhere1 = " AND (D.LABORLEV7NM = " + lcTRNumber + ")"
					lcTRWhere2 = " AND (HOMELABORLEVELNM7 = " + lcTRNumber + ")"
				ENDIF

				* main SQL
				lcSQL = ;
					" SELECT " + ;
					" D.LABORLEV2NM AS DIVISION, " + ;
					" D.LABORLEV3NM AS DEPT, " + ;
					" D.LABORLEV7NM AS TIMERVWR, " + ;
					" D.LABORLEV7DSC AS TRNAME, " + ;
					" C.FULLNM AS EMPLOYEE, " + ;
					" C.PERSONNUM AS FILE_NUM, " + ;
					" C.PERSONID, " + ;
					" E.ABBREVIATIONCHAR AS PAYCODE, " + ;
					" E.NAME AS PAYCODEDESC, " + ;
					" (SUM(A.DURATIONSECSQTY) / 3600.00) AS TOTHOURS, " + ;
					" SUM(A.MONEYAMT) AS TOTPAY " + ;
					" FROM WFCTOTAL A " + ;
					" JOIN WTKEMPLOYEE B " + ;
					" ON B.EMPLOYEEID = A.EMPLOYEEID " + ;
					" JOIN PERSON C " + ;
					" ON C.PERSONID = B.PERSONID " + ;
					" JOIN LABORACCT D " + ;
					" ON D.LABORACCTID = A.LABORACCTID " + ;
					" JOIN PAYCODE E " + ;
					" ON E.PAYCODEID = A.PAYCODEID " + ;
					" WHERE (A.APPLYDTM >= '" + lcSQLStartDate + "')" + ;
					" AND (A.APPLYDTM <= '" + lcSQLEndDate + "')" + ;
					" AND (D.LABORLEV1NM IN ('AXA','ZXU')) " + ;
					lcTRWhere1 + ;
					" GROUP BY D.LABORLEV2NM, D.LABORLEV3NM, D.LABORLEV7NM, D.LABORLEV7DSC, C.FULLNM, C.PERSONNUM, C.PERSONID, E.ABBREVIATIONCHAR, E.NAME " + ;
					" ORDER BY 1, 2, 3, 4, 5, 7 "
					
*!*						" AND (D.LABORLEV7NM = " + lcTRNumber + ")" + ;


				* SQL for getting all employees for the time reviewer, whether they had pay code activity or not.
				lcSQL3 = ;
					" SELECT " + ;
					" PERSONNUM AS FILE_NUM, " + ;
					" PERSONFULLNAME AS EMPLOYEE, " + ;						
					" HOMELABORLEVELNM2 AS DIVISION, " + ;
					" HOMELABORLEVELNM3 AS DEPT, " + ;
					" HOMELABORLEVELNM7 AS TIMERVWR, " + ;
					" HOMELABORLEVELDSC7 AS TRNAME " + ;
					" FROM VP_EMPLOYEEV42 " + ;
					" WHERE (HOMELABORLEVELNM1 IN ('AXA','ZXU')) " + ;
					lcTRWhere2 + ;
					" ORDER BY PERSONNUM "
					
*!*						" AND (HOMELABORLEVELNM7 = " + lcTRNumber + ")" + ;

				* This is sql for getting Daily info, also includes applydtm
				lcSQL4 = ;
					" SELECT " + ;
					" D.LABORLEV2NM AS DIVISION, " + ;
					" D.LABORLEV3NM AS DEPT, " + ;
					" D.LABORLEV7NM AS TIMERVWR, " + ;
					" D.LABORLEV7DSC AS TRNAME, " + ;
					" C.FULLNM AS EMPLOYEE, " + ;
					" C.PERSONNUM AS FILE_NUM, " + ;
					" A.APPLYDTM, " + ;
					" E.NAME AS PAYDESC, " + ;
					" A.DURATIONSECSQTY AS NSECS, " + ;
					" A.MONEYAMT AS NPAY " + ;
					" FROM WFCTOTAL A " + ;
					" JOIN WTKEMPLOYEE B " + ;
					" ON B.EMPLOYEEID = A.EMPLOYEEID " + ;
					" JOIN PERSON C " + ;
					" ON C.PERSONID = B.PERSONID " + ;
					" JOIN LABORACCT D " + ;
					" ON D.LABORACCTID = A.LABORACCTID " + ;
					" JOIN PAYCODE E " + ;
					" ON E.PAYCODEID = A.PAYCODEID " + ;
					" WHERE (A.APPLYDTM >= '" + lcSQLStartDate + "')" + ;
					" AND (A.APPLYDTM <= '" + lcSQLEndDate + "')" + ;
					" AND (D.LABORLEV1NM IN ('AXA','ZXU')) " + ;
					lcTRWhere1 + ;
					" ORDER BY 1, 2, 3, 4, 5, 6, 7 "
					
*!*						" AND (D.LABORLEV7NM = " + lcTRNumber + ")" + ;

				* 5/16/06 MB: Now using APPLYDTM instead of ADJSTARTDTM to solve problem where hours crossing Midnight Saturday were not being reported.
				*					" WHERE (A.ADJSTARTDTM >= '" + lcSQLStartDate + "')" + ;
				*					" AND (A.ADJSTARTDTM < '" + lcSQLEndDate + "')" + ;

				IF .lTestMode THEN
					.TrackProgress('', LOGIT+SENDIT)
					.TrackProgress('lcTRWhere1 = ' + lcTRWhere1, LOGIT+SENDIT)
					.TrackProgress('lcTRWhere2 = ' + lcTRWhere2, LOGIT+SENDIT)
					.TrackProgress('lcSQL =' + lcSQL, LOGIT+SENDIT)
*!*						.TrackProgress('', LOGIT+SENDIT)
*!*						.TrackProgress('lcSQL2 =' + lcSQL2, LOGIT+SENDIT)
					.TrackProgress('', LOGIT+SENDIT)
					.TrackProgress('lcSQL3 =' + lcSQL3, LOGIT+SENDIT)
					.TrackProgress('', LOGIT+SENDIT)
					.TrackProgress('lcSQL4 =' + lcSQL4, LOGIT+SENDIT)
				ENDIF

				.TrackProgress('Connecting to WTK System....', LOGIT+SENDIT)

				* get ss#s from ADP
				OPEN DATABASE F:\UTIL\KRONOS\KRONOSEDI

				*.nSQLHandle = SQLCONNECT("KRONOSEDI","tkcsowner","tkcsowner")
				.nSQLHandle = SQLCONNECT("WTK_ON_PAYROLL2","sa","kr0n1tes")

				IF USED('CURWTKHOURS') THEN
					USE IN CURWTKHOURS
				ENDIF

				IF .nSQLHandle > 0 THEN

					IF .ExecSQL(lcSQL, 'CURWTKHOURS', RETURN_DATA_NOT_MANDATORY) AND ;
							.ExecSQL(lcSQL3, 'CURALLTREMPS', RETURN_DATA_NOT_MANDATORY) AND ;
							.ExecSQL(lcSQL4, 'CURDAILYPRE', RETURN_DATA_NOT_MANDATORY) THEN
							
						SELECT DISTINCT EMPLOYEE, FILE_NUM, TRNAME ;
							FROM CURALLTREMPS ;
							INTO CURSOR CURDISTINCTEMPS ;
							ORDER BY 1, 2
						
*!*			SELECT CURWTKHOURS
*!*			BROWSE
*!*			throw

						SELECT EMPLOYEE, ;
							FILE_NUM, ;
							TTOD(APPLYDTM) AS DATE, ;
							PAYDESC, ;
							TRNAME, ;
							SUM(NSECS) AS TOTSECS ;
							FROM CURDAILYPRE ;
							INTO CURSOR CURDAILY ;
							GROUP BY 1, 2, 3, 4, 5 ;
							ORDER BY 1, 2, 3, 4
					

*!*			SELECT CURDAILY
*!*			BROWSE

		
*!*			X = 2 * .F.

						IF USED('CURALLTREMPS') AND (NOT EOF('CURALLTREMPS')) THEN
						
							* get list of emps for the timereviewer who had no activity
							IF USED('CURHADNOTIME') THEN
								USE IN CURHADNOTIME
							ENDIF
							
							SELECT * FROM CURALLTREMPS ;
								INTO CURSOR CURHADNOTIME ;
								WHERE FILE_NUM NOT IN (SELECT FILE_NUM FROM CURWTKHOURS) ;
							
							* insert emps with no activity into CURWTKHOURS so we are reporting on all emps for the tr
							IF USED('CURHADNOTIME') AND NOT EOF('CURHADNOTIME') THEN
								SELECT CURHADNOTIME
								SCAN
									SCATTER MEMVAR
									INSERT INTO CURWTKHOURS FROM MEMVAR
								ENDSCAN	
								USE IN CURHADNOTIME
							ENDIF

							SELECT CURWTKHOURS
							INDEX ON DIVISION TAG TDIV
							INDEX ON DEPT TAG TDEPT
							INDEX ON EMPLOYEE TAG TEMPLOYEE
							INDEX ON FILE_NUM TAG TFILE_NUM

							****** horizontalize cursor so paycodes are in separate columns

							IF USED('CUREMPLOYEES') THEN
								USE IN CUREMPLOYEES
							ENDIF

							SELECT ;
								DIVISION, ;
								DEPT, ;
								EMPLOYEE, ;
								FILE_NUM, ;
								TIMERVWR, ;
								TRNAME, ;
								0000.00 AS IN_OFFICE_HRS, ;
								0000.00 AS GTD_HRS, ;
								0000.00 AS SD1_HRS, ;
								0000.00 AS SPP_HRS, ;
								0000.00 AS ON_THE_ROAD_HRS, ;
								0000.00 AS WORKING_FROM_HOME_HRS, ;
								0000.00 AS SICK_HRS, ;
								0000.00 AS VAC_HRS, ;
								0000.00 AS VAC_RLVR_HRS, ;
								0000.00 AS HDAY_HRS, ;
								0000.00 AS PP_VAC_HRS, ;
								0000.00 AS PP_HDAY_HRS, ;
								0000.00 AS PP_BDAY_HRS, ;
								0000.00 AS BDAY_HRS, ;
								0000.00 AS PERS_HRS, ;
								0000.00 AS BDAY_ADV, ;
								0000.00 AS COMP_HRS, ;
								0000.00 AS JURY_HRS, ;
								0000.00 AS BREAVE_HRS, ;
								0000.00 AS OTHER_HRS, ;
								0000.00 AS RETRO_OTH, ;
								0000.00 AS OTHER_PAY, ;
								0000.00 AS CADRVR_PAY ;
								FROM CURWTKHOURS ;
								INTO CURSOR CUREMPLOYEES ;
								GROUP BY DIVISION, DEPT, EMPLOYEE, FILE_NUM, TIMERVWR, TRNAME ;
								ORDER BY FILE_NUM ;
								READWRITE

							* populate hours/code fields in main cursor
							SELECT CUREMPLOYEES
							SCAN
								SCATTER MEMVAR
								STORE 0000.00 TO ;
									m.IN_OFFICE_HRS, ;
									m.GTD_HRS, ;
									m.SD1_HRS, ;
									m.SPP_HRS, ;
									m.ON_THE_ROAD_HRS, ;
									m.WORKING_FROM_HOME_HRS, ;
									m.SICK_HRS, ;
									m.VAC_HRS, ;
									m.VAC_RLVR_HRS, ;
									m.HDAY_HRS, ;
									m.PP_VAC_HRS, ;
									m.PP_HDAY_HRS, ;
									m.PP_BDAY_HRS, ;
									m.PERS_HRS, ;
									m.BDAY_HRS, ;
									m.BDAY_ADV, ;
									m.COMP_HRS, ;
									m.JURY_HRS, ;
									m.BREAVE_HRS, ;
									m.OTHER_HRS, ;
									m.RETRO_OTH, ;
									m.OTHER_PAY, ;
									m.CADRVR_PAY
									
								SELECT CURWTKHOURS
								SCAN FOR ;
										DIVISION = CUREMPLOYEES.DIVISION AND ;
										DEPT = CUREMPLOYEES.DEPT AND ;
										EMPLOYEE = CUREMPLOYEES.EMPLOYEE AND ;
										FILE_NUM = CUREMPLOYEES.FILE_NUM
									lcPayCode = UPPER(ALLTRIM(CURWTKHOURS.PAYCODE))
									DO CASE
										CASE lcPayCode == "INO"
											m.IN_OFFICE_HRS = m.IN_OFFICE_HRS + CURWTKHOURS.TOTHOURS
										CASE lcPayCode == "ONT"
											m.ON_THE_ROAD_HRS = m.ON_THE_ROAD_HRS + CURWTKHOURS.TOTHOURS
										CASE lcPayCode == "WFH"
											m.WORKING_FROM_HOME_HRS = m.WORKING_FROM_HOME_HRS + CURWTKHOURS.TOTHOURS
										CASE INLIST(lcPayCode,"VAC","VH7","VAD","VA7")
											m.VAC_HRS = m.VAC_HRS + CURWTKHOURS.TOTHOURS
										CASE lcPayCode == "RVP"
											* Vacation Rollovers
											m.VAC_RLVR_HRS = m.VAC_RLVR_HRS + CURWTKHOURS.TOTHOURS
										CASE INLIST(lcPayCode,"HOL","HAD")
											m.HDAY_HRS = m.HDAY_HRS + CURWTKHOURS.TOTHOURS
										*CASE lcPayCode == "SIC"
										CASE INLIST(lcPayCode,"SIC","SC7")
											m.SICK_HRS = m.SICK_HRS + CURWTKHOURS.TOTHOURS
										CASE INLIST(lcPayCode,"BDY","BAD")
											m.BDAY_HRS = m.BDAY_HRS + CURWTKHOURS.TOTHOURS
											IF lcPayCode == "BAD"
												m.BDAY_ADV = m.BDAY_ADV + CURWTKHOURS.TOTHOURS
											ENDIF
										CASE INLIST(lcPayCode,"PRS","PAD")
											m.PERS_HRS = m.PERS_HRS + CURWTKHOURS.TOTHOURS
										CASE lcPayCode == "CMP"
											m.COMP_HRS = m.COMP_HRS + CURWTKHOURS.TOTHOURS
										CASE lcPayCode == "JUR"
											m.JURY_HRS = m.JURY_HRS + CURWTKHOURS.TOTHOURS
										CASE lcPayCode == "BRV"
											m.BREAVE_HRS = m.BREAVE_HRS + CURWTKHOURS.TOTHOURS
										CASE lcPayCode == "GTD"
											m.GTD_HRS = m.GTD_HRS + CURWTKHOURS.TOTHOURS
										CASE lcPayCode == "OTH"
											m.OTHER_HRS = m.OTHER_HRS + CURWTKHOURS.TOTHOURS
										CASE lcPayCode == "SD1"
											m.SD1_HRS = m.SD1_HRS + CURWTKHOURS.TOTHOURS
										CASE lcPayCode == "PPV"
											m.PP_VAC_HRS = m.PP_VAC_HRS + CURWTKHOURS.TOTHOURS
										CASE lcPayCode == "PPH"
											m.PP_HDAY_HRS = m.PP_HDAY_HRS + CURWTKHOURS.TOTHOURS
										CASE lcPayCode == "PPB"
											m.PP_BDAY_HRS = m.PP_BDAY_HRS + CURWTKHOURS.TOTHOURS
										CASE lcPayCode == "SPP"
											m.SPP_HRS = m.SPP_HRS + CURWTKHOURS.TOTHOURS
										CASE lcPayCode == "OTP"
											m.OTHER_PAY = m.OTHER_PAY + CURWTKHOURS.TOTPAY
										CASE lcPayCode == "RTR"
											m.RETRO_OTH = m.RETRO_OTH + CURWTKHOURS.TOTPAY
										CASE lcPayCode == "CAI"
											m.CADRVR_PAY = m.CADRVR_PAY + CURWTKHOURS.TOTPAY
										OTHERWISE
											* NOTHING
									ENDCASE

								ENDSCAN
								* accumulate totals
								SELECT CUREMPLOYEES
								GATHER MEMVAR
							ENDSCAN

							WAIT WINDOW NOWAIT "Preparing data..."

							SELECT CUREMPLOYEES

							GOTO TOP
							IF EOF() THEN
								.TrackProgress("There was no data to export!", LOGIT+SENDIT)
							ELSE

								lcRateType = "SALARIED"

								*!*								WAIT WINDOW NOWAIT "Opening Excel..."
								*!*								oExcel = CREATEOBJECT("excel.application")

								oWorkbook = .oExcel.workbooks.OPEN("F:\UTIL\KRONOS\TEMPLATES\KronosTRReportSALARIED.XLS")

								***********************************************************************************************************************
								***********************************************************************************************************************
								WAIT WINDOW NOWAIT "Looking for target directory..."
								* see if target directory exists, and, if not, create it
								* e.g. F:\timeclk\adpfiles\01-07-2005

								lcFileDate = PADL(MONTH(ldEndDate),2,"0") + "-"  + PADL(DAY(ldEndDate),2,"0") + "-" + PADL(YEAR(ldEndDate),4,"0")
								
								

								lcTargetDirectory = "F:\UTIL\KRONOS\TRREPORTS\SALARIED\" + lcFileDate + "\"
								
								* ACTIVATED WHEN f: DRIVE WAS DOWN....
								*lcTargetDirectory = "C:\UTIL\KRONOS\TRREPORTS\SALARIED\" + lcFileDate + "\"
								
								
								* for testing
								*lcTargetDirectory = "C:\TIMECLK\ADPFILES\" + lcFileDate + "\"

								* create directory if it doesn't exist
								IF NOT DIRECTORY(lcTargetDirectory) THEN
									MKDIR (lcTargetDirectory)
									WAIT WINDOW TIMEOUT 1 "Made Target Directory " + lcTargetDirectory
								ENDIF

								* if target directory exists, save there
								llSaveAgain = .F.
								IF DIRECTORY(lcTargetDirectory) THEN
									lcFiletoSaveAs = lcTargetDirectory + lcFileDate + " Kronos "  + lcRateType + " for " + lcTRName
									lcXLFileName = lcFiletoSaveAs + ".XLS"
									IF FILE(lcXLFileName) THEN
										DELETE FILE (lcXLFileName)
									ENDIF
									oWorkbook.SAVEAS(lcFiletoSaveAs)
									* set flag to save again after sheet is populated
									llSaveAgain = .T.
								ENDIF
								
								SET DATE AMERICAN

								***********************************************************************************************************************
								***********************************************************************************************************************
								*** populate 1st tab == weekly detail
								lnRow = 3
								lnStartRow = lnRow + 1
								lcStartRow = ALLTRIM(STR(lnStartRow))

								oWorksheet = oWorkbook.Worksheets[1]
								
								IF .lIncludeAllTimeReviewers THEN
									oWorksheet.RANGE("A" + lcStartRow,"S1500").clearcontents()
									oWorksheet.RANGE("A" + lcStartRow,"S1500").FONT.SIZE = 12
									oWorksheet.RANGE("A" + lcStartRow,"S1500").FONT.NAME = "Arial"
									oWorksheet.RANGE("A" + lcStartRow,"S1500").FONT.bold = .T.
								ELSE
									oWorksheet.RANGE("A" + lcStartRow,"S200").clearcontents()
									oWorksheet.RANGE("A" + lcStartRow,"S200").FONT.SIZE = 12
									oWorksheet.RANGE("A" + lcStartRow,"S200").FONT.NAME = "Arial"
									oWorksheet.RANGE("A" + lcStartRow,"S200").FONT.bold = .T.
								ENDIF

								lcTitle = "Attendance Detail for " + lcStartDate + " - " + lcEndDate + " (" + .cWhichWeek + " Week)"
								oWorksheet.RANGE("A1").VALUE = lcTitle
								oWorksheet.RANGE("A2").VALUE = "EEs for: " + lcTRName
								
								IF .lIncludeAllTimeReviewers THEN
									oWorksheet.RANGE("F3").VALUE = "Time Reviewer"
								ENDIF

								* main scan/processing
								* for each employee.file#, loop thru all days in week and display each day/paycode on a separate row.
								* If a day in Mon-Fri has no data, flag as error.
								* ldStartDate
								SELECT CURDISTINCTEMPS
								SCAN
									ldCurrentDate = ldStartDate
									DO WHILE ldCurrentDate <= ldEndDate
										SELECT CURDAILY
										COUNT FOR EMPLOYEE = CURDISTINCTEMPS.EMPLOYEE ;
											AND FILE_NUM = CURDISTINCTEMPS.FILE_NUM ;
											AND DATE = ldCurrentDate ;
											TO lnDayCount
										
										IF lnDayCount = 0 THEN
											* IF Mon - Fri add error row otherwise nothing
											IF INLIST(DOW(ldCurrentDate,1),2,3,4,5,6) THEN
												* add error row
												lnRow = lnRow + 1
												lcRow = LTRIM(STR(lnRow))
												oWorksheet.RANGE("A" + lcRow).VALUE = CURDISTINCTEMPS.EMPLOYEE
												oWorksheet.RANGE("B" + lcRow).VALUE = CURDISTINCTEMPS.FILE_NUM
												oWorksheet.RANGE("C" + lcRow).VALUE = CDOW(ldCurrentDate)
												oWorksheet.RANGE("D" + lcRow).VALUE = ldCurrentDate
												oWorksheet.RANGE("E" + lcRow).VALUE = "No paycode!"
												IF .lIncludeAllTimeReviewers THEN
													oWorksheet.RANGE("F" + lcRow).VALUE = CURDISTINCTEMPS.TRNAME
												ENDIF
												oWorksheet.RANGE("A" + lcRow + "..F" + lcRow).Font.ColorIndex = 3
											ELSE
												* Nothing
											ENDIF
										
										ELSE
											* add a row per date/paycode
											SELECT CURDAILY
											SCAN FOR EMPLOYEE = CURDISTINCTEMPS.EMPLOYEE ;
												AND FILE_NUM = CURDISTINCTEMPS.FILE_NUM ;
												AND DATE = ldCurrentDate
												
												lnRow = lnRow + 1
												lcRow = LTRIM(STR(lnRow))
												
												oWorksheet.RANGE("A" + lcRow).VALUE = CURDAILY.EMPLOYEE
												oWorksheet.RANGE("B" + lcRow).VALUE = CURDAILY.FILE_NUM
												oWorksheet.RANGE("C" + lcRow).VALUE = CDOW(CURDAILY.DATE)
												oWorksheet.RANGE("D" + lcRow).VALUE = CURDAILY.DATE
												oWorksheet.RANGE("E" + lcRow).VALUE = CURDAILY.PAYDESC
												IF .lIncludeAllTimeReviewers THEN
													oWorksheet.RANGE("F" + lcRow).VALUE = CURDAILY.TRNAME
												ENDIF
												
											ENDSCAN
										
										ENDIF
								
										ldCurrentDate = ldCurrentDate + 1
										
									ENDDO  &&  WHILE ldCurrentDate < ldEndDate
									
									* skip a row between employees
									lnRow = lnRow + 1
									lcRow = LTRIM(STR(lnRow))
									
								ENDSCAN					
								
								lnRow = lnRow + 1
								lcRow = LTRIM(STR(lnRow))
								oWorksheet.PageSetup.PrintArea = "$A$1:$F$" + lcRow
								
								
								***********************************************************************************************************************
								***********************************************************************************************************************
								*** populate 2nd tab == weekly summary
								lnRow = 3
								lnStartRow = lnRow + 1
								lcStartRow = ALLTRIM(STR(lnStartRow))

								oWkSht2 = oWorkbook.Worksheets[2]
								oWkSht2.RANGE("A" + lcStartRow,"S200").clearcontents()

								oWkSht2.RANGE("A" + lcStartRow,"S200").FONT.SIZE = 10
								oWkSht2.RANGE("A" + lcStartRow,"S200").FONT.NAME = "Arial Narrow"
								oWkSht2.RANGE("A" + lcStartRow,"S200").FONT.bold = .T.
								


								lcTitle = "Attendance Summary for " + lcStartDate + " - " + lcEndDate + " (" + .cWhichWeek + " Week)"
								oWkSht2.RANGE("A1").VALUE = lcTitle
								oWkSht2.RANGE("A2").VALUE = "EEs for: " + lcTRName

								* main scan/processing
								SELECT CUREMPLOYEES
								STORE 0 TO num_salary
								SCAN

									lnRow = lnRow + 1
									lcRow = LTRIM(STR(lnRow))
									oWkSht2.RANGE("A" + lcRow).VALUE = "'" + CUREMPLOYEES.DIVISION
									oWkSht2.RANGE("B" + lcRow).VALUE = CUREMPLOYEES.DEPT
									oWkSht2.RANGE("C" + lcRow).VALUE = CUREMPLOYEES.EMPLOYEE
									oWkSht2.RANGE("D" + lcRow).VALUE = CUREMPLOYEES.FILE_NUM
									oWkSht2.RANGE("E" + lcRow).VALUE = CUREMPLOYEES.IN_OFFICE_HRS
									oWkSht2.RANGE("F" + lcRow).VALUE = CUREMPLOYEES.ON_THE_ROAD_HRS
									oWkSht2.RANGE("G" + lcRow).VALUE = CUREMPLOYEES.WORKING_FROM_HOME_HRS
									oWkSht2.RANGE("H" + lcRow).VALUE = CUREMPLOYEES.SICK_HRS
									
									*oWkSht2.RANGE("I" + lcRow).VALUE = CUREMPLOYEES.VAC_HRS
									oWkSht2.RANGE("I" + lcRow).VALUE = ( CUREMPLOYEES.VAC_HRS + CUREMPLOYEES.VAC_RLVR_HRS )
									
									oWkSht2.RANGE("J" + lcRow).VALUE = CUREMPLOYEES.HDAY_HRS
									*oWkSht2.RANGE("K" + lcRow).VALUE = CUREMPLOYEES.BDAY_HRS
									oWkSht2.RANGE("K" + lcRow).VALUE = CUREMPLOYEES.PERS_HRS
									
									oWkSht2.RANGE("L" + lcRow).VALUE = CUREMPLOYEES.COMP_HRS
									
									oWkSht2.RANGE("M" + lcRow).VALUE = CUREMPLOYEES.JURY_HRS
									oWkSht2.RANGE("N" + lcRow).VALUE = CUREMPLOYEES.BREAVE_HRS
									oWkSht2.RANGE("O" + lcRow).VALUE = CUREMPLOYEES.OTHER_HRS
									oWkSht2.RANGE("P" + lcRow).VALUE = CUREMPLOYEES.PP_VAC_HRS
									oWkSht2.RANGE("Q" + lcRow).VALUE = CUREMPLOYEES.PP_HDAY_HRS
									oWkSht2.RANGE("R" + lcRow).VALUE = CUREMPLOYEES.PP_BDAY_HRS
									oWkSht2.RANGE("S" + lcRow).formula = "=sum(E" + lcRow + ":R" + lcRow + ")"
									
									IF oWkSht2.RANGE("S" + lcRow).VALUE != 40 THEN
										oWkSht2.RANGE("S" + lcRow).Font.ColorIndex = 3
									ENDIF

									oWkSht2.RANGE("A" + lcRow,"S" + lcRow).Interior.ColorIndex = IIF(((lnRow % 2) = 1),15,0)

								ENDSCAN

								lnEndRow = lnRow
								lcEndRow = ALLTRIM(STR(lnEndRow))

								***********************************
								* 01/20/04 MB - do all the bolding/clearing of numeric columns here in one nested loop,
								* to eliminate a lot of extraneous code above.
								lcColsToProcess = "EFGHIJKLMNOPQR"
								FOR i = 1 TO LEN(lcColsToProcess)
									lcBoldColumn = SUBSTR(lcColsToProcess,i,1)
									FOR j = lnStartRow TO lnEndRow
										lcBoldRow = ALLTRIM(STR(j))
										luCellValue = oWkSht2.RANGE(lcBoldColumn + lcBoldRow).VALUE
										IF TYPE([luCellValue])= "N" THEN
											IF luCellValue > 0.0 THEN
												oWkSht2.RANGE(lcBoldColumn + lcBoldRow).FONT.bold = .T.
											ELSE
												oWkSht2.RANGE(lcBoldColumn + lcBoldRow).VALUE = ""
											ENDIF
										ENDIF
									ENDFOR  && j
								ENDFOR  && i

								oWkSht2.RANGE("E" + lcStartRow,"S200").NumberFormat = "#,##0.00"								


*!*									* FORMAT LAST TWO COLUMNS FOR CURRENCY
*!*									oWkSht2.RANGE("S" + lcStartRow,"T1000").NumberFormat = "$#,##0.00"								

								***********************************

								*!*								lcJJ = lcRow

*!*									lnRow = lnRow + 2
*!*									lcRow = LTRIM(STR(lnRow))
*!*									oWkSht2.RANGE("C" + lcRow).VALUE = "Control Numbers"
								lnTotalsRow = lnRow + 2
								lcRow = LTRIM(STR(lnTotalsRow))

								oWkSht2.RANGE("A" + lcRow,"V" + lcRow).FONT.SIZE = 9

								oWkSht2.RANGE("E" + lcRow).formula = "=sum(E" + lcStartRow + ":E" + lcEndRow + ")"
								oWkSht2.RANGE("F" + lcRow).formula = "=sum(F" + lcStartRow + ":F" + lcEndRow + ")"
								oWkSht2.RANGE("G" + lcRow).formula = "=sum(G" + lcStartRow + ":G" + lcEndRow + ")"
								oWkSht2.RANGE("H" + lcRow).formula = "=sum(H" + lcStartRow + ":H" + lcEndRow + ")"
								oWkSht2.RANGE("I" + lcRow).formula = "=sum(I" + lcStartRow + ":I" + lcEndRow + ")"
								oWkSht2.RANGE("J" + lcRow).formula = "=sum(J" + lcStartRow + ":J" + lcEndRow + ")"
								oWkSht2.RANGE("K" + lcRow).formula = "=sum(K" + lcStartRow + ":K" + lcEndRow + ")"
								oWkSht2.RANGE("L" + lcRow).formula = "=sum(L" + lcStartRow + ":L" + lcEndRow + ")"
								oWkSht2.RANGE("M" + lcRow).formula = "=sum(M" + lcStartRow + ":M" + lcEndRow + ")"
								oWkSht2.RANGE("N" + lcRow).formula = "=sum(N" + lcStartRow + ":N" + lcEndRow + ")"
								oWkSht2.RANGE("O" + lcRow).formula = "=sum(O" + lcStartRow + ":O" + lcEndRow + ")"
								oWkSht2.RANGE("P" + lcRow).formula = "=sum(P" + lcStartRow + ":P" + lcEndRow + ")"
								oWkSht2.RANGE("Q" + lcRow).formula = "=sum(Q" + lcStartRow + ":Q" + lcEndRow + ")"
								oWkSht2.RANGE("R" + lcRow).formula = "=sum(R" + lcStartRow + ":R" + lcEndRow + ")"
								oWkSht2.RANGE("S" + lcRow).formula = "=sum(S" + lcStartRow + ":S" + lcEndRow + ")"

*!*									oWkSht2.RANGE("C" + lcRow).VALUE = "Salaries= " + ALLTRIM(STR(num_salary))

								* make sure the reference numbers come out on white background
								oWkSht2.RANGE("A" + lcRow,"S" + lcRow).Interior.ColorIndex = 0

								lnRow = lnRow + 1
								lcRow = LTRIM(STR(lnRow))
								*oWkSht2.RANGE("A" + lcStartRow,"T" + lcRow).SELECT
								oWkSht2.PageSetup.PrintArea = "$A$1:$S$" + lcRow

								* save again
								IF llSaveAgain THEN
									oWorkbook.SAVE()
								ENDIF
								oWorkbook.CLOSE()

								*******************************************************************************************
								*******************************************************************************************

*!*									lcTopBodyText = ;
*!*										"Salaried Time Reviewer," + CRLF + CRLF + ;
*!*										"The attached spreadsheet report shows the Attendance information for your Salaried employees." + CRLF + CRLF + ;
*!*										"Instructions:" + CRLF + ;
*!*										"1) - Print the spreadsheet report (the 1st tab of the spreadsheet)." + CRLF + ;
*!*										"2) - Review the printed report and make any corrections right on it." + CRLF + ;
*!*										"     (be sure to identify any Sick, Comp, or Vacation Days taken but not shown)" + CRLF + ;
*!*										"3) - Attach PANs (signed by employee and you) for any Sick, Comp, or Vacation taken." + CRLF + ;
*!*										"4) - Sign the printed report." + CRLF + ;
*!*										"5) - SCAN the signed report and PANs together." + CRLF + ;
*!*										"6) - Email the scan file to either Rebecca or Lucille in Human Resources." + CRLF + CRLF

								lcTopBodyText = ;
									"Salaried Time Reviewer," + CRLF + CRLF + ;
									"The attached spreadsheet report shows the Attendance information for your Salaried employees." + CRLF + CRLF + ;
									"Instructions:" + CRLF + ;
									"1) - Print the spreadsheet report (the 1st tab of the spreadsheet)." + CRLF + ;
									"2) - Review the printed report and make any corrections right on it." + CRLF + ;
									"     (be sure to identify any Sick, Comp, or Vacation Days taken but not shown)" + CRLF + ;
									"3) - Sign the printed report." + CRLF + ;
									"4) - Attach PANs (signed by employee and you) for any Sick, Comp, or Vacation taken." + CRLF + ;
									"5) - Mail the signed report & any PANs to either Marie or Lucille in Human Resources." + CRLF + ;
									"6) - NOTE: Please do *not* scan and email these documents to HR."  + CRLF + CRLF
									
									
									
*!*									lcTopBodyText = ;
*!*										"The attached spreadsheet shows the Attendance information for your Salaried employees " + ;
*!*										"for the " + .cWhichWeek + " pay period." + CRLF + CRLF + ;
*!*										"The 1st tab shows detail by day; the 2nd tab is a weekly summary."
									
*!*									lcTopBodyText = lcTopBodyText + CRLF + CRLF + ;
*!*										"Print, review, and sign the report, then send it to Human Resources."
*!*									lcTopBodyText = lcTopBodyText + CRLF + CRLF + ;
*!*										"If you see anything wrong with the report, contact Human Resources immediately."

*!*									IF .lSpecial THEN
*!*
*!*										TEXT TO lcSpecialtext
*!*	To all:
*!*	We are resending these e-mails for your approval.  Please note that both Connie and Lucille have audited the time punches and made any changes that were necessary.  These changes were made only to those employees that were punching in prior/after the 7 minute rule.  That one minute in California costs us 15 minutes of overtime!!!  We can only do these changes for last week, and this week, after that the overtime must be paid.  It will be up to each time-reviewer to speak to their employees, and take further action if necessary if it continues.  Please review this report with the previous one sent, then sign this one and write, �REVISED� on it, and forward to Ericka.  If you see something that does not look correct on this report, contact Lucille immediately, as payroll will run at exactly 12:00 o�clock EST. today.
*!*	Thanks for all your help this week, for a job well done.  Our first week of �Kronos� went rather smoothly.!!

*!*										ENDTEXT
*!*
*!*										lcTopBodyText = lcSpecialtext + CRLF + CRLF + lcTopBodyText
*!*
*!*									ENDIF  &&  .lSpecial


*!*								.cBodyText = lcTopBodyText + CRLF + ;
*!*									"==================================================================================================================" + ;
*!*									CRLF +lcMissingApprovals + CRLF + ;
*!*									"==================================================================================================================" + ;
*!*										CRLF + "<report log follows>" + ;
*!*										CRLF + CRLF + .cBodyText
							.cBodyText = lcTopBodyText + CRLF + ;
									CRLF + "<report log follows>" + ;
									CRLF + CRLF + .cBodyText

								*.oExcel.VISIBLE = .T.

								*.oExcel.QUIT()

							ENDIF  && EOF() CUREMPLOYEES

							IF FILE(lcXLFileName) THEN
								.cAttach = lcXLFileName
								.TrackProgress('Created Attendance File : ' + lcXLFileName, LOGIT+SENDIT)
								.TrackProgress('Kronos Salaried Attendance Report process ended normally.', LOGIT+SENDIT+NOWAITIT)
							ELSE
								.TrackProgress('!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!', LOGIT+SENDIT)
								.TrackProgress("ERROR: There was a problem Creating Attendance File : " + lcXLFileName, LOGIT+SENDIT)
								.TrackProgress('!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!', LOGIT+SENDIT)
							ENDIF

						ELSE

							.cAttach = ''
							.TrackProgress("We didn't find any employees in Kronos for you this week!", LOGIT+SENDIT)

						ENDIF && USED('CURALLTREMPS') AND (NOT EOF('CURALLTREMPS')) THEN

					ENDIF  &&  .ExecSQL(lcSQL, 'CURWTKEMPLOYEE', RETURN_DATA_MANDATORY)


					IF USED('CURWTKHOURS') THEN
						USE IN CURWTKHOURS
					ENDIF
					IF USED('CURACTUALPAYEXPORT') THEN
						USE IN CURACTUALPAYEXPORT
					ENDIF
					IF USED('CUREMPLOYEES') THEN
						USE IN CUREMPLOYEES
					ENDIF

					SQLDISCONNECT(.nSQLHandle)

					*CLOSE DATABASES ALL
				ELSE
					* connection error
					.TrackProgress('Unable to connect to WTK System.', LOGIT+SENDIT)
				ENDIF   &&  .nSQLHandle > 0
			CATCH TO loError

				.TrackProgress('There was an error.',LOGIT+SENDIT)
				.TrackProgress('The Error # is: ' + TRANSFORM(loError.ErrorNo), LOGIT+SENDIT)
				.TrackProgress('The Error Message is: ' + TRANSFORM(loError.MESSAGE), LOGIT+SENDIT)
				.TrackProgress('The Error occurred in Program: ' + TRANSFORM(loError.PROCEDURE), LOGIT+SENDIT)
				.TrackProgress('The Error occurred at line #: ' + TRANSFORM(loError.LINENO), LOGIT+SENDIT)
				lnNumberOfErrors = lnNumberOfErrors + 1
				IF TYPE('.oExcel') = 'O' AND NOT ISNULL(.oExcel) THEN
					.oExcel.VISIBLE = .T.
				ENDIF
				IF .nSQLHandle > 0 THEN
					SQLDISCONNECT(.nSQLHandle)
				ENDIF
				*CLOSE DATA
				*CLOSE ALL

			ENDTRY

			*CLOSE DATA
			WAIT CLEAR
			***************** INTERNAL email results ******************************
			.TrackProgress('About to send status email.',LOGIT)
			.TrackProgress('==================================================================================================================', SENDIT)
			.TrackProgress('The logfile is: ' + .cLogFile, SENDIT)
			.TrackProgress('Kronos Salaried Attendance Report process started: ' + .cStartTime, LOGIT+SENDIT)
			.TrackProgress('Kronos Salaried Attendance Report process finished: ' + TTOC(DATETIME()), LOGIT+SENDIT)

			IF .lSendInternalEmailIsOn THEN
				* try to trap error from not having dartmail dll's registered on user's pc...
				TRY
					DO FORM dartmail2 WITH .cSendTo,.cFrom,.cSubject,.cCC,.cAttach,.cBodyText,"A"
					.TrackProgress('Sent status email.',LOGIT)
				CATCH TO loError
					=MESSAGEBOX('There was an error sending the status email. Please contact Mark Bennett.',0+64,"Kronos Salaried Attendance Report")
					.TrackProgress('There was an error sending the status email.',LOGIT)
					.TrackProgress('The Error # is: ' + TRANSFORM(loError.ErrorNo), LOGIT)
					.TrackProgress('The Error Message is: ' + TRANSFORM(loError.MESSAGE), LOGIT)
					.TrackProgress('The Error occurred in Program: ' + TRANSFORM(loError.PROCEDURE), LOGIT)
					.TrackProgress('The Error occurred at line #: ' + TRANSFORM(loError.LINENO), LOGIT)
					lnNumberOfErrors = lnNumberOfErrors + 1
					CLOSE DATA
				ENDTRY
			ELSE
				.TrackProgress('Did not send status email: THIS.lSendInternalEmailIsOn = FALSE.',LOGIT)
			ENDIF

		ENDWITH
		RETURN
	ENDFUNC && main


	FUNCTION ExecSQL
		LPARAMETERS tcSQL, tcCursorName, tlNoDataReturnedIsOkay
		LOCAL llRetval, lnResult
		WITH THIS
			* close target cursor if it's open
			IF USED(tcCursorName)
				USE IN (tcCursorName)
			ENDIF
			WAIT WINDOW "Fetching remote data; please wait..." NOWAIT
			lnResult = sqlexec(.nSQLHandle, tcSQL, tcCursorName)
			llRetval = ( lnResult > 0 )
			IF llRetval THEN
				* see if any data came back
				IF NOT tlNoDataReturnedIsOkay THEN
					IF (NOT USED(tcCursorName)) OR EOF(tcCursorName) THEN
						llRetval = .F.
						.TrackProgress('No data was returned by this query: ' +CRLF + tcSQL, LOGIT+SENDIT)
					ENDIF
				ENDIF
			ELSE
				.TrackProgress("Result Code [" + TRANSFORM(lnResult) + "] returned by this query: " + tcSQL, LOGIT+SENDIT)
			ENDIF
			WAIT CLEAR
			RETURN llRetval
		ENDWITH
	ENDFUNC


	FUNCTION SetSpecial
		THIS.lSpecial = .T.
	ENDFUNC


	PROCEDURE TrackProgress
		* do any combination of Wait Window, writing to logfile, and adding to body of email,
		* based on nFlags parameter.
		LPARAMETERS tcExpression, tnFlags
		WITH THIS
			IF BITAND(tnFlags,LOGIT) = LOGIT THEN
				IF .lLoggingIsOn THEN
					?
					? .cProcessName + "  " + TTOC(DATETIME()) + ": " + tcExpression
				ENDIF
			ENDIF
			IF .lWaitWindowIsOn THEN
				IF BITAND(tnFlags,NOWAITIT) = NOWAITIT THEN
					WAIT WINDOW tcExpression NOWAIT
				ENDIF
				IF BITAND(tnFlags,WAITIT) = WAITIT THEN
					WAIT WINDOW tcExpression TIMEOUT .nWaitWindowTimeout
				ENDIF
			ENDIF
			IF BITAND(tnFlags,SENDIT) = SENDIT THEN
				IF .lSendInternalEmailIsOn THEN
					.cBodyText = .cBodyText + tcExpression + CRLF + CRLF
				ENDIF
			ENDIF
		ENDWITH
	ENDPROC  &&  TrackProgress
	
	
	FUNCTION GetActiveSalariedTimeReviewerList
		LOCAL lcList
		lcList = ""
		SELECT TIMERVWRTABLE
		SCAN FOR LACTIVE AND (NOT LHOURLY) AND (NOT ALLTRIM(TIMERVWR) == 'ALL')
			IF EMPTY(lcList) THEN
				lcList = ALLTRIM(TIMERVWRTABLE.TIMERVWR)
			ELSE
				lcList = lcList + ", " + ALLTRIM(TIMERVWRTABLE.TIMERVWR)
			ENDIF
		ENDSCAN
		RETURN lcList
	ENDFUNC


ENDDEFINE

