
coffice='L'
acctnum=61825
edi_type = "PL"

*Parameters coffice,acctnum
* call this like this: DO m:\dev\prg\2xu943.prg WITH "C", 6665

ltesting = .F.
Do m:\dev\prg\_setvars With ltesting
If Vartype(acctnum) = "C"
  acctnum = Int(Val(acctnum))
Endif

Public goffice,tsendto,tcc,lcaccountname,lcartonacct,ldogeneric,lcreateerr,pname,nacctnum,lholdfile,ldomix,nlinestart,cmod
Public m.style,m.color,m.id,m.size,m.pack,ctransfer,ltest,lallpicks,lcwhpath,lnormalexit,lcerrormessage,newacctnum
Public cacctname,lacctname,xacctnum,lprepack,nprepacks,npickpacks,tfrom,lprocerror,thisfile,lcpath,ldoslerror
Public cslerror,lpik,lpre,cmessage,cfilename,ltestmail,lmoderr,xfile,cmyfile,lxlsx,archivefile,ldeldbf,plqty1,plqty2,lcerrormessage

Store "" To m.style,m.color,m.id,m.size,m.pack,lcbrokerref,cmessage,cfilename,xfile

Close Databases All

gmasteroffice ="L"
gsystemmodule = "wh"

Do m:\dev\prg\lookups
ccustname = 'BIOWORLD'
lcerrormessage=""
ltest = .F.  && This directs output to F:\WHP\WHDATA test files (default = .f.)
ltestmail = .F. &&lTest && Causes mail to go to default test recipient(s)
lholdfile = .F. && Causes error CATCH message; on completion with no errors, changed to .t. (default = .f.)
lbrowse = lholdfile  && If this is set with lHoldFile, browsing will occur at various points (default = .f.)
loverridebusy = Iif(lholdfile Or ltest,.T.,.F.) && If this is set, CHKBUSY flag in FTPSETUP will be ignored (default = .f.)

*loverridebusy = .T.

lallpicks = .F.  && If this is set, all inbounding will be done as units...should not be used (default = .f.)
ldoslerror = .F.  && This flag will be set if there is missing info in the Courtaulds inbound sheet
lxlsx = .F.
ldeldbf = .T.

If Type("acctnum") = "C"
  acctnum = Val(acctnum)
Endif

Store acctnum To nacctnum

tfrom = "TGF WMS Inbound EDI System Operations <inbound-ops@fmiint.com>"
ldogeneric = .F.
lcreateerr = .F.
lmainmail = .T.
tsendto = ""
tcc = ""
cslerror = ""
lcartonacct = .F.
Select 0
Use F:\edirouting\ftpsetup

m.office = coffice
Do Case
Case coffice = "L" And acctnum = 6182
  ctransfer = "PL-BIOWORLD-ML"
  cmod = "Y"
Endcase

**********************************************************************************************
If ltest Or lholdfile
  tsendto = tsendtotest
  tcc = tcctest
Endif

xsqlexec("select * from account where inactive=0","account",,"qq")
Index On accountid Tag accountid

acctnum = 6182

Select account

If Seek(acctnum,"account","accountid")
  cacctname= account.acctname
Else
  lnormalexit=.T.
  Wait Window At 10,10 "No account name on file.........." Timeout 2
  Close Databases All
  Throw
Endif

xreturn = "X"
Do m:\dev\prg\wf_alt With coffice,acctnum
lcwhpath = Upper(xreturn)
If xreturn = "X"
  Wait Window "No such account for this office" Timeout 2
  Throw
Endif
***********************************************start 2xu

************************added TM
lcpath = "F:\FTPUSERS\BIOWORLD\943IN_EXCEL\"
lcarchivepath = 'F:\FTPUSERS\BIOWORLD\943IN_EXCEL\archive\'

************************************************************
len1 = Adir(tarray,"F:\FTPUSERS\BIOWORLD\943IN_EXCEL\*.xlsx")
If len1 = 0
  Wait Window "No files found...exiting" Timeout 2
  Close Data All
  schedupdate()
  On Error
  Return
Endif

lcpath ="F:\FTPUSERS\BIOWORLD\943IN_EXCEL\"

goffice='L'
useca("inwolog","wh",,,"mod=' "+goffice+"' and  accountid ="+Transform(acctnum) )

useca("pl","wh")
xsqlexec("select * from pl where .f.","xpl",,"wh")

For thisfile = 1 To len1
***  cfilename = alltrim(ary1[thisfile,1])
  cfilename = Alltrim(tarray[thisfile,1])
  xfile = lcpath+cfilename
  llcontinue = .T.
  Try
    xfile = lcpath+Alltrim(tarray[thisfile,1])
    If Inlist(Upper(Justext(xfile)),"TMP","DBF")
      Loop
    Endif

    attrfile = '"'+lcpath+Alltrim(tarray[thisfile,1])+'"'
    cfilename = Justfname(xfile)
    !Attrib -r &attrfile  && Removes read-only flag from file to allow deletion
    Wait Window "Filename = "+xfile Timeout 1
    Create Cursor tempmain (a c(50), ;
    b c(30), ;
    c c(30), ;
    d c(50), ;
    e c(30), ;
    F c(30), ;
    g c(30), ;
    h c(30), ;
    i c(30), ;
    j c(30), ;
    k c(30))

    xfilecsv = Juststem(Justfname(xfile))+".csv"
    Do m:\dev\prg\excel_to_csv With cfilename
    Select tempmain
    Append From [&xfilecsv] Type Csv
    Do  doimport
    archivefile=lcarchivepath+cfilename
    Copy File [&xfile] To [&archivefile]
    xfile= Lower(xfile)
    Delete File  [&xfile]
    xfile = Strtran(xfile,"xlsx","csv")
    Delete File  [&xfile]
    goodmail()

  Catch To oerr
    Assert .F. Message "In internal Catch Section (Not main loop)...debug"
    If lnormalexit = .F.
      tmessage = "Account: "+Alltrim(Str(acctnum))+" (Internal loop)"
      tmessage = tmessage+Chr(13)+"TRY/CATCH Exeception Message:"+Chr(13)+;
      [  Error: ] + Str(oerr.ErrorNo) +Chr(13)+;
      [  LineNo: ] + Str(oerr.Lineno) +Chr(13)+;
      [  Message: ] + oerr.Message +Chr(13)+;
      [  Procedure: ] + oerr.Procedure +Chr(13)+;
      [  Details: ] + oerr.Details +Chr(13)+;
      [  StackLevel: ] + Str(oerr.StackLevel) +Chr(13)+;
      [  LineContents: ] + Left(oerr.LineContents,50)+Chr(13)+;
      [  UserValue: ] + oerr.UserValue+Chr(13)

      tsubject = "Error Loading Inbound File "+xfile
      tsendto = tsendtoerr
      Do Case
      Case Inlist(coffice,"C","Y") And Inlist(nacctnum,4610,4694,6665)  && Nanjing
*            CASE cOffice = "Y" AND INLIST(nAcctNum,4610,4694)  && Nanjing
        tcc = ""
      Case coffice = "C" And nacctnum = 1285  && AgeGroup
        tcc = "yvalencia@fmiint.com"
      Otherwise
        tcc = tccerr
      Endcase
      tattach = ""
      Do Form m:\dev\frm\dartmail2 With tsendto,tfrom,tsubject,tcc,tattach,tmessage,"A"

      Messagebox(tmessage,0,"EDI Error",5000)

      Wait Clear
      llcontinue = .F.
    Else
      Wait Window "Normal Exit..." Timeout 2
    Endif

  Endtry
Next

Select ftpsetup
Replace ftpsetup.chkbusy With .F. In ftpsetup Next 1


**************************************************************try
Procedure doimport


If Empty(goffice) And ltest Or lholdfile
  goffice = "1"
Else
  goffice = coffice
Endif
Store "" To m.style,m.color,m.id,m.size,m.pack

Select * From inwolog Where .F. Into Cursor xinwolog Readwrite
Select tempmain

Delete For Empty(a)
Replace a With Upper(a) For !Empty(a)
Replace b With Upper(b) For !Empty(b)
Replace c With Upper(c) For !Empty(c)
Replace d With Upper(d) For !Empty(d)
Replace e With Upper(e) For !Empty(e)
Replace F With Upper(F) For !Empty(F)
Replace g With Upper(g) For !Empty(g)

Count To Recno


Select tempmain
Go Top

*****************************************************************start loop for multiple files here

Select * From inwolog Where .F. Into Cursor xinwolog Readwrite
recctr = 0

recctr = recctr +1
Wait Window At 10,10 "Checking Record # "+Transform(recctr) Nowait

m.adddt=Datetime()
m.updatedt=Datetime()
m.addby='BIOWORLD_ML'
m.updateby='BIOWORLD_ML'
m.accountid=6182
m.addproc='BIOWORLD_ML'
m.updproc='BIOWORLD_ML'
Select tempmain
Scan
  Do Case
  Case tempmain.a = 'ON DATE'
    m.wo_date=Ctod(tempmain.b)
  Case tempmain.a = 'CONTAINER'
    m.container= tempmain.b
  Case tempmain.a = 'ACCOUNT REF'
    m.acct_ref= tempmain.b
  Case tempmain.a = 'BROKER REF'
    m.brokerref= tempmain.b
  Case tempmain.a = 'VESSEL/AWB'
    m.reference= tempmain.c
  Case tempmain.a = 'SEAL'
    m.seal= tempmain.b
  Endcase
Endscan

Insert Into xinwolog From Memvar

********************************

Select * From pl Where .F. Into Cursor xpl Readwrite

Select b As Style,c As Color, d As Id, a As ctngrp, 0000 As ctnqty, e As upc, g As Pack, Round(Sum(Val(F)),0) As totqty ;
FROM tempmain Where !Empty(F) And F!='QTY' Group By 1,2,3,4,5 Order By ctngrp Into Cursor t1 Readwrite

Select t1
Scan
  If At("~",ctngrp) > 0
    loval=Val(Substr(ctngrp,1,(At("~",ctngrp))-1))
    hival=Val(Substr(ctngrp,(At("~",ctngrp))+1))
    Replace ctnqty With (hival-loval)+1 In t1
  Else
    Replace ctnqty With 1 In t1
  Endif
Endscan

recctr = 0
poctr=0
plid=0
m.units = .F.

m.adddt=Datetime()
m.updatedt=Datetime()
m.addby='BIOWORLD_ML'
m.updateby='BIOWORLD_ML'
m.accountid=6182
m.acctname='BIOWORLD'
m.addproc='BIOWORLD_ML'
m.updproc='BIOWORLD_ML'

Select *, Sum(totqty) As linetot, Count(1) numrecs  From t1 Group By ctngrp Into Cursor ctntypes Readwrite

Select ctntypes
Scan

  poctr=poctr+1
  plid=plid+1
  recctr = recctr +1
  m.units = .F.
  Insert Into xpl From Memvar

  Replace Style With ctntypes.Style In xpl
  If Empty(t1.Color)
      Replace Color With "NOT_TAGGED" In xpl
    Else
      Replace Color With Padl(Alltrim(t1.Color),10,"0") In xpl
   Endif
  If ctntypes.numrecs = 1
    Replace Id With ctntypes.Id In xpl
    Replace Pack With t1.Pack In xpl
  Else
    Replace Id With "ASST-"+Alltrim(Transform(dygenpk("PLCTR","why")))  In xpl
  Endif

  Replace Pack With Transform(ctntypes.linetot) In xpl
  xtotqty= ctntypes.linetot
  Replace totqty With ctntypes.ctnqty In xpl
  Replace xpl.po With Alltrim(Str(poctr)) In xpl
  Replace cayset With ctntypes.ctngrp In xpl

  Select t1
  Scan For ctngrp = ctntypes.ctngrp
    m.units = .T.
    Insert Into xpl From Memvar
    Replace Style With t1.Style In xpl
    If Empty(t1.Color)
      Replace Color With "NOT_TAGGED" In xpl
    Else
      Replace Color With Padl(Alltrim(t1.Color),10,"0") In xpl
    Endif
    Replace Id With t1.Id In xpl
    Replace Pack With '1' In xpl
    Replace units With .T. In xpl
    Replace cayset With ctntypes.ctngrp In xpl
    If ctntypes.ctnqty  > 1
      Replace xpl.totqty With ctntypes.ctnqty * t1.totqty In xpl
    Else
      Replace xpl.totqty With t1.totqty In xpl
    Endif
    Replace xpl.po With Alltrim(Str(poctr)) In xpl
    Replace Echo With Alltrim(t1.upc) In xpl
  Endscan
Endscan


Select xpl
Sum(totqty) To xunitsqty  For units
Sum(totqty) To xcartonsqty For !units

nuploadcount = 0

Select xinwolog
Scan  && Scanning xinwolog here
  m.plunitsinqty=xunitsqty
  m.plinqty=xcartonsqty
  m.quantity=xcartonsqty
  *m.inwologid=dygenpk("INWOLOG","wh") && was inwologid
  m.wo_num   =dygenpk("WONUM","whl")  && wonum was lower case

*  m.inwologid=dygenpk("INWOLOG","wh") && was inwologid
*  m.wo_num   =dygenpk("WONUM","WHL")  && wonum was lower case
  m.wo_date = Date()
  nwo_num = m.wo_num
  m.transfer =.t.
  insertinto("inwolog","wh",.T.)

  newinwologid = inwolog.inwologid
  Select xpl
  Scan For xpl.inwologid = xinwolog.inwologid
    Scatter Memvar Memo
    m.addby = "TOLLPROC"
    m.adddt = Datetime()
    m.addproc = "BIOWORLD UPLOAD"
    m.inwologid = inwolog.inwologid
    m.accountid = 6182
    m.wo_num = nwo_num
    m.office = "Y"  &&gMasterOffice
    m.mod = "Y"
    insertinto("pl","wh",.T.)
  Endscan
Endscan
tu("pl")
tu('inwolog')

*  endscan

Endproc
******************************
Procedure closedata
******************************

If Used('inwolog')
  Use In inwolog
Endif

If Used('pl')
  Use In pl
Endif

If Used('upcmast')
  Use In upcmast
Endif

If Used('upcmastsql')
  Use In upcmastsql
Endif

If Used('account')
  Use In account
Endif

*IF USED('whgenpk')
*  USE IN whgenpk
*ENDIF
Endproc

*****************************
Procedure slerrormail
******************************
If Used('mm')
  Use In mm
Endif
Select 0
Use F:\3pl\Data\mailmaster Alias mm
Locate For accountid = 4677 And taskname = "EXCELERROR"
lusealt = mm.use_alt
tsendto = Alltrim(Iif(lusealt,mm.sendtoalt,mm.sendto))
tcc = Alltrim(Iif(lusealt,mm.ccalt,mm.cc))
Use In mm
tsubject = "Missing information in Inbound Excel sheet"
tattach = ""
tmessage = "The following required FIELD(s) is/are missing data:"
tmessage = tmessage+Chr(13)+cslerror
Do Form m:\dev\frm\dartmail2 With tsendto,tfrom,tsubject,tcc,tattach,tmessage,"A"
Endproc

******************************
Procedure smerrmail
******************************
&& Used for all GENERIC INBOUND accounts
If Used('account')
  Use In account
Endif
Select 0
xsqlexec("select * from account where inactive=0","account",,"qq")
Index On accountid Tag accountid
Set Order To
If Seek(nacctnum,'account','accountid')
  cacctname = Alltrim(account.acctname)
Endif

If Used('mm')
  Use In mm
Endif
Select 0
Use F:\3pl\Data\mailmaster Alias mm
If !ltest
  If nacctnum # 6137
    Locate For mm.accountid = nacctnum And mm.office = goffice And mm.edi_type = 'PL'
  Else
    Locate For mm.accountid = nacctnum And mm.taskname = 'SMMAIL'
  Endif
  lusealt = mm.use_alt
  tsendto = Alltrim(Iif(lusealt,mm.sendtoalt,mm.sendto))
  tcc = Alltrim(Iif(lusealt,mm.ccalt,mm.cc))
Else
  Locate For mm.edi_type = "MISC" And mm.taskname = "GENERAL"
  lusealt = mm.use_alt
  tsendto = Alltrim(Iif(lusealt,mm.sendtoalt,mm.sendto))
  tcc = Alltrim(Iif(lusealt,mm.ccalt,mm.cc))
Endif
Use In mm
tsubject = "Style Errors in Inbound Excel sheet: "+cacctname
tattach = ""
tmessage = "File Name: "+Iif(!Empty(cfilename),cfilename,Justfname(xfile))
tmessage = tmessage+Chr(13)+Chr(13)+"The following Style/Color/Size combination(s) is/are not in our Style Master."
tmessage = tmessage+Chr(13)+"Please send these Style Master Updates, then RE-TRANSMIT the PL file. Thanks."+Chr(13)
tmessage = tmessage+Chr(13)+Padr("STYLE",22)+Padr("COLOR",12)+"SIZE"
tmessage = tmessage+Chr(13)+Replicate("=",38)
Select sm_err
Scan
  cstyle = Padr(Alltrim(sm_err.Style),22)
  ccolor = Padr(Alltrim(sm_err.Color),12)
  csize = Alltrim(sm_err.Id)
  tmessage = tmessage+Chr(13)+cstyle+ccolor+csize
Endscan
Use In sm_err
Do Form m:\dev\frm\dartmail2 With tsendto,tfrom,tsubject,tcc,tattach,tmessage,"A"
Endproc

*************************
Procedure goodmail
*************************
tsubject= "TGF "+ccustname+" Packing List Upload: " +Ttoc(Datetime())
tattach = ""
tmessage = "Packing List uploaded for "+ccustname+Chr(13)+"From File: "+cfilename+Chr(13)
tfrom ="TGF EDI Operations <toll-edi-ops@tollgroup.com>"
If ltesting
  tmessage = tmessage+Chr(13)+"*TEST DATA*"
Endif
Do Form m:\dev\frm\dartmail2 With tsendto,tfrom,tsubject,tcc,tattach,tmessage,"A"
