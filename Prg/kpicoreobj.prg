**Core Objectives KPI

* xoffice is the mod

Wait window "Creating Core Objectives Spreadsheet..." nowait noclear

goffice=xoffice

do case
case !empty(cmod)
	**removed the MOD - no longer used and complex code including acctgrp, etc. - mvw 12/22/15
*!*		cSubject = "Daily Core Objectives: "+Dtoc(dStartDt)+" ("+cMod+")"
	messagebox("Code for MOD lookup removed! Please contact MIS",16,"Invalid Pararmeter")
	return .f.
otherwise
	cSubject = "Daily Core Objectives: "+Dtoc(dStartDt)+" ("+Iif(cOffice="C","SP",Iif(cOffice="L","ML",Iif(cOffice="R","RI",Iif(cOffice="M", "FL", "NJ"))))+iif(!empty(cbuilding)," - Mod "+cbuilding,"")+")"
endcase

store tempfox+substr(sys(2015), 3, 10)+".xls" to cfile, cemailfile
**copying of an included file does not work, need to use StrToFile(FilToStr())
=StrtoFile(FileToStr("CoreObjectives.XLS"), cfile)

oWorkbook = oExcel.Workbooks.Open(cFile)

if cbuilding="I" and used("kpilog")
	replace step2 with .t. in kpilog
endif

**removed the MOD - no longer used and complex code including acctgrp, etc. - mvw 12/22/15
**just make the cGrpFilter = .t.
cGrpFilter=".t."
*!*	cGrpFilter = "inlist(acctGrpId, 7, 8)"
*!*	If !Empty(cMod)
*!*		Select acctGrp
*!*		Locate for Upper(groupName) = Upper(cMod)
*!*		If Eof()
*!*		  strMsg = "Building '"+Alltrim(cMod)+"' does not exist in acctGrp.dbf. Contact MIS"
*!*		  =MessageBox(strMsg, 16, "Error")
*!*		  USE in acctGrp
*!*		  Return
*!*		EndIf

*!*		cGrpFilter = "acctGrpId = "+Alltrim(Str(acctGrpId))
*!*	EndIf

xsqlexec("select * from acctgrp where mod='"+xoffice+"'",,,"wh")
xsqlexec("select * from acctdet where mod='"+xoffice+"'",,,"wh")

select acctdet
index on accountid tag accountid
set order to

*!*	cPdfFile = "f:\bols\pdf"+Left(Dtoc(dStartDt),2)+Right(Dtoc(dStartDt),2)+".dbf"
*!*	If File(cPdfFile)
*!*		USE &cPdfFile Alias pdfFile In 0
*!*	EndIf

if cbuilding="I" and used("kpilog")
	replace step3 with .t. in kpilog
endif

*!*	dPrevMonth = Ctod(Ltrim(Str(Month(dStartDt),2,0))+"/01/"+Right(Ltrim(Str(Year(dStartDt),4,0)),2))-1
*!*	cPdfFile = "f:\bols\pdf"+Left(Dtoc(dPrevMonth),2)+Right(Dtoc(dPrevMonth),2)+".dbf"
*!*	if file(cpdffile)
*!*		use &cpdffile alias pdfprev in 0
*!*	endif

cAcctFilter = Iif(Empty(cWhseAcct), "", "r."+cWhseAcct)

**use UPS/FEDEX vars created in lookups.prg - 01/10/17
**cNotUps = "(!inlist(o.ship_via,'DHL','AIRB','UPS','UNITED PARCEL','UPCO','U.P.S.','RPS','FEDEX','FED EX','FEDERAL EXP','US MAIL','PARCEL POST','PARCIAL POST','PARTIAL POST') "+;
**		  "or inlist(o.ship_via,'UPS SUPPLY','FEDEX NATIONAL','FEDEX FREIGHT','UPS FREIGHT'))"
cNotUps = "(!inlist(o.ship_via,"+xsmallpackagecarriers+") or inlist(o.ship_via,"+xnotsmallpackagecarriers+"))"


*****Receiving*****
drcvDt = dStartDt
**NOTE: if you are updating entire cursor in kpichgdate, must be at the top of the cursor as it does a do while !eof()
=kpichgdate("dRcvDt", -1, .f.) &&need to take the previous day's receiving bc they have 24 hour window
oworkbook.worksheets[1].range("A2").value = "Receiving for {"+Dtoc(dRcvDt)+"}: "+;
	cAcctName+" ("+Iif(cOffice="C","SP",Iif(cOffice="L","ML",Iif(cOffice="R","RI",Iif(cOffice="M", "FL", "NJ"))))+")"

if cbuilding="I" and used("kpilog")
	replace step4 with .t. in kpilog
endif

*	cIssueFltr="(rcvQty = 0 or rcvQty > awayQty or maxTm > minTm+(24*60*60))" &&24 hours
*	cIssueFltr="(rcvQty = 0 or rcvQty > awayQty or Ttod(maxTm) > Ttod(minTm)+1)" &&next business day
**if minTm is fri-sun
cIssueFltr="(rcvQty = 0 or rcvQty > awayQty or Ttod(maxTm) > Ttod(minTm)+Iif(Dow(minTm)=6, 3, Iif(Dow(minTm)=7, 2, 1)))" &&next business day

xsqlexec("select * from rcv where mod='"+xoffice+"' and (rcvDt={"+dtoc(dRcvDt)+"} or (rcvDt<{"+dtoc(dRcvDt)+"} and rcvDt#{}))","rcv",,"wh")

If cOffice # "C" or !Empty(cAcctFilter)
	if !empty(cacctfilter)
		xlocchgfilter=strtran(cacctfilter,"r.","")
	else
		xlocchgfilter=".t. and "
	endif
	
	xsqlexec("select startdt, wo_num from locchg where "+xlocchgfilter+"mod='"+xoffice+"' and between(chgdt,{"+dtoc(dstartdt-7)+"},{"+dtoc(denddt)+"})",,,"wh")
	**between() filter b/c could have wos. started day prior to dStartDt but didnt have a full 24-hours when ran report last
	Select r.*, "" as building, Min(l.startDt) as minTm, Max(l.startDt) as maxTm ;
		from rcv r ;
			left join locChg l on l.wo_num = r.wo_num ;
		where &cAcctFilter (rcvQty = 0 or rcvQty > awayQty) and !sp ;
		group by r.wo_num ;
	  into cursor csrDetail

	Sum Iif(&cIssueFltr, 1, 0), 1 to nIssue, nTot
	oworkbook.worksheets[1].range("A6").value = "Percentage: "+;
		Iif(nTot-nIssue = 0, "0.00", Alltrim(Str(Round((nTot-nIssue)/nTot,4)*100,6,2)))+;
		"% ("+Alltrim(Str(nTot-nIssue))+"/"+Alltrim(Str(nTot))+")"
Else
	if !empty(cacctfilter)
		xlocchgfilter=strtran(cacctfilter,"r.","")
	else
		xlocchgfilter=".t. and "
	endif
	
	xsqlexec("select startdt, wo_num from locchg where "+xlocchgfilter+"mod='"+xoffice+"' and between(chgdt,{"+dtoc(dstartdt-7)+"},{"+dtoc(denddt)+"})",,,"wh")
	Select r.*, acctGrpId, Iif(!Empty(cMod), "", Iif(acctGrpId=8, "SP1", "SP2")) as building, ;
		   Min(l.startDt) as minTm, Max(l.startDt) as maxTm ;
		from rcv r ;
			left join locChg l on l.wo_num = r.wo_num ;
			left join acctDet d on d.accountId = r.accountId ;
		where (rcvQty = 0 or rcvQty > awayQty) and ;
			 !InList(r.accountid,1586,3145,3980,4579,4654) and !sp and &cGrpFilter ; &&ignore polo (3pl), cayset, wormser & metro7
		group by r.wo_num ;
	  into cursor csrDetail

	DO case
	  Case Empty(cMod)
		sum Iif(&cIssueFltr and acctGrpId = 8, 1, 0), Iif(&cIssueFltr and acctGrpId = 7, 1, 0), ;
			Iif(acctGrpId = 8, 1, 0), Iif(acctGrpId = 7, 1, 0) ;
				to nIssue1, nIssue2, nTot1, nTot2

		oworkbook.worksheets[1].range("A6").value = "SP1 Percentages: "+;
			Iif(nTot1-nIssue1 = 0, "0.00", Alltrim(Str(Round((nTot1-nIssue1)/nTot1,4)*100,6,2)))+;
			"% ("+Alltrim(Str(nTot1-nIssue1))+"/"+Alltrim(Str(nTot1))+")"
		oworkbook.worksheets[1].range("A7").value = "SP2 Percentages: "+;
			Iif(nTot2-nIssue2 = 0, "0.00", Alltrim(Str(Round((nTot2-nIssue2)/nTot2,4)*100,6,2)))+;
			"% ("+Alltrim(Str(nTot2-nIssue2))+"/"+Alltrim(Str(nTot2))+")"
	  Otherwise
		sum Iif(&cIssueFltr,1,0), 1 to nIssue, nTot
		oworkbook.worksheets[1].range("A6").value = cMod+" Percentages: "+;
			Iif(nTot-nIssue = 0, "0.00", Alltrim(Str(Round((nTot-nIssue)/nTot,4)*100,6,2)))+;
			"% ("+Alltrim(Str(nTot-nIssue))+"/"+Alltrim(Str(nTot))+")"
	EndCase
EndIf

if cbuilding="I" and used("kpilog")
replace step5 with .t. in kpilog
endif

Select * from csrDetail where &cIssueFltr order by building, acctName into cursor csrDetail
nRow = 12
Scan
  oworkbook.worksheets[1].range("A"+Alltrim(Str(nRow))).value = wo_num
  oworkbook.worksheets[1].range("B"+Alltrim(Str(nRow))).value = Alltrim(acctName)
  oworkbook.worksheets[1].range("C"+Alltrim(Str(nRow))).value = rcvDt
  oworkbook.worksheets[1].range("D"+Alltrim(Str(nRow))).value = building
  oworkbook.worksheets[1].range("E"+Alltrim(Str(nRow))).value = comments
  nRow = nRow+1
EndScan
*****End Receiving*****

cAcctFilter = Iif(Empty(cWhseAcct), "", "o."+cWhseAcct)
cFMIFltr = Iif(cOffice = "N", "!InList(o.ship_via, 'FMI', 'F M I', 'F MI', 'FM I','TGF','TOLL')", ".t.") &&exclude TGF loads

*****Order Completion*****
dApptDt = dStartDt
**NOTE: if you are updating entire cursor in kpichgdate, must be at the top of the cursor as it does a do while !eof()
=kpichgdate("dApptDt", 1, .f.)
dCreateDt = dApptDt
**NOTE: if you are updating entire cursor in kpichgdate, must be at the top of the cursor as it does a do while !eof()
=kpichgdate("dCreateDt", -2, .f.)

oworkbook.worksheets[2].range("A2").value = "Order Completion by Day of Appointment for {"+Dtoc(dApptDt)+"}: "+;
	cAcctName+" ("+Iif(cOffice="C","SP",Iif(cOffice="L","ML",Iif(cOffice="R","RI",Iif(cOffice="M", "FL", "NJ"))))+")"

cIssueFltr = "((Emptynul(staged) or staged > dApptDt) and wo_date <= dCreateDt)"

if cbuilding="I" and used("kpilog")
replace step6 with .t. in kpilog
endif

If cOffice # "C" or !Empty(cAcctFilter)
	xquery="select * from outship o where "+cAcctFilter+" appt = {"+transform(dApptDt)+"} and mod = '"+goffice+"'"
	xsqlexec(xquery,"csrdetail",,"wh",,,,,,,.t.)

	Select o.*, a.acctName, "" as building ;
		from csrdetail o left join account a on a.accountId = o.accountId ;
		where &cFMIFltr ;
	  into cursor csrDetail

	Sum Iif(&cIssueFltr, ctnQty, 0), ctnQty to nIssue, nTot
	oworkbook.worksheets[2].range("A5").value = "Percentage: "+;
		Iif(nTot-nIssue = 0, "0.00", Alltrim(Str(Round((nTot-nIssue)/nTot,4)*100,6,2)))+"% ("+Alltrim(Str(nTot-nIssue))+"/"+Alltrim(Str(nTot))+")"
Else
	xquery="select o.* from outship o where appt = {"+transform(dApptDt)+"} and mod = '"+goffice+"'"
	xsqlexec(xquery,"csrdetail",,"wh",,,,,,,.t.)

	Select o.*, a.acctName, acctGrpId, Iif(!Empty(cMod), "", Iif(acctGrpId=8, "SP1", "SP2")) as building ;
		from csrdetail o ;
			left join account a on a.accountId = o.accountId ;
			left join acctDet d on d.accountId = o.accountId ;
		where !InList(o.accountid,1285,3145,3980,4579,4654) and &cGrpFilter ; &&ignore age group, cayset, wormser & metro7
		group by outshipid ; &&group by in case multiple recs for acct in acctDet
	  into cursor csrDetail

	DO case
	  Case Empty(cMod)
		sum Iif(&cIssueFltr and acctGrpId = 8, ctnQty, 0), Iif(&cIssueFltr and acctGrpId = 7, ctnQty, 0), ;
			Iif(acctGrpId = 8, ctnQty, 0), Iif(acctGrpId = 7, ctnQty, 0) ;
				to nIssue1, nIssue2, nTot1, nTot2 &&scanned same day

		oworkbook.worksheets[2].range("A5").value = "SP1 Percentages: "+;
			Iif(nTot1-nIssue1 = 0, "0.00", Alltrim(Str(Round((nTot1-nIssue1)/nTot1,4)*100,6,2)))+;
			"% ("+Alltrim(Str(nTot1-nIssue1))+"/"+Alltrim(Str(nTot1))+")"
		oworkbook.worksheets[2].range("A6").value = "SP2 Percentages: "+;
			Iif(nTot2-nIssue2 = 0, "0.00", Alltrim(Str(Round((nTot2-nIssue2)/nTot2,4)*100,6,2)))+;
			"% ("+Alltrim(Str(nTot2-nIssue2))+"/"+Alltrim(Str(nTot2))+")"
	  Otherwise
		sum Iif(&cIssueFltr,ctnQty,0), ctnQty to nIssue, nTot
		oworkbook.worksheets[2].range("A5").value = cMod+" Percentages: "+;
			Iif(nTot-nIssue = 0, "0.00", Alltrim(Str(Round((nTot-nIssue)/nTot,4)*100,6,2)))+;
			"% ("+Alltrim(Str(nTot-nIssue))+"/"+Alltrim(Str(nTot))+")"
	EndCase
EndIf

if cbuilding="I" and used("kpilog")
replace step7 with .t. in kpilog
endif

Select *, sum(ctnQty) as totQty from csrDetail ;
	where &cIssueFltr order by building, acctName group by wo_num ;
  into cursor csrDetail
nRow = 11
Scan
  oworkbook.worksheets[2].range("A"+Alltrim(Str(nRow))).value = wo_num
  oworkbook.worksheets[2].range("B"+Alltrim(Str(nRow))).value = Alltrim(acctName)
  oworkbook.worksheets[2].range("C"+Alltrim(Str(nRow))).value = totQty
  oworkbook.worksheets[2].range("D"+Alltrim(Str(nRow))).value = building
  oworkbook.worksheets[2].range("E"+Alltrim(Str(nRow))).value = Iif(Emptynul(staged), "Unstaged", "Staged "+Dtoc(staged))
  nRow = nRow+1
EndScan

if cbuilding="I" and used("kpilog")
replace step8 with .t. in kpilog
endif

*****Truck Loading*****
oworkbook.worksheets[3].range("A2").value = "Truck Loading for {"+Dtoc(dStartDt)+"}: "+;
	cAcctName+" ("+Iif(cOffice="C","SP",Iif(cOffice="L","ML",Iif(cOffice="R","RI",Iif(cOffice="M", "FL", "NJ"))))+")"

If cOffice # "C" or !Empty(cAcctFilter)
	xquery="select o.* from outship o where "+cAcctFilter+" del_date = {"+transform(dStartDt)+"} and mod = '"+goffice+"'"
	xsqlexec(xquery,"csrdetail",,"wh",,,,,,,.t.)

	Select o.*, a.acctName, "" as building, 0000 as blAcct, .f. as started, 0000000 as totQty, 0000000 as loadQty ;
		from csrdetail o left join account a on a.accountId = o.accountId ;
		where &cFMIFltr and &cNotUps ;
	  into cursor csrDetail readwrite
Else
	xquery="select o.* from outship o where del_date = {"+transform(dStartDt)+"} and mod = '"+goffice+"'"
	xsqlexec(xquery,"csrdetail",,"wh",,,,,,,.t.)

	Select o.*, a.acctName, acctGrpId, Iif(!Empty(cMod), "", Iif(acctGrpId=8, "SP1", "SP2")) as building, ;
		   0000 as blAcct, .f. as started, 0000000 as totQty, 0000000 as loadQty ;
		from csrdetail o ;
			left join account a on a.accountId = o.accountId ;
			left join acctDet d on d.accountId = o.accountId ;
		where &cNotUps and !InList(o.accountid,1285,3145,3980,4579,4654) and ; &&ignore age group, polo (3pl), cayset, wormser & metro7
			  &cGrpFilter ;
		group by outshipid ;&&group by in case multiple recs for acct in acctDet
	  into cursor csrDetail readwrite
EndIf

select csrdetail
scan
	xsqlexec("select * from bl where bol_no = '"+csrdetail.bol_no+" and accountid="+transform(csrdetail.accountid)+" and mod='"+goffice+"'",,,"wh")
	if !eof("bl")
		replace blacct with bl.accountid, started with bl.started, totqty with bl.totqty, loadqty with bl.loadqty in csrdetail
	endif
	use in bl
endscan
locate

**if a master bol was created, must see if the truck loading was done under the individual bol
Scan for !Empty(bol_no) and !(started and loadQty >= totQty)
	xsqlexec("select * from bldet where mod='"+goffice+"' and wo_num="+transform(csrDetail.wo_num)+" and ship_ref = '"+Iif(csrDetail.combinedPt, csrDetail.combPt, csrDetail.ship_ref)+"'",,,"wh")
	xsqlexec("select * from bl where mblnum = '"+csrdetail.bol_no+"' and mod='"+goffice+"'",,,"wh")
	Select bl.* from bl left join bldet d on bl.blid = d.blId where !isnull(d.blid) into cursor csrbl

	use in bl
	use in bldet

	Select csrDetail
	If !Eof("csrBl")
		replace blAcct with csrBl.accountId, ;
	  			started with csrBl.started, ;
	  			totQty with csrBl.totQty, ;
	  			loadQty with csrBl.loadQty
	EndIf
	use in csrbl
EndScan

if cbuilding="I" and used("kpilog")
replace step10 with .t. in kpilog
endif

DO case
  Case cOffice # "C" or !Empty(cAcctFilter)
	Sum Iif(started and loadQty >= totQty, ctnQty, 0), ctnQty to nUsed, nTot
	oworkbook.worksheets[3].range("A6").value = "Percentage: "+;
		Iif(nUsed = 0, "0.00", Alltrim(Str(Round(nUsed/nTot,4)*100,6,2)))+"% ("+Alltrim(Str(nUsed))+"/"+Alltrim(Str(nTot))+")"

  Case Empty(cMod)
	sum Iif(started and loadQty >= totQty and building = "SP1", ctnQty, 0), ;
		Iif(started and loadQty >= totQty and building = "SP2", ctnQty, 0), ;
		Iif(building = "SP1", ctnQty, 0), Iif(building = "SP2", ctnQty, 0) ;
			to nUsed1, nUsed2, nTot1, nTot2

	oworkbook.worksheets[3].range("A6").value = "SP1 Percentages: "+;
		Iif(nUsed1 = 0, "0.00", Alltrim(Str(Round(nUsed1/nTot1,4)*100,6,2)))+;
		"% ("+Alltrim(Str(nUsed1))+"/"+Alltrim(Str(nTot1))+")"
	oworkbook.worksheets[3].range("A7").value = "SP2 Percentages: "+;
		Iif(nUsed2 = 0, "0.00", Alltrim(Str(Round(nUsed2/nTot2,4)*100,6,2)))+;
		"% ("+Alltrim(Str(nUsed2))+"/"+Alltrim(Str(nTot2))+")"

  Otherwise
	sum Iif(started and loadQty >= totQty, ctnQty, 0), ctnQty to nUsed, nTot
	oworkbook.worksheets[3].range("A6").value = cMod+" Percentages: "+;
		Iif(nUsed = 0, "0.00", Alltrim(Str(Round(nUsed/nTot,4)*100,6,2)))+;
		"% ("+Alltrim(Str(nUsed))+"/"+Alltrim(Str(nTot))+")"
EndCase

Select Iif(Empty(bol_no), Str(wo_num,20,0), bol_no) as bolDisp, sum(ctnQty) as totCtnQty, * from csrDetail ;
	where !started or loadQty < totQty OR IsNull(started) order by building, acctName ;
	group by bolDisp into cursor csrDetail
nRow = 12
Scan
  oworkbook.worksheets[3].range("A"+Alltrim(Str(nRow))).value = Alltrim(bolDisp)
  oworkbook.worksheets[3].range("B"+Alltrim(Str(nRow))).value = Alltrim(acctName)
  oworkbook.worksheets[3].range("C"+Alltrim(Str(nRow))).value = totCtnQty
  oworkbook.worksheets[3].range("D"+Alltrim(Str(nRow))).value = building
  oworkbook.worksheets[3].range("E"+Alltrim(Str(nRow))).value = Iif(!Empty(bol_no), "", "No BOL created.")
  nRow = nRow+1
EndScan

if cbuilding="I" and used("kpilog")
replace step11 with .t. in kpilog
endif

*****POD Percentage*****
oworkbook.worksheets[4].range("A2").value = "POD Scan Percentage for {"+Dtoc(dStartDt)+"}: "+;
	cAcctName+" ("+Iif(cOffice="C","SP",iif(cOffice="L","ML",Iif(cOffice="R","RI",Iif(cOffice="M", "FL", "NJ"))))+")"

DO case
  Case cOffice # "C" or !Empty(cAcctFilter)
	xquery="select o.* from outship o where "+cacctfilter+" between(del_date,{"+transform(dstartdt-7)+"},{"+transform(dstartdt)+"}) and mod = '"+goffice+"'"
	xsqlexec(xquery,"csrdetail",,"wh",,,,,,,.t.)

	select o.*, sum(ctnqty) as totqty, a.acctname, "" as building, 0000 as pdfacct, {} as updated, .f. as scanned, .f. as late  ;
		from csrdetail o left join account a on a.accountid = o.accountid ;
		where &cfmifltr and &cnotups and !empty(bol_no) ;
		group by bol_no ;
	  into cursor csrdetail readwrite

	scan
		xsqlexec("select * from blpdf where bol_num = '"+csrdetail.bol_no+"' and accountid="+transform(csrdetail.accountid),,,"wh")

		**must check for all accountid on this BL, may be mixed for account groups
		if eof("blpdf")
			xquery="select accountid from outship where between(del_date,{"+transform(dstartdt-7)+"},{"+transform(dstartdt)+"})"+;
				" and bol_no='"+csrdetail.bol_no+"' and accountid<>"+transform(csrdetail.accountid)+" and mod = '"+goffice+"' group by accountid"
			xsqlexec(xquery,"xtemp",,"wh",,,,,,,.t.)

			scan
				xsqlexec("select * from blpdf where bol_num = '"+csrdetail.bol_no+"' and accountid="+transform(xtemp.accountid),,,"wh")
				if !eof("blpdf")
					exit
				endif
			endscan
			use in xtemp
		endif

		do case
		**delete if scanned and delivered not the date specified
		case !eof("blpdf") and csrdetail.del_date#dStartDt
			delete in csrdetail
		case !eof("blpdf")
			replace pdfacct with blpdf.accountid, updated with blpdf.updated, scanned with iif(blpdf.updated=csrdetail.del_date,.t.,.f.), late with iif(blpdf.updated<=csrdetail.del_date+1,.t.,.f.) in csrdetail
		endcase
	endscan
	
	Sum Iif(!scanned, 0, totQty), Iif(!late, 0, totQty), Iif(del_date < dStartDt, 0, totQty) to nScanned, nLate, nTot &&scanned same day

	If inlist(cOffice,"C","L","R")
		oworkbook.worksheets[4].range("A6").value = "Percentage by next day: "+Iif(nLate = 0, "0.00", Alltrim(Str(Round(nLate/nTot,4)*100,6,2)))+;
			"% ("+Alltrim(Str(nLate))+"/"+Alltrim(Str(nTot))+")"
	Else
		oworkbook.worksheets[4].range("A6").value = "Percentage: "+Iif(nScanned = 0, "0.00", Alltrim(Str(Round(nScanned/nTot,4)*100,6,2)))+"%"+;
			"  By next day: "+Iif(nLate = 0, "0.00", Alltrim(Str(Round(nLate/nTot,4)*100,6,2)))+"% ("+Alltrim(Str(nLate))+"/"+Alltrim(Str(nTot))+")"
	EndIf

  Otherwise
	xquery="select o.* from outship o where between(del_date,{"+transform(dstartdt-7)+"},{"+transform(dstartdt)+"}) and mod = '"+goffice+"'"
	xsqlexec(xquery,"csrdetail",,"wh",,,,,,,.t.)

	select o.*, sum(ctnqty) as totqty, a.acctname, "" as building, 0000 as pdfacct, {} as updated, .f. as scanned, .f. as late  ;
		from csrdetail o left join account a on a.accountid = o.accountid ;
		where &cnotups and !empty(bol_no) and &cGrpFilter ;
		group by bol_no ;
	  into cursor csrdetail readwrite

	**ignore polo (3pl), cayset, wormser & metro7
	delete for InList(accountid,3145,3980,4579,4654)

	scan
		if empty(cmod) and seek(accountid,"acctdet","accountid")
			replace building with Iif(acctdet.acctGrpId=7,"SP2","SP1") in csrdetail
		endif

		xsqlexec("select * from blpdf where bol_num = '"+csrdetail.bol_no+"' and accountid="+transform(csrdetail.accountid),,,"wh")

		**must check for all accountid on this BL, may be mixed for account groups
		if eof("blpdf")
			xquery="select accountid from outship where between(del_date,{"+transform(dstartdt-7)+"},{"+transform(dstartdt)+"})"+;
				" and bol_no='"+csrdetail.bol_no+"' and accountid<>"+transform(csrdetail.accountid)+" and mod = '"+goffice+"' group by accountid"
			xsqlexec(xquery,"xtemp",,"wh",,,,,,,.t.)

			scan
				xsqlexec("select * from blpdf where bol_num = '"+csrdetail.bol_no+"' and accountid="+transform(xtemp.accountid),,,"wh")
				if !eof("blpdf")
					exit
				endif
			endscan
			use in xtemp
		endif

		do case
		**delete if scanned and delivered not the date specified
		case !eof("blpdf") and csrdetail.del_date#dStartDt
			delete in csrdetail
		case !eof("blpdf")
			replace pdfacct with blpdf.accountid, updated with blpdf.updated, scanned with iif(blpdf.updated=csrdetail.del_date,.t.,.f.), late with iif(blpdf.updated<=csrdetail.del_date+1,.t.,.f.) in csrdetail
		endcase
	endscan

	DO case
	  Case Type("cMod") # "C"
		sum Iif(scanned and building = "SP1", totQty, 0), Iif(scanned and building = "SP2", totQty, 0), ;
			Iif(late and building = "SP1", totQty, 0), Iif(late and building = "SP2", totQty, 0), ;
			Iif(building = "SP1" and del_date = dStartDt, totQty, 0), ;
			Iif(building = "SP2" and del_date = dStartDt, totQty, 0) ;
				to nScanned1, nScanned2, nLate1, nLate2, nTot1, nTot2 &&scanned same day

		oworkbook.worksheets[4].range("A6").value = "SP1 Percentages by next day: "+;
			Iif(nLate1 = 0, "0.00", Alltrim(Str(Round(nLate1/nTot1,4)*100,6,2)))+"% ("+Alltrim(Str(nLate1))+"/"+Alltrim(Str(nTot1))+")"
		oworkbook.worksheets[4].range("A7").value = "SP2 Percentages by next day: "+;
			Iif(nLate2 = 0, "0.00", Alltrim(Str(Round(nLate2/nTot2,4)*100,6,2)))+"% ("+Alltrim(Str(nLate2))+"/"+Alltrim(Str(nTot2))+")"

	  Otherwise
		sum Iif(scanned, totQty, 0), Iif(late, totQty, 0), Iif(del_date = dStartDt, totQty, 0) to nScanned, nLate, nTot &&scanned same day
		oworkbook.worksheets[4].range("A6").value = cMod+" Percentages by next day: "+;
			Iif(nLate = 0, "0.00", Alltrim(Str(Round(nLate/nTot,4)*100,6,2)))+"% ("+Alltrim(Str(nLate))+"/"+Alltrim(Str(nTot))+")"
	EndCase
EndCase

cFilter = Iif(inlist(cOffice,"C","L","R"), "!late", "!scanned")
Select * from csrDetail where &cFilter order by building, acctName into cursor csrDetail
nRow = 12
Scan
  oworkbook.worksheets[4].range("A"+Alltrim(Str(nRow))).value = Alltrim(bol_no)
  oworkbook.worksheets[4].range("B"+Alltrim(Str(nRow))).value = Alltrim(acctName)
  oworkbook.worksheets[4].range("C"+Alltrim(Str(nRow))).value = totQty
  oworkbook.worksheets[4].range("D"+Alltrim(Str(nRow))).value = building
  oworkbook.worksheets[4].range("E"+Alltrim(Str(nRow))).value = Iif(empty(pdfAcct), "Unscanned", "Scanned on "+Dtoc(updated))
  nRow = nRow+1
EndScan
*****

oWorkbook.Worksheets[1].range("A1").activate()
oWorkbook.Save()

if cbuilding="I" and used("kpilog")
replace step12 with .t. in kpilog
endif

if used('outship')
	use in outship
endif
use in acctgrp
use in acctdet
if used("bl")
	use in bl
endif
use in locchg
use in csrdetail
