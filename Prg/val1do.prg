* whmatch

guserid="AUTOMATCH"
gprocess=guserid

? xoffice+" match"

try
	use ("f:\auto\xmatch"+xoffice)
	copy to ("f:\auto\xmatchold"+xoffice) for invendiff#invendiff2
	use
catch
endtry
	
whmatch(xoffice,.t.)

matchfix(xoffice)
invenfix(xoffice)

use ("f:\auto\xmatch"+xoffice)

count for !fixed
if _tally>0
	email("dyoung@fmiint.com","VAL "+transform(_tally)+" bad records in XMATCH"+xoffice,"xmatch"+xoffice,,,,.t.,,,,,.t.)
endif

count for fixed
if _tally>0
	email("dyoung@fmiint.com","VAL (fixed) "+transform(_tally)+" bad records in XMATCH"+xoffice,"xmatch"+xoffice,,,,.t.,,,,,.t.)
endif

* whcheck
	
guserid="AUTOCHECK"
gprocess=guserid
goffice=xoffice
gmodfilter="mod='"+goffice+"'"

? xoffice+" check"
whcheck(xoffice,.t.)

select xcheck
delete for accountid=1 and file="OUTDET"

select xcheck
replace all fixed with .f.
scan 
	do case
	case data="PLINQTY"
		replace fixed with .t.
		xsqlexec("update inwolog set plinqty="+transform(xcheck.amount)+" where wo_num="+transform(xcheck.wo_num),,,"wh")

	case data="PLUNITSINQTY"
		replace fixed with .t.
		xsqlexec("update inwolog set plunitsinqty="+transform(xcheck.amount)+" where wo_num="+transform(xcheck.wo_num),,,"wh")
			
	case data="PLOUTQTY"
		replace fixed with .t.
		xsqlexec("update inwolog set ploutqty="+transform(xcheck.amount)+" where wo_num="+transform(xcheck.wo_num),,,"wh")
			
	case file="INLOC" and data="REMOVEQTY"
		replace fixed with .t.

	case file="INLOC" and data="QUANTITY"
		replace fixed with .t.

	case file="INDET" and data="REMOVEQTY"
		replace fixed with .t.
		xsqlexec("update inwolog set removeqty="+transform(xcheck.amount)+" where wo_num="+transform(xcheck.wo_num),,,"wh")
		
	case file="INDET" and data="UNITSOUTQTY"
		replace fixed with .t.
		xsqlexec("update inwolog set unitsoutqty="+transform(xcheck.amount)+" where wo_num="+transform(xcheck.wo_num),,,"wh")

	case file="INDET" and data="QUANTITY"
		replace fixed with .t.
		xsqlexec("update inwolog set quantity="+transform(xcheck.amount)+" where wo_num="+transform(xcheck.wo_num),,,"wh")

	case file="INDET" and data="UNITSINQTY"
		replace fixed with .t.
		xsqlexec("update inwolog set unitsinqty="+transform(xcheck.amount)+" where wo_num="+transform(xcheck.wo_num),,,"wh")
		
	case file="INDET" and data="CUFT"
		replace fixed with .t.
		xsqlexec("update inwolog set cuft="+transform(xcheck.amount)+" where wo_num="+transform(xcheck.wo_num),,,"wh")
		
	case file="INDET" and data="WEIGHT"
		replace fixed with .t.
		xsqlexec("update inwolog set weight="+transform(xcheck.amount)+" where wo_num="+transform(xcheck.wo_num),,,"wh")
		
	case file="OUTDET" and data="CUFT"
		replace fixed with .t.
		xsqlexec("update outwolog set cuft="+transform(xcheck.amount)+" where wo_num="+transform(xcheck.wo_num),,,"wh")

	case file="OUTDET" and data="WEIGHT"
		replace fixed with .t.
		xsqlexec("update outwolog set weight="+transform(xcheck.amount)+" where wo_num="+transform(xcheck.wo_num),,,"wh")

	case file="OUTSHIP" and data="WEIGHT"		&& outship must be fixed and whcheck does not have the outshipid <-- huh?
		replace fixed with .t.	
		xsqlexec("update outwolog set weight="+transform(xcheck.amount)+" where wo_num="+transform(xcheck.wo_num),,,"wh")
		
	case file="OUTSHIP" and data="CUFT"
		replace fixed with .t.		
		xsqlexec("update outwolog set cuft="+transform(xcheck.amount)+" where wo_num="+transform(xcheck.wo_num),,,"wh")
		
	case file="OUTSHIP" and data="CTNQTY"
		replace fixed with .t.		
		xsqlexec("update outwolog set ctnqty="+transform(xcheck.amount)+" where wo_num="+transform(xcheck.wo_num),,,"wh")
		
	case file="OUTSHIP" and data="QUANTITY"
		replace fixed with .t.		
		xsqlexec("update outwolog set quantity="+transform(xcheck.amount)+" where wo_num="+transform(xcheck.wo_num),,,"wh")
		
	case inlist(file,"INVEN","ALLOC")
		if !used("inven")
			useca("inven","wh",,,"mod='"+goffice+"'")
			useca("invenloc","wh",,,"mod='"+goffice+"'")
		endif
		
		scatter memvar
		m.id=m.idd
		replace fixed with .t.

		select inven
		locate for accountid=m.accountid and units=m.units and style=m.style and color=m.color and id=m.id and pack=m.pack
		if !found()
			insertinto("inven","wh",.t.)
		endif

		fixinven(.t.,,.t.)

	case file="INVQTY"
		if !used("inven")
			useca("inven","wh",,,"mod='"+goffice+"'")
			useca("invenloc","wh",,,"mod='"+goffice+"'")
		endif

		replace fixed with .t.
		replace availqty with inven.totqty-inven.allocqty-inven.holdqty in inven for invenid=xcheck.id
		tu("inven")
	endcase
endscan

select xcheck
delete for inlist(wo_num,3027567,5039263) && ,7010049)
copy to ("f:\auto\xcheck"+xoffice)

count for !fixed
if _tally>0
	email("dyoung@fmiint.com","VAL "+transform(_tally)+" bad records in XCHECK"+xoffice,"xcheck"+xoffice,,,,.t.,,,,,.t.)
endif

count for fixed and file#"PL" and !(file="INDET" and inlist(data,"WEIGHT","CUFT"))
if _tally>0
	email("dyoung@fmiint.com","VAL (fixed) "+transform(_tally)+" bad records in XCHECK"+xoffice,"xcheck"+xoffice,,,,.t.,,,,,.t.)
endif
