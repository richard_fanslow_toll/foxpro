***********************************************************************
*    File:	gpImage.prg
* Version:	1.5
* Created:	12.09.2002
*Modified:	10.05.2003
*  Author:	Alexander Golovlev
* Country:	Russian Federation
*   Email:	avg.kedr@overta.ru , golovlev@yandex.ru
***********************************************************************

#include gpImage.h

*-- Error messages
#define ERR_MODULE		"Cannot load a module "
#define ERR_PICTYPE		"Unsupported picture type"
#define ERR_CLIPNOTOPEN	"Cannot open the clipboard"
#define ERR_CLIPNODATA	"No bitmap data found on the clipboard"
#define ERR_CLIPSETDATA	"Cannot place data on the clipboard"

*-- Constants
#define MAX_PATH	260
#define LPTR		0x0040
#define SRCCOPY		13369376
#define CRLF		Chr(13) + Chr(10)
#define VT_DISPATCH	9
#define IID_IDispatch Chr(0x00)+Chr(0x04)+Chr(0x02)+Chr(0x00)+ ;
		Replicate(Chr(0x00), 4)+Chr(0xC0)+Replicate(Chr(0x00), 6)+Chr(0x46)

*-- Picture Types
#define PICTYPE_UNINITIALIZED	(-1)
#define PICTYPE_NONE			0
#define PICTYPE_BITMAP			1
#define PICTYPE_METAFILE		2
#define PICTYPE_ICON			3
#define PICTYPE_ENHMETAFILE		4 

*-- Predefined Clipboard Formats
#define CF_BITMAP				2
#define CF_PALETTE				9
#define OBJ_BITMAP				7

*-- Encoder CLSIDs
#define CLSID_BMP				"{557CF400-1A04-11D3-9A73-0000F81EF32E}"
#define CLSID_JPEG				"{557CF401-1A04-11D3-9A73-0000F81EF32E}"
#define CLSID_GIF				"{557CF402-1A04-11D3-9A73-0000F81EF32E}"
#define CLSID_TIFF				"{557CF405-1A04-11D3-9A73-0000F81EF32E}"
#define CLSID_PNG				"{557CF406-1A04-11D3-9A73-0000F81EF32E}"

*-- Encoder parameter sets
#define GUID_Compress			"{e09d739d-ccd4-44ee-8eba-3fbf8be4fc58}"
#define GUID_ColorDepth			"{66087055-ad66-4c7c-9a18-38a2310b8337}"
#define GUID_Quality			"{1d5be4b5-fa4a-452d-9cdd-5db35105e7eb}"
#define GUID_Transform			"{8d0eb2d1-a58e-4ea8-aa14-108074b7b6f9}"
#define GUID_SaveFlag			"{292266fc-ac40-47bf-8cfc-a85b89a655de}"

*-- Predefined multi-frame dimension IDs
#define GUID_Time				"{6aedbd6d-3fb5-418a-83a6-7f45229dc872}"
#define GUID_Page				"{7462dc86-6180-4c7e-8e3f-ee7333a7a483}"

*-- Image file format identifiers
#define GUID_FormatUndefined	"{b96b3ca9-0728-11d3-9d7b-0000f81ef32e}"
#define GUID_FormatMemoryBMP	"{b96b3caa-0728-11d3-9d7b-0000f81ef32e}"
#define GUID_FormatBMP			"{b96b3cab-0728-11d3-9d7b-0000f81ef32e}"
#define GUID_FormatEMF			"{b96b3cac-0728-11d3-9d7b-0000f81ef32e}"
#define GUID_FormatWMF			"{b96b3cad-0728-11d3-9d7b-0000f81ef32e}"
#define GUID_FormatJPEG			"{b96b3cae-0728-11d3-9d7b-0000f81ef32e}"
#define GUID_FormatPNG			"{b96b3caf-0728-11d3-9d7b-0000f81ef32e}"
#define GUID_FormatGIF			"{b96b3cb0-0728-11d3-9d7b-0000f81ef32e}"
#define GUID_FormatTIFF			"{b96b3cb1-0728-11d3-9d7b-0000f81ef32e}"
#define GUID_FormatEXIF			"{b96b3cb2-0728-11d3-9d7b-0000f81ef32e}"
#define GUID_FormatIcon			"{b96b3cb5-0728-11d3-9d7b-0000f81ef32e}"

*-- Pixel Formats
#define	PixelFormatIndexed		0x00010000	&& Indexes into a palette
#define	PixelFormatGDI			0x00020000	&& Is a GDI-supported format
#define	PixelFormatAlpha		0x00040000	&& Has an alpha component
#define	PixelFormatPAlpha		0x00080000	&& Pre-multiplied alpha
#define	PixelFormatExtended		0x00100000	&& Extended color 16 bits/channel
#define	PixelFormatCanonical	0x00200000

#define	PixelFormat1bppIndexed		( 1 + ( 1 * 256) + PixelFormatIndexed + PixelFormatGDI)
#define	PixelFormat4bppIndexed		( 2 + ( 4 * 256) + PixelFormatIndexed + PixelFormatGDI)
#define	PixelFormat8bppIndexed		( 3 + ( 8 * 256) + PixelFormatIndexed + PixelFormatGDI)
#define	PixelFormat16bppGrayScale	( 4 + (16 * 256) + PixelFormatExtended)
#define	PixelFormat16bppRGB555		( 5 + (16 * 256) + PixelFormatGDI)
#define	PixelFormat16bppRGB565		( 6 + (16 * 256) + PixelFormatGDI)
#define	PixelFormat16bppARGB1555	( 7 + (16 * 256) + PixelFormatAlpha + PixelFormatGDI)
#define	PixelFormat24bppRGB			( 8 + (24 * 256) + PixelFormatGDI)
#define	PixelFormat32bppRGB			( 9 + (32 * 256) + PixelFormatGDI)
#define	PixelFormat32bppARGB		(10 + (32 * 256) + PixelFormatAlpha + PixelFormatGDI + PixelFormatCanonical)
#define	PixelFormat32bppPARGB		(11 + (32 * 256) + PixelFormatAlpha + PixelFormatPAlpha + PixelFormatGDI)
#define	PixelFormat48bppRGB			(12 + (48 * 256) + PixelFormatExtended)
#define	PixelFormat64bppARGB		(13 + (64 * 256) + PixelFormatAlpha + PixelFormatCanonical + PixelFormatExtended)
#define	PixelFormat64bppPARGB		(14 + (64 * 256) + PixelFormatAlpha + PixelFormatPAlpha + PixelFormatExtended)


***********************************************************************
*   Class:	gpInit
***********************************************************************

Define Class gpInit as Custom
	Protected gdiplusToken

	gdiplusToken = 0

	Protected Procedure Init(tcFileName)
		Declare Long PathFindOnPath in Shlwapi.dll String @ pszFile, Long ppszOtherDirs
		Declare Long UuidFromString in rpcrt4.dll String StringUuid, String @ Uuid

		Declare Long LocalAlloc in Win32API Long uFlags, Long uBytes
		Declare Long LocalFree in Win32API Long hMem
		Declare Long LoadLibrary in Win32API String FileName
		Declare Long FreeLibrary in Win32API Long hModule

		Declare Long GdiplusStartup in GdiPlus.dll ;
			Long @ token, String @ input, Long @ output
		Declare Long GdiplusShutdown in GdiPlus.dll ;
			Long token
		Declare Long GdipLoadImageFromFile in GdiPlus.dll ;
			String FileName, Long @ GpImage
		Declare Long GdipSaveImageToFile in GdiPlus.dll ;
			Long GpImage, String FileName, String @ encoderClsid, String @ encoderParams
		Declare Long GdipSaveAddImage in GdiPlus.dll ;
			Long GpImage, Long newImage, String @ encoderParams
		Declare Long GdipCreateBitmapFromScan0 in GdiPlus.dll ;
			Integer width, Integer height, Integer stride, Long format, Long scan0, Long @ bitmap
		Declare Long GdipCreateBitmapFromResource in GdiPlus.dll ;
			Long hInstance, String bitmapName, Long @ bitmap
		Declare Long GdipCreateBitmapFromHBITMAP in GdiPlus.dll ;
			Long hbm, Long hpal, Long @ bitmap
		Declare Long GdipCreateBitmapFromHICON in GdiPlus.dll ;
			Long hicon, Long @ bitmap
		Declare Long GdipCreateMetafileFromWmf in GdiPlus.dll ;
			Long hWmf, Long deleteWmf, Long wmfPlaceableFileHeader, Long @ metafile
		Declare Long GdipCreateMetafileFromEmf in GdiPlus.dll ;
			Long hEmf, Long deleteEmf, Long @ metafile
		Declare Long GdipCreateHBITMAPFromBitmap in GdiPlus.dll ;
			Long nativeImage, Long @ hbmReturn, Long argb
		Declare Long GdipGetImageRawFormat in GdiPlus.dll ;
			Long nativeImage, String @ RawFormat
		Declare Long GdipGetImagePixelFormat in GdiPlus.dll ;
			Long nativeImage, Long @ PixelFormat
		Declare Long GdipGetImageWidth in GdiPlus.dll ;
			Long nativeImage, Long @ width
		Declare Long GdipGetImageHeight in GdiPlus.dll ;
			Long nativeImage, Long @ height
		Declare Long GdipGetImageThumbnail in GdiPlus.dll ;
			Long nativeImage, Long thumbWidth, Long thumbHeight, Long @ thumbimage, ;
			Long callback, Long callbackData
		Declare Long GdipDisposeImage in GdiPlus.dll ;
			Long GpImage

		Declare Long GdipBitmapGetPixel in GdiPlus.dll ;
			Long nativeImage, Long x, Long y, Long @ argb
		Declare Long GdipBitmapSetPixel in GdiPlus.dll ;
			Long nativeImage, Long x, Long y, Long argb
		Declare Long GdipGetImageHorizontalResolution in GdiPlus.dll ;
			Long nativeImage, Single @ resolution
		Declare Long GdipGetImageVerticalResolution in GdiPlus.dll ;
			Long nativeImage, Single @ resolution
		Declare Long GdipBitmapSetResolution in GdiPlus.dll ;
			Long nativeImage, Single xdpi, Single ydpi
		Declare Long GdipCloneBitmapAreaI in GdiPlus.dll ;
			Long x, Long y, Long width, Long height, ;
			Long format, Long nativeImage, Long @ gpdstBitmap
		Declare Long GdipImageRotateFlip in GdiPlus.dll ;
			Long nativeImage, Long rotateFlipType

		Declare Long GdipGetImageGraphicsContext in GdiPlus.dll ;
			Long image, Long @ graphics
		Declare Long GdipSetInterpolationMode in GdiPlus.dll ;
			Long graphics, Long interpolationMode
		Declare Long GdipDrawImageRectI in GdiPlus.dll ;
			Long graphics, Long image, Integer x, Integer y, Integer width, Integer height

		Declare Long GdipImageGetFrameCount in GdiPlus.dll ;
			Long image, String @ dimensionID, Long @ count
		Declare Long GdipImageSelectActiveFrame in GdiPlus.dll ;
			Long image, String @ dimensionID, Long frameIndex

		Declare Long OpenClipboard in Win32API Long hWnd
		Declare Long CloseClipboard in Win32API
		Declare Long EmptyClipboard in Win32API
		Declare Long GetClipboardData in Win32API Long uFormat
		Declare Long SetClipboardData in Win32API Long uFormat, Long hMem
		Declare Long CopyImage In Win32API Long hImage, Long uType, Long cx, Long cy, Long uFlags
		Declare Long DeleteObject in Win32API Long hObject
		Declare Long GetObjectType in Win32API Long h
 
		Declare Long GetDesktopWindow in Win32API
		Declare Long GetWindowDC in Win32API Long hwnd
		Declare Long GetWindowRect in Win32API Long hwnd, String @ lpRect
		Declare Long CreateCompatibleDC in Win32API Long hdc
		Declare Long CreateCompatibleBitmap in Win32API Long hdc, Long nWidth, Long nHeight
		Declare Long SelectObject in Win32API Long hdc, Long hObject
		Declare Long ReleaseDC in Win32API Long hwnd, Long hdc
		Declare Long DeleteDC in Win32API Long hdc
		Declare Long BitBlt in Win32API ;
			Long hDestDC, Long x, Long y, Long nWidth, Long nHeight, ;
			Long hSrcDC, Long xSrc, Long ySrc, Long dwRop
		Declare RtlMoveMemory in Win32API AS RtlCopyLong ;
			Integer @ DestNum, String @ pVoidSource, Integer nLength

#if Version(5) >= 700
		Declare Long OleCreatePictureIndirect In oleaut32 ;
			String @ PicDesc, String @ RefIID, Long fPictureOwnsHandle, Object @ IPic
#else
		Declare Long OleCreatePictureIndirect In oleaut32 ;
			String @ PicDesc, String @ RefIID, Long fPictureOwnsHandle, Long @ IPic
		Declare Long MultiByteToWideChar In kernel32 ;
			Long iCodePage, Long dwFlags, String @ lpStr, Long iMultiByte, ;
			String @ lpWideStr, Long iWideChar 
		Declare Long OleSavePictureFile In oleaut32 Long IPic, Long bstrFile
		Declare Long SysAllocString In oleaut32 String @ szString
		Declare SysFreeString In oleaut32 Long bstr
		Declare Long VariantClear in oleaut32 String @ pvarg
#endif

		Local gdiplusStartupInput, lcToken
	*	struct GdiplusStartupInput
	*	{
	*		UINT32 GdiplusVersion;				// Must be 1
	*		DebugEventProc DebugEventCallback;	// Ignored on free builds
	*		BOOL SuppressBackgroundThread;		// FALSE unless you're prepared to call 
	*											// the hook/unhook functions properly
	*		BOOL SuppressExternalCodecs;		// FALSE unless you want GDI+ only to use
	*											// its internal image codecs.
	*	}
		gdiplusStartupInput = Chr(1) + Replicate(Chr(0), 15)	&& GdiplusStartupInput structure (sizeof = 16)

		* Initialize GDI+.
		lcToken = 0
		If GdiplusStartup(@lcToken, @gdiplusStartupInput, 0) != 0
			Return .F.
		EndIf
		This.gdiplusToken = lcToken
	EndProc

	Protected Procedure Destroy
		GdiplusShutdown(This.gdiplusToken)

#if Version(5) >= 700
		Clear Dlls "PathFindOnPath", "UuidFromString"
		Clear Dlls "LocalAlloc", "LocalFree", "LoadLibrary", "FreeLibrary"
		Clear Dlls "GdiplusStartup", "GdiplusShutdown", ;
			"GdipLoadImageFromFile", "GdipSaveImageToFile", "GdipSaveAddImage", ;
			"GdipCreateBitmapFromScan0", "GdipCreateBitmapFromResource", "GdipCreateBitmapFromHBITMAP", ;
			"GdipCreateBitmapFromHICON", "GdipCreateMetafileFromWmf", "GdipCreateMetafileFromEmf", ;
			"GdipCreateHBITMAPFromBitmap", "GdipGetImageRawFormat", "GdipGetImagePixelFormat", ;
			"GdipGetImageWidth", "GdipGetImageHeight", "GdipGetImageThumbnail", "GdipDisposeImage"
		Clear Dlls "GdipBitmapGetPixel", "GdipBitmapSetPixel", "GdipGetImageHorizontalResolution", ;
			"GdipGetImageVerticalResolution", "GdipBitmapSetResolution", "GdipCloneBitmapAreaI", ;
			"GdipImageRotateFlip"
		Clear Dlls "GdipGetImageGraphicsContext", "GdipSetInterpolationMode", "GdipDrawImageRectI"
		Clear Dlls "GdipImageGetFrameCount", "GdipImageSelectActiveFrame"
		Clear Dlls "OpenClipboard", "CloseClipboard", "EmptyClipboard", "GetClipboardData", "SetClipboardData", ;
			"CopyImage", "DeleteObject", "GetObjectType", "OleCreatePictureIndirect"
		Clear Dlls "GetDesktopWindow", "GetWindowDC", "GetWindowRect", "CreateCompatibleDC", ;
			"CreateCompatibleBitmap", "SelectObject", "ReleaseDC", "DeleteDC", "BitBlt", "RtlMoveMemory"
#endif
	EndProc

EndDefine


***********************************************************************
*   Class:	gpImage
***********************************************************************

Define Class gpImage as Custom
	Protected stat
	Protected nativeImage

	nativeImage = 0
	ImageFormat = ""
	PixelFormat = ""
	ImageWidth = 0
	ImageHeight = 0
	HorizontalResolution = 0.0
	VerticalResolution = 0.0

	Protected Procedure Init(tcFileName)
		Local lnImage

		* Create Image from file
		lnImage = 0
		If !Empty(tcFileName)
			If (Vartype(tcFileName) != 'C') or !File(tcFileName)
				Return .F.
			EndIf
			This.stat = GdipLoadImageFromFile(This.WideStr(tcFileName), @lnImage)
			If This.stat = 0
				This.nativeImage = lnImage
			EndIf
		EndIf
	EndProc

	Protected Procedure Destroy
		If This.nativeImage != 0
			GdipDisposeImage(This.nativeImage)
		EndIf
		This.nativeImage = 0
	EndProc

	Procedure Create(tnWidth, tnHeight, tcPixelFormat)
		If (Vartype(tnWidth) != 'N') or (Vartype(tnHeight) != 'N')
			Error 11
		EndIf

		Local newImage, lnFormat
		newImage = 0
		lnFormat = This.StrToPixelFormat(tcPixelFormat)

		This.stat = GdipCreateBitmapFromScan0(tnWidth, tnHeight, 0, lnFormat, 0, @newImage)
		If This.stat = 0
			GdipDisposeImage(This.nativeImage)
			This.nativeImage = newImage
		EndIf
	EndProc

	Procedure Load(tcFileName)
		If Vartype(tcFileName) != 'C'
			Error 11
		EndIf
		If !File(tcFileName)
			Error 1, tcFileName
		EndIf

		Local lnImage
		If This.nativeImage != 0
			GdipDisposeImage(This.nativeImage)
		EndIf
		This.nativeImage = 0

		* Create Image from file
		lnImage = 0
		This.stat = GdipLoadImageFromFile(This.WideStr(tcFileName), @lnImage)
		If This.stat = 0
			This.nativeImage = lnImage
		EndIf
	EndProc

	Procedure FromResource(tcFileName, tcBitmapName)
		If (Vartype(tcFileName) != 'C') or (Vartype(tcBitmapName) != 'C')
			Error 11
		EndIf
		* first parameter is used to return a full qualified path name
		If PathFindOnPath(Padr(tcFileName, MAX_PATH, Chr(0)), 0) = 0
			Error 1, tcFileName
		EndIf

		Local hInstance, bmp
		If This.nativeImage != 0
			GdipDisposeImage(This.nativeImage)
		EndIf
		This.nativeImage = 0

		hInstance = LoadLibrary(tcFileName)
		If hInstance = 0
			Error ERR_MODULE + tcFileName
		endif
		bmp = 0
		This.stat = GdipCreateBitmapFromResource(hInstance, This.WideStr(tcBitmapName), @bmp)
		If This.stat = 0
			This.nativeImage = bmp
		EndIf
		FreeLibrary(hInstance)
	EndProc

	Procedure FromPicture(toPicture)
		If (Vartype(toPicture) != 'O') or (ComClassInfo(toPicture, 5) != '3')
			Error 11
		EndIf

		Local newImage
		If This.nativeImage != 0
			GdipDisposeImage(This.nativeImage)
		EndIf
		This.nativeImage = 0

		newImage = 0
		Do Case
		Case toPicture.Type = PICTYPE_BITMAP
			This.stat = GdipCreateBitmapFromHBITMAP(toPicture.Handle, toPicture.hPal, @newImage)
		Case toPicture.Type = PICTYPE_METAFILE
			This.stat = GdipCreateMetafileFromWmf(toPicture.Handle, 0, 0, @newImage)
		Case toPicture.Type = PICTYPE_ICON
			This.stat = GdipCreateBitmapFromHICON(toPicture.Handle, @newImage)
		Case toPicture.Type = PICTYPE_ENHMETAFILE
			This.stat = GdipCreateMetafileFromEmf(toPicture.Handle, 0, @newImage)
		Otherwise
			Error ERR_PICTYPE
		EndCase 
		If This.stat = 0
			This.nativeImage = newImage
		EndIf
	EndProc

	Function GetPicture()
		Local hBmp, PictDesc, IPic
		hBmp = 0
		This.stat = GdipCreateHBITMAPFromBitmap(This.nativeImage, @hBmp, 0xFFFFFFFF)
		If This.stat = 0
			* Create Picture object according to PICTDESC structure
			PictDesc = DWord(16) ;				&& Size of PICTDESC structure
					 + DWord(PICTYPE_BITMAP) ;	&& Type of picture
					 + DWord(hBmp) ;			&& HBITMAP
					 + DWord(0)					&& HPALETTE
			iid = IID_IDispatch
			IPic = 0
			OleCreatePictureIndirect(@PictDesc, @iid, 1, @IPic)
#if Version(5) >= 700
			Return IPic
#else
			Local lcTmpFile, lnBstr, lcVar, loPic
			lcTmpFile = "tmp.bmp"
			lnBstr = SysAllocString(This.WideStr(lcTmpFile))
			OleSavePictureFile(IPic, lnBstr)
			SysFreeString(lnBstr)
			* IPicture->Release()
			lcVar = Chr(VT_DISPATCH) + Replicate(Chr(0), 7) + DWord(IPic)
			VariantClear(@lcVar)
			loPic = LoadPicture(lcTmpFile)
			Erase (lcTmpFile)
			Return loPic
#endif
		endif
	EndProc

	Procedure FromClipboard()
		Local hBmp, hPal, bmp
		If This.nativeImage != 0
			GdipDisposeImage(This.nativeImage)
		EndIf
		This.nativeImage = 0

		hBmp = 0
		hPal = 0
		If OpenClipboard(0) != 0
			hBmp = GetClipboardData(CF_BITMAP)
			hPal = GetClipboardData(CF_PALETTE)
			CloseClipboard()
		Else
			Error ERR_CLIPNOTOPEN
		EndIf
		If hBmp = 0 Or GetObjectType(hBmp) <> OBJ_BITMAP
			Error ERR_CLIPNODATA
	    EndIf

		bmp = 0
		This.stat = GdipCreateBitmapFromHBITMAP(hBmp, hPal, @bmp)
		If This.stat = 0
			This.nativeImage = bmp
		EndIf
		DeleteObject(hBmp)
    EndProc

	Procedure ToClipboard()
		Local hBmp, hReturnBmp, hndl
		hReturnBmp = 0
		This.stat = GdipCreateHBITMAPFromBitmap(This.nativeImage, @hReturnBmp, 0xFFFFFFFF)
		hBmp = CopyImage(hReturnBmp, 0, 0, 0, 0)
		DeleteObject(hReturnBmp)
		If This.stat = 0
			If OpenClipboard(0) != 0
				EmptyClipboard()
				hndl = SetClipboardData(CF_BITMAP, hBmp)
				CloseClipboard()
				If hndl = 0
					Error ERR_CLIPSETDATA
				EndIf
			Else
				Error ERR_CLIPNOTOPEN
			EndIf
		EndIf
	EndProc

	Procedure Thumbnail(tnWidth, tnHeight)
		If (Vartype(tnWidth) != 'N') or (Vartype(tnHeight) != 'N')
			Error 11
		EndIf

		Local thumbImage
		thumbImage = 0
		This.stat = GdipGetImageThumbnail(This.nativeImage, tnWidth, tnHeight, @thumbImage, 0, 0)
		If This.stat = 0
			GdipDisposeImage(This.nativeImage)
			This.nativeImage = thumbImage
		EndIf
	EndProc

	Procedure SaveAsBMP(tcFileName)
		If Vartype(tcFileName) != 'C'
			Error 11
		EndIf

		Local encoderClsid, lcFile
		lcFile = DefaultExt(tcFileName, "bmp")
		Erase (lcFile)
		encoderClsid = This.StrToGuid(CLSID_BMP)
		This.stat = GdipSaveImageToFile(This.nativeImage, This.WideStr(lcFile), @encoderClsid, NULL)
	EndProc

	* tnQuality - quality of JPEG compression, 0-100, optional
	*			0 - highest compression, 100 - highest quality, default value - 75.
	* tnTransform - type of transformation without loss of information,
	*			any of EncoderValueTransform values, optional.
	Procedure SaveAsJPEG(tcFileName, tnQuality, tnTransform)
		If Vartype(tcFileName) != 'C'
			Error 11
		EndIf
		If !Empty(tnQuality) and (Vartype(tnQuality) != 'N')
			Error 11
		EndIf
		If !Empty(tnTransform) and (Vartype(tnTransform) != 'N')
			Error 11
		EndIf

		Local encoderClsid, lcFile, encoderParams, loQuality, loTransform, paramCnt
		lcFile = DefaultExt(tcFileName, "jpg")
		Erase (lcFile)
		encoderClsid = This.StrToGuid(CLSID_JPEG)

		paramCnt = 0
		encoderParams = ""
		If Vartype(tnQuality) = 'N'
			loQuality = CreateObject("EncoderParameter", This.StrToGuid(GUID_Quality), tnQuality)
			encoderParams = encoderParams + loQuality.GetString()
			paramCnt = paramCnt + 1
		EndIf
		If Vartype(tnTransform) = 'N'
			loTransform = CreateObject("EncoderParameter", This.StrToGuid(GUID_Transform), tnTransform)
			encoderParams = encoderParams + loTransform.GetString()
			paramCnt = paramCnt + 1
		EndIf

		If paramCnt > 0
		*	class EncoderParameters
		*	{
		*	public:
		*		UINT Count;						// Number of parameters in this structure
		*		EncoderParameter Parameter[1];	// Parameter values
		*	}
			encoderParams = DWord(paramCnt) + encoderParams
			This.stat = GdipSaveImageToFile(This.nativeImage, This.WideStr(lcFile), @encoderClsid, @encoderParams)
		Else
			This.stat = GdipSaveImageToFile(This.nativeImage, This.WideStr(lcFile), @encoderClsid, NULL)
		EndIf
	EndProc

	Procedure SaveAsGIF(tcFileName)
		If Vartype(tcFileName) != 'C'
			Error 11
		EndIf

		Local encoderClsid, lcFile
		lcFile = DefaultExt(tcFileName, "gif")
		Erase (lcFile)
		encoderClsid = This.StrToGuid(CLSID_GIF)
		This.stat = GdipSaveImageToFile(This.nativeImage, This.WideStr(lcFile), @encoderClsid, NULL)
	EndProc

	* tnCompress - type of compression, any of EncoderValueCompression values, optional.
	* tnColorDepth - number of colors in bits per pixel (1,4,8,16,24,32,48,64), optional.
	Procedure SaveAsTIFF(tcFileName, tnCompress, tnColorDepth)
		If Vartype(tcFileName) != 'C'
			Error 11
		EndIf
		If !Empty(tnCompress) and (Vartype(tnCompress) != 'N')
			Error 11
		EndIf
		If !Empty(tnColorDepth) and (Vartype(tnColorDepth) != 'N')
			Error 11
		EndIf

		Local encoderClsid, lcFile, encoderParams, loQuality, loTransform, paramCnt
		lcFile = DefaultExt(tcFileName, "tif")
		Erase (lcFile)
		encoderClsid = This.StrToGuid(CLSID_TIFF)

		paramCnt = 0
		encoderParams = ""
		If Vartype(tnCompress) = 'N'
			loCompress = CreateObject("EncoderParameter", This.StrToGuid(GUID_Compress), tnCompress)
			encoderParams = encoderParams + loCompress.GetString()
			paramCnt = paramCnt + 1
		EndIf
		If Vartype(tnColorDepth) = 'N'
			loColorDepth = CreateObject("EncoderParameter", This.StrToGuid(GUID_ColorDepth), tnColorDepth)
			encoderParams = encoderParams + loColorDepth.GetString()
			paramCnt = paramCnt + 1
		EndIf

		If paramCnt > 0
		*	class EncoderParameters
		*	{
		*	public:
		*		UINT Count;						// Number of parameters in this structure
		*		EncoderParameter Parameter[1];	// Parameter values
		*	}
			encoderParams = DWord(paramCnt) + encoderParams
			This.stat = GdipSaveImageToFile(This.nativeImage, This.WideStr(lcFile), @encoderClsid, @encoderParams)
		Else
			This.stat = GdipSaveImageToFile(This.nativeImage, This.WideStr(lcFile), @encoderClsid, NULL)
		EndIf
	EndProc

	* tnCompress - type of compression, any of EncoderValueCompression values, optional.
	* tnColorDepth - number of colors in bits per pixel (1,4,8,16,24,32,48,64), optional.
	Procedure SaveAsMultipageTIFF(tcFileName, tnCompress, tnColorDepth)
		If Vartype(tcFileName) != 'C'
			Error 11
		EndIf
		If !Empty(tnCompress) and (Vartype(tnCompress) != 'N')
			Error 11
		EndIf
		If !Empty(tnColorDepth) and (Vartype(tnColorDepth) != 'N')
			Error 11
		EndIf

		Local encoderClsid, lcFile, encoderParams, loQuality, loTransform, paramCnt
		lcFile = DefaultExt(tcFileName, "tif")
		Erase (lcFile)
		encoderClsid = This.StrToGuid(CLSID_TIFF)

		loCompress = CreateObject("EncoderParameter", This.StrToGuid(GUID_SaveFlag), EncoderValueMultiFrame)
		encoderParams = loCompress.GetString()
		paramCnt = 1

		If Vartype(tnCompress) = 'N'
			loCompress = CreateObject("EncoderParameter", This.StrToGuid(GUID_Compress), tnCompress)
			encoderParams = encoderParams + loCompress.GetString()
			paramCnt = paramCnt + 1
		EndIf
		If Vartype(tnColorDepth) = 'N'
			loColorDepth = CreateObject("EncoderParameter", This.StrToGuid(GUID_ColorDepth), tnColorDepth)
			encoderParams = encoderParams + loColorDepth.GetString()
			paramCnt = paramCnt + 1
		EndIf

		encoderParams = DWord(paramCnt) + encoderParams
		This.stat = GdipSaveImageToFile(This.nativeImage, This.WideStr(lcFile), @encoderClsid, @encoderParams)
	EndProc

	Function GetImage
		Return This.nativeImage
	EndFunc

	Procedure SaveAddPage(tnImage)
		If Empty(tnImage) and (Vartype(tnImage) != 'N')
			Error 11
		EndIf

		Local encoderParams, paramCnt
		loCompress = CreateObject("EncoderParameter", This.StrToGuid(GUID_SaveFlag), EncoderValueFrameDimensionPage)
		encoderParams = loCompress.GetString()
		paramCnt = 1

		encoderParams = DWord(paramCnt) + encoderParams
		This.stat = GdipSaveAddImage(This.nativeImage, tnImage, @encoderParams)
	EndProc

	Procedure SaveAsPNG(tcFileName)
		If Vartype(tcFileName) != 'C'
			Error 11
		EndIf

		Local encoderClsid, lcFile
		lcFile = DefaultExt(tcFileName, "png")
		Erase (lcFile)
		encoderClsid = This.StrToGuid(CLSID_PNG)
		This.stat = GdipSaveImageToFile(This.nativeImage, This.WideStr(lcFile), @encoderClsid, NULL)
	EndProc

	Procedure ImageFormat_Access
		Local lcFormat
		lcFormat = This.StrToGuid(GUID_FormatUndefined)
		This.stat = GdipGetImageRawFormat(This.nativeImage, @lcFormat)
		Do Case
		Case lcFormat = This.StrToGuid(GUID_FormatMemoryBMP) or lcFormat = This.StrToGuid(GUID_FormatBMP)
			Return "BMP"
		Case lcFormat = This.StrToGuid(GUID_FormatEMF)
			Return "EMF"
		Case lcFormat = This.StrToGuid(GUID_FormatWMF)
			Return "WMF"
		Case lcFormat = This.StrToGuid(GUID_FormatJPEG)
			Return "JPEG"
		Case lcFormat = This.StrToGuid(GUID_FormatPNG)
			Return "PNG"
		Case lcFormat = This.StrToGuid(GUID_FormatGIF)
			Return "GIF"
		Case lcFormat = This.StrToGuid(GUID_FormatTIFF)
			Return "TIFF"
		Case lcFormat = This.StrToGuid(GUID_FormatEXIF)
			Return "EXIF"
		Case lcFormat = This.StrToGuid(GUID_FormatIcon)
			Return "ICON"
		Otherwise
			Return "UNDEFINED"
		EndCase
	EndProc

	Procedure PixelFormat_Access
		Local lnFormat
		lnFormat = 0
		This.stat = GdipGetImagePixelFormat(This.nativeImage, @lnFormat)
		Do Case
		Case lnFormat = PixelFormat1bppIndexed
			Return "1bppIndexed"
		Case lnFormat = PixelFormat4bppIndexed
			Return "4bppIndexed"
		Case lnFormat = PixelFormat8bppIndexed
			Return "8bppIndexed"
		Case lnFormat = PixelFormat16bppGrayScale
			Return "16bppGrayScale"
		Case lnFormat = PixelFormat16bppRGB555
			Return "16bppRGB555"
		Case lnFormat = PixelFormat16bppRGB565
			Return "16bppRGB565"
		Case lnFormat = PixelFormat16bppARGB1555
			Return "16bppARGB1555"
		Case lnFormat = PixelFormat24bppRGB
			Return "24bppRGB"
		Case lnFormat = PixelFormat32bppRGB
			Return "32bppRGB"
		Case lnFormat = PixelFormat32bppARGB
			Return "32bppARGB"
		Case lnFormat = PixelFormat32bppPARGB
			Return "32bppPARGB"
		Case lnFormat = PixelFormat48bppRGB
			Return "48bppRGB"
		Case lnFormat = PixelFormat64bppARGB
			Return "64bppARGB"
		Case lnFormat = PixelFormat64bppPARGB
			Return "64bppPARGB"
		Otherwise
			Return "Undefined"
		EndCase
	EndProc

	Procedure PixelFormat_Assign(tcFormat)
		If Vartype(tcFormat) != 'C'
			Error 11
		EndIf

		Local lnFormat, lnWidth, lnHeight, gpdstBitmap
		lnFormat = this.StrToPixelFormat(tcFormat)

		lnWidth = 0
		This.stat = GdipGetImageWidth(This.nativeImage, @lnWidth)
		lnHeight = 0
		This.stat = GdipGetImageHeight(This.nativeImage, @lnHeight)
		gpdstBitmap = 0
		This.stat = GdipCloneBitmapAreaI(0, 0, lnWidth, lnHeight, ;
			lnFormat, This.nativeImage, @gpdstBitmap)
		If This.stat = 0
			GdipDisposeImage(This.nativeImage)
			This.nativeImage = gpdstBitmap
		EndIf
	EndProc

	Procedure ImageWidth_Access
		Local lnWidth
		lnWidth = 0
		This.stat = GdipGetImageWidth(This.nativeImage, @lnWidth)
		Return lnWidth
	EndProc

	Procedure ImageHeight_Access
		Local lnHeight
		lnHeight = 0
		This.stat = GdipGetImageHeight(This.nativeImage, @lnHeight)
		Return lnHeight
	EndProc

	Procedure HorizontalResolution_Access
		Local lnResolution
		lnResolution = 0
		This.stat = GdipGetImageHorizontalResolution(This.nativeImage, @lnResolution)
		Return lnResolution
	EndProc

	Procedure VerticalResolution_Access
		Local lnResolution
		lnResolution = 0
		This.stat = GdipGetImageVerticalResolution(This.nativeImage, @lnResolution)
		Return lnResolution
	EndProc

	Procedure HorizontalResolution_Assign(tnResolution)
		If Vartype(tnResolution) != 'N'
			Error 11
		EndIf

		Local lnVertRes
		lnVertRes = 0
		This.stat = GdipGetImageVerticalResolution(This.nativeImage, @lnVertRes)
		This.stat = GdipBitmapSetResolution(This.nativeImage, tnResolution, lnVertRes)
	EndProc

	Procedure VerticalResolution_Assign(tnResolution)
		If Vartype(tnResolution) != 'N'
			Error 11
		EndIf

		Local lnHorRes
		lnHorRes = 0
		This.stat = GdipGetImageHorizontalResolution(This.nativeImage, @lnHorRes)
		This.stat = GdipBitmapSetResolution(This.nativeImage, lnHorRes, tnResolution)
	EndProc

	Procedure GetPixelColor(x, y)
		If (Vartype(x) != 'N') or (Vartype(y) != 'N')
			Error 11
		EndIf

		Local argb, red, green, blue
		argb = 0
		This.stat = GdipBitmapGetPixel(This.nativeImage, x, y, @argb)
		red = Bitrshift(Bitand(argb, 0x00FF0000), 16)
		green = Bitrshift(Bitand(argb, 0x0000FF00), 8)
		blue = Bitand(argb, 0x000000FF)
		Return red + Bitlshift(green, 8) + Bitlshift(blue, 16)
	EndProc

	Procedure GetPixelAlpha(x, y)
		If (Vartype(x) != 'N') or (Vartype(y) != 'N')
			Error 11
		EndIf

		Local argb
		argb = 0
		This.stat = GdipBitmapGetPixel(This.nativeImage, x, y, @argb)
		Return Bitrshift(argb, 24)
	EndProc

	Procedure SetPixel(x, y, tnColor, tnAlpha)
		If (Vartype(x) != 'N') or (Vartype(y) != 'N') or (Vartype(tnColor) != 'N')
			Error 11
		EndIf
		If !Empty(tnAlpha) and (Vartype(tnAlpha) != 'N')
			Error 11
		EndIf

		Local argb, red, green, blue
		blue = Bitrshift(Bitand(tnColor, 0x00FF0000), 16)
		green = Bitrshift(Bitand(tnColor, 0x0000FF00), 8)
		red = Bitand(tnColor, 0x000000FF)
		argb = blue + Bitlshift(green, 8) + Bitlshift(red, 16)
		If Vartype(tnAlpha) = 'N'
			argb = argb + Bitlshift(Bitand(tnAlpha, 0xFF), 24)
		Else
			argb = argb + Bitlshift(255, 24)
		EndIf
		This.stat = GdipBitmapSetPixel(This.nativeImage, x, y, argb)
	EndProc

	Procedure Crop(x, y, tnWidth, tnHeight)
		If (Vartype(x) != 'N') or (Vartype(y) != 'N')
			Error 11
		EndIf
		If (Vartype(tnWidth) != 'N') or (Vartype(tnHeight) != 'N')
			Error 11
		EndIf

		Local lnFormat, gpdstBitmap
		lnFormat = 0
		This.stat = GdipGetImagePixelFormat(This.nativeImage, @lnFormat)
		gpdstBitmap = 0
		This.stat = GdipCloneBitmapAreaI(x, y, tnWidth, tnHeight, ;
			lnFormat, This.nativeImage, @gpdstBitmap)
		If This.stat = 0
			GdipDisposeImage(This.nativeImage)
			This.nativeImage = gpdstBitmap
		EndIf
	EndProc

	Procedure RotateFlip(rotateFlipType)
		If Vartype(rotateFlipType) != 'N'
			Error 11
		EndIf

		This.stat = GdipImageRotateFlip(This.nativeImage, rotateFlipType)
	EndProc

	Procedure Resize(tnWidth, tnHeight, tnInterpolationMode)
		If (Vartype(tnWidth) != 'N') or (Vartype(tnHeight) != 'N')
			Error 11
		EndIf
		If !Empty(tnInterpolationMode) and (Vartype(tnInterpolationMode) != 'N')
			Error 11
		EndIf
		If Vartype(tnInterpolationMode) != 'N'
			tnInterpolationMode = InterpolationModeDefault
		EndIf

		Local lnFormat, graphics, resizedImage
		lnFormat = 0
		graphics = 0
		resizedImage = 0

		This.stat = GdipGetImagePixelFormat(This.nativeImage, @lnFormat)
		This.stat = GdipCreateBitmapFromScan0(tnWidth, tnHeight, 0, lnFormat, 0, @resizedImage)
		If This.stat != 0
			Return
		EndIf
		This.stat = GdipGetImageGraphicsContext(resizedImage, @graphics)
		If This.stat != 0
			Return
		EndIf
		This.stat = GdipSetInterpolationMode(graphics, tnInterpolationMode)
		This.stat = GdipDrawImageRectI(graphics, This.nativeImage, 0, 0, tnWidth, tnHeight)
		If This.stat = 0
			GdipDisposeImage(This.nativeImage)
			This.nativeImage = resizedImage
		EndIf
	EndProc

	Function GetPageCount()
		Local dimensionID, count
		dimensionID = This.StrToGuid(GUID_Page)
		count = 0

		GdipImageGetFrameCount(This.nativeImage, @dimensionID, @count)
		Return count
	EndFunc

	Procedure SelectPage(tnPage)
		If (Vartype(tnPage) != 'N')
			Error 11
		EndIf

		Local dimensionID
		dimensionID = This.StrToGuid(GUID_Page)
		GdipImageSelectActiveFrame(This.nativeImage, @dimensionID, tnPage)
	EndProc

	Function GetFrameCount()
		Local dimensionID, count
		dimensionID = This.StrToGuid(GUID_Time)
		count = 0

		GdipImageGetFrameCount(This.nativeImage, @dimensionID, @count)
		Return count
	EndFunc

	Procedure SelectFrame(tnFrame)
		If (Vartype(tnFrame) != 'N')
			Error 11
		EndIf

		Local dimensionID
		dimensionID = This.StrToGuid(GUID_Time)
		GdipImageSelectActiveFrame(This.nativeImage, @dimensionID, tnFrame)
	EndProc

	Procedure Capture(thWnd)
		If VarType(thWnd) != 'N'
			Error 11
		EndIf

		If This.nativeImage != 0
			GdipDisposeImage(This.nativeImage)
		EndIf
		This.nativeImage = 0

		Local bmp
		Local lhDC, lhMemDC, lhOldBmp, lhMemBmp
		Local lnWidth, lnHeight

		If Empty(thWnd)
			thWnd = GetDesktopWindow()
		EndIf
		lhDC = GetWindowDC(thWnd)
		= This.GetWinRect(thWnd, @lnWidth, @lnHeight)

	    lhMemDC = CreateCompatibleDC(lhDC)
	    lhMemBmp = CreateCompatibleBitmap(lhDC, lnWidth, lnHeight)

	    lhOldBmp = SelectObject(lhMemDC, lhMemBmp)
	    = BitBlt(lhMemDC, 0, 0, lnWidth, lnHeight, lhDC, 0, 0, SRCCOPY)
	    = SelectObject(lhMemDC, lhOldBmp)

		= DeleteDC(lhMemDC)
	    = ReleaseDC(thWnd, lhDC)

		bmp = 0
		This.stat = GdipCreateBitmapFromHBITMAP(lhMemBmp, 0, @bmp)
		If This.stat = 0
			This.nativeImage = bmp
		EndIf
		DeleteObject(lhMemBmp)
	EndProc

***********************************************************************
* Protected functions
***********************************************************************

	Protected Procedure GetWinRect(hWnd, lnWidth, lnHeight)
	* Returns width and height of window, passed by handle
	    Local lpRect, lnLeft, lnTop, lnRight, lnBottom
	    lpRect = Replicate(Chr(0), 16)
	    = GetWindowRect(hWnd, @lpRect)

	    lnLeft   = This.LongToNum(SubStr(lpRect,  1, 4))   
	    lnTop    = This.LongToNum(SubStr(lpRect,  5, 4))   
	    lnRight  = This.LongToNum(SubStr(lpRect,  9, 4))   
	    lnBottom = This.LongToNum(SubStr(lpRect, 13, 4))   

	    lnWidth  = lnRight - lnLeft 
	    lnHeight = lnBottom - lnTop 
	EndProc

	Protected Function LongToNum(tcLong)
	* Converts a signed long value to a VFP numeric
		Local nNum
		nNum = 0
		RtlCopyLong(@nNum, tcLong, 4)
		Return nNum
	EndFunc

	Protected Function StrToPixelFormat(tcFormat)
		tcFormat = Upper(tcFormat)
		Do Case
		Case tcFormat = "1BPPINDEXED"
			return PixelFormat1bppIndexed
		Case tcFormat = "4BPPINDEXED"
			return PixelFormat4bppIndexed
		Case tcFormat = "8BPPINDEXED"
			return PixelFormat8bppIndexed
		Case tcFormat = "16BPPGRAYSCALE"
			return PixelFormat16bppGrayScale
		Case tcFormat = "16BPPRGB555"
			return PixelFormat16bppRGB555
		Case tcFormat = "16BPPRGB565"
			return PixelFormat16bppRGB565
		Case tcFormat = "16BPPARGB1555"
			return PixelFormat16bppARGB1555
		Case tcFormat = "24BPPRGB"
			return PixelFormat24bppRGB
		Case tcFormat = "32BPPRGB"
			return PixelFormat32bppRGB
		Case tcFormat = "32BPPARGB"
			return PixelFormat32bppARGB
		Case tcFormat = "32BPPPARGB"
			return PixelFormat32bppPARGB
		Case tcFormat = "48BPPRGB"
			return PixelFormat48bppRGB
		Case tcFormat = "64BPPARGB"
			return PixelFormat64bppARGB
		Case tcFormat = "64BPPPARGB"
			return PixelFormat64bppPARGB
		Otherwise
			Error 11
		EndCase
	EndFunc

	Protected Function StrToGuid(s)
		Local guid
		guid = Space(16)
		s = Chrtran(s, "{}", "")
		UuidFromString(s, @guid)
		Return guid
	EndFunc

	Protected Function WideStr(tcStr)
#if Version(5) >= 700
		Return Strconv(tcStr + Chr(0), 5)
#else
		Local lnLen, lcWideStr
		lnLen = 2 * (Len(tcStr) + 1)
		lcWideStr = Replicate(Chr(0), lnLen)
		MultiByteToWideChar(0, 0, @tcStr, Len(tcStr), @lcWideStr, lnLen)
		Return lcWideStr
#endif
	EndFunc

	Protected Procedure stat_Assign(tnStatus)
		If tnStatus != 0
			Local res
			res = MessageBox("GDI+ error in " + Program(Program(-1) - 1) + CRLF;
				+ "Error code : " + Transform(tnStatus) + CRLF ;
				+ "Description: " + This.ErrorInfo(tnStatus) + CRLF + CRLF;
				+ "Press 'Retry' to debug the application.", 16 + 2, "Error")
*			If res = 3		&& dy 2/23/10
*				Cancel
*			EndIf
*			If res = 4
*				Suspend
*			EndIf
		EndIf
		This.stat = tnStatus
	EndProc

	Protected Function ErrorInfo(tnStatus)
		Do Case
		Case tnStatus = 0
			Return "Ok"
		Case tnStatus = 1
			Return "Generic Error"
		Case tnStatus = 2
			Return "Invalid Parameter"
		Case tnStatus = 3
			Return "Out Of Memory"
		Case tnStatus = 4
			Return "Object Busy"
		Case tnStatus = 5
			Return "Insufficient Buffer"
		Case tnStatus = 6
			Return "Not Implemented"
		Case tnStatus = 7
			Return "Win32 Error"
		Case tnStatus = 8
			Return "Wrong State"
		Case tnStatus = 9
			Return "Aborted"
		Case tnStatus = 10
			Return "File Not Found"
		Case tnStatus = 11
			Return "Value Overflow"
		Case tnStatus = 12
			Return "Access Denied"
		Case tnStatus = 13
			Return "Unknown Image Format"
		Case tnStatus = 14
			Return "Font Family Not Found"
		Case tnStatus = 15
			Return "Font Style Not Found"
		Case tnStatus = 16
			Return "Not True Type Font"
		Case tnStatus = 17
			Return "Unsupported Gdiplus Version"
		Case tnStatus = 18
			Return "Gdiplus Not Initialized"
		Case tnStatus = 19
			Return "Property Not Found"
		Case tnStatus = 20
			Return "Property Not Supported"
		Otherwise
			Return "Unknown Error"
		EndCase
	EndFunc

EndDefine


***********************************************************************
*   Class:	EncoderParameter
***********************************************************************

Define Class EncoderParameter as Custom
	*	class EncoderParameter
	*	{
	*	public:
	*		GUID    Guid;				// GUID of the parameter
	*		ULONG   NumberOfValues;		// Number of the parameter values
	*		ULONG   Type;				// Value type, like ValueTypeLONG  etc.
	*		VOID*   Value;				// A pointer to the parameter values
	*	}

	Protected ValuePtr
	Protected encoderParam

	Procedure Init(lcGuid, lnValue)
		If (Vartype(lcGuid) != 'C') or (Vartype(lnValue) != 'N')
			Error 11
		EndIf

		This.ValuePtr = LocalAlloc(LPTR, 4)
		If This.ValuePtr = 0
			Error 1149
		EndIf
		Sys(2600, This.ValuePtr, 4, DWord(lnValue))
		This.encoderParam = lcGuid + DWord(1) + DWord(4)
		This.encoderParam = This.encoderParam + DWord(This.ValuePtr)
	EndFunc

	Procedure Destroy
		LocalFree(This.ValuePtr)
	EndFunc

	Function GetString
		Return This.encoderParam
	EndFunc

EndDefine


***********************************************************************

Function DWord(lnValue)
	* Creates a DWORD (unsigned 32-bit) 4 byte string from a number
	Local byte(4)
	If lnValue < 0
		lnValue = lnValue + 4294967296
	EndIf
	byte(1) = lnValue % 256
	byte(2) = BitRShift(lnValue, 8) % 256
	byte(3) = BitRShift(lnValue, 16) % 256
	byte(4) = BitRShift(lnValue, 24) % 256
	Return Chr(byte(1))+Chr(byte(2))+Chr(byte(3))+Chr(byte(4))
EndFunc
