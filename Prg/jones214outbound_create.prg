PARAMETERS cStatusCode,nWO_Num,cOffice,cReasonCode,dTrig_time

PUBLIC c_CntrlNum,c_GrpCntrlNum,cWO_Num,cGroupName,cDivision,cCustName,cFilename,div214,cST_CSZ
PUBLIC tsendto,tcc,tsendtoerr,tccerr,cFin_Status,lDoCatch,lDoJfilesout,cPrgName

cPrgName =  "JONES 214"
set step On 

lTesting = .F.
lDoJfilesout = !lTesting
lEmail = !lTesting
lUseTestData = .F.
cProcType = ""

DO m:\dev\prg\_setvars WITH lTesting

IF lTesting
	CLOSE DATA ALL
ELSE
	closefiles()
ENDIF

cFilename = ""
lFClose = .T.
lIsError = .F.
lDoCatch = .T.
cShip_ref = ""
cST_CSZ = ""
cFin_Status = ""
nFilenum = 0

*ASSERT .f.
TRY
	cSystemName = "UNASSIGNED"
	nAcctNum = 1
	cWO_Num = ""
	lDoError = .F.

	IF lTesting
*cStatusCode = "X1"
		cStatusCode = "B6"
*cStatusCode = "SD"
*cStatusCode = "CL"
*cReasonCode = "NS"
		cReasonCode = "NS"

		cOffice = "C"
		nWO_Num = 1465124
		dTrig_time = DATETIME()
	ENDIF

	cWO_Num = TRANSFORM(nWO_Num)

	DO CASE
		CASE cStatusCode = "B6"
			cProcType = "Available"
		CASE cStatusCode = "SD"
			cProcType = "Not Available"
		CASE cStatusCode = "X1"
			cProcType = "Pickup Confirmed"
		CASE cStatusCode = "CL"
			cProcType = "Empty Return"
		OTHERWISE
			cProcType = "UNK"
	ENDCASE

	IF USED("manifest")
		USE IN manifest
	ENDIF
	USE F:\wo\wodata\manifest IN 0 ALIAS manifest SHARED NOUPDATE

	IF USED("detail")
		USE IN DETAIL
	ENDIF
	USE F:\wo\wodata\DETAIL IN 0 ALIAS DETAIL SHARED NOUPDATE

	IF USED("wolog")
		USE IN wolog
	ENDIF
	USE F:\wo\wodata\wolog IN 0 ALIAS  wolog ORDER TAG wo_num SHARED NOUPDATE

	xsqlexec("select * from pudlocs",,,"wo")
	index on accountid tag accountid
	set order to

	IF !SEEK(nWO_Num,"WOLOG","WO_NUM")
		cFin_Status = "MISS WO-WOLOG"
		THROW
	ENDIF

	DO CASE
		CASE wolog.TYPE = "A"
			DO CASE
				CASE cOffice = "M"
					cDivision = "Florida"
					cOffCity = "MIAMI"
					cOffState = "FL"
					cCityUsed = "MIAMI, FL"
				CASE cOffice = "C"
					cDivision = "San Pedro"
					cOffCity = "SANPEDRO"
					cOffState = "CA"
					cCityUsed = "INGLEWOOD, CA"
				OTHERWISE
					cDivision = "New Jersey"
					cOffCity = "CARTERET"
					cOffState = "NJ"
					cCityUsed = "JAMAICA, NY"
			ENDCASE
		CASE wolog.TYPE='D' AND !EMPTY(wolog.fromloc)
			SELECT pudlocs
			LOCATE FOR dash=wolog.fromloc AND accountid=wolog.accountid
			IF FOUND()
				cCityUsed = ALLTRIM(pudlocs.city)+", "+ALLTRIM(pudlocs.state)
			ENDIF
		OTHERWISE
			DO CASE
				CASE wolog.office='C'
					xCity = "LONG BEACH"
					xState = "CA"
				CASE wolog.office='M'
					Xcity = "MIAMI"
					Xstate = "FL"
				CASE wolog.office = 'N' AND wolog.puloc= "HOWLAND"
					Xcity = "STATEN ISLAND"
					Xstate = "NY"
				OTHERWISE
					Xcity = "ELIZABETH"
					Xstate = "NJ"
			ENDCASE
			cCityUsed = ALLTRIM(Xcity)+", "+ALLTRIM(Xstate)
	ENDCASE


	DIMENSION thisarray(1)
	cCustName = "JONES"
	cMailName = "JONES"
	cCustFolder = "JONES"
	dtmail = TTOC(DATETIME())
	dt2 = DATETIME()
	cCustPrefix = cCustName+"214_"
	dt1 = TTOC(DATETIME(),1)

	IF USED('mailmaster')
	USE IN mailmaster
	ENDIF
	
	SELECT 0
	USE F:\3pl\DATA\mailmaster ALIAS mm SHARED
	LOCATE FOR mm.edi_type = "214" AND mm.accountid = nAcctNum
	_SCREEN.CAPTION = ALLTRIM(mm.scaption)
	tsendto = IIF(mm.use_alt,mm.sendtoalt,mm.sendto)
	tcc = IIF(mm.use_alt,mm.ccalt,mm.cc)
	cFilename = ALLTRIM(mm.holdpath)+("LNIJA2SGL"+dt1+".214")
	cFilenameShort = JUSTFNAME(cFilename)
	cFilenameArch = (ALLTRIM(mm.archpath)+cFilenameShort)
	cFilenameOut = (ALLTRIM(mm.basepath)+cFilenameShort)
	LOCATE
	LOCATE FOR mm.edi_type = "MISC" AND mm.taskname = "GENERAL"
	tsendtoerr = IIF(mm.use_alt,mm.sendtoalt,mm.sendto)
	tccerr = IIF(mm.use_alt,mm.ccalt,mm.cc)
	USE IN mm

	tfrom = "TGF Warehouse Operations <fmi-warehouse-ops@fmiint.com>"
	tattach = ""

	SELECT 0
	nFilenum = FCREATE(cFilename)

	cfd = "*"  && Field delimiter
	csegd = ""  && Segment delimiter
	cterminator = ">"  && Used at end of ISA segment

	csendqual = "01"  && Sender qualifier
	csendid = "TOLLGLOBAL"  && Sender ID code
	crecqual = "ZZ"  && Recip qualifier
	crecid = "LNIJA2214"   && Recip ID Code

	cDate = DTOS(DATE())
	cTruncDate = RIGHT(cDate,6)
	cTruncTime = SUBSTR(TTOC(DATETIME(),1),9,4)
	cfiledate = cDate+cTruncTime
	csendidlong = PADR(csendid,15," ")
	crecidlong = PADR(crecid,15," ")
	cString = ""

	nSTCount = 0
	nSegCtr = 0
	nLXNum = 1

	STORE "" TO cEquipNum,cB10

	WAIT CLEAR
	WAIT WINDOW "Now creating Trucking WO#-based 214.." NOWAIT

	cISACode = IIF(lTesting,"T","P")

	DO num_incr_isa
	cISA_Num = PADL(c_CntrlNum,9,"0")

	STORE "ISA"+cfd+"00"+cfd+"          "+cfd+"00"+cfd+"          "+cfd+csendqual+cfd+csendidlong+cfd+;
		crecqual+cfd+crecidlong+cfd+cTruncDate+cfd+cTruncTime+cfd+"U"+cfd+"00401"+cfd+;
		cISA_Num+cfd+"0"+cfd+cISACode+cfd+":"+csegd TO cString  && +cfd+cterminator
	DO cstringbreak

	STORE "GS"+cfd+"QM"+cfd+csendid+cfd+crecid+cfd+cDate+cfd+cTruncTime+cfd+c_CntrlNum+;
		cfd+"X"+cfd+"004010"+csegd TO cString
	DO cstringbreak
	DO num_incr_st

	STORE "ST"+cfd+"214"+cfd+PADL(c_GrpCntrlNum,9,"0")+csegd TO cString
	DO cstringbreak
	nSTCount = nSTCount + 1
	nSegCtr = nSegCtr + 1
	cRefID = ALLT(wolog.brokref)

	DO CASE
		CASE wolog.TYPE="D" AND SEEK(wolog.wo_num,"manifest","wo_num")
			cB10=ALLTRIM(manifest.hawb)
		CASE SEEK(wolog.wo_num,"detail","wo_num")
			cB10=ALLTRIM(DETAIL.hawb)
		OTHERWISE
			cB10 = ""  && Added as ok to be blank, per Mike W., 10.05.2012
*			cFin_Status = "MISSING HAWB, WO "+cWO_Num
*			THROW
	ENDCASE

	STORE "B10"+cfd+cB10+cfd+cfd+"FMIX"+csegd TO cString
	DO cstringbreak
	nSegCtr = nSegCtr + 1

*!* Now in line loop
	STORE "LX"+cfd+ALLTRIM(STR(nLXNum))+csegd TO cString  && Size
	DO cstringbreak
	nSegCtr = nSegCtr + 1
	nLXNum = nLXNum + 1
	cStatusDate = LEFT(TTOC(dTrig_time,1),8)
	cStatusTime = RIGHT(TTOC(dTrig_time,1),6)

	IF lTesting
		SET STEP ON
	ENDIF
	STORE "AT7"+cfd+cStatusCode+cfd+cReasonCode+cfd+cfd+cfd+cStatusDate+cfd+cStatusTime+csegd TO cString
	DO cstringbreak
	nSegCtr = nSegCtr + 1


	STORE "MS1"+cfd+IIF(wolog.TYPE = "A",cCityUsed,TRIM(wolog.puloc))+csegd TO cString
	DO cstringbreak
	nSegCtr = nSegCtr + 1

	cChassis = IIF(wolog.TYPE # "A",LEFT(ALLTRIM(wolog.CONTAINER),4)+cfd+SUBSTR(ALLTRIM(wolog.CONTAINER),5),ALLTRIM(wolog.awb))
	IF !EMPTY(cChassis)
		STORE "MS2"+cfd+cChassis+csegd TO cString
		DO cstringbreak
		nSegCtr = nSegCtr + 1
	ENDIF

*!* Finish processing
	close214()
	IF nFilenum>0
		=FCLOSE(nFilenum)
	ENDIF

	ASSERT .F. MESSAGE "At file closure"

	IF !lTesting
		DO ediupdate WITH "214 CREATED",.F.,.F.
	ENDIF


	COPY FILE [&cFilename] TO [&cFilenameArch]
	IF lDoJfilesout
		COPY FILE &cFilename TO &cFilenameOut
		DELETE FILE &cFilename
	ENDIF

	IF !lTesting
		IF !USED("ftpedilog")
			SELECT 0
			USE F:\edirouting\ftpedilog ALIAS ftpedilog
			INSERT INTO ftpedilog (TRANSFER,ftpdate,filename,acct_name,TYPE) VALUES ("214-"+cCustName,dt2,cFilename,UPPER(cCustName),"214")
			USE IN ftpedilog
		ENDIF
	ENDIF

	tsubject = "Jones 214 "+IIF(lTesting,"*TEST*","")+" EDI from TGF as of "+dtmail
	tmessage = "214 ("+cProcType+") EDI Info from TGF-CA, Truck WO# "+cWO_Num+CHR(10)
	tmessage = tmessage + "for "+cMailName+CHR(13)
	tmessage = tmessage +cStatusCode+" has been created."+CHR(10)+"(Filename: "+cFilenameShort+")"
*!*  +CHR(10)+CHR(10)
*!*  tmessage = tmessage + "If you have any questions, please eMail or call 732-750-9000 x217."
	IF lTesting
		tmessage = tmessage + CHR(10)+CHR(10)+"This is a TEST RUN..."
	ENDIF

	IF lEmail
		DO FORM dartmail2 WITH tsendto,tfrom,tsubject,tcc,tattach,tmessage,"A"
	ENDIF

	SELECT 0
	USE F:\edirouting\ftpjobs
*  Insert Into ftpjobs (jobname,Userid,jobtime) Values ("214-BIGLOTS","PAULG",Datetime())

	RELEASE ALL LIKE c_CntrlNum,c_GrpCntrlNum
	RELEASE ALL LIKE nOrigSeq,nOrigGrpSeq
	WAIT CLEAR
	WAIT WINDOW cMailName+" 214 "+cProcType+" EDI File output complete" AT 20,60 NOWAIT TIMEOUT 3

	closefiles()

CATCH TO oErr
	SET STEP ON
	IF lDoCatch
		lEmail = .T.
		IF EMPTY(cFin_Status)
			cFin_Status = "ERRHAND ERROR"
		ENDIF
		DO ediupdate WITH cFin_Status,.T.
		tsubject = cCustName+" Error ("+TRANSFORM(oErr.ERRORNO)+") at "+TTOC(DATETIME())
		tattach  = ""
		tsendto  = tsendtoerr
		tmessage = tccerr

		tmessage =tmessage+ "Try/CATCH Exeception Message:"+CHR(13)+;
			[  Error: ] + STR(oErr.ERRORNO) +CHR(13)+;
			[  LineNo: ] + STR(oErr.LINENO) +CHR(13)+;
			[  Message: ] + oErr.MESSAGE +CHR(13)+;
			[  Procedure: ] + oErr.PROCEDURE +CHR(13)+;
			[  Details: ] + oErr.DETAILS +CHR(13)+;
			[  StackLevel: ] + STR(oErr.STACKLEVEL) +CHR(13)+;
			[  LineContents: ] + oErr.LINECONTENTS+CHR(13)+;
			[  UserValue: ] + oErr.USERVALUE

		tmessage = tmessage+CHR(13)+cFin_Status

		tsubject = "214 EDI Poller Error at "+TTOC(DATETIME())
		tattach  = ""
		tcc=""
		tfrom    ="TGF EDI 214 Poller Operations <fmi-transload-ops@fmiint.com>"
		DO FORM dartmail2 WITH tsendto,tfrom,tsubject,tcc,tattach,tmessage,"A"
		lEmail = .F.
	ENDIF
FINALLY
	SET HOURS TO 12
	FCLOSE(nFilenum)
	closefiles()
ENDTRY

&& END OF MAIN CODE SECTION



****************************
PROCEDURE num_incr_isa
****************************

	IF !USED("serfile")
		USE ("f:\3pl\data\serial\"+cCustName+"214_serial") IN 0 ALIAS serfile
	ENDIF
	SELECT serfile
	IF lTesting
		nOrigSeq = serfile.seqnum
		nOrigGrpSeq = serfile.grpseqnum
		lISAFlag = .F.
	ENDIF
	nISA_Num = serfile.seqnum
	c_CntrlNum = ALLTRIM(STR(serfile.seqnum))
	REPLACE serfile.seqnum WITH serfile.seqnum + 1 IN serfile
	USE IN serfile
	SELECT wolog
ENDPROC

****************************
PROCEDURE num_incr_st
****************************

	IF !USED("serfile")
		USE ("f:\3pl\data\serial\"+cCustName+"214_serial") IN 0 ALIAS serfile
	ENDIF
	SELECT serfile
	IF lTesting
		nOrigSeq = serfile.seqnum
		nOrigGrpSeq = serfile.grpseqnum
		lSTFlag = .F.
	ENDIF
	c_GrpCntrlNum = ALLTRIM(STR(serfile.grpseqnum))
	REPLACE serfile.grpseqnum WITH serfile.grpseqnum + 1 IN serfile
	USE IN serfile
	SELECT wolog
ENDPROC

****************************
PROCEDURE segmentget
****************************

	PARAMETER thisarray,lcKey,nLength

	FOR i = 1 TO nLength
		IF i > nLength
			EXIT
		ENDIF
		lnEnd= AT("*",thisarray[i])
		IF lnEnd > 0
			lcThisKey =TRIM(SUBSTR(thisarray[i],1,lnEnd-1))
			IF OCCURS(lcKey,lcThisKey)>0
				RETURN SUBSTR(thisarray[i],lnEnd+1)
				i = 1
			ENDIF
		ENDIF
	ENDFOR

	RETURN ""

****************************
PROCEDURE close214
****************************
	STORE  "SE"+cfd+ALLTRIM(STR(nSegCtr+1))+cfd+PADL(c_GrpCntrlNum,9,"0")+csegd TO cString
	DO cstringbreak

	STORE  "GE"+cfd+ALLTRIM(STR(nSTCount))+cfd+c_CntrlNum+csegd TO cString
	DO cstringbreak

	STORE  "IEA"+cfd+"1"+cfd+PADL(c_CntrlNum,9,"0")+csegd TO cString
	DO cstringbreak

	RETURN

****************************
PROCEDURE cstringbreak
****************************
	cLen = LEN(ALLTRIM(cString))
	FPUTS(nFilenum,cString)
	RETURN



**************************
PROCEDURE ediupdate
**************************
	PARAMETERS cFin_Status, lIsError,lFClose
	IF !lTesting
		SELECT edi_trigger
		LOCATE
		IF lIsError
			REPLACE edi_trigger.processed WITH .T.,edi_trigger.created WITH .F.,;
				edi_trigger.fin_status WITH cFin_Status,edi_trigger.errorflag WITH .T.;
				edi_trigger.when_proc WITH DATETIME() ;
				FOR edi_trigger.wo_num = nWO_Num AND edi_trigger.edi_type = "214 "
			IF nFilenum>0
				=FCLOSE(nFilenum)
			ENDIF
			IF FILE(cFilename)
				DELETE FILE &cFilename
			ENDIF
			closefiles()
		ELSE
			REPLACE edi_trigger.processed WITH .T.,edi_trigger.created WITH .T.,proc214 WITH .T.;
				edi_trigger.when_proc WITH DATETIME(),;
				edi_trigger.fin_status WITH cStatusCode+" 214 CREATED";
				edi_trigger.errorflag WITH .F.,file214 WITH cFilename ;
				FOR edi_trigger.wo_num = nWO_Num AND edi_trigger.edi_type = "214 "
		ENDIF
	ENDIF
ENDPROC

*********************
PROCEDURE closefiles
*********************
	IF USED('ftpedilog')
		USE IN ftpedilog
	ENDIF
	IF USED('serfile')
		USE IN serfile
	ENDIF
	IF USED('wolog')
		USE IN wolog
	ENDIF
	IF USED('xref')
		USE IN xref
	ENDIF
	IF USED('ftpjobs')
		USE IN ftpjobs
	ENDIF
ENDPROC
