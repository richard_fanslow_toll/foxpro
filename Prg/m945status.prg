Parameters whichoffice,whichuser

Public fieldlist

numdays = 10

Try

	whichoffice = Alltrim(Transform(whichoffice))
	whichuser = Alltrim(Transform(whichuser))

*MessageBox("945 report:+ P1 = "+Transform(whichoffice)+" P2 = "+Transform(whichuser),16,"Debugger")

	Set Exclusive Off
	Set Safety Off
	Close Data All
	Set Deleted On
	Set EngineBehavior 70

	Wait Window At 10,10 "Parsing the FTP logs... Please wait......." Nowait

	Do parse_all_ftp_logs

	Use F:\whl\whdata\outship In 0 Alias outship_ca
	Use F:\whn\whdata\outship In 0 Alias outship_nj
	If !Used('edi_trigger')
		cEDIFolder = Iif(Date()<{^2010-01-24},"F:\JAG3PL\DATA\","F:\3PL\DATA\")
		Use (cEDIFolder+"edi_trigger") In 0 Alias edi_trigger
	Endif
	Use F:\ftplogs\Data\ftplog In 0

	Wait Window At 10,10 "Loading in the outship records from CA and FL..... Please wait......." Nowait

	Select wo_num As FMIWorkOrder,wo_date As workorderdate,"CA" As office,ship_ref As PONumber,bol_no As BOL,scac,Start,Cancel,del_date,appt,Space(32) As ShipmentFile,Space(20) As edi_status,;
		SPACE(15) As UA_Status,Space(20) As Xfertimetxt,{  /  /       :  :  } As trigtime,{  /  /       :  :  } As proctime, {  /  /       :  :  } As xfertime,Space(8) As hour_delay ;
		FROM outship_ca Where Inlist(accountid,5453,5452,5446,5449,5447,5448,5451,5741,5742,5743,5663) And (appt = Date() Or Emptynul(del_date) Or del_date >= Date()-numdays) Into Cursor bols Readwrite;
		GROUP By BOL

* from outship_ca where accountid = 5687 and (del_date > Date()-15 or Emptynul(del_date)) into cursor bols readwrite

	Select bols
	Goto Top
	Scan
		Delete For workorderdate < {^2009-06-30}
	Endscan

	Use In outship_ca

	Select wo_num As FMIWorkOrder,wo_date As workorderdate,"NJ" As office,ship_ref As PONumber,bol_no As BOL,scac,Start,Cancel,del_date,appt,Space(32) As ShipmentFile,Space(20) As edi_status,;
		SPACE(15) As UA_Status,Space(20) As Xfertimetxt,{  /  /       :  :  } As trigtime,{  /  /       :  :  } As proctime, {  /  /       :  :  } As xfertime,Space(8) As hour_delay ;
		FROM outship_nj Where Inlist(accountid,5453,5452,5446,5449,5447,5448,5451,5741,5742,5743,5663) And (appt= Date() Or Emptynul(del_date) Or del_date >= Date()-numdays) Into Cursor bols_nj Readwrite;
		GROUP By BOL

* from outship_fl where accountid = 5687 and (del_date > Date()-15 or Emptynul(del_date)) into cursor bols_fl readwrite
	Select bols
	Goto Top
	Scan
		Delete For workorderdate < {^2009-06-30}
	Endscan

	Use In outship_nj

	Select bols_nj
	Scan
		Select bols_nj
		Scatter Memvar
		Select bols
		Append Blank
		Gather Memvar
	Endscan

	Wait Window At 10,10 "Updating the EDI Trigger information..... Please wait......." Nowait
	Select bols
	Goto Top
	Scan
		If !Empty(bols.BOL)
			Select edi_trigger
			Set Order To BOL
			Seek bols.BOL
			If Found()
					Replace bols.ShipmentFile With Justfname(file945)
					Replace bols.proctime With when_proc
					Replace bols.trigtime With trig_time
					Replace bols.edi_status With fin_status
			Endif
		Endif
	Endscan

	Select bols
	Goto Top

	Scan
		Select ftplog
		Locate For Upper(Alltrim(bols.ShipmentFile))$Upper(ftplog.filename)
		If Found()
			Replace bols.UA_Status With "PickedUp" In bols
			Replace bols.Xfertimetxt With ftplog.xfertime In bols
		Else
			Replace bols.UA_Status With "NO" In bols
		Endif
	Endscan

	Select bols
	lcErrMessage = "No Undelivered Shipment files........."
	Select * From bols Where !Empty(ShipmentFile) And UA_Status = "NO" Into Cursor errs
	If Reccount("errs") >=1
		lcErrMessage ="Please check the following shipment files that were NOT picked up:"+Chr(13)+Chr(13)
		Select errs
		Goto Top
		Scan
			lcErrMessage = lcErrMessage + "   Shipment File: "+Alltrim(errs.ShipmentFile)+"    PO Number: "+errs.PONumber+Chr(13)
		Endscan
	Endif
	Select bols
	Goto Top
	Scan
		Replace xfertime With txttotime(Xfertimetxt)
		Replace hour_delay With Str((xfertime-trigtime)/3600,5,2)
	Endscan

	Export To c:\tempfox\UA_Shipment_Status.Xls Type Xls

	Select * From bols Group By BOL Order By del_date Descen Into Cursor temp Readwrite
	Select temp
	Delete For Empty(BOL)
	rptok = .F.
	Do reporttopdf With "moret945report_RevB","c:\tempfox\moret945Report",rptok

	Wait Window At 10,10 "Sending the Email..... Please wait......." Timeout 1

*!*	Do case
*!*	  Case  whichoffice = "M"
*!*	    tsendto ="jabbate@fmiint.com,bvaldes@fmiint.com,kzelaya@fmiint.con"
*!*	    tcc = "twinneberger@underarmour.com,gchristhilf@underarmour.com,pgaidis@fmiint.com"
*!*	  Case  InList(whichoffice,"5","C")
*!*	    tsendto ="ghecht@fmiint.com,mvega@fmiint.com,eruiz@fmiint.com"
*!*	    tcc = "twinneberger@underarmour.com,gchristhilf@underarmour.com,pgaidis@fmiint.com"
*!*	  otherwise
*!*	    tsendto ="pgaidis@fmiint.com"
*!*	EndCase

*!*	Do case
*!*	  Case  Upper(whichuser) = "JUAN"
*!*	    tsendto ="juan@fmiint.com"
*!*	    tcc = ""
*!*	  Case  Upper(whichuser) = "JOEB"
*!*	    tsendto ="joe.bianchi@tollgroup.com"
*!*	    tcc = ""
*!*	  Case  Upper(whichuser) = "JIMK"
*!*	    tsendto ="jkillen@fmiint.com"
*!*	    tcc = ""
*!*	  Case  Upper(whichuser) = "PAULG"
*!*	    tsendto ="pgaidis@fmiint.com"
*!*	    tcc = ""
*!*	EndCase

*!*	* email for those without the attachment
*!*	tsubject = "Updated Under Armour Shipment Status"
*!*	tmessage = "See attached report file" + CHR(13) + CHR(13) + " For content or distribution changes contact "+Chr(13)+" Paul Gaidis 732-750-9000 x143"+Chr(13)+Chr(13)
*!*	tmessage = tmessage +lcErrMessage+Chr(13)+" Triggered by: "+Upper(whichuser)+" Warehouse: "+whichoffice
*!*	tattach  = "c:\tempfox\UA945Report.pdf"
*!*	tfrom    ="FMI EDI Processing Center <fmi-transload-ops@fmiint.com>"
*!*	DO FORM m:\dev\FRM\dartmail2 WITH tsendto,tfrom,tsubject,tcc,tattach,tmessage,"A"

*!*	* email for those with the attachment
*!*	tsubject = "Updated Under Armour Shipment Status"
*!*	tmessage = "See attached report file" + CHR(13) + CHR(13) + " For content or distribution changes contact "+Chr(13)+" Paul Gaidis 732-750-9000 x143"+Chr(13)+Chr(13)
*!*	tmessage = tmessage +lcErrMessage+Chr(13)+"triggered by: "+Upper(whichuser)+" Warehouse: "+whichoffice

*!*	tattach  = "c:\tempfox\UA_Shipment_Status.xls"
*!*	tfrom    ="FMI EDI Processing Center <fmi-transload-ops@fmiint.com>"
*!*	DO FORM m:\dev\FRM\dartmail2 WITH tsendto,tfrom,tsubject,tcc,tattach,tmessage,"A"

	Use In edi_trigger
	Use In ftplog

Catch To oErr
	ptError = .T.
	tsubject = "Report Program Error ("+Transform(oErr.ErrorNo)+") at "+Ttoc(Datetime())
	tattach  = ""
	tsendto  = "pgaidis@fmiint.com"
	tmessage = "Report Program Error processing "

	tmessage =tmessage+ "Try/CATCH Exeception Message:"+Chr(13)+;
		[  Error: ] + Str(oErr.ErrorNo) +Chr(13)+;
		[  LineNo: ] + Str(oErr.Lineno) +Chr(13)+;
		[  Message: ] + oErr.Message +Chr(13)+;
		[  Procedure: ] + oErr.Procedure +Chr(13)+;
		[  Details: ] + oErr.Details +Chr(13)+;
		[  StackLevel: ] + Str(oErr.StackLevel) +Chr(13)+;
		[  LineContents: ] + oErr.LineContents+Chr(13)+;
		[  UserValue: ] + oErr.UserValue+Chr(13)


	tsubject = "Report Processing Error at "+Ttoc(Datetime())
	tattach  = ""
	tcc=""
	tsendto  = "pgaidis@fmiint.com"
	tFrom    ="TGF WMS Report Engine <fmi-transload-ops@fmiint.com>"
	Do Form dartmail2 With tsendto,tFrom,tsubject,tcc,tattach,tmessage,"A"
Endtry
