********************************************************************************************
** this has been superseded by synclaire_returns_rpt.prg (project of same name) 12/8/2017 MB
********************************************************************************************

RETURN

Parameters whichaccount

*whichaccount = 6521
lcAcct = Transform(whichaccount)
*set step On 


close data all 
lcStr = "accountid = "+lcAcct+" and wo_date >= {11/01/2017}"
useca("inwolog","wh",,,lcStr)

lcStr = "accountid = "+lcAcct
useca("indet","wh",,,lcStr)

xsqlexec("select * from zzindet where accountid = "+lcAcct+" and date_rcvd >= {11/01/2017}","xindet",,"wh")


Create Cursor indat (;
  wo_num   int,;
  container char(10),;
  wo_date date,;
  brokerref char(20),;
  reference char(20),;
  confirmdt date,;
  PO      char(15),;
  POnum   char(15),;
  UPC     char(12),;
  name    char(40),;
  ctnqty  n(10),;
  pack    char(10),;
  totqty  n(10),;
  totwt   n(10.2),;
  totcube n(10.2) )

Select wo_num,wo_date,container,acct_ref,confirmdt From inwolog Where acct_ref ="RETURN" And wo_date >= Date()-365  And !isnull(confirmdt) Into Cursor wolist readwrite
Select wolist
Delete For Empty(container)

Select indat
Zap 

Select wolist
Scan

 Select inwolog.wo_num ,wo_date,container,brokerref,reference,confirmdt,indet.po, indet.style as upc, indet.pack, indet.totqty, Space(20) as name FROM inwolog LEFT OUTER JOIN indet ON indet.inwologid = inwolog.inwologid;
 WHERE inwolog.accountid = &lcAcct And inwolog.wo_num = wolist.wo_num and units And acct_ref="RETURN" and confirmdt >= Date()-365 order by confirmdt Into Cursor temp
 Select temp
 If Reccount("temp")=0
 Select inwolog.wo_num ,wo_date,container,brokerref,reference,confirmdt,xindet.po, xindet.style as upc, xindet.pack, xindet.totqty, Space(20) as name FROM inwolog LEFT OUTER JOIN xindet ON xindet.inwologid = inwolog.inwologid;
 WHERE inwolog.accountid = &lcAcct And inwolog.wo_num = wolist.wo_num and units And acct_ref="RETURN" and confirmdt >= Date()-365 order by confirmdt Into Cursor temp

 Endif 

 scan
   Select temp
   Scatter Memvar 
   Select indat
   Append Blank
   Gather memvar 
*   Insert Into indat From memvar
 Endscan 

 Select inwolog.wo_num ,wo_date,container,brokerref,reference,confirmdt,indet.po, indet.cayset as ponum,indet.totcube,indet.totwt,indet.style as upc, indet.pack, indet.totqty,;
  Space(20) as name FROM inwolog LEFT OUTER JOIN indet ON indet.inwologid = inwolog.inwologid;
 WHERE inwolog.accountid = &lcAcct And inwolog.wo_num = wolist.wo_num and !units And acct_ref="RETURN" and confirmdt >= Date()-365 order by confirmdt Into Cursor temp
 Select temp

 If Reccount("temp") =0
   Select inwolog.wo_num ,wo_date,container,brokerref,reference,confirmdt,xindet.po, xindet.cayset as ponum,xindet.totcube,xindet.totwt,xindet.style as upc, xindet.pack,;
   xindet.totqty, Space(20) as name FROM inwolog LEFT OUTER JOIN xindet ON xindet.inwologid = inwolog.inwologid;
   WHERE inwolog.accountid = &lcAcct And inwolog.wo_num = wolist.wo_num and !units And acct_ref="RETURN" and confirmdt >= Date()-365 order by confirmdt Into Cursor temp
 Endif 

 scan
   Select indat
   Locate For wo_num = temp.wo_num And po = temp.po
   If Found()
     replace ctnqty  With temp.totqty
     replace pack    With temp.pack
     replace totcube With temp.totcube
     replace totwt   With temp.totwt
     replace ponum   With temp.ponum
   endif 
 Endscan 
endscan


Select indat
Scan
  If whichaccount = 6521
    upcmastsql(6521,indat.upc)
  Else
    upcmastsql(6221,indat.upc)
  endif
  replace indat.name With upcmast.descrip in indat
Endscan

If whichaccount = 6521
  Export To f:\ftpusers\synclaire\inboundReport\synclaire_return_rpt.xls Type Xls Fields wo_num,container,brokerref,reference,confirmdt,ponum,upc,name,ctnqty,pack,totqty,totwt,totcube
Else
  Export To f:\ftpusers\BCNY\InboundReport\bcny_return_rpt.xls Type Xls Fields wo_num,container,brokerref,reference,confirmdt,ponum,upc,name,ctnqty,pack,totqty,totwt,totcube
endif    
return
************************************************************************************************************************************
