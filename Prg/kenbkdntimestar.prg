* create headcount by div/dept/jobclass report for Ken

* Build EXE as F:\UTIL\TIMESTAR\kenbkdn.exe

* this version uses employee info data extracted from Timestar instead of ADP data.

runack("KENBKDN")

utilsetup("KENBKDN")

LOCAL loKenBkdnReport
loKenBkdnReport = CREATEOBJECT('KenBkdnReport')
loKenBkdnReport.MAIN()

=SCHEDUPDATE()

CLOSE DATABASES ALL
RETURN

#DEFINE LOGIT 1
#DEFINE WAITIT 2
#DEFINE NOWAITIT 4
#DEFINE SENDIT 8
#DEFINE CRLF CHR(13) + CHR(10)
#DEFINE RETURN_DATA_MANDATORY .F.
#DEFINE RETURN_DATA_NOT_MANDATORY .T.
#DEFINE CSVFORMAT 6
#DEFINE WRITE_ONLY_UNBUFFERED 11
#DEFINE COMMA ","

DEFINE CLASS KenBkdnReport AS CUSTOM

	cProcessName = 'KenBkdnReport'

	lTestMode = .F.  && .F. FOR NORMAL OPERATION, .T. TO SUPPRESS ALL BUT DEVELOPER EMAILS AND CHANGE INPUT TABLE, ETC.

	dToday = DATE()

	lAutoYield = .T.

	cStartTime = TTOC(DATETIME())

	* report properties
*!*		*lShowSalaries = .F.
*!*		*lShowSalaries = .T.

	* connection properties
	nSQLHandle = 0

	* wait window properties
	nWaitWindowTimeout = 2
	lWaitWindowIsOn = .T.

	* logfile properties
	lLoggingIsOn = .T.
	cLogFile = 'F:\UTIL\TIMESTAR\LOGFILES\KENBKDN_REPORT_log.txt'

	* INTERNAL email properties
	lSendInternalEmailIsOn = .T.
	cFrom = 'mark.bennett@tollgroup.com'
	*cSendTo = 'Ken.Kausner@tollgroup.com'
	cSendTo = 'Todd.Wilen@tollgroup.com'
	cCC = 'mbennett@fmiint.com'
	cSubject = 'Employee Breakdown Report for ' + DTOC(DATE())
	cAttach = ''
	cBodyText = ''

	cCOMPUTERNAME = ''
	cUSERNAME = ''


	FUNCTION INIT
		IF NOT DODEFAULT()
			RETURN .F.
		ENDIF
		WITH THIS
			*SET RESOURCE OFF
			CLOSE DATA
			SET CENTURY ON
			SET DATE AMERICAN
			SET HOURS TO 24
			SET ANSI ON
			SET TALK OFF
			SET DELETED ON
			SET CONSOLE OFF
			SET EXCLUSIVE OFF
			SET SAFETY OFF
			SET EXACT OFF
			SET STATUS BAR ON
			SET SYSMENU OFF
			SET ENGINEBEHAVIOR 70
			_VFP.AUTOYIELD = .lAutoYield
			.cCOMPUTERNAME = GETENV("COMPUTERNAME")
			.cUSERNAME = GETENV("USERNAME")
			IF .lTestMode THEN
				.cLogFile = 'F:\UTIL\TIMESTAR\LOGFILES\KENBKDN_REPORT_log_TESTMODE.txt'
				.cSendTo = 'mbennett@fmiint.com'
				.cCC = ''
			ENDIF
			.lLoggingIsOn = .lLoggingIsOn AND NOT EMPTY(.cLogFile)
			IF .lLoggingIsOn THEN
				SET ALTERNATE TO (.cLogFile) ADDITIVE
				SET ALTERNATE ON
			ENDIF
		ENDWITH
	ENDFUNC


	FUNCTION DESTROY
		WITH THIS
			IF .lLoggingIsOn  THEN
				SET ALTERNATE OFF
				SET ALTERNATE TO
			ENDIF
		ENDWITH
		DODEFAULT()
	ENDFUNC


	FUNCTION MAIN
		WITH THIS
			LOCAL lnNumberOfErrors, lcSQL, lcSQL2, lcSQL3, loError, ldToday
			LOCAL lcFiletoSaveAs, lcSpreadsheetTemplate, lcGrandTotalRow, lcWhere
			LOCAL oExcel, oWorkbook, oWorksheet
			TRY
				lnNumberOfErrors = 0

				.TrackProgress("Employee Breakdown REPORT process started....", LOGIT+SENDIT+NOWAITIT)
				.TrackProgress('COMPUTERNAME = ' + .cCOMPUTERNAME, LOGIT+SENDIT)
				.TrackProgress('USERNAME = ' + .cUSERNAME, LOGIT+SENDIT)
				.TrackProgress('PROJECT = KENBKDN', LOGIT+SENDIT)
				IF .lTestMode THEN
					.TrackProgress('                        TEST MODE                        ', LOGIT+SENDIT)
				ENDIF

				ldToday = .dToday

				DO CASE
					CASE .lTestMode
						.cSendTo = 'mbennett@fmiint.com'
*!*						CASE .lShowSalaries
*!*							.cSendTo = 'lucille.waldrip@Tollgroup.com, marie.freiberger@Tollgroup.com'
					OTHERWISE
						*.cSendTo = 'Ken.Kausner@Tollgroup.com'
						.cSendTo = 'Todd.Wilen@Tollgroup.com'
				ENDCASE

				USE F:\UTIL\TIMESTAR\DATA\EEINFO.DBF AGAIN IN 0 ALIAS EEINFO


					lcWhere = ""
					*lcWhere = " AND (A.CUSTAREA3 = 'NJ1' OR A.CUSTAREA3 = 'NJ2' OR A.CUSTAREA3 = 'NJ3' OR A.CUSTAREA3 = 'NJ4')"
					*lcWhere = " AND (A.CUSTAREA3 = 'SP1' OR A.CUSTAREA3 = 'SP2')"
					*lcWhere = " AND {fn LEFT(A.HOMEDEPARTMENT,2)} IN ('07')"
					SELECT ;
					DIVISION, ;
					DEPT, ;
					DEPTDESC, ;
					NAME, ;
					INSPID AS FILE_NUM, ;
					RATETYPE, ;
					WRKDSTATE ;
					FROM EEINFO ;
					INTO CURSOR SQLCURSOR1 ;
					WHERE STATUS <> 'T' ;
					&lcWhere ;
					ORDER BY 1, 2, 4


					*lcSpreadsheetTemplate = "F:\UTIL\ADPREPORTS\KENBKDN_TEMPLATE.XLS"
					lcSpreadsheetTemplate = "F:\UTIL\ADPREPORTS\KENBKDN_TEMPLATE2.XLS"
					lcFiletoSaveAs = "F:\UTIL\ADPREPORTS\REPORTS\Employee Breakdown Report for " + STRTRAN(DTOC(ldToday),"/","-") + ".XLS"


						**************************************************************************************************************
						**************************************************************************************************************

						IF FILE(lcFiletoSaveAs) THEN
							DELETE FILE (lcFiletoSaveAs)
						ENDIF


						***********************************************************************************
						***********************************************************************************
						** output to Excel spreadsheet
						.TrackProgress('Creating spreadsheet: ' + lcFiletoSaveAs, LOGIT+SENDIT+NOWAITIT)

						oExcel = CREATEOBJECT("excel.application")
						oExcel.VISIBLE = .F.
						oWorkbook = oExcel.workbooks.OPEN(lcSpreadsheetTemplate)
						oWorkbook.SAVEAS(lcFiletoSaveAs)

						oWorksheet = oWorkbook.Worksheets[1]
						oWorksheet.RANGE("A5","AT2000").ClearContents()
						oWorksheet.RANGE("A1").VALUE = "TGF Staffing Report for " + TRANSFORM(ldToday)

						LOCAL lnStartRow, lnRow, lnEndRow, lnRowsProcessed, lcStartRow, lcEndRow, lcPrevDiv, lcPrevDept
						LOCAL lnDivCount, lnDeptCount

						lnStartRow = 5
						lcStartRow = ALLTRIM(STR(lnStartRow))

						*!*							lnEndRow = lnStartRow - 1
						lnRow = lnStartRow - 1

						STORE 0 TO lnDivCount, lnDeptCount

						SELECT SQLCURSOR1
						LOCATE

						* init break check variables
						lcPrevDiv = SQLCURSOR1.DIVISION
						lcPrevDept = SQLCURSOR1.DEPT

						SCAN

							IF (SQLCURSOR1.DIVISION <> lcPrevDiv) OR (SQLCURSOR1.DEPT <> lcPrevDept) THEN
								* break in div or dept -- insert subtotals line
								lnRow = lnRow + 1
								lnEndRow = lnRow
								lcRow = ALLTRIM(STR(lnRow))
							ENDIF
							IF (SQLCURSOR1.DEPT <> lcPrevDept) THEN
								*  insert DEPT subtotals line
								oWorksheet.RANGE("I"+lcRow).VALUE = lnDeptCount
								* reset counter
								lnDeptCount = 0
							ENDIF
							IF (SQLCURSOR1.DIVISION <> lcPrevDiv) THEN
								*  insert DIVISION subtotals line
								oWorksheet.RANGE("J"+lcRow).VALUE = lnDivCount
								* reset counter
								lnDivCount = 0
							ENDIF

							lnRow = lnRow + 1
							*!*								lnEndRow = lnRow
							lcRow = ALLTRIM(STR(lnRow))

							* INCREMENT SUBTOTALS COUNTERS
							lnDivCount = lnDivCount + 1
							lnDeptCount = lnDeptCount + 1

							oWorksheet.RANGE("A"+lcRow).VALUE = "'" + SQLCURSOR1.DIVISION
							oWorksheet.RANGE("B"+lcRow).VALUE = "'" + SQLCURSOR1.DEPT
							oWorksheet.RANGE("C"+lcRow).VALUE = "'" + SQLCURSOR1.DEPTDESC
							oWorksheet.RANGE("D"+lcRow).VALUE = "'" + SQLCURSOR1.NAME
							oWorksheet.RANGE("E"+lcRow).VALUE = "'" + SQLCURSOR1.RATETYPE
							oWorksheet.RANGE("F"+lcRow).VALUE = "'" + SQLCURSOR1.WRKDSTATE


							* 'remember' prev div/depts
							lcPrevDiv = SQLCURSOR1.DIVISION
							lcPrevDept = SQLCURSOR1.DEPT

						ENDSCAN

						*!*							* populate header counts, sums, etc.
						*!*							lnRowsProcessed = lnEndRow - lnStartRow + 1
						*!*							lcEndRow = ALLTRIM(STR(lnEndRow))

						* final subtotals line
						lnRow = lnRow + 1
						*!*							lnEndRow = lnRow
						lcRow = ALLTRIM(STR(lnRow))

						*  insert DEPT subtotals line
						oWorksheet.RANGE("I"+lcRow).VALUE = lnDeptCount
						*  insert DIVISION subtotals line
						oWorksheet.RANGE("J"+lcRow).VALUE = lnDivCount

						lcGrandTotalRow = ALLTRIM(STR(lnRow + 2))

						*!*	Grand Total for DivCount column
						oWorksheet.RANGE("D" + lcGrandTotalRow).VALUE = "Grand Total Headcount:"
						oWorksheet.RANGE("J" + lcGrandTotalRow).VALUE = "=SUM(J5:J" + lcRow + ")"
						oWorksheet.RANGE("D" + lcGrandTotalRow,"J" + lcGrandTotalRow).FONT.bold = .T.

						* SAVE AND QUIT EXCEL
						oWorkbook.SAVE()
						oExcel.QUIT()

						***********************************************************************************
						***********************************************************************************


						IF FILE(lcFiletoSaveAs) THEN
							* attach output file to email
							.cAttach = lcFiletoSaveAs
							.cBodyText = "See attached Employee Breakdown report." + ;
								CRLF + CRLF + "(do not reply - this is an automated report)" + ;
								CRLF + CRLF + "<report log follows>" + ;
								CRLF + .cBodyText
						ELSE
							.TrackProgress('ERROR: unable to email spreadsheet: ' + lcFiletoSaveAs, LOGIT+SENDIT+NOWAITIT)
						ENDIF


					.TrackProgress("Employee Breakdown REPORT process ended normally.", LOGIT+SENDIT+NOWAITIT)

			CATCH TO loError

				.TrackProgress('There was an error.',LOGIT+SENDIT)
				.TrackProgress('The Error # is: ' + TRANSFORM(loError.ERRORNO), LOGIT+SENDIT)
				.TrackProgress('The Error Message is: ' + TRANSFORM(loError.MESSAGE), LOGIT+SENDIT)
				.TrackProgress('The Error occurred in Program: ' + TRANSFORM(loError.PROCEDURE), LOGIT+SENDIT)
				.TrackProgress('The Error occurred at line #: ' + TRANSFORM(loError.LINENO), LOGIT+SENDIT)
				lnNumberOfErrors = lnNumberOfErrors + 1
				IF TYPE('oWorkbook') = "O" THEN
					oWorkbook.SAVE()
				ENDIF
				IF TYPE('oExcel') = "O" THEN
					oExcel.QUIT()
				ENDIF
				*CLOSE DATABASES ALL

			ENDTRY

			CLOSE DATA
			WAIT CLEAR
			***************** INTERNAL email results ******************************
			.TrackProgress('About to send status email.',LOGIT)
			.TrackProgress('The logfile is: ' + .cLogFile, SENDIT)
			.TrackProgress("Employee Breakdown Report process started: " + .cStartTime, LOGIT+SENDIT)
			.TrackProgress("Employee Breakdown Report process finished: " + TTOC(DATETIME()), LOGIT+SENDIT)

			IF .lSendInternalEmailIsOn THEN
				DO FORM dartmail2 WITH .cSendTo,.cFrom,.cSubject,.cCC,.cAttach,.cBodyText,"A"
				.TrackProgress('Sent status email.',LOGIT)
			ELSE
				.TrackProgress('Did not send status email: THIS.lSendInternalEmailIsOn = FALSE.',LOGIT)
			ENDIF

		ENDWITH
		RETURN
	ENDFUNC && main


*!*		FUNCTION ExecSQL
*!*			LPARAMETERS tcSQL, tcCursorName, tlNoDataReturnedIsOkay
*!*			LOCAL llRetval, lnResult
*!*			WITH THIS
*!*				* close target cursor if it's open
*!*				IF USED(tcCursorName)
*!*					USE IN (tcCursorName)
*!*				ENDIF
*!*				WAIT WINDOW "Fetching remote data; please wait..." NOWAIT
*!*				lnResult = SQLEXEC(.nSQLHandle, tcSQL, tcCursorName)
*!*				llRetval = ( lnResult > 0 )
*!*				IF llRetval THEN
*!*					* see if any data came back
*!*					IF NOT tlNoDataReturnedIsOkay THEN
*!*						IF (NOT USED(tcCursorName)) OR EOF(tcCursorName) THEN
*!*							llRetval = .F.
*!*							.TrackProgress('No data was returned by this query: ' +CRLF + tcSQL, LOGIT+SENDIT)
*!*							.cSendTo = 'mbennett@fmiint.com'
*!*						ENDIF
*!*					ENDIF
*!*				ELSE
*!*					.TrackProgress("Result Code [" + TRANSFORM(lnResult) + "] returned by this query: " + tcSQL, LOGIT+SENDIT)
*!*					.cSendTo = 'Mark Bennett <mbennett@fmiint.com>'
*!*				ENDIF
*!*				WAIT CLEAR
*!*				RETURN llRetval
*!*			ENDWITH
*!*		ENDFUNC


	PROCEDURE TrackProgress
		* do any combination of Wait Window, writing to logfile, and adding to body of email,
		* based on nFlags parameter.
		LPARAMETERS tcExpression, tnFlags
		WITH THIS
			IF BITAND(tnFlags,LOGIT) = LOGIT THEN
				IF .lLoggingIsOn THEN
					?
					? .cProcessName + "  " + TTOC(DATETIME()) + ": " + tcExpression
				ENDIF
			ENDIF
			IF .lWaitWindowIsOn THEN
				IF BITAND(tnFlags,NOWAITIT) = NOWAITIT THEN
					WAIT WINDOW tcExpression NOWAIT
				ENDIF
				IF BITAND(tnFlags,WAITIT) = WAITIT THEN
					WAIT WINDOW tcExpression TIMEOUT .nWaitWindowTimeout
				ENDIF
			ENDIF
			IF BITAND(tnFlags,SENDIT) = SENDIT THEN
				IF .lSendInternalEmailIsOn THEN
					.cBodyText = .cBodyText + tcExpression + CRLF + CRLF
				ENDIF
			ENDIF
		ENDWITH
	ENDPROC  &&  TrackProgress

ENDDEFINE

