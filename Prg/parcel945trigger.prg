*!* Triggers 945 parcel shipments for all accounts

utilsetup("PARCEL945TRIGGER")
CLEAR
*ASSERT .F. MESSAGE "At start of Parcel Triggering"

TRY
	PUBLIC nAccountid,cBOL,nAccountName,cLocName,cOffice,nDaysback,cUserMessage,cShip_refBOL,lMoret,cAcctGrp,lAriat
	PUBLIC cPTString,cPTMail,cMissSCACs,cSCACString,lSendmail,tFrom,tCC,cGroup,cPTSWC,dDate,tSendto,lAmazon,guserid,cMod,cOffice,gOffice,gMasterOffice
	lTesting = .F.
	lTestInput = .F.
	lTestmail = lTesting
	lTestLoad = lTesting
	lSendmail = !lTesting
	lOverridebusy = lTesting
	lKeepScreen = lTesting
	guserid = "JOEB"
	lDoFinish = .T.  && Default = .t., if .f., will not allow the post-process reports and early trigger process to run
	NormalExit = .F.
	lAmazon = .F.

*!*	lOverridebusy = .T.
*!*	lKeepScreen = .T.
*!*	lDoFinish = .f.


	IF !lTesting
		SELECT 0
		USE F:\edirouting\FTPSETUP SHARED
		LOCATE FOR FTPSETUP.TRANSFER = "TRIG-PARCEL945"
		IF FTPSETUP.chkbusy = .T. AND !lOverridebusy
			WAIT WINDOW 'TRIG Busy...try again later' TIMEOUT 1
			NormalExit = .T.
			CLOSE DATABASES ALL
			THROW
		ENDIF
		REPLACE chkbusy WITH .T.,trig_time WITH DATETIME() FOR FTPSETUP.TRANSFER = "TRIG-PARCEL945"

		IF !lOverridebusy
			LOCATE FOR FTPSETUP.TRANSFER = "TRIG-PARCEL945"
			IF FTPSETUP.folderchk = .F. AND !lOverridebusy
				WAIT WINDOW 'TRIG Stopped-No Folderchk flag' TIMEOUT 1
				CLOSE DATABASES ALL
				THROW
			ENDIF
		ENDIF
		USE IN FTPSETUP
	ENDIF

	DO m:\dev\prg\_setvars WITH .T.
	DO m:\dev\prg\lookups


*!* Added preprocessing list
	replaceltn()  && Added this to ensure that current Synclaire long trk numbers are populated
	SET STEP ON 
	create_ship_pkg()  && Added this to populate Shipment and Package tables with manual Amazon-field tracking numbers, 04.23.2018, Joe
*	parcel945trigger_new()  && Handles EMSY-Ontrac tracking number updating until it's used as the main Parcel Trigger prg.  Added line 01.26.2018, Joe
*!* End list

	CLEAR

	_SCREEN.WINDOWSTATE=IIF(lTesting OR lOverridebusy OR lKeepScreen,2,1)
	_SCREEN.CAPTION = "EDI 945 PARCEL Trigger-All Accounts"

	CLOSE DATABASES ALL

	SELECT 0
	USE F:\3pl\DATA\mailmaster ALIAS mm
	LOCATE
	LOCATE FOR mm.edi_type = 'MISC' AND mm.taskname = "GENERAL"
	tSendto = IIF(mm.use_alt,mm.sendtoalt,mm.sendto)
	tCC = IIF(mm.use_alt,mm.ccalt,mm.cc)
	USE IN mm

	NormalExit = .F.

	tFrom ="Toll EDI  Operations <toll-edi-ops@tollgroup.com>"
	cUserMessage = ""
	cShip_refBOL = ""

	IF USED('edi_trigger')
		USE IN edi_trigger
	ENDIF

	cEDIFolder = IIF(lTesting,"C:\TEMPFOX\","F:\3PL\DATA\")
	IF lTesting
		WAIT WINDOW "This will be run into the TEST edi_trigger table" TIMEOUT 3
	ENDIF
	USE (cEDIFolder+"edi_trigger") IN 0 ALIAS edi_trigger SHARED

	USE F:\3pl\DATA\trigger945accts IN 0 ALIAS triggers2 ORDER TAG whseacct NOUPDATE
	SELECT 0
	SELECT acctgrp,IIF(triggers2.GROUP = "MORET",5742,accountid) AS accountid,office,whse,GROUP,NAME,ACTIVE FROM triggers2 WHERE ACTIVE = .T. GROUP BY acctgrp,office INTO CURSOR triggers
	IF lKeepScreen
*	BROWSE
	ENDIF

	USE F:\3pl\DATA\parcel_carriers IN 0 ALIAS parcels ORDER TAG scac NOUPDATE

	nDaysback = IIF(INLIST(DOW(DATE()),1,7),10,8)
	nDaysback = 10
	dDate = DATE()-nDaysback

	cMissBOLs = ""
	cEmptySCAC = ""
	cPTSWC = ""
	cBOLREPL = ""

*ASSERT .F. MESSAGE "At start of accounts loop"
	SELECT triggers
	LOCATE
	cWhse = "XYZ"

****************************************************************************************
* explain here what is supposed to be accomplished
* and what outship fields may get updated
*
*
****************************************************************************************

	SCAN FOR triggers.ACTIVE = .T.

*		    Scan For triggers.Active And triggers.office = "N"
*			SCAN FOR triggers.ACTIVE AND INLIST(triggers.accountid,&gmjacctlist) AND office = "L"
*			SCAN FOR triggers.ACTIVE AND triggers.GROUP = "MORET" and office = "L"*
*			Scan For triggers.Active And triggers.accountid = 6561
*			SCAN FOR triggers.ACTIVE AND LOWER(triggers.acctgrp)='gsteelseriesaccts'
*			SCAN FOR triggers.ACTIVE AND INLIST(triggers.accountid,&gs3accounts)

		IF lKeepScreen
			? triggers.acctgrp,triggers.office,triggers.GROUP
		ENDIF

		IF triggers.GROUP = "ARIAT"  && Added to correct SCAC code from FXSP -> FSP
		xsqlexec("update outship set scac = 'FSP' where accountid = 6532 and del_date > {"+DTOC(DATE()-3)+"} and scac = 'FXSP'",,,"wh")
		endif
		
		IF cWhse # ALLTRIM(triggers.whse)
			lNewWhse = .T.
			cWhse = ALLTRIM(triggers.whse)
		ELSE
			lNewWhse = .F.
		ENDIF

		lMoret = .F.
		cPTMail = ""
		cPTString = ""
		cOffice = ALLT(triggers.office)
		gMasterOffice = cOffice
		cMod = UPPER(ALLTRIM(triggers.whse))
		goffice = cMod
		nAccountid = triggers.accountid
		lAriat = IIF(nAccountid=6532,.T.,.F.)

		cAccountName = ALLT(triggers.NAME)
		tCC = ""  && ALLT(triggers.whsemail)
		cGroup = ALLT(triggers.GROUP)
		lMoret = IIF(cGroup="MORET",.T.,.F.)
		lPerry = .F.
		SELECT parcels
*		SET FILTER TO parcels.perry = lPerry
		SELECT triggers
		cLocName = ICASE(INLIST(cOffice,"C","R"),"San Pedro",INLIST(cOffice,"N","I","J"),"New Jersey",cOffice = "L","Mira Loma",cOffice = "Y","Carson2",cOffice = "K","Louisville",cOffice = "X","Carson","Florida")

		nWO_Num = 0
		cBOL = ""
		IF lNewWhse
			xReturn = "XXX"

			IF lOverridebusy && OR lKeepScreen
				WAIT WINDOW "Running Group "+cGroup TIMEOUT 5
			ELSE
				WAIT WINDOW "Running Group "+cGroup NOWAIT
			ENDIF

		ENDIF

		IF USED('SHIPMENT')
			USE IN shipment
		ENDIF

		IF USED('PACKAGE')
			USE IN package
		ENDIF

		lookups()

		cAcctGrp = ""
		cAcctGrp = ALLTRIM(triggers.acctgrp)

		IF cAcctGrp = 'gmjacctlist'
			csq1 = [select * from shipment where mod = ']+cMod+[' and (right(rtrim(str(accountid)),4) in (]+&cAcctGrp+[) or accountid = 9999) and attention <> 'RETURN' and shipdate >= {]+DTOC(dDate)+[}]
		ELSE
			csq1 = [select * from shipment where mod = ']+cMod+[' and (right(rtrim(str(accountid)),4) in (]+&cAcctGrp+[) or accountid = 9999) and shipdate >= {]+DTOC(dDate)+[}]
		ENDIF
		xsqlexec(csq1,,,"wh")

		INDEX ON shipmentid TAG shipmentid
		INDEX ON pickticket TAG pickticket

*!* Added the following code to select only the package records attached to the selected shipment records
		SELECT shipment
		LOCATE
		IF lKeepScreen
*			BROWSE TIMEOUT 30
		ENDIF

		IF RECCOUNT("shipment") > 0
			xjfilter="shipmentid in ("
			SCAN
				nShipmentId = shipment.shipmentid
				xjfilter=xjfilter+TRANSFORM(shipment.shipmentid)+","
			ENDSCAN
			xjfilter=LEFT(xjfilter,LEN(xjfilter)-1)+")"

			xsqlexec("select * from package where "+xjfilter,,,"wh")
		ELSE
			xsqlexec("select * from package where .f.",,,"wh")
		ENDIF
		INDEX ON shipmentid TAG shipmentid
		SET ORDER TO TAG shipmentid

		IF lKeepScreen
*			BROWSE
		ENDIF

*		cOfficeSWC = IIF(INLIST(cOffice,"N","I"),"I",goffice)
		cOfficeSWC = cOffice

		csq1 = [select * from swcdet where office = ']+cOfficeSWC+[' and accountid = ]+TRANSFORM(nAccountid)
		xsqlexec(csq1,,,"wh")

		IF USED('outship')
			USE IN outship
		ENDIF

*			useca("outship","wh")
		xsqlexec("select * from outship where mod = '"+cMod+"' and accountid in ("+&cAcctGrp+") and empty(bol_no) and del_date >= {"+DTOC(dDate)+"}",,,"wh")
		SELECT outship
		INDEX ON ship_ref TAG ship_ref
		INDEX ON outshipid TAG outshipid
		INDEX ON wo_num TAG wo_num
		IF lKeepScreen
*				BROWSE
		ENDIF

		SELECT outship

		IF cGroup = "MORET"
			SCAN FOR !EMPTYnul(outship.del_date) AND BETWEEN(outship.del_date,dDate,DATE()) AND EMPTY(outship.scac)
				DO CASE
					CASE ship_via = "DHL"
						REPLACE outship.scac WITH "DHLC" IN outship NEXT 1
					CASE ship_via = "UPS" OR ship_via = "U.P.S" OR ship_via = "UNITED PARCEL"
						REPLACE outship.scac WITH "UPSN" IN outship NEXT 1
					CASE ship_via = "FEDEX" OR ship_via = "FED EX" OR ship_via = "FEDERAL EXPRESS"
						REPLACE outship.scac WITH "FDEG" IN outship NEXT 1
					CASE outship.scac = 'UPGC'
						REPLACE outship.scac WITH "UPSL" IN outship NEXT 1
					CASE outship.scac = 'FGC'
						REPLACE outship.scac WITH "FDEG" IN outship NEXT 1
				ENDCASE
			ENDSCAN
		ENDIF

		LOCATE
		WAIT WINDOW "Now processing: "+cGroup+" - "+cLocName  TIMEOUT 1
		WAIT WINDOW "Now processing: "+cGroup+" - "+cLocName  NOWAIT NOCLEAR
*		ASSERT .F. MESSAGE "In General selection"


*!* At selection queries

		IF triggers.accountid = 6561
			SELECT accountid,consignee,wo_num,ship_ref,cnee_ref,ship_via,scac,swcnum,carrcode,del_date,.T. AS parcel ;
				FROM outship ;
				WHERE !" OV"$outship.ship_ref ;
				AND OCCURS(STR(outship.accountid,4),&cAcctGrp)>0 ;
				GROUP BY 1,2,3,4 ;
				INTO CURSOR outdelpt READWRITE
		ELSE
			SELECT accountid,consignee,wo_num,ship_ref,cnee_ref,ship_via,scac,swcnum,carrcode,del_date,.T. AS parcel ;
				FROM outship ;
				WHERE !" OV"$outship.ship_ref ;
				AND OCCURS(STR(outship.accountid,4),&cAcctGrp)>0 ;
				GROUP BY 1,3,4 ;
				ORDER BY 1,3,4 ;
				INTO CURSOR outdelpt READWRITE
		ENDIF

		LOCATE
		IF lKeepScreen
*!*				BROWSE
*!*				SET STEP ON
		ENDIF

		IF EOF()
			WAIT WINDOW "There are NO empty BOLs for "+cGroup+" - "+cLocName NOWAIT
			USE IN outdelpt
			lParcelRecs = .F.
		ELSE
			SELECT outdelpt
			COUNT TO ePT FOR !DELETED()
			LOCATE

			WAIT WINDOW "Group: "+cGroup+" - "+cLocName+"...There are initially "+TRANSFORM(ePT)+" PTs with empty BOL#" TIMEOUT 2

			LOCATE
			lParcelRecs = .F.

			SCAN FOR !DELETED()
				cConsignee = ALLTRIM(outdelpt.consignee)
				STORE .F. TO lAmazon,lAmazonFBA
				DO CASE
					CASE cConsignee = "AMAZON FBA"
						lAmazonFBA = .T.
					CASE "AMAZON"$cConsignee
						lAmazon = .T.
					CASE cConsignee = "SAMPLE " ;
							AND cConsignee#"SAMPLER STORES" ;
							AND OCCURS(STR(outdelpt.accountid,4),(gsamsungaccounts))=0
						DELETE NEXT 1 IN outdelpt
						LOOP
					CASE "SAMPLE"$cConsignee ;
							AND !INLIST(outdelpt.accountid,6803,6561,6532) ;
							AND OCCURS(STR(outdelpt.accountid,4),(gsamsungaccounts))=0 ;
							AND OCCURS(STR(outdelpt.accountid,4),(gtkohighlineaccounts))=0 ;
							AND !("FRENCH CONNECTION"$cConsignee)
						DELETE NEXT 1 IN outdelpt
						LOOP
					CASE (outdelpt.ship_ref = "SAMPLE" OR "DAMAGE"$outdelpt.ship_ref)
						DELETE NEXT 1 IN outdelpt
						LOOP
				ENDCASE

				nWO_Num = outdelpt.wo_num
				cSCAC = PADR(ALLTRIM(outdelpt.scac),4)

				IF cSCAC = 'UPGC' && Added per Darren/Bibian, 06.28.2010, Joe.
					cSCAC = "UPSL"
					REPLACE outdelpt.scac WITH cSCAC NEXT 1 IN outdelpt
				ENDIF
				IF cSCAC = 'FEG' AND cGroup = "MODERN SHOE"  && Added per Joe Smith, 09.10.2010, Joe.
					cSCAC = "FDEG"
					REPLACE outdelpt.scac WITH cSCAC NEXT 1 IN outdelpt
				ENDIF
				IF cGroup = "LP" AND cSCAC = "USPS" AND LEFT(TRIM(outdelpt.carrcode),2) = "03"
					REPLACE outdelpt.scac WITH "UPSN" IN outdelpt NEXT 1
				ENDIF

				lookups()

				cAcctGrp = ALLTRIM(triggers.acctgrp)
				IF STR(outdelpt.accountid,4)$(cAcctGrp) AND cSCAC = "DHLG"
					lUPS = .F.
					lFedex = .F.
				ELSE
					IF !SEEK(cSCAC,'parcels','scac')
						WAIT WINDOW "Not a Parcel SCAC...deleting" NOWAIT
						DELETE NEXT 1 IN outdelpt
						LOOP
					ELSE
						lUPS = parcels.ups
						lFedex = parcels.fedex
					ENDIF
				ENDIF

				cShip_via = ALLT(outdelpt.ship_via)
				cShip_refDel = ALLT(outdelpt.ship_ref)

				IF STR(outdelpt.accountid,4)$(gNanjingAccounts)
					IF SEEK(cShip_refDel,'outship','ship_ref')
						IF outship.qty = 0 AND EMPTY(outship.bol_no)
							DELETE NEXT 1
							LOOP
						ENDIF
					ENDIF
				ENDIF

				IF EMPTY(cSCAC)
					cEmptySCACString = PADR(ALLT(STR(outdelpt.accountid)),6)+PADR(TRIM(cShip_via),32)+TRIM(cShip_refDel)
					cEmptySCAC = IIF(EMPTY(cEmptySCAC),cEmptySCACString,cEmptySCAC+CHR(13)+cEmptySCACString)
					LOOP
				ELSE
					WAIT WINDOW "In SCAC crosscheck" NOWAIT
					IF (SEEK(cSCAC,'parcels','scac')) OR (STR(triggers.accountid,4)$(gBioworldAccounts) AND cSCAC = "DHLG")
						STORE ALLTRIM(outdelpt.ship_ref) TO cShip_ref
						
						STORE outdelpt.swcnum TO cSWCNum
						lFedex = IIF(parcels.fedex,.T.,.F.)
						lParcelRecs = .T.
						DO CASE
							CASE triggers.accountid = 6561 AND (cSCAC = "MIMK")  && Re-added 04.21.2015, Joe
								IF SEEK(cShip_refDel,'outship','ship_ref')
									cbn2="MIMK"+cShip_refDel
									cbl6 = RIGHT(TRIM(outship.bol_no),6)
									xsqlexec("update outship set bol_no = '"+cbn2+"' where accountid = 6561 and ship_ref = '"+cShip_refDel+"'",,,"wh")
									xsqlexec("update outship set blnum6 = '"+cbl6+"' where accountid = 6561 and ship_ref = '"+cShip_refDel+"'",,,"wh")
									RELEASE cbn2,cbl6

*!*									REPLACE outship.bol_no WITH ("MIMK"+cShip_refDel) NEXT 1 IN outship
*!*									REPLACE outship.blnum6 WITH RIGHT(TRIM(outship.bol_no),6) IN outship
								ENDIF
							CASE STR(triggers.accountid,4)$(gBioworldAccounts) AND ("AMAZON"$cConsignee OR "MODERN LIVING"$cConsignee)
								IF SEEK(cShip_refDel,'outship','ship_ref')
									cbn2="AMZ"+cShip_refDel
									cbl6 = RIGHT(TRIM(outship.bol_no),6)
									xsqlexec("update outship set bol_no = '"+cbn2+"' where accountid in ("+gBioworldAccounts+") and ship_ref = '"+cShip_refDel+"'",,,"wh")
									xsqlexec("update outship set blnum6 = '"+cbl6+"' where accountid in ("+gBioworldAccounts+") and ship_ref = '"+cShip_refDel+"'",,,"wh")
									RELEASE cbn2,cbl6

*!*									REPLACE outship.bol_no WITH ("AMZ"+cShip_refDel) IN outship
*!*									REPLACE outship.blnum6 WITH RIGHT(TRIM(outship.bol_no),6) IN outship
								ENDIF
							OTHERWISE
								DO PARCELBOL_INSERT WITH nWO_Num,cShip_ref,cSWCNum
								SELECT outdelpt
								DELETE NEXT 1
						ENDCASE
					ELSE
						IF cShip_refDel = "DO NOT USE"
* Do nothing
						ELSE
							cBOLString = PADR(cGroup,15)+PADR(ALLT(STR(outdelpt.accountid)),6)+PADR(TRIM(cOffice),3)+PADR(cSCAC,6)+cShip_refDel
							cMissBOLs = IIF(EMPTY(cMissBOLs),cBOLString,cMissBOLs+CHR(13)+cBOLString)
							cBOLString = ""
						ENDIF
					ENDIF
				ENDIF
			ENDSCAN
			IF !EMPTY(cMissBOLs)
				EmptyBOLMail()
			ENDIF
			USE IN outdelpt
		ENDIF

		IF lParcelRecs
			WAIT WINDOW "There were UPS/FedEx records processed...Group "+cGroup+" - "+cLocName TIMEOUT 1
			WAIT WINDOW "There were UPS/FedEx records processed...Group "+cGroup+" - "+cLocName NOWAIT
		ELSE
			WAIT WINDOW "There were NO UPS/FedEx records to process for Group "+cGroup+" - "+cLocName TIMEOUT 1
			WAIT WINDOW "There were NO UPS/FedEx records to process for Group "+cGroup+" - "+cLocName NOWAIT
		ENDIF

		SELECT outshipid,ship_ref,cnee_ref,wo_num,wo_date AS DATE,del_date,scac,outship.accountid AS accountid,cOffice AS office,cnee_ref,;
			"945 "AS edi_type,bol_no AS bol,consignee,"TRIG-PARCELS" AS trig_from,"TGFPROC" AS trig_by,;
			DATETIME() AS trig_time,.F. AS processed,.F. AS errorflag,SPACE(25) AS fin_status, ;
			IIF((LEFT(vendor_num,3)="10-" AND outship.accountid = 4694),.T.,.F.) AS adny,.F. AS moret,"" AS comments,IIF(triggers.GROUP = "MERKURY" AND INLIST(cacctnum,"GROUPDS","LIVSO10","SUPPORT","QUIB","STAPL"),.T.,.F.) AS ptflag ;
			FROM outship ;
			WHERE BETWEEN(del_date,dDate,DATE()) ;
			AND !EMPTYnul(del_date) ;
			AND !EMPTY(bol_no) ;
			AND STR(outship.accountid,4)$(cAcctGrp) ;
			ORDER BY bol_no,ship_ref ;
			INTO CURSOR outdel READWRITE

		SELECT outdel
		LOCATE

		SCAN FOR !DELETED()
			IF moretaccount("outdel.accountid")  && Moret
				IF outdel.accountid # 6303
					REPLACE outdel.moret WITH .T. IN outdel
				ENDIF
			ENDIF
			cSCAC = outdel.scac
			IF !SEEK(cSCAC,'parcels','scac')
				DELETE
			ENDIF
		ENDSCAN

		SELECT outdel
		LOCATE

		lookups()
		SCAN FOR !DELETED()
			SELECT edi_trigger
			LOCATE
			=SEEK(STR(outdel.accountid,4)+outdel.ship_ref,"edi_trigger","acct_pt")
			IF FOUND()
				WAIT WINDOW "Found in Triggers" NOWAIT
				SELECT outdel
				DELETE NEXT 1 IN outdel
			ENDIF
			SELECT outdel
		ENDSCAN
		lLoad = .T.
		LOCATE

		IF EOF()
			WAIT WINDOW "No records to load for "+cGroup+" - "+cLocName NOWAIT
			lLoad = .F.
		ENDIF

		SELECT edi_trigger
		IF lTestLoad OR !lLoad
			WAIT WINDOW "Skipping EDI_TRIGGER load" NOWAIT
		ELSE
			APPEND FROM DBF('outdel')
		ENDIF
		USE IN outdel
		WAIT WINDOW cGroup+" Parcels - 945 trigger process complete" NOWAIT
*		NormalExit = .T.
		WAIT CLEAR
		IF !EMPTY(cEmptySCAC)
			EmptySCACMail()
		ENDIF

		IF !EMPTY(cPTSWC)
			DO SWCMail
		ENDIF

		IF !EMPTY(cPTMail)
			DO PTMail
		ENDIF

*!*				SELECT outship
*!*				LOCATE
*!*				IF lKeepScreen
*!*					SET STEP ON
*!*				ENDIF
*!*				IF RECCOUNT()>0
*!*					LOCATE
*!*					SET STEP ON
*!*					SCAN
*!*						cShip_ref = outship.ship_ref
*!*						cBOL = outship.bol_no
*!*						xsqlexec("update outship set bol_no = '"+cBOL+"' where ship_ref = '"+cShip_ref+"' and accountid in ("+&cAcctGrp+")",,,"wh")
*!*					ENDSCAN
*!*				ENDIF
	ENDSCAN

	IF lDoFinish
		openparcel945mailer()

*!*	Added 09.08.2016 to record runs of this process
		SELECT 0
		USE F:\3pl\DATA\trigger_runs
		INSERT INTO trigger_runs (PROCESS,run_time) VALUES ("PARCEL",DATETIME())
		USE IN trigger_runs

		WAIT WINDOW "Now running pre-close parcels to mark delivered" TIMEOUT 1
		parcel945earlytrigger()
	ENDIF


CATCH TO oErr
	IF !NormalExit
		SET STEP ON
		ASSERT .F. MESSAGE "At CATCH..."
		tsubject = "Parcel 945 Trigger "+cOffice+" Upload Error ("+TRANSFORM(oErr.ERRORNO)+") at "+TTOC(DATETIME())
		tattach  = ""
		tmessage = "Parcel 945 Trigger "+cOffice+" Process Error"
		lcSourceMachine = SYS(0)
		lcSourceProgram = SYS(16)

		tmessage = tmessage+ "Try/CATCH Exeception Message:"+CHR(13)+;
			[  Error: ] + STR(oErr.ERRORNO) +CHR(13)+;
			[  LineNo: ] + STR(oErr.LINENO) +CHR(13)+;
			[  Message: ] + oErr.MESSAGE +CHR(13)+;
			[  Procedure: ] + oErr.PROCEDURE +CHR(13)+;
			[  Details: ] + oErr.DETAILS +CHR(13)+;
			[  StackLevel: ] + STR(oErr.STACKLEVEL) +CHR(13)+;
			[  LineContents: ] + oErr.LINECONTENTS+CHR(13)+;
			[  UserValue: ] + oErr.USERVALUE+CHR(13)+;
			[  Computer:  ] +lcSourceMachine+CHR(13)+;
			[  Program:   ] +lcSourceProgram

		tmessage = tmessage + CHR(13) + cUserMessage

		tattach = ""
		tCC = IIF(lAriat,"paul.gaidis@tollgroup.com,juan.rocio@tollgroup.com","")
		tFrom = "TGF EDI Processing Center <toll-edi-ops@tollgroup.com>"
		DO FORM m:\dev\frm\dartmail2 WITH tSendto,tFrom,tsubject,tCC,tattach,tmessage,"A"
	ELSE
		WAIT WINDOW AT 10,10 "Normal Exit " TIMEOUT 2
	ENDIF
FINALLY
	WAIT WINDOW "Finished processing all accounts/locations...exiting"  TIMEOUT 2
	WAIT CLEAR
	schedupdate()
	IF USED('FTPSETUP')
		USE IN FTPSETUP
	ENDIF
	SELECT 0
	USE F:\edirouting\FTPSETUP SHARED
	REPLACE FTPSETUP.chkbusy WITH .F. FOR FTPSETUP.TRANSFER = "TRIG-PARCEL945"
	CLOSE DATA ALL
ENDTRY


****************************************************************************************
PROCEDURE PARCELBOL_INSERT
****************************************************************************************
* explain what this is spupposed to accomplish
****************************************************************************************
	PARAMETERS nWO_Num,cShip_ref,cSWCNum
	lRunLocate = .F.

	WAIT CLEAR
	WAIT WINDOW "Now scanning PT# "+cShip_ref NOWAIT NOCLEAR

	SELECT outship
	LOCATE
	LOCATE FOR outship.ship_ref = PADR(ALLTRIM(cShip_ref),20) AND wo_num = nWO_Num AND (OCCURS(STR(outship.accountid,4),&cAcctGrp)>0  OR outship.accountid = 9999)
	cAccountid = ALLT(STR(outship.accountid))
	IF FOUND()
		IF cGroup = "MORET"
			IF ISALPHA(cShip_ref)
				cShip_RefUnix = SUBSTR(LEFT(ALLTRIM(cShip_ref),6),2)
				cOffChar = ICASE(cOffice = "M","4",INLIST(cOffice,"C","7"),"3","5")
				cShip_refBOL = ALLT(cAccountid+cOffChar+PADL(cShip_RefUnix,9,'0'))
			ELSE
				cShip_refBOL = ALLT(cAccountid+PADL(ALLTRIM(STRTRAN(cShip_ref,"~","")),10,'0'))
			ENDIF
		ELSE
			IF "~"$cShip_ref  && Split PT
				STORE LEFT(cShip_ref,AT("~",cShip_ref)-1) TO cShip_refBOL
			ELSE
				STORE cShip_ref TO cShip_refBOL
			ENDIF
		ENDIF

		IF EMPTY(bol_no) AND !EMPTYnul(del_date)
*ASSERT .F. MESSAGE "In ParcelBOL_Insert-Empty BOL"
			WAIT WINDOW "In ParcelBOL_Insert-Empty BOL" NOWAIT

			IF lMoret AND lAmazonFBA AND !lAmazon
				IF lFedex
					cBOLREPL = "PSF"+PADL(ALLT(cShip_refBOL),14,"0")
				ELSE
					cBOLREPL = "PSU"+PADL(ALLT(cShip_refBOL),14,"0")
				ENDIF
				IF EMPTY(TRIM(outship.bol_no))
					DO WHILE !RLOCK('outship')
						LOOP
					ENDDO
					cBN6 = RIGHT(TRIM(outship.bol_no),6)

					xsqlexec("update outship set bol_no = '"+cBOLREPL+"' where accountid = "+cAccountid+" and ship_ref = '"+cShip_ref+"'",,,"wh")
					xsqlexec("update outship set blnum6 = '"+cBN6+"' where accountid = "+cAccountid+" and ship_ref = '"+cShip_ref+"'",,,"wh")
					RELEASE cBN6
*!*					REPLACE outship.bol_no WITH cBOLREPL NEXT 1 IN outship
*!*					REPLACE outship.blnum6 WITH RIGHT(TRIM(outship.bol_no),6) IN outship
				ENDIF

				xsqlexec("select * from swcdet where mod='"+cWhse+"' and swcnum='"+cSWCNum+"'","xswcdet",,"wh")
				IF RECCOUNT()#0
					xsqlexec("update swcdet set bol_no='"+cBOLREPL+"', blnum6='"+RIGHT(TRIM(cBOLREPL),6)+"' " + ;
						"where mod='"+cWhse+"' and swcnum='"+cSWCNum+"'",,,"wh")
				ELSE
					cPTSWC = IIF(EMPTY(cPTSWC),cShip_ref,cPTSWC+CHR(13)+cShip_ref)
				ENDIF
				USE IN xswcdet
				SELECT outdelpt
			ELSE
				IF INLIST(cGroup,"COURTAULDS")
					ASSERT .F. MESSAGE "At Bugaboo check"
					cBOLREPL = "PSU"+PADL(ALLT(cShip_refBOL),14,"0")
					IF EMPTY(TRIM(outship.bol_no))
						cBN6 = RIGHT(TRIM(outship.bol_no),6)

						xsqlexec("update outship set bol_no = '"+cBOLREPL+"' where accountid = "+cAccountid+" and ship_ref = '"+cShip_ref+"'",,,"wh")
						xsqlexec("update outship set blnum6 = '"+cBN6+"' where accountid = "+cAccountid+" and ship_ref = '"+cShip_ref+"'",,,"wh")
						RELEASE cBN6
*!*						REPLACE outship.bol_no WITH cBOLREPL NEXT 1
*!*						REPLACE outship.blnum6 WITH RIGHT(TRIM(outship.bol_no),6) IN outship
					ENDIF

					xsqlexec("select * from swcdet where mod='"+cWhse+"' and swcnum='"+cSWCNum+"'","xswcdet",,"wh")
					IF RECCOUNT()#0
						xsqlexec("update swcdet set bol_no='"+cBOLREPL+"', blnum6='"+RIGHT(TRIM(cBOLREPL),6)+"' " + ;
							"where mod='"+cWhse+"' and swcnum='"+cSWCNum+"'",,,"wh")
					ELSE
						cPTSWC = IIF(EMPTY(cPTSWC),cShip_ref,cPTSWC+CHR(13)+cShip_ref)
					ENDIF
					USE IN xswcdet
					SELECT outdelpt
				ELSE
					SELECT shipment
					LOCATE
					lRunLocate = .T.
					IF INLIST(outdelpt.accountid,6320,6532)
						LOCATE FOR pickticket = PADR(ALLTRIM(cShip_ref),20) AND wo_num = nWO_Num
					ELSE
						WAIT WINDOW "Locating PT in appropriate account" NOWAIT NOCLEAR
						LOCATE FOR shipment.pickticket = PADR(ALLTRIM(cShip_ref),20) AND shipment.wo_num = nWO_Num ;
							AND ((RIGHT(STR(shipment.accountid),4)$&cAcctGrp) OR (shipment.accountid = 9999))
						WAIT CLEAR
					ENDIF
				ENDIF
			ENDIF

			IF lRunLocate
				IF FOUND()
					SELECT package
					LOCATE FOR package.shipmentid = shipment.shipmentid
					IF !FOUND()
						cTrkMessage = cGroup+" ShipID "+ALLTRIM(STR(shipment.shipmentid))+" (PT# "+cShip_ref+") not in Package"
						WAIT WINDOW cTrkMessage TIMEOUT 2
						MissTrkNumMail()
						RETURN
					ENDIF

					IF INLIST(outdelpt.accountid,&gbioworldaccounts)
						SCAN FOR package.shipmentid = shipment.shipmentid
							cBOLREPL = ALLTRIM(package.trknumber)
							IF EMPTY(cBOLREPL)
								LOOP
							ELSE
								EXIT
							ENDIF
						ENDSCAN
						IF EMPTY(cBOLREPL)
							cTrkMessage = cGroup+": No Tracking # for PT "+ALLTRIM(cShip_ref)+", SHP/ID "+ALLTRIM(STR(shipment.shipmentid))+" in Package"
							WAIT WINDOW cTrkMessage TIMEOUT 1
							MissTrkNumMail()
							RETURN
						ENDIF
					ELSE
						cBOLREPL = ALLTRIM(package.trknumber) && Can only use first line for whole PT
						IF EMPTY(cBOLREPL)
							cTrkMessage = cGroup+": No Tracking # for PT "+ALLTRIM(cShip_ref)+", SHP/ID "+ALLTRIM(STR(shipment.shipmentid))+" in Package"
							WAIT WINDOW cTrkMessage TIMEOUT 1
							MissTrkNumMail()
							RETURN
						ENDIF
					endif

						SUM(package.pkgcharge) TO nUPSCharge FOR package.shipmentid = shipment.shipmentid

						SELECT outship
						IF EMPTY(TRIM(outship.bol_no))
							IF DATETIME()<DATETIME(2017,11,13,21,00,00)
								SET STEP ON
							ENDIF
							nAcct = outship.accountid
							cShipins = outship.shipins
							cBN6 = RIGHT(TRIM(outship.bol_no),6)
							cBOLReplShort = LEFT(cBOLREPL,20)

							xsqlexec("update outship set bol_no = '"+cBOLReplShort+"' where accountid = "+TRANSFORM(nAcct)+" and ship_ref = '"+PADR(cShip_ref,20)+"'",,,"wh")
							xsqlexec("update outship set blnum6 = '"+cBN6+"' where accountid = "+TRANSFORM(nAcct)+" and ship_ref = '"+PADR(cShip_ref,20)+"'",,,"wh")
							RELEASE cBN6
*!*						REPLACE outship.bol_no WITH cBOLREPL NEXT 1 IN outship
*!*						REPLACE outship.blnum6 WITH RIGHT(TRIM(outship.bol_no),6) IN outship
							IF LEN(ALLTRIM(cBOLREPL))>20
								IF !"LONGTRKNUM*"$outship.shipins
									cShipins = cShipins+CHR(13)+"LONGTRKNUM*"+cBOLREPL
									xsqlexec("update outship set shipins = '"+cShipins+"' where accountid = "+TRANSFORM(nAcct)+" and ship_ref = '"+PADR(cShip_ref,20)+"'",,,"wh")
									RELEASE cShipins
								ENDIF
							ENDIF
						ENDIF

						xsqlexec("select * from swcdet where mod='"+cWhse+"' and swcnum='"+cSWCNum+"'","xswcdet",,"wh")
						IF RECCOUNT()#0
							xsqlexec("update swcdet set bol_no='"+cBOLReplShort+"', blnum6='"+RIGHT(TRIM(cBOLREPL),6)+"' " + ;
								"where mod='"+cWhse+"' and swcnum='"+cSWCNum+"'",,,"wh")
						ELSE
							cPTSWC = IIF(EMPTY(cPTSWC),cShip_ref,cPTSWC+CHR(13)+cShip_ref)
						ENDIF
						USE IN xswcdet

						SELECT outship
						IF EMPTY(outship.scac)
							IF lFedex
								xsqlexec("update outship set scac = 'FDEG' where accountid = "+cAccountid+" and ship_ref = '"+PADR(cShip_ref,20)+"'",,,"wh")
*							REPLACE outship.scac WITH "FDEG" NEXT 1 IN outship
							ELSE
								xsqlexec("update outship set scac = 'UPSN' where accountid = "+cAccountid+" and ship_ref = '"+PADR(cShip_ref,20)+"'",,,"wh")
*							REPLACE outship.scac WITH "UPSN" NEXT 1 IN outship
							ENDIF
						ENDIF

						IF !("UPSCHARGE*"$outship.shipins)
							cShipins = outship.shipins
							cShipins = cShipins+CHR(13)+"UPSCHARGE*"+ALLTRIM(STR(nUPSCharge,8,2))
							xsqlexec("update outship set shipins = '"+cShipins+"' where accountid = "+cAccountid+" and ship_ref = '"+PADR(cShip_ref,20)+"'",,,"wh")
							RELEASE cShipins
*						REPLACE outship.shipins WITH outship.shipins+CHR(13)+"UPSCHARGE*"+ALLTRIM(STR(nUPSCharge,8,2)) NEXT 1 IN outship
						ENDIF
					ELSE
						DO CASE
							CASE STR(outship.accountid,4)$(gtkohighlineaccounts)  && Modern Shoe
								nAcct = outship.accountid
								IF EMPTY(TRIM(outship.bol_no))
									cBOLREPL = "PSU"+PADL(ALLT(outship.ship_ref),14,"0")
									IF EMPTY(TRIM(outship.bol_no))
										cBN6 = RIGHT(TRIM(outship.bol_no),6)
										xsqlexec("update outship set bol_no = '"+cBOLREPL+"' where accountid = "+TRANSFORM(nAcct)+" and ship_ref = '"+PADR(cShip_ref,20)+"'",,,"wh")
										xsqlexec("update outship set blnum6 = '"+cBN6+"' where accountid = "+TRANSFORM(nAcct)+" and ship_ref = '"+PADR(cShip_ref,20)+"'",,,"wh")
										RELEASE cBN6
									ENDIF

									xsqlexec("select * from swcdet where mod='"+cWhse+"' and swcnum='"+cSWCNum+"'","xswcdet",,"wh")
									IF RECCOUNT()#0
										xsqlexec("update swcdet set bol_no='"+cBOLREPL+"', blnum6='"+RIGHT(TRIM(cBOLREPL),6)+"' " + ;
											"where mod='"+cWhse+"' and swcnum='"+cSWCNum+"'",,,"wh")
										cPTSWC = IIF(EMPTY(cPTSWC),cShip_ref,cPTSWC+CHR(13)+cShip_ref)
									ENDIF
									USE IN xswcdet

									SELECT outship
								ENDIF
							CASE lFedex
								IF EMPTY(outship.bol_no)
									nAcct = outship.accountid
									DO CASE
										CASE LEFT(cSCAC,3) = "DHL"
											cBOLREPL = "PSD"+PADL(ALLTRIM(cShip_refBOL),14,"0")
										CASE INLIST(cSCAC,"PRRM","USPS","USPZ","PCPS","SFIR","SPRI","SPAD") OR LEFT(ALLTRIM(outship.ship_via),4) = "USPS" && Postal Parcel
											cBOLREPL = "PSP"+PADL(ALLTRIM(cShip_refBOL),14,"0")
										OTHERWISE
											IF !INLIST(outship.accountid,5102,1285,6416,6532)
												cBOLREPL = "PSF"+PADL(ALLTRIM(cShip_refBOL),14,"0")
											ENDIF
									ENDCASE
									IF EMPTY(TRIM(outship.bol_no))
										cBN6 = RIGHT(TRIM(outship.bol_no),6)
										xsqlexec("update outship set bol_no = '"+cBOLREPL+"' where accountid = "+TRANSFORM(nAcct)+" and ship_ref = '"+PADR(cShip_ref,20)+"'",,,"wh")
										xsqlexec("update outship set blnum6 = '"+cBN6+"' where accountid = "+TRANSFORM(nAcct)+" and ship_ref = '"+PADR(cShip_ref,20)+"'",,,"wh")
										RELEASE cBN6

*									REPLACE outship.bol_no WITH cBOLREPL IN outship NEXT 1
*									REPLACE outship.blnum6 WITH RIGHT(TRIM(outship.bol_no),6) IN outship
									ENDIF
								ENDIF
							OTHERWISE
								IF cGroup = "MORET"
									nAcct = outship.accountid
									IF !lAmazon
										cBOLREPL = "PSU"+PADL(ALLT(cShip_refBOL),14,"0")
										cBN6 = RIGHT(TRIM(outship.bol_no),6)
										IF EMPTY(TRIM(outship.bol_no))
											xsqlexec("update outship set bol_no = '"+cBOLREPL+"' where accountid = "+TRANSFORM(nAcct)+" and ship_ref = '"+PADR(cShip_ref,20)+"'",,,"wh")
											xsqlexec("update outship set blnum6 = '"+cBN6+"' where accountid = "+TRANSFORM(nAcct)+" and ship_ref = '"+PADR(cShip_ref,20)+"'",,,"wh")
											RELEASE cBN6

*										REPLACE outship.bol_no WITH cBOLREPL IN outship NEXT 1
*										REPLACE outship.blnum6 WITH RIGHT(TRIM(outship.bol_no),6) IN outship
										ENDIF
									ELSE
										IF LEFT(ALLTRIM(outship.keyrec),2)="1Z"
											cBN6 = RIGHT(TRIM(outship.bol_no),6)
											cBOLREPL = ALLTRIM(outship.keyrec)
											xsqlexec("update outship set bol_no = '"+cBOLREPL+"' where accountid = "+TRANSFORM(nAcct)+" and ship_ref = '"+PADR(cShip_ref,20)+"'",,,"wh")
											xsqlexec("update outship set blnum6 = '"+cBN6+"' where accountid = "+TRANSFORM(nAcct)+" and ship_ref = '"+PADR(cShip_ref,20)+"'",,,"wh")
											RELEASE cBN6

*										REPLACE outship.bol_no WITH ALLTRIM(outship.keyrec) IN outship NEXT 1
*										REPLACE outship.blnum6 WITH RIGHT(TRIM(outship.bol_no),6) IN outship
										ENDIF
									ENDIF
								ELSE
									WAIT WINDOW "PT# "+cShip_ref+" Not Found in UPS SHIPMENT Table" TIMEOUT 1
									cPTString = PADR(ALLT(STR(nAccountid)),8)+PADR(ALLT(cOffice),3)+cShip_ref
									cPTMail = IIF(EMPTY(cPTMail),cPTString,cPTMail+CHR(13)+cPTString)
									cPTString = ""
								ENDIF
						ENDCASE
					ENDIF
				ENDIF
			ELSE
				WAIT WINDOW "PT Not Found in OUTSHIP for this Account!" NOWAIT
			ENDIF
		ENDIF

		SELECT outship
		SET ORDER TO
	ENDPROC
*****************************************************************************************************************************

****************************
PROCEDURE PTMail
****************************
	tsubject= cGroup+" 945 Trigger: Missing PTs"
	tattach = ""
	tmessage = "During 945 triggering, the following picktickets were not found in the UPS table:"+CHR(10)
	tmessage = tmessage + cPTMail

	IF STR(outship.accountid,4)$(gtkohighlineaccounts)
		tsendto2 = tSendto
		tcc2 = tCC
		SELECT 0
		USE F:\3pl\DATA\mailmaster ALIAS mm
		LOCATE
		LOCATE FOR mm.edi_type = 'MISC' AND mm.taskname = "PARCELERR"
		tSendto = IIF(mm.use_alt,mm.sendtoalt,mm.sendto)
		tCC = IIF(mm.use_alt,mm.ccalt,mm.cc)
		USE IN mm
	ENDIF
	IF lSendmail
		DO FORM m:\dev\frm\dartmail2 WITH tSendto,tFrom,tsubject,tCC,tattach,tmessage,"A"
	ENDIF
	cPTMail = ""
	IF STR(outship.accountid,4)$(gtkohighlineaccounts)
		tCC = tcc2
		tSendto = tsendto2
	ENDIF
ENDPROC


****************************
PROCEDURE EmptySCACMail
****************************
	tsubject= "945 Trigger: Empty SCAC Report"
	tattach = ""
	tmessage = cGroup+" during 945 triggering, is missing SCAC codes in the following PTs:"+CHR(10)
	tmessage = tmessage + cEmptySCAC

	IF moretaccount("outship.accountid")
		SELECT 0
		USE F:\3pl\DATA\mailmaster ALIAS mm
		LOCATE FOR edi_type = "MISC" AND taskname = "MORETNOSCAC"
		tCC = IIF(mm.use_alt,ccalt,cc)
		USE IN mm
	ENDIF

	IF lSendmail
		DO FORM m:\dev\frm\dartmail2 WITH tSendto,tFrom,tsubject,tCC,tattach,tmessage,"A"
	ENDIF
	cEmptySCAC = ""
ENDPROC


****************************
PROCEDURE EmptyBOLMail
****************************
	ASSERT .F. MESSAGE "In Empty BOL Mail procedure"

	IF moretaccount("outship.accountid")
		SELECT 0
		USE F:\3pl\DATA\mailmaster ALIAS mm
		LOCATE FOR edi_type = "MISC" AND taskname = "MORETOTHERR"
		tCC = IIF(mm.use_alt,ccalt,cc)
		USE IN mm
	ENDIF

	tsubject= "Parcel 945 Trigger: Empty BOL Report"
	tattach = ""
	tmessage = cGroup+" during 945 triggering, is missing BOL#(s) in the following PTs:"+CHR(10)
	tmessage = tmessage + cMissBOLs

	IF lSendmail
		DO FORM m:\dev\frm\dartmail2 WITH tSendto,tFrom,tsubject,tCC,tattach,tmessage,"A"
	ENDIF
	cMissBOLs = ""
ENDPROC

****************************
PROCEDURE SWCMail
****************************
	tsubject= cGroup+" 945 Trigger Process: Missing SWCs"
	tattach = ""
	tmessage = "During 945 triggering, the following picktickets didn't have SWC Numbers in SWCDET:"+CHR(13)
	tmessage = tmessage + cPTSWC

	IF moretaccount("outship.accountid")
		SELECT 0
		USE F:\3pl\DATA\mailmaster ALIAS mm
		LOCATE FOR edi_type = "MISC" AND taskname = "MORETOTHERR"
		tCC = IIF(mm.use_alt,ccalt,cc)
		USE IN mm
		lSendmail = .T.
	ELSE
		lSendmail = .F.
	ENDIF

	IF lSendmail
		DO FORM m:\dev\frm\dartmail2 WITH tSendto,tFrom,tsubject,tCC,tattach,tmessage,"A"
	ENDIF
	cPTSWC = ""
ENDPROC

****************************
PROCEDURE MissBldgMail
****************************
	SET STEP ON
	tsubject= cGroup+" 945 Trigger Process: Missing BUILDING"
	tattach = ""
	tmessage = "During 945 triggering, the following didn't have a BUILDING code in 'Account':"+CHR(13)
	tmessage = tmessage+cGroup
	tmessage = tmessage+CHR(13)+"TIME: "+TTOC(DATETIME())
	tCC = " "
	DO FORM m:\dev\frm\dartmail2 WITH tSendto,tFrom,tsubject,tCC,tattach,tmessage,"A"
ENDPROC

****************************
PROCEDURE MissTrkNumMail
****************************
	tsendtotrk = tSendto
*	tsendtotrk = "pgaidis@fmiint.com"
	IF USED('mm')
		USE IN mm
	ENDIF
	tsubject= "945 Trigger Process: Parcel Table Issue"
	tattach = ""
	tmessage = cTrkMessage
	tmessage = tmessage+CHR(13)+"TIME: "+TTOC(DATETIME())
*	tCCtrk = " "
	tCCtrk = IIF(lAriat,"paul.gaidis@tollgroup.com,juan.rocio@tollgroup.com,maria.estrella@tollgroup.com","")
	DO FORM m:\dev\frm\dartmail2 WITH tsendtotrk,tFrom,tsubject,tCCtrk,tattach,tmessage,"A"
	cTrkMessage = ""
ENDPROC
