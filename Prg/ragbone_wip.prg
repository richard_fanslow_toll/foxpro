* WIP for RAG and Bone
*
* modified list of email recipients 2/20/2018 MB

utilsetup("RAGBONE_WIP")

guserid="RAGBONE"
gwipbywo=.F.
gwipbybl=.F.
xpnpacct=.F.
xwip=.T.
xtitle="Work in Progress"
GOFFICE='I'

If !Used("edi_trigger")
  use f:\3pl\data\edi_trigger shared In 0
ENDIF

xsqlexec("select * from pt where mod='"+GOFFICE+"' and accountid=6699",,,"wh")
Index On ptid Tag ptid
Set Order To

xsqlexec("select * from account where inactive=0","account",,"qq")
Index On accountid Tag accountid
Set Order To

xsqlexec("select * from outship where mod='I' and accountid=6699 and (del_date={}  OR del_date>={"+DTOC(DATE()-7)+"}) and notonwip=0",,,"wh")

Select dept, ship_ref, batch_num As div,consignee, cnee_ref, qty, ptdate, Start, Cancel, {}As called, {} routedfor, {} appt, scac, ;
	space(20) appt_num,Space(35) apptremarks, ship_via,Space(20) bol_no, 00000000 wo_num,{}  wo_date,{}  pulleddt,{}  picked,{} ;
	labeled,{} staged, {} del_date ,000000  ctnqty,0000  outshipid,0000 As pt_age, 0000 As days_to_cncll, accountid As account,Space(2) As WHSE ;
	from pt ;
	where qty!=0 	Into Cursor xrpt2 Readwrite

Select dept, ship_ref, batch_num As div,consignee, cnee_ref, qty, ptdate, Start, Cancel, called, routedfor, appt, scac, ;
	appt_num, apptremarks, ship_via, bol_no, wo_num, wo_date, pulleddt, picked, labeled, staged, del_date, ctnqty, outshipid,0000 As pt_age, 0000 As days_to_cncl, accountid As account,Space(2) As WHSE ;
	from outship ;
	where !notonwip And (emptynul(del_date) OR del_date>=DATE()-5) ;
	into Cursor xrpt1 Readwrite

Select * From xrpt1 Union All Select * From xrpt2 Into Cursor xrpt3 Readwrite
Replace All pt_age With (Date()-ptdate) FOR (Date()-ptdate)>0
Replace All   days_to_cncl With 9999 For Cancel={01/01/1900}
Replace All days_to_cncl With (Cancel-Date()) For Cancel !={01/01/1900}
Replace All WHSE With 'NJ'
Select xrpt3.*, Space(15) As Status, SPACE(50) as file945 From xrpt3 Into Cursor XRPT4 Readwrite
Replace All Status With Iif(emptynul(wo_date),"UNALLOCATED",Iif(!emptynul(staged),"STAGED",Iif(!emptynul(labeled),"LABELED",Iif(!emptynul(picked),"PICKED",Iif(!emptynul(pulleddt),"PULLED","ALLOCATED"))))) In XRPT4
replace status WITH "SHIPPED" FOR (del_date)>{01/01/1900} In XRPT4

SET STEP on
 

Select * From XRPT4 Into Cursor xrptj Readwrite
Scan
	xsqlexec("select called from apptcall where mod='I' and outshipid="+Transform(xrptj.outshipid),,,"wh")
	Calculate Min(called) To aa
	Replace called With aa In xrptj
	
	SELECT * FROM edi_trigger WHERE ship_ref = xrptj.ship_ref AND !EMPTY(file945) INTO CURSOR etrig
	IF RECCOUNT() > 0
		this945 = RTRIM(etrig.file945)
		char1 = RAT('\', this945)
		fNameLen = LEN(this945)-char1
		REPLACE file945 WITH RIGHT(this945,fNameLen) in xrptj
	ENDIF 
Endscan

SELECT xrptj
set step on 
If Reccount() > 0 
	copy to F:\FTPUSERS\RagBone\reports\ragnbone_wip.xls type xls
        xfile='F:\FTPUSERS\RagBone\reports\ragnbone_wip.xls'
        oexcel = createobject("excel.application")
        oexcel.displayalerts = .f.
        oworkbook = oexcel.workbooks.open(xfile,,.f.)
        xfile=xfile+"x"
        oworkbook.saveas(xfile) && "39" converts the save format to excel95/97
        oworkbook.close()
        oexcel.quit()

	* removed some recipients per RF385852 2/20/201 MB
	*tsendto = "Keara.Mageras@rag-bone.com; Tess.Betts@rag-bone.com; Andrew.Stambro@rag-bone.com; Sandra.Elescano@rag-bone.com; Greg.Hubbert@rag-bone.com; Jason.Cartagena@rag-bone.com"
	tsendto = "Keara.Mageras@rag-bone.com; Andrew.Stambro@rag-bone.com; Sandra.Elescano@rag-bone.com; Greg.Hubbert@rag-bone.com; Maggie.huang@rag-bone.com; Leandra.murchison@tollgroup.com; Chris.Malcolm@tollgroup.com"
*	tsendto = "darren.young@tollgroup.com"
	tattach = "F:\FTPUSERS\RagBone\reports\ragnbone_wip.xlsx" 
	emailx(tsendto,,"Rag and Bone WIP COMPLETE","Rag and Bone WIP_:"+Ttoc(Datetime()),,tattach)	
ENDIF

schedupdate()

* removed stuff dy 4/5/18

*!*	close data all 

*!*	Try
*!*		_Screen.Caption=gscreencaption
*!*	Catch
*!*	Endtry

*!*	On Error