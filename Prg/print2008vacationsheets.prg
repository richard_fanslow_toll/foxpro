* Print the sheets showing 2008 1/1 balances.
SET SAFETY OFF

LOCAL oExcel, oWorkbook, oWorksheet
LOCAL lnRow, lcRow, lcXLFile, lcPrintMark, lnJan1VacBalanceInDays

*lcXLFile = GETFILE()
lcXLFile = 'C:\ACCRUALS_2008_FILES\vacation_1_1_2008.xls'

oExcel = CREATEOBJECT("excel.application")
oExcel.VISIBLE = .T.

oWorkbook = oExcel.workbooks.OPEN(lcXLFile)

*oWorkbook.SAVEAS(lcFiletoSaveAs)

oWorksheet = oWorkbook.Worksheets[1]

FOR lnRow = 3 TO 661
	lcRow = ALLTRIM(STR(lnRow))

	* SHOULD WE PRINT THIS ROW?
	lcPrintMark = "N"
	
	IF NOT ISNULL( oWorksheet.RANGE("Z" + lcRow).VALUE ) THEN
		lcPrintMark = UPPER(ALLTRIM(oWorksheet.RANGE("Z" + lcRow).VALUE))
	ENDIF
	
	* see if 1/1/2008 Vac balance was > 0
	lnJan1VacBalanceInDays = oWorksheet.RANGE("I" + lcRow).VALUE / 8
	
	IF (lcPrintMark = "Y") AND (lnJan1VacBalanceInDays > 0) THEN
	
		* PRINT VAC EXPLANATION SHEET
		
		* first populate the print area of the spreadsheet
		
		* Employee name
		oWorksheet.RANGE("AH3").VALUE = "For: " + ALLTRIM(oWorksheet.RANGE("C" + lcRow).VALUE)
		
		* Employee Seniority Date
		oWorksheet.RANGE("AH7").VALUE = "Your effective Hire Date was: " + TRANSFORM(TTOD(oWorksheet.RANGE("G" + lcRow).VALUE))
		
		* 1/1/2008 vac Balance
		oWorksheet.RANGE("AI9").VALUE = lnJan1VacBalanceInDays
		
		* division
		*oWorksheet.RANGE("AH20").VALUE = "Div: " + PADL(INT(oWorksheet.RANGE("A" + lcRow).VALUE),2,"0")
		oWorksheet.RANGE("AH20").VALUE = "Div: " + oWorksheet.RANGE("A" + lcRow).VALUE
		
		* YAY PRINT!!!
		oWorksheet.PageSetup.PrintArea = "$AH$2:$AJ$21"
		oWorksheet.PrintOut()
		
		* MARK AS PRINTED
		oWorksheet.RANGE("AB" + lcRow).VALUE = "Y"
	ENDIF
	
	
*!*		IF ISNULL( oWorksheet.RANGE("Z" + lcRow).VALUE ) THEN
*!*			EXIT for
*!*		ENDIF

ENDFOR


* SAVE CHANGES

oWorkbook.Save()

oWorkbook.Close()

oExcel.Quit()