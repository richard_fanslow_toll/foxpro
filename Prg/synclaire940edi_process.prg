*!* m:\dev\prg\synclaire940edi_process.prg
WAIT WINDOW "Now in PROCESS Phase..." NOWAIT

CD &lcPath

LogCommentStr = "LOADED"
ll940test = .f.

If ll940test
  xpath = "f:\ftpusers\synclaire\940intest\"
  lnNum = Adir(tarray,xpath+"*.*")
Else
  lnNum = Adir(tarray,lcPath+"*.*")
Endif


*lnNum = ADIR(tarray,"*.*")

IF lnNum = 0
	WAIT WINDOW AT 10,10 "      No "+cCustname+" 940s to import     " TIMEOUT 3
	NormalExit = .T.
	THROW
ENDIF

ASSERT .F. MESSAGE "At initial input file loop"

FOR thisfile = 1  TO lnNum
*	Xfile = lcPath+tarray[thisfile,1] && +"."

  If ll940test
    xfile = xpath+tarray[thisfile,1] && +"."
  Else
    xfile = lcPath+tarray[thisfile,1] && +"."
  Endif

	cfilename = ALLTRIM(LOWER(tarray[thisfile,1]))
	nFileSize = VAL(TRANSFORM(tarray(thisfile,2)))
*WAIT WINDOW "" &message_timeout2
	ArchiveFile = (lcArchivePath+cfilename)

	WAIT WINDOW "Importing file: "+cfilename &message_timeout2
	lcStr = FILETOSTR(Xfile)
	cDelimiter = SUBSTR(lcStr,4,1)  && they are sending us either ^ or *
	RELEASE lcStr
	cTranslateOption = "CARROT"
	DO m:\dev\prg\loadedifile WITH Xfile,cDelimiter,cTranslateOption,cCustname

	SELECT x856
	LOCATE
	IF lTesting
*BROWSE
	ENDIF

	LOCATE
	LOCATE FOR x856.segment = "GS" AND x856.f1 = "FA"
	IF FOUND() && If a 997
		lc997Path = 'F:\FTPUSERS\BCNY\997IN\'  && DO NOT CHANGE THIS PATH!
		c997file = lc997Path+cfilename
		COPY FILE [&xfile] TO [&c997file]
		DELETE FILE [&xfile]
		RELEASE lc997Path,c997file
		LOOP
	ENDIF

*!* Code below to move CA files to the correct folder for processing, added 06.12.2014
	IF !lTesting
		LOCATE FOR ALLT(x856.segment) = "N9" AND ALLT(x856.f1) = "WH"
		lCA = IIF(ALLT(x856.f2) = "CA",.T.,.F.)

		IF lCA AND cOffice = "I" && Brought in to NJ folder, but a CA file
			WAIT WINDOW "File "+cfilename+" is a CA file...moving" TIMEOUT 2
			RELEASE lCA
			lcPath2 = STRTRAN(ALLTRIM(lcPath),"NJ","CA")
			transfile = (lcPath2+cfilename)
			COPY FILE [&xfile] TO [&transfile]
			IF FILE(Xfile)
				DELETE FILE [&xfile]
			ENDIF
			LOOP
		ENDIF

		IF !lCA AND cOffice = "C" && Brought in to CA folder, but a NJ file
			WAIT WINDOW "File "+cfilename+" is a CA file...moving" TIMEOUT 2
			RELEASE lCA
			lcPath2 = STRTRAN(ALLTRIM(lcPath),"CA","NJ")
			transfile = (lcPath2+cfilename)
			COPY FILE [&xfile] TO [&transfile]
			IF FILE(Xfile)
				DELETE FILE [&xfile]
			ENDIF
			LOOP
		ENDIF
	ENDIF

	c940XferFile = ("F:\FTPUSERS\Synclaire\940xfer\"+cfilename)
	COPY FILE [&xfile] TO [&c940XferFile]  && Copies 940 file to xfer folder for 997 creation

	LOCATE
	LOCATE FOR x856.segment = "GS" AND x856.f1 = "RG"
	IF FOUND() && If a 754
		c754XferFile = ("F:\FTPUSERS\Synclaire\754in\"+cfilename)
		COPY FILE [&xfile] TO [&c754XferFile]  && Moves 754 files to the correct folder
		DELETE FILE [&xfile]
		RELEASE c754XferFile
		LOOP
	ENDIF
	IF lTesting
		LOCATE
	ENDIF

	CREATE CURSOR ptdata (ship_ref c(20),PNP l,Prepack l,splitpt l)
	SELECT ptdata
	INDEX ON ship_ref TAG ship_ref
	SELECT x856
	LOCATE
	SCAN FOR INLIST(x856.segment,"W05","W01")
		IF x856.segment = "W05"
			m.ship_ref = ALLT(x856.f2)
			INSERT INTO ptdata FROM MEMVAR
		ELSE
			IF INLIST(x856.f2, "EA", "PR") && AND nAcctNum = 6521
				REPLACE ptdata.PNP WITH .T. IN ptdata
			ELSE
				REPLACE ptdata.Prepack WITH .T. IN ptdata
			ENDIF
			IF ptdata.PNP AND ptdata.Prepack
				REPLACE ptdata.splitpt WITH .T. IN ptdata
			ENDIF
		ENDIF
	ENDSCAN
	SELECT ptdata
	LOCATE
	IF lTesting
*BROWSE
	ENDIF

	c940XferFile = ("f:\ftpusers\synclaire\940xfer\"+cfilename)
	COPY FILE [&xfile] TO [&c940XferFile]  && Creates a copy for 997s to run from

	DO create_pt_cursors WITH cUseFolder && Added to prep for SQL Server transition

	lDoImport = .T.
	DO ("m:\dev\prg\"+cCustname+"940edi_bkdn")

	IF !lDoImport
		DELETE FILE [&xfile]
		LOOP
	ELSE
		DO "m:\dev\prg\all940_import"
	ENDIF

NEXT thisfile

&& now clean up the 940in archive files, delete for the upto the last 10 days
**deletefile(lcArchivePath,20)
** moved to anotyher location

WAIT CLEAR
WAIT "Entire "+cCustname+" 940 Process Now Complete" WINDOW &message_timeout2
