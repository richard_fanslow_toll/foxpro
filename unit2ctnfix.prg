lparameters xoffice, xmod

close databases all
set deleted on

goffice=xmod
gmasteroffice=xoffice
guserid="FIX"
xmasteroffice=gmasteroffice
gmodfilter="mod='"+goffice+"'"

use ("f:\auto\xlocqty"+goffice) alias xlocqty

useca("adj","wh")

useca("inven","wh",,,"mod='"+goffice+"'")
useca("invenloc","wh",,,"mod='"+goffice+"'")

m.adjdt=date()
m.comment="CARTON TO UNITS"

select xlocqty
scan for problem="carton in unit loc"
	wait window recno() nowait

	scatter memvar

	m.totqty=m.locqty*val(m.pack)
	m.remainqty=m.totqty
	m.removeqty=0
	m.inadj=.t.
	m.units=.t.
	m.pack="1"
	m.whseloc=xlocqty.whseloc
	insertinto("adj","wh",.t.)
	
	m.totqty=-m.locqty
	m.remainqty=0
	m.removeqty=m.totqty
	m.inadj=.f.
	m.units=.f.
	m.pack=xlocqty.pack
	m.whseloc=xlocqty.whseloc
	insertinto("adj","wh",.t.)
	
	m.totqty=-m.locqty*val(m.pack)
	m.remainqty=0
	m.removeqty=m.totqty
	m.inadj=.f.
	m.units=.t.
	m.pack="1"
	m.whseloc="RACK"
	insertinto("adj","wh",.t.)
endscan

tu("adj")

select accountid, units, style, color, id, pack from adj with (buffering=.t.) group by 1,2,3,4,5,6 into cursor xrpt

i=0

scan
	i=i+1
	wait window i nowait
	
	select inven
	locate for accountid=xrpt.accountid and units=xrpt.units and style=xrpt.style and color=xrpt.color and id=xrpt.id and pack=xrpt.pack
	if found()
		fixinven(.t.,,.t.)
	endif
endscan

gunshot()