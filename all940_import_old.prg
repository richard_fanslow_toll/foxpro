WAIT WINDOW "Now in IMPORT Phase..." NOWAIT
EmailcommentStr = ""

cOfficeImp = IIF(cOffice = "N","I",cOffice)
nUploadCount = 0
m.acct_num = nAcctNum
cPTString = ""

IF lTestImport OR lTesting
	cUseFolder = "F:\WHP\WHDATA\"
ENDIF

guserid = "TGFPROC"

useca("pt","wh")
useca("ptdet","wh")

IF USED('outship')
	USE IN outship
ENDIF

SELECT xpt
SET ORDER TO
LOCATE
lnCount = 0
STORE RECCOUNT() TO lnCount
WAIT WINDOW "Picktickets to load ...........  "+STR(lnCount) NOWAIT &&TIMEOUT 2

IF nAcctNum = 6561
cPTString = PADR("CONSIGNEE",42)+PADR("PO NUM",27)+PADR("CANCEL",12)+PADR("PICKTICKET",22)+PADR("QTY",6)+"SALESORDER"+CHR(13)
else
cPTString = PADR("CONSIGNEE",42)+PADR("PO NUM",27)+PADR("CANCEL",12)+PADR("PICKTICKET",22)+"QTY"+CHR(13)
endif

ASSERT .F. MESSAGE "At PT upload scan"

gMasterOffice = IIF(INLIST(cOffice,"J","I"),"N",cOffice)
gOffice = cMod

** init some variables for the extra PTID verification  PG 04/04/2017
xptid =0
xdisaster = .f.

SCAN
	WAIT "AT XPT RECORD "+ALLTRIM(STR(RECNO())) WINDOW NOWAIT NOCLEAR
	*!* Check for existence of PT in outship
	*!* First choice does a select by PT 

	csq1 = [select * from outship where accountid = ]+TRANSFORM(nAcctNum)+[ and office = ']+cOfficeImp+[' and ship_ref = ']+PADR(ALLTRIM(xpt.ship_ref),20)+[']
	xsqlexec(csq1,"tempos1",,"wh",,,,,,.T.)
	IF RECCOUNT('tempos1')>0
		WAIT WINDOW "Found in outship...looping" NOWAIT
		USE IN tempos1
		SELECT xpt
		LOOP
	ENDIF

	SELECT pt
	xShip_ref = xpt.ship_ref
	lcQuery = [select * from pt where accountid = ]+TRANSFORM(nAcctNum)+[ and ship_ref = ']+xShip_ref+[']
	xsqlexec(lcQuery,"p1",,"wh")
	SELECT p1
	LOCATE
	IF RECCOUNT('p1') > 0
		USE IN p1
		SELECT xpt
		LOOP
	ENDIF
	USE IN p1

	SELECT xpt
	SCATTER MEMVAR MEMO BLANK
	
	IF nAcctNum = 6561
	cPTString = cPTString+CHR(13)+PADR(ALLT(xpt.consignee),42)+PADR(ALLTRIM(xpt.cnee_ref),27)+PADR(DTOC(xpt.CANCEL),12)+PADR(ALLTRIM(xpt.ship_ref),22)+PADR(ALLTRIM(STR(xpt.qty)),6)+cCO
	else
	cPTString = cPTString+CHR(13)+PADR(ALLT(xpt.consignee),42)+PADR(ALLTRIM(xpt.cnee_ref),27)+PADR(DTOC(xpt.CANCEL),12)+PADR(ALLTRIM(xpt.ship_ref),22)+ALLTRIM(STR(xpt.qty))
	endif
	nPTID = xpt.ptid
	SCATTER FIELDS EXCEPT ptid MEMVAR MEMO
	m.adddt = DATETIME()
	m.addby = "TGF-PROC"
	m.addproc = "940UPLOAD"

	SELECT pt
	m.office = cOffice
	m.mod = cMod
	insertinto("pt","wh",.T.)

  If xptid!=0 And xptid=pt.ptid && check to make sure that xptid has changed since the last scan loop, if not then abort and alert
    xdisaster = .t.
  Endif 

	nUploadCount = nUploadCount  +1
   
	SELECT xptdet
	SCATTER MEMVAR MEMO BLANK
	SCAN FOR ptid=xpt.ptid
		SCATTER MEMVAR MEMO
		IF !lTesting
			m.office = cOffice
			m.mod = cMod
		ENDIF
		m.accountid = xpt.accountid
		m.ship_ref = xpt.ship_ref
		m.adddt = DATETIME()
		m.addby = "TGF-PROC"
		m.addproc = "940UPLOAD"
		m.ptid=pt.ptid
		insertinto("ptdet","wh",.T.)
	ENDSCAN
  xptid= pt.ptid
ENDSCAN

If !xdisaster 
  tu("pt")
  tu("ptdet")
Else
  tsubject= "EDI PT Upload Error, MIxed PTIDs for "+cCustname+" Pickticket Upload: " +TTOC(DATETIME())
  tattach = ""
  tmessage = "PT upload error for "+cCustname+CHR(13)+"From File: "+cfilename+CHR(13)+"and PTID = "+Transform(xptid)
  tFrom ="TGF EDI Operations <toll-edi-ops@tollgroup.com>"
  tsendto = "pgaidis@fmiint.com,joe.bianchi@tollgroup.com,Todd.Margolin@tollgroup.com,Mike.Winter@tollgroup.com"
  tcc = "darren.young@tollgroup.com"
  DO FORM m:\dev\frm\dartmail2 WITH tsendto,tFrom,tsubject,tcc,tattach,tmessage,"A"
  return
Endif 

Select ptid,ship_ref From ptdet Group By ptid,ship_ref Into Cursor xpts

IF nUploadCount > 0
	pthist("P")
	pthist("E")
ENDIF

SELECT xpt
LOCATE
pickticket_num_start = xpt.ship_ref
GOTO BOTT
pickticket_num_end = xpt.ship_ref
LOCATE
COPY FILE [&xfile] TO [&ArchiveFile]

DELETE FILE [&xfile]

IF nUploadCount > 0
	WAIT WINDOW "Picktickets uploaded...........  "+STR(nUploadCount) NOWAIT &&TIMEOUT 2
ELSE
	WAIT WINDOW "No New Picktickets uploaded from file "+xfile NOWAIT &&TIMEOUT 2
ENDIF

WAIT "940 Import Round Complete" WINDOW NOWAIT && TIMEOUT 2
WAIT CLEAR

USE IN pt
USE IN ptdet
If Used("outship")
 use IN outship
Endif

currfile = xfile
DO CASE
	CASE cOffice = "C"
		cOfficeGroup = "SP"
	CASE INLIST(cOffice,"L","Z")
		cOfficeGroup = "ML"
	CASE INLIST(cOffice,"K","S")
		cOfficeGroup = "KY"
	CASE INLIST(cOffice,"J","I","N")
		cOfficeGroup = "NJ"
	CASE cOffice = "M"
		cOfficeGroup = "FL"
	CASE INLIST(cOffice,"X","Y")
		cOfficeGroup = "CR"
	OTHERWISE
		SELECT 0
		USE F:\3pl\DATA\mailmaster ALIAS mm SHARED
		LOCATE FOR edi_type = "MISC" AND taskname = "GENERAL"
		tsendto = ALLTRIM(IIF(mm.use_alt,mm.sendtoalt,mm.sendto))
		tcc = ALLTRIM(IIF(mm.use_alt,mm.ccalt,mm.cc))
		USE IN mm
		cOfficeGroup = "XX"
ENDCASE
IF lTesting
	tcc = ""
ENDIF

EmailcommentStr = EmailcommentStr+CHR(13)+cPTString

*************************************************************************************************
*!* E-Mail process
*************************************************************************************************

ASSERT .F. MESSAGE "At email"
IF nUploadCount > 0
	cPTQty = ALLT(STR(nUploadCount))
*SET STEP ON
	IF lEmail
		IF cOfficeGroup = "XX"
			tcc = ""
		ENDIF
		cMailLoc = ICASE(INLIST(cOffice,"L","Z"),"Mira Loma",INLIST(cOffice,"K","S"),"Louisville",INLIST(cOffice,"Y","Z"),"Carson",cOffice = "M","Miami",INLIST(cOffice,"I","N","J"),"New Jersey","San Pedro")
		tsubject= "TGF "+cMailLoc+" "+cCustname+" Pickticket Upload: " +TTOC(DATETIME())
		tattach = ""
		tmessage = "Uploaded "+cPTQty+" Picktickets for "+cCustname+CHR(13)+"From File: "+cfilename+CHR(13)
		tmessage = tmessage + EmailcommentStr
		tFrom ="TGF EDI Operations <toll-edi-ops@tollgroup.com>"
		IF lTesting OR lTestImport
			tmessage = tmessage+CHR(13)+"*TEST DATA* - LOADED INTO F:\WHP TABLES"
		ENDIF
		IF nAcctNum != 6532
			DO FORM m:\dev\frm\dartmail2 WITH tsendto,tFrom,tsubject,tcc,tattach,tmessage,"A"
		ENDIF
	ENDIF
	nUploadCount = 0
	cPTQty = ""
ENDIF
*************************************************************************************************

RETURN
