Parameters xtype

Set Deleted On
Set Escape On
Set enginebehavior 70
Set Safety Off
Set Date Dmy
Set Century On
Set Date AMERICAN

Do setenvi

Public RecPtr
Public gcDocNumber
Public thisfile
Public xfile

gcDocNumber=""
RecPtr =0

Close Data All

xrunall=.T.

xaccountid=5687
xacctname="UNDER ARMOUR"

*xtype = "PO"
xtype = "ASN"

If xtype = "ASN"
  lcpath="f:\ftpusers\underarmour\asnin\"
  lcholdpath="f:\ftpusers\underarmour\asnin\newhold\"
  lcarchivepath="f:\ftpusers\underarmour\asnin\archive\"
Endif

If xtype = "PO"
  lcpath="f:\ftpusers\underarmour\poin\"
  lcarchivepath="f:\ftpusers\underarmour\poin\archive\"
  lcholdpath="f:\ftpusers\underarmour\asnin\newhold\"
Endif


If !xrunall
  lnnum=1
*  cfilename = alltrim(getfile())
  cfilename=Alltrim(lcarchivepath+"manh_tpm_shipment0004192734.xml ")
Else
  lnnum = Adir(tarray,lcpath+"*.xml")
  If lnnum = 0
    Return
  Endif
Endif

Create Cursor csrpodata (;
ponum Char(25),;
start d,;
cancel d,;
name Char(25),;
name2 Char(25),;
street Char(25),;
street2 Char(25),;
city Char(25),;
state Char(2),;
zip Char(10);
)
#Define Tab Chr(0x9)

Create Cursor orderdetailsdata (;
ibd     Char(20),;
ponum   Char(25),;
linenum Char(10),;
odata   Memo)

Create Cursor xmlloaded (;
container char(20),;
ibd     Char(20),;
ponum   Char(25),;
whseloc Char(10),;
shipid Char(10),;
qty     Int )

use f:\wo\wodata\detail in 0
use f:\underarmour\data\uadetail in 0
select * from uadetail where .f. into cursor uatemp readwrite
select uatemp
scatter memvar memo blank


useca("ctnucc","wh")
Select * From ctnucc Where .F. Into Cursor uaucctemp Readwrite
Scatter Memvar Memo Blank

Use F:\underarmour\Data\podata In 0

llclosetables = .F.
*!*  If !Used("shiphdr")
*!*    Use F:\underarmour\Data\shiphdr In 0
*!*    Use F:\underarmour\Data\cartons In 0
*!*    llclosetables = .T.
*!*  Endif

*!*  Use F:\underarmour\Data\ShipMfst In 0

xsqlexec("select * from dellocs",,,"wo")
index on str(accountid,4)+location tag acct_loc
set order to

Select 200
Create Cursor temp1 (field1 c(254))
Select temp1

xdateloaded=Dtot({})
*Set Step On



For thisfile = 1  To lnnum

  Select 200
  Create Cursor temp1 (field1 c(254))
  Select temp1

  xfile = lcpath+tarray[thisfile,1] && +"."
  xholdfile = lcholdpath+tarray[thisfile,1] && +"."
  cfilename = Alltrim(Lower(tarray[thisfile,1]))
  ctns=0

**issues with dateloaded being same for multiple files - mvw 11/30/10
  Do While xdateloaded=Datetime()
* Wait Timeout 1
  Enddo
  xdateloaded=Datetime()

  Wait Window At 10,10 "uploading file: "+xfile Nowait

  lcStr = Filetostr(xfile)
  lnAt = At(Chr(9),lcStr)
 
 If lnAt >0
    lcStr = Strtran(lcStr,Chr(9),"")
    Strtofile(lcStr,"h:\fox\text.xml")
    cchar=Chr(13)+Chr(10)
    Append From "h:\fox\text.xml" Type Delimited With Character &cchar
    Replace All field1 With Alltrim(field1)
    Locate
  Else
    lcStr = Strtran(lcStr,"><",">"+Chr(13)+"<")
    Strtofile(lcStr,"h:\fox\text.xml")
    cchar=Chr(13)+Chr(10)
    Append From "h:\fox\text.xml" Type Delimited With Character &cchar
    Replace All field1 With Alltrim(field1)
    Locate
  Endif

 If lnAt >0 And Reccount("temp1") = 2
    lcStr = Filetostr(xfile)
    lcStr = Strtran(lcStr,"><",">"+Chr(13)+"<")
    Strtofile(lcStr,"h:\fox\text.xml")
    cchar=Chr(13)+Chr(10)
    Append From "h:\fox\text.xml" Type Delimited With Character &cchar
    Replace All field1 With Alltrim(field1)
    Locate
 Endif 


  Strtofile(lcStr,"h:\fox\text.xml")
  cchar=Chr(13)+Chr(10)
  Append From "h:\fox\text.xml" Type Delimited With Character &cchar
  Replace All field1 With Alltrim(field1)
  Locate

  GotoTag("<DocType")

  Do Case
  Case ">PO<"$field1
    Doctype = "PO"
    Select temp1
    Go Top
  Case ["STANDARD">ASN</DocType>]$field1
    Doctype = "ASN"
    DocVariant = "STANDARD"
    Select temp1
    Go Top
  Case ["RESERVED">ASN</DocType>]$field1
    Doctype = "ASN"
    DocVariant = "RESERVED"
    Select temp1
    Go Top
  Otherwise
    Doctype = "ASN"
    Messagebox("Invalid XML type",0,"UA XML Upload")
    Return
  Endcase

  If Doctype = "PO"
    new_parse_po()
    archivefile  = (lcarchivepath+cfilename)
    If !File(archivefile)
      Copy File [&Xfile] To [&archivefile]
    Endif
    If File(xfile)
      Delete File [&xfile]
    Endif
*   Loop
  Endif

  If Doctype = "ASN"
    Select orderdetailsdata
    Select temp1
    parse_newasn2()
    archivefile  = (lcarchivepath+cfilename)
    If !File(archivefile)
      Copy File [&Xfile] To [&archivefile]
    Endif
    If File(xfile)
      Delete File [&xfile]
    Endif
*   LOOP
  Endif
Next

If xtype = "ASN"
  tfrom    ="TOLL EDI Processing Center <fmi-transload-ops@fmiint.com>"
  tsendto  = "Kristen.Floeck@tollgroup.com,Joe.Abbate@tollgroup.com,adonis.perez@tollgroup.com,isis.gonzalez@tollgroup.com"
  tsubject = "XML Data uploaded from UA........."
  tcc= "pgaidis@fmiint.com"
  tattach =""
  tmessage = ""
  Select xmlloaded
  Scan
    tmessage = tmessage+Alltrim(xmlloaded.container)+" - "+Alltrim(xmlloaded.ibd)+" - "+Alltrim(xmlloaded.ponum)+" - "+Alltrim(xmlloaded.whseloc)+" - "+Alltrim(xmlloaded.shipid)+" - "+Transform(xmlloaded.qty)+Chr(13)
  Endscan
  Do Form m:\dev\FRM\dartmail2 With tsendto,tfrom,tsubject,tcc,tattach,tmessage,"A"
Endif

*************************************************************************************************************
Procedure parse_newasn2

GotoTag("<Header")

GotoTag("<DocNumber")
m.docnumber=getfield(field1,[DocNumber>],"<")
m.reference =m.docnumber
gcDocNumber = m.docnumber

GotoTag("<ExtDocNumber")
m.docnumber=getfield(field1,[ExtDocNumber>],"<")

If DocVariant = "STANDARD"
  GotoTag([<DocType Variant="STANDARD"])    && get Hawb
  m.subbol=getfield(field1,[[<DocType Variant="RESERVED">],"<")
  m.asntype = "STANDARD"
Else
  GotoTag([<DocType Variant="RESERVED"])    && get Hawb
  m.subbol=getfield(field1,[[<DocType Variant="RESERVED">],"<")
  m.asntype = "RESERVED"
Endif

GotoTag("<SubBOL")    && get Hawb
m.subbol=getfield(field1,[SubBOL>],"<")
m.hawb = m.subbol

GotoTag("<Trailer")    && get Hawb
m.container=getfield(field1,[Trailer>],"<")
m.container = Strtran(m.container," ","")
m.container = Strtran(m.container,"-","")

GotoTag("<TotalNumOfCartons")    && get Hawb
m.totcartons=getfield(field1,[TotalNumOfCartons>],"<")
*set step on

GotoTag("<ShippedFrom")    && get Hawb
GotoTag("<ID")    && get Hawb
m.shipfrom=getfield(field1,[ID>],"<")

GotoTag("<Name")    && get Hawb
m.shipfromname=getfield(field1,[Name>],"<")

GotoTag("<ID")    && get Hawb
m.shipfromid=getfield(field1,[ID>],"<")

GotoTag([<Date DateQualifier="DeliveryDate"])    && get Hawb
m.deldate=getfield(field1,[<Date DateQualifier="DeliveryDate">],"<")

GotoTag("<MessageID")    && get Hawb
m.messageid=getfield(field1,[MessageID>],"<")


Skip 2 In temp1

llDone = .F.

Do While !llDone

  GotoTag("<Order")
  m.ordernum=getfield(field1,[Order>],"<")

  GotoTag("<Type")
  m.upc=getfield(field1,[Type>],"<")

  GotoTag("<CustomerPO")
  m.style=getfield(field1,[CustomerPO>],"<")

  GotoTag("<ShipTo")
  GotoTag("<ID")
  m.shipid=getfield(field1,[ID>],"<")
  m.shiptoID=getfield(field1,[ID>],"<")

  GotoTag("<Name")    && get Hawb
  m.shiptoname=getfield(field1,[Name>],"<")

********** roll up the Order Details Loop ************************************
  lcOrderDetailsStr = ""
  GotoTag("<OrderDetails")
  lcOrderDetailsStr = Alltrim( temp1.field1)
  lnORD_Start = Recno("temp1")

  llOrderDetailsDone = .F.
  Do While !llOrderDetailsDone
    Skip 1 In temp1
    If temp1.field1 ="<LineNum>"
      m.linenum =getfield(field1,[LineNum>],"<")
      m.palletstatus =getfield(field1,[LineNum>],"<")
    Endif
    If temp1.field1 ="<SAPPurchOrderNum>"
      m.ponum =getfield(field1,[SAPPurchOrderNum>],"<")
    Endif

    If temp1.field1 ="<SAPPurchOrderType>"
      m.potype =getfield(field1,[SAPPurchOrderType>],"<")
    Endif

    If temp1.field1 ="</OrderDetails>"
      Select orderdetailsdata
      Append Blank
      Replace odata   With lcOrderDetailsStr+Chr(13)+"</OrderDetails>"
      Replace ponum   With m.ponum
      Replace linenum With m.linenum
      Replace ibd     With m.ordernum
      Select temp1
*      lcOrderDetailsStr = lcOrderDetailsStr+Chr(13)+Alltrim(temp1.field1)
      lcOrderDetailsStr = ""
      Skip 1 In temp1
      If temp1.field1 ="<OrderDetails>"

        lcOrderDetailsStr = lcOrderDetailsStr+Chr(13)+Alltrim(temp1.field1)
** there is another OrderDetail loop
      Else
        llOrderDetailsDone = .T.
        m.orddetails = lcOrderDetailsStr
        Skip -1 In temp1
      Endif
    Else
      lcOrderDetailsStr = lcOrderDetailsStr+Chr(13)+Alltrim(temp1.field1)
    Endif
  Enddo

********** roll up the Carton Loop ************************************
  lcCartonStr = "<Carton>"

  if !GotoTag("<Carton")  && really should be here already
	Copy File [&Xfile] To [&xholdfile]
	return .f.
  endif

  lccarton = Alltrim( temp1.field1)
  lnCtn_Start = Recno("temp1")
  lnCtn = 1
  llCartonDone = .F.

  Do While !llCartonDone
    Skip 1 In temp1
    If temp1.field1 ="<OrderLine>"
      m.orderline =getfield(field1,[OrderLine>],"<")
    Endif
    If temp1.field1 ="</Carton>"
      lcCartonStr = lcCartonStr+Chr(13)+Alltrim(temp1.field1)
      Skip 1 In temp1
      If temp1.field1 ="<Carton>"
** write out this carton
        m.ctndetails = lcCartonStr
        Select orderdetailsdata
        Locate For linenum = m.orderline
        m.orddetails =  orderdetailsdata.odata
        m.ponum = orderdetailsdata.ponum
        m.serialno = orderdetailsdata.linenum
        Select temp1
        Do getCtnRecord

        lcCartonStr = "<Carton>"
        lnCtn = lnCtn +1
      Else
        m.ctndetails = lcCartonStr
        Select orderdetailsdata
        Locate For linenum = m.orderline
        m.orddetails =  orderdetailsdata.odata
        m.ponum = orderdetailsdata.ponum
        m.serialno = orderdetailsdata.linenum
        Select temp1

        Do getCtnRecord
        llCartonDone = .T.
        Skip -1 In temp1
      Endif
    Else
      lcCartonStr = lcCartonStr+Chr(13)+Alltrim(temp1.field1)

      If temp1.field1 = "<CartonID>"
        m.ucc=getfield(field1,[CartonID>],"<")
        m.pl_qty = m.pl_qty+1
      Endif

      If temp1.field1 =[<Value type="GrossWeight"]    && get Hawb
        m.ctnwt=getfield(field1,[Value type="GrossWeight">],"<")
        m.plweight = m.plweight + Val(m.ctnwt)
      Endif

      If temp1.field1 = [<Value type="Width"]    && get Hawb
        m.width=getfield(field1,[Value type="Width">],"<")
      Endif

      If temp1.field1 =[<Value type="Height"]    && get Hawb
        m.height=getfield(field1,[Value type="Height">],"<")
      Endif

      If temp1.field1 = [<Value type="Length"]  && get Hawb
        m.length=getfield(field1,[Value type="Length">],"<")
        m.cbm = m.cbm+(Val(m.width)*Val(m.height)*Val(Length))/1728
        m.suppdata = m.suppdata+"ASN*"+m.docnumber+Chr(13)
        m.suppdata = m.suppdata+"CTNWT*"+Transform((Val(m.ctnwt)*2.2))+Chr(13)
        m.suppdata = m.suppdata+"CTNLENGTH*"+m.length+Chr(13)
        m.suppdata = m.suppdata+"CTNWIDTH*"+m.width+Chr(13)
        m.suppdata = m.suppdata+"CTNHEIGHT*"+m.height+Chr(13)
        m.suppdata = m.suppdata+"CTNCUBE*"+Transform(((Val(m.length)/2.524) * (Val(m.width)/2.542) * (Val(Height))/2.542)/1728.0)+Chr(13)
      Endif
    Endif
  Enddo
  llCartonFound = .T.
  m.pl_qty = 0
  m.plweight =0
  m.cbm = 0.0

  Skip 2 In temp1
  If temp1.field1 = "</Header>"
    llDone =.T.
  Endif

  If temp1.field1 = "<Order>"
    llOrderFound = .T.
    Do getRecord
    llOrderDetailsDone = .F.
  Endif

  If llDone = .T.
    Exit
  Endif

Enddo

gmasteroffice ="N"
goffice = "I"

Select uaucctemp
Scan
  Do Case
  Case m.potype = "ZPT"
    m.optype = "TRANSLOAD"
  Case Inlist(m.potype,"YCON","ZNB") And !Inlist(m.shiptoID,"9000","9001","9002","9003")
    m.optype = "WHSEDIST "
  Otherwise
    m.optype = "UNKNOWN  "
  Endcase

  Replace suppdata With suppdata +"SHIPTOID*"+m.shiptoID+Chr(13)
  Replace suppdata With suppdata +"SHIPTONAME*"+m.shiptoname+Chr(13)
  Replace suppdata With suppdata +"ASNTYPE*"+m.asntype+Chr(13)
  Replace suppdata With suppdata +"POTYPE*"+m.potype+Chr(13)
  Replace suppdata With suppdata +"OPTYPE*"+m.optype+Chr(13)

  Do Case
  Case m.shiptoID="9007"
    Replace suppdata With suppdata +"PTNAME*DH VENLO DVS"+Chr(13)
    Replace suppdata With suppdata +"PTADDRESS*50 POPEWEG"+Chr(13)
    Replace suppdata With suppdata +"PTCSZ*VENLO, NL 5928"+Chr(13)
    Replace suppdata With suppdata +"PTCOUNTRY*SC"+Chr(13)

  Case m.shiptoID="9008"
    Replace suppdata With suppdata +"PTNAME*DH HONG KONG LFL"+Chr(13)
    Replace suppdata With suppdata +"PTADDRESS*DD122 BLK2 YUNG YUEN RD"+Chr(13)
    Replace suppdata With suppdata +"PTCSZ*WING NING TSEN YL "+Chr(13)
    Replace suppdata With suppdata +"PTCOUNTRY*HK"+Chr(13)

  Case m.shiptoID="9011"
    Replace suppdata With suppdata +"PTNAME*DH COLON LSP"+Chr(13)
    Replace suppdata With suppdata +"PTADDRESS*10 RANDOLPH AVE MANZANA LOTE 869"+Chr(13)
    Replace suppdata With suppdata +"PTCSZ*COLON FREE ZONE, 0302-00122"+Chr(13)
    Replace suppdata With suppdata +"PTCOUNTRY*PA"+Chr(13)

  Case m.shiptoID="9004"
    Replace suppdata With suppdata +"PTNAME*DH TORONTO NLS"+Chr(13)
    Replace suppdata With suppdata +"PTADDRESS*475 ADMIRAL BLVD, UNIT A"+Chr(13)
    Replace suppdata With suppdata +"PTCSZ*MISSISSAUGA,ONTARION L5T 2N1"+Chr(13)
    Replace suppdata With suppdata +"PTCOUNTRY*CA"+Chr(13)

  Case m.shiptoID="9005"
    Replace suppdata With suppdata +"PTNAME*DH BRAMPTON NLS"+Chr(13)
    Replace suppdata With suppdata +"PTADDRESS*200 CHRYSLER DRIVE"+Chr(13)
    Replace suppdata With suppdata +"PTCSZ*BRAPTON, ONTARIO L6S 6G8"+Chr(13)
    Replace suppdata With suppdata +"PTCOUNTRY*CA"+Chr(13)

  Endcase

  Do Case
  Case "MIA"$xfile
    Replace whseloc With "TRSUS_MIA" In uaucctemp
    Replace office With "M"
    Replace Mod With "M"

  Case "NJ"$xfile
    Replace whseloc With "TRSUS_NJ" In uaucctemp
    Replace office With "N"
    Replace Mod With "I"
  Otherwise
    Replace whseloc With "TRSUS_UNK" In uaucctemp
  Endcase
Endscan

select uaucctemp
locate
thisibd=alltrim(uaucctemp.reference)

**new logic to eliminate overwriting data - mvw 03/06/18
**  -	If a new IBD for a wo that already has a breakdown created and scanning has not started for that WO, send an email to Ops that the 
**			breakdown will need to be deleted and re-created.
**  -	If an existing IBD comes in that is already on a breakdown, scanning has not yet started for that IBD and the ctn qty is the same 
**			as existing breakdown, delete existing and import as is done currently - update the inwonum to match wo.
**  -	If an existing IBD comes in that is already on a breakdown, scanning has not yet started for that WO and the ctn qty differs from 
**			the existing breakdown, delete existing and import as is done currently and email ops that the breakdown will need to be deleted 
**			and re-created.
**  -	If an existing IBD comes in that is already on a breakdown and scanning has started for that IBD,  email ops that an update was sent
**			 for that IBD but scaning has been started so the update cannot be imported - changes will need to be made manually.
xload=.t.
xsubject=""
xbody=""
xsendto=""
xattach=""
xinwonum=0

xquery="select inwonum, max(adddt) as zadddt from ctnucc "+;
	"where accountid=5687 and container='"+uaucctemp.container+"' and adddt>={"+transform(date()-60)+"} and inwonum<>0 "+;
	"group by inwonum order by zadddt desc"
if xsqlexec(xquery,"xtemp",,"wh")>0
	select detail
	locate for wo_num=xtemp.inwonum and "IBD*"+thisibd $ suppdata
	if !found()
		locate for wo_num=xtemp.inwonum
		if found()
			xsendto=iif(detail.office="M","joe.abbate@tollgroup.com,adonis.perez@tollgroup.com,isis.gonzalez@tollgroup.com","kristen.floeck@tollgroup.com")
			xsubject="New IBD received for UA WO "+transform(xtemp.inwonum)
			xbody="Breakdown will need to be deleted and re-imported!"
		endif
	else
		xsendto=iif(detail.office="M","joe.abbate@tollgroup.com,adonis.perez@tollgroup.com,isis.gonzalez@tollgroup.com","kristen.floeck@tollgroup.com")

		xquery="select sum(1) as ctnqty, max(palletid) as palletid, max(outwonum) as outwonum from ctnucc "+;
			"where accountid=5687 and container='"+uaucctemp.container+"' and reference='"+thisibd+"'"
		if xsqlexec(xquery,"xtemp2",,"wh")>0
			do case
			case emptynul(xtemp2.palletid) and emptynul(xtemp2.outwonum) and detail.pl_qty=xtemp2.ctnqty
				xinwonum=detail.wo_num
			case emptynul(xtemp2.palletid) and emptynul(xtemp2.outwonum)
				xsubject="An update to an existing IBD has been received for UA WO "+transform(xtemp.inwonum)
				xbody="Breakdown will need to be deleted and re-imported!"
			otherwise
				xload=.f.
				xsubject="An update to an existing IBD that has ALREADY BEGUN SCANNING has been received for UA WO "+transform(xtemp.inwonum)
				xbody="Because scanning has already begun, no update can be made to this IBD!"+chr(13)+chr(10)+chr(13)+chr(10)+"File: "+xfile
				xsendto=xsendto+",mike.winter@tollgroup.com,juan.rocio@tollgroup.com,paul.gaidis@tollgroup.com"
				xattach="h:\fox\uccdata.xls"

				select uaucctemp
				copy to &xattach type xls
			endcase
		endif

		use in xtemp2
	endif
endif

use in xtemp

if !emptynul(xsubject)
	if emptynul(xsendto)
		xsendto="kristen.floeck@tollgroup.com,joe.abbate@tollgroup.com,adonis.perez@tollgroup.com,isis.gonzalez@tollgroup.com"
	endif
	xcc=""
	do form m:\dev\frm\dartmail2 with xsendto,"TGFSYSTEM",xsubject,xcc,xattach,xbody,"A"
endif

if xload
	lcQuery = [delete from ctnucc where accountid = 5687 and reference = ']+thisIBD+[']
	xsqlexec(lcQuery,"temp",,"wh")

	select uaucctemp
	scan
	  scatter memvar memo
	  m.inwonum=xinwonum
	  insertinto("ctnucc","wh",.t.)
	  m.suppdata = ""
	  m.orddetails = ""
	  m.ctndetails = ""
	endscan

	tu("ctnucc")
endif

select count(1) as qty, reference as ibd,container,ponum,shipid,whseloc from uaucctemp group by ponum,ibd,container into cursor temp
scan
	scatter memvar
	insert into  xmlloaded from memvar
endscan 

zap in uaucctemp
zap in orderdetailsdata


Return

Endproc
*********************************************************************************************************
Procedure new_parse_po

done = .F.

Do While !done

  GotoTag("<Orders")

  GotoTag("<Order>")
  m.ponum=getfield(field1,[Order>],"<")

  GotoTag("<SoldTo")
  GotoTag("<ID")    && get Hawb
  m.soldtoID=getfield(field1,[ID>],"<")

  GotoTag("<ShipTo")
  GotoTag("<ID")    && get Hawb
  m.shiptoID=getfield(field1,[ID>],"<")


  GotoTag("<Name")    && get Hawb
  m.name=Strtran(Upper(getfield(field1,[Name>],"<")),"&APOS;","")

  GotoTag("<Name2")    && get Hawb
  m.name2=Upper(getfield(field1,[Name2>],"<"))
  If m.name2 = "NA"
    m.name2=""
  Endif

  GotoTag("<Street")    && get Hawb
  m.street=Upper(getfield(field1,[Street>],"<"))

  GotoTag("<Street2")    && get Hawb
  m.street2=Upper(getfield(field1,[Street2>],"<"))
  If m.street2 = "NA"
    m.street2=""
  Endif

  GotoTag("<City")    && get Hawb
  m.city=Upper(getfield(field1,[City>],"<"))

  GotoTag("<State")    && get Hawb
  m.state=Upper(getfield(field1,[State>],"<"))

  GotoTag("<PostalCode")    && get Hawb
  m.zip=Upper(getfield(field1,[PostalCode>],"<"))

  GotoTag([<Date DateQualifier="ReqDelDate">])
  lcDate=getfield(field1,[Date DateQualifier="ReqDelDate">],"<")
  lxDate = Substr(lcDate,5,2)+"/"+Substr(lcDate,7,2)+"/"+Left(lcDate,4)
  m.start = Ctod(lxDate)

  GotoTag([<Date DateQualifier="CancelDate">])
  lcDate=getfield(field1,[Date DateQualifier="CancelDate">],"<")
  lxDate = Substr(lcDate,5,2)+"/"+Substr(lcDate,7,2)+"/"+Left(lcDate,4)
  m.cancel = Ctod(lxDate)

  Select csrpodata
  Insert Into csrpodata From Memvar
  Select podata
  Locate For podata.ponum = csrpodata.ponum
  If !Found()
    m.filename = xfile
    Insert Into podata From Memvar
  Else
    Select podata
    Delete For  podata.ponum = csrpodata.ponum
    m.filename = xfile
    Insert Into podata From Memvar
  Endif

  done=.T.
  Exit

Enddo

Select csrpodata
Zap

Endproc
*****************************************************************************
Procedure getCtnRecord

Select uaucctemp
Append Blank
m.asnfile = Justfname(xfile)
m.accountid= 5687
m.loaddt = Date()
Gather Memvar Memo
Replace orddetails With orderdetailsdata.odata In uaucctemp
m.suppdata = ""
Select temp1
Endproc
*************************************************************************************************************

**************************************************************************
Procedure getfield
Parameters sdata,match,enddata

lnstart = At(match,sdata)
firstquote = lnstart+Len(match)
cc = Substr(sdata,firstquote,1)
If cc ="<"
* ? "NA"
  Return "NA"
Endif
If Len(Alltrim(sdata)) = Len(Alltrim(match))+1
* ? "NA"
  Return "NA"
Endif

lnsecond = firstquote+1
Do While Substr(sdata,lnsecond,1)!=enddata
  lnsecond = lnsecond+1
Enddo

? "Data: "+Substr(sdata,firstquote,lnsecond-(firstquote))

Return Substr(sdata,firstquote,lnsecond-(firstquote))

Endproc


*************************************************************************************************************
Procedure getfield
Parameters sdata,match,enddata

lnstart = At(match,sdata)
firstquote = lnstart+Len(match)
cc = Substr(sdata,firstquote,1)
If cc ="<"
* ? "NA"
  Return "NA"
Endif
If Len(Alltrim(sdata)) = Len(Alltrim(match))+1
* ? "NA"
  Return "NA"
Endif

lnsecond = firstquote+1
Do While Substr(sdata,lnsecond,1)!=enddata
  lnsecond = lnsecond+1
Enddo

*? "Data: "+Substr(sdata,firstquote,lnsecond-(firstquote))

Return Substr(sdata,firstquote,lnsecond-(firstquote))

Endproc

**************************************************************************
Procedure GotoTag
Parameters tagname,stoptag

lnThisRec = Recno("temp1")

*? "lloking for TAG: "+tagname
xtagfilter=Iif(Empty(stoptag),"","and FIELD1!="+stoptag)

xxRec = 1
Do While field1!= tagname &&xtagfilter
  Skip 1 In temp1
  xxRec = xxRec+1
  If xxRec > 1000 or eof('temp1')
	if tagname="<Carton"
		return .f.
	endif
	
    Select temp1
    Goto lnThisRec
    Set Step On
  Endif
Enddo
Endproc

**************************************************************************
Procedure FindTag
Parameters tagname,stoptag
Do While field1!= tagname
  Skip 1 In temp1
Enddo
Endproc
**************************************************************************
Procedure getRecord
Select uatemp
Append Blank
m.filename = xfile
m.dateloaded=Date()
m.accountid= 5687
m.qty_type="CARTONS"
m.loose = .F.
m.type = "O"
m.uploadtm=Datetime()
m.acctname ="UNDER ARMOUR"
Gather Memvar Memo
Select temp1
Endproc
**************************************************************************
Procedure getCtnRecord

Select uaucctemp
Append Blank
m.asnfile = Justfname(xfile)
m.accountid= 5687
m.loaddt = Date()
Gather Memvar Memo
Replace orddetails With orderdetailsdata.odata In uaucctemp
m.suppdata = ""
Select temp1
Endproc
**************************************************************************

