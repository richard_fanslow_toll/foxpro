lparameters xoffice, xaccountid, xunits, xstyle, xcolor, xid, xpack

close databases all
set deleted on

goffice=upper(xoffice)

outdetfill(xaccountid, xunits, xstyle, xcolor, xid, xpack, .f., .f., .t.)

select * from outdet into cursor xoutdet
index on outdetid tag outdetid

select * from outloc into cursor xoutloc
index on outdetid tag outdetid

select xoutdet
set relation to outdetid into xoutloc
